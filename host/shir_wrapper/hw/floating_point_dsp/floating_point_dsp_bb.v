module floating_point_dsp (
		input  wire        aclr,   //   aclr.aclr
		input  wire [31:0] ay,     //     ay.ay
		input  wire [31:0] az,     //     az.az
		input  wire        clk,    //    clk.clk
		input  wire        ena,    //    ena.ena
		output wire [31:0] result  // result.result
	);
endmodule

