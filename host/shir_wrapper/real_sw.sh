#!/bin/bash

cd sw
# create symlinks to memory images
ln -sf ../hw/rtl/generated/mem_initial.dat .
ln -sf ../hw/rtl/generated/mem_final.dat .

# compile
cmake .
make clean
make

# run software part with hugepage support
LD_PRELOAD="/usr/lib/libopae-c.so /usr/lib/libhugetlbfs.so" HUGETLB_MORECORE=yes HUGETLB_VERBOSE=99 ./run_shir_afu |& tee ../real_run.log

cd ..
# show area usage numbers from synthesis reports
grep --color=never -E 'Logic utilization|Total block memory bits|Total RAM Blocks|Total DSP Blocks' build_synth/build/output_files/afu_default.fit.summary
