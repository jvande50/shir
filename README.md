# README

SHIR is a multi-level IR and compiler for turning high-level functional expressions into low-level optimized code (e.g. VHDL) using rewrite rules.

## SHIR Compiler Prerequisites

### JDK 8, 11 or newer

On Ubuntu/Debian:
    
    ```bash
    $ sudo apt-get update
    $ sudo apt-get install default-jdk
    ```

### SBT

On Ubuntu/Debian:
    
    ```bash
    $ sudo apt-get install apt-transport-https curl gnupg -yqq
    $ echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | sudo tee /etc/apt/sources.list.d/sbt.list
    $ echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | sudo tee /etc/apt/sources.list.d/sbt_old.list
    $ curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo -H gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/scalasbt-release.gpg --import
    $ sudo chmod 644 /etc/apt/trusted.gpg.d/scalasbt-release.gpg
    $ sudo apt-get update
    $ sudo apt-get install sbt
    ```

For other systems the installation process is explained here: https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html

### Scala

On Ubuntu/Debian:
    
    ```bash
    $ sudo apt-get install scala
    ```

For other systems Coursier can be used to install Scala, as explained here: https://get-coursier.io/docs/cli-installation

## Setup IntelliJ IDEA

- delete any existing .idea folder from your directory
- update to the most recent version: `git pull`
- start IntelliJ IDEA
- close any opened project
- select import project
- select sbt
- make sure when IntelliJ IDEA asks you towards the end of the process to import the two modules (one is called -build)
- add the roots to the version control (a popup should appear about this)

## Acceleration Stack Setup (for the FPGA server and ModelSim)

### Prerequisites:

1. Check the version of gcc installed on your computer. If on Ubuntu 18.04, it should be 7.5.0. If on Ubuntu 16.04, go with default gcc version, and dont try to force update it, otherwise, later it will cause problems, when it tries to find equivalent 32-bit Libraries.
    
    ```bash
    $ gcc --version
    ```

2. Install cmake-3.18.3 (same as Edinburgh's machine) using the instructions from this [link](https://graspingtech.com/upgrade-cmake/).

3. Make sure that the repo list is updated, and build-essentials is installed.
    
    ```bash
    $ sudo apt update
    $ sudo apt upgrade
    $ sudo apt install build-essential
    ```
    There are some other pre-requisite packages that will be required during installation of ModelSim and OPAE, those pre-requisites are mentioned later in their own sections in this README.

4. Download the Acceleration stack v1.1 (or newer: v1.2.1) for Development from the [official link](https://www.intel.com/content/www/us/en/programmable/solutions/acceleration-hub/archives.html) and extract the tar.gz downloaded file.

5. You will be prompted to create an account or sign in using an existing account. The link can be broken sometimes and lead to your Intel Homepage. That is not the correct link. Wait until the link gets fixed.

### Basic Steps:

1. The extracted directory should look like this (Each subfolder will have some files inside it. Those are not shown here):

    ```
    ├── a10_gx_pac_ias_1_1_pv_dev_installer
        ├── components
        ├── licenses
        ├── setup.sh
    ```

    For v1.1 only: In the extracted directory, open the _setup.sh_ script and comment out the following lines of code:

    ```bash
    if [ "$PKG_TYPE" = "dev" ] ;then
        install_quartus_dev_package
    else
        install_quartus_rte_package
    fi
    ```
    Otherwise, when you run the script, Quartus will get installed, but the script gets stuck here for unknown reasons, and does not progress further. Quartus will need to be installed seperately. More recent versions finish their installation successfully.

2. Run the setup.sh script. When prompted to choose if you want to install OPAE, choose 'N'. This is because the version of OPAE that comes with v1.1 acceleration stack is outdated for our purpose and also, the script does not have support for installing OPAE on the Ubuntu distribution. OPAE will also need to be installed seperately.

3. The default installation directory this script uses is _~/inteldevstack_. The directory structure should look like this:

    ```
    ├── inteldevstack
        ├── a10_gx_pac_ias_1_1_pv
            ├── bin
            ├── hw
            ├── opencl
            ├── sw
        ├── a10_gx_pac_ias_1_1_pv.tar.gz
        ├── init_env.sh
    ```

4. Open that directory to see if the files have been extracted correctly and if _init_env.sh_ script is present. _init_env,sh_ will be used for exporting all the requred environment variables and will be modified later. Write the following line in your _~/.bashrc_ file, to source the file everytime the terminal is opened:

    ```bash
    source /path/to/directory/init_env.sh
    ```

### Installing Quartus 17.1 (for Acceleration Stack v1.1 only) and ModelSim:

1. Install the following pre-requisities:

    ```bash
    sudo apt install libc6:i386 libncurses5:i386 libxtst6:i386 libxft2:i386 libstdc++6:i386 libc6-dev-i386 lib32z1 lib32ncurses5 lib32bz2-1.0 libpng12 libqt5xml5 liblzma-dev
    ```
    __Note__: You may get the error that libpng12 library is not available. In that case, follow the instructions on this [link](https://www.linuxuprising.com/2018/05/fix-libpng12-0-missing-in-ubuntu-1804.html). You may also get the error that lib32bz2-1.0 is not available, in that case, run the following command to get the equivalent 32-bit library ([ref link](https://askubuntu.com/questions/637113/unable-to-locate-package-lib32bz2-1-0)):

    ```bash
    sudo apt install libbz2-1.0:i386
    ```

1. In the original extracted folder _inteldevstack_, under the folder _components_, there are setup files for Quartus 17.1. Run the following files in order, by double clicking the file (and not from the terminal):
    
    1. QuartusProSetup-17.1.0.240-linux.run
    
    2. QuartusProSetup-17.1.1.273-linux.run
    
    3. QuartusProSetup-17.1.1-Patch-1.01dcp-linux.run
    
    4. quartus-17.1.1-1.36-linux.run
    
    5. AOCLProSetup-17.1.1.273-linux.run (__Note__: This file will need to be run with root privileges. Otherwise you will need to manually copy a file after the installation, which will be shown to you by the software during installation)

    In case, you get an error that the files are not executable, then run the following command from terminal to make them executable: (__Note__: This is applicable for any executable file used in this guide)

    ```bash
    $ sudo chmod +x filename
    ```

    You can choose any installation directory you want (should not contain special characters), but the environment variables will need to be set correctly. The default installation directory is _~/intelFPGA_pro/17.1/_ for all the packages. Make sure that the directory is same for all the packages.

2. In the _init_env.sh_ script, set the following environment variables and add them to the PATH:

    ```bash
    export QUARTUS_HOME="/path/to/directory/17.1/quartus"
    QUARTUS_BIN="/path/to/directory/17.1/quartus/bin"
    export PATH="${QUARTUS_BIN}":"${PATH}"
    export PATH="${QUARTUS_HOME}":"${PATH}"
    ```
    __Note__: Verify that the installation is working by running:

    ```bash
    path/to/directory/quartus/bin/quartus
    ```

3. Download the ModelSim software package for v17.1 from this [official link](https://fpgasoftware.intel.com/17.1/?edition=pro&platform=linux&product=modelsim_ae#tabs-2). There is only a single package that contains both the student and non-student versions of ModelSim. During installation, you will be prompted to choose which version you want to use. It is preferable that ModelSim is installed in the same directory as Quartus, i.e., during installation, set the installation directoy as _~/intelFPGA_pro/17.1/_.

4. There are many pre-requisite 32-bit packages required for ModelSim, so install ModelSim using instructions mentioned in [this link](https://gist.github.com/Razer6/cafc172b5cffae189b4ecda06cf6c64f). It is highly likely that there will be some errors still left after installation, so verify that the installation is working correctly, and fix the bugs accordingly, following the instructions in the [same link](https://gist.github.com/Razer6/cafc172b5cffae189b4ecda06cf6c64f).

4. In the _init_env.sh_ script, set the following environment variables and add them to the PATH:

    ```bash
    export QUESTA_HOME="/path/to/directory/17.1/modelsim_ase"
    export MODELSIM_HOME="/path/to/directory/17.1/modelsim_ase"
    export MTI_HOME="/path/to/directory/17.1/modelsim_ase"
    QUESTA_BIN="/path/to/directory/17.1/modelsim_ase/bin"
    export PATH="${QUESTA_BIN}":"${PATH}"
    ```

5. Verify that the directory structure looks like this:

    ```
        ├── intelFPGA_pro
            ├── 17.1
                ├── hld
                ├── hls
                ├── ip
                ├── licenses
                ├── logs
                ├── modelsim_ase
                ├── nios2eds
                ├── qdb
                ├── qsys
                ├── quartus
                ├── uninstall
    ```

### Installing OPAE

1. Install the pre-requisite packages first:

    ```bash
    $ sudo -E apt-get -f install dkms uuid-dev php7.0-dev php7.0-cli libjson-c-dev libhwloc-dev python-pip libjson-c-dev libhwloc-dev
    $ sudo -E pip install intelhex
    ```
    It may be possible that php7.0-dev and php7.0-cli will not be found. In that case, refer to these [instructions](https://askubuntu.com/questions/741913/php7-0-dev-installation-fails-on-ubuntu-14-0-4)

2. Download the source code from [this link](https://github.com/OPAE/opae-sdk/releases/tag/1.3.0-2). Untar the downloaded package.

3. Build the OPAE C library using these commands:
    
    ```bash
    $ cd opae-sdk-1.3.0-2
    $ mkdir mybuild
    $ cd mybuild
    $ cmake .. -DBUILD_ASE=1 -DCMAKE_INSTALL_PREFIX=/path/to/directory/a10_gx_pac_ias_1_1_pv
    $ make
    $ make install
    ```

4. In the _init_env.sh_ script, set the following environment variables and add them to the PATH:

    ```bash
    export OPAE_PLATFORM_ROOT="/path/to/directory/a10_gx_pac_ias_1_1_pv"
    export DCP_LOC="/path/to/directory/a10_gx_pac_ias_1_1_pv"
    export SHELL="/bin/bash"
    export PATH="${PATH}":"${OPAE_PLATFORM_BIN}"
    export PATH="${PATH}":"${OPAE_PLATFORM_ROOT}"
    export OPAE_BASEDIR="/path/to/directory/opae-sdk-1.3.0-2"
    export INC_PATH="/path/to/directory/a10_gx_pac_ias_1_1_pv/include"
    export LIB_PATH="/path/to/directory/a10_gx_pac_ias_1_1_pv/lib"

    export C_INCLUDE_PATH="${INC_PATH}":"${C_INCLUDE_PATH}" 
    export LIBRARY_PATH="${LIB_PATH}":"${LIBRARY_PATH}"
    export LD_LIBRARY_PATH="${LIB_PATH}":"${LD_LIBRARY_PATH}"
    ```

5. If you want to interface with real FPGA, then FPGA drivers need to be installed additionally. Download _opae-intel-fpga-driver-1.3.0-2.x86_64.deb_ from the [same site](https://github.com/OPAE/opae-sdk/releases/tag/1.3.0-2) and run:

    ```bash
    $ sudo dpkg -i opae-intel-fpga-driver-1.3.0-2.x86_64.deb
    ```

    __Note__: This is incompatible with Linux Kernel versions 4.15+. So you need to downgrade your kernel to v4.15, if you want to install these drivers.

6. Verify that the _inteldevstack_ directory structure looks like this:
    
    ```
    ├── inteldevstack
        ├── a10_gx_pac_ias_1_1_pv
            ├── bin
            ├── hw
            ├── include
            ├── lib
            ├── opencl
            ├── share
            ├── src
            ├── sw
        ├── a10_gx_pac_ias_1_1_pv.tar.gz
        ├── init_env.sh
    ```

### Verifying installation using the Intel FPGA BBB repo:

1. Download the v1.3.0 release of the repo from [this link](https://github.com/OPAE/intel-fpga-bbb/releases/tag/1.3.0-1)

2. Set the following environment variable in your _init_env.sh_ script:
    
    ```bash
    export FPGA_BBB_CCI_SRC="/path/to/directory/intel-fpga-bbb-1.3.0-1"
    ```

3. We will be running the 01_hello_world example of the most basic AFU possible. Open the terminal and do the following:

    ```bash
    $ cd /path/to/directory/intel-fpga-bbb-1.3.0-1/samples/tutorial/01_hello_world/
    $ afu_sim_setup --source hw/rtl/sources.txt build_sim
    $ cd build_sim
    $ sed -i '1i SHELL:=/bin/bash' Makefile
    $ make
    $ make sim
    ```
    __Note__: The command _sed -i '1i SHELL:=/bin/bash' Makefile_ is required for telling the compiler to use the bash shell. An alternative to using this command is making the following change to the Makefile:

    ```bash
    cd $(WORK)/verilog_libs; libs=`echo *_ver`; echo -L $${libs// / -L } > ../quartus_msim_verilog_libs
    ```
    change to:

    ```bash
    cd $(WORK)/verilog_libs; libs=`echo *_ver`; echo -L $$(libs// / -L ) > ../quartus_msim_verilog_libs
    ```

4. You might encounter a message like this when running make sim.

    ```bash
    ** Fatal: ** Error: (vsim-3828) Could not link 'vsim_auto_compile.so'
    ```
    In this case, remove all gcc folders present in _modelsim_ase_ directory.

5. You may get another error, saying that the HOSTNAME is invalid. This will cause the simulator to crash. This happens if the hostname of your machine contains any special character. Change your hostname to a simple (preferably single) word using the instructions from this [link](https://linuxize.com/post/how-to-change-hostname-on-ubuntu-18-04/). For example:

    ```bash
    $ user@foo-bar:
    ```
    will not work, and should be changed to something like:

    ```bash
    $ user@foobar:
    ```

6. If everything works out perfectly, you will see a screen like this:

    ```bash
    #   [SIM]  ** ATTENTION : BEFORE running the software application **
    #   [SIM]  Set env(ASE_WORKDIR) in terminal where application will run (copy-and-paste) =>
    #   [SIM]  $SHELL   | Run:
    #   [SIM]  ---------+---------------------------------------------------
    #   [SIM]  bash/zsh | export ASE_WORKDIR=/path/to/directory/intel-fpga-bbb-1.3.0-1/samples/tutorial/01_hello_world/build_sim/work
    #   [SIM]  tcsh/csh | setenv ASE_WORKDIR /path/to/directory/intel-fpga-bbb-1.3.0-1/samples/tutorial/01_hello_world/build_sim/work
    #   [SIM]  For any other $SHELL, consult your Linux administrator
    #   [SIM]  
    #   [SIM]  Ready for simulation...
    #   [SIM]  Press CTRL-C to close simulator...
    ```

7. Open another terminal and export the ASE_WORKDIR environment variable as shown by the simulator, and run the following commands to build and run the software:

    ```bash
    # Set ASE_WORKDIR as directed by the simulator
    $ cd sw
    $ make
    $ ./cci_hello_ase
    ```

    __Note__: You may get an error that some libraries are missing. In that case, check if the LIBRARY_PATH and LD_LIBRARY_PATH environment variables are set properly.

### References:

1. https://github.com/OPAE/intel-fpga-bbb/tree/release/1.3.0/samples
2. https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug-qs-ias-v1-2-1.pdf
3. https://opae.github.io/1.3.0/docs/install_guide/installation_guide.html#opae-sdk-build-installation-from-opae-sdk-source
4. https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/manual/quartus_install.pdf

## Using the FPGA on the server

- create a copy of the `shir_wrapper` folder (in this repo under `host/shir_wrapper` or from `/opt/shir_wrapper` on the server) for your project. This should contain:
    - `hw/shir_afu.json` (the afu config file)
    - `hw/rtl/shir_acc.vhd`
    - `hw/rtl/afu.sv`
    - `hw/rtl/ccip_interface_reg.sv`
    - `hw/rtl/ccip_std_afu.sv`
- copy all the generated HDL files and memory images (.dat) into the `hw/rtl/generated` subdirectory.
- run the `real.sh` script, which will prepare and start the synthesis (this may take a while!)
- install the bitstream on the FPGA using the `real_start.sh` script
- compile and run the software part to trigger the FPGA using the `read_sw.sh` script
