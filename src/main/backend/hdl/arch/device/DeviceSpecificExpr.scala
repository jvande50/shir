package backend.hdl.arch.device

import backend.hdl.arch.ArchIR
import backend.hdl._
import core._
import lift.arithmetic.simplifier.SimplifyIfThenElse

sealed trait DeviceSpecificExpr extends BuiltinExpr with ArchIR

case class Mul2AddIntExpr private[arch](innerIR: Expr) extends DeviceSpecificExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): Mul2AddIntExpr = Mul2AddIntExpr(newInnerIR)
}
object Mul2AddInt extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): Mul2AddIntExpr = Mul2AddIntExpr({
    val tttv = TupleTypeVar(TupleTypeVar(ScalarTypeVar(), ScalarTypeVar()), TupleTypeVar(ScalarTypeVar(), ScalarTypeVar()))
    val outit =
      ConditionalTypeVar(
        tttv,
        { case TupleType(Seq(TupleType(Seq(i1tv: IntTypeT, i2tv: IntTypeT)), TupleType(Seq(i3tv: IntTypeT, i4tv: IntTypeT)))) =>
          if (i1tv.kind != i2tv.kind || i1tv.kind != i3tv.kind || i1tv.kind != i4tv.kind)
            throw MalformedExprException(this.getClass().getName() + " does not support mixing signed and unsigned ints")

          val mul1Width = i1tv.bitWidth.ae + i2tv.bitWidth.ae
          val mul2Width = i3tv.bitWidth.ae + i4tv.bitWidth.ae
          val addWidth =
            SimplifyIfThenElse(
              mul1Width.gt(mul2Width),
              mul1Width,
              SimplifyIfThenElse(
                mul1Width.eq(mul2Width),
                mul1Width + 1,
                mul2Width
              )
            )
          if (i1tv.kind == SignedIntTypeKind) SignedIntType(addWidth)
          else IntType(addWidth)
        }
      )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(tttv, HWFunType(tttv, outit))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): Mul2AddIntExpr = apply(inputs.head)
  def unapply(expr: Mul2AddIntExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}
