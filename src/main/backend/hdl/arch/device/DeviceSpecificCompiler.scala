package backend.hdl.arch.device

import backend.hdl.arch.mem.MemFunctionsCompiler
import backend.hdl.arch.rewrite.ExtractMul2AddRules
import core._
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph

object DeviceSpecificCompiler extends CompilerPass {

  override def phaseBefore: CompilerPhase = MemFunctionsCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val exprBefore = TypeChecker.checkAssert(expr)
    val exprAfter = RewriteStep(RewriteAll(), ExtractMul2AddRules.get()).apply(exprBefore)
    TypeChecker.checkAssert(exprAfter)
  }
}
