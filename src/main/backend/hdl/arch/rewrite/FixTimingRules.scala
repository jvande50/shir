package backend.hdl.arch.rewrite

import backend.hdl.VectorTypeT
import backend.hdl.arch.{AddInt, ArchLambda, JoinUnorderedStream, MapVector, MulInt, Registered, SplitVector, Tuple2}
import core.{Expr, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object FixTimingRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      pipelineReduceVector,
      balanceTuplePipelineReduceVector,
      //      bufferJoinUnorderedStream, // use rewrites instead to get rid of JoinUnorderedStream
      //      bufferMul, // is now obsolete (the registers are now fixed in the template)
    )

  def bufferJoinUnorderedStream: Rule = Rule("bufferJoinUnorderedStream", {
    case JoinUnorderedStream(input, _)
      if (input match {
        case Registered(_, _) => false
        case _ => true
      }) => JoinUnorderedStream(Registered(input))
  })

  def pipelineReduceVector: Rule = Rule("pipelineReduceVector", {
    case MapVector(ArchLambda(p, AddInt(addIn, _), _), SplitVector(splitIn, chunkSize, _), _) =>
      MapVector(ArchLambda(p, Registered(AddInt(addIn))), SplitVector(splitIn, chunkSize))
//    case SplitVector(MapVector(ArchLambda(p, AddInt(addIn, _), _), mapIn, _), chunkSize, _) =>
//      SplitVector(MapVector(ArchLambda(p, Registered(AddInt(addIn))), mapIn), chunkSize)
  })

  def balanceTuplePipelineReduceVector: Rule = Rule("balancePipelineReduceVector", {
    case MapVector(ArchLambda(p1, fun, _), input, _) if {
      var expr0Found = false
      fun.visit{
        case Tuple2(pow2, rest, _) if  {
          var expr1Found = false
          pow2.visit{
            case ParamUse(p2) if p1.id == p2.id =>
              expr1Found = true
            case _ =>
          }
          expr1Found
        } && {
          var expr1Found = false
          rest.visit{
            case ParamUse(p2) if p1.id == p2.id =>
              expr1Found = true
            case _ =>
          }
          expr1Found
        } && {
          var expr1Found = false
          val inputtc = TypeChecker.checkIfUnknown(input, input.t)
          if(inputtc.t.asInstanceOf[VectorTypeT].len.ae.evalInt == 1 &&
            inputtc.t.asInstanceOf[VectorTypeT].et.isInstanceOf[VectorTypeT]) {
            val len = inputtc.t.asInstanceOf[VectorTypeT].et.asInstanceOf[VectorTypeT].len
            val pow2Pow = math.floor(math.log(len.ae.evalInt)/math.log(2)).toInt
            val pow2Len = math.pow(2, pow2Pow).toInt
            val restLen = len.ae.evalInt - pow2Len
            val restLenPow = math.floor(math.log(restLen)/math.log(2)).toInt
            val regCountLeft = if(pow2Pow > 2) pow2Pow - 2 else 0
            val regCountRight = if(restLenPow > 2) restLenPow - 2 else 0
            if (regCountLeft > regCountRight)
              expr1Found = true
          }
          expr1Found
        } && {
          var expr1Found = false
          rest.visit{
            case Registered(nonRegIn, _) if {
              var expr2Found = false
              nonRegIn.visit{
                case ParamUse(p2) if p1.id == p2.id =>
                  expr2Found = true
                case _ =>
              }
              expr2Found
            } =>
              expr1Found = true
            case _ =>
          }
          !expr1Found
        } =>
          expr0Found = true
        case _ =>
      }
      expr0Found
    } =>
      val inputtc = TypeChecker.checkIfUnknown(input, input.t)
      val len = inputtc.t.asInstanceOf[VectorTypeT].et.asInstanceOf[VectorTypeT].len
      val pow2Pow = math.floor(math.log(len.ae.evalInt)/math.log(2)).toInt
      val pow2Len = math.pow(2, pow2Pow).toInt
      val restLen = len.ae.evalInt - pow2Len
      val restLenPow = math.floor(math.log(restLen)/math.log(2)).toInt
      val regCountLeft = if(pow2Pow > 2) pow2Pow - 2 else 0
      val regCountRight = if(restLenPow > 2) restLenPow - 2 else 0
      val diff = regCountLeft - regCountRight

      def nestedReg(input: Expr, idx: Int): Expr = {
        if(idx >= diff)
          input
        else
          nestedReg(Registered(input), idx + 1)
      }

      MapVector(ArchLambda(p1, fun.visitAndRebuild{
        case AddInt(Tuple2(pow2, rest, _), _) if {
          var exprFound = false
          rest.visit{
            case ParamUse(p2) if p1.id == p2.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          AddInt(Tuple2(pow2, nestedReg(rest, 0)))
        case e => e
      }.asInstanceOf[Expr]), input)
  })
}
