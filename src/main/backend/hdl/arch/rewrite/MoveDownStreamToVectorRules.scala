package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.{ArchLambda, DropOrderedStream, DropVector, JoinOrderedStream, JoinVector, MapOrderedStream, OrderedStreamToVector, Select2, Tuple2, Zip2OrderedStream, ZipVector}
import core.{Conversion, Expr, LambdaT, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object MoveDownStreamToVectorRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFission,
//      moveDownMapStreamToVector2,
//      moveDownMapStreamToVector3
    )

  def skipTupleStructure: Seq[Rule] =
    Seq(
      mapFusion,
      skipZipStream2DTuple,
      mapFission2,
      skipSelectTuple,
      mapFusionMapParam,
    ) ++ all(None)

  def mapFission: Rule = Rule("moveDownMapStreamToVector_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case OrderedStreamToVector(ParamUse(p2), _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case OrderedStreamToVector(ParamUse(p2), _) if p1.id == p2.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case OrderedStreamToVector(ParamUse(p2), _) if p1.id == p2.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(OrderedStreamToVector.asFunction(), input)
      )
  })

  def mapFission2: Rule = Rule("moveDownMapStreamToVector_mapFission2", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(2, OrderedStreamToVector.asFunction(), input)
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
        OrderedStreamToVector(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _) if
          p1.id == p6.id && p2.id == p5.id && p3.id == p4.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
        OrderedStreamToVector(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _) if
          p1.id == p6.id && p2.id == p5.id && p3.id == p4.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
              OrderedStreamToVector(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _) if
                p1.id == p6.id && p2.id == p5.id && p3.id == p4.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(3, OrderedStreamToVector.asFunction(), input)
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4,
        OrderedStreamToVector(ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _)
          if p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4,
        OrderedStreamToVector(ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _)
          if p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4,
              OrderedStreamToVector(ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _)
                if p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(4, OrderedStreamToVector.asFunction(), input)
      )
  })

//  def moveDownMapStreamToVector2: Rule = Rule("moveDownMapStreamToVector2", {
//    case MapOrderedStream(fun @ ArchLambda(p1, OrderedStreamToVector(ParamUse(p2, _), _), _), Marker(input, text), _) if p1.id == p2.id =>
//      Marker(MapOrderedStream(fun, input), text)
//    case OrderedStreamToVector(Conversion(input, _), t) =>
//      ???
//      Conversion(SplitOrderedStream(input, chunkSize), t)
//  })

//  def moveDownMapStreamToVector3: Rule = Rule("moveDownMapStreamToVector3", {
//    // map fusion
//    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2, _), _), _), MapOrderedStream(ArchLambda(param, body, _), input, _), _) if p1.id == p2.id =>
//      MapOrderedStream(
//        {
//          ArchLambda(
//            param,
//            OrderedStreamToVector(body)
//          )
//        },
//        input
//      )
//  })

  def skipDropStream: Rule = Rule("moveDownMapStreamToVector_skipDropStream", {
    case OrderedStreamToVector(DropOrderedStream(input, firstElements, lastElements, _), _) =>
      DropVector(OrderedStreamToVector(input), firstElements, lastElements)
  })

  def mapFusion: Rule = Rule("moveDownMapStreamToVector_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case OrderedStreamToVector(ParamUse(p3), _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val stm2vecInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        stm2vecInnerMost.body match {
          case OrderedStreamToVector(ParamUse(p3), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFusionMapParam: Rule = Rule("moveDownMapStreamToVector_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val stm2vecInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        stm2vecInnerMost.body match {
          case OrderedStreamToVector(ParamUse(p3), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val stm2vecInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        stm2vecInnerMost.body match {
          case OrderedStreamToVector(ParamUse(p3), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def skipZipStream2DTuple: Rule = Rule("moveDownMapStreamToVector_skipZipStream2DTuple", {
    case MapOrderedStream(ArchLambda(p1, body, _),
    Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      body.visit{
        case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p2), _), _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = Zip2OrderedStream(Tuple2(
        MapOrderedStream(OrderedStreamToVector.asFunction(), input1),
        MapOrderedStream(OrderedStreamToVector.asFunction(), input2)
      ))
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p2), _), _) if p1.id == p2.id =>
                ZipVector(ParamUse(param))
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  def skipSelectTuple: Rule = Rule("moveDownMapStreamToVector_skipSelectTuple", {
    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(MapOrderedStream(3, OrderedStreamToVector.asFunction(), input1), input2)
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 0 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 0 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )

    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 1 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(input1, MapOrderedStream(3, OrderedStreamToVector.asFunction(), input2))
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 1 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 1 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )
  })

  def skipJoinStream: Rule = Rule("moveDownMapStreamToVector_skipJoinStream", {
    case OrderedStreamToVector(JoinOrderedStream(input, _), _) =>
      JoinVector(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(), input)))
    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _),
    JoinOrderedStream(input, _), _) if p1.id == p2.id =>
      JoinOrderedStream(MapOrderedStream(2, OrderedStreamToVector.asFunction(), input))
  })
}
