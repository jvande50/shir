package backend.hdl.arch.rewrite

import backend.hdl.{IntTypeT, OrderedStreamTypeT}
import backend.hdl.arch.{AddInt, AddInt2, ArchLambda, ResizeInteger, FoldOrderedStream, FoldVector, MapOrderedStream, MapVector, MulInt, OrderedStreamToVector, SplitOrderedStream, Tuple2}
import core.{ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object ParallelizeDotProductRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(parallelizeDotProduct(config.getOrElse(2))) ++
    Seq(CleanupRules.joinWrites) ++
    RemoveStreamVectorConversionRules.get() ++
    MoveUpVectorToStreamRules.get() ++
    MoveUpJoinStreamRules.all(None) ++
    Seq(MoveUpJoinStreamRules.moveIntoFold) ++
    //      MoveDownStreamToVectorRules.get ++ // try to avoid
    MoveDownSplitStreamRules.get() // try to keep set minimal (use MoveUpJoinStream more)

  def parallelizeDotProduct(parMuls: Int = 2): Rule = Rule("parallelizeDotProduct", {
    case FoldOrderedStream(
      ArchLambda(d0, ArchLambda(d1, AddInt(Tuple2(ParamUse(u0), ParamUse(u1), _), _), _), _),
      MapOrderedStream(mulfunc @ ArchLambda(d3, MulInt(ParamUse(u3), _), _), input, mapt: OrderedStreamTypeT), resTy)
    if (
      ((d0.id == u0.id && d1.id == u1.id) || (d0.id == u1.id && d1.id == u0.id))
      && d3.id == u3.id
      && parMuls > 1 && mapt.len.ae.evalInt > 1 && mapt.len.ae.evalInt % parMuls == 0
    ) =>
      val newInput = TypeChecker.check(SplitOrderedStream(input, parMuls))
      val reduction = TypeChecker.check(FoldOrderedStream(
        // do not reuse the add function here, because input precision will be different
        AddInt2.asFunction(),
        MapOrderedStream(
          {
            val p = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              p,
              FoldVector(
                AddInt2.asTypeFunction(),
                MapVector(
                  mulfunc,
                  OrderedStreamToVector(ParamUse(p))
                )
              )
            )
          },
          newInput
        )
      ))

      // Consider this: parallelize input of Seq[Tuple[Int(8);Int(9)]; 25] by 5.
      // the original reduction gives Int(8+9 + ceil(log2(25))) = Int(22)
      // the parallel reduction gives Int(8+9 + ceil(log2(5)) + ceil(log2(5))) = Int(23)
      //
      // in a case like this one, resize the output to match the original.
      (resTy, reduction.t) match {
        case (a: IntTypeT, b: IntTypeT) if a.bitWidth.ae.evalInt != b.bitWidth.ae.evalInt =>
          ResizeInteger(reduction, a.bitWidth.ae.evalInt)
        case _ => reduction
      }
  })

}
