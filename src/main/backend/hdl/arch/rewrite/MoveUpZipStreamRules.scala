package backend.hdl.arch.rewrite

import backend.hdl.arch.{ArchLambda, MapOrderedStream, ZipOrderedStream}
import core.{Expr, LambdaT, ParamDef, ParamUse}
import core.rewrite.{Rule, RulesT}

object MoveUpZipStreamRules extends RulesT {
  override def all(config: Option[Int]): Seq[Rule] = Seq(
    mapFusion,
    mapFusionMapParam,
    mapFission
  )

  private def mapFusion: Rule = Rule("MoveUpZipStreamRules_mapFusion", {
    // map fusion
    case MapOrderedStream(ArchLambda(pd, body, _), MapOrderedStream(ArchLambda(p1, ZipOrderedStream(ParamUse(p2), n, _), _), input, _), _)
      if p1.id == p2.id &&
        (body match {
          case ZipOrderedStream(ParamUse(p3), _, _) if pd.id == p3.id => false
          case _ => true
        }) && {
        val isZipOnly: Boolean = body match {
          case ZipOrderedStream(ParamUse(_), _, _) => true
          case MapOrderedStream(innerZipFun: LambdaT, _,  _) if {
            var exprFound = false
            val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(innerZipFun)
            innerMostFun.body match {
              case ZipOrderedStream(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
              case _ =>
            }
            exprFound
          } => true
          case _ => false
        }
        !isZipOnly
      } =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id => ZipOrderedStream(ParamUse(newParam), n)
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
  })

  def mapFusionMapParam: Rule = Rule("moveZipStream_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val zipInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        zipInnerMost.body match {
          case ZipOrderedStream(ParamUse(p3), _, _) if p3.id == zipInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit{
          case ParamUse(p4) if p1.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val zipInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        zipInnerMost.body match {
          case ZipOrderedStream(ParamUse(p3), _, _) if p3.id == zipInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  private def mapFission: Rule = Rule("moveUpZipStream_mapFission", {
    // map fission
    case MapOrderedStream(ArchLambda(origParam, ZipOrderedStream(fun, n, _), _), input, _)
      // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
      if !RulesHelper.isParamUse(fun) =>
      MapOrderedStream(
        ZipOrderedStream.asFunction(Seq(None), Seq(n)),
        MapOrderedStream(ArchLambda(origParam, fun), input)
      )

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(zipStmFun: LambdaT, inputInner, _), _), inputOuter, _)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(zipStmFun)
        innerMostBody.body match {
          case ZipOrderedStream(ParamUse(p2), _, _) if p2.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p2) if p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              zipStmFun,
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
  })
}
