package backend.hdl.arch.rewrite.conv

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.{JoinStreamAll, JoinVector, OrderedStreamToVector, SlideGeneralOrderedStream, SlideVector, SplitStreamAll, VectorToOrderedStream}
import core.TypeChecker
import core.rewrite.{Rule, RulesT}

object SlideGeneralConversionRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    convertSlideGeneralIntoSlideVector
  )

  /** Convert SlideGeneralOrderedStream into SlideVector because there is no hardware template for SlideGeneralOrdered-
    * Stream.
    */
  def convertSlideGeneralIntoSlideVector: Rule = Rule("convertSlideGeneralIntoSlideVector", {
    case SlideGeneralOrderedStream(input, windowSize, stepSize, t:OrderedStreamTypeT) =>
      val outputDims = t.dimensions
      // New Slide window size and step size must be multiplied with total size of inner dimensions
      // because we first "flatten the stream", convert it to vector, and then do sliding.
      val innerDimSize = outputDims.dropRight(2).map(_.ae.evalInt).foldLeft(1)(_ * _)
      val joinedInput = TypeChecker.check(OrderedStreamToVector(JoinStreamAll(input)))

      val slidData = TypeChecker.check(VectorToOrderedStream(JoinVector(SlideVector(joinedInput,
        windowSize.ae.evalInt * innerDimSize, stepSize.ae.evalInt * innerDimSize))))
      SplitStreamAll(slidData, outputDims.dropRight(1).reverse)
  })
}
