package backend.hdl.arch.rewrite.conv

import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch._
import backend.hdl.{OrderedStreamTypeT, VectorTypeT}
import core.rewrite.{Rule, RulesT}
import core._
import lift.arithmetic.{Cst, IntDiv, Mod, Prod, Sum}

object MoveDownSlideVectorRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      skipSplitVec,
      skipDropVector,
      skipJoinVector,
      skipStreamToVector,
      skipVectorToStream,
      skipTransposeVec,
      mapVecFusion,
      mapVecFission,
      mapFissionMapVecSlide,
      mapFissionMapNDMapVecSlide,
      mapFusionMapNDMapVecSlide,
      skipSlideStream,
      mapFissionMapSlide,
      mapFusionMapNDSlide,
      mapFusion,
      mapFission,
    )

  def mapVecFusion: Rule = Rule("moveDownSlideVector_mapVecFusion", {
    case MapVector(ArchLambda(p1, body1, _), MapVector(ArchLambda(p2, body2, _),
    input, _), _)
      if {
        var exprFound = false
        body1.visit {
          case SlideVector(ParamUse(p3), windowSize, stepSize, _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit {
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapVector({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFusion: Rule = Rule("moveDownSlideVector_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _), MapOrderedStream(ArchLambda(p2, body2, _),
    input, _), _)
      if {
        var exprFound = false
        body1.visit {
          case SlideVector(ParamUse(p3), windowSize, stepSize, _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit {
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def skipSplitVec: Rule = Rule("moveDownSlideVector_skipSplitVec", {
    case SlideVector(SplitVector(input, chunkSize, _), windowSize, stepSize, _) if {
      var exprFound = false
      input match {
        case PermuteVector(_, _, _) =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      val newWindowSize = windowSize.ae.evalInt * chunkSize.ae.evalInt
      val newStepSize = stepSize.ae.evalInt * chunkSize.ae.evalInt
      val slidedInput = SlideVector(input, newWindowSize, newStepSize)
      MapVector(SplitVector.asFunction(Seq(None), Seq(chunkSize)), slidedInput)
  })

  def mapVecFission: Rule = Rule("moveDownSlideVector_mapVecFission", {
    case MapVector(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case SlideVector(ParamUse(p2), windowSize, stepSize, _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isSlideVecOnly: Boolean = body match {
        case SlideVector(ParamUse(p2), windowSize, stepSize, _) if p1.id == p2.id => true
        case _ => false
      }
      !isSlideVecOnly && exprFound && paramUses == 1
    } =>
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      MapVector(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case SlideVector(ParamUse(p2), ws, ss, _) if p1.id == p2.id =>
                windowSize = ws
                stepSize = ss
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapVector(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), input)
      )
  })

  def mapFission: Rule = Rule("moveDownSlideVector_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case SlideVector(ParamUse(p2), windowSize, stepSize, _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isSlideVecOnly: Boolean = body match {
        case SlideVector(ParamUse(p2), windowSize, stepSize, _) if p1.id == p2.id => true
        case _ => false
      }
      !isSlideVecOnly && exprFound && paramUses == 1
    } =>
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case SlideVector(ParamUse(p2), ws, ss, _) if p1.id == p2.id =>
                windowSize = ws
                stepSize = ss
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), input)
      )
  })

  def mapFissionMapVecSlide: Rule = Rule("moveDownSlideVector_mapFissionMapVecSlide", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapVector(ArchLambda(p2, SlideVector(ParamUse(p3), windowSize, stepSize, _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapSlideVecOnly: Boolean = body match {
        case MapVector(ArchLambda(p2, SlideVector(ParamUse(p3), windowSize, stepSize, _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id => true
        case _ => false
      }
      !isMapSlideVecOnly && exprFound && paramUses == 1
    } =>
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapVector(ArchLambda(p2, SlideVector(ParamUse(p3), ws, ss, _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
                windowSize = ws
                stepSize = ss
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(
          {
            val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param,
              MapVector(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), ParamUse(param))
            )
          }, input
        )
      )
  })

  def mapFissionMapNDMapVecSlide: Rule = Rule("moveDownSlideVector_mapFissionMapNDMapVecSlide", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
          p1.id == p2.id && {
            val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
            var innerExprFound = false
            innerMostBody.body match {
              case MapVector(ArchLambda(p3, SlideVector(ParamUse(p4), windowSize, stepSize, _), _), ParamUse(p5), _) if p3.id == p4.id && p5.id == innerMostBody.param.id =>
                innerExprFound = true
              case _ =>
            }
            innerExprFound
          } =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapNDMapSlideVecOnly: Boolean = body match {
        case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
          p1.id == p2.id && {
            val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
            var innerExprFound = false
            innerMostBody.body match {
              case MapVector(ArchLambda(p3, SlideVector(ParamUse(p4), windowSize, stepSize, _), _), ParamUse(p5), _) if p3.id == p4.id && p5.id == innerMostBody.param.id =>
                innerExprFound = true
              case _ =>
            }
            innerExprFound
          } => true
        case _ => false
      }
      !isMapNDMapSlideVecOnly && exprFound && paramUses == 1
    } =>
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      var innerLevels: Int = 0
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
                p1.id == p2.id && {
                  val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
                  var innerExprFound = false
                  innerLevels = RulesHelper.getMapNDLevels(innerBody)
                  innerMostBody.body match {
                    case MapVector(ArchLambda(p3, SlideVector(ParamUse(p4), ws, ss, _), _), ParamUse(p5), _) if p3.id == p4.id && p5.id == innerMostBody.param.id =>
                      windowSize = ws
                      stepSize = ss
                      innerExprFound = true
                    case _ =>
                  }
                  innerExprFound
                } =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(
          innerLevels + 1,
          {
            val param = ParamDef(RulesHelper.getInnerMostStreamType(input.t).asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param,
              MapVector(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), ParamUse(param))
            )
          }, input
        )
      )
  })

  def mapFusionMapNDSlide: Rule = Rule("moveDownSlideVector_mapFusionMapNDSlide", {
    case MapOrderedStream(ArchLambda(p1, body1, _), MapOrderedStream(ArchLambda(p2, body2, _),
    input, _), _)
      if {
        var exprFound = false
        body1.visit {
          case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
            p1.id == p2.id && {
              val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
              var innerExprFound = false
              innerMostBody.body match {
                case SlideVector(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
                  innerExprFound = true
                case _ =>
              }
              innerExprFound
            } =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit {
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFusionMapNDMapVecSlide: Rule = Rule("moveDownSlideVector_mapFusionMapNDMapVecSlide", {
    case MapOrderedStream(ArchLambda(p1, body1, _), MapOrderedStream(ArchLambda(p2, body2, _),
    input, _), _)
      if {
        var exprFound = false
        body1.visit {
          case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
            p1.id == p2.id && {
              val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
              var innerExprFound = false
              innerMostBody.body match {
                case MapVector(ArchLambda(p3, SlideVector(ParamUse(p4), windowSize, stepSize, _), _), ParamUse(p5), _) if p3.id == p4.id && p5.id == innerMostBody.param.id =>
                  innerExprFound = true
                case _ =>
              }
              innerExprFound
            } =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit {
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFissionMapSlide: Rule = Rule("moveDownSlideVector_mapFissionMapSlide", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(ArchLambda(p2, SlideVector(ParamUse(p3), windowSize, stepSize, _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapSlideVecOnly: Boolean = body match {
        case MapOrderedStream(ArchLambda(p2, SlideVector(ParamUse(p3), windowSize, stepSize, _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id => true
        case _ => false
      }
      !isMapSlideVecOnly && exprFound && paramUses == 1
    } =>
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapOrderedStream(ArchLambda(p2, SlideVector(ParamUse(p3), ws, ss, _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
                windowSize = ws
                stepSize = ss
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(
          {
            val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param,
              MapOrderedStream(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), ParamUse(param))
            )
          }, input
        )
      )
  })

  def skipSlideStream: Rule = Rule("moveDownSlideVector_skipSlideStream", {
    case MapOrderedStream(ArchLambda(p1, MapVector(ArchLambda(p2, SlideVector(ParamUse(p3), vecWindowSize, vecStepSize, _), _), ParamUse(p4), _), _),
    SlideOrderedStream(input, stmWindowSize, _), _) if p1.id == p4.id && p2.id == p3.id =>
      SlideOrderedStream(MapOrderedStream(SlideVector.asFunction(Seq(None), Seq(vecWindowSize, vecStepSize)), input), stmWindowSize)
  })

  def skipDropVector: Rule = Rule("moveDownSlideVector_skipDropVector", {
    case SlideVector(DropVector(input, firstElements, lastElements, _), windowSize, stepSize, _) =>
      val dropMargin = firstElements.ae.evalInt + lastElements.ae.evalInt
      val newWindowSize = windowSize.ae.evalInt + dropMargin
      val slidedInput = SlideVector(input, newWindowSize, stepSize)
      MapVector(DropVector.asFunction(Seq(None), Seq(firstElements, lastElements)), slidedInput)
  })

  def skipJoinVector: Rule = Rule("moveDownSlideVector_skipJoinVector", {
    case SlideVector(JoinVector(input, _), windowSize, stepSize, _) if {
      val innerLen = input.t.asInstanceOf[VectorTypeT].et.asInstanceOf[VectorTypeT].len
      windowSize.ae.evalInt % innerLen.ae.evalInt == 0 && stepSize.ae.evalInt % innerLen.ae.evalInt == 0
    } =>
      val innerLen = input.t.asInstanceOf[VectorTypeT].et.asInstanceOf[VectorTypeT].len
      val newWindowSize = windowSize.ae.evalInt / innerLen.ae.evalInt
      val newStepSize = stepSize.ae.evalInt / innerLen.ae.evalInt
      val slidedInput = SlideVector(input, newWindowSize, newStepSize)
      MapVector(JoinVector.asFunction(), slidedInput)
  })

  def skipStreamToVector: Rule = Rule("moveDownSlideVector_skipStreamToVector", {
    case SlideVector(OrderedStreamToVector(input, _), windowSize, stepSize, _) =>
      val slidedInput = SlideGeneralOrderedStream(input, windowSize, stepSize)
      OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(), slidedInput))

    case MapVector(ArchLambda(p1, SlideVector(ParamUse(p2), windowSize, stepSize, _), _), OrderedStreamToVector(input, _), _) if p1.id == p2.id =>
      OrderedStreamToVector(MapOrderedStream(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), input))
  })

  def skipVectorToStream: Rule = Rule("moveDownSlideVector_skipVectorToStream", {
    case MapOrderedStream(ArchLambda(p1, SlideVector(ParamUse(p2), windowSize, stepSize, _), _), VectorToOrderedStream(input, _), _) if p1.id == p2.id =>
      VectorToOrderedStream(MapVector(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), input))
  })

  def skipTransposeVec: Rule = Rule("moveDownSlideVector_skipTransposeVec", {
    case SlideVector(SplitVector(PermuteVector(JoinVector(input, _), permuteFun, _), chunkSize, _), windowSize, stepSize, _) if {
      var exprFound = false
      permuteFun.b.ae match {
        case Sum(IntDiv(v1, c1) :: Prod(Cst(c3) :: Mod(v2, c2) :: Nil) :: Nil) if v1 == v2 && c1 == c2 && c3.toInt == chunkSize.ae.evalInt =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val slidedVecInput = MapVector(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), input)
      val slidedVecInputtc = TypeChecker.check(slidedVecInput)
      val joinIntermediate = MapVector(JoinVector.asFunction(), slidedVecInputtc)
      SplitVector(Transpose(joinIntermediate), windowSize)
  })
}
