package backend.hdl.arch.rewrite.conv

import backend.hdl.{IntType, IntTypeT, OrderedStreamTypeT}
import backend.hdl.arch.mem.{Read, ReadOutOfBound}
import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, CounterInteger, MapOrderedStream, PadOrderedStream, SlideGeneralOrderedStream}
import core.{ArithTypeT, Expr, LambdaT, Let, Marker, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object MoveDownPadRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    skipMarker,
    mapFission,
    skipSlide,
    mapNDFusion,
    skipLet,
  )


  def skipMap: Rule = Rule("MoveDownPad_skipMap", {
    case PadOrderedStream(MapOrderedStream(fun: LambdaT, input, _), firstElements, lastElements, value, _) if {
      var exprFound = false
      // Moving Pad across Read requires extra address transformations.
      // Handle this in another rule.
      fun.body visit {
        case Read(_, _, _) => exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      MapOrderedStream(fun, PadOrderedStream(input, firstElements, lastElements, value))
  })

  def skipMarker: Rule = Rule("MoveDownPad_skipMarker", {
    case PadOrderedStream(Marker(input, ts), firstElements, lastElements, value, _) =>
      Marker(PadOrderedStream(input, firstElements, lastElements, value), ts)

    case MapOrderedStream(fun: LambdaT, Marker(input, ts), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val levels = RulesHelper.getMapNDLevels(fun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      Marker(MapOrderedStream(levels, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), input), ts)
  })

  def mapFission: Rule = Rule("MoveDownPad_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit {
        case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body match {
        case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var newInput: Expr = null
      body.visit {
        case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>

          val param = ParamDef()
          newInput = MapOrderedStream(ArchLambda(param, PadOrderedStream(ParamUse(param), firstElements, lastElements, value)), input)
        case _ =>
      }
      MapOrderedStream(
        {
          val param1 = ParamDef()
          ArchLambda(
            param1,
            body.visitAndRebuild {
              case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>
                ParamUse(param1)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p3), firstElements, lastElements, value, _) if p3.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p3), firstElements, lastElements, value, _) if p3.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      var level: Int = 0
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p3), fes, les, v, _) if p3.id == innerMostFun.param.id =>
              level = RulesHelper.getMapNDLevels(fun)
              firstElements = fes
              lastElements = les
              value = v
              exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
        case _ =>
      }

      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
                var exprFound1 = false
                val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
                innerMostFun.body match {
                  case PadOrderedStream(ParamUse(p3), firstElements, lastElements, value, _) if p3.id == innerMostFun.param.id =>
                    level = RulesHelper.getMapNDLevels(fun)
                    exprFound1 = true
                  case _ =>
                }
                exprFound1
              } =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(level + 1, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), input)
      )
  })

  def skipSlide: Rule = Rule("MoveDownPad_skipSlide", {
    case MapOrderedStream(fun: LambdaT, SlideGeneralOrderedStream(input, windowSize, stepSize, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound && RulesHelper.getMapNDLevels(fun) >= 2 // Avoid accessing the dimension controlled by slide.
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val levels = RulesHelper.getMapNDLevels(fun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      SlideGeneralOrderedStream(TypeChecker.check(MapOrderedStream(levels - 1, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), input)), windowSize, stepSize)
  })

  private def mapNDFusion: Rule = Rule("MoveDownPad_mapNDFusion", {
    // map ND fusion
    case MapOrderedStream(repFun: LambdaT, MapOrderedStream(ArchLambda(param, body, _), input, _), _) if { //ArchLambda(p1, Repeat(ParamUse(p2), repetitions, _), _)
      // Avoid looping with multiple-repeat-removal guard in moveDownMapRepeatParamUse
      val isMapRepeatOnly: Boolean = body match {
        case PadOrderedStream(ParamUse(_), _, _, _, _) => true
        case MapOrderedStream(innerRepFun: LambdaT, _, _) if {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(innerRepFun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p), _, _, _, _) if p.id == innerMostFun.param.id => exprFound = true
            case _ =>
          }
          exprFound
        } => true
        case _ => false
      }
      !isMapRepeatOnly
    } && {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val funLevels = RulesHelper.getMapNDLevels(repFun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      MapOrderedStream(
        {
          ArchLambda(
            param,
            if(funLevels == 1)
              PadOrderedStream(body, firstElements, lastElements, value)
            else
              MapOrderedStream(funLevels - 1, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), body)
          )
        },
        input
      )
  })

  def skipLet: Rule = Rule("MoveDownPad_skipLet", {
    case PadOrderedStream(Let(p, body, arg, _), firstElements, lastElements, value, _) =>
      Let(p, PadOrderedStream(body, firstElements, lastElements, value), arg)

    case MapOrderedStream(fun: LambdaT, Let(p, body, arg, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val levels = RulesHelper.getMapNDLevels(fun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      Let(p, MapOrderedStream(levels, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), body), arg)
  })
}

