package backend.hdl.arch.rewrite.conv

import backend.hdl.{IntType, IntTypeT, OrderedStreamTypeT, ScalarTypeT, VectorTypeT}
import backend.hdl.arch.{ConcatOrderedStream, ConstantValue, PadOrderedStream, RepeatAll, VectorGeneratorAll}
import core.TypeChecker
import core.rewrite.{Rule, RulesT}

object PadConversionRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    convertPadIntoConcat
  )

  def convertPadIntoConcat: Rule = Rule("PadConversion_convertPadIntoConcat", {
    case PadOrderedStream(input, firstElements, lastElements, value, t:OrderedStreamTypeT) =>
      val leafType = t.leafType
      val padData = leafType match {
        case innerType: ScalarTypeT => ConstantValue(value, Some(innerType))
        case innerType: VectorTypeT =>
          val vecDims = innerType.dimensions
          val innerMostType = innerType.leafType
          VectorGeneratorAll(ConstantValue(value, Some(innerMostType)), vecDims.reverse)
        case _ => ???
      }
      val dims = t.dimensions
      val padTop = TypeChecker.check(RepeatAll(padData, (dims.dropRight(1) :+ firstElements).reverse))
      val padBot = TypeChecker.check(RepeatAll(padData, (dims.dropRight(1) :+ lastElements).reverse))
      (firstElements.ae.evalInt, lastElements.ae.evalInt) match {
        case (e1, e2) if e1 < 0 || e2 < 0 => ???
        case (0, 0) => input
        case (0, _) => ConcatOrderedStream(input, padBot)
        case (_, 0) => ConcatOrderedStream(padTop, input)
        case _ => ConcatOrderedStream(TypeChecker.check(ConcatOrderedStream(padTop, input)), padBot)
      }
  })
}