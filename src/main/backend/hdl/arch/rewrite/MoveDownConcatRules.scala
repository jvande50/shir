package backend.hdl.arch.rewrite

import backend.hdl.arch.mem.{Read, ReadOutOfBound}
import backend.hdl.{IntType, LogicType, OrderedStreamTypeT, ScalarTypeT, VectorType, VectorTypeT}
import backend.hdl.arch.{ArchLambda, ConcatOrderedStream, ConstantValue, CounterInteger, DropOrderedStream, JoinOrderedStream, MapOrderedStream, Repeat, SplitOrderedStream, SplitVector, VectorGenerator, VectorToOrderedStream}
import core.{Conversion, Expr, Let, Marker, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object MoveDownConcatRules extends RulesT {
  override def all(config: Option[Int]): Seq[Rule] = Seq(
    cancelSplitJoin,
    skipMarker,
    moveIntoMap,
    skipMapConvert,
    skipDropJoinStm,
    skipMapStmToVec,
    skipMapSplitVec,
    mapFission,
    leaveMapFun,
    skipLet,
    mergeIntoReadCounter
  )

  def cancelSplitJoin: Rule = Rule("moveDownConcat_cancelSplitJoin", {
    case SplitOrderedStream(ConcatOrderedStream(JoinOrderedStream(in1, _), in2, dim, _), cs, _) if
      cs.ae.evalInt == (in1.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt + in2.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt) =>
      ConcatOrderedStream(in1, Repeat(in2, 1), dim.ae.evalInt + 1)
    case SplitOrderedStream(ConcatOrderedStream(in1, JoinOrderedStream(in2, _), dim, _), cs, _) if
      cs.ae.evalInt == (in1.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt + in2.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt) =>
      ConcatOrderedStream(Repeat(in1, 1), in2, dim.ae.evalInt + 1)
  })

  def skipMarker: Rule = Rule("moveDownConcat_skipMarker", {
    case ConcatOrderedStream(Marker(in1, ts), in2, dim, _) => Marker(ConcatOrderedStream(in1, in2, dim), ts)
    case ConcatOrderedStream(in1, Marker(in2, ts), dim, _) => Marker(ConcatOrderedStream(in1, in2, dim), ts)
  })

  def moveIntoMap: Rule = Rule("moveDownConcat_moveIntoMap", {
    case ConcatOrderedStream(MapOrderedStream(ArchLambda(p1, body, _), in1, mapt: OrderedStreamTypeT), Repeat(in2, rep, _), dim, _) if
      RulesHelper.containsParam(body, p1) && mapt.len.ae.evalInt == rep.ae.evalInt && dim.ae.evalInt != 0 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body))
        innerMostFun.body match {
          case Read(_, ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
          case ReadOutOfBound(_, ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(body, in2, dim.ae.evalInt - 1)), in1)
    case ConcatOrderedStream(Repeat(in1, rep, _), MapOrderedStream(ArchLambda(p1, body, _), in2, mapt: OrderedStreamTypeT), dim, _) if
      RulesHelper.containsParam(body, p1) && mapt.len.ae.evalInt == rep.ae.evalInt && dim.ae.evalInt != 0 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body))
        innerMostFun.body match {
          case Read(_, ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
          case ReadOutOfBound(_, ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(in1, body, dim.ae.evalInt - 1)), in2)
  })

  def skipMapConvert: Rule = Rule("moveDownConcat_skipMapConvert", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, Conversion(pt @ ParamUse(p2), _), _), in, _), Repeat(ConstantValue(intValue, _), rep, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(ConstantValue(intValue, Some(pt.t)), rep), dim))
    case ConcatOrderedStream(Repeat(ConstantValue(intValue, _), rep, _), MapOrderedStream(cnvFun @ ArchLambda(p1, Conversion(pt @ ParamUse(p2), _), _), in, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(ConstantValue(intValue, Some(pt.t)), rep), in, dim))
  })

  def skipDropJoinStm: Rule = Rule("moveDownConcat_skipDropJoinStm", {
    // Only works for Drop with zero
    case ConcatOrderedStream(Repeat(in1, rep, _), DropOrderedStream(JoinOrderedStream(in2, _), fstElements, lstElements, _), dim, _) if
      fstElements.ae.evalInt == 0 && dim.ae.evalInt == 0 =>
      val innerLen = in2.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      val diff = innerLen.ae.evalInt - (rep.ae.evalInt + 1) % innerLen.ae.evalInt + 1
      val newOuterRep = math.ceil(rep.ae.evalInt.toFloat / innerLen.ae.evalInt).toInt
      val newIn1 = TypeChecker.check(Repeat(Repeat(in1, innerLen.ae.evalInt), newOuterRep))
      DropOrderedStream(JoinOrderedStream(ConcatOrderedStream(newIn1, in2, dim)), diff, lstElements)
    case ConcatOrderedStream(DropOrderedStream(JoinOrderedStream(in1, _), fstElements, lstElements, _), Repeat(in2, rep, _),  dim, _) if
      lstElements.ae.evalInt == 0 && dim.ae.evalInt == 0 =>
      val innerLen = in1.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      val diff = innerLen.ae.evalInt - (rep.ae.evalInt + 1) % innerLen.ae.evalInt + 1
      val newOuterRep = math.ceil(rep.ae.evalInt.toFloat / innerLen.ae.evalInt).toInt
      val newIn2 = TypeChecker.check(Repeat(Repeat(in2, innerLen.ae.evalInt), newOuterRep))
      DropOrderedStream(JoinOrderedStream(ConcatOrderedStream(in1, newIn2, dim)), fstElements, diff)
  })

  def skipMapStmToVec: Rule = Rule("moveDownConcat_skipMapStmToVec", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, VectorToOrderedStream(pt @ ParamUse(p2), _), _), in, _), Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), dim))
    case ConcatOrderedStream(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), MapOrderedStream(cnvFun @ ArchLambda(p1, VectorToOrderedStream(pt @ ParamUse(p2), _), _), in, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), in, dim))
  })

  def skipMapSplitVec: Rule = Rule("moveDownConcat_skipskipMapSplitVec", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, SplitVector(pt @ ParamUse(p2), cs, _), _), in, _),
    Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), dim, _) if intValue.ae.evalInt == 0 &&
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(ConstantValue(intValue, Some(pt.t)), rep2), dim))
    case ConcatOrderedStream(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, SplitVector(pt @ ParamUse(p2), cs, _), _), in, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), in, dim))
  })

  def mapFission: Rule = Rule("moveDownConcat_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit{
        case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body match{
        case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var newInput: Expr = null
      body.visit{
        case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          val param = ParamDef()
          newInput = MapOrderedStream(ArchLambda(param, ConcatOrderedStream(padInput, ParamUse(param))), input)
        case _ =>
      }
      MapOrderedStream(
        {
          val param1 = ParamDef()
          ArchLambda(
            param1,
            body.visitAndRebuild{
              case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
                p1.id == p2.id && dim.ae.evalInt == 0 =>
                ParamUse(param1)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit{
        case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body match{
        case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var newInput: Expr = null
      body.visit{
        case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          val param = ParamDef()
          newInput = MapOrderedStream(ArchLambda(param, ConcatOrderedStream(ParamUse(param), padInput)), input)
        case _ =>
      }
      MapOrderedStream(
        {
          val param1 = ParamDef()
          ArchLambda(
            param1,
            body.visitAndRebuild{
              case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
                p1.id == p2.id && dim.ae.evalInt == 0 =>
                ParamUse(param1)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  def leaveMapFun: Rule = Rule("moveDownConcat_leaveMapFun", {
    case MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(ParamUse(p2), in2, dim, _), _), in1, _) if p1.id == p2.id =>
      ConcatOrderedStream(in1, Repeat(in2, in1.t.asInstanceOf[OrderedStreamTypeT].len), dim.ae.evalInt + 1)
    case MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(in1, ParamUse(p2), dim, _), _), in2, _) if p1.id == p2.id =>
      ConcatOrderedStream(Repeat(in1, in2.t.asInstanceOf[OrderedStreamTypeT].len), in2, dim.ae.evalInt + 1)
  })

  def skipLet: Rule = Rule("moveDownPaddingRules_skipLet", {
    case ConcatOrderedStream(in1, Let(p, in2, arg, _), dim, _) =>
      Let(p, ConcatOrderedStream(in1, in2, dim), arg)
    case ConcatOrderedStream(Let(p, in1, arg, _), in2, dim, _) =>
      Let(p, ConcatOrderedStream(in1, in2, dim), arg)
  })

  def mergeIntoReadCounter: Rule = Rule("moveDownPaddingRules_mergeIntoReadCounter", {
    case ConcatOrderedStream(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _),
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Read(mem, ParamUse(p3), _), _), ParamUse(p4), _), _),
    CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _), dim, _) if
    p1.id == p4.id && p2.id == p3.id && intValue.ae.evalInt == 0 =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1
      val newElementType = IntType(newBitWidth)
      val newCounter = CounterInteger(start, increment, loop, dimensions, repetitions, Some(newElementType))
      val invalidAddr = scala.math.pow(2, newBitWidth).toInt - 1
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(Repeat(Repeat(newConst, rep1), rep2), newCounter, dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(2, ArchLambda(param, ReadOutOfBound(mem, ParamUse(param))), newAddr)
    case ConcatOrderedStream(
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Read(mem, ParamUse(p3), _), _), ParamUse(p4), _), _),
    CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _),
    Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), dim, _) if
      p1.id == p4.id && p2.id == p3.id && intValue.ae.evalInt == 0 =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1
      val newElementType = IntType(newBitWidth)
      val newCounter = CounterInteger(start, increment, loop, dimensions, repetitions, Some(newElementType))
      val invalidAddr = scala.math.pow(2, newBitWidth).toInt - 1
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(newCounter, Repeat(Repeat(newConst, rep1), rep2), dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(2, ArchLambda(param, ReadOutOfBound(mem, ParamUse(param))), newAddr)
  })
}
