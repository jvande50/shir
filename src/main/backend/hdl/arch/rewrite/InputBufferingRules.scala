package backend.hdl.arch.rewrite

import backend.hdl.{BlockRamTypeT, LogicType, OrderedStreamType, OrderedStreamTypeT, RamArrayTypeT, VectorType}
import backend.hdl.arch.mem._
import backend.hdl.arch._
import core.rewrite.Rule
import core._

object InputBufferingRules {

  def readDoubleBuffering: Seq[Rule] =
    Seq(
      mapFissionReadAsync,
      doubleBufferRead
    )

  def bufferRepeatedStream: Rule = Rule("bufferRepeatedStream", {
    case l: LambdaT if
    {
      RulesHelper.hasRepeatedStream(l) && RulesHelper.isComputation(l) &&
        !RulesHelper.exprContains(l.body, { // prevent infinite rewrites
          case MemoryAllocation(_, _, _, _, _) => true
        })
    } =>
      // TODO fix before using
      ???
      // this rule inserts a buffer next to a Tuple, which results in a deadlock!
      // buffer must be inserted at input source
      val param = ParamDef(l.param.t)
      ArchLambda(param, FunctionCall(l, Marker(BufferStreamInBlockRam(ParamUse(param)), TextType("buffer_repeated_stream"))))
  })

  def bufferInputRow(inputLabel: String, scale: Int = 1, useOnBoardRam: Boolean = false): Rule = bufferInput(inputLabel, 1, scale, useOnBoardRam)

  def bufferInputMatrix(inputLabel: String, scale: Int = 1, useOnBoardRam: Boolean = false): Rule = bufferInput(inputLabel, 2, scale, useOnBoardRam)

  def bufferInput(inputLabel: String, dimensionsToBuffer: Int = 1, scale: Int = 1, useOnBoardRam: Boolean = false): Rule = Rule("bufferInput", {
    case Marker(input, text)
      if text.s.contains(inputLabel) &&
        !RulesHelper.exprContains(input, {
          // Avoid inserting buffer (BlockRAM) multiple times. If there is a host ram write, we can still insert a buffer.
          case Write(_, _, t: RamArrayTypeT) if t.memoryLocation.isInstanceOf[BlockRamTypeT] => true
        }) =>
      Marker(_bufferInput(input, dimensionsToBuffer, scale, useOnBoardRam), text)
  })

  private def _bufferInput(input: Expr, dimensionsToBuffer: Int = 1, scale: Int, useOnBoardRam: Boolean): Expr = {
    val inputType: OrderedStreamTypeT = input.t match {
      case s: OrderedStreamTypeT => s
      case _ => throw MalformedExprException("Cannot buffer anything other than streams.")
    }
    val inputDimensions = inputType.dimensions

    if (scale > 1 && inputDimensions.head.ae.evalInt % scale != 0)
      throw new Exception("Cannot scale memory input and output, because the innermost stream does not contain a multiple of " + scale + " elements (" + inputDimensions.head.ae.evalInt + ") !")

    val scaledLeafType = VectorType(LogicType(), inputType.leafType.bitWidth.ae.evalInt * scale)
    val scaledType = OrderedStreamType(ArithType(inputDimensions.head.ae.evalInt / scale) +: inputDimensions.tail, scaledLeafType)

    val bufferedType = OrderedStreamType(scaledType.dimensions.slice(0, dimensionsToBuffer), scaledLeafType)

    val inputConversion = ArchConversion(input, inputType, scaledType)

    val buffer =
      MapOrderedStream(
        inputType.dimensions.length - dimensionsToBuffer,
        {
          val p = ParamDef(bufferedType)
          ArchLambda(p, {
            if (useOnBoardRam)
              BufferStreamInOnBoardRam(ParamUse(p))
            else
              BufferStreamInBlockRam(ParamUse(p))
          })
        },
        inputConversion
      )

    ArchConversion(buffer, scaledType, inputType)
  }

  def doubleBufferRead: Rule = Rule("doubleBufferRead", {
    case ArchLambda(p1, ReadAsyncOrdered(readMemoryController, baseAddr, ParamUse(p2), _), _) if p1.id == p2.id =>

      val f1 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrdered(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0)))
      }
      val f2 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrdered(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0)))
      }
      ArchLambda(p1, Alternate(f1, f2, Tuple2(ParamUse(p1), baseAddr)))
  })

  def limitParallelHostRamReadRequests(maxParReq: Int = 2): Rule = Rule("limitParallelHostRamReadRequests", {
    case Let(param, body, ArchLambda(p1, memController@ReadHostMemoryController(ParamUse(p2), _, _, _), _), _) if p1.id == p2.id && {
      RulesHelper.exprContains(body, {
        case ReadAsyncOrdered(ParamUse(p), _, _, t: OrderedStreamTypeT)
          if param.id == p.id && t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 => true
      })
    } =>
      Let(
        param,
        body.visitAndRebuild({
          case ReadAsyncOrdered(ParamUse(p), baseAddr, input, t: OrderedStreamTypeT)
            if param.id == p.id && t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 =>
            val splitInput = TypeChecker.check(SplitOrderedStream(input, maxParReq))
            JoinOrderedStream(
              MapOrderedStream(
                {
                  val pIn = ParamDef()
                  ArchLambda(
                    pIn,
                    ReadAsyncOrdered(ParamUse(p), baseAddr, ParamUse(pIn))
                  )
                },
                splitInput
              )
            )
          case e => e
        }).asInstanceOf[Expr],
        memController
      )
  })

  def limitParallelReadRequests(maxParReq: Int = 2): Rule = Rule("limitParallelReadRequests", {
    case MapOrderedStream(ArchLambda(p1, Read(memAllocation, ParamUse(p2), _), _), input, t: OrderedStreamTypeT)
      if t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 && p1.id == p2.id =>
      JoinOrderedStream(
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(p,
              MapOrderedStream(
                {
                  val pp = ParamDef()
                  ArchLambda(pp, Read(memAllocation, ParamUse(pp)))
                },
                ParamUse(p)
              )
            )
          },
          SplitOrderedStream(input, maxParReq)
        )
      )

    case ReadAsyncOrdered(readMemoryController, baseAddr, input, t: OrderedStreamTypeT)
      if t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 =>
      val splitInput = TypeChecker.check(SplitOrderedStream(input, maxParReq))
      JoinOrderedStream(
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(
              p,
              ReadAsyncOrdered(readMemoryController, baseAddr, ParamUse(p))
            )
          },
          splitInput
        )
      )

    case ReadAsync(readMemoryController, baseAddr, input, t: OrderedStreamTypeT) => ???
  })

  def mapFissionReadAsync: Rule = Rule("mapFissionReadAsync", {
    // map fission
    // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
    case MapOrderedStream(ArchLambda(origParam, ReadAsyncOrdered(mc, ba, body, _), _), input, _) if !RulesHelper.isParamUse(body) =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            ReadAsyncOrdered(mc, ba, ParamUse(param))
          )
        }, MapOrderedStream(ArchLambda(origParam, body), input)
      )
  })
}
