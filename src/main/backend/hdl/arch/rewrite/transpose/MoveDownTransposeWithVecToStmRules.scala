package backend.hdl.arch.rewrite.transpose

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.rewrite.conv.MoveUpSlideStreamRules
import backend.hdl.arch.rewrite.{CleanupRules, RemoveStreamVectorConversionRules, RulesHelper}
import backend.hdl.arch.{ArchLambda, DropVector, JoinVector, MapOrderedStream, MapVector, OrderedStreamToVector, PermuteVector, SlideOrderedStream, SplitVector, Transpose, TransposeNDOrderedStream, VectorToOrderedStream}
import core.{ArithType, ArithTypeT, Expr, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import lift.arithmetic.{Cst, IntDiv, Mod, Prod, Sum}

import scala.collection.Seq

object MoveDownTransposeWithVecToStmRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFusion,
      skipTransposeVec,
      skipMapMapVec2DSplitVec,
      skipSlideStream,
      skipMapMapVecDropVec,
      skipMapMapVecJoinVec,
    ) ++ Seq(MoveUpSlideStreamRules.mapFission, CleanupRules.mapFissionMapVecFun, RemoveStreamVectorConversionRules.removeConversion3)

  def mapFusion: Rule = Rule("moveDownTransposeWithVecToStm_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), dimOrder, _) if
            p1.id == p5.id && p3.id == p4.id && dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p6) if p2.id == p6.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )

    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, VectorToOrderedStream(ParamUse(p5), _), _),
          VectorToOrderedStream(ParamUse(p6), _), _), _), ParamUse(p7), _), dimOrder, _) if
            p1.id == p7.id && p3.id == p6.id && p4.id == p5.id && dimOrder.head.ae.evalInt == 1 && dimOrder(1).ae.evalInt == 2 && dimOrder.last.ae.evalInt == 0 =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p8) if p2.id == p8.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def skipTransposeVec: Rule = Rule("moveDownTransposeWithVecToStm_skipTransposeVec", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _),
    MapOrderedStream(ArchLambda(p3, SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(p4), _), _),
    perFun, _), chunkSize2, _), chunkSize1, _), _), input, _), _), dimOrder, _) if
      p1.id == p2.id && p3.id == p4.id && {
      var exprFound = false
        perFun.b.ae match {
        case Sum(IntDiv(v1, c1)::Prod(Cst(_)::Mod(v2, c2)::Nil)::Nil) if v1 == v2 && c1 == c2 =>
          exprFound = true
        case _=>
      }
      exprFound
    } && dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, 2, 1)
      val stmInput = MapOrderedStream(
        {
          val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(ParamUse(param)))
          )
        }, input
      )
      val stmInputtc = TypeChecker.check(stmInput)
      val transposedInput = TransposeNDOrderedStream(stmInputtc, newDimOrder)
      val transposedInputtc = TypeChecker.check(transposedInput)
      val vecIntermediate = MapOrderedStream(2, OrderedStreamToVector.asFunction(), transposedInputtc)
      val vecIntermediatetc = TypeChecker.check(vecIntermediate)
      MapOrderedStream(
        {
          val param1 = ParamDef(vecIntermediatetc.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param1,
            MapOrderedStream(
              {
                val param2 = ParamDef(vecIntermediatetc.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(
                  param2,
                  Transpose(ParamUse(param2))
                )
              }, ParamUse(param1)
            )
          )
        }, vecIntermediatetc
      )
  })

  def skipMapMapVec2DSplitVec: Rule = Rule("moveDownTransposeWithVecToStm_skipMapMapVec2DSplitVec", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _),
    VectorToOrderedStream(ParamUse(p4), _), _), _), MapOrderedStream(ArchLambda(p5, MapVector(ArchLambda(p6,
    MapVector(ArchLambda(p7, SplitVector(ParamUse(p8), chunkSize, _), _), ParamUse(p9), _), _), ParamUse(p10), _), _), input, _), _), dimOrder, _) if
      p1.id == p4.id && p2.id == p3.id && p5.id == p10.id && p6.id == p9.id && p7.id == p8.id =>
      val transposedInput = TransposeNDOrderedStream(
        MapOrderedStream(
          {
            val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param,
              MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(ParamUse(param)))
            )
          }, input
        ),
        dimOrder
      )
      val transposedInputtc = TypeChecker.check(transposedInput)
      MapOrderedStream(3, SplitVector.asFunction(Seq(None), Seq(chunkSize)), transposedInputtc)
  })

  def skipSlideStream: Rule = Rule("moveDownTransposeWithVecToStm_skipSlideStream", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _),
    VectorToOrderedStream(ParamUse(p4), _), _), _), SlideOrderedStream(input, windowSize, _), _), dimOrder, _) if
      p1.id == p4.id && p2.id == p3.id && dimOrder.last.ae.evalInt == 0 && dimOrder(1).ae.evalInt == 2 =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, 2)
      val transposedInput = TransposeNDOrderedStream(
        MapOrderedStream(VectorToOrderedStream.asFunction(), input),
        newDimOrder
      )
      val transposedInputtc = TypeChecker.check(transposedInput)
      val slidedInput = MapOrderedStream(SlideOrderedStream.asFunction(Seq(None), Seq(windowSize)), transposedInputtc)
      val slidedInputtc = TypeChecker.check(slidedInput)
      MapOrderedStream(2, VectorToOrderedStream.asFunction(), slidedInputtc)
  })

  def skipMapMapVecDropVec: Rule = Rule("moveDownTransposeWithVecToStm_skipMapMapVecDropVec", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), MapOrderedStream(ArchLambda(p3,
    MapVector(ArchLambda(p4, DropVector(ParamUse(p5), firstElements, lastElements, _), _) ,ParamUse(p6), _), _), input, _), _), dimOrder, _) if
      p1.id == p2.id && p3.id == p6.id && p4.id == p5.id && dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 =>
      val transposedInput = TransposeNDOrderedStream(MapOrderedStream(VectorToOrderedStream.asFunction(), input), dimOrder)
      val transposedInputtc = TypeChecker.check(transposedInput)
      MapOrderedStream(2, DropVector.asFunction(Seq(None), Seq(firstElements, lastElements)), transposedInputtc)
  })

  def skipMapMapVecJoinVec: Rule = Rule("moveDownTransposeWithVecToStm_skipMapMapVecJoinVec", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), MapOrderedStream(ArchLambda(p3,
    MapVector(ArchLambda(p4, JoinVector(ParamUse(p5), _), _) ,ParamUse(p6), _), _), input, _), _), dimOrder, _) if
      p1.id == p2.id && p3.id == p6.id && p4.id == p5.id && dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 =>
      val transposedInput = TransposeNDOrderedStream(MapOrderedStream(VectorToOrderedStream.asFunction(), input), dimOrder)
      val transposedInputtc = TypeChecker.check(transposedInput)
      MapOrderedStream(2, JoinVector.asFunction(), transposedInputtc)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), MapOrderedStream(ArchLambda(p3,
    MapVector(ArchLambda(p4, MapVector(ArchLambda(p5, JoinVector(ParamUse(p6), _), _) ,ParamUse(p7), _), _) ,ParamUse(p8), _), _), input, _), _), dimOrder, _) if
      p1.id == p2.id && p3.id == p8.id && p4.id == p7.id && p5.id ==p6.id && dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 =>
      val transposedInput = TransposeNDOrderedStream(MapOrderedStream(VectorToOrderedStream.asFunction(), input), dimOrder)
      val transposedInputtc = TypeChecker.check(transposedInput)
      MapOrderedStream(2, {
        val param = ParamDef()
        ArchLambda(
          param,
          MapVector(JoinVector.asFunction(), ParamUse(param))
        )
      }, transposedInputtc)
  })

}
