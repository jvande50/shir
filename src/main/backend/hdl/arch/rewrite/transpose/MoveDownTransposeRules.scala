package backend.hdl.arch.rewrite.transpose

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.rewrite.{CleanupRules, RulesHelper}
import backend.hdl.arch.{ArchLambda, CounterInteger, JoinOrderedStream, JoinVector, MapOrderedStream, PermuteVector, SplitVector, TransposeNDOrderedStream, VectorToOrderedStream}
import core.{ArithType, ArithTypeT, Expr, LambdaT, Let, Marker, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object MoveDownTransposeRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      skipMarker,
    )

  def mergeIntoReadAddress: Seq[Rule] = Seq(
    moveIntoMapNonCompute,
    skipMapJoinStream,
    mapFission,
    skipMapParam2D,
    moveIntoLet,
    skipMapWithCounter,
    swapABCounter,
    mapFission2,
    skipVecToStm2D,
  ) ++ Seq(CleanupRules.mapFissionMapStmFun) ++ Seq(
    TransposeConversionRules.mergeIntoCounter,
    TransposeConversionRules.mergeIntoMap
  )

  def moveIntoMapNonCompute: Rule = Rule("moveDownTranspose_moveIntoMapNonCompute", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, body ,_), input, _), dimOrder, _) if
      !RulesHelper.isComputation(body) && dimOrder.last.ae.evalInt == dimOrder.length - 1 =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, dimOrder.length - 1)
      MapOrderedStream(
        ArchLambda(p1, TransposeNDOrderedStream(body, newDimOrder)), input
      )
  })

  def moveIntoMapNonCompute2: Rule = Rule("moveDownTranspose_moveIntoMapNonCompute2", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(fun.body, p1) &&
        !RulesHelper.isComputation(input2) && dimOrder.last.ae.evalInt == dimOrder.length - 1 =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, dimOrder.length - 1)
      MapOrderedStream(
        ArchLambda(p1, TransposeNDOrderedStream(MapOrderedStream(fun: LambdaT, input2), newDimOrder)), input1
      )
  })
  def skipMapJoinStream: Rule = Rule("moveDownTranspose_skipMapJoinStream", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _) ,_), input, _), dimOrder, _) if
      p1.id == p2.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 2, dimOrder.length-1)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      JoinOrderedStream(transposedInput)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _) ,_), ParamUse(p4), _), _), input, _), dimOrder, _) if
      p1.id == p4.id && p2.id == p3.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && RulesHelper.checkDimOrder(dimOrder.drop(1)) =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 2, dimOrder.length-1)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      MapOrderedStream(JoinOrderedStream.asFunction(), transposedInput)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _) ,_), ParamUse(p5), _), _), ParamUse(p6), _), _), input, _), dimOrder, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && RulesHelper.checkDimOrder(dimOrder.drop(1)) =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 2, dimOrder.length-1)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      MapOrderedStream(2, JoinOrderedStream.asFunction(), transposedInput)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _) ,_), ParamUse(p4), _), _), input, _), dimOrder, _) if
      p1.id == p4.id && p2.id == p3.id && dimOrder.last.ae.evalInt == dimOrder.length - 1 &&
        dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 3 &&
        dimOrder(dimOrder.length - 3).ae.evalInt == dimOrder.length - 2 &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(3)) =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 3, dimOrder.length - 2)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      MapOrderedStream(JoinOrderedStream.asFunction(), transposedInput)
  })

  def mapFission: Rule = Rule("moveDownTranspose_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit{
        case TransposeNDOrderedStream(ParamUse(p2), dimOrder, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val dimOrder = {
        var exprFound = Seq().asInstanceOf[Seq[ArithTypeT]]
        body.visit{
          case TransposeNDOrderedStream(ParamUse(p2), dimOrder, _) if p1.id == p2.id =>
            exprFound = dimOrder
          case _ =>
        }
        exprFound
      }
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length, dimOrder.length)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      val transposedInputtc = TypeChecker.checkIfUnknown(transposedInput, transposedInput.t)
      MapOrderedStream(
        {
          val param = ParamDef(transposedInputtc.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            body.visitAndRebuild{
              case TransposeNDOrderedStream(ParamUse(p2), dimOrder, _) if p1.id == p2.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, transposedInputtc
      )
  })

  def skipMapParam2D: Rule = Rule("moveDownTranspose_skipMapParam2D", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun, ParamUse(p2), _) ,_), input, _), dimOrder, _) if
      p1.id == p2.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(2)) =>
      val inputDims = input.t.asInstanceOf[OrderedStreamTypeT].dimensions
      val newDimOrder = if(inputDims.length <= dimOrder.length)
        dimOrder.drop(dimOrder.length - inputDims.length).map(_.ae.evalInt - (dimOrder.length - inputDims.length)).map(ArithType(_))
      else
        RulesHelper.generateCountSeq(0, inputDims.length - dimOrder.length) ++ dimOrder.map(_.ae.evalInt + (inputDims.length - dimOrder.length)).map(ArithType(_))
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      val transposedInputtc = TypeChecker.check(transposedInput)
      MapOrderedStream(2, fun, transposedInputtc)
  })

  private def skipMarker: Rule = Rule("moveDownTranspose_skipMarker", {
    case TransposeNDOrderedStream(Marker(input, text), dimOrder, _)  =>
      Marker(TransposeNDOrderedStream(input, dimOrder), text)
  })

  def moveIntoLet: Rule = Rule("moveDownTranspose_moveIntoLet", {
    case TransposeNDOrderedStream(Let(arg, body, param, _), dimOrder, _)  =>
      Let(arg, TransposeNDOrderedStream(body, dimOrder), param)
  })

  def swapABMap: Rule = Rule("moveDownTranspose_swapABMap", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, body, _), input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(body, p1) && RulesHelper.containsParam(body, p2) && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
       RulesHelper.checkDimOrder(dimOrder.dropRight(2)) =>
      MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p2, body), input1)), input2)
  })

  def swapABCounter: Rule = Rule("moveDownTranspose_swapABCounter", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, body, _), input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(body, p1) && RulesHelper.containsParam(body, p2) && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(2)) && {
        var exprFound = false
        input1 match {
          case CounterInteger(_, _, _, _, _, _) =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        input2 match {
          case CounterInteger(_, _, _, _, _, _) =>
            exprFound = true
          case _ =>
        }
        exprFound
      }=>
      MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p1, body), input1)), input2)
  })

  def skipMapWithCounter: Rule = Rule("moveDownTranspose_skipMapWithCounter", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, body ,_), input, _), dimOrder, _) if {
      var exprFound = false
      input match {
        case CounterInteger(_, _, _, _, _, _) =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && dimOrder.last.ae.evalInt == dimOrder.length - 1 =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, dimOrder.length - 1)
      MapOrderedStream(
        ArchLambda(p1, TransposeNDOrderedStream(body, newDimOrder)), input
      )
  })

  def skipVecToStm2D: Rule = Rule("moveDownTranspose_skipVecToStm2D", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _),
    VectorToOrderedStream(ParamUse(p4), _), _), _), input, _), dimOrder, _) if
      p1.id == p4.id && p2.id ==p3.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(2)) =>
      MapOrderedStream(2, VectorToOrderedStream.asFunction(), TransposeNDOrderedStream(MapOrderedStream(VectorToOrderedStream.asFunction(), input), Seq(1, 0)))
  })

  def mapFission2: Rule = Rule("moveDownTranspose_mapFission2", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(p2), _), _), fun, _), cs2, _), cs1, _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val alreadyApplied: Boolean = body match {
        case SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(p2), _), _), fun, _), cs2, _), cs1, _) if p1.id == p2.id => true
        case _ => false
      }
      !alreadyApplied && exprFound && paramUses == 1
    } =>
      val param = ParamDef()
      var tranposeFun: Expr = null
      val newFun = {
        val newParam = ParamDef()
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(p2), _), _), fun, _), cs2, _), cs1, _) if p1.id == p2.id =>
              tranposeFun = SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(param))), fun), cs2), cs1)
              ParamUse(newParam)
            case ParamUse(pd) if pd.id == newParam.id =>
              ParamUse(newParam)
            case e => e
          }.asInstanceOf[Expr]
        )
      }
      MapOrderedStream(newFun, MapOrderedStream(ArchLambda(param, tranposeFun), input))
  })
}