package backend.hdl.arch.rewrite.transpose

import backend.hdl.{IntType, NamedTupleTypeT, OrderedStreamTypeT}
import backend.hdl.arch.{AddInt, ArchLambda, CounterInteger, IndexUnorderedStream, JoinOrderedStream, JoinStreamAll, JoinVector, MapOrderedStream, OrderedStreamToUnorderedStream, OrderedStreamToVector, PermuteVector, ResizeInteger, SplitOrderedStream, SplitStreamAll, Transpose, TransposeNDOrderedStream, TruncInteger, Tuple2, VectorToOrderedStream, Zip2OrderedStream}
import backend.hdl.arch.rewrite.RulesHelper
import core.{ArithLambdaType, ArithType, ArithTypeT, ArithTypeVar, Expr, Marker, ParamDef, ParamUse, TextType, TypeChecker}
import core.rewrite.{Rule, RulesT}
import lift.arithmetic.{ArithExpr, Cst, IntDiv, Mod, Prod, Sum}

import scala.annotation.tailrec

object TransposeConversionRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      cleanTranspose,
      mergeTransposes,
      replacePermuteVecWithTransposeStm,
    )

  def cleanTranspose: Rule = Rule("cleanTranspose", {
    case TransposeNDOrderedStream(input, dimOrder, _) if RulesHelper.checkDimOrder(dimOrder) =>
      input
  })

  def mergeTransposes: Rule = Rule("mergeTransposes", {
    case TransposeNDOrderedStream(TransposeNDOrderedStream(input, dimOrder1, _), dimOrder2, _) if
      dimOrder1.length == dimOrder2.length =>
      val newDimOrder = RulesHelper.mergeDimOrders(dimOrder2, dimOrder1)
      TransposeNDOrderedStream(input, newDimOrder)
  })

  def convertTranspose2DIntoPermuteVec: Rule = Rule("convertTranspose2DIntoPermuteVec", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(JoinVector(OrderedStreamToVector(
    MapOrderedStream(ArchLambda(p2, body, _), input2, _), _), _), _), _), input1, _), dimOrder, t: OrderedStreamTypeT) if dimOrder.length == 2 &&
      dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 &&
      t.len.ae.evalInt % t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt== 0 &&
      t.len.ae.evalInt > t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt =>
      JoinOrderedStream(
        MapOrderedStream(
          ArchLambda(
            p2,
            TransposeNDOrderedStream(MapOrderedStream(
              ArchLambda(
                p1,
                VectorToOrderedStream(body)
              ),
              input1
            ), Seq(1, 0))
          ), input2
        )
      )
    case TransposeNDOrderedStream(input, dimOrder, _) if dimOrder.length == 2 && dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 =>
      val inputVec = TypeChecker.check(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(), input)))
      val transposedInput = Transpose(inputVec)
      MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(transposedInput))

    case TransposeNDOrderedStream(input, dimOrder, _) if dimOrder.length == 3 && dimOrder.head.ae.evalInt == 0 && dimOrder(1).ae.evalInt == 2 && dimOrder.last.ae.evalInt == 1 =>
      val inputVec = TypeChecker.check(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(), MapOrderedStream(2, OrderedStreamToVector.asFunction(), input))))
      val transposedInput = Transpose(inputVec)
      MapOrderedStream(2, VectorToOrderedStream.asFunction(), MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(transposedInput)))

    case TransposeNDOrderedStream(input, dimOrder, _) if dimOrder.length == 3 && dimOrder.head.ae.evalInt == 1 && dimOrder(1).ae.evalInt == 2 && dimOrder.last.ae.evalInt == 0 =>
      val chunkSize = input.t.asInstanceOf[OrderedStreamTypeT].dimensions(1).ae.evalInt
      val inputVec = TypeChecker.check(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(),  JoinOrderedStream(input))))
      val transposedInput = Transpose(inputVec)
      MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(transposedInput)))
  })

  def generateSumOfCounters(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT],
                            repetitions: Seq[ArithTypeT], dimOrder: Seq[ArithTypeT]): (Expr, Long) = {
    // dimOrder shouldn't be empty
    // (if it does happen, the match would throw an error)
    dimOrder match {
      case Seq(x) =>
        val order = x.ae.evalInt
        val dim = dimensions(order)
        val rep = repetitions(order)
        val incr = dimensions.slice(0, order).foldLeft(increment.ae)(_ * _.ae)
        val last = start.ae + (if (rep.ae.evalInt == 1) 0 else incr * (dim.ae - 1))
        (CounterInteger(start, incr, loop, Seq(dim), Seq(rep), None), last.evalInt)
      case xs :+ x =>
        val (prevSum, prevLast) = generateSumOfCounters(start, increment, loop, dimensions, repetitions, xs)
        val prevSumtc = TypeChecker.check(prevSum)
        val prevSumLevel = prevSumtc.t.asInstanceOf[OrderedStreamTypeT].dimensions.length

        val order = x.ae.evalInt
        val dim = dimensions(order)
        val rep = repetitions(order)
        val incr = dimensions.slice(0, order).foldLeft(increment.ae)(_ * _.ae)
        val last = prevLast + (if (rep.ae.evalInt == 1) 0 else (incr * (dim.ae - 1)).evalInt)

        val counter = CounterInteger(
          ArithType(0), incr, loop, Seq(dim), Seq(rep),
          Some(IntType(last.toBinaryString.length)))

        (Marker(MapOrderedStream(
          {
            val param1 = ParamDef(counter.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param1,
              MapOrderedStream(
                prevSumLevel,
                {
                  val param2 = ParamDef(prevSumtc.t.asInstanceOf[OrderedStreamTypeT].leafType)
                  ArchLambda(
                    param2,
                    TypeChecker.check(AddInt(Tuple2(ParamUse(param1), ParamUse(param2))))
                  )
                }, prevSumtc
              )
            )
          }, counter
        ), TextType("FusionGuard")), last)
    }
  }

  def mergeIntoCounter: Rule = Rule("mergeIntoCounter", {
    case TransposeNDOrderedStream(CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), dimOrder, _)  =>
      val bitWidth = t.leafType.bitWidth
      val (sum, _) = generateSumOfCounters(start, increment, loop, dimensions, repetitions, dimOrder)
      val sumtc = TypeChecker.check(sum)
      val sumLevel = dimOrder.length
      MapOrderedStream(
        sumLevel,
        {
          val param = ParamDef(sumtc.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(
            param,
            ResizeInteger(ParamUse(param), bitWidth)
          )
        }, sumtc
      )
  })

  def mergeIntoMap: Rule = Rule("mergeIntoMap", {
    case TransposeNDOrderedStream(maps @ MapOrderedStream(_, _, _), order, _) if (
      // the check to make sure maps is a MapOrderedStream is for visualization
      canExchangeMap(maps, order)
    ) =>
      @tailrec
      def loop(expr: Expr, order: Seq[ArithTypeT], xchg: Seq[(Int, ParamDef, Expr)], min: Option[Int]): Expr = (order, expr) match {
        case (order :+ x, MapOrderedStream(ArchLambda(p1, expr, _), counter, _)) =>
          val index = x.ae.evalInt
          loop(expr, order, (index, p1, counter) +: xchg, Some(min.getOrElse(index).min(index)))
        case (Seq(), expr) =>
          xchg.map(p => xchg(p._1 - min.get)).foldLeft(expr)({ case (body, (_, param, stream)) =>
            MapOrderedStream(ArchLambda(param, body), stream)
          })
      }
      loop(maps, dropInnerIdentityOrder(order), Seq(), None)
  })

  def dropInnerIdentityOrder(order: Seq[ArithTypeT]): Seq[ArithTypeT] =
    order.zipWithIndex.dropWhile(p => p._1.ae.evalInt == p._2).map(_._1)

  def canExchangeMap(expr: Expr, order: Seq[ArithTypeT]): Boolean = {
    @tailrec
    def loop(expr: Expr, order: Seq[_], ivs: Set[Long]): Boolean = (order, expr) match {
      case (Seq(), _) => true
      case (order :+ _, MapOrderedStream(ArchLambda(p1, expr, _), counter, _)) if {
        // disallow map exchange if the counter refers to any of the induction
        // variables introduced in higher levels (which is sufficient).
        var usesIV = false
        counter.visit({
          case ParamUse(id) => usesIV |= ivs.contains(id.id)
          case _ =>
        })
        !usesIV
      } =>
        loop(expr, order, ivs + p1.id)

      case _ => false
    }

    // identity transpose should be dealt with by other rewrites, so make sure
    // we are transposing something before checking for the nested maps.
    val leftovers = dropInnerIdentityOrder(order)
    leftovers.nonEmpty && loop(expr, leftovers, Set.empty)
  }

  def replacePermuteVecWithTransposeStm = Rule("replacePermuteVecWithTransposeStm", {
    case SplitOrderedStream(VectorToOrderedStream(PermuteVector(
    OrderedStreamToVector(JoinOrderedStream(input, _), _), arithFun, _), _), chunkSize, _) if {
      var exprFound = false
      arithFun.b.ae match {
        case Sum(IntDiv(v1, c1)::Prod(Cst(_)::Mod(v2, c2)::Nil)::Nil) if v1 == v2 && c1 == c2 =>
          exprFound = true
        case _=>
      }
      exprFound
    } =>
      TransposeNDOrderedStream(input, Seq(1,0))
  })

  def mergeIntoIndexStm: Rule = Rule("mergeIntoIndexStm", {
    case IndexUnorderedStream(OrderedStreamToUnorderedStream(joinTransposeInput, _), t) if {
      var exprFound = false
      val innerMostInput = RulesHelper.getCurrentOrJoinNDInput(joinTransposeInput)
      innerMostInput match {
        case TransposeNDOrderedStream(input, dimOrder, transposedType: OrderedStreamTypeT) if
          dimOrder.length - 1 == RulesHelper.getJoinNDLevels(joinTransposeInput) =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      // Get transposition's info
      var transposedDims: Seq[ArithTypeT] = Seq()
      var dimOrder: Seq[ArithTypeT] = Seq()
      var input: Expr = joinTransposeInput

      val innerMostInput = RulesHelper.getCurrentOrJoinNDInput(joinTransposeInput)
      innerMostInput match {
        case TransposeNDOrderedStream(i, order, tT: OrderedStreamTypeT) if
          order.length - 1 == RulesHelper.getJoinNDLevels(joinTransposeInput) =>
          transposedDims = tT.dimensions
          dimOrder = order
          input = i
        case _ =>
      }

      // Reshape address counter
      val addrBitWidth = t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[NamedTupleTypeT].namedTypes.last._2.bitWidth
      val currentRange = math.pow(2, addrBitWidth.ae.evalInt).toInt
      val requiredBitWidth = transposedDims.map(_.ae.evalInt).product
      val inverseDimOrder = RulesHelper.invertDimOrder(dimOrder)
      val addrCounter = if(currentRange <= requiredBitWidth) {
        val tmpCounter = TypeChecker.check(TransposeNDOrderedStream(CounterInteger(0, 1, transposedDims, Some(IntType(addrBitWidth.ae.evalInt + 1))), inverseDimOrder))
        MapOrderedStream(
          transposedDims.length,
          {
            val param = ParamDef(tmpCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
            ArchLambda(
              param,
              TruncInteger(ParamUse(param), addrBitWidth)
            )
          }, tmpCounter
        )
      } else {
        TransposeNDOrderedStream(CounterInteger(0, 1, transposedDims, Some(IntType(addrBitWidth))), inverseDimOrder)
      }

      Zip2OrderedStream(Tuple2(JoinStreamAll(input), JoinStreamAll(addrCounter)))
  })

  def limitedConvertTransposeNDIntoPermute: Rule = Rule("limitedConvertTransposeNDIntoPermute", {
    case TransposeNDOrderedStream(input, dimOrder, _) if dimOrder.length == 4 && dimOrder(1).ae.evalInt == 2 && dimOrder(2).ae.evalInt == 1 && dimOrder(3).ae.evalInt == 3 && dimOrder(0).ae.evalInt == 0 =>
      val inputAsVec =  TypeChecker.check(MapOrderedStream(OrderedStreamToVector.asFunction(), MapOrderedStream(2, OrderedStreamToVector.asFunction(), MapOrderedStream(3, OrderedStreamToVector.asFunction(), input))))
      val transposed = MapOrderedStream({
        val param = ParamDef(inputAsVec.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          Transpose(ParamUse(param))
        )
      }, inputAsVec)
      MapOrderedStream(3, VectorToOrderedStream.asFunction(), MapOrderedStream(2, VectorToOrderedStream.asFunction(), MapOrderedStream(VectorToOrderedStream.asFunction(), transposed)))

    case TransposeNDOrderedStream(input, dimOrder, t) if dimOrder.length == 3 && dimOrder(1).ae.evalInt == 0 && dimOrder(0).ae.evalInt == 1 =>
      val inputAsVec =  TypeChecker.check(MapOrderedStream(OrderedStreamToVector.asFunction(), MapOrderedStream(2, OrderedStreamToVector.asFunction(), input)))
      val transposed = MapOrderedStream({
        val param = ParamDef(inputAsVec.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          Transpose(ParamUse(param))
        )
      }, inputAsVec)
      MapOrderedStream(2, VectorToOrderedStream.asFunction(), MapOrderedStream(VectorToOrderedStream.asFunction(), transposed))
  })

  def ConvertTransposeNDIntoPermute: Rule = Rule("ConvertTransposeNDIntoPermute", {
    // Convert TransposeND into PermuteVec because there is no hardware template for it.
    case TransposeNDOrderedStream(input, dimOrder, t: OrderedStreamTypeT) =>
      val inputDims = input.t.asInstanceOf[OrderedStreamTypeT].dimensions
      val outputDims = t.dimensions
      val stepSizePerDim = inputDims.map(_.ae.evalInt).scanLeft(1)(_*_).map(e => ArithType(e)).dropRight(1)
      val v = ArithTypeVar()
      val lambdaFunBody = {
        // Calculate coordinates from index and dimensions
        val newDimVar = stepSizePerDim.zip(inputDims).map(e => v / e._1.ae.evalInt % e._2.ae.evalInt)
        // Reorder dimensions
        val reorderedDimVars = dimOrder.map(_.ae.evalInt).map(e => newDimVar(e))
        // Calculate new index after transposition
        val newIndex = outputDims
          .scanLeft(1)(_ * _.ae.evalInt)
          .zip(reorderedDimVars)
          .map(e => e._1 * e._2)
          .foldLeft[ArithExpr](0)(_ + _)
        newIndex
      }
      val vecJoinAll = TypeChecker.check(OrderedStreamToVector(JoinStreamAll(input)))
      val permuteAll = TypeChecker.check(VectorToOrderedStream(PermuteVector(vecJoinAll, ArithLambdaType(v, lambdaFunBody))))
      SplitStreamAll(permuteAll, outputDims.dropRight(1).reverse)
  })
}
