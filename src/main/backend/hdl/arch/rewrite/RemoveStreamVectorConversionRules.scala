package backend.hdl.arch.rewrite

import backend.hdl.VectorTypeT
import backend.hdl.arch.{ArchLambda, JoinOrderedStream, JoinVector, MapOrderedStream, OrderedStreamToVector, SplitVector, VectorToOrderedStream}
import core.ParamUse
import core.rewrite.{Rule, RulesT}

object RemoveStreamVectorConversionRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      removeConversion1,
      removeConversion2
    )

  private def removeConversion1: Rule = Rule("removeConversion1", {
    case OrderedStreamToVector(VectorToOrderedStream(input, _), _) => input
    case VectorToOrderedStream(OrderedStreamToVector(input, _), _) => input
  })

  private def removeConversion2: Rule = Rule("removeConversion2", {
    case OrderedStreamToVector(JoinOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), _), _)
      if p1.id == p2.id =>
      JoinVector(OrderedStreamToVector(input))
  })

  // TODO make private
  def removeConversion3: Rule = Rule("removeConversion3", {
    case MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), MapOrderedStream(ArchLambda(p3, OrderedStreamToVector(ParamUse(p4), _), _), input, _), _) if
      p1.id == p2.id && p3.id == p4.id => input
  })

  // TODO make private and move to CleanupRules
  def removeStm2VecSize1: Rule = Rule("removeStm2VecSize1", {
    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), t: VectorTypeT), _), input, _) if
      p1.id == p2.id && t.len.ae.evalInt == 1 && t.et.isInstanceOf[VectorTypeT] =>
      MapOrderedStream(SplitVector.asFunction(Seq(None), Seq(t.et.asInstanceOf[VectorTypeT].len)), JoinOrderedStream(input))
  })
}
