package backend.hdl.arch.rewrite

import backend.hdl.{IntTypeT, OrderedStreamTypeT}
import backend.hdl.arch._
import backend.hdl.arch.mem.Read
import core.rewrite.{Rule, RulesT}
import core._
import core.util.IRDotGraph

object MoveUpJoinStreamRules extends RulesT {

  // TODO include moveIntoFold (will break other rewrites)
  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFission,
      skipExpr,
      mapFusion,
      skipSplit,
      reorderJoins
    )

  private def mapFission: Rule = Rule("moveUpJoinStream_mapFission", {
    // map fission
    // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
    case MapOrderedStream(ArchLambda(origParam, JoinOrderedStream(fun, _), _), input, _) if !RulesHelper.isParamUse(fun) && {
      var exprFound = false
      fun match {
        case JoinOrderedStream(_, _) =>
          exprFound = true
        // prevent loops
        case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), _, _)
          if p1.id == p4.id && p2.id == p3.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      MapOrderedStream(
        JoinOrderedStream.asFunction(),
        MapOrderedStream(ArchLambda(origParam, fun), input)
      )

    // MapNDFission
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(joinFun: LambdaT, inputInner, _), _), inputOuter, _)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(joinFun)
        innerMostBody.body match {
          case JoinOrderedStream(ParamUse(p2), _) if p2.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p2) if p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              joinFun,
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
  })

  private def skipExpr: Rule = Rule("moveUpJoinStream_skipExpr", {
    case MapOrderedStream(fun: LambdaT, JoinOrderedStream(input, _), _) if {
      var exprFound = false
      fun match {
        case ArchLambda(_, Read(_, _, _), _) =>
          exprFound = true
        case ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } && {
      // Avoid looping with OStm2UStm
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToUnorderedStream(ParamUse(pd), _) if pd.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      JoinOrderedStream(
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(
              p,
              MapOrderedStream(
                fun,
                ParamUse(p)
              )
            )
          },
          input
        )
      )
    case Conversion(JoinOrderedStream(input, _), t: OrderedStreamTypeT) =>
      JoinOrderedStream(
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(p,
              MapOrderedStream(
                {
                  val pp = ParamDef()
                  ArchLambda(pp,
                    Conversion(ParamUse(pp), t.et)
                  )
                },
                ParamUse(p)
              )
            )
          },
          input
        )
      )
    case RepeatHidden(MapOrderedStream(fun @ ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), input, _), repetitions, _) if p1.id == p2.id =>
      MapOrderedStream(fun, RepeatHidden(input, repetitions))
    case Marker(MapOrderedStream(fun @ ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), input, _), text) if p1.id == p2.id =>
      MapOrderedStream(fun, Marker(input, text))
    case Marker(JoinOrderedStream(input, _), text) =>
      JoinOrderedStream(Marker(input, text))
    case Marker(MapOrderedStream(joinFun: LambdaT, input, _), text)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(joinFun)
        innerMostBody.body match {
          case JoinOrderedStream(ParamUse(p), _) if p.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream(joinFun, Marker(input,  text))
    case Zip2OrderedStream(Tuple2(JoinOrderedStream(in1, _), JoinOrderedStream(in2, _), _), _)
      if {
        val in1tc = TypeChecker.checkIfUnknown(in1, in1.t)
        val in2tc = TypeChecker.checkIfUnknown(in2, in2.t)
        in1tc.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt == in2tc.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt
      } =>
      JoinOrderedStream(
        MapOrderedStream(
          Zip2OrderedStream.asFunction(),
          Zip2OrderedStream(Tuple2(in1, in2))
        )
      )
    case MapOrderedStream(ArchLambda(p1, Zip2OrderedStream(ParamUse(p2), _), _), Zip2OrderedStream(Tuple2(
    MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _), _), input1, _),
    MapOrderedStream(ArchLambda(p5, JoinOrderedStream(ParamUse(p6), _), _), input2, _), _), _), _)
    if p1.id == p2.id && p3.id == p4.id && p5.id == p6.id &&
       p3.t.asInstanceOf[OrderedStreamTypeT].len.ae == p5.t.asInstanceOf[OrderedStreamTypeT].len.ae =>
      MapOrderedStream(
        JoinOrderedStream.asFunction(),
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(p,
              MapOrderedStream(
                Zip2OrderedStream.asFunction(),
                ParamUse(p)
              )
            )
          },
          MapOrderedStream(
            Zip2OrderedStream.asFunction(),
            Zip2OrderedStream(Tuple2(input1, input2))
          )
        )
      )
  })

  private def mapFusion: Rule = Rule("moveUpJoinStream_mapFusion", {
    // map fusion
    case MapOrderedStream(ArchLambda(pd, body, _), MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), input, _), _)
      if p1.id == p2.id &&
        (body match {
            case JoinOrderedStream(ParamUse(p3), _) if pd.id == p3.id => false
            case OrderedStreamToUnorderedStream(ParamUse(p3), _) if pd.id == p3.id => false
            // prevent loops
            case MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _)
              if pd.id == p3.id && p1.id == p2.id => false
            case MapOrderedStream(ArchLambda(p1, OrderedStreamToUnorderedStream(ParamUse(p2), _), _), ParamUse(p3), _)
              if pd.id == p3.id && p1.id == p2.id => false
            case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id => JoinOrderedStream(ParamUse(newParam))
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )

    // map map fusion
    case MapOrderedStream(ArchLambda(pd, body, _), MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), _)
      if p2.id == p3.id && p1.id == p4.id &&
        (body match {
          // prevent loops with map map fission rule
          case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), ParamUse(p5), _)
            if pd.id == p5.id && p1.id == p4.id && p2.id == p3.id => false
          case JoinOrderedStream(ParamUse(p), _) if pd.id == p.id => false // prevent loops with join stream rule
          case OrderedStreamToUnorderedStream(ParamUse(p), _) if pd.id == p.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id =>
                  MapOrderedStream(
                    JoinOrderedStream.asFunction(),
                    ParamUse(newParam)
                  )
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
  })

  // TODO make private
  def mapFusionMapParam: Rule = Rule("moveUpJoinStreamRules_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        joinInnerMost.body match {
          case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit{
          case ParamUse(p4) if p1.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        joinInnerMost.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
            exprFound = true
          case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def skipSplit: Rule = Rule("moveUpJoinStream_skipSplit", {
    case SplitOrderedStream(JoinOrderedStream(input, _), chunkSizeAe,  _) if {
      val innerLen = input.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt
      val chunkSize = chunkSizeAe.ae.evalInt
      innerLen == chunkSize || innerLen % chunkSize == 0 || chunkSize % innerLen == 0
    } =>
      val innerLen = input.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt
      val chunkSize = chunkSizeAe.ae.evalInt
      if (innerLen == chunkSize) {
        input
      } else if (chunkSize < innerLen) {
        JoinOrderedStream(
          MapOrderedStream(
            SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)),
            input
          )
        )
      } else { // chunkSize > innerLen
        MapOrderedStream(
          JoinOrderedStream.asFunction(),
          SplitOrderedStream(
            input,
            chunkSize / innerLen
          )
        )
      }
  })

  def skipMapNDSplit: Rule = Rule("moveUpJoinStream_skipMapNDSplit", {
    case MapOrderedStream(splitFun, JoinOrderedStream(input, _), _) if {
      var exprFound = false
      val inputtc = TypeChecker.checkIfUnknown(input, input.t)
      val innerDims = inputtc.t.asInstanceOf[OrderedStreamTypeT].dimensions
      val innermostSplitFun = RulesHelper.getCurrentOrInnerMostLambda(splitFun.asInstanceOf[LambdaT])
      val splitFunLevels = RulesHelper.getMapNDLevels(splitFun.asInstanceOf[LambdaT])
      val selectAxis = innerDims.length - 1 - (splitFunLevels + 1)
      val innerLen = innerDims(selectAxis).ae.evalInt
      innermostSplitFun.visit{
        case SplitOrderedStream(ParamUse(p1), chunkSizeAe, _) if p1.id == innermostSplitFun.param.id && {
          val chunkSize = chunkSizeAe.ae.evalInt
          innerLen == chunkSize || innerLen % chunkSize == 0 || chunkSize % innerLen == 0
        } =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innermostSplitFun = RulesHelper.getCurrentOrInnerMostLambda(splitFun.asInstanceOf[LambdaT])
      val splitFunLevels = RulesHelper.getMapNDLevels(splitFun.asInstanceOf[LambdaT])
      JoinOrderedStream(MapOrderedStream(splitFunLevels + 1, innermostSplitFun, input))
  })

  def moveIntoFold: Rule = Rule("moveUpJoinStream_moveIntoFold", {
    // TODO just use 'fun', with reset types. because the types of the two instantiated functions will be different
    case FoldOrderedStream(fun @ ArchLambda(p1, ArchLambda(p2, AddInt(Tuple2(ParamUse(p3), ParamUse(p4), _), _), _), _), JoinOrderedStream(input, _), resTy)
      if p1.id == p3.id && p2.id == p4.id || p1.id == p4.id && p2.id == p3.id =>
      val reduction = TypeChecker.check(FoldOrderedStream(AddInt2.asFunction(),
        MapOrderedStream(
          {
            val p = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              p,
              FoldOrderedStream(AddInt2.asFunction(),
                ParamUse(p)
              )
            )
          },
          input
        )
      ))

      // Resize the integer if necessary
      (resTy, reduction.t) match {
        case (a: IntTypeT, b: IntTypeT) if a.bitWidth.ae.evalInt != b.bitWidth.ae.evalInt =>
          ResizeInteger(reduction, a.bitWidth.ae.evalInt)
        case _ => reduction
      }
  })

  def reorderJoins: Rule = Rule("moveUpJoinStream_reorderJoins", {
    case JoinOrderedStream(MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), input,  _), _) if p1.id == p2.id =>
      JoinOrderedStream(JoinOrderedStream(input))
    case JoinOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _), _), ParamUse(p4),  _), _), input,  _), _) if p1.id == p4.id && p2.id == p3.id =>
      MapOrderedStream(JoinOrderedStream.asFunction(), JoinOrderedStream(input))
  })

}
