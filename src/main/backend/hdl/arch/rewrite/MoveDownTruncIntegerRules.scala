package backend.hdl.arch.rewrite

import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core._

object MoveDownTruncIntegerRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      skipJoinStream,
      skipSplitStream,
      skipTransposeStream
    )

  def skipJoinStream: Rule = Rule("moveDownTruncInteger_skipJoinStream", {
    case MapOrderedStream(conversionFun, JoinOrderedStream(input, _), _) if {
      var exprFound = false
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
      innermostFun.body match {
        case Conversion(DropVector(Conversion(ParamUse(p1), t1), drop1, drop2, _), t2) if p1.id == innermostFun.param.id =>
          exprFound = true
        case Conversion(ParamUse(p1), t) if p1.id == innermostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
      val conversionFunLevels = RulesHelper.getMapNDLevels(conversionFun.asInstanceOf[LambdaT])
      val newConversionFunLevels = conversionFunLevels + 1
      val convertedInput = MapOrderedStream(newConversionFunLevels, innermostFun, input)
      JoinOrderedStream(convertedInput)
  })

  def skipSplitStream: Rule = Rule("moveDownTruncInteger_skipSplitStream", {
    case MapOrderedStream(conversionFun, SplitOrderedStream(input, chunkSize, _), _) if {
      var exprFound = false
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
      innermostFun.body match {
        case Conversion(DropVector(Conversion(ParamUse(p1), t1), drop1, drop2, _), t2) if p1.id == innermostFun.param.id =>
          exprFound = true
        case Conversion(ParamUse(p1), t) if p1.id == innermostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
      val conversionFunLevels = RulesHelper.getMapNDLevels(conversionFun.asInstanceOf[LambdaT])
      val newConversionFunLevels = conversionFunLevels - 1
      val convertedInput = MapOrderedStream(newConversionFunLevels, innermostFun, input)
      SplitOrderedStream(convertedInput, chunkSize)
  })

  def skipTransposeStream: Rule = Rule("moveDownTruncInteger_skipTransposeStream", {
    case MapOrderedStream(conversionFun, TransposeNDOrderedStream(input, dimOrder, _), _) if {
      var exprFound = false
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
      innermostFun.body match {
        case Conversion(DropVector(Conversion(ParamUse(p1), t1), drop1, drop2, _), t2) if p1.id == innermostFun.param.id =>
          exprFound = true
        case Conversion(ParamUse(p1), t) if p1.id == innermostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
      val conversionFunLevels = RulesHelper.getMapNDLevels(conversionFun.asInstanceOf[LambdaT])
      val convertedInput = MapOrderedStream(conversionFunLevels, innermostFun, input)
      TransposeNDOrderedStream(convertedInput, dimOrder)
  })

  def mapFusion: Rule = Rule("moveDownTruncInteger_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _), MapOrderedStream(ArchLambda(p2, body2, _),
    input, _), _)
      if {
        var exprFound = false
        body1.visit {
          case Conversion(DropVector(Conversion(ParamUse(p2), t1), drop1, drop2, _), _) if p1.id == p2.id =>
            exprFound = true
          case Conversion(ParamUse(p2), _) if p1.id == p2.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit {
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
    // map map fusion
    case MapOrderedStream(ArchLambda(p1, MapVector(conversionFun, ParamUse(p2), _), _), MapOrderedStream(ArchLambda(param, body, _), input, _), _) if
      p1.id == p2.id && {
        var exprFound = false
        val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
        innermostFun.body match {
          case Conversion(DropVector(Conversion(ParamUse(p1), t1), drop1, drop2, _), t2) if p1.id == innermostFun.param.id =>
            exprFound = true
          case Conversion(ParamUse(p1), t) if p1.id == innermostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream(
        {
          ArchLambda(
            param,
            MapVector(conversionFun, body)
          )
        },
        input
      )
  })

  def skipStm2Vec: Rule = Rule("moveDownTruncInteger_skipStm2Vec", {
    case MapVector(ArchLambda(p1, MapVector(conversionFun, ParamUse(p2), _), _), OrderedStreamToVector(input, _), _) if p1.id == p2.id && {
      var exprFound = false
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(conversionFun.asInstanceOf[LambdaT])
      innermostFun.body match {
        case Conversion(DropVector(Conversion(ParamUse(p1), t1), drop1, drop2, _), t2) if p1.id == innermostFun.param.id =>
          exprFound = true
        case Conversion(ParamUse(p1), t) if p1.id == innermostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      OrderedStreamToVector(MapOrderedStream(ArchLambda(p1, MapVector(conversionFun, ParamUse(p2))), input))
  })
}
