package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.{ArchLambda, JoinStreamAll, OrderedStreamToVector, SplitStreamAll, VectorToOrderedStream}
import core.{Expr, FunctionCall, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object MultipleParamUsesRules extends RulesT {
  /**
    * Distributor template does not support multiple ParamUses with stream type.
    * To fix this, we convert the paramUses mush be in vector type.
    */

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    vectorizeParams
  )

  def vectorizeParams: Rule = Rule("MultipleParamUses_vectorizeParams", {
    case FunctionCall(ArchLambda(p1, body, _), input, _) if {
      var paramUses: Int = 0
      body.visit{
        case ParamUse(p2) if p1.id == p2.id => paramUses = paramUses + 1
        case _ =>
      }
      paramUses >= 2 && p1.t.isInstanceOf[OrderedStreamTypeT]
    } =>
      val paramDims = p1.t.asInstanceOf[OrderedStreamTypeT].dimensions
      val vecInput = TypeChecker.check(OrderedStreamToVector(JoinStreamAll(input)))

      val newParam = ParamDef(vecInput.t)
      val newBody = body.visitAndRebuild{
        case ParamUse(p2) if p1.id == p2.id => SplitStreamAll(VectorToOrderedStream(ParamUse(newParam)), paramDims.dropRight(1).reverse)
        case e => e
      }.asInstanceOf[Expr]

      FunctionCall(ArchLambda(newParam, newBody), vecInput)
  })
}
