package backend.hdl.arch.rewrite

import backend.hdl.{NamedTupleTypeT, VectorTypeT}
import backend.hdl.arch.{ArchLambda, ConcatVector, ConstantValue, MapVector, MulInt, Tuple2, VectorGenerator}
import core.ParamUse
import core.rewrite.Rule

/**
  * TODO merge into ExtractMul2AddRules
  */
object PadMul2AddRules {
  def padMul2Add: Rule = Rule("padMul2Add", {
    case ConcatVector(Tuple2(MapVector(ArchLambda(p1, MulInt(ParamUse(p2), _), _), input, vecT: VectorTypeT), VectorGenerator(ConstantValue(value, _), size, _), _), _) if
      p1.id == p2.id && size.ae.evalInt == 1 && value.ae.evalInt == 0 && vecT.len.ae.evalInt % 2 == 1 =>
      val inTupElement1Type = input.t.asInstanceOf[VectorTypeT].et.asInstanceOf[NamedTupleTypeT].namedTypes.head._2
      val inTupElement2Type = input.t.asInstanceOf[VectorTypeT].et.asInstanceOf[NamedTupleTypeT].namedTypes.last._2
      val padTuple = Tuple2(ConstantValue(value, Some(inTupElement1Type)), ConstantValue(value, Some(inTupElement2Type)))
      val newInput = ConcatVector(Tuple2(input, VectorGenerator(padTuple, size)))
      MapVector(MulInt.asFunction(), newInput)
  })
}
