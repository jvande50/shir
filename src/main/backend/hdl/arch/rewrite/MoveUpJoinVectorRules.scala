package backend.hdl.arch.rewrite

import backend.hdl.arch.{ArchLambda, JoinVector, MapVector}
import core.rewrite.Rule
import core.{ParamUse}

object MoveUpJoinVectorRules {
  def reorderJoins: Rule = Rule("moveUpJoinStream_reorderJoins", {
    case JoinVector(MapVector(ArchLambda(p1, JoinVector(ParamUse(p2), _), _), input, _), _) if p1.id == p2.id =>
      JoinVector(JoinVector(input))
  })
}
