package backend.hdl.arch.rewrite

import backend.hdl.arch.{ArchLambda, JoinOrderedStream, MapOrderedStream, OrderedStreamToUnorderedStream}
import core.{LambdaT, ParamUse}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object MoveUpOrderedToUnorderedStreamRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      skipJoin,
      fissionND
    ) ++ Seq(CleanupRules.moveUpMapOrderedToUnorderedStream, CleanupRules.replaceJoinUnorderedStream)

  def skipJoin: Rule = Rule("moveUpOrderedToUnorderedStream_skipJoin", {
    case JoinOrderedStream(MapOrderedStream(fun: LambdaT, input, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToUnorderedStream(ParamUse(pd), _) if pd.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && RulesHelper.getMapNDLevels(fun) > 1 =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      MapOrderedStream(levels - 1, innerMostFun, JoinOrderedStream(input))
  })

  def fissionND: Rule = Rule("moveUpOrderedToUnorderedStream_fissionND", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, innerInput, _), _), input, _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToUnorderedStream(ParamUse(pd), _) if pd.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && RulesHelper.containsParam(innerInput, p1) && !RulesHelper.isParamUse(innerInput) &&
      RulesHelper.getMapNDLevels(fun) > 1=>
      val levels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      MapOrderedStream(levels + 1, innerMostFun, MapOrderedStream(ArchLambda(p1, innerInput), input))
  })
}
