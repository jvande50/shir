package backend.hdl.arch.rewrite.tiling

import algo.{Add, Add2, AlgoLambda, Fold, JoinAll, Select2, SeqTypeT, Split, SplitAll, TransposeND, Tuple2}
import backend.hdl.arch.rewrite.RulesHelper
import core.{Expr, Lambda, LambdaT, ParamDef, ParamUse, TypeChecker}
import core.rewrite.Rule

object FoldTilingRules {
  def tilingInsideFold(tileSize: Int = 128): Rule = Rule("tilingInsideFold", {
    case algo.Map(fun1: LambdaT, input1, _) if {
      var exprFound = false
      val fun1InnerMostLambda = RulesHelper.getCurrentOrInnerMostLambda(fun1)
      fun1InnerMostLambda.visit{
        case algo.Map(fun2: LambdaT, input2, _) if {
          var exprFound = false
          val fun2InnerMostLambda = RulesHelper.getCurrentOrInnerMostLambda(fun2)
          fun2InnerMostLambda.visit{
            // Only accepts Fold1D with Add
            case Fold(Lambda(_, Lambda(_, Add(_, _), _), _), tupleIn, _) if {
              var exprFound = false
              tupleIn.visit{
                case algo.Zip2(Tuple2(ParamUse(fun1InnerMostLambda.param), ParamUse(fun2InnerMostLambda.param), _), t: SeqTypeT) if
                  t.len.ae.evalInt > tileSize && t.len.ae.evalInt % tileSize == 0 &&
                    !fun1InnerMostLambda.param.t.asInstanceOf[SeqTypeT].et.isInstanceOf[SeqTypeT] &&
                    !fun2InnerMostLambda.param.t.asInstanceOf[SeqTypeT].et.isInstanceOf[SeqTypeT]=>
                  exprFound = true
                case algo.Zip2(Tuple2(ParamUse(fun2InnerMostLambda.param), ParamUse(fun1InnerMostLambda.param), _), t: SeqTypeT) if
                  t.len.ae.evalInt > tileSize && t.len.ae.evalInt % tileSize == 0 &&
                    !fun1InnerMostLambda.param.t.asInstanceOf[SeqTypeT].et.isInstanceOf[SeqTypeT] &&
                    !fun2InnerMostLambda.param.t.asInstanceOf[SeqTypeT].et.isInstanceOf[SeqTypeT]=>
                  exprFound = true
                case _ =>
              }
              exprFound
            } =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          exprFound = true
        case _ =>
      }
      exprFound
    }
    =>
      // Get input2 and fun2
      val fun1InnerMostLambda = RulesHelper.getCurrentOrInnerMostLambda(fun1)
      var input2 = input1
      var fun2 = fun1
      fun1InnerMostLambda.visit{
        case algo.Map(f2: LambdaT, i2, _) if
          RulesHelper.containsParam(f2.body, fun1.param) =>
          input2 = i2
          fun2 = f2
        case _ =>
      }

      val fun2InnerMostLambda = RulesHelper.getCurrentOrInnerMostLambda(fun2)
      val fun1Levels = RulesHelper.getMapNDLevels(fun1)
      val fun2Levels = RulesHelper.getMapNDLevels(fun2)

      // Get Fold Function
      val foldInput = {
        var exprFound = fun2InnerMostLambda.body
        fun2InnerMostLambda.visit{
          case Fold(f, tupleIn, _) if {
            var exprFound = false
            tupleIn.visit{
              case algo.Zip2(Tuple2(ParamUse(fun1InnerMostLambda.param), ParamUse(fun2InnerMostLambda.param), _), t) =>
                exprFound = true
              case algo.Zip2(Tuple2(ParamUse(fun2InnerMostLambda.param), ParamUse(fun1InnerMostLambda.param), _), t)  =>
                exprFound = true
              case _ =>
            }
            exprFound
          } =>
            exprFound = tupleIn
          case _ =>
        }
        exprFound
      }

      // Split inputs
      val splitInput1 = algo.Map(fun1Levels, Split.asFunction(Seq(None), Seq(tileSize)), input1)
      val splitInput2 = algo.Map(fun2Levels, Split.asFunction(Seq(None), Seq(tileSize)), input2)
      val splitInput1tc = TypeChecker.checkIfUnknown(splitInput1, splitInput1.t)
      val splitInput2tc = TypeChecker.checkIfUnknown(splitInput2, splitInput2.t)

      // Calculate transposition for inputs
      val splitInput1Levels = splitInput1tc.t.asInstanceOf[SeqTypeT].dimensions.length
      val splitInput1DimOrderHead = RulesHelper.generateCountSeq(0, splitInput1Levels - fun1Levels - 1)
      val splitInput1DimOrderMid = RulesHelper.generateCountSeq(splitInput1Levels- fun1Levels - 1, 1)
      val splitInput1DimOrderTail = RulesHelper.generateCountSeq(splitInput1Levels - fun1Levels, fun1Levels)
      val input1TransposeDimOrder = splitInput1DimOrderHead ++ splitInput1DimOrderTail ++ splitInput1DimOrderMid
      val splitInput2Levels = splitInput2tc.t.asInstanceOf[SeqTypeT].dimensions.length
      val splitInput2DimOrderHead = RulesHelper.generateCountSeq(0, splitInput2Levels- fun2Levels - 1)
      val splitInput2DimOrderMid = RulesHelper.generateCountSeq(splitInput2Levels - fun2Levels - 1, 1)
      val splitInput2DimOrderTail = RulesHelper.generateCountSeq(splitInput2Levels- fun2Levels, fun2Levels)
      val input2TransposeDimOrder = splitInput2DimOrderHead ++ splitInput2DimOrderTail ++ splitInput2DimOrderMid
      val transposeInput1 = TransposeND(splitInput1tc, input1TransposeDimOrder)
      val transposeInput2 = TransposeND(splitInput2tc, input2TransposeDimOrder)

      // Build NewFold
      val transposeInput1InnerParam = ParamDef()
      val transposeInput2InnerParam = ParamDef()

      val newFoldInput = foldInput.visitAndRebuild{
        case algo.Map(f, algo.Zip2(Tuple2(ParamUse(p1), ParamUse(p2), _), _), _) if
          p1.id == fun1InnerMostLambda.param.id && p2.id == fun2InnerMostLambda.param.id =>
          algo.Map(f, algo.Zip2(Tuple2(ParamUse(transposeInput1InnerParam), ParamUse(transposeInput2InnerParam))))
        case algo.Map(f, algo.Zip2(Tuple2(ParamUse(p1), ParamUse(p2), _), _), _) if
          p2.id == fun1InnerMostLambda.param.id && p1.id == fun2InnerMostLambda.param.id =>
          algo.Map(f, algo.Zip2(Tuple2(ParamUse(transposeInput2InnerParam), ParamUse(transposeInput1InnerParam))))
        case e => e
      }.asInstanceOf[Expr]
      val newFold = Fold(Add2.asFunction(), newFoldInput)
      // Compute partial sum
      val zipTransposeInputs = algo.Zip2(Tuple2(transposeInput1, transposeInput2))
      val partialSumStm = algo.Map(
        {
          val param1 = ParamDef()
          val param1_1 = Select2(ParamUse(param1), 0)
          val param1_2 = Select2(ParamUse(param1), 1)
          AlgoLambda(
            param1,
            {
              val newInnerFun = AlgoLambda(transposeInput2InnerParam, newFold)
              val innerMap = algo.Map(fun1Levels, newInnerFun, param1_2)
              val newOuterFun = AlgoLambda(transposeInput1InnerParam, innerMap)
              val outerMap = algo.Map(fun2Levels, newOuterFun, param1_1)
              outerMap
            }
          )
        }, zipTransposeInputs
      )
      val partialSumStmtc = TypeChecker.checkIfUnknown(partialSumStm, partialSumStm.t)
      // Calculate partial sum
      val flattenPartialSumStm = algo.Map(
        {
          val param = ParamDef(partialSumStmtc.t.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param,
            JoinAll(ParamUse(param))
          )
        }, partialSumStmtc
      )

      val finalSum = Fold(
        {
          val p1 = ParamDef()
          val p2 = ParamDef()
          AlgoLambda(
            Seq(p1, p2),
            algo.Map(
              Add.asFunction(),
              algo.Zip2(Tuple2(ParamUse(p1), ParamUse(p2)))
            )
          )
        }, flattenPartialSumStm
      )
      val splitDims = partialSumStmtc.t.asInstanceOf[SeqTypeT].dimensions
      // Drop the dim of partial sum and the outermost dim
      val deflattenFinalSum = SplitAll(finalSum, splitDims.dropRight(2))
      deflattenFinalSum
  })
}
