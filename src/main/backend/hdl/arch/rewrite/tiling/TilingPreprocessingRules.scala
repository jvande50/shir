package backend.hdl.arch.rewrite.tiling

import algo.{AlgoLambda, SeqTypeT}
import backend.hdl.arch.rewrite.RulesHelper
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import core.rewrite.Rule

object TilingPreprocessingRules {

  def moveDownInnerMapBody: Rule = Rule("moveDownInnerMapBody", {
    case algo.Map(AlgoLambda(p1, algo.Map(AlgoLambda(p2, fun, _), body, _), _), input, _)
      if RulesHelper.containsParam(body, p1) &&
        RulesHelper.containsParam(fun, p2) &&
        !RulesHelper.matchesParam(body, p1) =>
      val innerMap = algo.Map(
        {
          val param = ParamDef()
          AlgoLambda(
            param,
            body.visitAndRebuild{
              case ParamUse(usedParam) if usedParam.id == p1.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, input
      )
      val innerMaptc = TypeChecker.checkIfUnknown(innerMap, innerMap.t)
      algo.Map(
        {
          val param = ParamDef(innerMaptc.t.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param,
            algo.Map(
              {
                val param2 = ParamDef()
                AlgoLambda(
                  param2,
                  fun.visitAndRebuild{
                    case ParamUse(usedParam) if usedParam.id == p2.id =>
                      ParamUse(param2)
                    case e => e
                  }.asInstanceOf[Expr]
                )
              }, ParamUse(param)
            )
          )
        }, innerMaptc
      )
  })
}
