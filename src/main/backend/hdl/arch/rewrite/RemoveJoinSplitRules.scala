package backend.hdl.arch.rewrite

import backend.hdl.arch.{ArchLambda, JoinOrderedStream, MapOrderedStream, SplitOrderedStream}
import core.rewrite.{Rule, RulesT}

object RemoveJoinSplitRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      removeJoinSplit
      // see skipSplit in MoveUpJoinStreamRules as well
    )

  def removeJoinSplit: Rule = Rule("removeJoinSplit", {
    case JoinOrderedStream(SplitOrderedStream(input, _, _), _) =>
      input
  })

  def simplifyJoinJoinMapSplit: Rule = Rule("simplifyJoinJoinMapSplit", {
    case JoinOrderedStream(JoinOrderedStream(MapOrderedStream(ArchLambda(p1, SplitOrderedStream(body, _, _), _), input, _), _), _) if RulesHelper.containsParam(body, p1)=>
      JoinOrderedStream(MapOrderedStream(ArchLambda(p1, body), input))
  })
}
