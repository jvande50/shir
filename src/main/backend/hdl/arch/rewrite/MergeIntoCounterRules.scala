package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core._

object MergeIntoCounterRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(mergeRepeat, mergeMapRepeat, mergeMapMapRepeat, mergeSplitStream, mergeMapSplitStream, splitRepeatSplitStream)

  def mergeRepeat: Rule = Rule("mergeRepeat", {
    case Repeat(CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), outerRep, _) =>
      CounterInteger(start, increment, loop, dimensions :+ outerRep, repetitions :+ ArithType(1), Some(t.leafType))
  })

  def mergeMapRepeat: Rule = Rule("mergeMapRepeat", {
    case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), mapRep, _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p2.id =>
      CounterInteger(start, increment, loop, dimensions.init :+ mapRep :+ dimensions.last, repetitions.init :+ ArithType(1) :+ repetitions.last, Some(t.leafType))
  })

  def mergeMapMapRepeat: Rule = Rule("mergeMapMapRepeat", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Repeat(ParamUse(p3), mapRep, _), _), ParamUse(p4), _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p4.id && p2.id == p3.id =>
      CounterInteger(start, increment, loop, dimensions.init.init ++ Seq(mapRep) ++ dimensions.takeRight(2), repetitions.init.init ++ Seq(ArithType(1)) ++ repetitions.takeRight(2), Some(t.leafType))
  })

  def mergeSplitStream: Rule = Rule("mergeSplitStream", {
    case SplitOrderedStream(CounterInteger(start, increment, loop, dimensions, repetitions, _), chunkSize, t: OrderedStreamTypeT) =>
      CounterInteger(start, increment, loop, dimensions.init :+ chunkSize :+ ArithType(dimensions.last.ae / chunkSize.ae), repetitions :+ repetitions.last, Some(t.leafType))
  })

  def mergeMapSplitStream: Rule = Rule("mergeMapSplitStream", {
    case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), chunkSize, _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p2.id =>
      CounterInteger(start, increment, loop, dimensions.init.init :+ chunkSize :+ ArithType(dimensions.init.last.ae / chunkSize.ae) :+ dimensions.last, repetitions.init :+ repetitions.init.last :+ repetitions.last, Some(t.leafType))

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, SplitOrderedStream(ParamUse(p3), chunkSize, _), _), ParamUse(p4), _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p4.id && p2.id == p3.id =>
      CounterInteger(start, increment, loop, dimensions.init.init.init :+ chunkSize :+ ArithType(dimensions.init.init.last.ae / chunkSize.ae) :+ dimensions.init.last :+ dimensions.last, repetitions.init.init :+ repetitions.init.init.last :+ repetitions.init.last :+ repetitions.last, Some(t.leafType))
  })

  def splitRepeatSplitStream: Rule = Rule("splitRepeatSplitStream", {
    case MapOrderedStream(ArchLambda(param, body, _), input, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a SplitOrderedStream
      var splits: Int = 0
      var paramUses: Int = 0
      body.visit{
        case SplitOrderedStream(ParamUse(pd), _, _) if pd.id == param.id =>
          splits = splits + 1
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapSplitOnly: Boolean = body match {
        case SplitOrderedStream(ParamUse(_), _, _) => true
        case _ => false
      }
      !isMapSplitOnly && splits > 0 && splits == paramUses
    } =>
      var chunkSize: ArithTypeT = 0
      val newParam = ParamDef()
      val newBody = {
        body.visitAndRebuild{
          case SplitOrderedStream(ParamUse(pd), cs, _) if pd.id == param.id =>
            chunkSize = cs
            ParamUse(newParam)
          case e => e
        }.asInstanceOf[Expr]
      }
      MapOrderedStream(
        ArchLambda(
          newParam,
          newBody
        ),
        MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), input)
      )
  })

}
