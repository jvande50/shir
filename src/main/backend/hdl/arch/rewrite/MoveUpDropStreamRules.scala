package backend.hdl.arch.rewrite

import backend.hdl.arch.{ArchLambda, DropOrderedStream, MapOrderedStream}
import core.{Expr, ParamDef, ParamUse}
import core.rewrite.{Rule, RulesT}

object MoveUpDropStreamRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFission
    )

  def mapFission: Rule = Rule("moveUpDropStreamRules_mapFission", {
    // map fission
    // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
    case MapOrderedStream(ArchLambda(origParam, DropOrderedStream(fun, firstElements, lastElements, _), _), input, _) if !RulesHelper.isParamUse(fun) =>
      MapOrderedStream(
        DropOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements)),
        MapOrderedStream(ArchLambda(origParam, fun), input)
      )
  })

  def mapFusionMapParam: Rule = Rule("moveUpDropStreamRules_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val dropInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        dropInnerMost.body match {
          case DropOrderedStream(ParamUse(p3), _, _, _) if p3.id == dropInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit{
          case ParamUse(p4) if p1.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })
}
