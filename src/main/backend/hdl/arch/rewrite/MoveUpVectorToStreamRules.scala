package backend.hdl.arch.rewrite

import backend.hdl.BasicDataTypeT
import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph
import core.{Expr, LambdaT, Marker, ParamDef, ParamUse}

object MoveUpVectorToStreamRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFission,
      mapFusion,
      mapFusionMapParam,
      convertMapStreamToMapVector,
      skipExprs,
      skipTuple,
      skipSplitStream,
      mergeWithStreamToVector,
      skipDropJoinStream
    )

  def solveTranspose: Seq[Rule] =
    Seq(
      mapFission,
      mapFusion,
      convertMapStreamToMapVector,
      skipExprs,
      skipTuple,
      skipSplitStream,
      mergeWithStreamToVector,
      mapFusionMapParam
    )

  private def mergeWithStreamToVector: Rule = Rule("moveUpVectorToStream_mergeWithStreamToVector", {
    case
      MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _),
      MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _), _),
      MapOrderedStream(ArchLambda(p5, MapOrderedStream(ArchLambda(p6, VectorToOrderedStream(ParamUse(p7), _), _), ParamUse(p8), _), _), input, _), _), _)
      if p1.id == p2.id && p3.id == p4.id && p5.id == p8.id && p6.id == p7.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p,
            JoinVector(OrderedStreamToVector(ParamUse(p)))
          )
        },
        input
      )
  })

  private def mapFission: Rule = Rule("moveUpVectorToStream_mapFission", {
    // map fission
    case MapOrderedStream(ArchLambda(origParam, VectorToOrderedStream(fun, _), _), input, _)
      // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
      if !RulesHelper.isParamUse(fun) =>
      MapOrderedStream(
        VectorToOrderedStream.asFunction(),
        MapOrderedStream(ArchLambda(origParam, fun), input)
      )
    // map map fission
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), inputInner, _), _), inputOuter, _)
      if p2.id == p3.id &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p4) if p1.id == p4.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              VectorToOrderedStream.asFunction(),
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
    // map map map fission
    case MapOrderedStream(ArchLambda(p1,
          MapOrderedStream(ArchLambda(p2,
            MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _),
            inputInner, _), _),
          inputOuter, _)
      if p3.id == p4.id && p2.id == p5.id &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p6) if p1.id == p6.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(pOuter,
            MapOrderedStream(
              {
                val pInner = ParamDef()
                ArchLambda(pInner,
                  MapOrderedStream(
                    VectorToOrderedStream.asFunction(),
                    ParamUse(pInner)
                  )
                )
              },
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
    // MapNDFission
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(vecToStmFun: LambdaT, inputInner, _), _), inputOuter, _)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(vecToStmFun)
        innerMostBody.body match {
          case VectorToOrderedStream(ParamUse(p2), _) if p2.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p2) if p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              vecToStmFun,
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
  })

  private def mapFusion: Rule = Rule("moveUpVectorToStream_mapFusion", {
    // map fusion
    case MapOrderedStream(ArchLambda(pd, body, _), MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), _)
      if p1.id == p2.id &&
        (body match {
          // prevent loops with map map fission rule
          case MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case JoinOrderedStream(ParamUse(p), _) if pd.id == p.id => false
          case MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id => VectorToOrderedStream(ParamUse(newParam))
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
    // map map fusion
    case MapOrderedStream(ArchLambda(pd, body, _), MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), _)
      if p2.id == p3.id && p1.id == p4.id &&
        (body match {
          // prevent loops with map map fission rule
          case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), ParamUse(p5), _)
            if pd.id == p5.id && p1.id == p4.id && p2.id == p3.id => false
          case JoinOrderedStream(ParamUse(p), _) if pd.id == p.id => false // prevent loops with join stream rule
          case MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id =>
                  MapOrderedStream(
                    VectorToOrderedStream.asFunction(),
                    ParamUse(newParam)
                  )
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
    // map map map fusion
    case MapOrderedStream(ArchLambda(pd, body, _),
    MapOrderedStream(ArchLambda(p1,
    MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(
    ParamUse(p4), _), _),
    ParamUse(p5), _), _),
    ParamUse(p6), _), _), input, _), _)
      if p3.id == p4.id && p2.id == p5.id && p1.id == p6.id &&
        (body match {
          // prevent loops with map map map fission rule
          case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _)
            if pd.id == p7.id && p1.id == p6.id && p2.id == p5.id && p3.id == p4.id => false
          case JoinOrderedStream(ParamUse(p), _) if pd.id == p.id => false // prevent loops with join stream rule
          case MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _)
            if pd.id == p7.id && p1.id == p6.id && p2.id == p5.id && p3.id == p4.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id =>
                  MapOrderedStream(
                    {
                      val newInnerParam = ParamDef()
                      ArchLambda(newInnerParam,
                        MapOrderedStream(
                          VectorToOrderedStream.asFunction(),
                          ParamUse(newInnerParam)
                        )
                      )
                    },
                    ParamUse(newParam)
                  )
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
  })

  // TODO make private
  def mapFusionMapParam: Rule = Rule("moveUpVectorToStream_mapFusionMapParam", {
    // This rule conflicts with MoveUpJoinStream(MapFission)!
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val vec2stmInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        vec2stmInnerMost.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit{
          case ParamUse(p4) if p1.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val vec2stmInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        vec2stmInnerMost.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case JoinOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  private def convertMapStreamToMapVector: Rule = Rule("moveUpVectorToStream_convertMapStreamToMapVector", {
    // if fun is too generic, this statement may parallelize even further by replacing more mapstreams with mapvectors
//    case MapOrderedStream(fun @ ArchLambda(p1, Conversion(ParamUse(p2, _), _), _), VectorToOrderedStream(input, _), _) if p1.id == p2.id =>
//      VectorToOrderedStream(MapVector(fun, input))
    case MapOrderedStream(fun: LambdaT, VectorToOrderedStream(input, _), _) if
      fun.t.outType.isInstanceOf[BasicDataTypeT] &&
      !RulesHelper.isComputation(fun) &&
      !RulesHelper.exprContains(fun, {
        case VectorToOrderedStream(_, _) => true // do not rewrite if there is an inner stream creation (may temporary create a vector of stream, which is forbidden!)
      }) =>
      VectorToOrderedStream(MapVector(fun, input))
  })

  private def skipExprs: Rule = Rule("moveUpVectorToStream_skipExprs", {
    case RepeatHidden(MapOrderedStream(fun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), repetitions, _)
      if p2.id == p3.id && p1.id == p4.id =>
      MapOrderedStream(fun, RepeatHidden(input, repetitions))
    case Marker(MapOrderedStream(fun @ ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), text)
      if p1.id == p2.id =>
      MapOrderedStream(fun, Marker(input, text))
    case Marker(MapOrderedStream(fun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), text)
      if p2.id == p3.id && p1.id == p4.id =>
      MapOrderedStream(fun, Marker(input, text))
    case Marker(
    MapOrderedStream(fun @ ArchLambda(p1,
    MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3,
    VectorToOrderedStream(
    ParamUse(p4), _), _),
    ParamUse(p5), _), _),
    ParamUse(p6), _), _),
    input, _), text)
      if p3.id == p4.id && p2.id == p5.id && p1.id == p6.id =>
      MapOrderedStream(fun, Marker(input, text))
    case Marker(MapOrderedStream(vecToStmFun: LambdaT, input, _), text)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(vecToStmFun)
        innerMostBody.body match {
          case VectorToOrderedStream(ParamUse(p), _) if p.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream(vecToStmFun, Marker(input,  text))
  })

  private def skipTuple: Rule = Rule("moveUpVectorToStream_skipTuple", {
    case
      MapOrderedStream(
      ArchLambda(p1, Zip2OrderedStream(ParamUse(p2), _), _),
      Zip2OrderedStream(Tuple2(
      MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), input1, _),
      MapOrderedStream(ArchLambda(p5, VectorToOrderedStream(ParamUse(p6), _), _), input2, _), _), _), _)
      if p1.id == p2.id && p3.id == p4.id && p5.id == p6.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p, VectorToOrderedStream(ZipVector(ParamUse(p))))
        },
        Zip2OrderedStream(Tuple2(input1, input2))
      )
    case
      MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Zip2OrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _),
      MapOrderedStream(ArchLambda(p5, Zip2OrderedStream(ParamUse(p6), _), _),
      Zip2OrderedStream(Tuple2(
      MapOrderedStream(ArchLambda(p7, MapOrderedStream(ArchLambda(p8, VectorToOrderedStream(ParamUse(p9), _), _), ParamUse(p10), _), _), input1, _),
      MapOrderedStream(ArchLambda(p11, MapOrderedStream(ArchLambda(p12, VectorToOrderedStream(ParamUse(p13), _), _), ParamUse(p14), _), _), input2, _), _), _), _), _)
      if p1.id == p4.id && p2.id == p3.id && p5.id == p6.id && p7.id == p10.id && p8.id == p9.id && p11.id == p14.id && p12.id == p13.id =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(pOuter,
            MapOrderedStream(
              {
                val pInner = ParamDef()
                ArchLambda(pInner,
                  VectorToOrderedStream(ZipVector(ParamUse(pInner)))
                )
              },
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          Zip2OrderedStream.asFunction(),
          Zip2OrderedStream(Tuple2(input1, input2))
        )
      )
    case MapOrderedStream(ArchLambda(p1, Zip2OrderedStream(ParamUse(p2), _), _), Zip2OrderedStream(Tuple2(MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), input1, _), input2, _), _), _)
      if p1.id == p2.id && p3.id == p4.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p, VectorToOrderedStream(ZipVector(ParamUse(p))))
        },
        Zip2OrderedStream(Tuple2(
          input1,
          MapOrderedStream(
            OrderedStreamToVector.asFunction(),
            input2,
          )
        ))
      )
  })

  // TODO make private
  def skipSplitStream: Rule = Rule("moveUpVectorToStream_skipSplitStream", {
    case SplitOrderedStream(VectorToOrderedStream(input, _), chunkSize, _) =>
      MapOrderedStream(
        VectorToOrderedStream.asFunction(),
        VectorToOrderedStream(SplitVector(input, chunkSize))
      )
    case SplitOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), chunkSize, _) if p1.id == p2.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p,
            MapOrderedStream(
              VectorToOrderedStream.asFunction(),
              ParamUse(p)
            )
          )
        },
        SplitOrderedStream(input, chunkSize)
      )
  })

  private def skipJoinStream: Rule = Rule("moveUpVectorToStream_skipJoinStream", {
    case JoinOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), _)
      if p1.id == p4.id && p2.id == p3.id =>
      MapOrderedStream(
        VectorToOrderedStream.asFunction(),
        JoinOrderedStream(input)
      )
//    case JoinOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), input, _), _)
//      if p1.id == p6.id && p2.id == p5.id && p3.id == p4.id =>
//      MapOrderedStream(
//        {
//          val p = ParamDef()
//          ArchLambda(p, MapOrderedStream(VectorToOrderedStream.asFunction(), ParamUse(p)))
//        },
//        JoinOrderedStream(input)
//      )
  })

  private def skipDropStream: Rule = Rule("moveDownMapStreamToVector_skipDropStream", {
    case DropOrderedStream(VectorToOrderedStream(input, _), firstElements, lastElements, _) =>
      VectorToOrderedStream(DropVector(input, firstElements, lastElements))
  })

  private def skipDropJoinStream: Rule = Rule("moveDownMapStreamToVector_skipDropJoinStream", {
    case DropOrderedStream(JoinOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), _),
    firstElements, lastElements, _) if p1.id == p2.id =>
      VectorToOrderedStream(DropVector(JoinVector(OrderedStreamToVector(input)), firstElements, lastElements))
  })
}
