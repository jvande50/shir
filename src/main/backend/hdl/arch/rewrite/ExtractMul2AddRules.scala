package backend.hdl.arch.rewrite

import backend.hdl.VectorTypeT
import backend.hdl.arch._
import backend.hdl.arch.device.Mul2AddInt
import core.{ParamDef, ParamUse}
import core.rewrite.{Rule, RulesT}

object ExtractMul2AddRules extends RulesT {

  final def MAX_BITWIDTH_FOR_TWO_MUL = 18

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(extractMul2Add)

  def extractMul2Add: Rule = Rule("extractMul2Add", {
    case MapVector(ArchLambda(p1, AddInt(VectorToTuple2(ParamUse(p2), _), _), _), SplitVector(MapVector(ArchLambda(p3, MulInt(ParamUse(p4), _), _), input, _), at, _), _)
      if p1.id == p2.id && p3.id == p4.id && at.ae.evalInt == 2 && p2.t.asInstanceOf[VectorTypeT].et.bitWidth.ae.evalInt <= MAX_BITWIDTH_FOR_TWO_MUL =>
      MapVector(
        {
          val p = ParamDef()
          ArchLambda(p,
            Mul2AddInt(VectorToTuple2(ParamUse(p)))
          )
        },
        SplitVector(input, 2)
      )
  })

}
