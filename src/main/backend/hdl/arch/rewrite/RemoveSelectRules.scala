package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core._

object RemoveSelectRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      removeSelect,
      removeMapSelect,
      moveExprsBetweenTupleAndSelect,
      rewriteConcatStreamSelect,
      fixZipSelectDim,
      removeFlipTuple,
    )

  def removeSelect: Rule = Rule("removeSelect", {
    case Tuple(Select(ParamUse(p1), n, sel0, _) +: xs, _) if n.ae.evalInt == xs.length + 1 && sel0.ae.evalInt == 0 && xs.zipWithIndex.forall({
      case (Select(ParamUse(p2), _, sel, _), i) if p2.id == p1.id && sel.ae.evalInt == i + 1 => true
      case _ => false
    }) =>
      ParamUse(p1)
    case Tuple(Select(ParamUse(p1), n, sel0, _) +: xs, _) if n.ae.evalInt == xs.length + 1 && sel0.ae.evalInt == n.ae.evalInt - 1 && xs.zipWithIndex.forall({
      case (Select(ParamUse(p2), _, sel, _), i) if p2.id == p1.id && sel.ae.evalInt == n.ae.evalInt - 2 - i => true
      case _ => false
    }) =>
      FlipTuple(ParamUse(p1), n)
    case Select(Tuple(elements, _), _, sel, _) =>
      elements(sel.ae.evalInt)
  })

  def removeMapSelect: Rule = Rule("removeMapSelect", {
    case MapOrderedStream(ArchLambda(p1, Select(ParamUse(p2), _, sel, _), _), ZipOrderedStream(Tuple(elements, _), _, _), _)
      if p1.id == p2.id =>
      elements(sel.ae.evalInt)
  })

  def removeFlipTuple: Rule = Rule("removeFlipTuple", {
    case Select(FlipTuple(input, _, _), n, sel, _) =>
      Select(input, n, n.ae - 1 - sel.ae)
  })

  def moveExprsBetweenTupleAndSelect: Rule = Rule("moveExprsBetweenTupleAndSelect", {
    case MapOrderedStream(ArchLambda(p, body, _), Zip2OrderedStream(Tuple2(t0, t1, _), _), _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        // do not rewrite if this is just a tuple with 2 selects
        case Tuple2(Select2(_, _, _), Select2(_, _, _), _) =>
        case Tuple2(element1, element2, _) =>
          var sel1 = -1
          var sel2 = -1
          element1.visit {
            case Select2(ParamUse(selParam), sel, _) if p.id == selParam.id =>
              sel1 = sel.ae.evalInt
            case _ =>
          }
          element2.visit {
            case Select2(ParamUse(selParam), sel, _) if p.id == selParam.id =>
              sel2 = sel.ae.evalInt
            case _ =>
          }
          if ((sel1 == 0 && sel2 == 1) || (sel1 == 1 && sel2 == 0)) {
            exprFound = true
          }
        case ParamUse(p2) if p.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // make sure the parameter is not used without any select statement
      exprFound && paramUses == 2
    } =>
      var element1Sel = -1
      var element1Exprs: Expr = null
      var element2Sel = -1
      var element2Exprs: Expr = null
      val newFun = {
        val param = ParamDef()
        ArchLambda(
          param,
          body.visitAndRebuild{
            case t @ Tuple2(Select2(_, _, _), Select2(_, _, _), _) => t
            case t @ Tuple2(element1, element2, _) =>
              var sel1 = -1
              var sel2 = -1
              element1.visit {
                case Select2(ParamUse(selParam), sel, _) if p.id == selParam.id =>
                  sel1 = sel.ae.evalInt
                case _ =>
              }
              element2.visit {
                case Select2(ParamUse(selParam), sel, _) if p.id == selParam.id =>
                  sel2 = sel.ae.evalInt
                case _ =>
              }
              if (sel1 == 0 && sel2 == 1) {
                element1Sel = sel1
                element1Exprs = element1
                element2Sel = sel2
                element2Exprs = element2
                ParamUse(param)
              } else if (sel1 == 1 && sel2 == 0) {
                element1Sel = sel2
                element1Exprs = element2
                element2Sel = sel1
                element2Exprs = element1
                FlipTuple2(ParamUse(param))
              } else {
                t
              }
            case e => e
          }.asInstanceOf[Expr]
        )
      }

      val newElement1 = element1Exprs match {
        case Select2(ParamUse(selParam), _, _) if p.id == selParam.id => t0
        case _ => MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              element1Exprs.visitAndRebuild {
                case Select2(ParamUse(selParam), sel, _) if p.id == selParam.id && sel.ae.evalInt == element1Sel =>
                  ParamUse(param)
                case e => e
              }.asInstanceOf[Expr]
            )
          }, t0)
      }

      val newElement2 = element2Exprs match {
        case Select2(ParamUse(selParam), _, _) if p.id == selParam.id => t1
        case _ => MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              element2Exprs.visitAndRebuild{
                case Select2(ParamUse(selParam), sel, _) if p.id == selParam.id && sel.ae.evalInt == element2Sel =>
                  ParamUse(param)
                case e => e
              }.asInstanceOf[Expr]
            )
          }, t1)
      }

      if (newFun.isIdentity) {
        Zip2OrderedStream(Tuple2(newElement1, newElement2))
      } else {
        MapOrderedStream(
          newFun,
          Zip2OrderedStream(Tuple2(newElement1, newElement2))
        )
      }
  })

  def rewriteConcatStreamSelect: Rule = Rule("rewriteConcatStreamSelect", {
    case MapOrderedStream(ArchLambda(p, body, _), Zip2OrderedStream(Tuple2(t0, t1, _), _), _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        // do not rewrite if this is just a tuple with 2 selects
        case ConcatOrderedStream(Select2(ParamUse(p0), sel0, _), Select2(ParamUse(p1), sel1, _), _, _) if p0.id == p.id && p1.id == p.id && ((sel0.ae.evalInt == 0 && sel1.ae.evalInt == 1) || (sel0.ae.evalInt == 1 && sel1.ae.evalInt == 0)) =>
          exprFound = true
        case ParamUse(pu) if p.id == pu.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // make sure the parameter is not used without any select statement
      exprFound && paramUses == 2
    } =>
      var dimension: ArithTypeT = ArithType(0)
      var flipped = false

      val newFun = {
        val param = ParamDef()
        ArchLambda(
          param,
          body.visitAndRebuild{
            case ConcatOrderedStream(Select2(ParamUse(p0), sel0, _), Select2(ParamUse(p1), sel1, _), dim, _) if p0.id == p.id && p1.id == p.id =>
              flipped = sel0.ae.evalInt == 1 && sel1.ae.evalInt == 0
              dimension = dim
              ParamUse(param)
            case e => e
          }.asInstanceOf[Expr]
        )
      }

      val newConcat = if (flipped) {
        ConcatOrderedStream(t1, t0, dimension.ae + 1)
      } else {
        ConcatOrderedStream(t0, t1, dimension.ae + 1)
      }

      if (newFun.isIdentity) {
        newConcat
      } else {
        MapOrderedStream(newFun, newConcat)
      }
  })

  def fixZipSelectDim: Rule = Rule("fixZipSelectDim", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, Zip2OrderedStream(ParamUse(p2), _), _), _),
    Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if p1.id == p2.id &&
      in1.t.asInstanceOf[OrderedStreamTypeT].dimensions.length - in2.t.asInstanceOf[OrderedStreamTypeT].dimensions.length == 2 &&{
      var exprFound = false
      fun.body.visit{
        case Select2(ParamUse(p), _, _) if fun.param.id == p.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(MapOrderedStream(2, JoinOrderedStream.asFunction(), in1), in2)))
      MapOrderedStream(
        {
          val param1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          val newInnerInput = TypeChecker.check(Zip2OrderedStream(ParamUse(param1)))
          ArchLambda(
            param1,
            MapOrderedStream(
              {
                val param2 = ParamDef(newInnerInput.t.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(
                  param2,
                  fun.body.visitAndRebuild{
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 0 =>
                      SplitOrderedStream(Select2(ParamUse(param2), sel), in1.t.asInstanceOf[OrderedStreamTypeT].dimensions.head)
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 1 =>
                      Select2(ParamUse(param2), sel)
                    case e => e
                  }.asInstanceOf[Expr]
                )
              }, newInnerInput
            )
          )
        }, newInput
      )

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, Zip2OrderedStream(ParamUse(p2), _), _), _),
    Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if p1.id == p2.id &&
      in2.t.asInstanceOf[OrderedStreamTypeT].dimensions.length - in1.t.asInstanceOf[OrderedStreamTypeT].dimensions.length == 2 &&{
      var exprFound = false
      fun.body.visit{
        case Select2(ParamUse(p), _, _) if fun.param.id == p.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(in1, MapOrderedStream(2, JoinOrderedStream.asFunction(), in2))))
      MapOrderedStream(
        {
          val param1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          val newInnerInput = TypeChecker.check(Zip2OrderedStream(ParamUse(param1)))
          ArchLambda(
            param1,
            MapOrderedStream(
              {
                val param2 = ParamDef(newInnerInput.t.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(
                  param2,
                  fun.body.visitAndRebuild{
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 1 =>
                      SplitOrderedStream(Select2(ParamUse(param2), sel), in2.t.asInstanceOf[OrderedStreamTypeT].dimensions.head)
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 0 =>
                      Select2(ParamUse(param2), sel)
                    case e => e
                  }.asInstanceOf[Expr]
                )
              }, newInnerInput
            )
          )
        }, newInput
      )

    // Dimension diff == 3
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, Zip2OrderedStream(ParamUse(p2), _), _), _),
    Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if p1.id == p2.id &&
      in1.t.asInstanceOf[OrderedStreamTypeT].dimensions.length - in2.t.asInstanceOf[OrderedStreamTypeT].dimensions.length == 3 &&{
      var exprFound = false
      fun.body.visit{
        case Select2(ParamUse(p), _, _) if fun.param.id == p.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newIn1 = MapOrderedStream(
        2, {
          val param = ParamDef()
          ArchLambda(
            param,
            JoinOrderedStream(JoinOrderedStream(ParamUse(param)))
          )
        }, in1
      )
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(newIn1, in2)))
      MapOrderedStream(
        {
          val param1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          val newInnerInput = TypeChecker.check(Zip2OrderedStream(ParamUse(param1)))
          ArchLambda(
            param1,
            MapOrderedStream(
              {
                val param2 = ParamDef(newInnerInput.t.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(
                  param2,
                  fun.body.visitAndRebuild{
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 0 =>
                      SplitOrderedStream(SplitOrderedStream(Select2(ParamUse(param2), sel), in1.t.asInstanceOf[OrderedStreamTypeT].dimensions.head), in1.t.asInstanceOf[OrderedStreamTypeT].dimensions(1))
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 1 =>
                      Select2(ParamUse(param2), sel)
                    case e => e
                  }.asInstanceOf[Expr]
                )
              }, newInnerInput
            )
          )
        }, newInput
      )

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, Zip2OrderedStream(ParamUse(p2), _), _), _),
    Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if p1.id == p2.id &&
      in2.t.asInstanceOf[OrderedStreamTypeT].dimensions.length - in1.t.asInstanceOf[OrderedStreamTypeT].dimensions.length == 3 &&{
      var exprFound = false
      fun.body.visit{
        case Select2(ParamUse(p), _, _) if fun.param.id == p.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newIn2 = MapOrderedStream(
        2, {
          val param = ParamDef()
          ArchLambda(
            param,
            JoinOrderedStream(JoinOrderedStream(ParamUse(param)))
          )
        }, in1
      )
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(in1, newIn2)))
      MapOrderedStream(
        {
          val param1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          val newInnerInput = TypeChecker.check(Zip2OrderedStream(ParamUse(param1)))
          ArchLambda(
            param1,
            MapOrderedStream(
              {
                val param2 = ParamDef(newInnerInput.t.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(
                  param2,
                  fun.body.visitAndRebuild{
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 1 =>
                      SplitOrderedStream(SplitOrderedStream(Select2(ParamUse(param2), sel), in2.t.asInstanceOf[OrderedStreamTypeT].dimensions.head), in2.t.asInstanceOf[OrderedStreamTypeT].dimensions(1))
                    case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt == 0 =>
                      Select2(ParamUse(param2), sel)
                    case e => e
                  }.asInstanceOf[Expr]
                )
              }, newInnerInput
            )
          )
        }, newInput
      )
  })
}
