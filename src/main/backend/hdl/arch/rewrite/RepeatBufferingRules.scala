package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.{ArchLambda, JoinStreamAll, MapOrderedStream, OrderedStreamToVector, Repeat, SplitStreamAll, VectorToOrderedStream}
import core.{ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object RepeatBufferingRules extends RulesT {

  /** Buffer stream data if any Repeat is not correctly moved down to counter. Because this stage (MapCompiler) happens
    * after MemoryLayout, it is impossible to insert "BRAM" or host buffer. To circumvent this problem, we insert a reg-
    * ister array to store the values in a stream. The register array is created from OrderedStreamToVector.
    */

  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    bufferRepeatParam
  ) ++ MoveUpVectorToStreamRules.get() ++ RemoveStreamVectorConversionRules.get() ++ Seq(CleanupRules.removeEmptyMap)
  // MoveDownStreamToVectorRules is not included because we want to put Repeat and StreamToVector together!

  def bufferRepeatParam: Rule = Rule("bufferRepeatParam", {
    case Repeat(input@ParamUse(_), times, t: OrderedStreamTypeT) if t.et.isInstanceOf[OrderedStreamTypeT] =>
      val inputDims = input.t.asInstanceOf[OrderedStreamTypeT].dimensions
      val joinInput = TypeChecker.check(JoinStreamAll(input))
      val bufInput = TypeChecker.check(OrderedStreamToVector(joinInput))
      val repeatInput = TypeChecker.check(Repeat(bufInput, times))
      MapOrderedStream(
        {
          val param = ParamDef(repeatInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            TypeChecker.check(SplitStreamAll(TypeChecker.check(VectorToOrderedStream(ParamUse(param))), inputDims.dropRight(1).reverse))
          )
        }, repeatInput
      )
  })
}
