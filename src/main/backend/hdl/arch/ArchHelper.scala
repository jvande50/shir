package backend.hdl.arch

import core.{Expr, ParamDef}

object ArchHelper {
  /**
   * Get Lambda Function's body.
   */
  def getLambdaBody(e: Expr): Expr = e match {
    case ArchLambda(p, body, _) => getLambdaBody(body)
    case e => e
  }

  /**
   * Get Lambda Params.
   */
  def getLambdaParams(e: Expr): Seq[ParamDef] = e match {
    case ArchLambda(p, body, _) => Seq(p) ++ getLambdaParams(body)
    case _ => Seq()
  }

  def getLambdaParamIds(e: Expr): Seq[Long] = e match {
    case ArchLambda(p, body, _) => Seq(p.id) ++ getLambdaParamIds(body)
    case _ => Seq()
  }
}
