package backend.hdl.arch.tiling

import backend.hdl.arch.rewrite.conv.{MoveDownPadRules, MoveDownSlideGeneralStreamRules, PadConversionRules}
import backend.hdl.arch.rewrite.transpose.{MoveDownTransposeRules, SwapMapsRules}
import core.{Expr, TypeChecker}
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph

object PaddingCompiler extends CompilerPass {
  override def phaseBefore: CompilerPhase = TranspositionCompiler.phaseAfter

  /**
    * This pass handles padding related issues since paddings and transpositions complicate the expressions. It is very
    * tricky to move transpositions across paddings. Direct transposition and creation of If-else are still inefficient.
    * To solve this, we first move padding down to address generators, and then merge it into counters. The process also
    * introduces a pad-enable controller to hande out-of-bounds accesses. After that, padding related primitives are re-
    * moved and we can further move transposition downs to address generators.
    */
  override def run(expr: Expr): Expr = {
    val expr0 = TypeChecker.checkAssert(expr)
    val expr1 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownPadRules.get()).apply(expr0))
    val expr2 = TypeChecker.check(RewriteStep(RewriteAll(), PadConversionRules.get()).apply(expr1))
    val expr3 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownSlideGeneralStreamRules.get()).apply(expr2))
    val expr4 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownTransposeRules.get() ++ MoveDownTransposeRules.mergeIntoReadAddress).apply(expr3))
    expr4
  }
}