package backend.hdl.arch.tiling

import backend.hdl.arch.rewrite.conv.SlideGeneralConversionRules
import backend.hdl.arch.rewrite.transpose.{MoveDownTransposeRules, TransposeConversionRules}
import backend.hdl.arch.rewrite.{CleanupRules, MoveDownSplitStreamRules, MoveDownStreamToVectorRules, MoveUpJoinStreamRules, MoveUpVectorToStreamRules, MultipleParamUsesRules, PadMul2AddRules, RemoveStreamVectorConversionRules}
import core.{Expr, TypeChecker}
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph

object ParallelizationCompiler extends CompilerPass {
  override def phaseBefore: CompilerPhase = PaddingCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val expr0 = TypeChecker.checkAssert(expr)
    val expr1 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(CleanupRules.mapFusionMapParam)).apply(expr0))
    // Clean up Join and Split
    val expr2 = TypeChecker.check(RewriteStep(RewriteAll(), MoveUpJoinStreamRules.all(None) ++ MoveDownSplitStreamRules.skipTupleStructure ++ MultipleParamUsesRules.get() ++ Seq(CleanupRules.removeEmptyMap)).apply(expr1))
    // Replace transposition with PermuteVec and replace SlideGeneral with SlideVec
    val expr3 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(TransposeConversionRules.convertTranspose2DIntoPermuteVec, TransposeConversionRules.limitedConvertTransposeNDIntoPermute, MoveDownTransposeRules.mapFission, MoveDownTransposeRules.moveIntoMapNonCompute2, TransposeConversionRules.ConvertTransposeNDIntoPermute) ++ SlideGeneralConversionRules.get()).apply(expr2))
    // Clean up Vec2Stm and Stm2Vec
    val expr4 = TypeChecker.check(RewriteStep(RewriteAll(), MoveUpVectorToStreamRules.get() ++ RemoveStreamVectorConversionRules.get() ++ MoveUpJoinStreamRules.get() ++
      MoveDownStreamToVectorRules.skipTupleStructure).apply(expr3))
    val expr5 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(CleanupRules.mapFusionMapParam)).apply(expr4))
    val expr6 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(PadMul2AddRules.padMul2Add, CleanupRules.removeEmptyMap, RemoveStreamVectorConversionRules.removeStm2VecSize1) ++ CleanupRules.all(None)).apply(expr5))
    expr6
  }
}

