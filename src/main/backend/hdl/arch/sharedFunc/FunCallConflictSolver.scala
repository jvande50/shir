package backend.hdl.arch.sharedFunc

import backend.hdl.arch.mem.Write
import backend.hdl.arch.rewrite.sharedFunc.BufferInsertionHelperRules
import backend.hdl.{HWDataTypeT, RamArrayTypeT}
import backend.hdl.arch.{ArchLambda, MapOrderedStream, ReduceOrderedStream, Tuple2}
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph
import core.{Expr, FunctionCall, LambdaT, Let, MalformedExprException, Marker, ParamDef, ParamUse, TextType, TextTypeT, TypeChecker}

object FunCallConflictSolver {
  /**
   * For solving conflicts.
   */

  private var functionIdList: Seq[ParamDef] = Seq()
  private var callId: Int = 0
  private var callGraphs: Map[Long, Map[Int, IFGNode]] = Map() // Scope, Graph
  private var callSeqs: Map[Long, Map[Int, Seq[Int]]] = Map()
  private var callExprs: Map[Long, Expr] = Map() // Scope, Graph
  private var functionNesting: Map[Long, Seq[Long]] = Map()
  private var tmpSeq: Map[Int, Seq[Int]] = Map()
  private var tmpId: Int = -1
  def solveConflicts(expr: Expr): Expr = {
    val newExpr1 = buildGraphs(expr)
    val newExpr2 = TypeChecker.check(insertBuffer(newExpr1))
    val newExpr3 = TypeChecker.check(removeFuncMark(newExpr2))
    val newExpr4 = TypeChecker.check(RewriteStep(RewriteAll(), BufferInsertionHelperRules.get()).apply(newExpr3))
    val newExpr5 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(BufferInsertionHelperRules.replaceWithRealBuffer)).apply(newExpr4))
    newExpr5
  }

  /**
   * Get all functions.
   */
  private def getAllFuns(expr: Expr): Unit = expr.visit{
    case Let(p, _, _: LambdaT, _) if functionIdList.contains(p) => throw MalformedExprException("Function definition is duplicated!")
    case Let(p, _, _: LambdaT, _) => functionIdList = functionIdList :+ p
    case _ =>
  }

  /**
   * Get all functions.
   */
  private def getFunDependency(expr: Expr): Unit = expr.visit {
    case Let(p, _, arg: LambdaT, _) =>
      var funSeq: Seq[Long] = Seq()
      arg.visit {
        case FunctionCall(FunctionCall(ParamUse(pf), _, _), _, _: HWDataTypeT) => funSeq = funSeq :+ pf.id
        case FunctionCall(ParamUse(pf), _, _: HWDataTypeT) => funSeq = funSeq :+ pf.id
        case _ =>
      }
      funSeq = funSeq.distinct
      functionNesting = functionNesting + (p.id -> funSeq)
    case _ =>
  }

  private def resetAndGetAllFuns(expr: Expr): Unit = {
    functionIdList = Seq()
    functionNesting = Map()
    getAllFuns(expr)
    getFunDependency(expr)
  }

  private def isConflict(pid1: Long, pid2: Long): Boolean = {
    var res = pid1 == pid2
    if(!res) {
      val childrenConflicts1 = functionNesting.get(pid2).get.map(pid => isConflict(pid1, pid)).foldLeft(false)(_||_)
      val childrenConflicts2 = functionNesting.get(pid1).get.map(pid => isConflict(pid2, pid)).foldLeft(false)(_||_)
      res = res || childrenConflicts1 || childrenConflicts2
    }
    res
  }

  /**
   * Mark all function calls to have us to build interference graph.
   */
  private def markAllFunCalls(expr: Expr): Expr = expr.visitAndRebuild {
    case FunctionCall(FunctionCall(ParamUse(p), input1, _), input2, _: HWDataTypeT) if functionIdList.contains(p) =>
      callId = callId + 1
      FunctionCall(FunctionCall(Marker(ParamUse(p), TextType(callId.toString)), input1), input2)
    case FunctionCall(ParamUse(p), input, _: HWDataTypeT) if functionIdList.contains(p) =>
      callId = callId + 1
      FunctionCall(Marker(ParamUse(p), TextType(callId.toString )), input)
    case e => e
  }.asInstanceOf[Expr]

  private def resetAndMarkAllFunCalls(expr: Expr): Expr = {
    callId = 0
    markAllFunCalls(expr)
  }

  private def removeFuncMark(expr: Expr): Expr = expr.visitAndRebuild {
    case FunctionCall(FunctionCall(Marker(ParamUse(p), _), input1, _), input2, _: HWDataTypeT) =>
      FunctionCall(FunctionCall(ParamUse(p), input1), input2)
    case FunctionCall(Marker(ParamUse(p), _), input1, _: HWDataTypeT) =>
      FunctionCall(ParamUse(p), input1)
    case e => e
  }.asInstanceOf[Expr]

  /**
   * Build interference graphs and also collect corresponding expression body.
   * Note that param id = -1 indicates the main function.
   */
  private def buildGraphs(expr: Expr): Expr = {
    // reset everything
    val exprtc = TypeChecker.check(expr)
    resetAndGetAllFuns(exprtc)
    val markerExpr = TypeChecker.check(resetAndMarkAllFunCalls(exprtc))
    callGraphs = Map()
    callSeqs = Map()
    callExprs = Map()
    markerExpr.visit {
      case Let(p, _, arg: LambdaT, _) if functionIdList.contains(p) =>
        buildGraph(arg, p.id)
      case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, ReduceOrderedStream(_, _, _, _), _), _), _, input, t: RamArrayTypeT) =>
        buildGraph(input, -1)
      case _ =>
    }
    markerExpr
  }

  /**
   * Build an interference graph under scope (id).
   * Note that param id = -1 indicates the main function.
   */
  private def buildGraph(expr: Expr, id: Long): Unit = {
    var callGraph: Map[Int, IFGNode] = Map()
    tmpSeq = Map(-1 -> Seq())
    tmpId = -1
    // Build nodes
    expr.visit{
      case FunctionCall(FunctionCall(Marker(ParamUse(p), ts), input1, _), input2, _: HWDataTypeT) if ts.s.forall(_.isDigit)  =>
        callGraph = callGraph + (ts.s.toInt -> new IFGNode(p.id.toInt, ts.s.toInt))
        tmpSeq = tmpSeq + (ts.s.toInt -> Seq())
      case FunctionCall(Marker(ParamUse(p), ts), input, _: HWDataTypeT) if ts.s.forall(_.isDigit) =>
        callGraph = callGraph + (ts.s.toInt -> new IFGNode(p.id.toInt, ts.s.toInt))
        tmpSeq = tmpSeq + (ts.s.toInt -> Seq())
      case _ =>
    }
    // build conflicts
    expr.visit {
      case FunctionCall(FunctionCall(Marker(ParamUse(p), ts), input1, _), input2, _: HWDataTypeT) if ts.s.forall(_.isDigit) =>
        // Initialize graph and call seq
        val funCallId = ts.s.toInt
        var conflicts: Seq[Int] = Seq()
        conflicts = conflicts ++ traverseFunCallInputs(input1, p) ++ traverseFunCallInputs(input2, p)
        conflicts = conflicts ++ traverseTupleFunCall(expr, p, funCallId)
        conflicts = conflicts ++ traverseMapFunCall(expr, p, funCallId)
        conflicts = conflicts ++ traverseFunCallOutput(expr, p, funCallId)
        conflicts = conflicts.distinct
        val nodeList = conflicts.map(e => callGraph.get(e).get)
        nodeList.map(e => callGraph.get(funCallId).get.addEdge(e))
      case FunctionCall(Marker(ParamUse(p), ts), input, _: HWDataTypeT) if ts.s.forall(_.isDigit) =>
        // Initialize graph and call seq
        val funCallId = ts.s.toInt
        var conflicts: Seq[Int] = Seq()
        conflicts = conflicts ++ traverseFunCallInputs(input, p)
        conflicts = conflicts ++ traverseTupleFunCall(expr, p, funCallId)
        conflicts = conflicts ++ traverseMapFunCall(expr, p, funCallId)
        conflicts = conflicts ++ traverseFunCallOutput(expr, p, funCallId)
        conflicts = conflicts.distinct
        val nodeList = conflicts.map(e => callGraph.get(e).get)
        nodeList.map(e => callGraph.get(funCallId).get.addEdge(e))
      case _ =>
    }
    // Update call sequence
    getCallSequence(expr, -1)
    // Update super graphs
    callGraphs = callGraphs + (id -> callGraph)
    callSeqs = callSeqs + (id -> tmpSeq)
    callExprs = callExprs + (id -> expr)
  }

  /**
   * Traverse inputs of a function call to find out all the conflicts. Note that there is no search under write access.
   *
   * @param expr Input expression for searching.
   * @param pf Parameter of a function.
   * @return A sequence of function call IDs (Int).
   */
  private def traverseFunCallInputs(expr: Expr, pf: ParamDef): Seq[Int] = expr match {
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _), _), _), _, _, _) => Seq()
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _), _), _), _, _, _), _), _), _, _, _) => Seq()
    case FunctionCall(FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _), in2, _: HWDataTypeT) if isConflict(pd.id, pf.id) =>
      Seq(tx.s.toInt) ++ traverseFunCallInputs(in1, pf) ++ traverseFunCallInputs(in2, pf)
    case FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _: HWDataTypeT) if isConflict(pd.id, pf.id) =>
      Seq(tx.s.toInt) ++ traverseFunCallInputs(in1, pf)
    case _ =>
      var tmpSeq: Seq[Int] = Seq()
      expr.children.foreach({
        case e: Expr =>
          tmpSeq = tmpSeq ++ traverseFunCallInputs(e, pf)
        case _ =>
      })
      tmpSeq
  }

  /**
   * Traverse function call's output.
   *
   * @param expr Input expression for searching.
   * @param pf   Parameter of a function.
   * @param id   Function call ID for searching conflicts.
   * @return A sequence of function call IDs (Int).
   */
  private def traverseFunCallOutput(expr: Expr, pf: ParamDef, id: Int): Seq[Int] = expr match {
    case FunctionCall(FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _), in2, _: HWDataTypeT) if pd.id == pf.id && tx.s.toInt == id => Seq()
    case FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _: HWDataTypeT) if pd.id == pf.id && tx.s.toInt == id => Seq()
    case FunctionCall(FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _), in2, _: HWDataTypeT) if isConflict(pd.id, pf.id)  =>
      val in1Seq = traverseFunCallInputs(in1, pf)
      val in2Seq = traverseFunCallInputs(in2, pf)
      (in1Seq.contains(id), in2Seq.contains(id)) match {
        case (true, true) => ???
        case (true, false) => Seq(tx.s.toInt)
        case (false, true) => Seq(tx.s.toInt)
        case (false, false) => Seq()
      }
    case FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _: HWDataTypeT) if isConflict(pd.id, pf.id) =>
      val in1Seq = traverseFunCallInputs(in1, pf)
      if(in1Seq.contains(id))
        Seq(tx.s.toInt)
      else
        Seq()
    case _ =>
      var tmpSeq: Seq[Int] = Seq()
      expr.children.foreach({
        case e: Expr =>
          tmpSeq = tmpSeq ++ traverseFunCallOutput(e, pf, id)
        case _ =>
      })
      tmpSeq
  }

  /**
   * Traverse function calls inside tuple to find out all the conflicts.
   * For example, if there is a call in the left child, we need to do search in the right child.
   *
   * @param expr Input expression for searching.
   * @param pf Parameter of a function.
   * @param id Function call ID for searching conflicts.
   * @return A sequence of function call IDs (Int).
   */
  private def traverseTupleFunCall(expr: Expr, pf: ParamDef, id: Int): Seq[Int] = expr match {
    case Tuple2(in0, in1, _) =>
      val leftSeq = traverseFunCallInputs(in0, pf)
      val rightSeq = traverseFunCallInputs(in1, pf)
      (leftSeq.contains(id), rightSeq.contains(id)) match {
        case (true, false) => rightSeq ++ traverseTupleFunCall(in0, pf, id)
        case (false, true) => leftSeq ++ traverseTupleFunCall(in1, pf, id)
        case (false, false) => Seq()
      }
    case _ =>
      var tmpSeq: Seq[Int] = Seq()
      expr.children.foreach({
        case e: Expr =>
          tmpSeq = tmpSeq ++ traverseTupleFunCall(e, pf, id)
        case _ =>
      })
      tmpSeq
  }

  /**
   * Traverse function calls inside MapOrderedStream and MapOrderedStream2Input to find out all the conflicts.
   *
   * @param expr Input expression for searching.
   * @param pf   Parameter of a function.
   * @param id   Function call ID for searching conflicts.
   * @return A sequence of function call IDs (Int).
   */
  private def traverseMapFunCall(expr: Expr, pf: ParamDef, id: Int): Seq[Int] = expr match {
    // TODO: handle MapAsync
    case MapOrderedStream(ArchLambda(_, in0, _), in1, _) =>
      val funSeq = traverseFunCallInputs(in0, pf)
      val inSeq = traverseFunCallInputs(in1, pf)
      (funSeq.contains(id), inSeq.contains(id)) match {
        case (true, true) => ???
        case (true, false) => inSeq.filter(_ != id) ++ traverseMapFunCall(in0, pf, id)
        case (false, true) => funSeq.filter(_ != id) ++ traverseMapFunCall(in1, pf, id)
        case (false, false) => Seq()
      }
    case _ =>
      var tmpSeq: Seq[Int] = Seq()
      expr.children.foreach({
        case e: Expr =>
          tmpSeq = tmpSeq ++ traverseMapFunCall(e, pf, id)
        case _ =>
      })
      tmpSeq
  }

  /**
   * Call sequence related. Note that we have to trace the last call id in mapStream.
   */
  private def getCallSequence(expr: Expr, currentId: Int): Unit = expr match {
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _), _), _), _, input, _) => // reset sequence if write access
      tmpId = -1
      getCallSequence(input, -1)
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _), _), _), _, _, _), _), _), _, input, _) => Seq()
      tmpId = -1
      getCallSequence(input, -1)
    // TODO: handle MapAsync
    case MapOrderedStream(ArchLambda(_, in0, _), in1, _) =>
      getCallSequence(in0, currentId)
      val storedId = tmpId
      getCallSequence(in1, storedId)
    case FunctionCall(FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _), in2, _: HWDataTypeT) =>
      tmpId = tx.s.toInt
      getCallSequence(in1, tx.s.toInt)
      tmpId = tx.s.toInt
      getCallSequence(in2, tx.s.toInt)
      val newSeq = tmpSeq.get(currentId).get ++ Seq(tx.s.toInt)
      tmpSeq = tmpSeq.updated(currentId, newSeq)
    case FunctionCall(Marker(ParamUse(pd), tx: TextTypeT), in1, _: HWDataTypeT) =>
      tmpId = tx.s.toInt
      getCallSequence(in1, tx.s.toInt)
      val newSeq = tmpSeq.get(currentId).get ++ Seq(tx.s.toInt)
      tmpSeq = tmpSeq.updated(currentId, newSeq)
    case _ =>
      expr.children.foreach({
        case e: Expr =>
          getCallSequence(e, currentId)
        case _ =>
      })
  }

  /**
   * Buffer Insertion.
   */

  private def hasConflict(graph: Map[Int, IFGNode]): Boolean = graph.foldLeft(false)((a, t) => a || (t._2.connectedNodes.length > 0))
  private def callDepends(topCall: Int, botCall: Int, callSeq: Map[Int, Seq[Int]]): Boolean = {
    if(callSeq.get(topCall).get.contains(botCall)) {
      true
    } else if(callSeq.get(topCall).get.length == 0) {
      false
    } else {
      callSeq.get(topCall).get.map(e => callDepends(e, botCall, callSeq)).foldLeft(false)(_||_)
    }
  }

  private def containsPath(path1: (Int, Int), path2: (Int, Int), callSeq: Map[Int, Seq[Int]]): Boolean =
    ((path1._1 == path2._1 && path1._2 == path2._2) ||
      (path1._1 == path2._1 && callDepends(path2._2, path1._2, callSeq)) ||
      (path1._2 == path2._2 && callDepends(path1._1, path2._1, callSeq)) ||
      ((path1._2 != path2._1) && callDepends(path1._1, path2._1, callSeq) && callDepends(path2._2, path1._2, callSeq))) &&
      callDepends(path1._1, path1._2, callSeq) && callDepends(path2._1, path2._2, callSeq)

  private def collectEdges(graph: Map[Int, IFGNode]): Seq[(Int, Int)] = {
    var newCollect: Seq[(Int, Int)] = Seq()
    graph.foreach(nodeTup => nodeTup._2.connectedNodes.foreach(otherNode => newCollect = newCollect :+ (nodeTup._1, otherNode.funcCallId)))
    newCollect
  }

  private def containsCall(expr: Expr, callId: Int): Boolean = {
    var exprFound = false
    expr.visit{
      case Marker(ParamUse(_), ts: TextTypeT) if ts.s.toInt == callId => exprFound = true
      case _ =>
    }
    exprFound
  }

  private def bufferAdded(expr: Expr): Boolean = expr match {
    case Marker(_, ts: TextTypeT) if ts.s.contains("buffer") => true
    case _ => false
  }

  private def insertBuffer(expr: Expr): Expr = {
    var newExpr: Expr = expr
    for((id, g) <- callGraphs) {
      while(hasConflict(callGraphs(id))){
        // Calculate score for each edge. Remove the edge that can solve most conflicts.
        val edges = collectEdges(callGraphs(id))
        val callSeq = callSeqs(id)
        val scores = edges.map(edge1 => edges.map(edge2 => containsPath(edge2, edge1, callSeq)).map(e => if (e) 1 else 0).foldLeft(0)(_+_))
        val idxOfMax = scores.zipWithIndex.maxBy(_._1)._2
        val (topCall, botCall) = edges(idxOfMax)
        // Find Correct edges. We have three choices: Input(1 and 2 ports), MapInput(1 and 2 ports) and TupleInput.
        var bufferInserted: Boolean = false
        newExpr = newExpr.visitAndRebuild{
          // TODO: handle MapAsync, Tuple3 or larger, Multi-Input FunCall...
          case MapOrderedStream(ArchLambda(p, body, _), input, _) if containsCall(body, topCall) &&
            containsCall(input, botCall) && !bufferAdded(input)=>
            bufferInserted = true
            MapOrderedStream(ArchLambda(p, body), Marker(input, TextType("buffer")))
          case Tuple2(input1, input2, _) if ((containsCall(input1, topCall) && containsCall(input2, botCall)) ||
            (containsCall(input1, botCall) && containsCall(input2, topCall))) &&
            !bufferAdded(input1) && !bufferAdded(input2) =>
            bufferInserted = true
            Tuple2(Marker(input1, TextType("buffer")), Marker(input2, TextType("buffer")))
          case FunctionCall(Marker(ParamUse(p), ts), input, _) if ts.s.toInt == topCall && containsCall(input, botCall) &&
            !bufferAdded(input) =>
            bufferInserted = true
            FunctionCall(Marker(ParamUse(p), ts), Marker(input, TextType("buffer")))
          case FunctionCall(FunctionCall(Marker(ParamUse(p), ts), input1, _), input2, _) if
            ts.s.toInt == topCall && containsCall(input1, botCall) &&
              !bufferAdded(input1) =>
            bufferInserted = true
            FunctionCall(FunctionCall(Marker(ParamUse(p), ts), Marker(input1, TextType("buffer"))), input2)
          case FunctionCall(FunctionCall(Marker(ParamUse(p), ts), input1, _), input2, _) if
            ts.s.toInt == topCall && containsCall(input2, botCall) &&
              !bufferAdded(input2) =>
            bufferInserted = true
            FunctionCall(FunctionCall(Marker(ParamUse(p), ts), input1), Marker(input2, TextType("buffer")))
          case e => e
        }.asInstanceOf[Expr]

        // Remove edges based on inserted buffer.
        if(! bufferInserted) {
          ???
        } else {
          val dependentEdges = edges.filter(e => containsPath(e, edges(idxOfMax), callSeq))
          val involvedEdges = if(dependentEdges.isEmpty) Seq(edges(idxOfMax)) else dependentEdges
          involvedEdges.foreach(e => {
            val topNode = e._1
            val botNode = e._2
            callGraphs(id)(topNode).removeEdge(callGraphs(id)(botNode))
            callGraphs(id)(botNode).removeEdge(callGraphs(id)(topNode))
          })
        }
      }
    }
    newExpr
  }
}
