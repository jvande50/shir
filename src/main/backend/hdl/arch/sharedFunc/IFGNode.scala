package backend.hdl.arch.sharedFunc

class IFGNode(val letId: Int = 0, val funcCallId: Int = 0, var connectedNodes: Seq[IFGNode] = Seq()) {

  def addEdge(node: IFGNode): Unit = {
    // Remove let id check to allow nested conflicts
    if(!connectedNodes.contains(node) && /*node.letId == this.letId &&*/ node.funcCallId != this.funcCallId)
      connectedNodes = connectedNodes :+ node
  }

  def removeEdge(node: IFGNode): Unit = {
    if(connectedNodes.contains(node))
      connectedNodes = connectedNodes.patch(connectedNodes.indexOf(node), Nil, 1)
  }

  def getEdgeIds: Seq[Int] = connectedNodes.map(_.letId)

  def hasEdge(node: IFGNode): Boolean = connectedNodes.contains(node)

  def equals(node: IFGNode): Boolean = node.letId == this.letId && node.funcCallId == this.funcCallId

  override def toString(): String = "LetId" + letId.toString + "_" + "FuncCallId" + funcCallId.toString
}

