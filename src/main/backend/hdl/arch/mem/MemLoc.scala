package backend.hdl.arch.mem

import core.TextTypeT

sealed trait MemLoc
case object Block extends MemLoc
case object OnBoard extends MemLoc
case class Host(identifier: TextTypeT) extends MemLoc