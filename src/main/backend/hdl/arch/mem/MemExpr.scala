package backend.hdl.arch.mem

import backend.hdl.arch._
import backend.hdl._
import backend.hdl.arch.mem.BufferStreamInBlockRam.apply
import backend.hdl.specs.{FPGA, FPGASpecs}
import core._
import lift.arithmetic.{Cst, GoesToRange}

/*
 ***********************************************************************************************************************
 memory related expressions
 ***********************************************************************************************************************
 */

sealed trait MemExpr extends BuiltinExpr with ArchIR

/*
 ***********************************************************************************************************************
 high-level memory expressions (using ArrayType)
 ***********************************************************************************************************************
 */

case class MemoryAllocationExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MemoryAllocationExpr = MemoryAllocationExpr(newInnerIR, newTypes)
}
object MemoryAllocation {
  def unapply(expr: MemoryAllocationExpr): Some[(TextTypeT, TextTypeT, BasicDataTypeT, Seq[ArithTypeT], Type)] =
    Some((expr.types.head.asInstanceOf[TextTypeT], expr.types(1).asInstanceOf[TextTypeT], expr.types(2).asInstanceOf[BasicDataTypeT], expr.types.tail.tail.tail.map(_.asInstanceOf[ArithTypeT]), expr.t))
}
object BlockRam {
  private[arch] def apply(identifier: TextTypeT = TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()),
                          description: TextTypeT = TextType(""),
                          elementType: BasicDataTypeT,
                          dimensions: Seq[ArithTypeT]): MemoryAllocationExpr = {
    val allocationSize = dimensions.fold(ArithType(1))(_.ae * _.ae)
    val at = RamArrayType(elementType, allocationSize, BlockRamType())
    MemoryAllocationExpr(
      Value(at),
      Seq(identifier, description, elementType) ++ dimensions
    )
  }

  /**
    * allocate memory to fit given dataType
    */
  private[arch] def apply(identifier: TextTypeT, description: TextTypeT, dataType: HWDataTypeT): MemoryAllocationExpr = dataType match {
    case streamType: UnorderedStreamTypeT => apply(identifier, description, streamType.leafType, streamType.dimensions)
    case noStreamType: BasicDataTypeT => apply(identifier, description, noStreamType, Seq(ArithType(1)))
    case _ => throw new RuntimeException("never happens")
  }
}

object BankedBlockRam {
  private[arch] def apply(identifier: TextTypeT = TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()),
                          description: TextTypeT = TextType(""),
                          elementType: BasicDataTypeT,
                          numBanks: ArithTypeT,
                          dimensions: Seq[ArithTypeT],
                          addressPerBank: ArithTypeT = ArithType(0)): MemoryAllocationExpr = {
    val allocationSize = dimensions.fold(ArithType(1))(_.ae * _.ae)
    val allocationSizePerBank = 1 + (allocationSize.ae - 1) / numBanks.ae
    val at = if(addressPerBank.ae.evalInt > allocationSizePerBank.evalInt)
      RamArrayType(elementType, addressPerBank, BankedBlockRamType(numBanks))
    else
      RamArrayType(elementType, allocationSizePerBank, BankedBlockRamType(numBanks))
    MemoryAllocationExpr(
      Value(at),
      Seq(identifier, description, elementType) ++ dimensions
    )
  }

  private[arch] def apply(identifier: TextTypeT, description: TextTypeT, dataType: HWDataTypeT, numBanks: ArithTypeT): MemoryAllocationExpr = dataType match {
    case streamType: UnorderedStreamTypeT => apply(identifier, description, streamType.leafType, numBanks, streamType.dimensions)
    case noStreamType: BasicDataTypeT => apply(identifier, description, noStreamType, numBanks, Seq(ArithType(1)))
    case _ => throw new RuntimeException("never happens")
  }
}

object OnBoardRam {
  private[arch] def apply(identifier: TextTypeT = TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()),
                          description: TextTypeT = TextType(""),
                          origElementType: BasicDataTypeT,
                          dimensions: Seq[ArithTypeT]): MemoryAllocationExpr = {
    val allocationSize = dimensions.fold(ArithType(1))(_.ae * _.ae)
    val at = RamArrayType(FPGA.current.onBoardRamDataType, allocationSize, OnBoardRamType)
    MemoryAllocationExpr(
      Value(at),
      Seq(identifier, description, origElementType) ++ dimensions
    )
  }
}

object HostRam {
  private[arch] def apply(identifier: TextTypeT = TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()),
                          description: TextTypeT = TextType(""),
                          origElementType: BasicDataTypeT,
                          dimensions: Seq[ArithTypeT]): MemoryAllocationExpr = {
    val allocationSize = dimensions.fold(ArithType(1))(_.ae * _.ae)
    val at = RamArrayType(FPGA.current.cachelineType, allocationSize, HostRamType)
    MemoryAllocationExpr(
      Value(at),
      Seq(identifier, description, origElementType) ++ dimensions
    )
  }
}

case class MemoryRegionExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MemoryRegionExpr = MemoryRegionExpr(newInnerIR, newTypes)
}
object MemoryRegion {
  private[arch] def apply(identifier: TextTypeT, description: TextTypeT, baseAddr: ArithTypeT, origElementType: BasicDataTypeT, dimensions: Seq[ArithTypeT], ramArrayType: RamArrayTypeT): MemoryRegionExpr = {
    MemoryRegionExpr(
      Value(ramArrayType),
      Seq(identifier, description, baseAddr, origElementType) ++ dimensions
    )
  }
  def unapply(expr: MemoryRegionExpr): Some[(TextTypeT, TextTypeT, ArithTypeT, BasicDataTypeT, Seq[ArithTypeT], Type)] =
    Some((expr.types.head.asInstanceOf[TextTypeT], expr.types(1).asInstanceOf[TextTypeT], expr.types(2).asInstanceOf[ArithTypeT], expr.types(3).asInstanceOf[BasicDataTypeT], expr.types.tail.tail.tail.tail.map(_.asInstanceOf[ArithTypeT]), expr.t))
}

case class ReadExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadExpr = ReadExpr(newInnerIR)
}
object Read extends BuiltinExprFun {
  override def args: Int = 2
  /**
    * read data at the given offset(s) (input) from the allocated memory (memAllocation)
    * @param memAllocation allocated memory to read (of type ArrayType)
    * @param input offset in allocated area
    */
  private[arch] def apply(memAllocation: Expr, input: Expr): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    apply(memAllocation, inputtc, inputtc.t)
  }
  private def apply(memAllocation: Expr, input: Expr, inputType: Type): BuiltinExpr = inputType match {
    case streamType: UnorderedStreamTypeT =>
      MapOrderedStream(
        {
          val param = ParamDef(NonArrayTypeVar())
          ArchLambda(
            param,
            apply(memAllocation, ParamUse(param), streamType.et)
          )
        },
        input
      )
    case _: TypeVarT => throw MalformedTypeException("must be able to solve this type")
    case _ =>
      ReadExpr({
        val datatv = BasicDataTypeVar()
        val memAllocationtv = RamArrayTypeVar(datatv)
        val inputtv = BasicDataTypeVar()
        val outputDataType = datatv
        FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
          TypeFunType(
            Seq(memAllocationtv, inputtv),
            HWFunTypes(
              Seq(memAllocationtv, inputtv),
              outputDataType
            )
          )),
          Seq(memAllocation.t, input.t)),
          Seq(memAllocation, input)
        )
      })
  }

  /**
    * read *all* data from allocated memory (memAllocation)
    * @param memAllocation allocated memory to read (of type ArrayType)
    */
  private[arch] def apply(memAllocation: MemoryAllocationExpr): Expr = {
    val memAllocationtc = TypeChecker.checkIfUnknown(memAllocation, memAllocation.t)
    val MemoryAllocation(_, _, _, dimensions, _) = memAllocationtc
    val param = ParamDef(memAllocationtc.t)
    Let(
      param,
      apply(ParamUse(param), CounterInteger(0, 1, dimensions)),
      memAllocationtc
    )
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, inputs(1))
  def unapply(expr: ReadExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class ReadOutOfBoundExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadOutOfBoundExpr = ReadOutOfBoundExpr(newInnerIR, newTypes)
}
object ReadOutOfBound extends BuiltinExprFun {
  override def args: Int = 2
  /**
    * read data at the given offset(s) (input) from the allocated memory (memAllocation)
    * @param memAllocation allocated memory to read (of type ArrayType)
    * @param input offset in allocated area
    */
  private[arch] def apply(memAllocation: Expr, input: Expr, strategy: MemAddrStrategyT): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val memLen = memAllocation.t.asInstanceOf[RamArrayTypeT].len.ae.evalInt
    strategy match {
      case Mirroring =>
        val v = ArithTypeVar(GoesToRange(2 * memLen - 1))
        val c1 = Cst(memLen - 1)
        val c2 = Cst(2 * memLen - 2)
        apply(memAllocation, inputtc, ArithLambdaType(v, (v gt c1) ?? (c2 - v) !! v), inputtc.t)
      case Clipping =>
        val v = ArithTypeVar(GoesToRange(memLen + 1))
        val c = Cst(memLen - 1)
        apply(memAllocation, inputtc, ArithLambdaType(v, (v gt c) ?? c !! v), inputtc.t)
      case _ => apply(memAllocation, inputtc) // padding is default
    }
  }

  private[arch] def apply(memAllocation: Expr, input: Expr): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val memLen = memAllocation.t.asInstanceOf[RamArrayTypeT].len.ae.evalInt
    val v = ArithTypeVar(GoesToRange(memLen + 1))
    val c = Cst(memLen - 1)
    apply(memAllocation, inputtc, ArithLambdaType(v, (v gt c) ?? -1 !! v), inputtc.t)
  }

  private def apply(memAllocation: Expr, input: Expr, addrRule: ArithLambdaType, inputType: Type): BuiltinExpr = inputType match {
    case streamType: UnorderedStreamTypeT =>
      MapOrderedStream(
        {
          val param = ParamDef(NonArrayTypeVar())
          ArchLambda(
            param,
            apply(memAllocation, ParamUse(param), addrRule, streamType.et)
          )
        },
        input
      )
    case _: TypeVarT => throw MalformedTypeException("must be able to solve this type")
    case _ =>
      ReadOutOfBoundExpr({
        val datatv = BasicDataTypeVar()
        val memAllocationtv = RamArrayTypeVar(datatv)
        val inputtv = BasicDataTypeVar()
        val outputDataType = datatv
        FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
          TypeFunType(
            Seq(memAllocationtv, inputtv),
            HWFunTypes(
              Seq(memAllocationtv, inputtv),
              outputDataType
            )
          )),
          Seq(memAllocation.t, input.t)),
          Seq(memAllocation, input)
        )
      }, Seq(addrRule))
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, inputs(1))
  def unapply(expr: ReadOutOfBoundExpr): Some[(Expr, Expr, ArithLambdaType, Type)] =
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithLambdaType], expr.t))
}

case class WriteExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteExpr = WriteExpr(newInnerIR)
}
object Write extends BuiltinExprFun {
  override def args: Int = 2
  /**
    * write given data at given offset(s) (input) into the allocated memory (memAllocation)
    * @param memAllocation allocated memory to write (of type ArrayType)
    * @param input data and offset in allocated area
    */
  private[arch] def apply(memAllocation: ParamUse, input: Expr): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    apply(memAllocation, inputtc, inputtc.t)
  }
  private[arch] def apply(memAllocation: ParamUse, input: Expr, inputType: Type): BuiltinExpr = inputType match {
    case streamType: UnorderedStreamTypeT =>
      ReduceOrderedStream(
        {
          val accParam = ParamDef(RamArrayTypeVar())
          val inputParam = ParamDef(NonArrayTypeVar())
          ArchLambdas(
            Seq(accParam, inputParam),
            apply(ParamUse(accParam), ParamUse(inputParam), streamType.et)
          )
        },
        memAllocation,
        input
      )
    case TupleType(entries) =>
      WriteExpr({
        val tvs = Seq.fill(entries.size)(BasicDataTypeVar())
        val memAllocationtv = RamArrayTypeVar(tvs.head)
        val inputTupletv = TupleTypeVar(tvs: _*)
        val outputArrayType = memAllocationtv
        FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
          TypeFunType(
            Seq(memAllocationtv, inputTupletv),
            HWFunTypes(
              Seq(memAllocationtv, inputTupletv),
              outputArrayType
            )
          )),
          Seq(memAllocation.t, input.t)),
          Seq(memAllocation, input)
        )
      })
    case _ =>
      throw MalformedTypeException("must be able to solve this type as a tuple")
  }

  private[arch] def apply(memAllocation: MemoryAllocationExpr, input: Expr): Expr = {
    val memAllocationtc = TypeChecker.checkIfUnknown(memAllocation, memAllocation.t)
    val param = ParamDef(memAllocationtc.t)
    Let(
      param,
      apply(ParamUse(param), input),
      memAllocationtc
    )
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head.asInstanceOf[ParamUse], inputs(1))
  def unapply(expr: WriteExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

/*
 ***********************************************************************************************************************
 low-level memory expressions (no ArrayType! Ram is modelled as a function)
 ***********************************************************************************************************************
 */

/*
 ***********************************************************************************************************************
 SYNC
 ***********************************************************************************************************************
 */

case class ReadSyncExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadSyncExpr = ReadSyncExpr(newInnerIR)
}
object ReadSync extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param readMemoryController function which takes an address and returns the data at this address
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this read
    * @param input offset to the base address
    */
  private[arch] def apply(readMemoryController: Expr, baseAddr: Expr, input: Expr): ReadSyncExpr = ReadSyncExpr({
    val datatv = BasicDataTypeVar()
    val relAddrTv = IntTypeVar()
    val absAddrTv = IntTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = FunTypeVar(absAddrTv, datatv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, relAddrTv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, relAddrTv),
          datatv
        )
      )),
      Seq(readMemoryController.t, baseAddr.t, input.t)),
      Seq(readMemoryController, baseAddr, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadSyncExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: ReadSyncExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class WriteSyncExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteSyncExpr = WriteSyncExpr(newInnerIR)
}
object WriteSync extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param writeMemoryController function which takes an address and data and returns unit
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this write
    * @param input offset to the base address with data
    */
  private[arch] def apply(writeMemoryController: Expr, baseAddr: Expr, input: Expr): WriteSyncExpr = WriteSyncExpr({
    val datatv = BasicDataTypeVar()
    val relAddrTv = IntTypeVar()
    val absAddrTv = IntTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = HWFunTypeVar(AddressedDataTypeVar(datatv, absAddrTv), UnitType())
    val inputtv = TupleTypeVar(datatv, relAddrTv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, inputtv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, inputtv),
          baseaddrtv
        )
      )),
      Seq(writeMemoryController.t, baseAddr.t, input.t)),
      Seq(writeMemoryController, baseAddr, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): WriteSyncExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: WriteSyncExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class ReadSyncMemoryControllerExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadSyncMemoryControllerExpr =
    ReadSyncMemoryControllerExpr(newInnerIR)
}
object ReadSyncMemoryController extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(ram: Expr, input: Expr): ReadSyncMemoryControllerExpr = ReadSyncMemoryControllerExpr({
    val datatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val ramftv = FunTypeVar(SyncRamInputType(datatv, addrtv), datatv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(ramftv, addrtv),
        HWFunTypes(
          Seq(ramftv, addrtv),
          datatv
        )
      )),
      Seq(ram.t, input.t)),
      Seq(ram, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadSyncMemoryControllerExpr = apply(inputs.head, inputs(1))
  def unapply(expr: ReadSyncMemoryControllerExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class WriteSyncMemoryControllerExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteSyncMemoryControllerExpr =
    WriteSyncMemoryControllerExpr(newInnerIR)
}
object WriteSyncMemoryController extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(ram: Expr, input: Expr): WriteSyncMemoryControllerExpr = WriteSyncMemoryControllerExpr({
    val datatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val ramftv = FunTypeVar(SyncRamInputType(datatv, addrtv), datatv)
    val inputtv = AddressedDataTypeVar(datatv, addrtv)
    val outputt = UnitType()

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(ramftv, inputtv),
        HWFunTypes(
          Seq(ramftv, inputtv),
          outputt
        )
      )),
      Seq(ram.t, input.t)),
      Seq(ram, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): WriteSyncMemoryControllerExpr = apply(inputs.head, inputs(1))
  def unapply(expr: WriteSyncMemoryControllerExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class BlockRamBufferExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): BlockRamBufferExpr = BlockRamBufferExpr(newInnerIR, newTypes)
}
object BlockRamBuffer extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, at: RamArrayTypeT): BlockRamBufferExpr = apply(input, getFunType(at), at.len)
  private[arch] def apply(input: Expr, ramFunType: HWFunTypeT, len: ArithTypeT): BlockRamBufferExpr =
    BlockRamBufferExpr(FunctionCall(BuiltinFunction(ramFunType), input), Seq(ramFunType, len))
  private def getFunType(at: RamArrayTypeT): HWFunTypeT =
    at.memoryLocation match {
      case _: BlockRamTypeT =>
        HWFunType(
          SyncRamInputType(at.et, at.len),
          at.et
        )
      case _ => throw MalformedExprException("Block Rams must have sync ports")
    }

  // TODO maybe remove
  private[arch] def asFunction(at: RamArrayTypeT): ArchLambda = {
    val ramInputParam = ParamDef(getFunType(at).inType)
    ArchLambda(ramInputParam, apply(ParamUse(ramInputParam), at))
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BlockRamBufferExpr = apply(inputs.head, types.head.asInstanceOf[HWFunTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: BlockRamBufferExpr): Some[(Expr, HWFunTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[HWFunTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

/*
 ***********************************************************************************************************************
 BANKED
 ***********************************************************************************************************************
 */
case class ReadSyncBankedExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadSyncBankedExpr = ReadSyncBankedExpr(newInnerIR)
}
object ReadSyncBanked extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param readMemoryController function which takes an address and returns the data at this address
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this read
    * @param input offset to the base address
    */
  private[arch] def apply(readMemoryController: Expr, baseAddr: Expr, input: Expr): ReadSyncBankedExpr = ReadSyncBankedExpr({
    val datatv = BasicDataTypeVar()
    val relAddrTv = BasicDataTypeVar()//IntTypeVar()
    val absAddrTv = BasicDataTypeVar()//IntTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = FunTypeVar(absAddrTv, datatv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, relAddrTv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, relAddrTv),
          datatv
        )
      )),
      Seq(readMemoryController.t, baseAddr.t, input.t)),
      Seq(readMemoryController, baseAddr, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadSyncBankedExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: ReadSyncBankedExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class WriteSyncBankedExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteSyncBankedExpr = WriteSyncBankedExpr(newInnerIR)
}
object WriteSyncBanked extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param writeMemoryController function which takes an address, data, and write-enabled and returns unit
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this write
    * @param input offset to the base address with data
    */
  private[arch] def apply(writeMemoryController: Expr, baseAddr: Expr, input: Expr): WriteSyncBankedExpr = WriteSyncBankedExpr({
    val datatv = BasicDataTypeVar()
    val relAddrTv = BasicDataTypeVar()//IntTypeVar()
    val absAddrTv = BasicDataTypeVar()//IntTypeVar()
    val weTv = BasicDataTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = HWFunTypeVar(BankedBlockRamInputType(datatv, absAddrTv, weTv), UnitType())
    val inputtv = TupleTypeVar(datatv, relAddrTv, weTv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, inputtv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, inputtv),
          baseaddrtv
        )
      )),
      Seq(writeMemoryController.t, baseAddr.t, input.t)),
      Seq(writeMemoryController, baseAddr, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): WriteSyncBankedExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: WriteSyncBankedExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class ReadSyncBankedMemoryControllerExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  //override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadSyncBankedMemoryControllerExpr =
    ReadSyncBankedMemoryControllerExpr(newInnerIR, newTypes)
}
object ReadSyncBankedMemoryController extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(ram: Expr, input: Expr, numBanks: ArithTypeT): ReadSyncBankedMemoryControllerExpr = ReadSyncBankedMemoryControllerExpr({
    val datatv = BasicDataTypeVar()
    val addrtv = BasicDataTypeVar()//IntTypeVar()
    val ramftv = FunTypeVar(BankedBlockRamInputType(datatv, addrtv, numBanks), datatv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(ramftv, addrtv),
        HWFunTypes(
          Seq(ramftv, addrtv),
          datatv
        )
      )),
      Seq(ram.t, input.t)),
      Seq(ram, input)
    )
  }, Seq(numBanks))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadSyncBankedMemoryControllerExpr = apply(inputs.head, inputs(1), types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: ReadSyncBankedMemoryControllerExpr): Some[(Expr, Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class WriteSyncBankedMemoryControllerExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteSyncBankedMemoryControllerExpr =
    WriteSyncBankedMemoryControllerExpr(newInnerIR)
}
object WriteSyncBankedMemoryController extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(ram: Expr, input: Expr): WriteSyncBankedMemoryControllerExpr = WriteSyncBankedMemoryControllerExpr({
    val datatv = BasicDataTypeVar()
    val addrtv = BasicDataTypeVar()//IntTypeVar()
    val weTv = BasicDataTypeVar()
    val ramftv = FunTypeVar(BankedBlockRamInputType(datatv, addrtv, weTv), datatv)
    val inputtv = BankedBlockRamInputTypeVar(datatv, addrtv, weTv)
    val outputt = UnitType()

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(ramftv, inputtv),
        HWFunTypes(
          Seq(ramftv, inputtv),
          outputt
        )
      )),
      Seq(ram.t, input.t)),
      Seq(ram, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): WriteSyncBankedMemoryControllerExpr = apply(inputs.head, inputs(1))
  def unapply(expr: WriteSyncBankedMemoryControllerExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class SyncBankedBufferExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SyncBankedBufferExpr = SyncBankedBufferExpr(newInnerIR, newTypes)
}
object SyncBankedBuffer extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, at: RamArrayTypeT): SyncBankedBufferExpr = apply(input, getFunType(at), at.len, at.memoryLocation.asInstanceOf[BankedBlockRamType].numBanks)
  private[arch] def apply(input: Expr, ramFunType: HWFunTypeT, len: ArithTypeT, numBanks: ArithTypeT): SyncBankedBufferExpr =
    SyncBankedBufferExpr(FunctionCall(BuiltinFunction(ramFunType), input), Seq(ramFunType, len, numBanks))
  private def getFunType(at: RamArrayTypeT): HWFunTypeT =
    at.memoryLocation match {
      case _: BankedBlockRamTypeT =>
        HWFunType(
          BankedBlockRamInputType(at.et, at.len, at.memoryLocation.asInstanceOf[BankedBlockRamType].numBanks),
          at.et
        )
      case _ => throw MalformedExprException("Block Rams must have sync ports")
    }

  // TODO maybe remove
  private[arch] def asFunction(at: RamArrayTypeT): ArchLambda = {
    val ramInputParam = ParamDef(getFunType(at).inType)
    ArchLambda(ramInputParam, apply(ParamUse(ramInputParam), at))
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SyncBankedBufferExpr = apply(inputs.head, types.head.asInstanceOf[HWFunTypeT], types(1).asInstanceOf[ArithTypeT], types(2).asInstanceOf[ArithTypeT])
  def unapply(expr: SyncBankedBufferExpr): Some[(Expr, HWFunTypeT, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[HWFunTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.types(2).asInstanceOf[ArithTypeT],  expr.t))
}

/*
 ***********************************************************************************************************************
 ASYNC
 ***********************************************************************************************************************
 */

case class ReadAsyncExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadAsyncExpr = ReadAsyncExpr(newInnerIR)
}
object ReadAsync extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param readMemoryController function which takes a request (address and req id) and returns a response (data and req id)
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this read
    * @param input stream, with offsets to the base address, where the requested data is located
    */
  private[arch] def apply(readMemoryController: Expr, baseAddr: Expr, input: Expr): ReadAsyncExpr = ReadAsyncExpr({
    val datatv = BasicDataTypeVar()
    val metadatatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = HWFunTypeVar(RequestType(IntTypeVar(), metadatatv), RequestType(datatv, metadatatv))

    val lentv = ArithTypeVar()
    val inputtv = OrderedStreamTypeVar(addrtv, lentv)
    val output = UnorderedStreamType(datatv, lentv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, inputtv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, inputtv),
          output
          // the read itself is sync, but the memory controller it uses is async
        )
      )),
      Seq(readMemoryController.t, baseAddr.t, input.t)),
      Seq(readMemoryController, baseAddr, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadAsyncExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: ReadAsyncExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class ReadAsyncOrderedExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadAsyncOrderedExpr = ReadAsyncOrderedExpr(newInnerIR)
}
object ReadAsyncOrdered extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param readMemoryController function which takes a request (address and req id) and returns a response (data and req id)
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this read
    * @param input stream, with offsets to the base address, where the requested data is located
    */
  private[arch] def apply(readMemoryController: Expr, baseAddr: Expr, input: Expr): ReadAsyncOrderedExpr = ReadAsyncOrderedExpr({
    val datatv = BasicDataTypeVar()
    val metadatatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = HWFunTypeVar(RequestType(IntTypeVar(), metadatatv), RequestType(datatv, metadatatv))

    val lentv = ArithTypeVar()
    val inputtv = OrderedStreamTypeVar(addrtv, lentv)
    val output = OrderedStreamType(datatv, lentv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, inputtv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, inputtv),
          output
          // the read itself is sync, but the memory controller it uses is async
        )
      )),
      Seq(readMemoryController.t, baseAddr.t, input.t)),
      Seq(readMemoryController, baseAddr, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadAsyncOrderedExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: ReadAsyncOrderedExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class ReadAsyncOrderedOutOfBoundExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadAsyncOrderedOutOfBoundExpr = ReadAsyncOrderedOutOfBoundExpr(newInnerIR, newTypes)
}
object ReadAsyncOrderedOutOfBound extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param readMemoryController function which takes a request (address and req id) and returns a response (data and req id)
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this read
    * @param input stream, with offsets to the base address, where the requested data is located
    */
  private[arch] def apply(readMemoryController: Expr, baseAddr: Expr, input: Expr, addrRule: ArithLambdaType): ReadAsyncOrderedOutOfBoundExpr = ReadAsyncOrderedOutOfBoundExpr({
    val datatv = BasicDataTypeVar()
    val metadatatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = HWFunTypeVar(RequestType(IntTypeVar(), metadatatv), RequestType(datatv, metadatatv))

    val lentv = ArithTypeVar()
    val inputtv = OrderedStreamTypeVar(addrtv, lentv)
    val output = OrderedStreamType(datatv, lentv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, inputtv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, inputtv),
          output
          // the read itself is sync, but the memory controller it uses is async
        )
      )),
      Seq(readMemoryController.t, baseAddr.t, input.t)),
      Seq(readMemoryController, baseAddr, input)
    )
  }, Seq(addrRule))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadAsyncOrderedOutOfBoundExpr = apply(inputs.head, inputs(1), inputs(2), types.head.asInstanceOf[ArithLambdaType])
  def unapply(expr: ReadAsyncOrderedOutOfBoundExpr): Some[(Expr, Expr, Expr, ArithLambdaType, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.types.head.asInstanceOf[ArithLambdaType], expr.t))
}

case class WriteAsyncExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteAsyncExpr = WriteAsyncExpr(newInnerIR)
}
object WriteAsync extends BuiltinExprFun {
  override def args: Int = 3
  /**
    * @param writeMemoryController function which takes a request (data and address and req id) and returns a response (req id)
    * @param baseAddr expression that provides the base address (int) of the allocated memory for this write
    * @param input stream, with data and offsets to the base address, where the data shall be written
    */
  private[arch] def apply(writeMemoryController: Expr, baseAddr: Expr, input: Expr): WriteAsyncExpr = WriteAsyncExpr({
    val datatv = BasicDataTypeVar()
    val metadatatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val baseaddrtv = IntTypeVar()
    val memcontrollerftv = HWFunTypeVar(RequestType(AddressedDataType(datatv, IntTypeVar()), metadatatv), RequestType(datatv, metadatatv))

    val lentv = ArithTypeVar()
    val inputtv = OrderedStreamTypeVar(TupleTypeVar(datatv, addrtv), lentv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(memcontrollerftv, baseaddrtv, inputtv),
        HWFunTypes(
          Seq(memcontrollerftv, baseaddrtv, inputtv),
          baseaddrtv
          // the read itself is sync, but the memory controller it uses is async
        )
      )),
      Seq(writeMemoryController.t, baseAddr.t, input.t)),
      Seq(writeMemoryController, baseAddr, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): WriteAsyncExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: WriteAsyncExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class ReadHostMemoryControllerExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadHostMemoryControllerExpr = ReadHostMemoryControllerExpr(newInnerIR, newTypes)
}
object ReadHostMemoryController extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, specs: FPGASpecs): ReadHostMemoryControllerExpr =
    apply(input, specs.hostRamReadInterface, specs.readReqBufferSize)
  private[arch] def apply(input: Expr, ramInterface: HWFunTypeT, reqBufferSize: ArithTypeT): ReadHostMemoryControllerExpr = ReadHostMemoryControllerExpr({
    FunctionCall(BuiltinFunction(
      ramInterface),
      input
    )
  }, Seq(ramInterface, reqBufferSize))

  // TODO maybe remove, but async!!
  private[arch] def asFunction(specs: FPGASpecs): ArchLambda = {
    val param = ParamDef(specs.hostRamReadInterface.inType)
    ArchLambda(param, apply(ParamUse(param), specs), AsyncCommunication, FirstComeFirstServedSchedulingType)
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadHostMemoryControllerExpr = apply(inputs.head, types.head.asInstanceOf[HWFunTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: ReadHostMemoryControllerExpr): Some[(Expr, HWFunTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[HWFunTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class WriteHostMemoryControllerExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteHostMemoryControllerExpr = WriteHostMemoryControllerExpr(newInnerIR, newTypes)
}
object WriteHostMemoryController extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, specs: FPGASpecs): WriteHostMemoryControllerExpr =
    apply(input, specs.hostRamWriteInterface, specs.writeReqBufferSize) //TODO: Add a similar field as reqBufferSize to local controllers
  private[arch] def apply(input: Expr, ramInterface: HWFunTypeT, reqBufferSize: ArithTypeT): WriteHostMemoryControllerExpr = WriteHostMemoryControllerExpr({
    FunctionCall(BuiltinFunction(
      ramInterface),
      input
    )
  }, Seq(ramInterface, reqBufferSize))

  // TODO maybe remove, but async!!
  private[arch] def asFunction(specs: FPGASpecs): ArchLambda = {
    val param = ParamDef(specs.hostRamWriteInterface.inType)
    ArchLambda(param, WriteHostMemoryController(ParamUse(param), specs), AsyncCommunication, FirstComeFirstServedSchedulingType)
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): WriteHostMemoryControllerExpr = apply(inputs.head, types.head.asInstanceOf[HWFunTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: WriteHostMemoryControllerExpr): Some[(Expr, HWFunTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[HWFunTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class ReadLocalMemoryControllerExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReadLocalMemoryControllerExpr =
    ReadLocalMemoryControllerExpr(newInnerIR)
}
object ReadLocalMemoryController extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(ram: Expr, input: Expr): ReadLocalMemoryControllerExpr = ReadLocalMemoryControllerExpr({
    val datatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val reqidtv = VectorTypeVar()
    val ramftv = HWFunTypeVar(AsyncRamInputType(datatv, addrtv, reqidtv), RequestType(datatv, reqidtv))
    val inputtv = RequestTypeVar(addrtv, reqidtv)
    val outputt = RequestType(datatv, reqidtv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(ramftv, inputtv),
        HWFunType(
          inputtv,
          HWFunType(
            ramftv,
            outputt
          ),
          AsyncCommunication
        )
      )),
      Seq(ram.t, input.t)),
      Seq(ram, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReadLocalMemoryControllerExpr = apply(inputs.head, inputs(1))
  def unapply(expr: ReadLocalMemoryControllerExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class WriteLocalMemoryControllerExpr private[arch](innerIR: Expr) extends MemExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): WriteLocalMemoryControllerExpr =
    WriteLocalMemoryControllerExpr(newInnerIR)
}
object WriteLocalMemoryController extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(ram: Expr, input: Expr): WriteLocalMemoryControllerExpr = WriteLocalMemoryControllerExpr({
    val datatv = BasicDataTypeVar()
    val addrtv = IntTypeVar()
    val reqidtv = VectorTypeVar(LogicType(), 16)
    val ramftv = HWFunTypeVar(AsyncRamInputType(datatv, addrtv, reqidtv), RequestType(datatv, reqidtv))
    val inputtv = RequestTypeVar(AddressedDataType(datatv, addrtv), reqidtv)
    val outputt = RequestType(datatv, reqidtv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(ramftv, inputtv),
        HWFunType(
          inputtv,
          HWFunType(
            ramftv,
            outputt
          ),
          AsyncCommunication
        )
      )),
      Seq(ram.t, input.t)),
      Seq(ram, input)
    )
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): WriteLocalMemoryControllerExpr = apply(inputs.head, inputs(1))
  def unapply(expr: WriteLocalMemoryControllerExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class OnBoardRamBufferExpr private[arch](innerIR: Expr, types: Seq[Type]) extends MemExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): OnBoardRamBufferExpr = OnBoardRamBufferExpr(newInnerIR, newTypes)
}
object OnBoardRamBuffer extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, specs: FPGASpecs): OnBoardRamBufferExpr =
    apply(input, specs.onBoardRamDataType, specs.onBoardRamSize, VectorType(LogicType(), specs.onBoardRamBurstWidth), specs.onBoardRamReqIdSize)
  private[arch] def apply(input: Expr, dataType: BasicDataTypeT, size: ArithTypeT, burstType: BasicDataTypeT, reqIdSize: ArithTypeT): OnBoardRamBufferExpr = OnBoardRamBufferExpr({
    FunctionCall(BuiltinFunction(
      HWFunType(
        AsyncRamInputType(dataType, size, reqIdSize),
        RequestType(dataType, VectorType(LogicType(), reqIdSize)),
        AsyncCommunication
      )),
      input
    )
  }, Seq(dataType, size, burstType, reqIdSize))

  // TODO maybe remove
  private[arch] def asFunction(specs: FPGASpecs): ArchLambda = {
    val param = ParamDef(AsyncRamInputType(specs.onBoardRamDataType, specs.onBoardRamSize, specs.onBoardRamReqIdSize))
    ArchLambda(param, apply(ParamUse(param), specs), AsyncCommunication)
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): OnBoardRamBufferExpr = apply(inputs.head, types(1).asInstanceOf[BasicDataType], types(2).asInstanceOf[ArithTypeT], types(3).asInstanceOf[BasicDataTypeT], types(4).asInstanceOf[ArithTypeT])
  def unapply(expr: OnBoardRamBufferExpr): Some[(Expr, BasicDataTypeT, ArithTypeT, BasicDataTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types(0).asInstanceOf[BasicDataTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.types(2).asInstanceOf[BasicDataTypeT], expr.types(3).asInstanceOf[ArithTypeT], expr.t))
}

object Input {
  def apply(identifier: String, numInnerElements: ArithTypeT, numOuterElements: ArithTypeT, elementType: BasicDataTypeT): Expr =
    apply(TextType(identifier), elementType, Seq(numInnerElements, numOuterElements), None, None)

  def apply(identifier: String, elementType: BasicDataTypeT, numInnerElements: ArithTypeT, numOuterElements: ArithTypeT): Expr =
    apply(TextType(identifier), elementType, Seq(numInnerElements, numOuterElements), None, None)

  def apply(identifier: TextTypeT, elementType: BasicDataTypeT, dimensions: Seq[ArithTypeT]): Expr =
    apply(identifier, elementType, dimensions, None, None)

  def apply(identifier: TextTypeT, elementType: BasicDataTypeT, dimensions: Seq[ArithTypeT], algoTypeText: TextTypeT): Expr =
    apply(identifier, elementType, dimensions, Some(algoTypeText), None)

  def apply(identifier: TextTypeT, elementType: BasicDataTypeT, dimensions: Seq[ArithTypeT], memWrite: Expr): Expr =
    apply(identifier, elementType, dimensions, None, Some(memWrite))

  def apply(identifier: TextTypeT, elementType: BasicDataTypeT, dimensions: Seq[ArithTypeT], algoTypeText: Option[TextTypeT], memWrite: Option[Expr]): Expr = {

    val desiredDataType = OrderedStreamType(dimensions, elementType)
    val desiredRowType = UnorderedStreamType(elementType, dimensions.head)
    val cacheLinesPerRow = ArchConversion.calcLength(desiredRowType, FPGA.current.cachelineType)
    val dimensionsForMemory = cacheLinesPerRow +: dimensions.tail // replace number of elements per row by number of cachelines required to fit an entire row

    val mapRead = memWrite match {
      case Some(writeMem) =>
        val writeMemtc = TypeChecker.check(writeMem)
        val writeParam = ParamDef(writeMemtc.t)
        Let(
          writeParam,
          Read(
            ParamUse(writeParam),
            CounterInteger(0, 1, dimensionsForMemory)
          ),
          writeMemtc
        )
      case None =>
        val typeText = algoTypeText match {
          case Some(text) => text
          case None => TextType(desiredDataType + "")
        }
        val mem = HostRam(identifier, typeText, elementType, dimensionsForMemory)
        // TODO buffer here tends to get better performance (because larger elements are stored)
        Read(mem)
      }
    val mapReadtc = TypeChecker.check(mapRead)

    Marker(
      ArchConversion(
        Marker(
          mapReadtc,
          TextType("input_" + identifier.s)
        ),
        mapReadtc.t,
        desiredDataType
      ),
      TextType("map_fusion_guard")
    )
  }
}

object StoreResult {
  def apply(input: Expr): Expr = apply(input, TextType(MemoryImage.RESULT_VAR_NAME))

  def apply(input: Expr, identifier: TextTypeT): Expr = {
    val archExpr = TypeChecker.check(input)
    // wrap into a stream if data type is not a stream (writing back data requires a stream type)
    val archExprWrapped = TypeChecker.check(archExpr.t match {
      //        case _: OrderedStreamTypeT => OrderedStreamToUnorderedStream(archExpr)
      case _: UnorderedStreamTypeT => archExpr
      case _ => Repeat(archExpr, 1)
    })

    val givenType = archExprWrapped.t.asInstanceOf[UnorderedStreamTypeT]

    val givenRowType = UnorderedStreamType(givenType.leafType, givenType.dimensions.head)
    val cacheLinesPerRow = ArchConversion.calcLength(givenRowType, FPGA.current.cachelineType)

    // replace number of elements per row by number of cachelines required to fit an entire row
    val typeForMemory = OrderedStreamType(givenType.dimensions.tail, UnorderedStreamType(FPGA.current.cachelineType, cacheLinesPerRow))

    val convertedInputData =
      IndexStream(
        ArchConversion(archExprWrapped, archExprWrapped.t, typeForMemory)
      )
    val mem = HostRam(identifier, TextType(input.t + ""), givenType.leafType, typeForMemory.dimensions)

    Write(mem, convertedInputData)
  }
}

object BufferStreamInBlockRam extends BufferStream {
  def apply(input: Expr): Expr = apply(input, Block)
}
object BufferStreamInOnBoardRam extends BufferStream {
  def apply(input: Expr): Expr = apply(input, OnBoard)
}
object BufferStreamInHostRam extends BufferStream {
  def apply(input: Expr, identifier: TextTypeT): Expr = apply(input, Host(identifier))
}

sealed trait BufferStream {
  protected def apply(input: Expr, targetMem: MemLoc): Expr = {
    val inputtc = TypeChecker.check(input)
    inputtc.t match {
      case streamType: UnorderedStreamTypeT =>
        targetMem match {
          case Block => apply(JoinStreamAll(input), streamType, false)
          case OnBoard => apply(JoinStreamAll(input), streamType, true)
          case Host(identifier) => apply(input, identifier)
        }
      case _ => ???
      // TODO generate Stream
    }
  }

  private def apply(flatStream: Expr, originalStreamType: UnorderedStreamTypeT, useOnBoardRam: Boolean): Expr = {
    val memAllocation = if (useOnBoardRam) {
      OnBoardRam(TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()), TextType(originalStreamType + ""), originalStreamType.leafType, originalStreamType.dimensions)
    } else {
      BlockRam(TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()), TextType(originalStreamType + ""), originalStreamType.leafType, originalStreamType.dimensions)
    }
    //    val memAllocation = BlockRam(TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()), TextType(originalStreamType + ""), originalStreamType.leafType, originalStreamType.dimensions)
    val memAllocationtc = TypeChecker.checkIfUnknown(memAllocation, memAllocation.t)

    val memAllocParam = ParamDef(memAllocationtc.t)
    val write =
      Write(
        ParamUse(memAllocParam),
        IndexStream(flatStream)
      )
    val writeParam = ParamDef(write.t)

    Let(
      memAllocParam,
      Let(
        writeParam,
        Read(
          ParamUse(writeParam),
          CounterInteger(0, 1, originalStreamType.dimensions)
        ),
        write
      ),
      memAllocationtc
    )
  }

  private def apply(input: Expr, identifier: TextTypeT): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val mem = TypeChecker.check(StoreResult(inputtc, identifier))
    Input(identifier, inputtc.t.asInstanceOf[OrderedStreamTypeT].leafType, inputtc.t.asInstanceOf[OrderedStreamTypeT].dimensions, mem)
  }
}


object BufferStream2DInBlockRam {
  def apply(input: Expr): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val vecInput = TypeChecker.check(MapOrderedStream(OrderedStreamToVector.asFunction(), inputtc))
    val intBitWidth = vecInput.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].et.asInstanceOf[IntTypeT].bitWidth
    val vecBitWidth = vecInput.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].bitWidth
    val convertedInput = MapOrderedStream(
      {
        val param1 = ParamDef(vecInput.t.asInstanceOf[OrderedStreamTypeT].leafType)
        ArchLambda(
          param1,
          Conversion(JoinVector(MapVector(
            {
              val param2 = ParamDef(vecInput.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].et)
              ArchLambda(
                param2,
                Conversion(ParamUse(param2), VectorType(LogicType(), intBitWidth))
              )
            }, ParamUse(param1)
          )), if(inputtc.t.asInstanceOf[OrderedStreamTypeT].leafType.isInstanceOf[SignedIntTypeT]) SignedIntType(vecBitWidth) else IntType(vecBitWidth))
        )
      }, vecInput
    )
    val convertedInputtc = TypeChecker.check(convertedInput)
    val bufferedOutput = TypeChecker.check(BufferStreamInBlockRam(convertedInputtc))

    val convertedOutput = MapOrderedStream(
      {
        val param1 = ParamDef(bufferedOutput.t.asInstanceOf[OrderedStreamTypeT].leafType)
        ArchLambda(
          param1,
          MapVector(
            {
              val param2 = ParamDef()
              ArchLambda(
                param2,
                Conversion(ParamUse(param2),
                  if(inputtc.t.asInstanceOf[OrderedStreamTypeT].leafType.isInstanceOf[SignedIntTypeT]) SignedIntType(intBitWidth) else IntType(intBitWidth))
              )
            }, SplitVector(Conversion(ParamUse(param1), VectorType(LogicType(), vecBitWidth)), intBitWidth)
          )
        )
      }, bufferedOutput
    )
    val convertedOutputtc = TypeChecker.check(convertedOutput)
    TypeChecker.check(MapOrderedStream(VectorToOrderedStream.asFunction(), convertedOutputtc))
  }
}

object BankedBufferRWStream {
  def apply(input: Expr, writeAddr:Expr, writeWe: Expr, readAddr: Expr, numBanks: ArithTypeT): Expr = {
    val inputtc = TypeChecker.check(input)
    inputtc.t match {
      case streamType: UnorderedStreamTypeT =>
        apply(JoinStreamAll(input), writeAddr, writeWe, readAddr, streamType, numBanks, ArithType(0))
      case _ => ???
      // TODO generate Stream
    }
  }
  def apply(input: Expr, writeAddr:Expr, writeWe: Expr, readAddr: Expr, numBanks: ArithTypeT, addressPerBank: ArithTypeT): Expr = {
    val inputtc = TypeChecker.check(input)
    inputtc.t match {
      case streamType: UnorderedStreamTypeT =>
        apply(JoinStreamAll(input), writeAddr, writeWe, readAddr, streamType, numBanks, addressPerBank)
      case _ => ???
      // TODO generate Stream
    }
  }
  /**
    * @param addressPerBank the number of addresses per bank. By default, the number of addresses per bank is set to length(flatStream) / numBanks.
    *                       This parameter is to manually assign the number.
    */
  private def apply(flatStream: Expr, writeAddr:Expr, writeWe: Expr, readAddr:Expr, originalStreamType: UnorderedStreamTypeT, numBanks: ArithTypeT, addressPerBank: ArithTypeT = ArithType(0)): Expr = {
    val memAllocation = BankedBlockRam(TextType("mem_id_" + Counter(MemoryAllocationExpr.getClass).next()), TextType(originalStreamType + ""), originalStreamType.leafType, numBanks, originalStreamType.dimensions, addressPerBank)
    val memAllocationtc = TypeChecker.checkIfUnknown(memAllocation, memAllocation.t)

    val inputTuple = Zip3OrderedStream(Tuple3(flatStream, writeAddr, writeWe))

    val memAllocParam = ParamDef(memAllocationtc.t)
    val write =
      Write(
        ParamUse(memAllocParam),
        inputTuple
      )
    val writeParam = ParamDef(write.t)

    Let(
      memAllocParam,
      Let(
        writeParam,
        Read(
          ParamUse(writeParam),
          readAddr
        ),
        write
      ),
      memAllocationtc
    )
  }
}