package backend.hdl.arch.mem

sealed trait MemAddrStrategyT

case object Padding extends MemAddrStrategyT
case object Clipping extends MemAddrStrategyT
case object Mirroring extends MemAddrStrategyT
