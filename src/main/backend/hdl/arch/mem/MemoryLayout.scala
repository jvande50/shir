package backend.hdl.arch.mem

import backend.hdl.{BaseAddrInitType, HWDataTypeT, HostRamType, IntType, IntTypeT, RamArrayTypeT, SignedIntType, SignedIntTypeT}
import backend.hdl.arch.ConstantValue
import core.util.ToFile
import core.{ArithType, ArithTypeT, Expr}

import scala.collection.mutable

class MemoryLayout private (info: Map[String, (Long, HWDataTypeT, Seq[ArithTypeT])]) extends ToFile {

  def baseAddrs: Map[String, Long] = info.mapValues(_._1)
  def metaData: Map[String, (Seq[ArithTypeT], HWDataTypeT)] = info.mapValues(p => (p._3, p._2))

  override def toStringIterator: Iterator[String] = {
    info.view.toSeq.sortBy(_._2._1).flatMap({ case (name, (addr, ty, dims)) =>
      val tyname = ty match {
        case t: SignedIntTypeT => s"s${t.bitWidth.ae.evalInt}"
        case t: IntTypeT => s"u${t.bitWidth.ae.evalInt}"
        case _ => s"<${ty}>"
      }
      Seq(
        s"${name}\t${addr.toHexString}\t${tyname}",
        dims.view.map(_.ae.evalInt).mkString(",")
      )
    }).toIterator
  }

  override def toString: String = info.toString()
}

object MemoryLayout {

  def apply(expr: Expr): MemoryLayout = {
    assert(!expr.hasUnknownType, "Expression contains free type variables. Cannot generate memory images.")
    expr.visit({
      case MemoryAllocation(_, _, _, _, _) => throw MemoryException("Memory must be allocated into regions first!")
      case _ =>
    })

    val info: mutable.Map[String, (Long, HWDataTypeT, Seq[ArithTypeT])] = mutable.Map()
    expr.visit({
      case MemoryRegion(identifier, _, baseAddr, origElementType, dims, t: RamArrayTypeT) if t.memoryLocation == HostRamType =>
        info.put(identifier.s, (baseAddr.ae.evalLong, origElementType, dims))
      case ConstantValue(baseAddr, BaseAddrInitType(_, identifier, _, memoryLocation, origElementType, dims)) if memoryLocation == HostRamType =>
        info.put(identifier.s, (baseAddr.ae.evalLong, origElementType, dims))
      case _ => Unit
    })
    new MemoryLayout(info.toMap)
  }

  def fromFile(fileName: String): MemoryLayout = {
    val s = io.Source.fromFile(fileName)
    val m = s.getLines().grouped(2).map({ case Seq(info, dimstr) =>
      val dims: Seq[ArithTypeT] = dimstr.split(',').map(i => ArithType(i.toInt))
      info.split('\t') match {
        case Array(name, addr, tystr) =>
          val ty = tystr(0) match {
            case 's' => SignedIntType(tystr.tail.toInt)
            case 'u' => IntType(tystr.tail.toInt)
          }
          name -> (java.lang.Long.parseLong(addr, 16), ty, dims)
      }
    }).toMap
    s.close()
    new MemoryLayout(m)
  }
}