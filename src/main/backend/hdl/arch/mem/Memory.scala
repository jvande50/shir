package backend.hdl.arch.mem

final case class MemoryException(private val message: String = "", private val cause: Throwable = None.orNull) extends Exception(message, cause)
