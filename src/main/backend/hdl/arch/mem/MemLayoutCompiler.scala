package backend.hdl.arch.mem

import backend.hdl.RamArrayTypeT
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.tiling.ParallelizationCompiler
import core.compile.{CompilerPass, CompilerPhase}
import core.util.Log
import core.{ArithType, Expr, IdTypeT, TypeChecker}

import scala.collection.mutable

object MemLayoutCompiler extends CompilerPass {

  override def phaseBefore: CompilerPhase = ParallelizationCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val baseAddrs: mutable.Map[IdTypeT, Long] = mutable.Map()
    TypeChecker.check(
      expr.visitAndRebuild({
        case MemoryAllocation(identifier, description, origElementType, dimensions, t: RamArrayTypeT) =>
          val startAddress = baseAddrs.get(t.memoryLocation.ramId) match {
            case Some(addr) => addr
            case None => 0
          }
          val endAddress = startAddress + t.len.ae.evalInt
          baseAddrs.put(t.memoryLocation.ramId, endAddress)

          Log.info(this, "allocating memory of " + t.len.ae.evalInt + " words for " + description.s + " as '" + identifier.s + "' to [" + startAddress + ", " + endAddress + "] in " + t.memoryLocation)
          MemoryRegion(identifier, description, ArithType(startAddress), origElementType, dimensions, t)
        case e => e
      }).asInstanceOf[Expr]
    )
  }
}
