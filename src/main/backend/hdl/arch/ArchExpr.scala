package backend.hdl.arch

import backend.hdl._
import core._
import lift.arithmetic
import lift.arithmetic.simplifier.SimplifyIfThenElse
import lift.arithmetic.{ArithExpr, StartFromRange, ceil}

import scala.annotation.tailrec

final case class ArchLambda private (param: ParamDef, body: Expr, t: FunTypeT) extends LambdaT {
  override def build(newChildren: Seq[ShirIR]): ArchLambda = ArchLambda(newChildren.head.asInstanceOf[ParamDef], newChildren(1).asInstanceOf[Expr], newChildren(2).asInstanceOf[FunTypeT])
}

object ArchLambda {
  private[arch] def apply(param: ParamDef, body: Expr, syncType: CommunicationTypeT = SyncCommunication, schedulingStrategy: SchedulingStrategyTypeT = DefaultScheduling()): ArchLambda = ArchLambda(param, body, HWFunType(param.t, body.t, syncType, schedulingStrategy))
  def unapply(expr: Expr): Option[(ParamDef, Expr, Type)] = expr match {
    case l: LambdaT => Some(l.param, l.body, expr.t)
    case _ => None
  }
}

object ArchLambdas {
  @tailrec
  private[arch] def apply(params: Seq[ParamDef], body: Expr, syncType: CommunicationTypeT = SyncCommunication, schedulingStrategy: SchedulingStrategyTypeT = DefaultScheduling()): ArchLambda = {
    if (params.isEmpty) {
      throw MalformedExprException("Lambda expression must have at least one parameter")
    } else if (params.size == 1) {
      ArchLambda(params.head, body, syncType, schedulingStrategy)
    } else {
      ArchLambdas(params.tail, ArchLambda(params.head, body, syncType, schedulingStrategy), syncType, schedulingStrategy)
    }
  }
}

object ArchLet {
  private[arch] def apply(param: ParamDef, body: Expr, arg: Expr, syncType: CommunicationTypeT = SyncCommunication, schedulingStrategy: SchedulingStrategyTypeT = DefaultScheduling(), t: Type = UnknownType): Expr =
    FunctionCall(ArchLambda(param, body, HWFunType(param.t, body.t, syncType, schedulingStrategy)), arg, t, applyBetaReduction = false)

  def unapply(expr: FunctionCall): Option[(ParamDef, Expr, Expr, CommunicationTypeT, SchedulingStrategyTypeT, Type)] = expr.f match {
    case ArchLambda(param, body, funt: HWFunTypeT) => Some(param, body, expr.arg, funt.comType, funt.schedulingStrategy, expr.t)
    case l: LambdaT => Some(l.param, l.body, expr.arg, SyncCommunication, DefaultScheduling(), expr.t)
    case _ => None
  }
}

/**
  * root trait of all expression in the backend.hdl.arch package
  */
sealed trait ArchExpr extends BuiltinExpr with ArchIR

/**
 * A dummy primitive to test the correctness of function's types.
 */
case class DoSomeExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): DoSomeExpr = DoSomeExpr(newInnerIR)
}
object DoSome extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(function: Expr, input: Expr): DoSomeExpr = DoSomeExpr({
    /**
     * Type: indtv |-> (indtv -> indtv) -> indtv -> indtv
     */
    val indtv = LogicTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(indtv),
      //FunType(indtv, FunType(HWFunType(indtv, indtv), indtv)))),
      FunType(indtv, FunType(FunType(indtv, indtv), indtv)))),
      Seq(UnknownType)), Seq(function, input))
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): DoSomeExpr = apply(inputs.head, inputs(1))
  def unapply(expr: DoSomeExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

/*
 ***********************************************************************************************************************
 value/input generators
 ***********************************************************************************************************************
 */

case class ConstantValueExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConstantValueExpr =
    ConstantValue(newTypes.head.asInstanceOf[ArithTypeT], Some(newTypes(1)))
}
object ConstantValue {
  private[arch] def apply(value: ArithTypeT, valueType: Option[Type] = None): ConstantValueExpr = {
    val requiredBits = if(value.ae.evalInt < 0)
      (~value.ae.evalInt).toBinaryString.length + 1
    else
      value.ae.evalInt.toBinaryString.length
    valueType match {
      case Some(tv: TypeVarT) => ConstantValueExpr(Value(tv), Seq(value, tv))
      case Some(t: HWDataTypeT) if t.bitWidth.ae.evalInt >= requiredBits => ConstantValueExpr(Value(t), Seq(value, t))
      case Some(_: HWDataTypeT) => throw MalformedTypeException("constant value requires higher precision type")
      case Some(t) => throw MalformedTypeException("constant value requires hw data type; found type: " + t)
      case Some(t: SignedIntTypeT) => throw MalformedTypeException("WaHaHa")
      case None =>
        val intType = IntType(requiredBits)
        ConstantValueExpr(Value(intType), Seq(value, intType))
    }
  }
  def unapply(expr: ConstantValueExpr): Some[(ArithTypeT, Type)] =
    Some(expr.types.head.asInstanceOf[ArithTypeT], expr.t)
}

case class ConstantBitVectorExpr private[arch] (innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConstantBitVectorExpr =
    ConstantBitVector(newTypes.asInstanceOf[Seq[ArithTypeT]])
}
object ConstantBitVector {

  /**
   * Construct a logic-vector with specified bits
   *
   * @param values the bits [0, 1] of the vector. MSB first, LSB last
   */
  private[arch] def apply(values: Seq[ArithTypeT]): ConstantBitVectorExpr = {
    assert(values.map(_.ae.evalInt).max <= 1 && values.map(_.ae.evalInt).min >= 0)
    val dataType = LogicType()
    ConstantBitVectorExpr(Value(VectorType(dataType, values.length)), values)
  }
  def unapply(expr: ConstantBitVectorExpr): Some[(Seq[ArithTypeT], Type)] =
    Some(expr.types.asInstanceOf[Seq[ArithTypeT]], expr.t)
}

object ConstantVector {
  private[arch] def apply(values: Seq[ArithTypeT], valueType: Option[Type] = None): Expr = {
    valueType match {
      case Some(t: IntTypeT)  =>
        val valueBits = t.bitWidth.ae.evalInt
        val bStringSeq = values.map("0"* valueBits +_.ae.evalInt.toBinaryString).map(_.takeRight(valueBits))
        val digitSeq = bStringSeq.mkString.toList.map(_.asDigit)
        val constGen = ConstantBitVector(digitSeq.map(ArithType(_)))
        MapVector(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              ArchConversion(ParamUse(param), VectorType(LogicType(), valueBits), t)
            )
          }, SplitVector(constGen, valueBits)
        )
      case Some(t) => throw MalformedTypeException("ConstantVector does not support " + t)
      case None =>
        val intSeq = values.map(_.ae.evalInt)
        val valueBits = if(intSeq.min < 0) intSeq.max.toBinaryString.length + 1 else intSeq.max.toBinaryString.length
        val bStringSeq = values.map("0"* valueBits +_.ae.evalInt.toBinaryString).map(_.takeRight(valueBits))
        val digitSeq = bStringSeq.mkString.toList.map(_.asDigit)
        val constGen = ConstantBitVector(digitSeq.map(ArithType(_)))
        val dataType = if(intSeq.min < 0) SignedIntType(valueBits) else IntType(valueBits)
        MapVector(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              ArchConversion(ParamUse(param), VectorType(LogicType(), valueBits), dataType)
            )
          }, SplitVector(constGen, valueBits)
        )
    }
  }
}

case class CounterIntegerExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): CounterIntegerExpr = CounterIntegerExpr(newInnerIR, newTypes)
}
object CounterInteger {
  def apply(start: ArithTypeT, increment: ArithTypeT, dimensions: Seq[ArithTypeT]): CounterIntegerExpr =
    apply(start, increment, 0, dimensions, Seq.fill(dimensions.size)(0), None)
  def apply(start: ArithTypeT, increment: ArithTypeT, dimensions: Seq[ArithTypeT], valueType: Option[Type]): CounterIntegerExpr =
    apply(start, increment, 0, dimensions, Seq.fill(dimensions.size)(0), valueType)
  def apply(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], valueType: Option[Type]): CounterIntegerExpr =
    apply(start, increment, 0, dimensions, Seq.fill(dimensions.size)(0), valueType)

  /**
    * @param start initial value of the counter
    * @param increment step size to increment counter
    * @param loop counter is just counting up once (if value is 0), counter is looping infinitely (otherwise)
    * @param dimensions sequence of arithmetic values with the innermost dimension first and the outermost dimension last
    * @param repetitions if value at position N is 1, the Nth dimension will repeat the values instead of incrementing them
    *                    this is similar to a combination of the Counter and Repeat primitives
    * @param valueType data type for the counter's output (e.g. 8-bit int)
    */
  private[arch] def apply(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT], valueType: Option[Type]): CounterIntegerExpr = {
    val limit = dimensions.zip(repetitions).filter(_._2.ae.evalInt == 0).map(_._1).fold(ArithType(1))(_.ae * _.ae)
    val requiredBits = (start.ae + increment.ae * (limit.ae - 1)).evalInt.toBinaryString.length
    valueType match {
      case Some(t: IntTypeT) if t.bitWidth.ae.evalInt >= requiredBits => _apply(start, increment, loop, dimensions, repetitions, t)
      case Some(_: IntTypeT) => throw MalformedTypeException("counter requires higher precision type")
      case Some(_) => throw MalformedTypeException("counter requires int data type")
      case None => _apply(start, increment, loop, dimensions, repetitions, IntType(requiredBits))
    }
  }
  private def _apply(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT], valueType: BasicDataTypeT): CounterIntegerExpr = {
    assert(dimensions.length == repetitions.length)
    CounterIntegerExpr(Value(OrderedStreamType(dimensions, valueType)),
      Seq(start, increment, loop) ++ dimensions ++ repetitions
    )
  }

  def unapply(expr: CounterIntegerExpr): Some[(ArithTypeT, ArithTypeT, ArithTypeT, Seq[ArithTypeT], Seq[ArithTypeT], Type)] = {
    val seqs = expr.types.tail.tail.tail.map(_.asInstanceOf[ArithTypeT])
    val (dimensions, repetitions) = seqs.splitAt(seqs.length / 2)
    Some(
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.types(1).asInstanceOf[ArithTypeT],
      expr.types(2).asInstanceOf[ArithTypeT],
      dimensions,
      repetitions,
      expr.t
    )
  }
}

object TruncInteger extends BuiltinExprFun {
  override def args: Int = 1

  private[arch] def apply(input: Expr, bitWidthDelta: ArithTypeT, reduceBitWidth: Boolean): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    TypeFunctionCall(
      {
        val tv = IntTypeVar()
        TypeLambda(
          tv,
          {
            if (reduceBitWidth) {
              Conversion(
                DropVector(
                  Conversion(inputtc, VectorType(LogicType(), tv.bitWidth)),
                  0, bitWidthDelta
                ),
                IntType(tv.bitWidth.ae - bitWidthDelta.ae)
              )
            } else {
              Conversion(ConcatVector(Tuple2(
                Conversion(inputtc, VectorType(LogicType(), tv.bitWidth)),
                VectorGenerator(ConstantValue(0, Some(LogicType())), bitWidthDelta)
              )), IntType(tv.bitWidth.ae + bitWidthDelta.ae))
            }
          }
        )
      },
      inputtc.t
    )
  }

  private[arch] def apply(input: Expr, targetBitWidth: ArithTypeT): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    inputtc.t match {
      case it: IntTypeT =>
        if (it.bitWidth.ae.evalInt >= targetBitWidth.ae.evalInt) {
          val bitsToDrop = it.bitWidth.ae - targetBitWidth.ae
          apply(inputtc, bitsToDrop, reduceBitWidth = true)
        } else {
          val bitsToAppend = targetBitWidth.ae - it.bitWidth.ae
          apply(inputtc, bitsToAppend, reduceBitWidth = false)
        }
      case _ => throw MalformedTypeException("Cannot trunc non-integer types.")
    }
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): Expr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
}

case class ResizeIntegerExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ResizeIntegerExpr =
    ResizeIntegerExpr(newInnerIR, newTypes)
}
object ResizeInteger extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, width: ArithTypeT): ResizeIntegerExpr = ResizeIntegerExpr({
    val dtv = ScalarTypeVar()
    val outtv = ConditionalTypeVar(
      dtv,
      {
        case _: SignedIntTypeT => SignedIntType(width)
        case _: IntTypeT => IntType(width)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(dtv, HWFunType(dtv, outtv))
    ), input.t), input)
  }, Seq(width))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ResizeIntegerExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: ResizeIntegerExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class OrderedStreamToItemExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): OrderedStreamToItemExpr =
    OrderedStreamToItemExpr(newInnerIR)
}
object OrderedStreamToItem extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): OrderedStreamToItemExpr = OrderedStreamToItemExpr({
    val dtv = BasicDataTypeVar()
    val inputtv = OrderedStreamTypeVar(dtv, 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(inputtv, HWFunType(inputtv, dtv))
    ), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): OrderedStreamToItemExpr =
    apply(inputs.head)
  def unapply(expr: OrderedStreamToItemExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class RepeatExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RepeatExpr = RepeatExpr(newInnerIR, newTypes)
}
object Repeat extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, dimensions: Seq[ArithTypeT]): Expr = dimensions.size match {
    case 0 => input
    case _ => Repeat(apply(input, dimensions.tail), dimensions.head)
  }
  private[arch] def apply(input: Expr, len: ArithTypeT): RepeatExpr = RepeatExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val outst = OrderedStreamType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv, lentv), HWFunType(dtv, outst))), Seq(input.t, len)), input)
  }, Seq(len))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RepeatExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: RepeatExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object RepeatAll extends BuiltinExprFun {
  override def args: Int = 1
  @tailrec
  def apply(input: Expr, dims: Seq[ArithTypeT]): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    if(dims.nonEmpty) {
      RepeatAll(Repeat(inputtc, dims.last), dims.init)
    } else {
      inputtc.asInstanceOf[BuiltinExpr]
    }
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, types.asInstanceOf[Seq[ArithTypeT]])
}

case class RepeatHiddenExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RepeatHiddenExpr = RepeatHiddenExpr(newInnerIR, newTypes)
}
object RepeatHidden extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, dimensions: Seq[ArithTypeT]): Expr = dimensions.size match {
    case 0 => input
    case _ => RepeatHidden(apply(input, dimensions.tail), dimensions.head)
  }
  def apply(input: Expr, len: ArithTypeT): RepeatHiddenExpr = RepeatHiddenExpr({
    val stv = NonArrayTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(stv, HWFunType(stv, stv))), input.t), input)
  }, Seq(len))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RepeatHiddenExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: RepeatHiddenExpr): Some[(Expr, ArithTypeT, Type)] =
    Some(expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t)
}

case class VectorGeneratorExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): VectorGeneratorExpr = VectorGeneratorExpr(newInnerIR, newTypes)
}
object VectorGenerator extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, len: ArithTypeT): VectorGeneratorExpr = VectorGeneratorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val outvt = VectorType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv, lentv), HWFunType(dtv, outvt))), Seq(input.t,len)), input)
  }, Seq(len))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): VectorGeneratorExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: VectorGeneratorExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object VectorGeneratorAll extends BuiltinExprFun {
  override def args: Int = 1
  @tailrec
  def apply(input: Expr, dims: Seq[ArithTypeT]): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    if(dims.nonEmpty) {
      VectorGeneratorAll(VectorGenerator(inputtc, dims.last), dims.init)
    } else {
      inputtc.asInstanceOf[BuiltinExpr]
    }
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, types.asInstanceOf[Seq[ArithTypeT]])
}

case class SinkExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SinkExpr = SinkExpr(newInnerIR)
}
object Sink {
  def apply(input: Expr): SinkExpr = SinkExpr({
    val dtv = HWDataTypeVar()
    val fixedOutType = UnitType()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, HWFunType(dtv, fixedOutType))), input.t), input)
  })
  def unapply(expr: SinkExpr): Some[(Expr, Type)] =
    Some(expr.args.head, expr.t)
}

case class RegisteredExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RegisteredExpr = RegisteredExpr(newInnerIR)
}
object Registered extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): RegisteredExpr = RegisteredExpr({
    val dtv = BasicDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, HWFunType(dtv, dtv))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RegisteredExpr = apply(inputs.head)
  def unapply(expr: RegisteredExpr): Some[(Expr, Type)] =
    Some(expr.args.head, expr.t)
}

case class DelayExpr private[arch](innerIR: Expr, cycles: ArithTypeT) extends ArchExpr {
  override def types: Seq[Type] = Seq(cycles)
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): DelayExpr = DelayExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])
}
object Delay extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, cycles: ArithTypeT): DelayExpr = DelayExpr({
    val dtv = HWDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, HWFunType(dtv, dtv))), input.t), input)
  }, cycles)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): DelayExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: DelayExpr): Some[(Expr, Type, Type)] =
    Some(expr.args.head, expr.cycles, expr.t)
}

case class TestBenchExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TestBenchExpr = TestBenchExpr(newInnerIR)
}
object TestBench {
  def apply(input: Expr): TestBenchExpr = TestBenchExpr({
    val dtv = HWDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, HWFunType(dtv, dtv))), input.t), input)
  })
  def unapply(expr: TestBenchExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

/*
 ***********************************************************************************************************************
 simple operations
 ***********************************************************************************************************************
 */

case class IdExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): IdExpr = IdExpr(newInnerIR)
}
object Id extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): IdExpr = IdExpr({
    val dtv = HWDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, HWFunType(dtv, dtv))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): IdExpr = apply(inputs.head)
  def unapply(expr: IdExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class SignedExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SignedExpr = SignedExpr(newInnerIR)
}
object Signed extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): SignedExpr = SignedExpr({
    val width = ArithTypeVar()
    val itv = IntTypeVar(width)
    val outt = SignedIntType(width + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, HWFunType(itv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SignedExpr = apply(inputs.head)
  def unapply(expr: SignedExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class UnSignedExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): UnSignedExpr = UnSignedExpr(newInnerIR)
}
object Unsigned extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): UnSignedExpr = UnSignedExpr({
    val width = ArithTypeVar()
    val itv = SignedIntTypeVar(width)
    val outt = IntType(width - 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, HWFunType(itv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): UnSignedExpr = apply(inputs.head)
  def unapply(expr: UnSignedExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class ClipBankersRoundExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ClipBankersRoundExpr = ClipBankersRoundExpr(newInnerIR, newTypes)
}
object ClipBankersRound extends BuiltinExprFun {
  override def args: Int = 1

  /**
    * Apply clipping to most significant bits and rounding to least significant bits.
    * The rounding technique is round-to-nearest even. (To follow the rounding scenario in Python.)
    *
    * @param input        The input expression.
    * @param lowElements  The number of least significant bits for rounding.
    * @param highElements The number of most significant bits for rounding.
    */
  private[arch] def apply(input: Expr, lowElements: ArithTypeT, highElements: ArithTypeT): ClipBankersRoundExpr = ClipBankersRoundExpr({
    val itv = IntTypeVar()
    val outt = ConditionalTypeVar(
      itv,
      {
        case SignedIntType(width) => SignedIntType(width.ae - lowElements.ae - highElements.ae)
        case IntType(width) => IntType(width.ae - lowElements.ae - highElements.ae)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, HWFunType(itv, outt))), input.t), input)
  }, Seq(lowElements, highElements))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ClipBankersRoundExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: ClipBankersRoundExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class ClipBankersRoundShiftExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ClipBankersRoundShiftExpr = ClipBankersRoundShiftExpr(newInnerIR)
}
object ClipBankersRoundShift extends BuiltinExprFun {
  override def args: Int = 1

  private[arch] def apply(input: Expr): ClipBankersRoundShiftExpr = ClipBankersRoundShiftExpr({
    val w = ArithTypeVar()
    val i = SignedIntTypeVar(w)
    val s = SignedIntTypeVar()
    val ttv = TupleTypeVar(i, s)
    val out = SignedIntType(w)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, out))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ClipBankersRoundShiftExpr = apply(inputs.head)
  def unapply(expr: ClipBankersRoundShiftExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object QScale extends BuiltinExprFun {
  override def args: Int = 1

  private[arch] def apply(input: Expr): Expr = {
    val i = Select2(input, 0)
    val f = Conversion(Select2(input, 1), VectorType(LogicType(), 32))

    // XXX:
    // Some potential improvements to make this routine more generic
    // (but a qscale would never reach these cases)
    //
    // *  pad "00" instead of "01" if subnormal (f[31:24] = 0), (includes 0)
    // *  negate fractional bits (s25) if sign bit (f[31]) is set
    // *  handle infinity and NaN
    val frac = Conversion(
      ConcatVector(Tuple2(DropVector(f, 0, 9), ConstantBitVector(Seq(0, 1)))),
      SignedIntType(25)
    )
    val shamt = SubInt(Tuple2(
      Conversion(
        ConcatVector(Tuple2(DropVector(f, 23, 1), ConstantBitVector(Seq(0)))),
        SignedIntType(9)
      ),
      ConstantValue(150, Some(SignedIntType(9)))
    ))

    ClipBankersRoundShift(Tuple2(MulInt(Tuple2(i, frac)), shamt))
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): Expr = apply(inputs.head)
}

case class AddIntExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): AddIntExpr = AddIntExpr(newInnerIR)
}
object AddInt extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): AddIntExpr = AddIntExpr({
    val ttv = TupleTypeVar(ScalarTypeVar(), ScalarTypeVar())
    val outit =
      ConditionalTypeVar(
        ttv,
        {
          case TupleType(Seq(t0@(_: IntTypeT | _: SignedIntTypeT), t1@(_: IntTypeT | _: SignedIntTypeT))) if t0.kind != t1.kind => // Exclude info of size
            throw MalformedExprException(this.getClass().getName() + " does not support a tuple of " + t0.toString + " and " + t1.toString + ".")
          case TupleType(Seq(it1: SignedIntTypeT, it2: SignedIntTypeT)) =>
            SignedIntType(SimplifyIfThenElse(
              it1.bitWidth.ae.gt(it2.bitWidth.ae),
              it1.bitWidth.ae,
              SimplifyIfThenElse(
                it1.bitWidth.ae.eq(it2.bitWidth.ae),
                it1.bitWidth.ae + 1,
                it2.bitWidth.ae
              )
            ))
          case TupleType(Seq(it1: IntTypeT, it2: IntTypeT)) =>
            IntType(SimplifyIfThenElse(
              it1.bitWidth.ae.gt(it2.bitWidth.ae),
              it1.bitWidth.ae,
              SimplifyIfThenElse(
                it1.bitWidth.ae.eq(it2.bitWidth.ae),
                it1.bitWidth.ae + 1,
                it2.bitWidth.ae
              )
            ))
        }
      )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, outit))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddIntExpr = apply(inputs.head)
  def unapply(expr: AddIntExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class AddFloatExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): AddFloatExpr = AddFloatExpr(newInnerIR)
}
object AddFloat extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): AddFloatExpr = AddFloatExpr({
    val floatTv = FloatTypeVar()
    val ttv = TupleTypeVar(floatTv, floatTv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, floatTv))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddFloatExpr = apply(inputs.head)
  def unapply(expr: AddFloatExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object AddInt2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): AddIntExpr = AddInt(Tuple2(input1, input2))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddIntExpr = apply(inputs.head, inputs(1))
}

object AddFloat2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): AddFloatExpr = AddFloat(Tuple2(input1, input2))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddFloatExpr = apply(inputs.head, inputs(1))
}

case class SubIntExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SubIntExpr = SubIntExpr(newInnerIR)
}
object SubInt extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): SubIntExpr = SubIntExpr({
    val itv1 = SignedIntTypeVar()
    val itv2 = SignedIntTypeVar()
    val ttv = TupleTypeVar(itv1, itv2)
    val outit = SignedIntType(
      SimplifyIfThenElse(
        itv1.bitWidth.ae.gt(itv2.bitWidth.ae),
        itv1.bitWidth.ae,
        itv2.bitWidth.ae
      )
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, outit))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SubIntExpr = apply(inputs.head)
  def unapply(expr: SubIntExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class MulIntExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MulIntExpr = MulIntExpr(newInnerIR)
}
object MulInt extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): MulIntExpr = MulIntExpr({
    val ttv = TupleTypeVar(ScalarTypeVar(), ScalarTypeVar())
    val outit =
      ConditionalTypeVar(
        ttv,
        {
          case TupleType(Seq(t0@(_: IntTypeT | _: SignedIntTypeT), t1@(_: IntTypeT | _: SignedIntTypeT))) if t0.kind != t1.kind => // Exclude info of size
            throw MalformedExprException(this.getClass().getName() + " does not support a tuple of " + t0.toString + " and " + t1.toString + ".")
          case TupleType(Seq(it1: SignedIntTypeT, it2: SignedIntTypeT)) =>
            SignedIntType(it1.bitWidth.ae + it2.bitWidth.ae)
          case TupleType(Seq(it1: IntTypeT, it2: IntTypeT)) =>
            IntType(it1.bitWidth.ae + it2.bitWidth.ae)
        }
      )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, outit))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MulIntExpr = apply(inputs.head)
  def unapply(expr: MulIntExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}


object SquareInt extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): MulIntExpr = MulInt(VectorToTuple2(VectorGenerator(input, 2)))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MulIntExpr = apply(inputs.head)
}

case class MulFloatExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MulFloatExpr = MulFloatExpr(newInnerIR)
}
object MulFloat extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): MulFloatExpr = MulFloatExpr({
    val floatTv = FloatTypeVar()
    val ttv = TupleTypeVar(floatTv, floatTv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, floatTv))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MulFloatExpr = apply(inputs.head)
  def unapply(expr: MulFloatExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class MinIntExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MinIntExpr = MinIntExpr(newInnerIR)
}
object MinInt extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): MinIntExpr = MinIntExpr({
    val ttv = TupleTypeVar(ScalarTypeVar(), ScalarTypeVar())
    val outit =
      ConditionalTypeVar(
        ttv,
        { case TupleType(Seq(t0: IntTypeT, t1: IntTypeT)) =>
            if (t0.kind != t1.kind)
              throw MalformedExprException(this.getClass().getName() + " does not support a tuple of " + t0.toString + " and " + t1.toString + ".")

          val outw = SimplifyIfThenElse(t0.bitWidth.ae.gt(t1.bitWidth.ae), t0.bitWidth.ae, t1.bitWidth.ae)
          if (t0.kind == SignedIntTypeKind) SignedIntType(outw) else IntType(outw)
        }
      )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, outit))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MinIntExpr = apply(inputs.head)
  def unapply(expr: MinIntExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object MinInt2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): MinIntExpr = MinInt(Tuple2(input1, input2))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MinIntExpr = apply(inputs.head, inputs(1))
}

case class MaxIntExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MaxIntExpr = MaxIntExpr(newInnerIR)
}
object MaxInt extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): MaxIntExpr = MaxIntExpr({
    val ttv = TupleTypeVar(ScalarTypeVar(), ScalarTypeVar())
    val outit =
      ConditionalTypeVar(
        ttv,
        { case TupleType(Seq(t0: IntTypeT, t1: IntTypeT)) =>
            if (t0.kind != t1.kind)
              throw MalformedExprException(this.getClass().getName() + " does not support a tuple of " + t0.toString + " and " + t1.toString + ".")

          val outw = SimplifyIfThenElse(t0.bitWidth.ae.gt(t1.bitWidth.ae), t0.bitWidth.ae, t1.bitWidth.ae)
          if (t0.kind == SignedIntTypeKind) SignedIntType(outw) else IntType(outw)
        }
      )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, HWFunType(ttv, outit))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MaxIntExpr = apply(inputs.head)
  def unapply(expr: MaxIntExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object MaxInt2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): MaxIntExpr = MaxInt(Tuple2(input1, input2))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MaxIntExpr = apply(inputs.head, inputs(1))
}

object ReLUInt extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): MaxIntExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    MaxInt2(inputtc, ConstantValue(0, Some(inputtc.t)))
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MaxIntExpr = apply(inputs.head)
}

case class SelectExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SelectExpr = SelectExpr(newInnerIR, newTypes)
}

object Select extends BuiltinExprFun {
  override def args: Int = 1

  private[arch] def apply(input: Expr, n: ArithTypeT, selection: ArithTypeT): SelectExpr = SelectExpr({
    val elts = n.ae.evalInt
    val sel = selection.ae.evalInt
    assert(sel < elts && sel >= 0, s"Invalid select element ${sel} from ${n}-tuple")

    val tvs = Seq.range(0, elts).map(_ => HWDataTypeVar())
    val inttv = TupleTypeVar(tvs: _*)
    val outt = tvs(sel)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, HWFunType(inttv, outt))), input.t), input)
  }, Seq(n, selection))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SelectExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])

  def unapply(expr: SelectExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

object Select2 extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, selection: ArithTypeT): SelectExpr = Select(input, 2, selection)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SelectExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SelectExpr): Option[(Expr, ArithTypeT, Type)] = expr match {
    case Select(e, n, s, t) if n.ae.evalInt == 2 => Some((e, s, t))
    case _ => None
  }
}

object Select3 extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, selection: ArithTypeT): SelectExpr = Select(input, 3, selection)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SelectExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SelectExpr): Option[(Expr, ArithTypeT, Type)] = expr match {
    case Select(e, n, s, t) if n.ae.evalInt == 3 => Some((e, s, t))
    case _ => None
  }
}

/*
 ***********************************************************************************************************************
 complex operations (map, reduce, slide, ...)
 ***********************************************************************************************************************
 */

case class MapOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapOrderedStreamExpr = MapOrderedStreamExpr(newInnerIR)
}
object MapOrderedStream extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(function: Expr, input: Expr): MapOrderedStreamExpr = MapOrderedStreamExpr({
    /**
     * Type: indtv |-> outdtv |-> lentv |-> (indtv -> outdtv) -> Stm[indtv, lentv] -> Stm[outtv, lentv]
     * So we only need these three type vars: indtv, outdtv, and lentv.
     */
    val indtv = NonArrayTypeVar()
    val outdtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(indtv, outdtv, lentv),
      FunType(OrderedStreamType(indtv, lentv), FunType(FunType(indtv, outdtv), OrderedStreamType(outdtv, lentv))))),
      Seq(UnknownType, UnknownType, UnknownType)), Seq(function, input))
  })
  private[arch] def apply(nestingLevels: Int, function: Expr, input: Expr): Expr = {
    if (nestingLevels <= 0) {
      FunctionCall(function, input)
    } else if (nestingLevels == 1) {
      apply(function, input)
    } else {
      val inputtc = TypeChecker.checkIfUnknown(input, input.t)
      val et = inputtc.t match {
        case st: OrderedStreamTypeT => st.et
        case _ => throw MalformedExprException(s"Cannot create $nestingLevels times nested Maps for input of type ${inputtc.t}")
      }
      MapOrderedStream(
        {
          val p = ParamDef(et)
          ArchLambda(p, apply(nestingLevels - 1, function, ParamUse(p)))
        },
        input
      )
    }
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MapOrderedStreamExpr = apply(inputs.head, inputs(1))
  def unapply(expr: MapOrderedStreamExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class MapSimpleOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapSimpleOrderedStreamExpr = MapSimpleOrderedStreamExpr(newInnerIR)
}
object MapSimpleOrderedStream extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(function: Expr, input: Expr): MapSimpleOrderedStreamExpr = MapSimpleOrderedStreamExpr({
    val indtv = NonArrayTypeVar()
    val outdtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(indtv, outdtv, lentv),
      FunType(OrderedStreamType(indtv, lentv), FunType(FunType(indtv, outdtv), OrderedStreamType(outdtv, lentv))))),
      Seq(UnknownType, UnknownType, UnknownType)), Seq(function, input))
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MapSimpleOrderedStreamExpr = apply(inputs.head, inputs(1))
  def unapply(expr: MapSimpleOrderedStreamExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class MapUnorderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapUnorderedStreamExpr = MapUnorderedStreamExpr(newInnerIR)
}
object MapUnorderedStream extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(function: Expr, input: Expr): MapUnorderedStreamExpr = MapUnorderedStreamExpr({
    val indtv = NonArrayTypeVar()
    val outdtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(indtv, outdtv, lentv),
      FunType(UnorderedStreamType(indtv, lentv), FunType(FunType(indtv, outdtv), UnorderedStreamType(outdtv, lentv))))),
      Seq(UnknownType, UnknownType, UnknownType)), Seq(function, input))
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MapUnorderedStreamExpr = apply(inputs.head, inputs(1))
  def unapply(expr: MapUnorderedStreamExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class MapVectorExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapVectorExpr = MapVectorExpr(newInnerIR)
}
object MapVector extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(function: Expr, input: Expr): MapVectorExpr = MapVectorExpr({
    val indtv = BasicDataTypeVar()
    val outdtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(indtv, outdtv, lentv),
      FunType(VectorType(indtv, lentv), FunType(FunType(indtv, outdtv), VectorType(outdtv, lentv))))),
      Seq(UnknownType, UnknownType, UnknownType)), Seq(function, input))
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MapVectorExpr = apply(inputs.head, inputs(1))
  def unapply(expr: MapVectorExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class MapAsyncOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapAsyncOrderedStreamExpr = MapAsyncOrderedStreamExpr(newInnerIR)
}

object MapAsyncOrderedStream {
  private[arch] def apply(function: Expr, inputs: Expr*): MapAsyncOrderedStreamExpr = MapAsyncOrderedStreamExpr({
    val indtvs = inputs.map(_ => NonArrayTypeVar())
    val outdtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val ft = FunType(indtvs, outdtv)
    val insts = indtvs.map(dtv => OrderedStreamType(dtv, lentv))
    val outst = OrderedStreamType(outdtv, lentv)
    /**
     * Note that the apply methods of FunType and TypeFunType revert the order of indtvs/indtvs.
     * In contrast, FunctionCall and TypeFunctionCall do not!
     */
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(indtvs.reverse :+ lentv :+ outdtv,
      FunType(ft +: insts.reverse, outst))), indtvs.map(_ => UnknownType) :+ UnknownType :+ UnknownType), function +: inputs)
  })

  def apply(nestingLevels: Int, function: Expr, inputs: Expr*): Expr = {
    if (nestingLevels <= 0) {
      FunctionCall(function, inputs)
    } else if (nestingLevels == 1) {
      apply(function, inputs: _*)
    } else {
      val inputstc = inputs.map(i => TypeChecker.check(i))
      val params = inputstc.map(i => i.t match {
        case st: OrderedStreamTypeT => ParamDef(st.et)
        case _ => throw MalformedExprException(s"Cannot create ${nestingLevels} times nested Maps for input of type ${i.t}")
      })
      val paramUses = params.map(p => ParamUse(p))
      val funBody = apply(function, paramUses: _*)
      /**
       * Note that the apply method of ArchLambdas reverts the order of params!
       */
      val fun = ArchLambdas(params.reverse, funBody)
      MapAsyncOrderedStream(fun, inputs: _*)
    }
  }

  def unapply(expr: MapAsyncOrderedStreamExpr): Some[(Expr, Seq[Expr], Type)] =
    Some((expr.args.head, expr.args.tail, expr.t))
}

case class FoldUpperBoundedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): FoldUpperBoundedStreamExpr = FoldUpperBoundedStreamExpr(newInnerIR)
}
object FoldUpperBoundedStream extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(function: Expr, input: Expr): FoldUpperBoundedStreamExpr = FoldUpperBoundedStreamExpr({
    /**
     * Type: dtv |-> acctv |-> lentv |-> (dtv -> acctv) -> Stm[dtv, lentv] -> outdtv
     * So we need these three type vars: dtv, acctv, and lentv.
     * acctv (TypeVar for accumulator) is required to avoid type-checking with Fold's output type (for stm size 1).
     */
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar(StartFromRange(1))
    val acctv = NonArrayTypeVar()

    val outt = ConditionalTypeVar(
      UpperBoundedStreamType(dtv, lentv),
      {
        case UpperBoundedStreamType(t: IntTypeT, len) =>
          var outBitWidth: ArithTypeT = t.bitWidth.ae + ceil(arithmetic.Log(2, len.ae))
          function.visit{
            case MaxInt(_, _) | MinInt(_, _) => outBitWidth = t.bitWidth.ae
            case _ =>
          }
          if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth)

        case UpperBoundedStreamType(OrderedStreamType(t: IntTypeT, innerLen), outerLen) =>
          var outBitWidth: ArithTypeT = t.bitWidth.ae + ceil(arithmetic.Log(2, outerLen.ae))
          function.visit {
            case MaxInt(_, _) | MinInt(_, _) => outBitWidth = t.bitWidth.ae
            case _ =>
          }
          OrderedStreamType(
            if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth),
            innerLen
          )

        case UpperBoundedStreamType(t: BasicDataTypeT, _) => t
      }
    )

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(dtv, acctv, lentv),
        FunType(Seq(FunType(dtv, FunType(outt, acctv)), UpperBoundedStreamType(dtv, lentv)), outt)
      )),
      Seq(UnknownType, UnknownType, UnknownType)),
      Seq(function, input)
    )
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FoldUpperBoundedStreamExpr = apply(inputs.head, inputs(1))
  def unapply(expr: FoldUpperBoundedStreamExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class FoldOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): FoldOrderedStreamExpr = FoldOrderedStreamExpr(newInnerIR)
}
object FoldOrderedStream extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(function: Expr, input: Expr): FoldOrderedStreamExpr = FoldOrderedStreamExpr({
    /**
     * Type: dtv |-> acctv |-> lentv |-> (dtv -> acctv) -> Stm[dtv, lentv] -> outdtv
     * So we need these three type vars: dtv, acctv, and lentv.
     * acctv (TypeVar for accumulator) is required to avoid type-checking with Fold's output type (for stm size 1).
     */
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar(StartFromRange(1))
    val acctv = NonArrayTypeVar()

    val outt = ConditionalTypeVar(
      OrderedStreamType(dtv, lentv),
      {
        case OrderedStreamType(t: IntTypeT, len) =>
          var outBitWidth: ArithTypeT = t.bitWidth.ae + ceil(arithmetic.Log(2, len.ae))
          function.visit{
            case MaxInt(_, _) | MinInt(_, _) => outBitWidth = t.bitWidth.ae
            case _ =>
          }
          if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth)

        case OrderedStreamType(OrderedStreamType(t: IntTypeT, innerLen), outerLen) =>
          var outBitWidth: ArithTypeT = t.bitWidth.ae + ceil(arithmetic.Log(2, outerLen.ae))
          function.visit {
            case MaxInt(_, _) | MinInt(_, _) => outBitWidth = t.bitWidth.ae
            case _ =>
          }
          OrderedStreamType(
            if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth),
            innerLen
          )

        case OrderedStreamType(t: BasicDataTypeT, _) => t
      }
    )

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(dtv, acctv, lentv),
        FunType(Seq(FunType(dtv, FunType(outt, acctv)), OrderedStreamType(dtv, lentv)), outt)
      )),
      Seq(UnknownType, UnknownType, UnknownType)),
      Seq(function, input)
    )
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FoldOrderedStreamExpr = apply(inputs.head, inputs(1))
  def unapply(expr: FoldOrderedStreamExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

object FoldVector{
  private[arch] def apply(function: TypeFunctionT, input: Expr): Expr = {
    val inputtc = TypeChecker.checkAssert(input)
    inputtc.t match {
      case VectorType(_, len) if len.ae.evalInt > 2 && len.ae.evalInt % 2 == 0 =>
        val split = TypeChecker.check(SplitVector(inputtc, 2))
        FoldVector(
          function,
          MapVector(
            {
              val param = ParamDef(VectorTypeVar())
              ArchLambda(
                param,
                //                function.call(VectorToTuple(ParamUse(param)))
                {
                  var func: Expr = AddInt(VectorToTuple2(ParamUse(param)))
                  function.visit{
                    case MaxInt(_, _) =>
                      func = MaxInt(VectorToTuple2(ParamUse(param)))
                    case MinInt(_, _) =>
                      func = MinInt(VectorToTuple2(ParamUse(param)))
                    case _ =>
                  }
                  func
                }
              )
            },
            split
          )
        )
      case VectorType(et, len) if len.ae.evalInt > 2 && len.ae.evalInt % 2 == 1 =>
        FoldVector(
          function,
          ConcatVector(Tuple2(inputtc, VectorGenerator(ConstantValue(0, Some(et)), 1)))
        )
      case VectorType(et: IntTypeT, len) if len.ae.evalInt > 2 && (math.log(len.ae.evalInt)/math.log(2)) % 1 != 0 =>
        val pow2Order = math.floor(math.log(len.ae.evalInt)/math.log(2))
        val pow2Len = math.pow(2, pow2Order).toInt
        val splitRest = len.ae.evalInt - pow2Len
        val splitInput = SplitVector(inputtc, len)
        val splitInputtc = TypeChecker.checkAssert(splitInput)
        val foldSum = MapVector(
          {
            val p = ParamDef(splitInputtc.t.asInstanceOf[VectorTypeT].et)
            ArchLambda(p,
              {
                val inPow2 = DropVector(ParamUse(p), 0, splitRest)
                val inRest = DropVector(ParamUse(p), pow2Len, 0)
                val sumPow2 = FoldVector(function, inPow2)
                val sumRest = FoldVector(function, inRest)
                var sum: Expr = AddInt(Tuple2(sumPow2, sumRest))
                function.visit{
                  case MaxInt(_, _) =>
                    sum = MaxInt(Tuple2(sumPow2, sumRest))
                  case MinInt(_, _) =>
                    sum = MinInt(Tuple2(sumPow2, sumRest))
                  case _ =>
                }
                val sumtc = TypeChecker.checkAssert(sum)
                TruncInteger(sumtc, ArithType(et.bitWidth.ae.evalInt + pow2Order.toInt + 1))
              }
            )
          }, splitInputtc
        )
        FoldVector(function, foldSum)
      case VectorType(_, len) if len.ae.evalInt == 2 =>
        //        function.call(VectorToTuple(inputtc))
        var func: Expr = AddInt(VectorToTuple2(inputtc))
        function.visit{
          case MaxInt(_, _) =>
            func = MaxInt(VectorToTuple2(inputtc))
          case MinInt(_, _) =>
            func = MinInt(VectorToTuple2(inputtc))
          case _ =>
        }
        func
      case VectorType(et: IntTypeT, len) if len.ae.evalInt == 1 =>
        ArchConversion(inputtc, VectorType(et, len), IntType(et.bitWidth))
      case _ => throw MalformedExprException("input to ReduceVector must be a vector.")
    }
  }
}

case class ReduceOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReduceOrderedStreamExpr = ReduceOrderedStreamExpr(newInnerIR)
}
object ReduceOrderedStream extends BuiltinExprFun {
  override def args: Int = 3
  private[arch] def apply(function: Expr, initialValue: Expr, input: Expr): ReduceOrderedStreamExpr = ReduceOrderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar(StartFromRange(1))
    val initialValueTv = HWDataTypeVar()
    val acctv = HWDataTypeVar()

    /**
     * We must include initialValueTv and Stm[dtv, lentv] because the information of length (lentv) is required.
     * If we do not give lentv to the ConditionalTypeVar, the type checker will have no such information!
     */
    val outt = ConditionalTypeVar(
      TupleType(initialValueTv, OrderedStreamType(dtv, lentv)),
      {
        case TupleType(Seq(t: IntTypeT, OrderedStreamType(_, len))) if t.kind == IntTypeKind => // do not allow subtypes of IntTypeT here!
          var outBitWidth: ArithTypeT = t.bitWidth.ae + ceil(arithmetic.Log(2, len.ae))
          function.visit {
            case MaxInt(_, _) | MinInt(_, _) => outBitWidth = t.bitWidth.ae
            case _ =>
          }
          IntType(outBitWidth)

        case TupleType(Seq(initt: HWDataTypeT, _)) if initt.kind != HWDataTypeKind => initt
      }
    )

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(dtv, initialValueTv, acctv, lentv),
        FunType(Seq(FunType(dtv, FunType(outt, acctv)), initialValueTv, OrderedStreamType(dtv, lentv)), outt)
      )),
      Seq(UnknownType, UnknownType, UnknownType, UnknownType)),
      Seq(function, initialValue, input)
    )
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReduceOrderedStreamExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: ReduceOrderedStreamExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class SlideOrderedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SlideOrderedStreamExpr = SlideOrderedStreamExpr(newInnerIR, newTypes)
}
object SlideOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, windowWidth: ArithTypeT): SlideOrderedStreamExpr = SlideOrderedStreamExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)
    val outst = OrderedStreamType(VectorType(dtv, windowWidth), lentv - windowWidth.ae + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  }, Seq(windowWidth))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SlideOrderedStreamExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SlideOrderedStreamExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class AlternateExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): AlternateExpr = AlternateExpr(newInnerIR)
}
object Alternate extends BuiltinExprFun {
  override def args: Int = 3
  private[arch] def apply(f1: Expr, f2: Expr, input: Expr): AlternateExpr = AlternateExpr({
    val intv = HWDataTypeVar()
    val outt = HWDataTypeVar()
    val f1tv = FunTypeVar(intv, outt)
    val f2tv = FunTypeVar(intv, outt)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(f1tv, f2tv, intv), HWFunTypes(Seq(f1tv, f2tv, intv), outt))), Seq(f1.t, f2.t, input.t)), Seq(f1, f2, input))
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AlternateExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: AlternateExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class IfelseExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): IfelseExpr = IfelseExpr(newInnerIR)
}
object Ifelse extends BuiltinExprFun {
  override def args: Int = 3
  private[arch] def apply(function1: Expr, function2: Expr, input: Expr): IfelseExpr = IfelseExpr({
    val seltv = IntTypeVar()
    val intv = HWDataTypeVar()
    val intupletv = TupleTypeVar(intv, seltv)
    val outtv = HWDataTypeVar()
    val f1tv = FunTypeVar(intv, outtv)
    val f2tv = FunTypeVar(intv, outtv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(f1tv, f2tv, intupletv), HWFunTypes(Seq(f1tv, f2tv, intupletv), outtv))), Seq(function1.t, function2.t, input.t)), Seq(function1, function2, input))
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): IfelseExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: IfelseExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

/*
 ***********************************************************************************************************************
 data type conversions / rearrangement
 ***********************************************************************************************************************
 */

object ArchConversion {

  final case class ConversionException(private val message: String, private val cause: Throwable = None.orNull) extends Exception(message, cause)

  /**
    * calculates the required length of a collection A with elements of type 'elementType', when converted to / from a collection B of type 'collectionType'
    * this calculation includes possible unused bits, which are chopped of in conversion, e.g. when the bit widths of the elements from A and B are NOT a multiple of each other
    */
  def calcLength(collectionType: UnorderedStreamTypeT, elementType: HWDataTypeT): ArithTypeT = calcLength(1, collectionType: UnorderedStreamTypeT, elementType: HWDataTypeT)

  @scala.annotation.tailrec
  private def calcLength(outerLength: ArithTypeT, collectionType: UnorderedStreamTypeT, elementType: HWDataTypeT): ArithTypeT = collectionType.et match {
    // recursively go through collections and grab their length as 'outerLength', but only if the element Type is not a LogicType (We do not want to end up having streams of bits)
    case ct: UnorderedStreamTypeT if !ct.et.isInstanceOf[LogicTypeT] => calcLength(outerLength.ae * collectionType.len.ae, ct, elementType)
    case _ => SimplifyIfThenElse(
      collectionType.et.bitWidth.ae.gt(elementType.bitWidth.ae),
      outerLength.ae * collectionType.len.ae * (1 + ((collectionType.et.bitWidth.ae - 1) / elementType.bitWidth.ae)),
      1 + (outerLength.ae * collectionType.len.ae - 1) / // ceiling integer division
        SimplifyIfThenElse( // add this condition to prevent a division by zero error, because even if the condition of the first if is true, the else block is evaluated!
          collectionType.et.bitWidth.ae.gt(elementType.bitWidth.ae),
          1,
          elementType.bitWidth.ae / collectionType.et.bitWidth.ae
        )
    )
  }

  /**
    * generator for combinations of expressions to archive the desired data type conversion
    */
  def apply(input: Expr, from: Type, to: Type): Expr = (from, to) match {
    // no conversion needed if 'from' and 'to' are the same
    case (_, _) if from == to => input

    // if length of collections are equal => 'unwrap' and deal with inner elements
    case (OrderedStreamType(et1, len1), OrderedStreamType(et2, len2)) if len1 == len2 =>
      val p = ParamDef(et1)
      val f = ArchLambda(p, apply(ParamUse(p), et1, et2))
      val map = TypeChecker.check(MapOrderedStream(f, input))
      apply(map, map.t, to)
    case (UnorderedStreamType(et1, len1), UnorderedStreamType(et2, len2)) if len1 == len2 =>
      val p = ParamDef(et1)
      val f = ArchLambda(p, apply(ParamUse(p), et1, et2))
      val map = TypeChecker.check(MapUnorderedStream(f, input))
      apply(map, map.t, to)
    case (VectorType(et1, len1), VectorType(et2, len2)) if len1 == len2 =>
      val p = ParamDef(et1)
      val f = ArchLambda(p, apply(ParamUse(p), et1, et2))
      val map = TypeChecker.check(MapVector(f, input))
      apply(map, map.t, to)

    // convert all scalar types (in streams / vectors) to vectors of logic values
    case (st: ScalarTypeT, _) =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val conv = Conversion(input, newVecType)
      apply(conv, conv.t, to)
    case (_, st: ScalarTypeT) =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val newInput = apply(input, from, newVecType)
      Conversion(newInput, to)
    case (_, UnorderedStreamType(st: ScalarTypeT, len)) =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val newInput = apply(input, from, UnorderedStreamType(newVecType, len))
      val p = ParamDef(newVecType)
      val f = ArchLambda(p, Conversion(ParamUse(p), st))
      MapUnorderedStream(f, newInput)
    case (UnorderedStreamType(st: ScalarTypeT, _), _) =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val p = ParamDef(st)
      val f = ArchLambda(p, Conversion(ParamUse(p), newVecType))
      val map = TypeChecker.check(MapUnorderedStream(f, input))
      apply(map, map.t, to)
    case (_, OrderedStreamType(st: ScalarTypeT, len)) =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val newInput = apply(input, from, OrderedStreamType(newVecType, len))
      val p = ParamDef(newVecType)
      val f = ArchLambda(p, Conversion(ParamUse(p), st))
      MapOrderedStream(f, newInput)
    case (OrderedStreamType(st: ScalarTypeT, _), _) =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val p = ParamDef(st)
      val f = ArchLambda(p, Conversion(ParamUse(p), newVecType))
      val map = TypeChecker.check(MapOrderedStream(f, input))
      apply(map, map.t, to)
    case (_, VectorType(st: ScalarTypeT, len)) if !st.isInstanceOf[LogicTypeT] =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val newInput = apply(input, from, VectorType(newVecType, len))
      val p = ParamDef(newVecType)
      val f = ArchLambda(p, Conversion(ParamUse(p), st))
      MapVector(f, newInput)
    case (VectorType(st: ScalarTypeT, _), _) if !st.isInstanceOf[LogicTypeT] =>
      val newVecType = VectorType(LogicType(), st.bitWidth)
      val p = ParamDef(st)
      val f = ArchLambda(p, Conversion(ParamUse(p), newVecType))
      val map = TypeChecker.check(MapVector(f, input))
      apply(map, map.t, to)

    // convert all tuple types (in streams / vectors) to vectors of logic values
    case (TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT)), _) =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val conv = Conversion(input, newTupleType)
      apply(conv, conv.t, to)
    case (TupleType(Seq(_: VectorTypeT, _: VectorTypeT)), _) =>
      val concat = TypeChecker.check(ConcatVector(input))
      apply(concat, concat.t, to)
    case (_, TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT))) =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val newInput = apply(input, from, newTupleType)
      Conversion(newInput, to)
    case (_, TupleType(Seq(VectorType(et1, len1), VectorType(et2, len2)))) if et1 != et2 || len1 != len2 =>
      throw ConversionException("missing primitive to split vector into 2 chunks of different length.")
    case (_, TupleType(Seq(VectorType(et1, len1), VectorType(_, _)))) => // vectors have the same element type and length
      val newInput = apply(input, from, VectorType(VectorType(et1, len1), 2))
      VectorToTuple2(newInput)
    case (_, UnorderedStreamType(tt @ TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT)), len)) =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val newInput = apply(input, from, UnorderedStreamType(newTupleType, len))
      val p = ParamDef(newTupleType)
      val f = ArchLambda(p, Conversion(ParamUse(p), tt))
      MapUnorderedStream(f, newInput)
    case (_, UnorderedStreamType(TupleType(Seq(VectorType(et1, len1), VectorType(et2, len2))), _)) if et1 != et2 || len1 != len2 =>
      throw ConversionException("missing primitive to split vector into 2 chunks of different length.")
    case (_, UnorderedStreamType(TupleType(Seq(VectorType(et1, len1), VectorType(_, _))), len)) =>
      val newInput = apply(input, from, UnorderedStreamType(VectorType(VectorType(et1, len1), 2), len))
      MapUnorderedStream(VectorToTuple2.asFunction(), newInput)
    case (UnorderedStreamType(tt @ TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT)), _), _) =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val p = ParamDef(tt)
      val f = ArchLambda(p, Conversion(ParamUse(p), newTupleType))
      val map = TypeChecker.check(MapUnorderedStream(f, input))
      apply(map, map.t, to)
    case (UnorderedStreamType(TupleType(Seq(VectorType(_, _), VectorType(_, _))), _), _) =>
      val map = TypeChecker.check(MapUnorderedStream(ConcatVector.asFunction(), input))
      apply(map, map.t, to)
    case (_, OrderedStreamType(tt @ TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT)), len)) =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val newInput = apply(input, from, OrderedStreamType(newTupleType, len))
      val p = ParamDef(newTupleType)
      val f = ArchLambda(p, Conversion(ParamUse(p), tt))
      MapOrderedStream(f, newInput)
    case (_, OrderedStreamType(TupleType(Seq(VectorType(et1, len1), VectorType(et2, len2))), _)) if et1 != et2 || len1 != len2 =>
      throw ConversionException("missing primitive to split vector into 2 chunks of different length.")
    case (_, OrderedStreamType(TupleType(Seq(VectorType(et1, len1), VectorType(_, _))), len)) =>
      val newInput = apply(input, from, OrderedStreamType(VectorType(VectorType(et1, len1), 2), len))
      MapOrderedStream(VectorToTuple2.asFunction(), newInput)
    case (OrderedStreamType(tt @ TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT)), _), _) =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val p = ParamDef(tt)
      val f = ArchLambda(p, Conversion(ParamUse(p), newTupleType))
      val map = TypeChecker.check(MapOrderedStream(f, input))
      apply(map, map.t, to)
    case (OrderedStreamType(TupleType(Seq(VectorType(_, _), VectorType(_, _))), _), _) =>
      val map = TypeChecker.check(MapOrderedStream(ConcatVector.asFunction(), input))
      apply(map, map.t, to)
    case (_, VectorType(tt @ TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT)), len)) if !st1.isInstanceOf[LogicTypeT] && !st2.isInstanceOf[LogicTypeT] =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val newInput = apply(input, from, VectorType(newTupleType, len))
      val p = ParamDef(newTupleType)
      val f = ArchLambda(p, Conversion(ParamUse(p), tt))
      MapVector(f, newInput)
    case (_, VectorType(TupleType(Seq(VectorType(et1, len1), VectorType(et2, len2))), _)) if et1 != et2 || len1 != len2 =>
      throw ConversionException("missing primitive to split vector into 2 chunks of different length.")
    case (_, VectorType(TupleType(Seq(VectorType(et1, len1), VectorType(_, _))), len)) =>
      val newInput = apply(input, from, VectorType(VectorType(VectorType(et1, len1), 2), len))
      MapVector(VectorToTuple2.asFunction(), newInput)
    case (VectorType(tt @ TupleType(Seq(st1: ScalarTypeT, st2: ScalarTypeT)), _), _) if !st1.isInstanceOf[LogicTypeT] && !st2.isInstanceOf[LogicTypeT] =>
      val newTupleType = TupleType(
        VectorType(LogicType(), st1.bitWidth),
        VectorType(LogicType(), st2.bitWidth)
      )
      val p = ParamDef(tt)
      val f = ArchLambda(p, Conversion(ParamUse(p), newTupleType))
      val map = TypeChecker.check(MapVector(f, input))
      apply(map, map.t, to)
    case (VectorType(TupleType(Seq(VectorType(_, _), VectorType(_, _))), _), _) =>
      val map = TypeChecker.check(MapVector(ConcatVector.asFunction(), input))
      apply(map, map.t, to)

    // drop or concat
    case (OrderedStreamType(et1, len1), OrderedStreamType(et2, len2)) if et1 == et2 && len1.ae.evalInt > len2.ae.evalInt =>
      val drop = TypeChecker.check(DropOrderedStream(input, 0, len1.ae - len2.ae))
      apply(drop, drop.t, to)
    case (OrderedStreamType(et1, len1), OrderedStreamType(et2, len2)) if et1 == et2 && len1.ae.evalInt < len2.ae.evalInt =>
      val constantZero = ConstantValue(0, Some(et1))
      val streamZero = Repeat(constantZero, len2.ae - len1.ae)
      val concat = TypeChecker.check(ConcatOrderedStream(input, streamZero))
      apply(concat, concat.t, to)
    case (VectorType(et1, len1), VectorType(et2, len2)) if et1 == et2 && len1.ae.evalInt > len2.ae.evalInt =>
      val drop = TypeChecker.check(DropVector(input, 0, len1.ae - len2.ae))
      apply(drop, drop.t, to)
    case (VectorType(et1, len1), VectorType(et2, len2)) if et1 == et2 && len1.ae.evalInt < len2.ae.evalInt =>
      val constantZero = ConstantValue(0, Some(et1))
      val vectorZero = VectorGenerator(constantZero, len2.ae - len1.ae)
      val concat = TypeChecker.check(ConcatVector(Tuple2(input, vectorZero)))
      apply(concat, concat.t, to)
    case (VectorType(et1, len1), OrderedStreamType(et2, len2)) if et1 == et2 && len1.ae.evalInt > len2.ae.evalInt =>
      val drop = TypeChecker.check(DropVector(input, 0, len1.ae - len2.ae))
      apply(drop, drop.t, to)
    case (VectorType(et1, len1), UnorderedStreamType(et2, len2)) if et1 == et2 && len1.ae.evalInt > len2.ae.evalInt =>
      val drop = TypeChecker.check(DropVector(input, 0, len1.ae - len2.ae))
      apply(drop, drop.t, to)

    // collection conversion
    case (OrderedStreamType(et1, len1), UnorderedStreamType(et2, len2)) =>
      if (et1 == et2 && len1 == len2) {
        // strict condition here: we do not want to "loose" the ordering of a stream if we are not certain if that is the correct conversion to do
        val indexing = TypeChecker.check(OrderedStreamToUnorderedStream(input))
        apply(indexing, indexing.t, to)
      } else {
        // invoke indexing later
        val newInput = apply(input, from, OrderedStreamType(et2, len2))
        apply(newInput, newInput.t, to)
      }
    case (OrderedStreamType(OrderedStreamType(_, _), _), VectorType(_, _)) =>
      val join = TypeChecker.check(JoinOrderedStream(input))
      apply(join, join.t, to)
    case (OrderedStreamType(uos1 @ UnorderedStreamType(_, _), _), VectorType(_, _)) =>
      val p = ParamDef(uos1)
      val f = ArchLambda(p, UnorderedStreamToOrderedStream(ParamUse(p)))
      val map = TypeChecker.check(MapOrderedStream(f, input))
      apply(map, map.t, to)
    case (OrderedStreamType(_, _), VectorType(_, _)) =>
      // TODO this may lead to performance issues, if the 'from' stream is long and/or has high precision elements
      val vectorise = TypeChecker.check(OrderedStreamToVector(input))
      apply(vectorise, vectorise.t, to)
    case (VectorType(et1, len1), OrderedStreamType(et2, len2)) if et1 == et2 && len1 == len2 =>
      val toStream = TypeChecker.check(VectorToOrderedStream(input))
      apply(toStream, toStream.t, to)
    case (VectorType(et1, len1), UnorderedStreamType(et2, len2)) if et1 == et2 && len1 == len2 =>
      val toStream = TypeChecker.check(VectorToOrderedStream(input))
      apply(toStream, toStream.t, to)
    case (UnorderedStreamType(_, _), OrderedStreamType(_, _)) | (UnorderedStreamType(_, _), VectorType(_, _)) =>
      val sorting = TypeChecker.check(UnorderedStreamToOrderedStream(input))
      apply(sorting, sorting.t, to)
    // convert nested vector to stream
    case (OrderedStreamType(vt1 @ VectorType(et1, len1inner), len1outer), OrderedStreamType(et2, len2)) if (et1 == et2 && len1outer.ae.evalInt > 1) || len1inner.ae.evalInt * len1outer.ae.evalInt == len2.ae.evalInt =>
      // if there is only one vector we directly split the vector to get better performance
      val p = ParamDef(vt1)
      val f = ArchLambda(p, VectorToOrderedStream(ParamUse(p)))
      val map = TypeChecker.check(MapOrderedStream(f, input))
      apply(map, map.t, to)
    case (UnorderedStreamType(vt1 @ VectorType(et1, len1inner), len1outer), UnorderedStreamType(et2, len2)) if (et1 == et2 && len1outer.ae.evalInt > 1) || len1inner.ae.evalInt * len1outer.ae.evalInt == len2.ae.evalInt =>
      // if there is only one vector we directly split the vector to get better performance
      val p = ParamDef(vt1)
      val f = ArchLambda(p, VectorToOrderedStream(ParamUse(p)))
      val map = TypeChecker.check(MapUnorderedStream(f, input))
      apply(map, map.t, to)

    // join nested
    case (VectorType(VectorType(et1, len1inner), len1outer), VectorType(et2, len2)) if et1 == et2 || len1inner.ae.evalInt * len1outer.ae.evalInt == len2.ae.evalInt =>
      val join = TypeChecker.check(JoinVector(input))
      apply(join, join.t, to)
    case (OrderedStreamType(OrderedStreamType(et1, len1inner), len1outer), OrderedStreamType(et2, len2)) if et1 == et2 || len1inner.ae.evalInt * len1outer.ae.evalInt == len2.ae.evalInt =>
      val join = TypeChecker.check(JoinOrderedStream(input))
      apply(join, join.t, to)
    case (OrderedStreamType(uos1 @ UnorderedStreamType(et1, _), _), OrderedStreamType(et2, _)) if et1 == et2 =>
      val p = ParamDef(uos1)
      val f = ArchLambda(p, UnorderedStreamToOrderedStream(ParamUse(p)))
      val map = TypeChecker.check(MapOrderedStream(f, input))
      apply(map, map.t, to)
    case (UnorderedStreamType(UnorderedStreamType(et1, len1inner), len1outer), UnorderedStreamType(et2, len2)) if et1 == et2 || len1inner.ae.evalInt * len1outer.ae.evalInt == len2.ae.evalInt =>
      val join = TypeChecker.check(JoinUnorderedStream(input))
      apply(join, join.t, to)
    case (UnorderedStreamType(os1 @ OrderedStreamType(et1, _), _), UnorderedStreamType(et2, _)) if et1 == et2 =>
      val p = ParamDef(os1)
      val f = ArchLambda(p, OrderedStreamToUnorderedStream(ParamUse(p)))
      val map = TypeChecker.check(MapUnorderedStream(f, input))
      apply(map, map.t, to)

    // split vector
    case (OrderedStreamType(vt1 @ VectorType(et1, len1inner), len1outer), OrderedStreamType(VectorType(et2, len2inner), len2outer)) if et1 == et2 && len1inner.ae.evalInt > len2inner.ae.evalInt && (len1inner.ae.evalInt * len1outer.ae.evalInt >= len2inner.ae.evalInt * len2outer.ae.evalInt) =>
      val p = ParamDef(vt1)
      val f = ArchLambda(p, SplitVector(ParamUse(p), len2inner))
      val map = TypeChecker.check(MapOrderedStream(f, input))
      apply(map, map.t, to)
    case (UnorderedStreamType(vt1 @ VectorType(et1, len1inner), len1outer), UnorderedStreamType(VectorType(et2, len2inner), len2outer)) if et1 == et2 && len1inner.ae.evalInt > len2inner.ae.evalInt && (len1inner.ae.evalInt * len1outer.ae.evalInt >= len2inner.ae.evalInt * len2outer.ae.evalInt) =>
      val p = ParamDef(vt1)
      val f = ArchLambda(p, SplitVector(ParamUse(p), len2inner))
      val map = TypeChecker.check(MapUnorderedStream(f, input))
      apply(map, map.t, to)
    case (OrderedStreamType(VectorType(et1, width1), len1), OrderedStreamType(VectorType(et2, width2), _)) if et1 == et2 && width1.ae.evalInt < width2.ae.evalInt && len1.ae.evalInt % (width2.ae.evalInt / width1.ae.evalInt) > 0 =>
      // add elements to stream, if split is not possible
      val chunkSize = width2.ae.evalInt / width1.ae.evalInt
      val missingElements = chunkSize - (len1.ae.evalInt % chunkSize)
      val constantZero = ConstantValue(0, Some(et1))
      val vectorZero = VectorGenerator(constantZero, width1)
      val streamZero = Repeat(vectorZero, missingElements)
      val concat = TypeChecker.check(ConcatOrderedStream(input, streamZero))
      apply(concat, concat.t, to)
    case (UnorderedStreamType(VectorType(et1, width1), _), UnorderedStreamType(vt2 @ VectorType(et2, width2), len2)) if et1 == et2 && width1.ae.evalInt < width2.ae.evalInt =>
      val newInput = apply(input, from, OrderedStreamType(vt2, len2))
      apply(newInput, newInput.t, to)
    case (VectorType(et1, width1), OrderedStreamType(VectorType(et2, width2), len2)) if et1 == et2 && width1.ae.evalInt < width2.ae.evalInt * len2.ae.evalInt =>
      val newInput = apply(input, from, VectorType(et1, width2.ae * len2.ae))
      apply(newInput, newInput.t, to)
    case (VectorType(et1, width1), OrderedStreamType(VectorType(et2, width2), len2)) if et1 == et2 && width1.ae.evalInt == width2.ae.evalInt * len2.ae.evalInt =>
      val split = TypeChecker.check(SplitVector(input, width2))
      apply(split, split.t, to)
    case (VectorType(et1, width1), UnorderedStreamType(VectorType(et2, width2), len2)) if et1 == et2 && width1.ae.evalInt < width2.ae.evalInt * len2.ae.evalInt =>
      val newInput = apply(input, from, VectorType(et1, width2.ae * len2.ae))
      apply(newInput, newInput.t, to)
    case (VectorType(et1, width1), UnorderedStreamType(VectorType(et2, width2), len2)) if et1 == et2 && width1.ae.evalInt == width2.ae.evalInt * len2.ae.evalInt =>
      val split = TypeChecker.check(SplitVector(input, width2))
      apply(split, split.t, to)

    // split stream
    case (OrderedStreamType(et1, len1), OrderedStreamType(OrderedStreamType(et2, len2inner), len2outer)) if et1 != et2 && len1.ae.evalInt == len2inner.ae.evalInt * len2outer.ae.evalInt && len1.ae.evalInt % len2inner.ae.evalInt == 0 =>
      val split = TypeChecker.check(SplitOrderedStream(input, len2inner))
      apply(split, split.t, to)
    case (OrderedStreamType(et1, len1), OrderedStreamType(et2, len2)) if et1 != et2 && len1.ae.evalInt % len2.ae.evalInt == 0 =>
      val split = TypeChecker.check(SplitOrderedStream(input, len1.ae / len2.ae))
      apply(split, split.t, to)
    case (OrderedStreamType(et1, len1), OrderedStreamType(et2, len2)) if et1 != et2 && len2.ae.evalInt % len1.ae.evalInt == 0 =>
      // invoke split later
      val newInput = apply(input, from, OrderedStreamType(OrderedStreamType(et2, len2.ae / len1.ae), len1.ae))
      apply(newInput, newInput.t, to)
    case (UnorderedStreamType(et1, len1), UnorderedStreamType(et2, len2)) if et1 != et2 && len2.ae.evalInt % len1.ae.evalInt == 0 =>
      // invoke split later
      val newInput = apply(input, from, UnorderedStreamType(UnorderedStreamType(et2, len2.ae / len1.ae), len1.ae))
      apply(newInput, newInput.t, to)
    case (OrderedStreamType(et1, len1), OrderedStreamType(OrderedStreamType(et2, len2inner), len2outer)) if et1 != et2 && len1 != len2outer =>
      // invoke split later
      val newInput = apply(input, from, OrderedStreamType(et2, len2inner.ae * len2outer.ae))
      apply(newInput, newInput.t, to)
    case (UnorderedStreamType(et1, len1), UnorderedStreamType(UnorderedStreamType(et2, len2inner), len2outer)) if et1 != et2 && len1 != len2outer =>
      // invoke split later
      val newInput = apply(input, from, UnorderedStreamType(et2, len2inner.ae * len2outer.ae))
      apply(newInput, newInput.t, to)

    case _ =>
      throw ConversionException("Cannot convert type " + from + " to " + to)
  }
}

case class VectorToTupleExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): VectorToTupleExpr = VectorToTupleExpr(newInnerIR, newTypes)
}

object VectorToTuple extends BuiltinExprFun {
  override def args: Int = 1

  private[arch] def apply(input: Expr, n: ArithTypeT): VectorToTupleExpr = VectorToTupleExpr({
    val len = n.ae.evalInt
    val dtv = BasicDataTypeVar()
    val invtv = VectorTypeVar(dtv, len)
    val outst = TupleType(Seq.fill(len)(dtv): _*)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(invtv, HWFunType(invtv, outst))), input.t), input)
  }, Seq(n))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): VectorToTupleExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: VectorToTupleExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object VectorToTuple2 extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): VectorToTupleExpr = VectorToTuple(input, 2)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): VectorToTupleExpr = apply(inputs.head)
  def unapply(expr: VectorToTupleExpr): Option[(Expr, Type)] = expr match {
    case VectorToTuple((e, n, t)) if n.ae.evalInt == 2 => Some((e, t))
    case _ => None
  }
}

case class VectorToOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): VectorToOrderedStreamExpr = VectorToOrderedStreamExpr(newInnerIR)
}
object VectorToOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): VectorToOrderedStreamExpr = VectorToOrderedStreamExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val invtv = VectorTypeVar(dtv, lentv)
    val outst = OrderedStreamType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(invtv, HWFunType(invtv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): VectorToOrderedStreamExpr = apply(inputs.head)
  def unapply(expr: VectorToOrderedStreamExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class OrderedStreamToVectorExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): OrderedStreamToVectorExpr = OrderedStreamToVectorExpr(newInnerIR)
}
object OrderedStreamToVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): OrderedStreamToVectorExpr = OrderedStreamToVectorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)
    val outvt = VectorType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outvt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): OrderedStreamToVectorExpr = apply(inputs.head)
  def unapply(expr: OrderedStreamToVectorExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class IndexUnorderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): IndexUnorderedStreamExpr = IndexUnorderedStreamExpr(newInnerIR)
}
object IndexUnorderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): IndexUnorderedStreamExpr = IndexUnorderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar(StartFromRange(1))
    val instv = UnorderedStreamTypeVar(dtv, lentv)
    val outst = OrderedStreamType(TupleType(dtv, instv.indexType), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        instv,
        HWFunType(
          instv,
          outst
        )
      )),
      input.t),
      input
    )
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): IndexUnorderedStreamExpr = apply(inputs.head)
  def unapply(expr: IndexUnorderedStreamExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}
object IndexStream {
  private[arch] def apply(input: Expr): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    inputtc.t match {
      case OrderedStreamType(_: BasicDataTypeT, _) => apply(OrderedStreamToUnorderedStream(inputtc))
      case UnorderedStreamType(_: BasicDataTypeT, _) => IndexUnorderedStream(inputtc)
      case st: UnorderedStreamTypeT =>
        SplitOrderedStream(apply(JoinStream(inputtc)), st.dimensions.init.last)
      case _ =>
        throw MalformedExprException("input to IndexStream must be a stream, got instead: " + inputtc.t)
    }
  }
}

case class UnorderedStreamToOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): UnorderedStreamToOrderedStreamExpr = UnorderedStreamToOrderedStreamExpr(newInnerIR)
}
object UnorderedStreamToOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): UnorderedStreamToOrderedStreamExpr = UnorderedStreamToOrderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val instv = UnorderedStreamTypeVar(dtv, lentv)
    val outst = OrderedStreamType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): UnorderedStreamToOrderedStreamExpr = apply(inputs.head)
  def unapply(expr: UnorderedStreamToOrderedStreamExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}
//object UnorderedStreamToOrderedStream {
//  private[backend.hdl.arch] def apply(input: Expr): Expr = BufferUnorderedStream(input)
//}

case class OrderedStreamToUnorderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): OrderedStreamToUnorderedStreamExpr = OrderedStreamToUnorderedStreamExpr(newInnerIR)
}
object OrderedStreamToUnorderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): OrderedStreamToUnorderedStreamExpr = OrderedStreamToUnorderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)
    val outst = UnorderedStreamType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): OrderedStreamToUnorderedStreamExpr = apply(inputs.head)
  def unapply(expr: OrderedStreamToUnorderedStreamExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class OrderedStreamToUpperBoundedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): OrderedStreamToUpperBoundedStreamExpr = OrderedStreamToUpperBoundedStreamExpr(newInnerIR, newTypes)
}
object OrderedStreamToUpperBoundedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, maxLength: ArithTypeT): OrderedStreamToUpperBoundedStreamExpr = OrderedStreamToUpperBoundedStreamExpr({
    val instv = OrderedStreamTypeVar(NonArrayTypeVar(), ArithTypeVar())
    val outst = ConditionalTypeVar(
      instv,
      {
        case OrderedStreamType(dtv, len) if len.ae.evalInt <= maxLength.ae.evalInt => UpperBoundedStreamType(dtv, maxLength)
        case OrderedStreamType(_, len) => throw MalformedExprException(this.getClass().getName() + " cannot convert an OrderedStream of length " +
          len.ae.evalInt + " into an UpperBoundedStream of max length " + maxLength.ae.evalInt + ".")
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  }, Seq(maxLength))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): OrderedStreamToUpperBoundedStreamExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: OrderedStreamToUpperBoundedStreamExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class TupleExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TupleExpr = TupleExpr(newInnerIR)
}

object Tuple {
  private[arch] def apply(inputs: Expr*): TupleExpr = TupleExpr({
    val dtvs = inputs.map(_ => HWDataTypeVar())
    val outtt = TupleType(dtvs: _*)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtvs, HWFunTypes(dtvs, outtt))), inputs.map(_.t)), inputs)
  })

  def unapply(expr: TupleExpr): Some[(Seq[Expr], Type)] =
    Some((expr.args, expr.t))
}

object Tuple2 extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(input1: Expr, input2: Expr): TupleExpr = Tuple(input1, input2)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TupleExpr = apply(inputs.head, inputs(1))
  def unapply(expr: TupleExpr): Option[(Expr, Expr, Type)] = expr match {
    case Tuple((Seq(x, y), t)) => Some((x, y, t))
    case _ => None
  }
}

case class FlipTupleExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): FlipTupleExpr = FlipTupleExpr(newInnerIR, newTypes)
}

object FlipTuple extends BuiltinExprFun {
  override def args: Int = 1

  private[arch] def apply(input: Expr, n: ArithTypeT): FlipTupleExpr = FlipTupleExpr({
    val len = n.ae.evalInt
    val dtvs = Seq.range(0, len).map(_ => HWDataTypeVar())
    val intv = TupleTypeVar(dtvs: _*)
    val outtt = TupleType(dtvs.reverse: _*)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, HWFunType(intv, outtt))), input.t), input)
  }, Seq(n))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FlipTupleExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: FlipTupleExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object FlipTuple2 extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): FlipTupleExpr = FlipTuple(input, 2)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FlipTupleExpr = apply(inputs.head)
  def unapply(expr: FlipTupleExpr): Option[(Expr, Type)] = expr match {
    case FlipTuple((e, n, t)) if n.ae.evalInt == 2 => Some((e, t))
    case _ => None
  }
}

case class ZipOrderedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ZipOrderedStreamExpr = ZipOrderedStreamExpr(newInnerIR, newTypes)
}

object ZipOrderedStream extends BuiltinExprFun {
  override def args: Int = 1

  private[arch] def apply(input: Expr, n: ArithTypeT): ZipOrderedStreamExpr = ZipOrderedStreamExpr({
    val elts = n.ae.evalInt
    val dtvs = Seq.range(0, elts).map(_ => NonArrayTypeVar())
    val lentv = ArithTypeVar()
    val instvs = dtvs.map(t => OrderedStreamTypeVar(t, lentv))
    val inttv = TupleTypeVar(instvs: _*)
    val outst = OrderedStreamType(TupleType(dtvs: _*), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, HWFunType(inttv, outst))), input.t), input)
  }, Seq(n))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipOrderedStreamExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: ZipOrderedStreamExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object Zip2OrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): ZipOrderedStreamExpr = ZipOrderedStream(input, 2)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipOrderedStreamExpr = apply(inputs.head)
  def unapply(expr: ZipOrderedStreamExpr): Option[(Expr, Type)] = expr match {
    case ZipOrderedStream(e, n, t) if n.ae.evalInt == 2 => Some((e, t))
    case _ => None
  }
}

case class ZipVectorExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ZipVectorExpr = ZipVectorExpr(newInnerIR)
}
object ZipVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): ZipVectorExpr = ZipVectorExpr({
    val dtv1 = BasicDataTypeVar()
    val dtv2 = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val instv1 = VectorTypeVar(dtv1, lentv)
    val instv2 = VectorTypeVar(dtv2, lentv)
    val inttv = TupleTypeVar(instv1, instv2)
    val outst = VectorType(TupleType(dtv1, dtv2), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, HWFunType(inttv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipVectorExpr = apply(inputs.head)
  def unapply(expr: ZipVectorExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class SplitOrderedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SplitOrderedStreamExpr = SplitOrderedStreamExpr(newInnerIR, newTypes)
}
object SplitOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, chunkSize: ArithTypeT): SplitOrderedStreamExpr = apply(input, chunkSize, innerSize = true)
  private[arch] def apply(input: Expr, size: ArithTypeT, innerSize: Boolean): SplitOrderedStreamExpr = {
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)

    val (chunkSize: ArithExpr, numberOfChunks: ArithExpr) =
      if (innerSize)
        (size.ae, lentv.ae / size.ae)
      else
        (lentv.ae / size.ae, size.ae)

    SplitOrderedStreamExpr({
      val outst = OrderedStreamType(OrderedStreamType(dtv, chunkSize), numberOfChunks)
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
    }, Seq(chunkSize))
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SplitOrderedStreamExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SplitOrderedStreamExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object SplitStreamAll {
  @tailrec
  def apply(input: Expr, dims: Seq[ArithTypeT]): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    if(dims.nonEmpty) {
      SplitStreamAll(SplitOrderedStream(inputtc, dims.last), dims.init)
    } else {
      inputtc
    }
  }
}


case class SplitVectorExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SplitVectorExpr = SplitVectorExpr(newInnerIR, newTypes)
}
object SplitVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, chunkSize: ArithTypeT): SplitVectorExpr = SplitVectorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val invtv = VectorTypeVar(dtv, lentv)
    val outvt = VectorType(VectorType(dtv, chunkSize), lentv.ae / chunkSize.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(invtv, HWFunType(invtv, outvt))), input.t), input)
  }, Seq(chunkSize))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SplitVectorExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SplitVectorExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class JoinOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): JoinOrderedStreamExpr = JoinOrderedStreamExpr(newInnerIR)
}
object JoinOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  /**
    * @param input an expression with type ordered stream of ordered stream. If you want to join an ordered stream of unordered stream, reorder the inner unordered stream first
    */
  private[arch] def apply(input: Expr): JoinOrderedStreamExpr = JoinOrderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val innerlentv = ArithTypeVar()
    val outerlentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(OrderedStreamTypeVar(dtv, innerlentv), outerlentv)
    val outst = OrderedStreamType(dtv, innerlentv.ae * outerlentv.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): JoinOrderedStreamExpr = apply(inputs.head)
  def unapply(expr: JoinOrderedStreamExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class JoinUnorderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): JoinUnorderedStreamExpr = JoinUnorderedStreamExpr(newInnerIR)
}
object JoinUnorderedStream extends BuiltinExprFun {
  override def args: Int = 1
  /**
    * @param input an expression with type unordered stream of unordered stream. If you want to join an unordered stream of ordered stream, convert the inner ordered stream to an unordered stream first
    */
  private[arch] def apply(input: Expr): JoinUnorderedStreamExpr = JoinUnorderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val innerlentv = ArithTypeVar()
    val outerlentv = ArithTypeVar()
    val instv = UnorderedStreamTypeVar(UnorderedStreamTypeVar(dtv, innerlentv), outerlentv)
    val outst = UnorderedStreamType(dtv, innerlentv.ae * outerlentv.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): JoinUnorderedStreamExpr = apply(inputs.head)
  def unapply(expr: JoinUnorderedStreamExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object JoinStream {
  private[arch] def apply(input: Expr): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    inputtc.t match {
      case OrderedStreamType(et, _) => et match {
        case OrderedStreamType(_, _) => JoinOrderedStream(inputtc)
        case UnorderedStreamType(_, _) => JoinUnorderedStream(OrderedStreamToUnorderedStream(inputtc))
        case _ => throw new RuntimeException("never happens")
      }
      case UnorderedStreamType(et, _) => et match {
        case os @ OrderedStreamType(_, _) =>
          JoinUnorderedStream(MapUnorderedStream(
            {
              val p = ParamDef(os)
              ArchLambda(p, OrderedStreamToUnorderedStream(ParamUse(p)))
            },
            inputtc
          ))
        case UnorderedStreamType(_, _) => JoinUnorderedStream(inputtc)
        case _ => throw new RuntimeException("never happens")
      }
    }
  }
}
object JoinStreamAll {
  private[arch] def apply(input: Expr): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    inputtc.t match {
      case streamType: UnorderedStreamTypeT if streamType.dimensions.size > 1 => apply(JoinStream(inputtc))
      case _ => inputtc
    }
  }
}

case class JoinVectorExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): JoinVectorExpr = JoinVectorExpr(newInnerIR)
}
object JoinVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): JoinVectorExpr = JoinVectorExpr({
    val dtv = BasicDataTypeVar()
    val innerlentv = ArithTypeVar()
    val outerlentv = ArithTypeVar()
    val invtv = VectorTypeVar(VectorTypeVar(dtv, innerlentv), outerlentv)
    val outvt = VectorType(dtv, innerlentv.ae * outerlentv.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(invtv, HWFunType(invtv, outvt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): JoinVectorExpr = apply(inputs.head)
  def unapply(expr: JoinVectorExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class DropOrderedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): DropOrderedStreamExpr = DropOrderedStreamExpr(newInnerIR, newTypes)
}
object DropOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, firstElements: ArithTypeT, lastElements: ArithTypeT): DropOrderedStreamExpr = DropOrderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)
    val outst = OrderedStreamType(dtv, lentv.ae - firstElements.ae - lastElements.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  }, Seq(firstElements, lastElements))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): DropOrderedStreamExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: DropOrderedStreamExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class DropVectorExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): DropVectorExpr = DropVectorExpr(newInnerIR, newTypes)
}
object DropVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, lowElements: ArithTypeT, highElements: ArithTypeT): DropVectorExpr = DropVectorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val invtv = VectorTypeVar(dtv, lentv)
    val outvt = VectorType(dtv, lentv.ae - lowElements.ae - highElements.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(invtv, HWFunType(invtv, outvt))), input.t), input)
  }, Seq(lowElements, highElements))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): DropVectorExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: DropVectorExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class PadOrderedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PadOrderedStreamExpr = PadOrderedStreamExpr(newInnerIR, newTypes)
}
object PadOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, firstElements: ArithTypeT, lastElements: ArithTypeT, value: ArithTypeT): PadOrderedStreamExpr = PadOrderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)
    val outst = OrderedStreamType(dtv, lentv.ae + firstElements.ae + lastElements.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  }, Seq(firstElements, lastElements, value))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): PadOrderedStreamExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT], types(2).asInstanceOf[ArithTypeT])
  def unapply(expr: PadOrderedStreamExpr): Some[(Expr, ArithTypeT, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.types(2).asInstanceOf[ArithTypeT], expr.t))
}

case class ConcatOrderedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConcatOrderedStreamExpr = ConcatOrderedStreamExpr(newInnerIR, newTypes)
}
object ConcatOrderedStream extends BuiltinExprFun {
  override def args: Int = 2
  private[arch] def apply(input1: Expr, input2: Expr, dimension: ArithTypeT = ArithType(0)): ConcatOrderedStreamExpr = ConcatOrderedStreamExpr({
    val (instv1, instv2, outst) = getStreamTypes(dimension.ae.evalInt)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(instv1, instv2), HWFunTypes(Seq(instv1, instv2), outst))), Seq(input1.t, input2.t)), Seq(input1, input2))
  }, Seq(dimension))

  private def getStreamTypes(dimension: Int): (OrderedStreamTypeVar, OrderedStreamTypeVar, OrderedStreamType) = dimension match {
    case 0 =>
      val dtv = NonArrayTypeVar()
      val lentv1 = ArithTypeVar()
      val lentv2 = ArithTypeVar()
      (
        OrderedStreamTypeVar(dtv, lentv1),
        OrderedStreamTypeVar(dtv, lentv2),
        OrderedStreamType(dtv, lentv1 + lentv2)
      )
    case _ =>
      val lentv = ArithTypeVar()
      val streamTypes = getStreamTypes(dimension - 1)
      (
        OrderedStreamTypeVar(streamTypes._1, lentv),
        OrderedStreamTypeVar(streamTypes._2, lentv),
        OrderedStreamType(streamTypes._3, lentv)
      )
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ConcatOrderedStreamExpr = apply(inputs.head, inputs(1), types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: ConcatOrderedStreamExpr): Some[(Expr, Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class ConcatVectorExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConcatVectorExpr = ConcatVectorExpr(newInnerIR)
}
object ConcatVector extends BuiltinExprFun {
  override def args: Int = 1

  /**
   * Concatenates two vectors to make a larger vector
   *
   * @param input A tuple of the lower half followed by the upper half
   */
  private[arch] def apply(input: Expr): ConcatVectorExpr = ConcatVectorExpr({
    val dtv = BasicDataTypeVar()
    val lowinlentv = ArithTypeVar()
    val highinlentv = ArithTypeVar()
    val lowinvtv = VectorTypeVar(dtv, lowinlentv)
    val highinvtv = VectorTypeVar(dtv, highinlentv)
    val inttv = TupleTypeVar(lowinvtv, highinvtv)
    val outst = VectorType(dtv, lowinlentv + highinlentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, HWFunType(inttv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ConcatVectorExpr = apply(inputs.head)
  def unapply(expr: ConcatVectorExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class UpdateOrderedStreamExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): UpdateOrderedStreamExpr = UpdateOrderedStreamExpr(newInnerIR)
}
object UpdateOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): UpdateOrderedStreamExpr = UpdateOrderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)
    val outst = OrderedStreamType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): UpdateOrderedStreamExpr = apply(inputs.head)
  def unapply(expr: UpdateOrderedStreamExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class UpdateVectorExpr private[arch](innerIR: Expr) extends ArchExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): UpdateVectorExpr = UpdateVectorExpr(newInnerIR)
}
object UpdateVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): UpdateVectorExpr = UpdateVectorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val invtv = TupleTypeVar(
      VectorTypeVar(dtv, lentv), // the original vector
      TupleTypeVar(IntTypeVar(), BasicDataTypeVar()) // the index to update and its value
    )
    val outvt = VectorType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(invtv, HWFunType(invtv, outvt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): UpdateVectorExpr = apply(inputs.head)
  def unapply(expr: UpdateVectorExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class RotateVectorExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RotateVectorExpr = RotateVectorExpr(newInnerIR, newTypes)
}
object RotateVector extends BuiltinExprFun {
  override def args: Int = 1
  /**
    * @param input an expression with type tuple of VectorType and IntType. The integer identifies the distance to rotate.
    * @param rotateLeft a flag for left rotation. If 0, the input vector is rotated right (LSB). Otherwise the vector is rotated left (MSB).
    */
  private[arch] def apply(input: Expr, rotateLeft: ArithTypeT): RotateVectorExpr = RotateVectorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val intv1 = VectorTypeVar(dtv, lentv)
    val intv2 = IntTypeVar()
    val intv = TupleTypeVar(intv1, intv2)
    val outtv = VectorType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, HWFunType(intv, outtv))), input.t), input)
  }, Seq(rotateLeft))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RotateVectorExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: RotateVectorExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class PermuteVectorExpr private[arch] (innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PermuteVectorExpr = PermuteVectorExpr(newInnerIR, newTypes)
}
object PermuteVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, rule: ArithLambdaType): PermuteVectorExpr = PermuteVectorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = VectorTypeVar(dtv, lentv)
    val outseqt = VectorType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, HWFunType(inseqtv, outseqt))), input.t), input)
  }, Seq(rule))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): PermuteVectorExpr = apply(inputs.head, types.head.asInstanceOf[ArithLambdaType])
  def unapply(expr: PermuteVectorExpr): Some[(Expr, ArithLambdaType, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithLambdaType], expr.t))
}

case class SlideVectorExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SlideVectorExpr = SlideVectorExpr(newInnerIR, newTypes)
}
object SlideVector extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, windowWidth: ArithTypeT, stepSize: ArithTypeT): SlideVectorExpr = SlideVectorExpr({
    val dtv = BasicDataTypeVar()
    val lentv = ArithTypeVar()
    val instv = VectorTypeVar(dtv, lentv)
    val outst = VectorType(VectorType(dtv, windowWidth), (lentv  - windowWidth.ae) / stepSize.ae + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  }, Seq(windowWidth, stepSize))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SlideVectorExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: SlideVectorExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class SlideGeneralOrderedStreamExpr private[arch](innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SlideGeneralOrderedStreamExpr = SlideGeneralOrderedStreamExpr(newInnerIR, newTypes)
}
object SlideGeneralOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, windowWidth: ArithTypeT, stepSize: ArithTypeT): SlideGeneralOrderedStreamExpr = SlideGeneralOrderedStreamExpr({
    val dtv = NonArrayTypeVar()
    val lentv = ArithTypeVar()
    val instv = OrderedStreamTypeVar(dtv, lentv)
    val outst = OrderedStreamType(OrderedStreamType(dtv, windowWidth), (lentv  - windowWidth.ae) / stepSize.ae + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, HWFunType(instv, outst))), input.t), input)
  }, Seq(windowWidth, stepSize))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SlideGeneralOrderedStreamExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: SlideGeneralOrderedStreamExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

object Tuple3 extends BuiltinExprFun {
  override def args: Int = 3
  private[arch] def apply(input1: Expr, input2: Expr, input3: Expr): TupleExpr = Tuple(input1, input2, input3)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TupleExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: TupleExpr): Option[(Expr, Expr, Expr, Type)] = expr match {
    case Tuple((Seq(x, y, z), t)) => Some((x, y, z, t))
    case _ => None
  }
}

object Zip3OrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr): ZipOrderedStreamExpr = ZipOrderedStream(input, 3)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipOrderedStreamExpr = apply(inputs.head)
  def unapply(expr: ZipOrderedStreamExpr): Option[(Expr, Type)] = expr match {
    case ZipOrderedStream(e, n, t) if n.ae.evalInt == 3 => Some((e, t))
    case _ => None
  }
}

// Dummy primitive
case class TransposeNDOrderedStreamExpr private[arch] (innerIR: Expr, types: Seq[Type]) extends ArchExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TransposeNDOrderedStreamExpr = TransposeNDOrderedStreamExpr(newInnerIR, newTypes)
}
object TransposeNDOrderedStream extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, dimOrder: Seq[ArithTypeT]): TransposeNDOrderedStreamExpr = TransposeNDOrderedStreamExpr({
    def genReorderedStmType(dimOrder: Seq[ArithTypeT], lentvSeq: Seq[ArithTypeT], dtv: BasicDataTypeVar): OrderedStreamType = {
      assert(dimOrder.nonEmpty, "dimOrder must have at least one element")
      if (dimOrder.length <= 1) {
        OrderedStreamType(dtv, lentvSeq(dimOrder.last.ae.evalInt))
      } else {
        OrderedStreamType(genReorderedStmType(dimOrder.init, lentvSeq, dtv), lentvSeq(dimOrder.last.ae.evalInt))
      }
    }

    val dtv = BasicDataTypeVar()
    val lentvSeq = Seq.fill(dimOrder.length)(ArithTypeVar())
    val inseqtv = OrderedStreamTypeVar(lentvSeq, dtv)
    val outseqt = genReorderedStmType(dimOrder, lentvSeq, dtv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, HWFunType(inseqtv, outseqt))), input.t), input)
  }, dimOrder)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TransposeNDOrderedStreamExpr = apply(inputs.head, types.asInstanceOf[Seq[ArithTypeT]])
  def unapply(expr: TransposeNDOrderedStreamExpr): Some[(Expr, Seq[ArithTypeT], Type)] =
    Some((expr.args.head, expr.types.asInstanceOf[Seq[ArithTypeT]], expr.t))
}

object Transpose extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val height = inputtc.t.asInstanceOf[VectorTypeT].len.ae.evalInt
    val width = inputtc.t.asInstanceOf[VectorTypeT].et.asInstanceOf[VectorTypeT].len.ae.evalInt
    val v = ArithTypeVar()
    SplitVector(PermuteVector(JoinVector(inputtc), ArithLambdaType(v, v/width + (v%width) * height)), height)
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head)
}
