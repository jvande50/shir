package backend.hdl.arch

import backend.hdl.arch.MapCompiler.getUnboundParams
import backend.hdl.arch.mem.{WriteAsync, WriteSync}
import core.{ArithTypeT, Expr, LambdaT, Let, Marker, ParamDef, ParamUse, TextType}

object MapCompilerHelper {

  /**
   * Inserting RepeatHidden to data sources. We also introduce marker "LeaveGuard" to help us to identify which
   * RepeatHiddens are inserted by this method.
   *
   * @param expr       input expression.
   * @param repetition repetition for RepeatHidden.
   */
  def insertRepeatHiddenToDataSourceNodes(expr: Expr, repetition: ArithTypeT, boundParamIds: Seq[Long] = Seq()): Expr = expr match {
    case count@CounterInteger(_, _, _, _, _, _) => Marker(RepeatHidden(count, repetition), TextType("LeaveGuard"))
    case cst@ConstantValue(_, _) => Marker(RepeatHidden(cst, repetition), TextType("LeaveGuard"))
    case wrt@WriteAsync(_, _, _, _) => Marker(RepeatHidden(wrt, repetition), TextType("LeaveGuard"))
    case wrt@ReduceOrderedStream(ArchLambda(_, ArchLambda(_, WriteSync(_, _, _, _), _), _), _, win, _) if
      // Avoid inserting RepeatHidden if write access has a param. Then it is not data source anymore.
      // TODO: Not general. Need to consider Reduce(Reduce(Write)...) and WriteAsync(...).
      getUnboundParams(win, boundParamIds).length == 0 =>  // unBoundParam means the stored data can be changed and should not be repeated by RepeatHidden.
      Marker(RepeatHidden(wrt, repetition), TextType("LeaveGuard"))
    case Let(p, body, arg, _) if getUnboundParams(arg, boundParamIds).length == 0 =>
      Let(p, insertRepeatHiddenToDataSourceNodes(body, repetition, boundParamIds :+ p.id),
        insertRepeatHiddenToDataSourceNodes(arg, repetition, boundParamIds))
    case Alternate(f1, f2, in, _) => // repetition should be divided by 2 since there are 2 function copies.
      val newRepetition = (repetition.ae.evalInt + 1) / 2
      Alternate(
        insertRepeatHiddenToDataSourceNodes(f1, newRepetition, boundParamIds),
        insertRepeatHiddenToDataSourceNodes(f2, newRepetition, boundParamIds),
        insertRepeatHiddenToDataSourceNodes(in, repetition, boundParamIds)
      )
    case e: Expr => e.build(e.children.map({
      case ce: Expr => insertRepeatHiddenToDataSourceNodes(ce, repetition, boundParamIds)
      case t => t
    }))
  }

  /**
   * Check if an expression contains RepeatHiddens inserted by "insertRepeatHiddenToDataSourceNodes" method.
   */
  def detectRepeatHiddenToDataSourceNodes(expr: Expr): Boolean = {
    var found = false
    expr.visit {
      case Marker(RepeatHidden(CounterInteger(_, _, _, _, _, _), _, _), ts) if ts.s.contains("LeaveGuard") => found = true
      case Marker(RepeatHidden(ConstantValue(_, _), _, _), ts) if ts.s.contains("LeaveGuard") => found = true
      case Marker(RepeatHidden(WriteAsync(_, _, _, _), _, _), ts) if ts.s.contains("LeaveGuard") => found = true
      case Marker(ReduceOrderedStream(ArchLambda(_, ArchLambda(_, WriteSync(_, _,_, _), _), _), _, _, _), ts) if
        ts.s.contains("LeaveGuard") => found = true
      case _ =>
    }
    found
  }

  /**
   * Inserting RepeatHiddenInverse to data sources with RepeatHiddens. This aims at canceling unnecessary repetitions.
   *
   * @param expr        input expression.
   * @param inputLength repetition for RepeatHidden.
   */
  def insertRepeatHiddenInverseIntoWrite(expr: Expr, repetition: ArithTypeT): Expr =
    expr.visitAndRebuild {
      case Marker(RepeatHidden(cst@ConstantValue(_, _), repetition, _), ts) if ts.s.contains("LeaveGuard") =>
        Marker(RepeatHidden(InverseRepeatHidden(cst, repetition), repetition), ts)
      case Marker(RepeatHidden(wrt@WriteAsync(_, _, _, _), repetition, _), ts) if ts.s.contains("LeaveGuard") =>
        Marker(RepeatHidden(InverseRepeatHidden(wrt, repetition), repetition), ts)
      case Marker(RepeatHidden(count@CounterInteger(_, _, _, _, _, _), repetition, _), ts) if ts.s.contains("LeaveGuard") =>
        Marker(RepeatHidden(InverseRepeatHidden(count, repetition), repetition), ts)
      case e => e
    }.asInstanceOf[Expr]

  /**
   * Inserting RepeatHiddenInverse to data sources with RepeatHiddens in Let.
   * The case with stream is excluded since a stream must be repeated if we want to use it multiple times.
   *
   * @param expr        input expression.
   * @param inputLength repetition for RepeatHidden.
   */
  def insertRepeatHiddenInverseIntoLet(expr: Expr, repetition: ArithTypeT): Expr =
    expr.visitAndRebuild {
      case Marker(RepeatHidden(cst@ConstantValue(_, _), repetition, _), ts) if ts.s.contains("LeaveGuard") =>
        Marker(RepeatHidden(InverseRepeatHidden(cst, repetition), repetition), ts)
      case Marker(RepeatHidden(wrt@WriteAsync(_, _, _, _), repetition, _), ts) if ts.s.contains("LeaveGuard") =>
        Marker(RepeatHidden(InverseRepeatHidden(wrt, repetition), repetition), ts)
      case Marker(RepeatHidden(count@CounterInteger(_, _, _, _, _, _), repetition, _), ts) if ts.s.contains("LeaveGuard") =>
        Marker(RepeatHidden(InverseRepeatHidden(count, repetition), repetition), ts)
        // TODO: Might be dangerous. Need to test.
      case e => e
    }.asInstanceOf[Expr]

  /**
   * Inserting RepeatHiddenInverse to Let's arg and write's input.
   */
  def insertRepeatHiddenInverseToLetAndWrite(expr: Expr, repetition: ArithTypeT): Expr =
    expr.visitAndRebuild {
      case Let(p, body, arg, _) if detectRepeatHiddenToDataSourceNodes(arg) && getUnboundParams(arg, Seq(p.id)).length == 0 =>
        // Mainly for the case with memory const, which should be always valid (no need for RepeatHiddens).
        Let(p, body, insertRepeatHiddenInverseIntoLet(arg, repetition))
      case WriteAsync(ctrl, baseAddr, inputBody, _) if detectRepeatHiddenToDataSourceNodes(inputBody) =>
        WriteAsync(ctrl, baseAddr, insertRepeatHiddenInverseIntoWrite(inputBody, repetition))
      case e => e
    }.asInstanceOf[Expr]

  /**
   * Removing the markers introduced by insertRepeatHiddenToDataSourceNodes.
   */
  def insertRepeatHiddenCleanup(expr: Expr): Expr =
    expr.visitAndRebuild {
      case Marker(input, ts) if ts.s.contains("LeaveGuard") => input
      case e => e
    }.asInstanceOf[Expr]

  /**
   * Inserting RepeatHidden and RepeatHiddenInverse.
   */
  def insertRepeatHiddenAndRepeatHiddenInverse(expr: Expr, repetition: ArithTypeT, boundParamIds: Seq[Long] = Seq()): Expr =
    insertRepeatHiddenCleanup(
      insertRepeatHiddenInverseToLetAndWrite(
        insertRepeatHiddenToDataSourceNodes(expr, repetition, boundParamIds),
        repetition
      )
    )

  /**
   * Inserting RepeatHidden to nested Lambda.
   */
  def insertRepeatHiddenToHighOrderLambda(expr: Expr, repetition: ArithTypeT): Expr =
    expr match {
      case ArchLambda(p, body, _) => ArchLambda(p, insertRepeatHiddenToHighOrderLambda(body, repetition))
      case e: Expr => RepeatHidden(e, repetition)
      case e => e
    }

  /** Check if there is a top RepeatHidden from a high-order function*/
  def checkLambdaTopRepeatHidden(expr: Expr): Boolean = expr match {
    case ArchLambda(_, body, _) => checkLambdaTopRepeatHidden(body)
    case RepeatHidden(_, _, _) => true
    case _ => false
  }

  /** Get top RepeatHidden from a high-order function*/
  def getLambdaTopRepeatHidden(expr: Expr): Option[ArithTypeT] = expr match {
    case ArchLambda(_, body, _) => getLambdaTopRepeatHidden(body)
    case RepeatHidden(_, rep, _) => Some(rep)
    case _ => None
  }

  /** remove top RepeatHidden from a high-order function*/
  def removeLambdaTopRepeatHidden(expr: Expr): Expr = expr match {
    case ArchLambda(p, body, _) => ArchLambda(p, removeLambdaTopRepeatHidden(body))
    case RepeatHidden(input, _, _) => input
    case e => e
  }
}
