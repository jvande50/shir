package backend.hdl.arch.costModel

import algo.Mul
import backend.hdl.arch.ArchHelper.{getLambdaBody, getLambdaParamIds, getLambdaParams}
import backend.hdl.arch.device.Mul2AddInt
import backend.hdl.arch.mem._
import backend.hdl.arch._
import backend.hdl._
import core._

object CostModel {

  /** Intel Arria 10 bit width constraints for small integer multipliers.*/
  def arria10MulBitWidthCheck(bitWidth1: Int, bitWidth2: Int): Boolean =
    (bitWidth1 <= 18 && bitWidth2 <= 19) || (bitWidth1 <= 19 && bitWidth2 <= 18)

  /**
   * Calculate the number of DSP blocks.
   * Note that the unit here is DSP block, not DSP. A DSP supports 2 blocks.
   */
  def estimateDSPBlocks(expr: Expr): Int = expr match {
    case Mul(i, _) =>
      val numOfDSPs =
        i.t match {
          case algo.TupleType(Seq(sit1: algo.SignedIntTypeT, sit2: algo.SignedIntTypeT)) if
            arria10MulBitWidthCheck(sit1.width.ae.evalInt, sit2.width.ae.evalInt) => 1
          case algo.TupleType(Seq(it1: algo.IntTypeT, it2: algo.IntTypeT)) if
            arria10MulBitWidthCheck(it1.width.ae.evalInt, it2.width.ae.evalInt) => 1
          case algo.TupleType(Seq(_: algo.FloatTypeT, _: algo.FloatTypeT)) => 2
          case _ => 2
        }
      numOfDSPs + estimateDSPBlocks(i)
    case MulInt(i, _) => val numOfDSPs =
      i.t match {
        case TupleType(Seq(sit1: SignedIntTypeT, sit2: SignedIntTypeT)) if
          arria10MulBitWidthCheck(sit1.bitWidth.ae.evalInt, sit2.bitWidth.ae.evalInt) => 1
        case TupleType(Seq(it1: IntTypeT, it2: IntTypeT)) if
          arria10MulBitWidthCheck(it1.bitWidth.ae.evalInt, it2.bitWidth.ae.evalInt) => 1
        case _ => 2
      }
      numOfDSPs + estimateDSPBlocks(i)
    case MulFloat(i, _) => 2 + estimateDSPBlocks(i)
    case Mul2AddInt(i, _) => 2 + estimateDSPBlocks(i)
    case MapVector(f, i, t: VectorTypeT) => t.len.ae.evalInt * estimateDSPBlocks(f) + estimateDSPBlocks(i)
    case e => e.children.map(c => c match {
      case ce: Expr => estimateDSPBlocks(ce)
      case _ => 0
    }).foldLeft(0)(_+_)
  }

  /**
   * Calculate the number of wires to a function.
   * Note that MapVec(x -> FunCall(x), ...) is treated as an invalid case and is not considered.
   * Only support Arch level since Algo level lacks of bit width information of SeqTypeT.
   */
  def estimateInputWiring(expr: Expr): Map[ParamDef, Int] = {
    var res: Map[ParamDef, Int] = Map()
    expr.visit {
      case Let(p, body, arg@ArchLambda(_), t) =>
        val funParams = getLambdaParams(arg)
        var numberOfCalls = 0
        // Get input bit width
        val portBitWidth = funParams.map(p => p.t match {
          case t: UnorderedStreamTypeT => t.leafType.bitWidth.ae.evalInt // TopType for stream-based data is UnorderedStreamTypeT
          case t: BasicDataTypeT => t.bitWidth.ae.evalInt
          case _ => ???
        }).foldLeft(0)(_+_)
        // Get number of calls
        body.visit{
          case ParamUse(pd) if pd.id == p.id => numberOfCalls = numberOfCalls + 1
          case _ =>
        }
        res = res + (p -> portBitWidth * numberOfCalls)
      case _ =>
    }
    res
  }

  def estimateOutputWiring(expr: Expr): Map[ParamDef, Int] = {
    var res: Map[ParamDef, Int] = Map()
    expr.visit {
      case Let(p, body, arg@ArchLambda(_), t) =>
        val funBody = getLambdaBody(arg)
        var numberOfCalls = 0
        // Get input bit width
        val portBitWidth = funBody.t match {
          case t: UnorderedStreamTypeT => t.leafType.bitWidth.ae.evalInt // TopType for stream-based data is UnorderedStreamTypeT
          case t: BasicDataTypeT => t.bitWidth.ae.evalInt
          case _ => ???
        }
        // Get number of calls
        body.visit {
          case ParamUse(pd) if pd.id == p.id => numberOfCalls = numberOfCalls + 1
          case _ =>
        }
        res = res + (p -> portBitWidth * numberOfCalls)
      case _ =>
    }
    res
  }

  /**
   * Calculate the number of data read (in bit), with the consideration of repeating read.
   * Only consider read access for now since NN usually requires a lots of memory reads.
   *
   * @param expr input expression.
   * @param mapRead number of read accesses should be handled by Map-Repeat.
   * @param repeatOptimization Consider optimization related to Repeat.
   * @return a tuple of repeatable read, non-repeatable read, and map-read
   */
  def estimateHostReadAccess(expr: Expr, mapRead: Map[Long, Int] = Map(), repeatOptimization: Boolean = false): (Int, Int) = expr match {
    // Handle Reads
    case Read(mem, addr, t: VectorTypeT) =>
      var numOfReads: Int = 0
      mem.t match {
        case rt: RamArrayTypeT if rt.memoryLocation.isInstanceOf[HostRamTypeT] => numOfReads = t.bitWidth.ae.evalInt
        case _ =>
      }
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)
    case ReadOutOfBound(mem, addr, _, t: VectorTypeT) =>
      var numOfReads: Int = 0
      mem.t match {
        case rt: RamArrayTypeT if rt.memoryLocation.isInstanceOf[HostRamTypeT] => numOfReads = t.bitWidth.ae.evalInt
        case _ =>
      }
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)
    case ReadAsyncOrdered(_, _, addr, t: OrderedStreamTypeT) =>
      val numOfReads = t.et.bitWidth.ae.evalInt * t.len.ae.evalInt
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)
    case ReadAsyncOrderedOutOfBound(_, _, addr, _, t: OrderedStreamTypeT) =>
      val numOfReads = t.et.bitWidth.ae.evalInt * t.len.ae.evalInt
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)

    // Handle Writes
    // Note that if there is a buffer, a pair of read and write will be introduced.
    // Repeat Optimization will move Repeats down to the address of the buffer's Read primitive.
    case WriteAsync(_, _, input, _) if repeatOptimization =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)
    case Write(_, input, _) if repeatOptimization  =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _), _), _), _, input, _) if repeatOptimization  =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, ReduceOrderedStream(ArchLambda(_, ArchLambda(_,
    Write(_, _, _), _), _), _, _, _), _), _), _, input, _) if repeatOptimization =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)

    // Handle Maps
    case MapVector(f, i, t: VectorTypeT) =>
      val funNumOfReads = estimateHostReadAccess(f, mapRead, repeatOptimization)
      val prevNumOfReads = estimateHostReadAccess(i, mapRead, repeatOptimization)
      (prevNumOfReads._1 + t.len.ae.evalInt * funNumOfReads._1, prevNumOfReads._2 + t.len.ae.evalInt * funNumOfReads._2)
    case MapOrderedStream(f: LambdaT, i, t: OrderedStreamTypeT) =>
      // Estimate Repeated Reads
      val prevNumOfReads = estimateHostReadAccess(i, mapRead, repeatOptimization)
      val innerRead = prevNumOfReads._1 / t.len.ae.evalInt
      val newMapRead = Map(f.param.id -> innerRead) ++ mapRead
      // Estimate Reads happening inside Lambda
      val funNumOfReads = estimateHostReadAccess(f, newMapRead, repeatOptimization)
      (t.len.ae.evalInt * (funNumOfReads._1 + funNumOfReads._2), prevNumOfReads._2)
    case MapAsyncOrderedStream(f: LambdaT, ins, t: OrderedStreamTypeT) =>
      // Estimate Repeated Reads
      val prevNumsOfReads = ins.map(i => estimateHostReadAccess(i, mapRead, repeatOptimization))
      val prevNumOfReads = prevNumsOfReads.foldLeft((0, 0))((p, r) => (p._1 + r._1, p._2 + r._2))
      val params = getLambdaParamIds(f.asInstanceOf[Expr])
      val innerReads = prevNumsOfReads.map(p => p._1 / t.len.ae.evalInt)
      val newMapRead = params.zip(innerReads).toMap ++ mapRead
      // Estimate Reads happening inside Lambda
      val funNumOfReads = estimateHostReadAccess(f, newMapRead, repeatOptimization)
      (t.len.ae.evalInt * (funNumOfReads._1 + funNumOfReads._2) , prevNumOfReads._2)

    // Handle Repeat and the rest
    case Repeat(i, times, _) =>
      val prevNumOfReads = estimateHostReadAccess(i, mapRead, repeatOptimization)
      (prevNumOfReads._1 * times.ae.evalInt, prevNumOfReads._2)
    case ParamUse(pd) =>
      (mapRead.getOrElse(pd.id, 0), 0)
    case e => e.children.map(c => c match {
      case ce: Expr => estimateHostReadAccess(ce, mapRead, repeatOptimization)
      case _ => (0, 0)
    }).foldLeft((0, 0))((p, r) => (p._1 + r._1, p._2 + r._2))
  }

  def estimateHostReadCacheLines(expr: Expr, repeatOptimization: Boolean = false): Int = {
    val reads = estimateHostReadAccess(expr, repeatOptimization = repeatOptimization)
    (reads._1 + reads._2) / 512
  }
}
