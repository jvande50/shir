package backend.hdl.vhdl

import backend.hdl.{HWDataTypeT, LogicType, NaturalNumberType, NaturalRangeType, VectorType}
import core.TreeNode

import scala.language.implicitConversions

object VhdlIdentifier {
  implicit def strToIdentifier(s: String): VhdlIdentifier = VhdlIdentifier(s)
}

final case class VhdlCodeException(private val message: String, private val cause: Throwable = None.orNull) extends Exception(message, cause)

sealed trait VhdlCodeIR extends TreeNode[VhdlCodeIR]

final case class VhdlIdentifier(s: String) {
  assert(valid, "'" + s + "' is not a valid vhdl identifier")
  def valid: Boolean = s.nonEmpty && s.head.isLetter && s.forall(c => c == '_' || c.isLetterOrDigit)
  override def toString: String = s
}

final case class VhdlModule(name: VhdlIdentifier,
                            generics: Seq[VhdlGeneric],
                            ports: Seq[VhdlPort],
                            components: Seq[VhdlModule],
                            internalSignals: Seq[VhdlSignalT],
                            instantiations: Seq[VhdlModuleInstance],
                            statements: Seq[VhdlStatement],
                            template: Option[VhdlBehaviourTemplate]) extends VhdlCodeIR {
  def children: Seq[VhdlCodeIR] = components
}

final case class VhdlBehaviourTemplate(filename: String, signalSubstitutions: Map[String, VhdlSignalT], typeSubstitutions: Map[String, HWDataTypeT] = Map())

sealed trait VhdlSignalT {
  def id: VhdlIdentifier
  def purpose: VhdlIdentifier
  def vType: HWDataTypeT
  def toString: String
}

final case class VhdlSignal(id: VhdlIdentifier, purpose: VhdlIdentifier, vType: HWDataTypeT) extends VhdlSignalT {
  override def toString: String = id + "_" + purpose
}

final case class VhdlGeneric(purpose: VhdlIdentifier, value: VhdlConstant) extends VhdlSignalT {
  override def id: VhdlIdentifier = "g"
  override def vType: HWDataTypeT = value.vType
  override def toString: String = "" + purpose
}

final case class VhdlNatural(purpose: VhdlIdentifier) extends VhdlSignalT {
  override def id: VhdlIdentifier = "g"
  override def vType: HWDataTypeT = NaturalNumberType()
  override def toString: String = "" + purpose
}

final case class VhdlNaturalRange(purpose: VhdlIdentifier, startVal: VhdlConstant, endVal: VhdlConstant) extends VhdlSignalT {
  override def id: VhdlIdentifier = "g"
  override def vType: HWDataTypeT = NaturalRangeType(startVal.value.toInt, endVal.value.toInt)
  override def toString: String = "" + purpose
}

final case class VhdlPort(id: VhdlIdentifier, purpose: VhdlIdentifier, direction: VhdlSignalDirection, vType: HWDataTypeT) extends VhdlSignalT {
  override def toString: String = {
    if (purpose.s == VhdlPort.CLOCK_LABEL || purpose.s == VhdlPort.RESET_LABEL)
      purpose.s
    else
      id + "_" + direction.str + "_" + purpose
  }
}
object VhdlPort {
  final val CLOCK_LABEL = "clk"
  final val RESET_LABEL = "reset"
  final def CLOCK: VhdlPort = VhdlPort("p", CLOCK_LABEL, VhdlSignalDirection.IN, LogicType())
  final def RESET: VhdlPort = VhdlPort("p", RESET_LABEL, VhdlSignalDirection.IN, LogicType())
}

final case class VhdlModuleInstance(id: VhdlIdentifier, name: VhdlIdentifier, connections: Seq[VhdlConnection])

final case class VhdlStatement(code: String)

final case class VhdlConnection(sigA: VhdlSignalT, sigB: VhdlSignalT, sigAPart: Option[Int] = None, sigBPart: Option[Int] = None)

case class VhdlSignalDirection(str: VhdlIdentifier)
object VhdlSignalDirection {
  final val IN = VhdlSignalDirection("in")
  final val OUT = VhdlSignalDirection("out")
//  final val BUFFER = VhdlSignalDirection("buffer") // output that can be read by the entity itself
//  final val INOUT = VhdlSignalDirection("inout")
}

sealed trait VhdlConstant {
  def value: String
  def vType: HWDataTypeT
}

final case class VhdlConstantNatural(iValue: Int) extends VhdlConstant {
  override def value: String = iValue + ""
  override def vType: HWDataTypeT = NaturalNumberType()
}

final case class VhdlConstantNaturalVector(iValues: Seq[Int]) extends VhdlConstant {
  override def value: String = iValues.length match {
    case 1 => "(0 => " + iValues.head + ")"
    case _ => iValues.mkString ("(", ", ", ")")
  }
  override def vType: HWDataTypeT = VectorType(NaturalNumberType(), iValues.length)
}

final case class VhdlConstantLogic(bValue: Boolean) extends VhdlConstant {
  override def value: String = "'" + (if (bValue) "1" else "0") + "'"
  override def vType: HWDataTypeT = LogicType()
}

final case class VhdlConstantLogicVector(logicvector: String) extends VhdlConstant {
  override def value: String = "\"" + logicvector + "\""
  override def vType: HWDataTypeT = VectorType(LogicType(), logicvector.length)
}
object VhdlConstantLogicVector {
  def apply(boolvalues: Seq[Boolean]): VhdlConstantLogicVector =
    VhdlConstantLogicVector(
      boolvalues.map{
        case true => "1"
        case false => "0"
      }.mkString
    )
  def apply(value: Int): VhdlConstantLogicVector = VhdlConstantLogicVector(value.toBinaryString)
  def apply(value: Int, width: Int): VhdlConstantLogicVector = {
    // Direct conversion of negative integer always introduce a 32-bit binary string.
    // To fix the bit width, we first turn a negative value into a positive one.
    // With this, the actual bit width is the length of positive binary string plus one (signed bit).
    val binValue: String = if(value < 0)
      value.toBinaryString.takeRight((~value).toBinaryString.length + 1)
    else
      value.toBinaryString
    if (binValue.length > width)
      throw VhdlCodeException("Binary value for " + value + " exceeds precision of " + width + " bits!")
    VhdlConstantLogicVector(binValue.reverse.padTo(width, if(value < 0) '1' else '0').reverse)
  }
}
