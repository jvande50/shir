package backend.hdl.vhdl

import backend.hdl.util.VhdlIndention
import backend.hdl._
import core.TypeVarT
import core.util.ToFiles

import scala.collection.immutable.ListMap
import scala.io.Source

final case class VhdlProject(modules: Map[String, String], commonPackage: String) extends ToFiles {

  override def toStringIterators: Map[String, Iterator[String]] = {
    (modules ++ Map(VhdlProject.TYPE_PACKAGE -> commonPackage)).map(
      entry =>
        entry._1 + "." + VhdlProject.VHDL_FILE_EXTENSION -> VhdlIndention.indent(entry._2).split('\n').iterator
    )
  }

}

object VhdlProject {

  final val TYPE_PACKAGE: String = "common"

  final val VHDL_FILE_EXTENSION: String = "vhd"

  final val VHDL_TEMPLATES_FOLDER: String = "vhdltemplates"

  final val STATIC_VHDL_TEMPLATES: Seq[String] = Seq("ram_sp", "ram_dp", "fifo_ram_buffer", "fifo_reg_buffer")

  def apply(vm: VhdlModule): VhdlProject = {
    val vhdlTypes = getVhdlTypes(vm)

    val vhdlInlineTypes = vhdlTypes.map(elem => (elem._1, elem._2._1))
    // Q: why reverse? A (short): in order to keep the types in the right order and to adhere to dependencies
    // (the detailed answer has to with how scala merges to maps and replaces/updates values, and how ListMaps order gets changed, when keys are updated.)
    val vhdlPackageTypes: Seq[String] = vhdlTypes.map(elem => elem._2._2).filter(_.nonEmpty).toSeq.reverse.distinct
    val vhdlPackageTypesString =
      getIEEEPackages +
        "package " + TYPE_PACKAGE + " is\n" +
        vhdlPackageTypes.mkString("", "\n", "\n") +
        "end package;"

    val staticVhdlStrings = STATIC_VHDL_TEMPLATES.map(filename => {
      val file = Source.fromFile(VHDL_TEMPLATES_FOLDER + "/" + filename + "." + VHDL_FILE_EXTENSION)
      val content = file.getLines().mkString("\n")
      file.close()
      filename -> content
    }).toMap

    VhdlProject(staticVhdlStrings ++ _toStrings(vm, vhdlInlineTypes), vhdlPackageTypesString)
  }

  private def getIEEEPackages = "library ieee;\nuse ieee.std_logic_1164.all;\nuse ieee.numeric_std.all;\nuse ieee.math_real.all;\n"

  /**
    *
    * @return an ordered listmap, which contains a string tuple (value of the map) of the inline typename and the package definition for each vhdl data type (key of the map), that occurs in the given node
    */
  private def getVhdlTypes(vm: VhdlModule): ListMap[HWDataTypeT, (String, String)] =
    ListMap((vm.ports ++ vm.generics ++ vm.internalSignals).map(_.vType).flatMap(getVhdlType): _*) ++ ListMap(vm.components.flatMap(getVhdlTypes): _*)

  /**
    * note: when returning the listmap, add the innermost types last.
    *   e.g. VectorType(ElementType, Length) should return a listmap, that contains the entry for the VectorType first, followed by the entry for the ElementType (and possible further children)
    *
    * @return an ordered listmap, which contains a string tuple (value of the map) of the inline typename and the package definition for the given vhdl data type and its child types
    */
  private def getVhdlType(vType: HWDataTypeT): ListMap[HWDataTypeT, (String, String)] = {
    val typeId = "type_" + vType.toString.replaceAll("\\W", "")
    vType match {
      case OrderedStreamType(et, _) =>
        val childTypes = getVhdlType(et)
        ListMap((vType,
          (typeId,
            "alias " + typeId + " is " + childTypes(et)._1 + ";"
//            "type " + typeId + " is record\n" +
//              STREAM_DATA + ": " + childTypes(et)._1 + ";\n" +
//              "end record " + typeId + ";"
          )
        )) ++ childTypes
      case UpperBoundedStreamType(et, _) =>
        val childTypes = getVhdlType(et)
        ListMap((vType,
          (typeId,
            "alias " + typeId + " is " + childTypes(et)._1 + ";"
          )
        )) ++ childTypes
      case uos @ UnorderedStreamType(et, _) =>
        val childTypes = getVhdlType(et) ++ getVhdlType(uos.indexType)
        ListMap((vType,
          (typeId,
              "type " + typeId + " is record\n" +
              IndexedDataType.DATA_KEY + ": " + childTypes(et)._1 + ";\n" +
              IndexedDataType.INDEX_KEY + ": " + childTypes(uos.indexType)._1 + ";\n" +
              "end record " + typeId + ";"
          )
        )) ++ childTypes
      case _: UnitTypeT =>
        ListMap((vType, (typeId, "subtype " + typeId + " is std_logic_vector(-1 downto 0);")))
      case _: LogicTypeT =>
        ListMap((vType, (typeId, "subtype " + typeId + " is std_logic;")))
      case lt: LastVectorTypeT =>
        ListMap((vType, (typeId, "subtype " + typeId + " is std_logic_vector(" + (lt.bitWidth.ae - 1) + " downto 0);")))
      case rt: ReadyVectorTypeT =>
        ListMap((vType, (typeId, "subtype " + typeId + " is std_logic_vector(" + (rt.bitWidth.ae - 1) + " downto 0);")))
      case _: NaturalNumberType =>
        ListMap((vType, (typeId, "subtype " + typeId + " is natural;")))
      case NaturalRangeType(startVal, endVal) =>
        ListMap()
        //ListMap((vType, (typeId, "subtype " + typeId + " is natural range " + startVal.ae.evalInt + " to " + endVal.ae.evalInt + ";")))
      case i: IntTypeT =>
        ListMap((vType, (typeId, "subtype " + typeId + " is std_logic_vector(" + (i.bitWidth.ae - 1) + " downto 0);")))
      case i: SignedIntTypeT =>
        ListMap((vType, (typeId, "subtype " + typeId + " is std_logic_vector(" + (i.bitWidth.ae - 1) + " downto 0);")))
      case _: FixedTypeT => ???
      case f: FloatTypeT =>
        ListMap((vType, (typeId, "subtype " + typeId + " is std_logic_vector(" + (f.bitWidth.ae - 1) + " downto 0);")))
      case vt: VectorTypeT => vt.et match {
        case LogicType() =>
          ListMap((vType, (typeId, "subtype " + typeId + " is std_logic_vector(" + (vt.len.ae - 1) + " downto 0);")))
        case _ =>
          val childType = getVhdlType(vt.et)
          ListMap((vType, (typeId, "type " + typeId + " is array (" + (vt.len.ae.evalInt - 1) + " downto 0) of " + childType(vt.et)._1 + ";"))) ++ childType
      }
      case ntt: NamedTupleTypeT =>
        val childTypes = ListMap(ntt.namedTypes.values.toSeq.reverse.flatMap(getVhdlType): _*)
        val t1Name = vType.asInstanceOf[NamedTupleTypeT].namedTypes.head._1.toString
        val newTypeId = typeId + "_" + t1Name // patch typedId with the name of tuple's first element to avoid naming conflicts

        ListMap((vType,
          (newTypeId,
            "type " + newTypeId + " is record\n" +
              ntt.namedTypes.map(p => p._1 + ": " + childTypes(p._2)._1 + ";\n").mkString("") +
              "end record " + newTypeId + ";"
          )
        )) ++ childTypes
      case RamArrayType(_, _, _) =>
        throw VhdlCodeException("Cannot generate code that uses ArrayType!")
      case _: TypeVarT =>
        throw VhdlCodeException("Cannot generate code with type vars!")
      case HWDataType(_) | BasicDataType(_) | NonArrayType(_) | ScalarType(_) =>
        throw VhdlCodeException("Cannot generate code for abstract type!")
    }
  }

  private def _toStrings(vm: VhdlModule, types: Map[HWDataTypeT, String]): Map[String, String] =
    Map(vm.name.s -> _toString(vm, types)) ++ vm.components.flatMap(_toStrings(_, types)).toMap

  private def _toString(vm: VhdlModule, types: Map[HWDataTypeT, String]): String = {
    val templateCode: VhdlBehaviourLoadedTemplate = vm.template match {
      case Some(template) => loadTemplate(template, types)
      case None => VhdlBehaviourLoadedTemplate("", "", "", "", "")
    }

    // imports
    getIEEEPackages +
      "use work." + TYPE_PACKAGE + ".all;\n\n" +
      // entity
      "entity " + vm.name + " is\n" +
      (if (vm.generics.nonEmpty)
        vm.generics.map(g => {
          g + ": " + types(g.vType) + " := " + g.value.value
        }).mkString("generic(\n", ";\n", "\n);\n") else "") +
      (if (vm.ports.nonEmpty)
      vm.ports.map(p => {
        p + ": " + p.direction.str + " " + types(p.vType)
      }).mkString("port(\n", ";\n", "\n);\n") else "") +
      "end " + vm.name + ";\n\n" +
      // architecture
      "architecture behavioral of " + vm.name + " is\n" +
      // component declarations
      templateCode.components +
      vm.components.map(vc => {
        "component " + vc.name + "\n" +
          vc.ports.map(p => {
            p + ": " + p.direction.str + " " + types(p.vType)
          }).mkString("port(\n", ";\n", "\n);\n") +
        "end component;"
      }).mkString("\n", "\n", "\n") +
      // functions / procedures
      templateCode.functions + "\n" +
      // constants, type and signal declarations
      templateCode.declarations + "\n" +
      // internal signals (to connect components with each other)
      vm.internalSignals.map(sig =>
        sig match {
          case VhdlNaturalRange(identifier, startVal ,endVal) => "signal " + identifier + ": natural range " + startVal.value + " to " + endVal.value + " := 0;"
          case _ => "signal " + sig + ": " + types(sig.vType) + ";"
        }
      ).mkString("\n", "\n", "\n") +
      // begin
      "begin\n" +
      // component instantiations
      templateCode.instantiations + "\n" +
      vm.instantiations.map(vci => {
        vci.id + ": " + vci.name +
          // grouping to enforce that partial connections with the same sigA are defined consecutively (required by VHDL!)
          vci.connections.groupBy(_.sigA).flatMap(_._2).map(con => {
            con.sigA +
            (con.sigAPart match {
              case Some(part) => "(" + part + ")"
              case None => ""
            }) +
            " => " +
            con.sigB +
            (con.sigBPart match {
              case Some(part) => "(" + part + ")"
              case None => ""
            })
          }).mkString(" port map(\n", ",\n", "\n);\n")
      }).mkString("") +
      // behaviour
      templateCode.behaviour +
      vm.statements.map(_.code).filterNot(_.isEmpty).mkString("\n", "\n", "\n") +
      "end behavioral;\n"
  }

  case class VhdlBehaviourLoadedTemplate(components: String, functions: String, declarations: String, instantiations: String, behaviour: String)

  private def loadTemplate(template: VhdlBehaviourTemplate, types: Map[HWDataTypeT, String]): VhdlBehaviourLoadedTemplate = {
    val fullFilename = VHDL_TEMPLATES_FOLDER + "/" + template.filename + "." + VHDL_FILE_EXTENSION
    val file = Source.fromFile(fullFilename)
    // get file content, but remove lines that contain '%TESTING_ONLY' string, which are for testing  of the template only
    val rawcontent = file.getLines.filterNot(_.contains("%TESTING_ONLY")).map(_.trim).mkString("\n")
    file.close()

    val subs = template.signalSubstitutions.map(s => s._1 -> s._2.toString).foldLeft(rawcontent) { case (z, (s, r)) => z.replaceAll(s, r) } // TODO replace only if a non-word character (\\W) is before and after
    val content = template.typeSubstitutions.map(s => s._1 -> types(s._2)).foldLeft(subs) { case (z, (s, r)) => z.replaceAll(s, r) } // TODO replace only if a non-word character (\\W) is before and after

    // filter the code of the architecture: declaration (before 'begin') and statements (after 'begin')
    // explanation of this regex:
    // unanchored                       -> (at the end of the regex) allows this regex to start anywhere in the string
    // 'architecture ANY of ANY is'     -> from this line on ...
    // capture component declarations:
    //    (?s)\s*                       -> allow line breaks and any whitespace characters
    //    (component                    -> capture starting from 'component'
    //    .*                            -> greedily (as much as possible) allow any character
    //    end\ component;)              -> until 'end component' (including)
    //    ?                             -> allow 0-1 component declaration blocks
    // capture functions:
    //    (?s)\s*                       -> allow line breaks and any whitespace characters
    // TODO capture procedures as well!

    //    (function                     -> capture starting from 'function'
    //    .*                            -> greedily (as much as possible) allow any character
    //    end\ function;)               -> until 'end function' (including)
    //    ?                             -> allow 0-1 functions blocks
    // capture declarations:
    //    (?s)                          -> allow line breaks
    //    (.*?)                         -> capture anything, until
    //    'begin'                       -> is found
    // capture component instantiations:
    //    (?s)                          -> allow line breaks
    //    (                             -> start to capture anything, until
    //    .*                            -> greedily (as much as possible) allow any character, until
    //    'port map'                    -> is found (which indicates a component instantiation)
    //    .*?                           -> allow any character, until
    //    ');'                          -> is found
    //    ?                             -> allow 0-1 component instantiation blocks
    // capture behaviour:
    //    (?s)                          -> allow line breaks
    //    (.*?)                         -> capture anything (after 'begin', without component instantiations), until
    //    'end behavioral;'             -> is found

    val regex = """architecture.*?of.*?\ is(?s)\s*(component.*end\ component;)?(?s)\s*(function.*end\ function;)?(?s)(.*?)begin(?s)(.*port\ map.*?\);)?(.*?)end behavioral;""".r.unanchored
    content match {
      case regex(components, functions, declarations, instantiations, behaviour) =>
        VhdlBehaviourLoadedTemplate(
          if (components != null) components.trim else "",
          if (functions != null) functions.trim else "",
          if (declarations != null) declarations.trim else "",
          if (instantiations != null) instantiations.trim else "",
          if (behaviour != null) behaviour.trim else ""
        )
      case _ => throw VhdlCodeException("cannot find matching architecture in " + fullFilename)
    }
  }
}
