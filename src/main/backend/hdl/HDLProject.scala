package backend.hdl

import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemLayoutCompiler, MemoryImage}
import backend.hdl.arch.tiling.{PaddingCompiler, ParallelizationCompiler, TranspositionCompiler}
import backend.hdl.arch.{ArchCompiler, MapCompiler, Sink, TestBench}
import backend.hdl.graph.{GraphGenerator, NodeT}
import backend.hdl.sim.{ModelSimExec, SimResult}
import backend.hdl.vhdl.{VhdlGenerator, VhdlModule, VhdlProject}
import core.{Expr, TypeChecker}
import core.compile.{CompilerFlow, CompilerPass, CompilerPhase}
import core.rewrite.RewriteStep
import core.util.IRDotGraph

import java.io.File

final case class HDLProject(
    name: String,
    expr: Expr,
    startPhase: CompilerPhase = CompilerPhase.first(),
    rewrites: Seq[(CompilerPhase, RewriteStep)] = Seq(),
    passes: Seq[CompilerPass] =
      Seq(
        ArchCompiler,
        TranspositionCompiler,
        PaddingCompiler,
        ParallelizationCompiler,
        MemLayoutCompiler,
        MemFunctionsCompiler,
        DeviceSpecificCompiler,
        MapCompiler
      )
  ) extends CompilerFlow {

  val OUTPUT_FOLDER: String = "out"

  val PROJECT_FOLDER: String = OUTPUT_FOLDER + "/" + name

  private def prepareFolders(): Unit = {
    val folder = new File(PROJECT_FOLDER)
    if (folder.exists() && folder.isDirectory) {
      folder.listFiles(_.getName.endsWith("." + VhdlProject.VHDL_FILE_EXTENSION)).foreach(_.delete)
    } else {
      folder.mkdirs()
    }
  }

  lazy val compiledExpr: Expr = compile(TypeChecker.check(expr), startPhase)

  lazy val dataflowGraph: NodeT = GraphGenerator.generate(compiledExpr)

  lazy val compileHDL: VhdlModule = VhdlGenerator.generate(dataflowGraph)

  def writeHDLFiles(): Unit = {
    prepareFolders()
    VhdlProject(compileHDL).toFiles(PROJECT_FOLDER)
  }

  def writeAllFiles(inputData: Map[String, Seq[_]]): Unit = {
    writeHDLFiles()
    MemoryImage(compiledExpr, inputData).toFiles(PROJECT_FOLDER)
  }

  def simulateStandalone(): Unit = {
    val dataflowGraph: NodeT = GraphGenerator.generate(TestBench(Sink(compiledExpr)))
    val compileHDL: VhdlModule = VhdlGenerator.generate(dataflowGraph)
    prepareFolders()
    VhdlProject(compileHDL).toFiles(PROJECT_FOLDER)
    // simulate HDL (w/o copying hostram / tb file)
    ModelSimExec.debug(PROJECT_FOLDER)
  }

  def simulateWithHostRam(data: Map[String, Seq[_]], useOnBoardRAM: Boolean = false): SimResult = {
    writeHDLFiles()
    MemoryImage(compiledExpr, data).toFiles(PROJECT_FOLDER)
    // copy hostram + tb_top seperately here?
    ModelSimExec.run(PROJECT_FOLDER, useOnBoardRAM)
  }

  def debugWithHostRam(data: Map[String, Seq[_]]): Unit =  {
    writeHDLFiles()
    MemoryImage(compiledExpr, data).toFiles(PROJECT_FOLDER)
    // copy hostram + tb_top seperately here?
    ModelSimExec.debug(PROJECT_FOLDER)
  }
}
