package backend.hdl.graph

import backend.hdl.arch.ArchLambda
import backend.hdl.{HWDataTypeT, HWFunType, HWFunTypeT, NamedTupleTypeT, SyncCommunication}
import core.{Expr, FunctionCall, Type}

object GraphGeneratorHelper {
  def getAllHWFunIOs(ft: Type): Seq[HWDataTypeT] = ft match {
    case HWFunType(inType: HWDataTypeT, innerFt: Type, SyncCommunication, _) => inType +: getAllHWFunIOs(innerFt)
    case t: HWDataTypeT => Seq(t) // This should be the output type
  }

  def getAllFunCallArgs(expr: Expr): Seq[Expr] = expr match {
    case FunctionCall(f, arg, _) => getAllFunCallArgs(f) :+ arg
    case e => Seq(e) // This should be the function
  }

  def getArchLambdaDepth(expr: Expr): Int = expr match {
    case ArchLambda(_, body, _) => getArchLambdaDepth(body) + 1
    case _ => 0 // This should be the function
  }

  /** Check if all communication are Sync. */
  def isSyncCommunication(ft: Type): Boolean = ft match {
    case HWFunType(_, innerFt: Type, SyncCommunication, _) => isSyncCommunication(innerFt)
    case HWFunType(_, _: Type, _, _) => false
    case _ => true
  }
}
