package backend.hdl.graph.util

import core.util.{DotColor, DotConnection, DotGraph, DotGraphComponent, DotNode, DotSubGraph, Log}
import backend.hdl.graph._

object DataFlowDotGraph {

  private final val PORT_LABEL_LENGTH = 1 // 0, 1, 2, 3, 4, 5

  def apply(dfir: GraphIR): DotGraph = {
    val dot = visit(dfir, collectPortNodes(dfir))
    dot match {
      case g: DotSubGraph => DotGraph(g)
      case n: DotNode => DotGraph(DotSubGraph(Seq(n), Seq()))
      case _ => throw new RuntimeException("never happens")
    }
  }

  private def getPortString(port: PortT): String = PORT_LABEL_LENGTH match {
    case 0 => ""
    case 1 => port.dataType.toShortString.replaceAll("\\W", "").filter(_.isUpper)
    case 2 => port.dataType.toShortString.replaceAll("\\W", "")
    case 3 => port.dataType.toShortString.replaceAll("\\W", "") + (if (port.param.isDefined) "_p" else "")
    case 4 => "p_" + port.flatten.map(_.id).mkString("_") + "_" + port.dataType.toShortString.replaceAll("\\W", "")
    case 5 => "p_" + port.flatten.map(_.id).mkString("_") + "_" + port.dataType.toShortString.replaceAll("\\W", "") + (if (port.param.isDefined) "_param" else "")
  }

  private def collectPortNodes(dfir: GraphIR): Map[PortSimpleT, DotNode] = {
    (dfir match {
      case node: NodeT =>
        val ports = node match {
          case n: BehaviourNodeT => n.allPortsSimple
          case n: NodeT => n.portsSimple
        }
        ports.map(port => port.parent match {
          case None => port
          case Some(parent) => parent
        }).distinct
          .flatMap(port => {
            val dotNode = DotNode(getPortString(port))
            port.flatten.map(_ -> dotNode)
          }).toMap
      case _ => Map()
    }) ++
    dfir.children.flatMap(collectPortNodes).toMap
  }

  private def visit(dfir: GraphIR, portNodeMap: Map[PortSimpleT, DotNode]): DotGraphComponent = dfir match {
    case g: GraphT =>
      DotSubGraph(
        // sub nodes
        g.nodes.map(node => visit(node, portNodeMap)) ++
        // connections
        g.connections.map(con => {
          if (!portNodeMap.isDefinedAt(con.pout) || !portNodeMap.isDefinedAt(con.pin))
            throw MalformedGraphException("port for connection does not exist")
          DotConnection(portNodeMap(con.pout), portNodeMap(con.pin))
        })
      )
      //        (if (EDGE_LABELS && (con.pinPart.isDefined || con.poutPart.isDefined)) " [label=\"partly\"]" else "") + ";"
    case n: NodeT =>
      val graph = n.subGraph match {
        case Some(subGraph) => visit(subGraph, portNodeMap).asInstanceOf[DotSubGraph]
        case None => DotSubGraph()
      }
      // add port nodes
      val graphWithPorts = graph.addAll((n match {
        case nn: BehaviourNodeT => nn.allPortsSimple
        case nn: NodeT => nn.portsSimple
      }).map(portNodeMap(_)).distinct)
      // (if (PORTS_SAME_RANK) "{rank=same; " + n.portsSimple.map(getPortRep).toSet.mkString(";") + "}\n" else "") +
      n match {
        case BehaviourNodeMarker(_, _, label, highlight, _) => DotNode(label = label.s, color = (if (highlight) DotColor.HIGHLIGHT else ""), subGraph = Some(graphWithPorts))
        case _ => DotNode(label = n.name, subGraph = Some(graphWithPorts))
      }
  }

}
