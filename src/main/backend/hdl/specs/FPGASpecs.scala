package backend.hdl.specs

import backend.hdl.arch.Area
import backend.hdl.{BasicDataTypeT, HWFunTypeT, VectorType, VectorTypeT}
import core.ArithTypeT

object FPGA {
  def current: FPGASpecs = Arria10
}

trait FPGASpecs {

  def name: String

  def cachelineType: BasicDataTypeT
  def onBoardRamDataType: BasicDataTypeT
  def onBoardRamSize: ArithTypeT
  def onBoardRamBurstWidth: ArithTypeT
  def onBoardRamReqIdSize: ArithTypeT
  def readReqBufferSize: ArithTypeT
  def writeReqBufferSize: ArithTypeT
  def hostRamReadInterface: HWFunTypeT
  def hostRamWriteInterface: HWFunTypeT

  def usedArea: Area
  def availableArea: Area

  def ramBlockBits: Int
  def calcRamBlocks(datawidth: ArithTypeT, depth: ArithTypeT): ArithTypeT

}
