package backend.hdl.sim

import java.io.{File, PrintWriter}
import java.nio.file.{Files, Paths, StandardCopyOption}
import backend.hdl.arch.mem.MemFunctionsCompiler

import core.util.Log

import scala.collection.mutable
import scala.sys.process.{Process, ProcessLogger, stderr}

object ModelSimExec {

  // choose between slow and realistic simulation (false) or speed-up simulation with unrealistic, optimal memory behaviour (true)
  final val SPEED_UP_SIMULATION = true

  final val XTERM_BIN = "xterm -e "
  final val MODEL_SIM_BIN_PATHS = Seq("/opt/intelFPGA_pro/20.1/modelsim_ase/bin/vsim", "~/intelFPGA_pro/17.1/modelsim_ase/bin/vsim", "/add/your/path/here")
  final val MODEL_SIM_BIN = MODEL_SIM_BIN_PATHS.find(new File(_).exists()).getOrElse("vsim")
  final val SUPPRESS_WARNING_3116_OPTION = " -suppress 3116"
  final val COMMAND_LINE_OPTION = " -c"

  final val TESTING_VHD_FOLDER = "vhdltemplates/testing"
  final val TB_NO_LOCAL_RAM = "tb_top.vhd"
  final val TB_LOCAL_RAM = "tb_top_with_local_mem.vhd"

  // more realistic host ram simulation with possible out-of-order responses and random delays (which may take more overall simulation time)
  final val HOST_RAM_SLOW_VHD_FILE = "host_memory.vhd"
  // unrealistic 'optimal' host ram, with no delays and immediate responses (use this to decrease your simulation time)
  final val HOST_RAM_FAST_VHD_FILE = "host_memory_fast.vhd"

  final val LOCAL_RAM_SLOW_VHD_FILE = "local_memory.vhd"
  final val LOCAL_RAM_FAST_VHD_FILE = "local_memory_fast.vhd"

  final val HOST_RAM_VHD_FILE = if (SPEED_UP_SIMULATION) HOST_RAM_FAST_VHD_FILE else HOST_RAM_SLOW_VHD_FILE
  final val LOCAL_RAM_VHD_FILE = if (SPEED_UP_SIMULATION) LOCAL_RAM_FAST_VHD_FILE else LOCAL_RAM_SLOW_VHD_FILE

  final val DO_FILE = "sim.do"
  final val DO_GENERAL_INSTRUCTIONS =
    """
      |proc external_editor_ln {filename linenumber} {
      | exec gvim --remote-tab-silent +$linenumber $filename &
      | return
      |}
      |set PrefSource(altEditor) external_editor_ln
      |
      |vlib work
      |vcom -93 -check_synthesis -suppress 1246 common.vhd *.vhd
      |vsim tb_top
      |""".stripMargin
  final val DO_RUN_INSTRUCTIONS =
    """
      |run -all
      |quit
      |""".stripMargin

  def prepareSimulation(vhdProjectPath: String, useOnBoardRAM: Boolean = false, debug: Boolean = false): Unit = {
    if (useOnBoardRAM) {
      if (!new File(vhdProjectPath + "/" + TB_LOCAL_RAM).exists()) {
        Files.copy(Paths.get(TESTING_VHD_FOLDER + "/" + HOST_RAM_VHD_FILE), Paths.get(vhdProjectPath + "/" + HOST_RAM_VHD_FILE), StandardCopyOption.REPLACE_EXISTING)
        Files.copy(Paths.get(TESTING_VHD_FOLDER + "/" + LOCAL_RAM_VHD_FILE), Paths.get(vhdProjectPath + "/" + LOCAL_RAM_VHD_FILE), StandardCopyOption.REPLACE_EXISTING)
        Files.copy(Paths.get(TESTING_VHD_FOLDER + "/" + TB_LOCAL_RAM), Paths.get(vhdProjectPath + "/" + TB_LOCAL_RAM), StandardCopyOption.REPLACE_EXISTING)
      }
    }
    else {
      if (!new File(vhdProjectPath + "/" + TB_NO_LOCAL_RAM).exists()) {
        Files.copy(Paths.get(TESTING_VHD_FOLDER + "/" + HOST_RAM_VHD_FILE), Paths.get(vhdProjectPath + "/" + HOST_RAM_VHD_FILE), StandardCopyOption.REPLACE_EXISTING)
        Files.copy(Paths.get(TESTING_VHD_FOLDER + "/" + TB_NO_LOCAL_RAM), Paths.get(vhdProjectPath + "/" + TB_NO_LOCAL_RAM), StandardCopyOption.REPLACE_EXISTING)
      }
    }

    val pw = new PrintWriter(new File(vhdProjectPath + "/" + DO_FILE))
    pw.write(DO_GENERAL_INSTRUCTIONS)
    if (!debug)
      pw.write(DO_RUN_INSTRUCTIONS)
    pw.close()
  }

  def run(vhdProjectPath: String, useOnBoardRAM: Boolean = false): SimResult = {
    prepareSimulation(vhdProjectPath, useOnBoardRAM, debug = false)

    val command = MODEL_SIM_BIN + SUPPRESS_WARNING_3116_OPTION + COMMAND_LINE_OPTION + " -do " + DO_FILE
    Log.info(this, "simulating with command: " + command)
    val statsRegex = """.*<<<(.*?)\s*:\s*(.*?)>>>.*""".r
    val stats: mutable.Map[String, String] = mutable.LinkedHashMap()
    val tBefore = System.nanoTime()
    val errcode = Process(command, new File(vhdProjectPath)) ! ProcessLogger(
      {
        case line if line.contains("Error") && !line.contains("Errors: 0") => Log.error(this, line)
        case line if line.contains("Warning") && !line.contains("Warnings: 0") => Log.warn(this, line)
        case statsRegex(name, value) => stats.put(name, value)
        case _ => ()
      },
      line => stderr.println(line)  // error goes to console
    )
    val tAfter = System.nanoTime()
    if (errcode != 0)
      scala.sys.error("Nonzero exit value: " + errcode + " after " + (tAfter - tBefore) / 1000000 + "ms")

    Log.info(this, "finished modelsim simulation after " + (tAfter - tBefore) / 1000000 + "ms")
    SimResult(stats.toMap)
  }

  def debug(vhdProjectPath: String): Unit = {
    prepareSimulation(vhdProjectPath, useOnBoardRAM = false, debug = true)
    val command = XTERM_BIN + MODEL_SIM_BIN + SUPPRESS_WARNING_3116_OPTION + " -do " + DO_FILE
    println(command)
    val result = Process(command, new File(vhdProjectPath)).!
    println(result)
  }

}
