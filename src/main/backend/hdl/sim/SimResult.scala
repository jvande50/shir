package backend.hdl.sim

import core.util.Log

final case class SimResult(values: Map[String, String]) {

  val CORRECT_RESULT_KEY = "correctResult"
  val CYCLES_KEY = "cycles"

  val CYCLES_MAX_VALUE = 1000000

  def print(): Unit = {
    if (cycles >= CYCLES_MAX_VALUE) {
      Log.error(this, "Simulation did not write back any result!")
    }
    println(values.mkString("\n"))
  }

  def assertCorrect: SimResult = {
    assert(correct && cycles < CYCLES_MAX_VALUE)
    this
  }

  def correct: Boolean = values(CORRECT_RESULT_KEY) == "1"
  def cycles: Int = values(CYCLES_KEY).toInt

}
