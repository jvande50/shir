package backend.hdl

import core._

import scala.annotation.tailrec

case object CommunicationTypeKind extends Kind

sealed trait CommunicationTypeT extends MetaTypeT {
  override def kind: Kind = CommunicationTypeKind
}

final case class CommunicationType() extends CommunicationTypeT {
  override def build(newChildren: Seq[ShirIR]): Type = this
}

final case class CommunicationTypeVar(tvFixedId: Option[Long] = None) extends CommunicationTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = CommunicationTypeVar(tvFixedId)

  override def upperBound: Type = CommunicationType()
}

case object AsyncCommunicationTypeKind extends Kind

object AsyncCommunication extends CommunicationTypeT {
  override def kind: Kind = AsyncCommunicationTypeKind

  override def superType: Type = CommunicationType()

  override def build(newChildren: Seq[ShirIR]): Type = this
}

case object SyncCommunicationTypeKind extends Kind

object SyncCommunication extends CommunicationTypeT {
  override def kind: Kind = SyncCommunicationTypeKind

  override def superType: Type = CommunicationType()

  override def build(newChildren: Seq[ShirIR]): Type = this
}


case object HWFunTypeKind extends Kind

trait HWFunTypeT extends FunTypeT {
  val comType: CommunicationTypeT
  val schedulingStrategy: SchedulingStrategyTypeT

  override def kind: Kind = HWFunTypeKind

  override def superType: Type = superFunType

  override def children: Seq[Type] = Seq(inType, outType, comType, schedulingStrategy)
}

final case class HWFunType(inType: Type, outType: Type, comType: CommunicationTypeT = SyncCommunication, schedulingStrategy: SchedulingStrategyTypeT = DefaultScheduling()) extends HWFunTypeT {
  override def build(newChildren: Seq[ShirIR]): FunTypeT =
    HWFunType(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type], newChildren(2).asInstanceOf[CommunicationTypeT], newChildren(3).asInstanceOf[SchedulingStrategyTypeT])
}

object HWFunTypes {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type, syncType: CommunicationTypeT = SyncCommunication, schedulingStrategy: SchedulingStrategyTypeT = DefaultScheduling()): HWFunType = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      HWFunType(inTypes.head, outType, syncType, schedulingStrategy)
    } else {
      HWFunTypes(inTypes.tail, HWFunType(inTypes.head, outType, syncType, schedulingStrategy), syncType, schedulingStrategy)
    }
  }
}

final case class HWFunTypeVar(inType: Type = AnyTypeVar(), outType: Type = AnyTypeVar(), comType: CommunicationTypeT = CommunicationTypeVar(), schedulingStrategy: SchedulingStrategyTypeT = SchedulingStrategyTypeVar(), tvFixedId: Option[Long] = None) extends HWFunTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): HWFunTypeVar =
    HWFunTypeVar(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type], newChildren(2).asInstanceOf[CommunicationTypeT], newChildren(3).asInstanceOf[SchedulingStrategyTypeT], tvFixedId)

  override def upperBound: Type = HWFunType(inType, outType, comType, schedulingStrategy)
}

object HWFunTypeVars {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type, syncType: CommunicationTypeT = CommunicationTypeVar(), schedulingStrategy: SchedulingStrategyTypeT = SchedulingStrategyTypeVar()): HWFunTypeVar = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      HWFunTypeVar(inTypes.head, outType, syncType, schedulingStrategy)
    } else {
      HWFunTypeVars(inTypes.tail, HWFunTypeVar(inTypes.head, outType, syncType, schedulingStrategy), syncType, schedulingStrategy)
    }
  }
}
