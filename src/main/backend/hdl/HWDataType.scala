package backend.hdl

import core._
import core.util.Log
import lift.arithmetic.ArithExpr

import scala.annotation.tailrec
import scala.collection.immutable.SortedMap

/**
  * This type is the root of all types in the hw package
  */

case object HWDataTypeKind extends Kind

sealed trait HWDataTypeT extends BuiltinDataTypeT {
  override def toShortString: String = super.toShortString.split('.').last

  val bitWidth: ArithTypeT

  override def kind: Kind = HWDataTypeKind

  override def superType: Type = BuiltinDataType()

  override def children: Seq[Type] = Seq(bitWidth)
}

final case class HWDataType(bitWidth: ArithTypeT) extends HWDataTypeT {
  override def build(newChildren: Seq[ShirIR]): HWDataTypeT = HWDataType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class HWDataTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends HWDataTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = HWDataTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = HWDataType(bitWidth)
}

/**
  * determine the location of a array data type (in block ram, fpga ram, host ram ...)
  */

case object MemoryLocationTypeKind extends Kind

sealed trait MemoryLocationTypeT extends MetaTypeT {
  val ramId: IdTypeT

  override def kind: Kind = MemoryLocationTypeKind

  override def children: Seq[Type] = Seq(ramId)
}

final case class MemoryLocationType(ramId: IdTypeT) extends MemoryLocationTypeT {
  override def build(newChildren: Seq[ShirIR]): Type =
    MemoryLocationType(newChildren.head.asInstanceOf[IdTypeT])
}

final case class MemoryLocationTypeVar(ramId: IdTypeT = IdTypeVar(), tvFixedId: Option[Long] = None) extends MemoryLocationTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT =
    MemoryLocationTypeVar(newChildren.head.asInstanceOf[IdTypeT], tvFixedId)

  override def upperBound: Type = MemoryLocationType(ramId)
}

case object BlockRamTypeKind extends Kind

sealed trait BlockRamTypeT extends MemoryLocationTypeT {
  override def kind: Kind = BlockRamTypeKind

  override def superType: Type = MemoryLocationType(ramId)
}

final case class BlockRamType(ramId: IdTypeT = IdType()) extends BlockRamTypeT {
  override def build(newChildren: Seq[ShirIR]): Type =
    BlockRamType(newChildren.head.asInstanceOf[IdTypeT])
}

final case class BlockRamTypeVar(ramId: IdTypeT = IdTypeVar(), tvFixedId: Option[Long] = None) extends BlockRamTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT =
    BlockRamTypeVar(newChildren.head.asInstanceOf[IdTypeT], tvFixedId)

  override def upperBound: MetaTypeT = BlockRamType(ramId)
}

case object BankedBlockRamTypeKind extends Kind

sealed trait BankedBlockRamTypeT extends MemoryLocationTypeT {
  val numBanks: ArithTypeT
  override def kind: Kind = BankedBlockRamTypeKind

  override def superType: Type = MemoryLocationType(ramId)
}

final case class BankedBlockRamType(numBanks: ArithTypeT, ramId: IdTypeT = IdType()) extends BankedBlockRamTypeT {
  override def build(newChildren: Seq[ShirIR]): Type =
    BankedBlockRamType(numBanks, newChildren.head.asInstanceOf[IdTypeT])
}

final case class BankedBlockRamTypeVar(numBanks: ArithTypeT, ramId: IdTypeT = IdTypeVar(),  tvFixedId: Option[Long] = None) extends BlockRamTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT =
    BankedBlockRamTypeVar(numBanks, newChildren.head.asInstanceOf[IdTypeT], tvFixedId)

  override def upperBound: MetaTypeT = BankedBlockRamType(numBanks, ramId)
}

case object OnBoardRamTypeKind extends Kind

sealed trait OnBoardRamTypeT extends MemoryLocationTypeT {
  override def kind: Kind = OnBoardRamTypeKind

  override def superType: Type = MemoryLocationType(ramId)
}

/**
  * There is only *one (apparent)* On Board ram -> defined as a singleton object with one ram id.
  */
object OnBoardRamType extends OnBoardRamTypeT {
  final override val ramId: IdTypeT = IdType()
  final override def build(newChildren: Seq[ShirIR]): Type = this
}

case object HostRamTypeKind extends Kind

sealed trait HostRamTypeT extends MemoryLocationTypeT {
  override def kind: Kind = HostRamTypeKind

  override def superType: Type = MemoryLocationType(ramId)
}

/**
  * There is only *one* host ram -> defined as a singleton object with one ram id.
  */
object HostRamType extends HostRamTypeT {
  final override val ramId: IdTypeT = IdType()
  final override def build(newChildren: Seq[ShirIR]): Type = this
}

/**
  * specify the kind of scheduling when sharing a resource
  */

case object SchedulingStrategyTypeKind extends Kind

sealed trait SchedulingStrategyTypeT extends MetaTypeT {
  override def kind: Kind = SchedulingStrategyTypeKind
}

final case class SchedulingStrategyType() extends SchedulingStrategyTypeT {
  override def build(newChildren: Seq[ShirIR]): Type = this
}

final case class SchedulingStrategyTypeVar(tvFixedId: Option[Long] = None) extends SchedulingStrategyTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = SchedulingStrategyTypeVar(tvFixedId)

  override def upperBound: Type = SchedulingStrategyType()
}

case object RoundRobinSchedulingTypeKind extends Kind

object RoundRobinSchedulingType extends SchedulingStrategyTypeT {
  override def kind: Kind = RoundRobinSchedulingTypeKind

  override def superType: Type = SchedulingStrategyType()

  override def build(newChildren: Seq[ShirIR]): Type = this
}

case object FirstComeFirstServedSchedulingTypeKind extends Kind

object FirstComeFirstServedSchedulingType extends SchedulingStrategyTypeT {
  override def kind: Kind = FirstComeFirstServedSchedulingTypeKind

  override def superType: Type = SchedulingStrategyType()

  override def build(newChildren: Seq[ShirIR]): Type = this
}

case object DefaultScheduling {
  def apply(): SchedulingStrategyTypeT = RoundRobinSchedulingType
}

/**
  * abstract super type for everything but stream like types
  */

case object NonArrayTypeKind extends Kind

sealed trait NonArrayTypeT extends HWDataTypeT {
  override def kind: Kind = NonArrayTypeKind

  override def superType: Type = HWDataType(bitWidth)

  override def children: Seq[Type] = Seq(bitWidth)
}

final case class NonArrayType(bitWidth: ArithTypeT) extends NonArrayTypeT {
  override def build(newChildren: Seq[ShirIR]): NonArrayTypeT = NonArrayType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class NonArrayTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends NonArrayTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = NonArrayTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: HWDataTypeT = NonArrayType(bitWidth)
}

/**
  * parallel accessible collection of elements with the same type
  */
case object VectorTypeKind extends Kind

sealed trait VectorTypeT extends BasicDataTypeT {
  val et: BasicDataTypeT
  val len: ArithTypeT

  final override val bitWidth: ArithTypeT = len.ae * et.bitWidth.ae

  override def kind: Kind = VectorTypeKind

  override def superType: Type = BasicDataType(bitWidth)

  override def children: Seq[Type] = Seq(et, len)

  final def dimensions: Seq[ArithTypeT] = et match {
    case st: VectorTypeT => st.dimensions :+ len
    case tt: NamedTupleTypeT => tt.dimensions :+ len
    case _ => Seq(len)
  }

  @tailrec
  final def leafType: BasicDataTypeT = et match {
    case st: VectorTypeT => st.leafType
    case t: ScalarTypeT => t
    case _ => throw new RuntimeException("could not extract leaf type from stream")
  }
}

final case class VectorType(et: BasicDataTypeT, len: ArithTypeT) extends VectorTypeT {
  if (!ArithExpr.isSmaller(0, len.ae).getOrElse(true)) throw MalformedTypeException("invalid length for " + this.getClass)

  override def build(newChildren: Seq[ShirIR]): VectorTypeT = VectorType(newChildren.head.asInstanceOf[BasicDataTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}

final case class VectorTypeVar(et: BasicDataTypeT = BasicDataTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends VectorTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = VectorTypeVar(newChildren.head.asInstanceOf[BasicDataTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = VectorType(et, len)
}

/**
  * random accessible collection (using indices) of elements with the same type
  */
case object ArrayTypeKind extends Kind

sealed trait RamArrayTypeT extends HWDataTypeT {
  val et: BasicDataTypeT
  val len: ArithTypeT
  val memoryLocation: MemoryLocationTypeT

  final override val bitWidth: ArithTypeT = len.ae * et.bitWidth.ae

  override def kind: Kind = ArrayTypeKind

  override def superType: Type = HWDataType(bitWidth)

  override def children: Seq[Type] = Seq(et, len, memoryLocation)
}

final case class RamArrayType(et: BasicDataTypeT, len: ArithTypeT, memoryLocation: MemoryLocationTypeT) extends RamArrayTypeT {
  if (!ArithExpr.isSmaller(0, len.ae).getOrElse(true)) throw MalformedTypeException("invalid length for " + this.getClass)

  override def build(newChildren: Seq[ShirIR]): RamArrayTypeT = RamArrayType(newChildren.head.asInstanceOf[BasicDataTypeT], newChildren(1).asInstanceOf[ArithTypeT], newChildren(2).asInstanceOf[MemoryLocationTypeT])
}

final case class RamArrayTypeVar(et: BasicDataTypeT = BasicDataTypeVar(), len: ArithTypeT = ArithTypeVar(), memoryLocation: MemoryLocationTypeT = MemoryLocationTypeVar(), tvFixedId: Option[Long] = None) extends RamArrayTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = RamArrayTypeVar(newChildren.head.asInstanceOf[BasicDataTypeT], newChildren(1).asInstanceOf[ArithTypeT], newChildren(2).asInstanceOf[MemoryLocationTypeT], tvFixedId)

  override def upperBound: Type = RamArrayType(et, len, memoryLocation)
}


case object BaseAddrTypeKind extends Kind

sealed trait BaseAddrTypeT extends IntTypeT {
  val memoryLocation: MemoryLocationTypeT

  override def kind: Kind = BaseAddrTypeKind

  override def superType: Type = IntType(bitWidth)

  override def children: Seq[Type] = Seq(bitWidth, memoryLocation)
}

final case class BaseAddrType(bitWidth: ArithTypeT, memoryLocation: MemoryLocationTypeT) extends BaseAddrTypeT {
  override def build(newChildren: Seq[ShirIR]): BaseAddrTypeT = BaseAddrType(
    newChildren.head.asInstanceOf[ArithTypeT],
    newChildren(1).asInstanceOf[MemoryLocationTypeT]
  )
}

final case class BaseAddrInitType(bitWidth: ArithTypeT, identifier: TextTypeT, description: TextTypeT, memoryLocation: MemoryLocationTypeT, origElementType: BasicDataTypeT, dimensions: Seq[ArithTypeT]) extends BaseAddrTypeT {
  override def build(newChildren: Seq[ShirIR]): BaseAddrInitType = BaseAddrInitType(
    newChildren.head.asInstanceOf[ArithTypeT],
    identifier,
    description,
    newChildren(1).asInstanceOf[MemoryLocationTypeT],
    origElementType,
    dimensions
  )
  override def toShortString: String = BaseAddrType(bitWidth, memoryLocation).toShortString
}

/**
  * series of data with the same type, transmitted one after the other, unordered!
  */
case object UnorderedStreamTypeKind extends Kind

sealed trait UnorderedStreamTypeT extends NonArrayTypeT {
  val et: NonArrayTypeT
  val len: ArithTypeT

  final override val bitWidth: ArithTypeT = len.ae * et.bitWidth.ae

  override def kind: Kind = UnorderedStreamTypeKind

  override def superType: Type = NonArrayType(bitWidth)

  override def children: Seq[Type] = Seq(et, len)

  def indexType: IntTypeT = IntType(lift.arithmetic.ceil(lift.arithmetic.Log(2, len.ae)))

  /**
    * @return sequence of arithmetic values with the innermost dimension first and the outermost dimension last
    */
  final def dimensions: Seq[ArithTypeT] = et match {
    case st: UnorderedStreamTypeT => st.dimensions :+ len
    case tt: NamedTupleTypeT => tt.dimensions :+ len
    case _ => Seq(len)
  }

  @tailrec
  final def leafType: BasicDataTypeT = et match {
    case st: UnorderedStreamTypeT => st.leafType
    case t: BasicDataTypeT => t
    case _ => throw new RuntimeException("could not extract leaf type from stream")
  }
}

final case class UnorderedStreamType(et: NonArrayTypeT, len: ArithTypeT) extends UnorderedStreamTypeT {
  if (et.isInstanceOf[LogicTypeT]) Log.warn(this, "creating a stream of logic bits, this may be a performance bottleneck")
  override def build(newChildren: Seq[ShirIR]): UnorderedStreamTypeT = UnorderedStreamType(newChildren.head.asInstanceOf[NonArrayTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}

final case class UnorderedStreamTypeVar(et: NonArrayTypeT = NonArrayTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends UnorderedStreamTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = UnorderedStreamTypeVar(newChildren.head.asInstanceOf[NonArrayTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = UnorderedStreamType(et, len)
}

/**
  * Stream with unknown length, upperbounded!
  */
case object UpperBoundedStreamTypeKind extends Kind

sealed trait UpperBoundedStreamTypeT extends UnorderedStreamTypeT {
  override def kind: Kind = UpperBoundedStreamTypeKind

  override def superType: Type = UnorderedStreamType(et, len)
}

final case class UpperBoundedStreamType(et: NonArrayTypeT, len: ArithTypeT) extends UpperBoundedStreamTypeT {
  if (et.isInstanceOf[LogicTypeT]) Log.warn(this, "creating a stream of logic bits, this may be a performance bottleneck")
  override def build(newChildren: Seq[ShirIR]): UpperBoundedStreamTypeT = UpperBoundedStreamType(newChildren.head.asInstanceOf[NonArrayTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}
object UpperBoundedStreamType {
  def apply(dimensions: Seq[ArithTypeT], leafType: NonArrayTypeT): UpperBoundedStreamType = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => UpperBoundedStreamType(leafType, dimensions.last)
    case _ => UpperBoundedStreamType(apply(dimensions.init, leafType), dimensions.last)
  }
}

final case class UpperBoundedStreamTypeVar(et: NonArrayTypeT = NonArrayTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends UpperBoundedStreamTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = UpperBoundedStreamTypeVar(newChildren.head.asInstanceOf[NonArrayTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = UpperBoundedStreamType(et, len)
}
object UpperBoundedStreamTypeVar {
  def apply(dimensions: Seq[ArithTypeT], leafType: NonArrayTypeT): UpperBoundedStreamTypeVar = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => UpperBoundedStreamTypeVar(leafType, dimensions.last)
    case _ => UpperBoundedStreamTypeVar(apply(dimensions.init, leafType), dimensions.last)
  }
}

/**
  * series of data with the same type, transmitted one after the other, ordered!
  */
case object OrderedStreamTypeKind extends Kind

sealed trait OrderedStreamTypeT extends UpperBoundedStreamTypeT {
  override def kind: Kind = OrderedStreamTypeKind

  override def superType: Type = UpperBoundedStreamType(et, len)
}

final case class OrderedStreamType(et: NonArrayTypeT, len: ArithTypeT) extends OrderedStreamTypeT {
  if (et.isInstanceOf[LogicTypeT]) Log.warn(this, "creating a stream of logic bits, this may be a performance bottleneck")
  override def build(newChildren: Seq[ShirIR]): OrderedStreamTypeT = OrderedStreamType(newChildren.head.asInstanceOf[NonArrayTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}
object OrderedStreamType {
  def apply(dimensions: Seq[ArithTypeT], leafType: NonArrayTypeT): OrderedStreamType = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => OrderedStreamType(leafType, dimensions.last)
    case _ => OrderedStreamType(apply(dimensions.init, leafType), dimensions.last)
  }
}

final case class OrderedStreamTypeVar(et: NonArrayTypeT = NonArrayTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends OrderedStreamTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = OrderedStreamTypeVar(newChildren.head.asInstanceOf[NonArrayTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = OrderedStreamType(et, len)
}
object OrderedStreamTypeVar {
  def apply(dimensions: Seq[ArithTypeT], leafType: NonArrayTypeT): OrderedStreamTypeVar = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => OrderedStreamTypeVar(leafType, dimensions.last)
    case _ => OrderedStreamTypeVar(apply(dimensions.init, leafType), dimensions.last)
  }
}

/** Stream stuff ends*/

case object BasicDataTypeKind extends Kind

sealed trait BasicDataTypeT extends NonArrayTypeT {
  override def kind: Kind = BasicDataTypeKind

  override def superType: Type = NonArrayType(bitWidth)
}

final case class BasicDataType(bitWidth: ArithTypeT) extends BasicDataTypeT {
  override def build(newChildren: Seq[ShirIR]): BasicDataTypeT = BasicDataType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class BasicDataTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends BasicDataTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = BasicDataTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: HWDataTypeT = BasicDataType(bitWidth)
}

/**
  * parallel accessible composition of elements with different types
  */
case object NamedTupleKind extends Kind

sealed trait NamedTupleTypeT extends BasicDataTypeT with ComposedValueTypeT {
  // think in terms of OCaml records, which means:
  // *  each field has a name and type
  // *  the names ARE significant, but ordering is not
  //
  // that is why we use a *Sorted*Map here to enforce some canonical ordering.
  val namedTypes: SortedMap[String, HWDataTypeT]
  assert(namedTypes.nonEmpty)

  override val bitWidth: ArithTypeT = namedTypes.values.foldRight[ArithExpr](0)((t, a) => t.bitWidth.ae + a)

  override def kind: Kind = NamedTupleKind

  override def superType: Type = BasicDataType(bitWidth)

  override def children: Seq[Type] = {
    // recall that names matter, so we need to squeeze in their names to make
    // sure type checking takes that into account.
    //
    // implementing it this way means build and buildTV needs to account for
    // this extra name value.
    namedTypes.flatMap(p => Seq(TextType(p._1), p._2)).toSeq
  }

  final def dimensions: Seq[ArithTypeT] = {
    namedTypes.values.map{
      case st: UnorderedStreamTypeT => st.dimensions
      case tt: NamedTupleTypeT => tt.dimensions
      case _ => Seq()
    }.maxBy(_.length) // TODO temporary bugfix only. this is not correct. always find maximum length per dimension!
  }
}

final case class NamedTupleType(namedTypes: SortedMap[String, HWDataTypeT]) extends NamedTupleTypeT {
  override def build(newChildren: Seq[ShirIR]): NamedTupleTypeT = {
    val m = newChildren
      .grouped(2)
      .map(p => (p.head.asInstanceOf[TextType].s, p.last.asInstanceOf[HWDataTypeT]))
      .foldLeft(SortedMap[String, HWDataTypeT]())(_ + _)
    assert(m.keySet == namedTypes.keySet, "NamedTupleType field names have changed")
    NamedTupleType(m)
  }
}

final case class NamedTupleTypeVar(namedTypes: SortedMap[String, HWDataTypeT], tvFixedId: Option[Long] = None) extends NamedTupleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = {
    val m = newChildren
      .grouped(2)
      .map(p => (p.head.asInstanceOf[TextType].s, p.last.asInstanceOf[HWDataTypeT]))
      .foldLeft(SortedMap[String, HWDataTypeT]())(_ + _)
    assert(m.keySet == namedTypes.keySet, "NamedTupleTypeVar field names have changed")
    NamedTupleTypeVar(m, tvFixedId)
  }

  override def upperBound: Type = NamedTupleType(namedTypes)
}

object TupleType {
  final val TUPLE_PREFIX: String = "t"
  def apply(types: HWDataTypeT*): NamedTupleType =
    NamedTupleType(types
      .zipWithIndex
      .map(p => (TupleType.TUPLE_PREFIX + p._2, p._1))
      .foldLeft(SortedMap[String, HWDataTypeT]())(_ + _))

  def unapply(namedTupleTypeT: NamedTupleTypeT): Option[Seq[HWDataTypeT]] = {
    val isTuple = namedTupleTypeT.namedTypes
      .keys
      .zipWithIndex
      .forall(p => p._1 == TupleType.TUPLE_PREFIX + p._2)
    if (isTuple) Some(namedTupleTypeT.namedTypes.values.toSeq) else None
  }
}
object TupleTypeVar {
  def apply(types: HWDataTypeT*): NamedTupleTypeVar =
    NamedTupleTypeVar(types
      .zipWithIndex
      .map(p => (TupleType.TUPLE_PREFIX + p._2, p._1))
      .foldLeft(SortedMap[String, HWDataTypeT]())(_ + _))
}

//TODO: Change made here
object RequestType {
  def apply(dataType: BasicDataTypeT, reqIdType: BasicDataTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "req_id" -> reqIdType))
  def apply(reqIdType: BasicDataTypeT): NamedTupleType =
    NamedTupleType(SortedMap("req_id" -> reqIdType)) // XXX: This doesn't look right
  def unapply(t: NamedTupleTypeT): Option[(BasicDataTypeT, BasicDataTypeT)] =
    (t.namedTypes.get("data"), t.namedTypes.get("req_id")) match {
      case (Some(dataType: BasicDataTypeT), Some(reqIdType: BasicDataTypeT)) => Some((dataType, reqIdType))
      case _ => None
    }
}

object RequestTypeVar {
  def apply(dataType: BasicDataTypeT, reqIdType: BasicDataTypeT): NamedTupleTypeVar =
    NamedTupleTypeVar(SortedMap("data" -> dataType, "req_id" -> reqIdType))
}

object AddressedDataType {
  def apply(dataType: BasicDataTypeT, addrType: BasicDataTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "addr" -> addrType))

  def unapply(t: NamedTupleTypeT): Option[(BasicDataTypeT, BasicDataTypeT)] =
    (t.namedTypes.get("data"), t.namedTypes.get("addr")) match {
      case (Some(dataType: BasicDataTypeT), Some(addrType: BasicDataTypeT)) => Some((dataType, addrType))
      case _ => None
    }
}

object AddressedDataTypeVar {
  def apply(dataType: BasicDataTypeT, addrType: BasicDataTypeT): NamedTupleTypeVar =
    NamedTupleTypeVar(SortedMap("data" -> dataType, "addr" -> addrType))
}

object IndexedDataType {
  final val DATA_KEY: String = "data"
  final val INDEX_KEY: String = "idx"
  def apply(dataType: BasicDataTypeT, indexType: BasicDataTypeT): NamedTupleType =
    NamedTupleType(SortedMap(DATA_KEY -> dataType, INDEX_KEY -> indexType))
}

object SyncRamInputType {
  def apply(dataType: BasicDataTypeT, addrType: BasicDataTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "addr" -> addrType, "we" -> LogicType()))
  def apply(dataType: BasicDataTypeT, len: ArithTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "addr" -> IntType(len.ae.evalInt.toBinaryString.length), "we" -> LogicType()))
  def unapply(t: NamedTupleTypeT): Option[(BasicDataTypeT, BasicDataTypeT)] =
    (t.namedTypes.get("data"), t.namedTypes.get("addr")) match {
      case (Some(dataType: BasicDataTypeT), Some(addrType: BasicDataTypeT)) => Some((dataType, addrType))
      case _ => None
    }
}

object BankedBlockRamInputType {
  def apply(dataType: BasicDataTypeT, addrType: BasicDataTypeT, numBanks: ArithTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "addr" -> addrType, "we" -> VectorType(LogicType(), numBanks.ae.evalInt)))
  def apply(dataType: BasicDataTypeT, addrType: BasicDataTypeT, weType: BasicDataTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "addr" -> addrType, "we" -> weType))
  def apply(dataType: BasicDataTypeT, len: ArithTypeT, numBanks: ArithTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "addr" -> VectorType(IntType(len.ae.evalInt.toBinaryString.length), numBanks.ae.evalInt), "we" ->  VectorType(LogicType(), numBanks.ae.evalInt)))
}

object BankedBlockRamInputTypeVar {
  def apply(dataType: BasicDataTypeT, addrType: BasicDataTypeT, weType: BasicDataTypeT): NamedTupleTypeVar =
    NamedTupleTypeVar(SortedMap("data" -> dataType, "addr" -> addrType, "we" -> weType))
}

// TODO Change made here, should req_id be a VectorType or IntType?
object AsyncRamInputType {
  def apply(dataType: BasicDataTypeT, addrType: BasicDataTypeT, reqIdType: BasicDataTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "req_id" -> reqIdType, "addr" -> addrType, "we" -> LogicType()))
  def apply(dataType: BasicDataTypeT, len: ArithTypeT, reqIdLen: ArithTypeT): NamedTupleType =
    NamedTupleType(SortedMap("data" -> dataType, "req_id" -> VectorType(LogicType(), reqIdLen), "addr" -> IntType(len.ae.evalInt.toBinaryString.length), "we" -> LogicType()))
  def unapply(t: NamedTupleTypeT): Option[(BasicDataTypeT, BasicDataTypeT, BasicDataTypeT)] =
    (t.namedTypes.get("data"), t.namedTypes.get("addr"), t.namedTypes.get("req_id")) match {
      case (Some(dataType: BasicDataTypeT), Some(addrType: BasicDataTypeT), Some(reqIdType: BasicDataTypeT)) => Some((dataType, addrType, reqIdType))
      case _ => None
    }
}

/**
  * abstract super type for all simple scalar types
  */

case object ScalarTypeKind extends Kind

sealed trait ScalarTypeT extends BasicDataTypeT {
  override def kind: Kind = ScalarTypeKind

  override def superType: Type = BasicDataType(bitWidth)
}

final case class ScalarType(bitWidth: ArithTypeT) extends ScalarTypeT {
  override def build(newChildren: Seq[ShirIR]): ScalarTypeT = ScalarType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class ScalarTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends ScalarTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = ScalarTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = ScalarType(bitWidth)
}

case object LastVectorTypeKind extends Kind

sealed trait LastVectorTypeT extends ScalarTypeT {
  val nestingLevels: ArithTypeT
  override val bitWidth: ArithTypeT = nestingLevels.ae

  override def kind: Kind = LastVectorTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq(nestingLevels)
}
final case class LastVectorType(nestingLevels: ArithTypeT) extends LastVectorTypeT {
  override def build(newChildren: Seq[ShirIR]): LastVectorTypeT = LastVectorType(newChildren.head.asInstanceOf[ArithTypeT])
}
object LastVectorType {
  def apply(hwType: HWDataTypeT): LastVectorTypeT = hwType match {
    case st: UnorderedStreamTypeT => LastVectorType(st.dimensions.length)
    case TupleType(types) => types.map(t => apply(t)).maxBy(_.nestingLevels.ae.evalInt)
    case _ => LastVectorType(0)
  }
}

case object ReadyVectorTypeKind extends Kind

sealed trait ReadyVectorTypeT extends ScalarTypeT {
  val nestingLevels: ArithTypeT
  override val bitWidth: ArithTypeT = nestingLevels.ae + 1

  override def kind: Kind = ReadyVectorTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq(nestingLevels)
}
final case class ReadyVectorType(nestingLevels: ArithTypeT) extends ReadyVectorTypeT {
  override def build(newChildren: Seq[ShirIR]): ReadyVectorTypeT = ReadyVectorType(newChildren.head.asInstanceOf[ArithTypeT])
}
object ReadyVectorType {
  def apply(hwType: HWDataTypeT): ReadyVectorType = hwType match {
    case st: UnorderedStreamTypeT => ReadyVectorType(st.dimensions.length)
    case TupleType(types) => types.map(t => apply(t)).maxBy(_.nestingLevels.ae.evalInt)
    case _ => ReadyVectorType(0)
  }
}

case object UnitTypeKind extends Kind

sealed trait UnitTypeT extends ScalarTypeT {
  override val bitWidth: ArithTypeT = 0

  override def kind: Kind = UnitTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq()
}
final case class UnitType() extends UnitTypeT {
  override def build(newChildren: Seq[ShirIR]): UnitTypeT = this
}

/**
  * logic type can be 0 or 1 (represents 1 bit)
  */
case object LogicTypeKind extends Kind

sealed trait LogicTypeT extends ScalarTypeT {
  override val bitWidth: ArithTypeT = 1

  override def kind: Kind = LogicTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq()
}

final case class LogicType() extends LogicTypeT {
  override def build(newChildren: Seq[ShirIR]): LogicTypeT = this
}

final case class LogicTypeVar(tvFixedId: Option[Long] = None) extends LogicTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = LogicTypeVar(tvFixedId)

  override def upperBound: Type = LogicType()
}

case object NaturalNumberTypeKind extends Kind

sealed trait NaturalNumberTypeT extends ScalarTypeT {
  override val bitWidth: ArithTypeT = 32 // TODO normally unspecified, VHDL synthesis optimises the width

  override def kind: Kind = NaturalNumberTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq()
}

final case class NaturalNumberType() extends NaturalNumberTypeT {
  override def build(newChildren: Seq[ShirIR]): NaturalNumberTypeT = NaturalNumberType()
}

final case class NaturalNumberTypeVar(tvFixedId: Option[Long] = None) extends NaturalNumberTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = NaturalNumberTypeVar(tvFixedId)

  override def upperBound: Type = NaturalNumberType()
}

/**
 * Fake type for generating VHDL natural range.
 */
case object NaturalRangeTypeKind extends Kind

sealed trait NaturalRangeTypeT extends ScalarTypeT {
  override val bitWidth: ArithTypeT = 32 // TODO normally unspecified, VHDL synthesis optimises the width

  val startVal: ArithTypeT
  val endVal: ArithTypeT

  override def kind: Kind = NaturalRangeTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq()
}

final case class NaturalRangeType(startVal: ArithTypeT, endVal: ArithTypeT) extends NaturalRangeTypeT {
  override def build(newChildren: Seq[ShirIR]): NaturalRangeTypeT = NaturalRangeType(newChildren.head.asInstanceOf[ArithTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}

final case class NaturalRangeTypeVar(startVal: ArithTypeT, endVal: ArithTypeT, tvFixedId: Option[Long] = None) extends NaturalRangeTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = NaturalRangeTypeVar(newChildren.head.asInstanceOf[ArithTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = NaturalRangeType(startVal, endVal)
}
// end

case object IntTypeKind extends Kind

sealed trait IntTypeT extends ScalarTypeT {
  override def kind: Kind = IntTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq(bitWidth)
}

final case class IntType(bitWidth: ArithTypeT) extends IntTypeT {
  override def build(newChildren: Seq[ShirIR]): IntTypeT = IntType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class IntTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends IntTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = IntTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = IntType(bitWidth)
}

case object SignedIntTypeKind extends Kind

sealed trait SignedIntTypeT extends IntTypeT {
  override def kind: Kind = SignedIntTypeKind

  override def superType: Type = IntType(bitWidth)

  override def children: Seq[Type] = Seq(bitWidth)
}

final case class SignedIntType(bitWidth: ArithTypeT) extends SignedIntTypeT {
  override def build(newChildren: Seq[ShirIR]): SignedIntTypeT = SignedIntType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class SignedIntTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends SignedIntTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = SignedIntTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = SignedIntType(bitWidth)
}

/**
  * abstract super type for all float types
  */

case object FloatTypeKind extends Kind

sealed trait FloatTypeT extends ScalarTypeT {
  override def kind: Kind = FloatTypeKind

  override def superType: Type = ScalarType(bitWidth)
}

final case class FloatType(bitWidth: ArithTypeT) extends FloatTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatTypeT = FloatType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class FloatTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends FloatTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = FloatTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = FloatType(bitWidth)
}

case object FloatHalfTypeKind extends Kind

sealed trait FloatHalfTypeT extends FloatTypeT {
  override val bitWidth: ArithTypeT = 16

  override def kind: Kind = FloatHalfTypeKind

  override def superType: Type = FloatType(bitWidth)
}

final case class FloatHalfType() extends FloatHalfTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatHalfTypeT = FloatHalfType()
}

final case class FloatHalfTypeVar(tvFixedId: Option[Long] = None) extends FloatHalfTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = FloatHalfTypeVar(tvFixedId)

  override def upperBound: Type = FloatHalfType()
}

case object FloatSingleTypeKind extends Kind

sealed trait FloatSingleTypeT extends FloatTypeT {
  override val bitWidth: ArithTypeT = 32

  override def kind: Kind = FloatDoubleTypeKind

  override def superType: Type = FloatType(bitWidth)
}

final case class FloatSingleType() extends FloatSingleTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatSingleTypeT = FloatSingleType()
}

final case class FloatSingleTypeVar(tvFixedId: Option[Long] = None) extends FloatSingleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = FloatSingleTypeVar(tvFixedId)

  override def upperBound: Type = FloatSingleType()
}

case object FloatDoubleTypeKind extends Kind

sealed trait FloatDoubleTypeT extends FloatTypeT {
  override val bitWidth: ArithTypeT = 64

  override def kind: Kind = FloatDoubleTypeKind

  override def superType: Type = FloatType(bitWidth)
}

final case class FloatDoubleType() extends FloatDoubleTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatDoubleTypeT = FloatDoubleType()
}

final case class FloatDoubleTypeVar(tvFixedId: Option[Long] = None) extends FloatDoubleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = FloatDoubleTypeVar(tvFixedId)

  override def upperBound: Type = FloatDoubleType()
}

case object FixedTypeKind extends Kind

sealed trait FixedTypeT extends ScalarTypeT {
  override def kind: Kind = FixedTypeKind

  override def superType: Type = ScalarType(bitWidth)

  override def children: Seq[Type] = Seq(bitWidth)
}

final case class FixedType(bitWidth: ArithTypeT) extends FixedTypeT {
  override def build(newChildren: Seq[ShirIR]): FixedTypeT = FixedType(newChildren.head.asInstanceOf[ArithTypeT])
}

final case class FixedTypeVar(bitWidth: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends FixedTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = FixedTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = FixedType(bitWidth)
}
