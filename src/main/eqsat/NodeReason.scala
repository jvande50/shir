package eqsat

import core.Expr

/**
 * The reason why a node is present in an e-graph.
 */
sealed trait NodeReason

sealed trait AddedByRewriteT extends NodeReason {
  val rule: Rewrite
  val original: EClassT
  val matchInfo: Match[EClassT]
}

/**
 * Indicates that no reason was provided for the node's addition. This marker should only be used when node reasons are
 * disabled.
 */
object NoReason extends NodeReason {
  override def toString: String = "no reason"
}

/**
 * Indicates that the nominal e-graph modification should not produce any real changes.
 */
object AlreadyThere extends NodeReason {
  override def toString: String = "already there"
}

/**
 * Indicates that a node was added to the e-graph directly.
 */
object DirectlyAdded extends NodeReason {
  override def toString: String = "directly"
}

/**
 * Indicates that a node was added to the e-graph by an analysis.
 */
object AddedByAnalysis extends NodeReason {
  override def toString: String = "by analysis"
}

/**
 * Indicates that a node was added to the e-graph as a result of rewrite rule application.
 */
final case class AddedByRewrite(rule: Rewrite, original: EClassT, matchInfo: Match[EClassT]) extends AddedByRewriteT {
  override def toString: String = s"by ${rule.name}"
}

/**
 * Indicates that a node was added to the e-graph as a result of beta reduction.
 * @param rule The rewrite rule that performed the beta reduction.
 * @param original A non-canonical version of the e-class that was rewritten.
 * @param matchInfo The e-graph match that led to the beta reduction.
 * @param functionCall The extracted expression for the beta reduced function call.
 */
final case class AddedByBetaReduction(rule: Rewrite,
                                      original: EClassT,
                                      matchInfo: Match[EClassT],
                                      functionCall: Expr) extends AddedByRewriteT {
  override def toString: String = s"by beta-reducing ${ExprToText(functionCall)}"
}
