package eqsat

import algo.AlgoFunType
import cGen.CLambda
import core.{AnyTypeVar, BuiltinExpr, BuiltinTypeFunction, Expr, Formatter, FunTypeT, FunctionCall, LambdaT, ParamDef, ParamUse, ShirIR, TextFormatter, TreeNodeKind, Type, TypeChecker, TypeCheckerException, TypeFunType, TypeFunctionCall, Value}

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer

/**
 * An expression that increments or decrements De Bruijn variables.
 */
trait DeBruijnShiftT extends Expr {
  /**
   * The amount by which De Bruijn indices are shifted. That is, the amount by which all unbound De Bruijn indices in
   * this expression are implicitly decremented relative to expressions outside of this shift expression.
   *
   * For instance, a shift amount of `1` means that `DeBruijnParamUse(1)` in this expression has the same meaning as
   * `DeBruijnParamUse(0)` outside of it.
   * @return The shift amount.
   */
  def shiftAmount: Int
}

/**
 * A De Bruijn quantifier, such as a lambda or a forall quantifier.
 */
trait DeBruijnQuantifierT extends LambdaT with DeBruijnShiftT {
  def shiftAmount: Int = 1

  /**
   * Beta-reduces (aka inlines) this De Bruijn quantifier with an argument.
   * @param arg The argument to substitute into this quantifier's body.
   * @param shiftEagerly Specifies if arguments should be shifted eagerly by rewriting their indices or lazily by
   *                     introducing the appropriate amount of shift expressions.
   * @return This quantifier's body with `arg` substituted into it.
   */
  def betaReduce(arg: Expr, shiftEagerly: Boolean = true): Expr = {
    def replaceParam(value: ShirIR, depth: Int): ShirIR = value match {
      case p: DeBruijnParamUse if p.index == depth && shiftEagerly => DeBruijnParamUse.incrementUnbound(arg, depth)
      case p: DeBruijnParamUse if p.index == depth && !shiftEagerly => DeBruijnShift.shift(arg, depth)
      case p: DeBruijnParamUse if p.index > depth => DeBruijnParamUse(p.index - 1, p.t)
      case p: DeBruijnParamUse if p.index < depth => p
      case lam: LambdaT =>
        val newDepth = depth + 1
        if (newDepth == 0) {
          // Looks like our parameter got overwritten by a nested lambda (with some help from a shift expression).
          lam
        } else {
          // Our parameter still lives. Recurse.
          lam.map(replaceParam(_, newDepth))
        }
      case shift: DeBruijnShiftT => shift.map(replaceParam(_, depth + shift.shiftAmount))
      case other => other.map(replaceParam(_, depth))
    }

    replaceParam(body, 0).asInstanceOf[Expr]
  }
}

/**
 * A lambda expression that uses De Bruijn indices to refer to its parameter.
 */
final class DeBruijnLambda private (private val wrappedLambda: LambdaT) extends LambdaT with DeBruijnQuantifierT {
  override val param: ParamDef = wrappedLambda.param
  override val body: Expr = wrappedLambda.body
  override val t: FunTypeT = wrappedLambda.t

  override def refersToParamByName: Boolean = false

  override def build(newChildren: Seq[ShirIR]): DeBruijnLambda = {
    new DeBruijnLambda(wrappedLambda.build(newChildren).asInstanceOf[LambdaT])
  }

  override def children: Seq[ShirIR] = Seq(param, body, t)

  override def equals(obj: Any): Boolean = obj match {
    case lambda: DeBruijnLambda => (this eq lambda) || (body == lambda.body && t == lambda.t)
    case _ => false
  }

  override def hashCode(): Int = Seq(body, t).hashCode()

  /**
   * Turns this De Bruijn lambda back into a lambda with named parameters.
   * @param convertLambda Converts the resulting lambda to another lambda. Useful when the output lambda must be of a
   *                      certain type.
   * @return A lambda with named parameters.
   */
  def toNamed(implicit convertLambda: LambdaT => LambdaT = identity[LambdaT]): LambdaT = {
    def replaceParam(value: ShirIR, to: ParamDef, depth: Int): ShirIR = value match {
      case p: DeBruijnParamUse if p.index == depth => ParamUse(to)
      case p: DeBruijnParamUse => p
      case lam: LambdaT => lam.map(replaceParam(_, to, depth + 1))
      case shift: DeBruijnShiftT => shift.map(replaceParam(_, to, depth + shift.shiftAmount))
      case other => other.map(replaceParam(_, to, depth))
    }

    convertLambda(wrappedLambda.map(replaceParam(_, param, 0)).asInstanceOf[LambdaT])
  }

  /**
   * Collects all uses of this lambda's parameter, assuming the normal De Bruijn numbering scheme.
   * @return A list of all parameter uses.
   */
  def paramUses: Seq[DeBruijnParamUse] = {
    val uses = ArrayBuffer[DeBruijnParamUse]()
    def visitUses(value: ShirIR, depth: Int): Unit = value match {
      case p: DeBruijnParamUse if p.index == depth => uses.append(p)
      case lam: LambdaT => lam.children.foreach(visitUses(_, depth + 1))
      case shift: DeBruijnShiftT => shift.children.foreach(visitUses(_, depth + shift.shiftAmount))
      case _ => value.children.foreach(visitUses(_, depth))
    }

    visitUses(body, 0)
    uses
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        Some(formatter.makeAtomic(s"λ${formatter.formatNonAtomic(param.t)}. ${formatter.formatAtomic(body)}"))
      case _ => None
    }
  }
}

object DeBruijnLambda {
  def apply(paramType: Type, body: Expr)
           (implicit buildLambda: (ParamDef, Expr) => LambdaT = (p, d) => CLambda(p, d)): DeBruijnLambda =
    new DeBruijnLambda(buildLambda(ParamDef(paramType), body))

  /**
   * Creates a De Bruijn lambda from a lambda with named indices.
   * @param lambda A lambda expression to turn into a De Bruijn lambda.
   * @return A De Bruijn lambda equivalent to `lambda`.
   */
  def from(lambda: LambdaT): DeBruijnLambda = lambda match {
    case db: DeBruijnLambda => db
    case _ =>
      def replaceParam(value: ShirIR, from: ParamDef, depth: Int): ShirIR = value match {
        case p: ParamUse if p.p.id == from.id => DeBruijnParamUse(depth, p.t)
        case p: ParamUse => p
        case lam: LambdaT => lam.map(replaceParam(_, from, depth + 1))
        case shift: DeBruijnShiftT => shift.map(replaceParam(_, from, depth + shift.shiftAmount))
        case other => other.map(replaceParam(_, from, depth))
      }

      new DeBruijnLambda(lambda.map(replaceParam(_, lambda.param, 0)).asInstanceOf[LambdaT])
  }

  /**
   * Translates all named lambdas in an expression to De Bruijn lambdas.
   * @param expr The expression to process.
   * @return A version of `expr` in which all lambdas are De Bruijn lambdas.
   */
  def fromRec(expr: ShirIR): ShirIR = expr.visitAndRebuild({
    case quant: DeBruijnQuantifierT => quant
    case lambda: LambdaT => from(lambda)
    case other => other
  })

  /**
   * Translates all De Bruijn lambdas in an expression to lambdas with named variables.
   * @param expr The expression to process.
   * @param convertLambda Converts the resulting lambdas to another lambda. Useful when the output lambdas must be of a
   *                      certain type.
   * @return A version of `expr` in which all lambdas have named variables.
   */
  def toNamedRec(expr: ShirIR)(implicit convertLambda: LambdaT => LambdaT = identity[LambdaT]): ShirIR = expr.visitAndRebuild({
    case lambda: DeBruijnLambda => lambda.toNamed(convertLambda)
    case other => other
  })

  /**
   * Gather a sequence of type constraints that associate De Bruijn parameter definitions with parameter uses.
   * @param expr The expression to find type constraints for.
   * @return A sequence of type constraints.
   */
  def gatherParamTypeConstraints(expr: Expr): Seq[(Type, Type)] = {
    def recurse(expr: Expr, paramTypes: List[Type]): Seq[(Type, Type)] = {
      expr match {
        case shift: DeBruijnShiftExpr =>
          paramTypes match {
            case _ :: tail => recurse(shift.innerIR, tail)
            case Nil => recurse(shift.innerIR, Nil)
          }
        case use: DeBruijnParamUse if use.index < paramTypes.length => Seq((paramTypes(use.index), use.t))
        case lambda: LambdaT => recurse(lambda.body, lambda.param.t :: paramTypes)
        case builtin: BuiltinExpr => builtin.args.flatMap(recurse(_, paramTypes))
        case _ => expr.children.collect({ case e: Expr => e }).flatMap(recurse(_, paramTypes))
      }
    }

    recurse(expr, Nil).distinct
  }

  /**
   * Checks if De Bruijn parameter uses within an expression have types that correspond to their definitions.
   * @param expr An expression to find parameter definitions and uses in.
   * @return `true` if De Bruijn parameter uses in `expr` have types conforming to the parameter definitions;
   *         otherwise, `false`.
   */
  def tryCheckParamTypes(expr: Expr): Boolean = {
    TypeChecker.solveConstraints(gatherParamTypeConstraints(expr)).isDefined
  }

  /**
   * Asserts that De Bruijn parameter uses within an expression have types correspond to their definitions. Throws a
   * `TypeCheckerException` otherwise.
   * @param expr An expression to find parameter definitions and uses in.
   */
  def checkParamTypes(expr: Expr): Unit = {
    if (!tryCheckParamTypes(expr)) {
      throw TypeCheckerException("Mismatch between De Bruijn parameter use types and parameter definition types");
    }
  }
}

private final case class DeBruijnParamUseNodeKind(index: Int) extends TreeNodeKind

final case class DeBruijnParamUse private (innerIR: Expr, types: Seq[Type], index: Int) extends EqualitySaturationExpr {
  assert(index >= 0)

  override def nodeKind: TreeNodeKind = DeBruijnParamUseNodeKind(index)

  override def build(newInnerIR: Expr, newTypes: Seq[Type]): DeBruijnParamUse =
    DeBruijnParamUse(index, newTypes.head)

  def incrementIndex(amount: Int): DeBruijnParamUse = DeBruijnParamUse(innerIR, types, index + amount)
  def decrementIndex(amount: Int): DeBruijnParamUse = incrementIndex(-amount)

//  override protected def argsBuild(args: Seq[Expr]): CGenExpr = this

  override def toString: String = s"DeBruijnParamUse($index)"

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter => Some(formatter.makeAtomic(s"p_$index: ${formatter.formatAtomic(t)}"))
      case _ => None
    }
  }
}

object DeBruijnParamUse {
  def apply(index: Int, valueType: Type): DeBruijnParamUse = DeBruijnParamUse(Value(valueType), Seq(valueType), index)

  def unapply(expr: DeBruijnParamUse): Some[(Int, Type)] = Some(expr.index, expr.t)

  /**
   * In an expression, renumbers unbound De Bruijn indices.
   * @param expr The expression in which unbound De Bruijn indices are to be renumbered.
   * @param renumber A function that takes a De Bruijn index and replaces it with another index.
   * @return A rewritten expression, if one can be constructed such that no index gets renumbered to a value less than
   *         zero; otherwise, `None`.
   */
  def tryRenumberUnbound(expr: Expr, renumber: Int => Int): Option[Expr] = {
    def tryMap(value: ShirIR, f: ShirIR => Option[ShirIR]): Option[ShirIR] = {
      val children = value.children.map(f)
      if (children.forall(_.isDefined)) {
        Some(value.build(children.map(_.get)))
      } else {
        None
      }
    }

    def impl(value: ShirIR, depth: Int): Option[ShirIR] = value match {
      case p: DeBruijnParamUse if p.index >= depth =>
        val newIndex = renumber(p.index - depth) + depth
        if (newIndex < 0 || newIndex < depth)
          None
        else
          Some(DeBruijnParamUse(renumber(p.index - depth) + depth, p.t))
      case p: DeBruijnParamUse if p.index < depth => Some(p)
      case lam: LambdaT => tryMap(lam, impl(_, depth + 1))
      case shift: DeBruijnShiftT => tryMap(shift, impl(_, depth + shift.shiftAmount))
      case other => tryMap(other, impl(_, depth))
    }

    impl(expr, 0).map(_.asInstanceOf[Expr])
  }

  /**
   * In an expression, increments all references to unbound De Bruijn indices.
   * @param expr The expression in which all unbound De Bruijn indices are to be incremented.
   * @param amount The amount by which indices should be incremented.
   * @return A rewritten expression, if one can be constructed; otherwise, `None`.
   */
  def tryIncrementUnbound(expr: Expr, amount: Int = 1): Option[Expr] =
    tryRenumberUnbound(expr, _ + amount)

  /**
   * In an expression, increments all references to unbound De Bruijn indices.
   * @param expr The expression in which all unbound De Bruijn indices are to be incremented.
   * @param amount The amount by which indices should be incremented.
   * @return A rewritten expression.
   */
  def incrementUnbound(expr: Expr, amount: Int = 1): Expr =
    tryIncrementUnbound(expr, amount).get

  /**
   * In an expression, decrements all references to unbound De Bruijn indices.
   * @param expr The expression in which all unbound De Bruijn indices are to be decremented.
   * @param amount The amount by which indices should be decremented.
   * @return A rewritten expression.
   */
  def tryDecrementUnbound(expr: Expr, amount: Int = 1): Option[Expr] =
    tryIncrementUnbound(expr, -amount)
}

/**
 * An expression that increments all De Bruijn indices in its inner IR by one.
 * @param innerIR The inner IR to increment.
 */
final case class DeBruijnShiftExpr private (innerIR: Expr) extends BuiltinExpr with DeBruijnShiftT {
  override def shiftAmount: Int = -1

  override def types: Seq[Type] = Seq()

  override def build(newInnerIR: Expr, newTypes: Seq[Type]): BuiltinExpr = DeBruijnShiftExpr(newInnerIR)

  /**
   * Tests if this De Bruijn shift is a no-op. That is, if the unshifted expression it contains is equal to the shifted
   * expression obtained by eliding the shift.
   * @return `true` if this De Bruijn shift has no effect; otherwise, `false`.
   */
  def isNop: Boolean = {
    def containsUnboundParams(expr: ShirIR, depth: Int): Boolean = expr match {
      case p: DeBruijnParamUse if p.index >= depth => true
      case lam: LambdaT => lam.children.exists(containsUnboundParams(_, depth + 1))
      case shift: DeBruijnShiftT => shift.children.exists(containsUnboundParams(_, depth + shift.shiftAmount))
      case other => other.children.exists(containsUnboundParams(_, depth))
    }

    !containsUnboundParams(args.head, 0)
  }

  /**
   * A version of the shifted expression that has the shift baked in.
   * @return An expression.
   */
  def elided: Expr = DeBruijnParamUse.incrementUnbound(args.head)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case DeBruijnShift(expr, _) => Some(formatter.makeAtomic(s"shift ${formatter.formatAtomic(expr)}"))
        }
      case _ => super.tryFormat(formatter)
    }
  }
}

object DeBruijnShift {
  def apply(expr: Expr): DeBruijnShiftExpr = {
    DeBruijnShiftExpr({
      val itv = AnyTypeVar()
      val outit = itv
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, AlgoFunType(itv, outit))), expr.t), expr)
    })
  }

  def unapply(expr: DeBruijnShiftExpr): Some[(Expr, Type)] = {
    Some((expr.args.head, expr.t))
  }

  /**
   * Wraps `expr` in `amount` shift expressions.
   * @param expr The expression to shift.
   * @param amount The amount by which the expression ought to be shifted.
   * @return A shifted expression.
   */
  @tailrec
  def shift(expr: Expr, amount: Int): Expr = amount match {
    case 0 => expr
    case n if n > 0 => shift(DeBruijnShift(expr), amount - 1)
    case _ => ???
  }

  /**
   * Elides all `DeBruijnShift` expressions in an IR tree.
   * @param expr The IR tree to transform.
   * @return An IR tree that no longer contains any `DeBruijnShift` expressions.
   */
  def elideRec(expr: ShirIR): ShirIR = expr.visitAndRebuild({
    case shift: DeBruijnShiftExpr => shift.elided
    case other => other
  })
}

/**
 * A forall quantifier that introduces a De Bruijn parameter.
 * @param param The parameter introduced by the quantifier.
 * @param body An expression that may refer to `param` via De Bruijn indices.
 * @param t The type of the quantifier, as a function type.
 */
final class DeBruijnForall private (val param: ParamDef, val body: Expr, val t: FunTypeT) extends DeBruijnQuantifierT {
  override def build(newChildren: Seq[ShirIR]): DeBruijnForall = {
    val param = newChildren.head.asInstanceOf[ParamDef]
    val body = newChildren(1).asInstanceOf[Expr]
    new DeBruijnForall(param, body, AlgoFunType(param.t, body.t))
  }

  override def equals(obj: Any): Boolean = obj match {
    case lambda: DeBruijnForall => (this eq lambda) || (body == lambda.body && t == lambda.t)
    case _ => false
  }

  override def hashCode(): Int = Seq("forall", body, t).hashCode()
}

object DeBruijnForall {
  def apply(paramType: Type, body: Expr): DeBruijnForall =
    new DeBruijnForall(ParamDef(paramType), body, AlgoFunType(paramType, body.t))

  def unapply(forall: DeBruijnForall): Some[(Type, Expr, FunTypeT)] =
    Some((forall.param.t, forall.body, forall.t))
}

/**
 * A reversible transform that turns lambdas with named variables into De Bruijn lambdas.
 */
object DeBruijnTransform extends BidirectionalTransform[Expr, Expr] {
  /**
   * Performs the forward transform.
   *
   * @param value A value to transform.
   * @return A transformed version of `value`.
   */
  override def forward(value: Expr): Expr = DeBruijnLambda.fromRec(value).asInstanceOf[Expr]

  /**
   * Performs the backward transform, which inverts the forward transform.
   *
   * @param value A transformed value.
   * @return A value whose transformed version is `value`.
   */
  override def backward(value: Expr): Expr = DeBruijnLambda.toNamedRec(DeBruijnShift.elideRec(value)).asInstanceOf[Expr]
}
