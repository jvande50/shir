package eqsat

import cGen.ExternFunctionCall
import core.util.Log
import core.{Expr, ExprVar, ParamDef, ShirIR, Type, TypeChecker, TypeVarT, UnknownType}

import scala.collection.mutable

trait PatternApplier extends Applier {
  val pattern: Expr
  val mayRename: ParamDef => Boolean = _ => true
  def shouldCheckForDuplicates: Boolean = false

  /**
   * Merges a pattern instance with a match in a PEG.
   * @param eGraph A PEG.
   * @param instance A pattern instance created from `matchInfo`.
   * @return A set of all e-classes that were _directly_ modified as a result of the application.
   */
  protected def mergePatternInstance[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                        (reason: AddedByRewrite,
                                                                         instance: EClass): Set[EClass]

  protected def instantiateExprVar[EClass <: EClassT](exprVar: ExprVar, matchInfo: Match[EClass]): Expr = {
    matchInfo.instantiate(exprVar)
  }

  protected def instantiatePattern[EClass <: EClassT](matchInfo: Match[EClass],
                                                      stopwatch: HierarchicalStopwatch): Expr = {
    // First instantiate the pattern with the match's info.
    val renamedParameters = mutable.HashMap[Long, ParamDef]()
    def visitor: ShirIR => ShirIR = {
      case w: ExprVar => instantiateExprVar(w, matchInfo)
      case p: ParamDef if matchInfo.hasMappingFor(p) => matchInfo.instantiate(p)
      case p: ParamDef if mayRename(p) =>
        renamedParameters.getOrElseUpdate(p.id, ParamDef(p.t.visitAndRebuild(visitor).asInstanceOf[Type]))
      case tv: TypeVarT if matchInfo.hasMappingFor(tv) => matchInfo.instantiate(tv)
      case ExternFunctionCall(name, args, intvs, outt) =>
        ExternFunctionCall(
          name,
          args,
          intvs.map(_.visitAndRebuild(visitor).asInstanceOf[TypeVarT]),
          outt.visitAndRebuild(visitor).asInstanceOf[Type])
      case other => other
    }

    val instantiatedPattern = stopwatch.child(
      "Instantiating pattern",
      pattern.visitAndRebuild(visitor).asInstanceOf[Expr])

    try
      stopwatch.child(
        "Type checking instantiated pattern",
        TypeChecker.check(instantiatedPattern))
    catch {
      case e: Exception => throw e
    }
  }

  /**
   * Rewrites an e-class based on an expression and type substitution map.
   * @param eGraph The graph that contains the e-class to rewrite.
   * @param matchInfo The match to rewrite.
   * @return A set of all e-classes that were _directly_ modified as a result of the application.
   */
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {
    val instantiatedPattern = instantiatePattern(matchInfo, stopwatch)

    if (shouldCheckForDuplicates) {
      // Next, we want to make sure the instantiated pattern is not already present in the graph. Canonicalization can
      // handle most use cases for this, but not parameter renaming.
      val searcher = stopwatch.child("Compiling instantiated pattern", Pattern(instantiatedPattern))
      val existing = stopwatch.child(
        "Searching for duplicates",
        (s: HierarchicalStopwatch) => searcher.searchEClass(eGraph)(matchInfo.where, s))

      if (existing.nonEmpty) {
        return Set()
      }
    }

    // It looks like we are adding new information to the graph.
    val original = rule.searcher.instantiateMatchIfUseful(eGraph, matchInfo)
    val reason = AddedByRewrite(rule, original, matchInfo)
    val newClass = stopwatch.child(
      "Adding instantiated pattern to PEG",
      eGraph.add(instantiatedPattern, reason))

    if (newClass.t != original.t) {
      Log.error(
        this,
        s"Application of rule ${rule.name} results in ill-typed expression. Expected: ${ExprToText(original.t)}; got: ${ExprToText(newClass.t)}")
      assert(false)
    }

    mergePatternInstance(eGraph)(reason, newClass)
  }
}

/**
 * Represents a pattern, encoded as an expression. Patterns can be matched in PEGs.
 *
 * @param pattern The expression that encodes the pattern. Unbound `ParamDef` objects represent expression variables
 *                in the pattern; unbound type variables represent type variables in the pattern.
 * @param mayRename Specifies which `ParamDef` instances in `pattern` may be renamed when searching for or applying
 *                  the pattern. Renaming is desirable for parameters that should not escape the scope of the pattern.
 *                  Conversely, renaming is best disabled if separate pattern applications should all refer to the same
 *                  parameters.
 */
final case class Pattern(pattern: Expr,
                         override val mayRename: ParamDef => Boolean = _ => true,
                         override val shouldCheckForDuplicates: Boolean = false) extends Searcher with PatternApplier {
  override def mergePatternInstance[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                       (reason: AddedByRewrite,
                                                                        instance: EClass): Set[EClass] = {

    // Be specific about the pattern we're merging with, but only if explanations are enabled.
    if (eGraph.merge(reason.original.asInstanceOf[EClass], instance, reason)) {
      Set(reason.matchInfo.where.asInstanceOf[EClass])
    } else {
      Set()
    }
  }

  /**
   * The pattern to match, as a stream of pattern matching instructions.
   */
  private val compiledPattern: List[Instruction] = {
    val compiler = new PatternCompiler()
    compiler.compile(pattern, Reg(0))
  }

  /**
   * Relaxes the pattern until it matches an e-class.
   * @param eGraph The e-graph that defines the e-class.
   * @param eClass An e-class to search.
   * @param stopwatch A stopwatch.
   * @tparam ENode The type of an e-node.
   * @tparam EClass The type of an e-class.
   * @return A relaxed version of this pattern that matches both this pattern and `eClass`.
   */
  def relax[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                               (eClass: EClass,
                                                stopwatch: HierarchicalStopwatch): Option[Expr] = {

    // Truncate the list of instructions and find the longest list of instructions that nets us at least one match.
    for (length <- compiledPattern.indices.reverse) {
      val matches = truncatedSearch(eGraph)(eClass, stopwatch, length + 1)
      if (matches.nonEmpty) {
        // Decompile the instructions into a pattern.
        val pattern = PatternDecompiler(compiledPattern.take(length + 1))
        return Some(pattern)
      }
    }

    None
  }

  /**
   * Tests if an e-class matches the pattern this searcher looks for; returns all resulting matches.
   * @param eGraph The e-graph that defines the e-class.
   * @param eClass An e-class to search.
   * @return All matches for this pattern and e-class combo.
   */
  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                               (eClass: EClass,
                                                                stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {

    truncatedSearch(eGraph)(eClass, stopwatch, compiledPattern.length)
  }

  private def truncatedSearch[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                 (eClass: EClass,
                                                                  stopwatch: HierarchicalStopwatch,
                                                                  instructionCount: Int): Seq[Match[EClass]] = {
    val machine = Machine[EClass](List(eClass), Nil, Nil, Nil, Map())
    stopwatch.child("Running machine", runMachine(eGraph)(machine, compiledPattern.take(instructionCount)))
      .groupBy(m => MachineResult(m.typeConstraints, m.exprSubstitutions, m.paramSubstitutions))
      .map(_._2.head)
      .map(m => {
        // Clean up type constraints before feeding them to the type checker.
        val nonTautologicalTypeConstraints = m.typeConstraints
          .filterNot(p => p._1 == p._2) // Remove tautologies.
          .filter({
            // Unknown types do not matter for the purpose of pattern matching and application, so filter those out.
            case (UnknownType, _) | (_, UnknownType) => false
            case _ => true
          })
          .distinct // No point in having the same type constraint more than once.
        (m, stopwatch.child(
          s"Solving type constraints (${nonTautologicalTypeConstraints.length} constraints)",
          TypeChecker.solveConstraints(nonTautologicalTypeConstraints)))
      })
      .collect({
        case (m, Some(solution)) => PatternMatch(eClass, m, solution.toMap)
      })
      .toSeq
  }

  override def instantiateMatch[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass],
                                                                    found: Match[EClass]): EClass = {
    // Recover the machine from the match metadata.
    val machine = found.chain.collectFirst({ case m: PatternMatch[EClass] => m }).get.machine

    // Walk through the list of instructions in reverse, decanonicalizing registers.
    val decanonicalizedRegisters = machine.registers.toBuffer
    val tapeLengths = computeTapeLengths(compiledPattern)
    val bindInstructions = compiledPattern
      .zip(tapeLengths)
      .collect({
        case (insn: Bind, len) => (insn, len)
      })
      .zip(machine.boundNodes.reverse)

    for (((Bind(_, i), len), node) <- bindInstructions.reverse) {
      val args = decanonicalizedRegisters.slice(len, len + node.args.length)
      decanonicalizedRegisters(i.index) = eGraph.add(node.instantiate(args), AlreadyThere)
    }

    decanonicalizedRegisters.head
  }

  /**
   * Computes the length of a machine's tape before and after each instruction application.
   * @param instructions The instructions to apply to a machine's tape.
   * @return The tape length over time. The first element of the sequence is the initial tape length, successive
   *         elements at index `i + 1` represent the tape after the `i`th instruction is applied.
   */
  private def computeTapeLengths(instructions: Seq[Instruction]): Seq[Int] = {
    instructions
      .scanLeft(1)({
        case (len, Bind(node, _)) => len + ExprHelpers.args(node).length
        case (len, _) => len
      })
  }

  private def runMachine[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                            (machine: Machine[EClass],
                                                             instructions: List[Instruction]): List[Machine[EClass]] = {

    var remainingInstructions = instructions
    while (remainingInstructions.nonEmpty) {
      // Pop the first instruction off the list.
      val head = remainingInstructions.head
      remainingInstructions = remainingInstructions.tail

      head match {
        case Bind(node, i) =>
          val nodeTypes = node.children.collect({ case t: Type => t })

          def bindParamDefs(matched: ENode): Option[Map[Long, ParamDef]] =
            ExprHelpers.params(matched.expr)
              .zip(ExprHelpers.params(node))
              .foldLeft[Option[Map[Long, ParamDef]]](Some(machine.paramSubstitutions))(
                (mapping, pair) => mapping match {
                  case Some(mapping) =>
                    mapping.get(pair._2.id) match {
                      case Some(existingParam) if existingParam.id == pair._1.id => Some(mapping)
                      case None if pair._1.id == pair._2.id || mayRename(pair._2) =>
                        Some(mapping + (pair._2.id -> pair._1))
                      case _ => None
                    }
                  case None => None
                })

          return machine.reg(i)
            .nodes
            .map(_.asInstanceOf[ENode])
            .filter(x => ExprHelpers.isSameOp(x.expr, node)) // Check that the op types match.
            .map(x => (x, bindParamDefs(x))) // Bind parameter definitions.
            .collect({ case (x, Some(mapping)) => (x, mapping) }) // Check that parameter definition binding went well.
            .map(pair => {
              val matched = pair._1
              val newMachine = Machine[EClass](
                machine.registers ++ matched.args.map(_.asInstanceOf[EClass]),
                matched :: machine.boundNodes,
                machine.exprSubstitutions,
                machine.typeConstraints
                  ++ nodeTypes.zip(matched.expr.children.collect({ case t: Type => t }))
                  ++ ExprHelpers.params(node).map(_.t).zip(
                  ExprHelpers.params(matched.expr).map(_.t)),
                pair._2)
              runMachine(graph)(newMachine, remainingInstructions)
            })
            .foldLeft(List[Machine[EClass]]())(_ ++ _)

        case BindExprVar(v, i) =>
          val eClass = machine.reg(i)
          val newMachine = Machine[EClass](
            machine.registers,
            machine.boundNodes,
            (v, eClass) :: machine.exprSubstitutions,
            (v.t, eClass.t) :: machine.typeConstraints,
            machine.paramSubstitutions)
          return runMachine(graph)(newMachine, remainingInstructions)

        case AssertEClass(eClass, i) =>
          if (graph.find(machine.reg(i)) != graph.find(eClass.asInstanceOf[EClass])) {
            return Nil
          }

        case Compare(i, j) =>
          if (graph.find(machine.reg(i)) != graph.find(machine.reg(j))) {
            return Nil
          }
      }
    }

    List(machine)
  }

  private type VarToReg = mutable.ListMap[Long, Reg]

  private final class PatternCompiler {
    private val v2r = new VarToReg()
    private var tapeLength: Int = 0

    def compile(expr: Expr, out: Reg): List[Instruction] = {
      expr match {
        // If we encountered a wildcard, then we want to either bind the wildcard to a concrete expression *or*
        // ensure that the wildcard has been bound consistently.
        case w: ExprVar =>
          v2r get w.id match {
            case None =>
              v2r.put(w.id, out)
              List(BindExprVar(w, out))
            case Some(i) => List(Compare(i, out))
          }

        // If the pattern contains an e-class, then we want to check that the current register is exactly equal to said
        // e-class.
        case eClass: EClassT => List(AssertEClass(eClass, out))

        // If we encountered any other type of expression, we want to unpack it and compile its children.
        case e =>
          val intro = Bind(e, out)
          val args = ExprHelpers.args(e)
          val tapeLengthAtIntro = tapeLength + 1
          tapeLength += args.length
          val argInstructions = args
            .zipWithIndex
            .map(pair => compile(pair._1, Reg(tapeLengthAtIntro + pair._2)))
            .foldLeft(List[Instruction]())(_ ++ _)

          intro :: argInstructions
      }
    }
  }

  private object PatternDecompiler {
    def apply(instructions: Seq[Instruction]): Expr = {
      val instance = new Instance()
      instance.decompile(instructions)
    }

    private final class Instance {
      private val tape = mutable.Map[Reg, Expr]()
      private var constrained = Set[Reg]()
      private var tapeLength = 0

      private def affects(instruction: Instruction): Seq[Reg] = {
        instruction match {
          case Compare(i, j) if tape.contains(i) => Seq(j)
          case Compare(i, j) if tape.contains(j) => Seq(i)
          case Compare(i, j) => Seq(i, j)
          case BindExprVar(_, i) => Seq(i)
          case AssertEClass(_, i) => Seq(i)
          case Bind(_, i) => Seq(i)
        }
      }

      private def tryDecompile(instruction: Instruction, tapePosition: Int): Boolean = {
        instruction match {
          case Compare(i, j) if tape.contains(i) =>
            tape(j) = tape(i)
            true
          case Compare(i, j) if tape.contains(j) =>
            tape(i) = tape(j)
            true
          case BindExprVar(v, i) =>
            tape(i) = v
            true
          case AssertEClass(eClass, i) =>
            tape(i) = eClass
            true

          case Bind(node, i) =>
            val argCount = ExprHelpers.args(node).length
            val args = ExprHelpers.args(node).zipWithIndex
              .map(t => {
                val (arg, i) = t
                val pos = tapePosition + i
                val reg = Reg(pos)
                if (pos < tapeLength && (tape.contains(reg) || constrained.contains(reg)))
                  tape.get(reg)
                else
                  Some(ExprVar(arg.t))
              })
              .collect({ case Some(result) => result })

            if (args.length == argCount) {
              tape(i) = ExprHelpers.buildFromArgs(node, args)
              true
            } else {
              false
            }

          case _ => false
        }
      }

      def decompile(instructions: Seq[Instruction]): Expr = {
        val tapeLengths = computeTapeLengths(instructions)
        tapeLength = tapeLengths.last

        // Decompiling instructions is a matter of turning the tape back into an expression. The tape is populated
        // exclusively by Bind instructions. Other instructions merely provide additional constraints. We build an
        // expression by first applying these additional constraints

        // Decompile the instructions one by one until there are none left. Back to front likely works better than front
        // to back.
        var remainingInstructions = instructions.zip(tapeLengths)
        while (remainingInstructions.nonEmpty) {
          // Figure out which registers are constrained by the remaining instructions.
          constrained = remainingInstructions.map(_._1).flatMap(affects).toSet

          val newRemainingInstructions = mutable.ArrayBuffer[(Instruction, Int)]()
          for ((insn, length) <- remainingInstructions.reverse) {
            if (!tryDecompile(insn, length)) {
              newRemainingInstructions.append((insn, length))
            }
          }
          remainingInstructions = newRemainingInstructions.reverse
        }

        tape(Reg(0))
      }
    }
  }
}

object Pattern {
  /**
   * Finds the smallest pair of subexpressions for which the expressions differ.
   * @param first The first expression to examine.
   * @param second The second expression to examine.
   * @return The smallest pair of subexpressions of `first` and `second` that aren't equal.
   */
  def findDivergence(first: Expr, second: Expr): Option[(Expr, Expr)] = {
    if (ExprHelpers.isSameOp(first, second)) {
      ExprHelpers.args(first).zip(ExprHelpers.args(second)).flatMap(t => findDivergence(t._1, t._2)).headOption
    } else {
      Some((first, second))
    }
  }
}

/**
 * Represents an expression that will be added to a PEG, without merging it with any existing class.
 *
 * @param pattern The expression that encodes the pattern.
 * @param mayRename Specifies which `ParamDef` instances in `pattern` may be renamed when applying the pattern.
 *                  Renaming is desirable for parameters that should not escape the scope of the pattern.
 *                  Conversely, renaming is best disabled if separate pattern applications should all refer to the same
 *                  parameters.
 */
final case class AdditivePattern(pattern: Expr,
                                 override val mayRename: ParamDef => Boolean = _ => true) extends PatternApplier {
  override def mergePatternInstance[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                       (reason: AddedByRewrite,
                                                                        instance: EClass): Set[EClass] = {
    Set()
  }
}

/**
 * A pattern applier that uses extraction to shift expression variables.
 * @param pattern The pattern to rewrite a match as.
 * @param shiftAmounts The amount to shift a match by.
 * @param extractor An extractor that extracts expressions from e-classes so the expressions can be shifted.
 */
final case class ExtractionBasedShiftedPattern(pattern: Expr,
                                               extractor: Extractor,
                                               shiftAmounts: Seq[(ExprVar, Int)],
                                               mayRename: ParamDef => Boolean = _ => true) extends Applier {
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {
    val shiftedExprs = mutable.Map[Long, Expr]()
    for ((exprVar, shiftAmount) <- shiftAmounts) {
      val shifted = matchInfo.tryInstantiate(exprVar)
        .flatMap(extractor.apply)
        .map(DeBruijnShift.elideRec(_).asInstanceOf[Expr])
        .flatMap(DeBruijnParamUse.tryIncrementUnbound(_, shiftAmount))
      shifted match {
        case Some(result) => shiftedExprs.put(exprVar.id, result)

        // If an expression variable cannot be shifted, abort.
        case None => return Set()
      }
    }

    ShiftedPatternInstance(pattern, shiftedExprs.toMap, mayRename)(eGraph)(rule, matchInfo, stopwatch)
  }

  private final case class ShiftedPatternInstance(pattern: Expr,
                                                  shiftedExprVars: Map[Long, Expr],
                                                  override val mayRename: ParamDef => Boolean) extends PatternApplier {
    /**
     * Merges a pattern instance with a match in a PEG.
     *
     * @param eGraph   A PEG.
     * @param instance A pattern instance created from `matchInfo`.
     * @return A set of all e-classes that were _directly_ modified as a result of the application.
     */
    override protected def mergePatternInstance[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                                   (reason: AddedByRewrite,
                                                                                    instance: EClass): Set[EClass] = {
      // Be specific about the pattern we're merging with, but only if explanations are enabled.
      if (eGraph.merge(reason.original.asInstanceOf[EClass], instance, reason)) {
        Set(reason.matchInfo.where.asInstanceOf[EClass])
      } else {
        Set()
      }
    }

    override protected def instantiateExprVar[EClass <: EClassT](exprVar: ExprVar, matchInfo: Match[EClass]): Expr = {
      shiftedExprVars.getOrElse(exprVar.id, super.instantiateExprVar(exprVar, matchInfo))
    }
  }
}

// e-matching logic and data structures inspired by egg's machine.rs
// https://github.com/egraphs-good/egg/blob/main/src/machine.rs
private final case class MachineResult[A](typeConstraints: List[(Type, Type)],
                                          exprSubstitutions: List[(ExprVar, A)],
                                          paramSubstitutions: Map[Long, ParamDef])

private final case class Reg(index: Int)

/**
 * An instruction for the search virtual machine.
 */
private trait Instruction

/**
 * An instruction that asserts that the e-class stored at the `i`th register contains an e-node with a shape
 * equivalent to `node`. The e-node's children are unpacked and appended to the tape.
 * @param node The shape of the node to match.
 * @param i The index of the e-class to inspect and unpack.
 */
private final case class Bind(node: Expr, i: Reg) extends Instruction

private final case class AssertEClass(eClass: EClassT, i: Reg) extends Instruction
private final case class BindExprVar(v: ExprVar, i: Reg) extends Instruction

/**
 * An instruction that asserts that two e-classes are equivalent.
 * @param i The index on the tape of the first e-class to inspect.
 * @param j The index on the tape of the second e-class to inspect.
 */
private final case class Compare(i: Reg, j: Reg) extends Instruction

private final case class Machine[EClass](registers: List[EClass],
                                         boundNodes: List[ENodeT],
                                         exprSubstitutions: List[(ExprVar, EClass)],
                                         typeConstraints: List[(Type, Type)],
                                         paramSubstitutions: Map[Long, ParamDef]) {
  def reg(r: Reg): EClass = {
    registers(r.index)
  }
}

private final case class PatternMatch[EClass <: EClassT](where: EClass,
                                                         machine: Machine[EClass],
                                                         typeMap: Map[TypeVarT, Type]) extends Match[EClass] {

  private val wildcardIdMap = machine
    .exprSubstitutions
    .map(x => (x._1.id, x._2))
    .toMap

  /**
   * Takes an expression variable in the matched pattern and converts the variable to a concrete expression.
   *
   * @param exprVar An expression variable.
   * @return The concrete expression to which `exprVar` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(exprVar: ExprVar): Option[EClass] = wildcardIdMap.get(exprVar.id)

  /**
   * Takes a type variable in the matched pattern and converts the variable to a concrete type.
   *
   * @param typeVar A type variable.
   * @return The concrete type to which `typeVar` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(typeVar: TypeVarT): Option[Type] = typeMap.get(typeVar)

  /**
   * Takes a parameter definition in the matched pattern and converts it to another parameter definition.
   *
   * @param param A parameter definition.
   * @return The parameter definition to which `param` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(param: ParamDef): Option[ParamDef] = machine.paramSubstitutions.get(param.id)
}
