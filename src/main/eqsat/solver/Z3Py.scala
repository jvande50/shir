package eqsat.solver

import java.io.PrintWriter
import sys.process._
import java.nio.file.Files

object Z3Py extends Optimizer {
  private class CannotParseZ3Output(output: String, cause: Throwable)
    extends Exception(s"Cannot parse Z3 output: $output", cause)

  private def generateVarDef(variable: Var): String = {
    val varType = variable match {
      case BooleanVar(_) => "Bool"
      case IntVar(_) => "Int"
    }
    s"${variable.name} = $varType" + "(\"" + variable.name + "\")"
  }

  private def seqToPython(seq: Seq[Formula]): String = {
    seq.map(toPython).mkString(", ")
  }

  private def toPython(formula: Formula): String = {
    formula match {
      case v: Var => v.name
      case BooleanConstant(true) => "True"
      case BooleanConstant(false) => "False"
      case IntConstant(n) => n.toString
      case Add(lhs, rhs) => s"${toPython(lhs)} + ${toPython(rhs)}"
      case Mul(lhs, rhs) => s"${toPython(lhs)} * ${toPython(rhs)}"
      case Equals(lhs, rhs) => s"${toPython(lhs)} == ${toPython(rhs)}"
      case LessThan(lhs, rhs) => s"${toPython(lhs)} < ${toPython(rhs)}"
      case And(args) => s"And(${seqToPython(args)})"
      case Or(args) => s"Or(${seqToPython(args)})"
      case Implies(lhs, rhs) => s"Implies(${toPython(lhs)}, ${toPython(rhs)})"
    }
  }

  private def generatePythonScript(problem: MinimizationProblem): String = {
    val importStatement = "from z3 import *"
    val varDefs = problem.vars.map(generateVarDef)
    val solverDef = "s = Optimize()"
    val constraints = problem.constraints.map(formula => s"s.add(${toPython(formula)})")
    val minimization = Seq(
      s"result = s.minimize(${toPython(problem.cost)})",
      "s.check()",
      "s.lower(result)",
      "model = s.model()",
      "print(\"[\")",
      "for symbol in model.decls(): print(f\"{symbol} = {model[symbol]},\")",
      "print(\"]\")",
    )
    ((importStatement +: varDefs :+ solverDef) ++ constraints ++ minimization).mkString("\n")
  }

  private def parseScriptOutput(output: String, vars: Seq[Var]): Map[Var, Constant] = {
    assert(output(0) == '[')
    assert(output(output.length - 1) == ']')

    val varMap = vars.map(v => (v.name, v)).toMap
    val assignmentLines = output.substring(1, output.length - 1).split(',').filter(_.trim.nonEmpty)
    val assignments = assignmentLines.map(line => {
      val Array(lhs, rhs) = line.split('=')
      val name = lhs.trim
      val variable = varMap(name)
      val valueStr = rhs.trim
      val value = variable match {
        case IntVar(_) => IntConstant(BigInt(valueStr))
        case BooleanVar(_) =>
          valueStr match {
            case "True" => BooleanConstant(true)
            case "False" => BooleanConstant(false)
            case _ => ???
          }
      }
      (variable, value)
    })
    assignments.toMap
  }

  override def apply(problem: MinimizationProblem): Map[Var, Constant] = {
    // Generate script to invoke Z3
    val script = generatePythonScript(problem)

    // Create temporary file
    val file = Files.createTempFile("", ".py")
    try {
      // Write script to temporary file
      val out = new PrintWriter(file.toFile)
      try out.println(script)
      finally out.close()

      // Run the script, invoking Z3
      val scriptOutput = s"python3 $file" !!

      // Parse the script's output
      try parseScriptOutput(scriptOutput.trim, problem.vars)
      catch { case ex: Throwable => throw new CannotParseZ3Output(scriptOutput, ex) }
    } finally {
      // Delete temporary script file
      Files.delete(file)
    }
  }
}
