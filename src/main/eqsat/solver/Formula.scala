package eqsat.solver

sealed trait Formula {
  def args: Seq[Formula]

  final def vars: Seq[Var] = {
    this match {
      case v: Var => Seq(v)
      case _ => args.flatMap(_.vars).distinct
    }
  }

  final def +(that: Formula): Formula = Add(this, that)

  final def *(that: Formula): Formula = Mul(this, that)

  final def ||(that: Formula): Formula = {
    (this, that) match {
      case (Or(leftOperands), Or(rightOperands)) => Or(leftOperands ++ rightOperands)
      case (Or(leftOperands), _) => Or(leftOperands :+ that)
      case (_, Or(rightOperands)) => Or(this +: rightOperands)
      case _ => Or(Seq(this, that))
    }
  }

  final def &&(that: Formula): Formula = {
    (this, that) match {
      case (And(leftOperands), And(rightOperands)) => And(leftOperands ++ rightOperands)
      case (And(leftOperands), _) => And(leftOperands :+ that)
      case (_, And(rightOperands)) => And(this +: rightOperands)
      case _ => And(Seq(this, that))
    }
  }

  final def ==(that: Formula): Formula = Equals(this, that)
  final def <(that: Formula): Formula = LessThan(this, that)
}

sealed trait Var extends Formula {
  override def args: Seq[Formula] = Seq.empty

  var name: String
}

final case class BooleanVar(var name: String) extends Var

final case class IntVar(var name: String) extends Var

sealed trait Constant extends Formula {
  override def args: Seq[Formula] = Seq.empty
}

final case class IntConstant(value: BigInt) extends Constant

final case class BooleanConstant(value: Boolean) extends Constant

sealed trait BinaryOp extends Formula {
  var left: Formula
  var right: Formula

  override def args: Seq[Formula] = Seq(left, right)
}

final case class Add(var left: Formula, var right: Formula) extends BinaryOp
final case class Mul(var left: Formula, var right: Formula) extends BinaryOp
final case class Equals(var left: Formula, var right: Formula) extends BinaryOp
final case class LessThan(var left: Formula, var right: Formula) extends BinaryOp
final case class Implies(var left: Formula, var right: Formula) extends BinaryOp

final case class Or(operands: Seq[Formula]) extends Formula {
  override def args: Seq[Formula] = operands
}

final case class And(operands: Seq[Formula]) extends Formula {
  override def args: Seq[Formula] = operands
}

final case class MinimizationProblem(constraints: Seq[Formula], cost: Formula) {
  def vars: Seq[Var] = (cost.vars ++ constraints.flatMap(_.vars)).distinct
}

trait Optimizer {
  def apply(problem: MinimizationProblem): Map[Var, Constant]
}
