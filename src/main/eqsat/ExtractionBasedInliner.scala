package eqsat

import core.{Expr, FunctionCall, TypeChecker}

import scala.annotation.tailrec

/**
 * An applier that takes, for a given e-class, all function call expressions with a lambda argument and inlines them.
 * @param inlineMaximally Makes the inliner perform full beta reduction on its expressions instead of beta-reducing
 *                        only the outermost function call.
 * @param extractor An extractor for the purpose of inlining.
 */
final case class ExtractionBasedInliner(inlineMaximally: Boolean, extractor: Extractor = Extractor.DefaultSmallest) extends Applier {
  /**
   * Rewrites an e-class based on an expression and type substitution map.
   *
   * @param eGraph    The graph that contains the e-class to rewrite.
   * @param matchInfo The match to rewrite.
   * @param stopwatch A stopwatch that times the application.
   * @return A set of all e-classes that were _directly_ modified as a result of the application.
   */
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {

    var changed = false
    for (node <- matchInfo.where.nodes) {
      node.expr match {
        case FunctionCall(f: EClass, arg: EClass, t) if f.nodes.nonEmpty =>
          // TODO: maybe we should explicitly require that the smallest expression we extract from `f` is a lambda.
          val fSmallest = stopwatch.child("Extraction", extractor(f))
          val fInstance = fSmallest.map(DeBruijnShift.elideRec)
          val argSmallest = stopwatch.child("Extraction", extractor(arg))
          val argInstance = argSmallest.map(DeBruijnShift.elideRec(_).asInstanceOf[Expr])
          (fInstance, argInstance) match {
            case (Some(lambda: DeBruijnLambda), Some(argInstance)) =>
              var inlined = inline(lambda, argInstance, stopwatch)
              try {
                inlined = TypeChecker.check(inlined)
                //                DeBruijnLambda.checkParamTypes(inlined)
              } catch {
                case e: Throwable =>
                  println(ExprToText(lambda))
                  println(ExprToText(argInstance))
                  println(ExprToText(inlined))

                  eGraph.explainer.foreach(explainer => {
                    val explanation = explainer.explainExistence(lambda).flatten
                    println(s"Derivation (${explanation.steps.length} steps)")
                    explanation.steps.map(_.toExpr).indexWhere({
                      case e: Expr if !DeBruijnLambda.tryCheckParamTypes(e) => true
                      case _ => false
                    }) match {
                      case -1 =>
                      case firstIncident => println(s"First invalid binding at step $firstIncident")
                    }
                    println(explanation)
                  })
                  throw e
              }
              val original = if (eGraph.explainer.isDefined)
                eGraph.add(FunctionCall(fSmallest.get, argSmallest.get, t), AlreadyThere)
              else
                matchInfo.where

              val reason = AddedByBetaReduction(
                rule,
                original,
                matchInfo,
                FunctionCall(fSmallest.get, argSmallest.get, t))

              changed |= eGraph.merge(
                original,
                stopwatch.child("Adding instantiated pattern to PEG", eGraph.add(inlined, reason)),
                reason)

            case _ =>
          }

        case _ =>
      }
    }

    if (changed) Set(matchInfo.where) else Set()
  }

  @tailrec
  private def inline(lambda: DeBruijnLambda, arg: Expr, stopwatch: HierarchicalStopwatch): Expr = {
    val inlined = stopwatch.child("Beta reduction", lambda.betaReduce(arg))
    (inlineMaximally, inlined) match {
      case (true, FunctionCall(innerLambda: DeBruijnLambda, innerArg: Expr, _)) =>
        inline(innerLambda, innerArg, stopwatch)
      case _ => inlined
    }
  }
}
