package eqsat

import scala.collection.mutable

/**
 * An engine that applies the equality saturation algorithm to an e-graph.
 * @param graph The e-graph to operate on.
 * @param observer An observer that tracks actions performed by the saturation engine.
 * @param useBatchSearch Makes the saturation engine search the e-graph in batch if set to `true`. Makes the engine
 *                       search every e-class individually if set to `false`.
 * @param applyMatchesOnce Makes the saturation engine apply matches only once if set to `true`.
 * @param shouldPrintProgress Makes the saturation engine print progress messages to standard output if set to `true`.
 * @param rebuildAfterEveryApplication Makes the saturation engine rebuild the e-graph after every rule application if
 *                                     set to `true`.
 * @tparam ENode The type of e-node to operate on.
 * @tparam EClass The type of e-class to operate on.
 */
case class SaturationEngine[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                                observer: EGraphObserver = EGraphNullObserver,
                                                                useBatchSearch: Boolean = true,
                                                                applyMatchesOnce: Boolean = true,
                                                                shouldPrintProgress: Boolean = false,
                                                                rebuildAfterEveryApplication: Boolean = false) {

  def withGraph[ENode2 <: ENodeT, EClass2 <: EClassT](newGraph: EGraph[ENode2, EClass2]): SaturationEngine[ENode2, EClass2] =
    SaturationEngine(newGraph, observer, useBatchSearch, applyMatchesOnce, shouldPrintProgress, rebuildAfterEveryApplication)

  /**
   * Applies rewrite rules until a fixpoint is reached.
   *
   * @param rewrites The set of rewrite rules to apply.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(rewrites: Seq[Rewrite]): Int = {
    saturate(rewrites, _ => false)
  }

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param rewrites The set of rewrite rules to apply.
   * @param shouldTerminate An optional termination criterion that takes the number of saturation steps thus far and
   *                        returns a Boolean that indicates if saturation should terminate.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(rewrites: Seq[Rewrite], shouldTerminate: Int => Boolean): Int = {
    saturate(SimpleSaturationStrategy(rewrites), shouldTerminate)
  }

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param rewrites The set of rewrite rules that apply in a given iteration.
   * @param shouldTerminate An optional termination criterion that takes the number of saturation steps thus far and
   *                        returns a Boolean that indicates if saturation should terminate.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(rewrites: Int => Seq[Rewrite], shouldTerminate: Int => Boolean): Int = {
    saturate(ComputedSaturationStrategy(rewrites), shouldTerminate)
  }

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param strategy A saturation strategy.
   * @param shouldTerminate An optional termination criterion that takes the number of saturation steps thus far and
   *                        returns a Boolean that indicates if saturation should terminate.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(strategy: SaturationStrategy, shouldTerminate: Int => Boolean): Int = {
    var iteration = 0
    val engine = new RewriteEngineImpl()
    while (true) {
      if (shouldTerminate(iteration)) {
        observer.onFinish(graph)
        return iteration
      }
      observer.onSaturateStep(graph)
      if (shouldPrintProgress) {
        println(s"Running saturation iteration $iteration")
      }
      val more = strategy.runIteration(iteration, engine)
      if (!more) {
        observer.onFinish(graph)
        return iteration
      }
      iteration += 1
    }
    iteration
  }

  class RewriteEngineImpl extends RewriteEngine {
    private val appliedMatches = mutable.Map[Rewrite, mutable.HashSet[Match[EClass]]]()

    case class RebuildingApplier(innerApplier: Applier) extends Applier {
      /**
       * Rewrites an e-class based on an expression and type substitution map.
       * @param eGraph The graph that contains the e-class to rewrite.
       * @param rule The rewrite rule that triggered this applier.
       * @param matchInfo The match to rewrite.
       * @param stopwatch A stopwatch that times the application.
       * @return A set of all e-classes that were _directly_ modified as a result of the application.
       */
      def apply[ENode2 <: ENodeT, EClass2 <: EClassT](eGraph: EGraph[ENode2, EClass2])
                                                     (rule: Rewrite,
                                                      matchInfo: Match[EClass2],
                                                      stopwatch: HierarchicalStopwatch): Set[EClass2] = {
        val result = innerApplier(eGraph)(rule, matchInfo, stopwatch)
        graph.rebuild()
        result
      }
    }

    /**
     * Applies a set of rewrite rules in batch.
     *
     * @param rules The rules to apply.
     * @return The set of classes that were changed by applying the rules.
     */
    override def applyRules(rules: Seq[Rewrite]): Set[EClassT] = {
      // First, we'll find all matches in an (implicit) read phase during which e-graph invariants are maintained.
      val matches = rules.map(rw => (rw, ObservedRuleSearcher(rw, observer, useBatchSearch).search(graph)))

      // Then, invariants may be temporarily broken in a write phase, but that doesn't matter because we won't be matching
      // during this phase.
      val changedClasses = matches
        .map(pair => {
          val (rw, matchList) = pair
          val applied = appliedMatches.getOrElseUpdate(rw, mutable.HashSet[Match[EClass]]())
          val newMatches = if (applyMatchesOnce) matchList.filterNot(applied.contains) else matchList
          var applier: Applier = ObservedRuleApplier(observer)
          if (rebuildAfterEveryApplication) {
            applier = RebuildingApplier(applier)
          }
          val changed = applier.applyMany(graph)(rw, newMatches)
          newMatches.forall(applied.add)
          changed
        })
        .foldLeft(Set[EClass]())(_ ++ _)

      // Rebuild the graph's invariants.
      graph.rebuild()

      // Return the set of modified classes.
      changedClasses.toSet
    }
  }
}
