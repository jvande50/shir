package eqsat

/**
 * An applier that is generated from matches at application time.
 * @param generateApplier A function that takes a match and generates an applier to rewrite the match.
 */
final case class GeneratedApplier(generateApplier: (Match[EClassT], HierarchicalStopwatch) => Applier) extends Applier {
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {
    val applier = generateApplier(matchInfo, stopwatch)
    applier(eGraph)(rule, matchInfo, stopwatch)
  }
}
