package eqsat.explain

import eqsat.{ENodeT, ExprToText}
import core.Expr

case class FlatExplanation[L <: ENodeT](steps: Seq[FlatTerm[L]]) {
  def apply(index: Int): FlatTerm[L] = steps(index)
  def map(f: FlatTerm[L] => FlatTerm[L]): FlatExplanation[L] = FlatExplanation(steps.map(f))

  /**
   * Creates a new flat explanation that combines adjacent steps.
   * @return A flat explanation with combined adjacent steps.
   */
  def combineAdjacentSteps: FlatExplanation[L] = {
    val combinedSteps = steps
      .foldLeft[List[FlatTerm[L]]](Nil)({
        case (lastItem :: tail, item) =>
          lastItem.tryCombineRewrites(item) match {
            case Some(newRewrite) => newRewrite :: tail
            case None => item :: lastItem :: tail
          }

        case (results, item) => item :: results
      })
      .reverse

    FlatExplanation(combinedSteps)
  }

  override def toString: String = steps.map(_.toString).mkString("\n")
}

case class FlatTerm[L <: ENodeT](node: L,
                                 backwardRule: Option[String],
                                 forwardRule: Option[String],
                                 children: Seq[FlatTerm[L]]) {

  assert(children.length == node.args.length)

  def withChildren(newChildren: Seq[FlatTerm[L]]): FlatTerm[L] =
    FlatTerm(node, backwardRule, forwardRule, newChildren)

  def hasRewriteForward: Boolean = forwardRule.nonEmpty
  def hasRewriteBackward: Boolean = backwardRule.nonEmpty

  def this(node: L, children: Seq[FlatTerm[L]]) = this(node, None, None, children)

  def withoutRewrites: FlatTerm[L] = FlatTerm(node, None, None, children.map(_.withoutRewrites))

  /**
   * Tries to fold another term's rewrites into this term's, creating a new term that captures the meaning of both
   * rewrites.
   * @param other Another rewrite to fold into this rewrite.
   * @return A combined rewrite if one can be created; otherwise, `None`.
   */
  def tryCombineRewrites(other: FlatTerm[L]): Option[FlatTerm[L]] = {
    // We can't combine more than one forward or backward rewrite at the same node.
    (forwardRule, other.forwardRule, backwardRule, other.backwardRule) match {
      case (Some(_), Some(_), _, _) | (_, _, Some(_), Some(_)) => return None
      case _ =>
    }

    if (children.length != other.children.length || !node.matches(other.node)) {
      // Child arity or node type mismatch.
      return None
    }

    // Combine children.
    val newChildren = children
      .zip(other.children)
      .flatMap(t => t._1.tryCombineRewrites(t._2))

    if (newChildren.length != children.length) {
      // One or more children couldn't be combined.
      return None
    }

    Some((other.forwardRule, other.backwardRule) match {
      case (None, None) => FlatTerm(node, forwardRule, backwardRule, newChildren)
      case (None, backwardRule) => FlatTerm(node, forwardRule, backwardRule, newChildren)
      case (forwardRule, None) => FlatTerm(node, forwardRule, backwardRule, newChildren)
      case (forwardRule, backwardRule) => FlatTerm(node, forwardRule, backwardRule, newChildren)
    })
  }

  def combineRewrites(other: FlatTerm[L]): FlatTerm[L] = tryCombineRewrites(other).get

  private def toAnnotatedExpr: Expr = {
    val baseExpr = node.instantiate(children.map(_.toAnnotatedExpr))
    (forwardRule, backwardRule) match {
      case (Some(forward), None) => Annotation(s"Rewrite=> $forward", Seq(baseExpr))
      case (None, Some(backward)) => Annotation(s"Rewrite<= $backward", Seq(baseExpr))
      case (None, None) => baseExpr
      case (Some(_), Some(_)) => ???
    }
  }

  override def toString: String = ExprToText(toAnnotatedExpr)

  def toExpr: Expr = withoutRewrites.toAnnotatedExpr
}
