package eqsat.explain

import eqsat.{ENodeT, ExprToText}
import core.Expr

import scala.collection.mutable

case class TreeExplanation[L <: ENodeT](steps: Seq[TreeTerm[L]]) {
  def apply(index: Int): TreeTerm[L] = steps(index)

  /**
   * Rejiggers this tree explanation as a step-by-step flat explanation.
   * @return A `FlatExplanation[L]` equivalent to this `TreeExplanation[L]`.
   */
  def flatten: FlatExplanation[L] = {
    //    // Build sequence of flat steps, combining adjacent rewrites whenever useful. We'll accumulate in reverse because
    //    // that happens to be convenient.
    //    val flatSteps = steps
    //      .map(_.flattenExplanation.steps.toList)
    //      .foldLeft[List[FlatTerm[L]]](Nil)({
    //        case (lastItem :: tail, first :: newTail) if !first.hasRewriteForward && !first.hasRewriteBackward =>
    //          newTail.reverse ++ (lastItem.combineRewrites(first) :: tail)
    //
    //        case (results, item) => item.reverse ++ results
    //      })
    //      .reverse
    //
    //    FlatExplanation(flatSteps)

    FlatExplanation(steps.flatMap(_.flattenExplanation.steps)).combineAdjacentSteps
  }
}

case class TreeTerm[L <: ENodeT](node: L,
                                 backwardRule: Option[String],
                                 forwardRule: Option[String],
                                 childProofs: Seq[TreeExplanation[L]]) {


  def this(node: L, childProofs: Seq[TreeExplanation[L]]) = this(node, None, None, childProofs)

  def withForwardRule(forwardRule: Option[String]): TreeTerm[L] =
    TreeTerm(node, backwardRule, forwardRule, childProofs)

  def withBackwardRule(backwardRule: Option[String]): TreeTerm[L] =
    TreeTerm(node, backwardRule, forwardRule, childProofs)

  def withChildProofs(childProofs: Seq[TreeExplanation[L]]): TreeTerm[L] =
    TreeTerm(node, backwardRule, forwardRule, childProofs)

  def flattenExplanation: FlatExplanation[L] = {
    val proof = mutable.ArrayBuffer[FlatTerm[L]]()
    val childProofs = mutable.ArrayBuffer[FlatExplanation[L]]()
    val representativeTerms = mutable.ArrayBuffer[FlatTerm[L]]()

    for (childExplanation <- this.childProofs) {
      val flatProof = flattenProof(childExplanation.steps)
      representativeTerms.append(flatProof(0).withoutRewrites)
      childProofs.append(flatProof)
    }

    proof += FlatTerm(node, None, None, representativeTerms.clone())

    for ((childProof, i) <- childProofs.zipWithIndex) {
      val lastChildren = mutable.ArrayBuffer(proof.last.children:_*)
      lastChildren(i) = childProof(0)
      proof(proof.length - 1) = proof.last.withChildren(lastChildren)

      for (child <- childProof.steps.drop(1)) {
        val children = representativeTerms
          .zipWithIndex
          .map({
            case (_, j) if j == i => child
            case (repTerm, _) => repTerm
          })

        proof.append(FlatTerm(node, None, None, children))
      }

      representativeTerms(i) = childProof.steps.last.withoutRewrites
    }

    proof(0) = FlatTerm(
      proof(0).node,
      backwardRule,
      forwardRule,
      proof(0).children
    )

    FlatExplanation(proof)
  }

  private def flattenProof(proof: Seq[TreeTerm[L]]): FlatExplanation[L] = {
    val flatProof = mutable.ArrayBuffer[FlatTerm[L]]()
    for (tree <- proof) {
      val explanation = mutable.ArrayBuffer(tree.flattenExplanation.steps:_*)
      if (flatProof.nonEmpty && !explanation(0).hasRewriteForward && !explanation(0).hasRewriteBackward) {
        val last = flatProof.last
        flatProof.remove(flatProof.length - 1)
        explanation(0) = explanation(0).combineRewrites(last)
      }

      flatProof ++= explanation
    }

    FlatExplanation(flatProof)
  }

  private def toAnnotatedExpr: Expr = {
    val baseExpr = if (node.isLeaf) {
      node.expr
    } else {
      node.instantiate(childProofs.map(child => {
        if (child.steps.length == 1) {
          child.steps.head.toAnnotatedExpr
        } else {
          Annotation("Explanation", child.steps.map(_.toAnnotatedExpr))
        }
      }))
    }

    (forwardRule, backwardRule) match {
      case (Some(forward), None) => Annotation(s"Rewrite=> ${forward}", Seq(baseExpr))
      case (None, Some(backward)) => Annotation(s"Rewrite<= ${backward}", Seq(baseExpr))
      case (None, None) => baseExpr
      case (Some(_), Some(_)) => ???
    }
  }

  override def toString: String = ExprToText(toAnnotatedExpr)
}
