package eqsat.explain

import eqsat.ENodeT
import core.Expr

/**
 * A service that explains why expressions appear in an e-graph or are equivalent.
 * @tparam L The type of an e-node.
 */
trait ExplanationService[L <: ENodeT] {
  /**
   * Explains why an expression exists in the e-graph.
   * @param expr An expression to explain.
   * @return A tree-form explanation of the expression.
   */
  def explainExistence(expr: Expr): TreeExplanation[L]

  /**
   * Explains why two expressions are equivalent.
   * @param left A first expression.
   * @param right A second expression.
   * @return An explanation of why `left` and `right` are equivalent.
   */
  def explainEquivalence(left: Expr, right: Expr): TreeExplanation[L]
}
