package eqsat

import eqsat.explain.{Congruence, Explainer, Justification}

import scala.collection.mutable

/**
 * An e-graph implementation based on Willsey et al., egg: Fast and extensible equality saturation, 2021.
 */
final class Willsey2021EGraph(val observer: EGraphObserver = EGraphNullObserver,
                              val analyses: Analyses = Analyses(Seq()),
                              val applyMatchesOnce: Boolean = true,
                              val useBatchSearch: Boolean = true,
                              val rebuildAfterMerge: Boolean = false,
                              val shouldPrintProgress: Boolean = false,
                              val checkConsistency: Boolean = false,
                              val enableExplanations: Boolean = false) extends HashConsEGraph {
  private val repairWorklist = mutable.ArrayBuffer[HashConsEClass]()

  protected override val explainerImpl: Option[Explainer[HashConsENode, HashConsEClass]] = {
    if (enableExplanations) {
      Some(new Explainer[HashConsENode, HashConsEClass]())
    } else {
      None
    }
  }

  override def defaultSaturationEngine: SaturationEngine[HashConsENode, HashConsEClass] =
    SaturationEngine(this, observer, useBatchSearch, applyMatchesOnce, shouldPrintProgress)

  /**
   * Merges two e-classes. This indicates that `left` and `right` produce the exact same value.
   *
   * @param left  The first e-class to merge.
   * @param right The second e-class to merge.
   * @param reason The justification for why `left` and `right` are merged.
   * @return `true` if merging the e-classes changed the graph; otherwise, `false`.
   */
  override def performMerge(left: HashConsEClass, right: HashConsEClass, reason: Justification): Boolean = {
    //    println(s"Merging $left and $right")
    val result = mergeImpl(left, right, reason)
    if (rebuildAfterMerge) {
      rebuild()
    }
    result
  }

  private def classToText(eClass: HashConsEClass): String = {
    if (find(eClass) == eClass) {
      eClass.toString
    } else {
      s"$eClass (aka ${find(eClass)})"
    }
  }

  private def mergeImpl(left: HashConsEClass, right: HashConsEClass, reason: Justification): Boolean = {
    assert(left.t == right.t)

    //    println(s"Merging ${classToText(left)} and ${classToText(right)}")

    // We want to unconditionally register this merge with the explainer. If another rule has beaten us to the punch of
    // merging two e-classes, we still want to be able to explain the relationship between equivalent nodes within those
    // e-classes.
    explainerImpl.foreach(_.union(left, right, reason))

    // Find the set leader for the left and right classes.
    val (leftRoot, rightRoot) = (unionFind.find(left), unionFind.find(right))
    if (leftRoot == rightRoot) {
      return false
    }

    val leftAnalysisResults = analyses.analyses.map(a => (a, leftRoot.analysisResult(a))).toMap
    val rightAnalysisResults = analyses.analyses.map(a => (a, rightRoot.analysisResult(a))).toMap

    // Unify the left and right leaders.
    val newId = unionFind.union(leftRoot, rightRoot)

    // Construct a list of e-node, e-class pairs that need to be canonicalized and checked for congruence.
    repairWorklist.append(newId)

    val (oldId, oldAnalysisResults, newAnalysisResults) = if (newId == leftRoot) {
      (rightRoot, rightAnalysisResults, leftAnalysisResults)
    } else {
      (leftRoot, leftAnalysisResults, rightAnalysisResults)
    }

    newId.parents ++= oldId.parents
    for (node <- oldId.nodeSet) {
      node.expr match {
        case use1: DeBruijnParamUse =>
          assert(!newId.nodeSet.exists(_.expr match {
            case use2: DeBruijnParamUse => use1.index != use2.index
            case _ => false
          }))

        case _ =>
      }

      newId.nodeSet.add(node)

      // Patch up parent lists of child classes.
      for (arg <- node.args) {
        repairWorklist.append(arg)
      }
    }

    // Patch up parent lists of sibling classes.
    for ((_, parent) <- oldId.parents) {
      for (node <- parent.nodes) {
        for (arg <- node.args) {
          if (find(arg) != newId) {
            repairWorklist.append(arg)
          }
        }
      }
    }

    // Update the merged e-class' analysis results.
    for (analysis <- analyses.analyses) {
      val newResult = analysis.join(newAnalysisResults(analysis), oldAnalysisResults(analysis))
      newId.setAnalysisResult(analysis, newResult)
    }

    true
  }

  private def printClassContents(eClass: HashConsEClass): Unit = {
    println(s"$eClass nodes: ${eClass.nodes}")
    println(s"$eClass parents: ${eClass.parents}")
  }

  private def repair(eClass: HashConsEClass): Unit = {
    repairHashcons(eClass)

    for (analysis <- analyses.analyses) {
      analysis.modify(this)(find(eClass), find(eClass).analysisResult(analysis))

      for ((parentNode, parentClass) <- find(eClass).parents) {
        val canonicalNode = canonicalize(parentNode)
        val canonicalClass = find(parentClass)
        val oldResult = canonicalClass.analysisResult(analysis)
        val newResult = analysis.join(oldResult, analysis.make(canonicalNode))
        if (newResult != oldResult) {
          canonicalClass.setAnalysisResult(analysis, newResult)
          repairWorklist.append(canonicalClass)
        }
      }
    }
  }

  private def repairHashcons(eClass: HashConsEClass): Unit = {
    for ((parentNode, parentClass) <- eClass.parents) {
      val canonicalNode = canonicalize(parentNode)
      val canonicalClass = find(parentClass)
      hashCons.remove(parentNode)
      canonicalClass.nodeSet.remove(parentNode)
      hashCons(canonicalNode) = parentClass
      canonicalClass.nodeSet.add(canonicalNode)
    }

    repairParents(eClass)
  }

  private def repairParents(eClass: HashConsEClass): Unit = {
    val classesToMerge = mutable.Buffer[(HashConsEClass, HashConsEClass)]()
    val newParents = mutable.Map[HashConsENode, HashConsEClass]()
    for ((parentNode, parentClass) <- eClass.parents) {
      val canonicalNode = canonicalize(parentNode)
      newParents.get(canonicalNode) match {
        case Some(otherClass) if otherClass != parentClass =>
          //          println(s"Upward merging $parentClass and $otherClass")
          classesToMerge.append((parentClass, otherClass))

        case _ =>
      }
      newParents(canonicalNode) = parentClass
    }

    find(eClass).parents = newParents.toBuffer

    for ((left, right) <- classesToMerge) {
      mergeImpl(left, right, Congruence)
    }
  }

  /**
   * Rebuilds the e-graph's invariants, which may be temporarily broken during rule application.
   */
  override def rebuild(): Unit = {
    if (checkConsistency && shouldPrintProgress) {
      explainerImpl.foreach(_.assertConsistency())
      println("=== rebuilding ===")
    }
    while (repairWorklist.nonEmpty) {
      val todo = repairWorklist.map(find).distinct
      repairWorklist.clear()
      for (eClass <- todo) {
        repair(find(eClass))
      }
    }
    if (checkConsistency) {
      if (shouldPrintProgress) {
        println("=== done ===")
      }
      assertConsistency()
      for (analysis <- analyses.analyses) {
        analysis.assertConsistency(this)
      }
      if (shouldPrintProgress) {
        println("=== consistent ===")
      }
    }
  }
}
