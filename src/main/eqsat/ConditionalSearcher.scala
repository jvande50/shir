package eqsat

/**
 * A searcher that accepts only those patterns that accept a user-specified condition.
 * @param searcher An inner searcher that proposes patterns.
 * @param condition A predicate that accepts or rejects patterns proposed by `searcher`.
 */
final case class ConditionalSearcher(searcher: Searcher, condition: Match[EClassT] => Boolean) extends Searcher {

  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                               (eClass: EClass,
                                                                stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {
    val candidates = searcher.searchEClass(eGraph)(eClass, stopwatch)
    stopwatch.child("Custom filtering", candidates.filter(condition))
  }
}
