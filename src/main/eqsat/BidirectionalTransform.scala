package eqsat

/**
 * A reversible transform.
 * @tparam A The type of untransformed values.
 * @tparam B Te type of transformed values.
 */
trait BidirectionalTransform[A, B] {
  /**
   * Performs the forward transform.
   * @param value A value to transform.
   * @return A transformed version of `value`.
   */
  def forward(value: A): B

  /**
   * Performs the backward transform, which inverts the forward transform.
   * @param value A transformed value.
   * @return A value whose transformed version is `value`.
   */
  def backward(value: B): A
}

object BidirectionalTransform {
  /**
   * The identity transform, which does not change its inputs.
   * @tparam A The type of values that can be identity-transformed.
   * @return An identity transform.
   */
  def identity[A]: BidirectionalTransform[A, A] = new BidirectionalTransform[A, A] {
    override def forward(value: A): A = value
    override def backward(value: A): A = value
  }
}
