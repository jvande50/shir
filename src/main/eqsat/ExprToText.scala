package eqsat

import core.{ShirIR, TextFormatter, VariableRenamer}

object ExprToText {
  def apply(expr: ShirIR, mustBeAtomic: Boolean = false, renameVars: Boolean = true): String = {
    val formatter = TextFormatter(new VariableRenamer(renameVars), mustBeAtomic)
    formatter.format(expr)
  }
}
