package eqsat
import core.Expr
import eqsat.solver.MinimizationProblem

import scala.annotation.tailrec
import scala.collection.mutable

/**
 * An extractor implementation that relies on a solver to make a global selection of which e-nodes are optimal to
 * extract. This extractor assumes that extracted e-nodes can be reused in multiple places at no cost, an assumption
 * that is true when the final output is structured as, e.g., control-flow graphs. This assumption can also be enforced
 * by trailing an outlining pass after this extractor (see `OutliningExtractor`).
 *
 * Because of the global judgment this extractor makes, the extractor is particularly suitable for code size and
 * resource-usage optimization. It is not as readily applicable to computation time optimization for functional as
 * `costFunction` cannot multiply the cost of an expression by the cost of one of its arguments.
 * @param costFunction A cost function that determines the cost of each operation in the e-graph.
 * @param isEligible A predicate that determines if an e-node is an eligible candidate for extraction. Ineligible
 *                   e-nodes will never show up in the extractor's node selection and its extracted expressions.
 * @param solverInterface An interface to an external solver.
 */
case class SolverExtractor(costFunction: Expr => BigInt,
                           isEligible: ENodeT => Boolean = _ => true,
                           solverInterface: solver.Optimizer = solver.Z3Py) extends SelectingExtractor {

  override def select(eClass: EClassT): PartialFunction[EClassT, ENodeT] = {
    // Associate each class with two variables. One determines if the class is selected or not, the other is a unique
    // label for the class. The label is used to make sure we don't select a cyclic graph
    val classSelectionVars = mutable.Map[EClassT, solver.BooleanVar]()
    val classLabelVars = mutable.Map[EClassT, solver.Formula]()
    val reachableClasses = removeIneligibleClasses(eClass.reachable)
    val reachableClassSet = reachableClasses.toSet
    for ((c, index) <- reachableClasses.zipWithIndex) {
      classSelectionVars(c) = solver.BooleanVar(s"select_c$index")

      if (c == eClass) {
        classLabelVars(c) = solver.IntConstant(0)
      } else {
        classLabelVars(c) = solver.IntVar(s"label_c$index")
      }
    }

    // Encode constraints for each class
    val constraints = mutable.ArrayBuffer[solver.Formula]()
    val nodeSelectionVars = mutable.Map[(EClassT, ENodeT), solver.BooleanVar]()
    val classCosts = mutable.Map[EClassT, solver.Formula]()
    for (c <- reachableClasses) {
      val nodeSelectsAndCosts = mutable.ArrayBuffer[(solver.BooleanVar, solver.Formula)]()
      for (node <- c.nodes.filter(isNodeEligible(_, reachableClassSet))) {
        val selectNode = solver.BooleanVar(s"select_n${nodeSelectionVars.size}")
        nodeSelectionVars((c, node)) = selectNode
        val nodeCost = costFunction(node.expr)
        nodeSelectsAndCosts.append((selectNode, solver.IntConstant(nodeCost)))

        if (node.args.nonEmpty) {
          // Add a constraint that specifies that if a node is selected, its arguments must also be selected
          constraints.append(solver.Implies(selectNode, node.args.map(classSelectionVars(_)).reduce[solver.Formula](_ && _)))

          // Add another constraint that makes it so that c's label is less than each of its arguments' labels
          for (arg <- node.args) {
            constraints.append(solver.Implies(selectNode, classLabelVars(c) < classLabelVars(arg)))
          }
        }
      }

      // Set the total cost of the class to the sum of the costs of its selected nodes
      classCosts(c) = nodeSelectsAndCosts.map(x => x._1 * x._2).reduce(_ + _)

      // Add a constraint that requires any selected class to have at least one selected node
      constraints.append(solver.Implies(classSelectionVars(c), nodeSelectsAndCosts.map(_._1).reduce[solver.Formula](_ || _)))
    }

    // The optimization problem consists of the assertion that the root class is selected and the objective of
    // minimizing the total cost
    constraints.append(classSelectionVars(eClass) == solver.BooleanConstant(true))
    val totalCostVar = solver.IntVar("total_cost")
    constraints.append(totalCostVar == classCosts.values.reduce(_ + _))
    val solution = solverInterface(MinimizationProblem(constraints, totalCostVar))

    // Use the optimization problem's solution to determine which nodes we pick
    nodeSelectionVars.collect({
      case ((c, n), v) if solution(v).asInstanceOf[solver.BooleanConstant].value => (c, n)
    }).toMap
  }

  @tailrec
  private def removeIneligibleClasses(classes: Seq[EClassT]): Seq[EClassT] = {
    // Classes are ineligible either if all of their nodes are ineligible *or* if all of their nodes takes at least one
    // argument that is not in `classes`.
    val classSet = classes.toSet
    val newClasses = classes.filter(_.nodes.exists(isNodeEligible(_, classSet)))

    // Run this function until we reach a fixpoint
    if (newClasses == classes) {
      classes
    } else {
      removeIneligibleClasses(newClasses)
    }
  }

  private def isNodeEligible(node: ENodeT, eligibleClasses: Set[EClassT]): Boolean = {
    isEligible(node) && node.args.forall(eligibleClasses.contains)
  }
}
