package eqsat
import eqsat.explain.ExplanationService
import core.Expr

import scala.collection.mutable.ArrayBuffer

/**
 * An e-graph implementation that stores merge commands in a buffer that is flushed on `rebuild`.
 * @param graph An inner e-graph.
 * @tparam ENode The type for e-nodes in the graph.
 * @tparam EClass The type for e-classes in the graph.
 */
final class DeferredMergingEGraph[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
  extends EGraph[ENode, EClass] {

  /**
   * A buffer of outstanding `merge` commands.
   */
  private val mergeBuffer = ArrayBuffer[(EClass, EClass, NodeReason)]()

  /**
   * Gets the set of classes in this e-graph.
   *
   * @return The set of classes in this e-graph.
   */
  override def classes: Seq[EClass] = graph.classes

  /**
   * Adds an expression tree to this graph. The tree's descendants are recursively converted to e-nodes. They may be
   * unified with existing e-nodes/e-classes in the graph. If e-classes are included in the tree, then they are
   * canonicalized.
   *
   * @param expr   An expression to add to the graph.
   * @param reason The reason why nodes are added.
   * @return The e-class of the expression in the tree.
   */
  override def add(expr: Expr, reason: NodeReason): EClass = graph.add(expr, reason)

  /**
   * Computes an analysis' results for the entire graph.
   *
   * @param analysis The analysis to compute.
   * @tparam A The type of the analysis' results.
   */
  override def recomputeAnalysis[A](analysis: Analysis[A]): Unit = graph.recomputeAnalysis(analysis)

  override def merge(left: EClass, right: EClass, reason: NodeReason): Boolean = {
    assert(left.t == right.t)
    mergeBuffer.append((left, right, reason))
    graph.find(left) != graph.find(right)
  }

  /**
   * Rebuilds the e-graph's invariants, which may be temporarily broken during rule application.
   */
  override def rebuild(): Unit = {
    for ((left, right, reason) <- mergeBuffer) {
      graph.merge(left, right, reason)
    }
    mergeBuffer.clear()
    graph.rebuild()
  }

  override def defaultSaturationEngine: SaturationEngine[ENode, EClass] = graph.defaultSaturationEngine.withGraph(this)

  /**
   * The e-node explanation service, if one is configured for this e-graph.
   *
   * @return An explanation service.
   */
  override def explainer: Option[ExplanationService[ENode]] = graph.explainer
}
