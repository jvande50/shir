package eqsat

import java.time.Duration
import scala.collection.mutable

/**
 * A PEG observer that gathers statistics about
 */
class TimingObserver extends EGraphObserver {
  /**
   * The amount of time each rule application took.
   */
  private val ruleApplicationData = mutable.HashMap[Rewrite, MeasuredActivity]()

  /**
   * The amount of time each rule search took.
   */
  private val ruleSearchData = mutable.HashMap[Rewrite, MeasuredActivity]()

  def ruleApplications: Map[Rewrite, MeasuredActivity] = ruleApplicationData.toMap

  def ruleSearches: Map[Rewrite, MeasuredActivity] = ruleSearchData.toMap

  override def onApply[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                          (rule: Rewrite, duration: MeasuredActivity): Unit = {
    ruleApplicationData.put(rule, ruleApplicationData.get(rule) match {
      case Some(data) => data.union(duration)
      case None => duration
    })
  }

  override def onSearch[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                           (rule: Rewrite, duration: MeasuredActivity): Unit = {
    ruleSearchData.put(rule, ruleSearchData.get(rule) match {
      case Some(data) => data.union(duration)
      case None => duration
    })
  }

  def totalTime: Duration =
    ruleSearches.map(_._2.duration).reduce(_.plus(_)).plus(ruleApplications.map(_._2.duration).reduce(_.plus(_)))

  override def toString: String = {
    val delimiter = "============="
    (("Search" +: delimiter +: summarize(ruleSearches)) ++ ("" +: "Application" +: delimiter +: summarize(ruleApplications)))
      .mkString("\n")
  }

  private def summarize(data: Map[Rewrite, MeasuredActivity]) = {
    val dataPerRule = data
      .values
      .toSeq
      .sortBy(_.duration)
      .reverse

    val total = dataPerRule.reduce(_.union(_)).withName("All rules")

    (total +: dataPerRule).flatMap(_.toLines)
  }
}
