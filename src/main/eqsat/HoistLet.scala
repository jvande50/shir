package eqsat

import cGen.CLambda
import core.{AnyTypeVar, Expr, ExprVar, FunctionCall, LambdaT, ParamDef, TypeChecker, TypeVarT}

final case class HoistLet(override implicit val buildLambda: (ParamDef, Expr) => LambdaT = (p, d) => CLambda(p, d)) extends Hoist {
  override val argPattern: Searcher = {
    Pattern(TypeChecker.check(FunctionCall(DeBruijnLambda(HoistLetVars.t, HoistLetVars.body), HoistLetVars.arg)))
  }

  override protected def argInAbstraction[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                             (rule: Rewrite,
                                                                              matchInfo: HoistMatch[EClass],
                                                                              stopwatch: HierarchicalStopwatch): Expr = {
    matchInfo.instantiate(HoistLetVars.body)
  }

  override protected def argInApplication[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                             (rule: Rewrite,
                                                                              matchInfo: HoistMatch[EClass],
                                                                              stopwatch: HierarchicalStopwatch): Expr = {
    matchInfo.instantiate(HoistLetVars.arg)
  }
}

private object HoistLetVars {
  val t: TypeVarT = AnyTypeVar()
  val body: ExprVar = ExprVar()
  val arg: ExprVar = ExprVar(t)
}