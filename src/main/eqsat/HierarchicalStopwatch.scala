package eqsat

import java.time.Duration
import scala.collection.mutable

/**
 * An activity whose duration was measured.
 * @param name The activity's name.
 * @param count The number of times the activity was performed.
 * @param durationInNs The amount of time it took to complete all iterations of the activity, in nanoseconds.
 * @param children The activity's sub-activities, also measured.
 */
case class MeasuredActivity(name: String, count: Int, durationInNs: Long, children: Seq[MeasuredActivity]) {
  def duration: Duration = Duration.ofNanos(durationInNs)

  def withName(newName: String): MeasuredActivity = MeasuredActivity(newName, count, durationInNs, children)

  def union(other: MeasuredActivity): MeasuredActivity = {
    val ownChildren = children.map(c => (c.name, c)).toMap
    val otherChildren = other.children.map(c => (c.name, c)).toMap
    val mergedChildren = ownChildren.keySet
      .union(otherChildren.keySet)
      .toSeq
      .map(k => (ownChildren.get(k), otherChildren.get(k)) match {
        case (Some(m1), Some(m2)) => m1.union(m2)
        case (Some(m1), None) => m1
        case (None, Some(m2)) => m2
        case (None, None) => ???
      })
      .sortBy(_.duration)
      .reverse

    MeasuredActivity(name, count + other.count, durationInNs + other.durationInNs, mergedChildren)
  }

  def toLines: Seq[String] = {
    s"- $name: $count times, ${duration.toMillis}ms total, ${duration.toMillis / count}ms each" +:
      children.flatMap(_.toLines).map("\t" + _)
  }

  override def toString: String = {
    toLines.mkString("\n")
  }
}

/**
 * A stopwatch that measures a hierarchy of activities.
 * @param name The name of the activity being measured by this stopwatch.
 * @param start The start time of the activity being measures, in nanoseconds.
 * @param parent A parent activity to which this activity belongs.
 */
class HierarchicalStopwatch private (name: String,
                                     start: Long,
                                     isEnabled: Boolean,
                                     parent: Option[HierarchicalStopwatch]) {
  private val finishedChildren = mutable.ArrayBuffer[MeasuredActivity]()

  /**
   * The amount of time in nanoseconds that has elapsed since this stopwatch was started.
   * @return A duration in nanoseconds. If this stopwatch is disabled, a value of zero is returned.
   */
  def elapsedNanoseconds: Long = if (isEnabled) System.nanoTime() - start else 0

  /**
   * The amount of time that has elapsed since this stopwatch was started.
   * @return A duration. If this stopwatch is disabled, a zero duration is returned.
   */
  def elapsed: Duration = Duration.ofNanos(elapsedNanoseconds)

  /**
   * Creates a stopwatch for a child activity. This stopwatch must be completed manually. If this stopwatch is
   * disabled, then the child will be disabled as well.
   * @param name The name of the activity.
   * @return A stopwatch for a child activity.
   */
  def startChild(name: String): HierarchicalStopwatch = {
    if (isEnabled) {
      new HierarchicalStopwatch(name, System.nanoTime(), true, Some(this))
    } else {
      this
    }
  }

  /**
   * Creates a stopwatch for a child activity. This stopwatch must be completed manually. If this stopwatch is
   * disabled, then a fresh, enabled stopwatch is returned.
   * @param name The name of the activity.
   * @return A stopwatch for a child activity.
   */
  def startChildOrNew(name: String): HierarchicalStopwatch = {
    new HierarchicalStopwatch(name, System.nanoTime(), true, if (isEnabled) Some(this) else None)
  }

  /**
   * Runs a child activity.
   * @param name The name of the child activity.
   * @param activity The child activity to run.
   * @tparam A The type of value returned by the child activity.
   * @return The child activity's return value.
   */
  def child[A](name: String, activity: HierarchicalStopwatch => A): A = {
    if (isEnabled) {
      val childStopwatch = startChild(name)
      val result = activity(childStopwatch)
      childStopwatch.complete()
      result
    } else {
      activity(this)
    }
  }

  /**
   * Runs a child activity.
   * @param name The name of the child activity.
   * @param activity The child activity to run.
   * @tparam A The type of value returned by the child activity.
   * @return The child activity's return value.
   */
  def child[A](name: String, activity: => A): A = child(name, (_: HierarchicalStopwatch) => activity)

  /**
   * Completes the activity measured by this stopwatch.
   * @return A measurement of how long the activity took.
   */
  def complete(): MeasuredActivity = {
    val duration = elapsedNanoseconds
    val mergedChildren = finishedChildren.groupBy(c => c.name).mapValues(v => v.reduce(_.union(_))).values.toSeq
    val measurement = MeasuredActivity(name, 1, duration, mergedChildren)
    parent match {
      case Some(p) if isEnabled => p.finishedChildren.append(measurement)
      case _ =>
    }
    measurement
  }
}

object HierarchicalStopwatch {
  def start(name: String) = new HierarchicalStopwatch(name, System.nanoTime(), true, None)

  /**
   * A disabled stopwatch that can be used when activities need not be timed.
   */
  val disabled = new HierarchicalStopwatch("", System.nanoTime(), false, None)
}
