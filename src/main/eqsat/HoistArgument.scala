package eqsat

import cGen.CLambda
import core.util.Log
import core._

/**
 * A class of searcher/applier that hoists arguments out of e-nodes, turning the arguments into let bindings.
 * Symbolically, the hoister performs the following transformation:
 * `F(..., arg, ...) → (\ F(shift(...), p_0, shift(...))) arg`,
 * where `arg` is any expression that matches `argPattern`.
 */
final case class HoistArgument(argPattern: Searcher,
                               override val isEligible: ENodeT => Boolean = Hoist.isNoFunctionCallOrLambda)
                              (override implicit val buildLambda: (ParamDef, Expr) => LambdaT = (p, d) => CLambda(p, d)) extends Hoist {

  override protected def argInAbstraction[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                             (rule: Rewrite,
                                                                              matchInfo: HoistMatch[EClass],
                                                                              stopwatch: HierarchicalStopwatch): Expr = {
    val arg = matchInfo.eNode.args(matchInfo.argIndex)
    DeBruijnParamUse(0, arg.t)
  }

  override protected def argInApplication[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                             (rule: Rewrite,
                                                                              matchInfo: HoistMatch[EClass],
                                                                              stopwatch: HierarchicalStopwatch): Expr = {
    matchInfo.eNode.args(matchInfo.argIndex)
  }
}
