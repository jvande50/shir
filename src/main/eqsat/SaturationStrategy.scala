package eqsat

trait RewriteEngine {
  /**
   * Applies a set of rewrite rules in batch.
   * @param rules The rules to apply.
   * @return The set of classes that were changed by applying the rules.
   */
  def applyRules(rules: Seq[Rewrite]): Set[EClassT]
}

trait SaturationStrategy {
  /**
   * Runs a single iteration of this saturation strategy.
   * @param iteration The index of the iteration being run.
   * @param engine A rewrite engine that can execute the strategy.
   * @return `true` if the saturation strategy may have more work to do; otherwise, `false`.
   */
  def runIteration(iteration: Int, engine: RewriteEngine): Boolean
}

/**
 * A saturation strategy that applies a single set of rewrite rules on every iteration.
 * @param rules The rewrite rules to apply.
 */
case class SimpleSaturationStrategy(rules: Seq[Rewrite]) extends SaturationStrategy {
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = engine.applyRules(rules).nonEmpty
}

/**
 * A saturation strategy that computes the set of rewrite rules to apply for each iteration.
 * @param rules Computes the rewrite rules to apply.
 */
case class ComputedSaturationStrategy(rules: Int => Seq[Rewrite]) extends SaturationStrategy {
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean =
    engine.applyRules(rules(iteration)).nonEmpty
}

/**
 * A saturation strategy that, on every iteration, runs an expansion strategy once and runs a simplification strategy
 * until a fixpoint is reached.
 * @param expansionStrategy An expansion strategy to run once on every iteration.
 * @param simplificationStrategy A simplification strategy to run on every iteration until a fixpoint is reached.
 */
case class BimodalSaturationStrategy(expansionStrategy: SaturationStrategy,
                                     simplificationStrategy: SaturationStrategy) extends SaturationStrategy {
  /**
   * Applies the simplification strategy until a fixpoint is reached.
   */
  private def simplify(iteration: Int, engine: RewriteEngine): Boolean = {
    var anyChanges = simplificationStrategy.runIteration(iteration, engine)
    if (anyChanges) {
      while (anyChanges) {
        anyChanges = simplificationStrategy.runIteration(iteration, engine)
      }
      true
    } else {
      false
    }
  }

  /**
   * Runs a single iteration of this saturation strategy.
   *
   * @param iteration The index of the iteration being run.
   * @param engine    A rewrite engine that can execute the strategy.
   * @return `true` if the saturation strategy may have more work to do; otherwise, `false`.
   */
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    if (iteration == 0) {
      // Simplify before expanding on the first iteration.
      simplify(iteration, engine)
    }
    expansionStrategy.runIteration(iteration, engine) | simplify(iteration, engine)
  }
}
