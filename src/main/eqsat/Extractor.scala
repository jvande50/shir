package eqsat

import core.Expr

/**
 * An extractor produces concrete expressions from e-classes.
 */
trait Extractor {
  /**
   * Attempts to recover a concrete expression from an e-class.
   * @param eClass The e-class to extract an expression from.
   * @return An expression equivalent to `eClass` or `None`.
   */
  def apply(eClass: EClassT): Option[Expr]
}

object Extractor {
  /**
   * The default extractor, which produces smallest expressions.
   */
  object DefaultSmallest extends Extractor {
    override def apply(eClass: EClassT): Option[Expr] = {
      if (eClass.hasAnalysisResult(ExtractionAnalysis.smallestExpressionExtractor)) {
        Some(eClass.analysisResult(ExtractionAnalysis.smallestExpressionExtractor).get._1)
      } else {
        DirectExtractor(CostFunction.Size, _ => true, eClass.reachable).apply(eClass)
      }
    }
  }
}

/**
 * An extractor that produces concrete expressions from e-classes by solely by selecting specific e-nodes for relevant
 * e-classes. Hence, every expression produced by this extractor is assembled directly from e-nodes in the graph,
 * with no additions.
 */
trait SelectingExtractor extends Extractor {
  /**
   * Selects an e-node for `eClass` and for each of that e-node's arguments. This process is recursive: one e-node will
   * be selected for each e-class required to construct a full expression from `eClass`. If this full expression
   * cannot be constructed, an incomplete mapping of e-classes to e-nodes is returned.
   * @param eClass An e-class to extract an expression from.
   * @return A mapping of e-classes to the e-nodes that have been selected for them.
   */
  def select(eClass: EClassT): PartialFunction[EClassT, ENodeT]

  override def apply(eClass: EClassT): Option[Expr] = {
    SelectingExtractor.selectionToExpr(eClass, select(eClass))
  }
}

object SelectingExtractor {
  def selectionToExpr(eClass: EClassT, selection: PartialFunction[EClassT, ENodeT]): Option[Expr] = {
    selection.lift(eClass) match {
      case Some(node) =>
        val args = node.args.map(selectionToExpr(_, selection))
        val foundArgs = args.collect({ case Some(v) => v })
        if (args.length == foundArgs.length) {
          Some(node.instantiate(foundArgs))
        } else {
          None
        }
      case None => None
    }
  }
}
