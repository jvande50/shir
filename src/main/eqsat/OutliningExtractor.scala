package eqsat
import cGen.CLambda
import core._

import scala.collection.mutable

/**
 * An extractor that outlines common subexpressions into function definitions that are called from extracted
 * expressions.
 * @param extractor A selecting extractor that is used to determine which e-nodes are extracted.
 */
case class OutliningExtractor(extractor: SelectingExtractor,
                              isWorthOutlining: Expr => Boolean = !_.isInstanceOf[DeBruijnParamUse])
                             (implicit buildLambda: (ParamDef, Expr) => LambdaT = (p, body) => CLambda(p, body),
                              buildFunctionCall: (Expr, Expr) => Expr = FunctionCall(_, _)) extends Extractor {
  private case class AnonymousOutlinedFunction(arguments: Seq[DeBruijnParamUse], body: Expr) {
    val function: Expr = {
      arguments.foldLeft(body)((body, arg) => DeBruijnLambda(arg.t, body))
    }
  }

  private case class OutlinedFunction(name: ParamDef, anonymous: AnonymousOutlinedFunction) {
    val function: Expr = anonymous.function

    def arguments: Seq[DeBruijnParamUse] = anonymous.arguments

    val call: Expr = {
      arguments.foldLeft(ParamUse(name): Expr)((f, arg) => buildFunctionCall(f, arg))
    }

    def bind(expr: Expr): Expr = {
      val func = function
      buildFunctionCall(buildLambda(name, DeBruijnParamUse.incrementUnbound(expr)), func)
    }
  }

  private class Outliner(selection: PartialFunction[EClassT, ENodeT]) {
    private val choicesPerClass: mutable.Map[EClassT, ENodeT] = mutable.Map[EClassT, ENodeT]()
    private val extractionCounts: mutable.Map[EClassT, Int] = mutable.Map[EClassT, Int]()

    def inspect(eClass: EClassT): Unit = {
      extractionCounts.get(eClass) match {
        case Some(count) =>
          extractionCounts(eClass) = count + 1

        case None =>
          extractionCounts(eClass) = 1
          val node = selection(eClass)
          choicesPerClass(eClass) = node
          for (arg <- node.args) {
            inspect(arg)
          }
      }
    }

    private class TreeBuilder {
      val outlinedFunctions: mutable.LinkedHashMap[EClassT, OutlinedFunction] = mutable.LinkedHashMap[EClassT, OutlinedFunction]()

      private def incrementAll(uses: Seq[DeBruijnParamUse], amount: Int): Seq[DeBruijnParamUse] = {
        uses.collect({
          case use: DeBruijnParamUse if use.index + amount >= 0 => DeBruijnParamUse(use.index + amount, use.t)
        })
      }

      private def collectArgs(expr: ShirIR): Seq[DeBruijnParamUse] = {
        expr match {
          case p: DeBruijnParamUse => Seq(p)
          case lambda: LambdaT => incrementAll(collectArgs(lambda.body), -1)
          case DeBruijnShift(expr, _) => incrementAll(collectArgs(expr), 1)
          case _ => expr.children.flatMap(collectArgs)
        }
      }

      private def renumberArgs(expr: ShirIR, argRenumbering: Map[Int, Int]): ShirIR = {
        expr match {
          case p: DeBruijnParamUse =>
            argRenumbering.get(p.index) match {
              case Some(newIndex) => DeBruijnParamUse(newIndex, p.t)
              case None => p
            }

          case lambda: LambdaT =>
            val newNumbering = argRenumbering.map(p => (p._1 + 1, p._2 + 1))
            lambda.build(lambda.children.map(renumberArgs(_, newNumbering)))

          case DeBruijnShift(expr, _) =>
            val newNumbering = argRenumbering.map(p => (p._1 - 1, p._2 - 1))
            DeBruijnShift(renumberArgs(expr, newNumbering).asInstanceOf[Expr])

          case _ => expr.build(expr.children.map(renumberArgs(_, argRenumbering)))
        }
      }

      private def outline(expr: Expr): OutlinedFunction = {
        val args = collectArgs(expr).filter(p => p.index >= 0)
        val argRenumbering = args.map(_.index).reverse.zipWithIndex.toMap
        val exprWithRenumberedArgs = renumberArgs(expr, argRenumbering)
        val anon = AnonymousOutlinedFunction(args, exprWithRenumberedArgs.asInstanceOf[Expr])
        OutlinedFunction(ParamDef(anon.function.t), anon)
      }

      private def extract(eClass: EClassT): Expr = {
        val node = choicesPerClass(eClass)
        node.instantiate(node.args.map(toExpr))
      }

      def toExpr(eClass: EClassT): Expr = {
        extractionCounts.get(eClass) match {
          case Some(i) if i > 1 =>
            outlinedFunctions.get(eClass) match {
              case Some(f) => f.call
              case None =>
                val expr = extract(eClass)
                if (isWorthOutlining(expr)) {
                  val f = outline(expr)
                  outlinedFunctions(eClass) = f
                  f.call
                } else {
                  expr
                }
            }

          case _ => extract(eClass)
        }
      }
    }

    def toExpr(eClass: EClassT): Expr = {
      val builder = new TreeBuilder()
      val extracted = builder.toExpr(eClass)
      val functions = builder.outlinedFunctions.values.toSeq.reverse
      val bound = functions.foldLeft(extracted)((expr, func) => func.bind(expr))
      DeBruijnTransform.forward(bound)
    }
  }

  override def apply(eClass: EClassT): Option[Expr] = {
    // This extractor relies on a three-step process. First, it recursively chooses one node for each e-class in
    // a full expression tree. Then, common expressions in that tree are outlined into function definitions.
    // Finally, the extractor synthesizes an expression tree that includes calls to the outlined functions.
    val outliner = new Outliner(extractor.select(eClass))
    outliner.inspect(eClass)
    Some(outliner.toExpr(eClass))
  }
}
