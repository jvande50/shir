package eqsat

import core.{Expr, LambdaT, ParamDef}

object BoundVariables {
  /**
   * Finds all bound variables in an expression.
   * @param expr The expression to examine.
   * @return The set of all variables defined by the expression.
   */
  def findBoundVars(expr: Expr): Set[ParamDef] = {
    expr match {
      case lambda: LambdaT => Set(lambda.param) ++ findBoundVars(lambda.body)
      case _ => expr
        .children
        .collect({ case e: Expr => e })
        .map(findBoundVars)
        .foldLeft(Set[ParamDef]())(_ ++ _)
    }
  }

  /**
   * Renames all mentions of a parameter in an expression.
   * @param from The parameter to rename.
   * @param to The parameter to replace `from` with.
   * @param expr An expression in which `from` is to be replaced with `to`.
   * @return An expression in which `from` has been replaced with `to`.
   */
  def rename(from: ParamDef, to: ParamDef)(expr: Expr): Expr = {
    expr.visitAndRebuild({
      case p: ParamDef if p.id == from.id => to
      case other => other
    }).asInstanceOf[Expr]
  }
}
