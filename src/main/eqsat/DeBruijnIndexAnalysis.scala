package eqsat

import core.{LambdaT, Type}

/**
 * An analysis that computes the set of all De Bruijn parameters used in any given e-class.
 */
object DeBruijnIndexAnalysis extends Analysis[Seq[DeBruijnParamUse]] {
  /**
   * The analysis' unique identifier.
   */
  override val identifier: String = "de-bruijn-indices"

  private def toPair(use: DeBruijnParamUse): (Int, Type) = {
    (use.index, use.t)
  }

  private def fromPair(pair: (Int, Type)): DeBruijnParamUse = {
    DeBruijnParamUse(pair._1, pair._2)
  }

  private def distinct(values: Seq[DeBruijnParamUse]): Seq[DeBruijnParamUse] = {
    values.map(toPair).distinct.map(fromPair)
  }

  private def intersection(xs: Seq[DeBruijnParamUse], ys: Seq[DeBruijnParamUse]): Seq[DeBruijnParamUse] = {
    xs.map(toPair).toSet.intersect(ys.map(toPair).toSet).map(fromPair).toSeq
  }

  /**
   * Constructs a new analysis result for a newly created singleton e-class.
   *
   * @param node The node in the singleton e-class.
   * @return An analysis result for the singleton e-class containing `node`.
   */
  override def make(node: ENodeT): Seq[DeBruijnParamUse] = {
    val argResults = distinct(node.args.flatMap(_.analysisResult(this)))
    node.expr match {
      case use: DeBruijnParamUse => Seq(use)
      case _: LambdaT => argResults.collect({
        case use if use.index > 0 => use.decrementIndex(1)
      })
      case shift: DeBruijnShiftT => argResults.collect({
        case use if use.index - shift.shiftAmount >= 0 => use.decrementIndex(shift.shiftAmount)
      })
      case _ => argResults
    }
  }

  /**
   * When two e-classes are merged, join their analysis results into a new analysis result for the merged e-class.
   *
   * @param left  The analysis result of the first e-class being merged.
   * @param right The analysis result of the second e-class being merged.
   * @return A new analysis result for the merged e-class.
   */
  override def join(left: Seq[DeBruijnParamUse], right: Seq[DeBruijnParamUse]): Seq[DeBruijnParamUse] =
    intersection(left, right)

  /**
   * Optionally modify an e-class based on its analysis result. Calling `modify` on the same e-class more than once
   * must produce the same result as calling it only once.
   *
   * @param graph          The graph that defines `eClass`.
   * @param eClass         The e-class to potentially modify.
   * @param analysisResult This analysis' result for `eClass`.
   */
  override def modify[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                         (eClass: EClass, analysisResult: Seq[DeBruijnParamUse]): Unit = ()
}
