package eqsat

import core.{Expr, LambdaT, ParamDef}

import scala.collection.mutable

/**
 * An extractor that does not rely on e-graph analyses.
 * @param costFunction The cost function to use.
 * @param maySelect A function that determines whether a node is eligible for extraction or not.
 */
final case class DirectExtractor[C](costFunction: CostFunction[C], maySelect: ENodeT => Boolean, classes: Seq[EClassT])
                                   (implicit num: Numeric[C]) extends Extractor {
  private def seqOfOptionToOptionOfList[A](values: Seq[Option[A]]): Option[List[A]] = {
    values
      .foldLeft[Option[List[A]]](Some(Nil))({
        case (None, _) | (_, None) => None
        case (Some(xs), Some(x)) => Some(x :: xs)
      })
      .map(_.reverse)
  }

  // TODO: make sure that we do not select undefined parameters.
  def apply(root: EClassT): Option[Expr] = costs.get(root) match {
    case None => None
    case Some((_, node)) =>
      seqOfOptionToOptionOfList(node.args.map(apply)) match {
        case None => None
        case Some(args) =>
          val result = ExprHelpers.buildFromArgs(node.expr, args)
          result match {
            case _: DeBruijnLambda => Some(result)
            case lambda: LambdaT => Some(BoundVariables.rename(lambda.param, ParamDef(lambda.param.t))(lambda))
            case _ => Some(result)
          }
      }
  }

  val costs: Map[EClassT, (C, ENodeT)] = {
    val costs = mutable.HashMap[EClassT, (C, ENodeT)]()
    var didSomething = true
    while (didSomething) {
      didSomething = false
      for (eClass <- classes) {
        val nodesAndCosts = eClass
          .nodes
          .filter(maySelect)
          .map(n => (n, seqOfOptionToOptionOfList(n.args.map(costs.get))))
          .collect({ case (n, Some(cs)) => (n, cs.map(_._1)) })

        if (nodesAndCosts.nonEmpty) {
          val cheapestNode = nodesAndCosts.minBy(t => costFunction(t._1.expr, t._2))

          val cost = costFunction(cheapestNode._1.expr, cheapestNode._2)
          costs.get(eClass) match {
            case None =>
              costs.put(eClass, (cost, cheapestNode._1))
              didSomething = true

            case Some((oldCost, _)) if num.lt(cost, oldCost) =>
              costs.put(eClass, (cost, cheapestNode._1))
              didSomething = true

            case _ =>
          }
        }
      }
    }
    costs.toMap
  }
}