package eqsat

/**
 * An observer for program equivalence graphs.
 */
trait EGraphObserver {
  /**
   * Notes that a rule has been applied to a graph.
   * @param graph The graph to which `rule` was applied.
   * @param rule The rule that was applied to `graph`.
   * @param duration A measurement of the wall-clock time that elapsed while `rule` was applied to `graph`.
   */
  def onApply[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                 (rule: Rewrite, duration: MeasuredActivity): Unit = ()

  /**
   * Notes that a rule has considered an e-class.
   * @param graph The graph that contains the e-class.
   * @param rule The rule that was considering `eClass`.
   * @param duration A measurement of the wall-clock time elapsed while `eClass` was being considered.
   */
  def onSearch[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                  (rule: Rewrite, duration: MeasuredActivity): Unit = ()

  /**
   * Notes that the graph is about to perform a step in the Equality Saturation algorithm.
   * @param graph The graph that is about to take a step
   */
  def onSaturateStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = ()

  def onFinish[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = ()
}

/**
 * A PEG observer that does nothing.
 */
object EGraphNullObserver extends EGraphObserver

/**
 * A PEG observer that is a composition of other observers.
 * @param observers A sequence of observers of which each element is notified whenever an event occurs.
 */
case class EGraphCompositeObserver(observers: EGraphObserver*) extends EGraphObserver {
  override def onApply[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                          (rule: Rewrite, duration: MeasuredActivity): Unit = {
    for (obs <- observers) {
      obs.onApply(graph)(rule, duration)
    }
  }

  override def onSearch[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                           (rule: Rewrite, duration: MeasuredActivity): Unit = {
    for (obs <- observers) {
      obs.onSearch(graph)(rule, duration)
    }
  }

  override def onSaturateStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    for (obs <- observers) {
      obs.onSaturateStep(graph)
    }
  }

  override def onFinish[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    for (obs <- observers) {
      obs.onFinish(graph)
    }
  }
}

/**
 * An applier that triggers a PEG observer.
 * @param observer The observer that is triggered upon rule activation.
 */
case class ObservedRuleApplier(observer: EGraphObserver) extends Applier {
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {
    val childStopwatch = stopwatch.startChildOrNew(rule.name)

    // Apply the rule.
    val changed = rule.applier.apply(eGraph)(rule, matchInfo, childStopwatch)

    // Notify the observer.
    observer.onApply(eGraph)(rule, childStopwatch.complete())

    // Return the set of changed e-classes.
    changed
  }
}

/**
 * A searcher that triggers a PEG observer.
 * @param rule The rewrite rule that is used.
 */
case class ObservedRuleSearcher(rule: Rewrite, observer: EGraphObserver, batchSearch: Boolean = true) extends Searcher {
  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                               (eClass: EClass,
                                                                stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {
    val childStopwatch = stopwatch.startChildOrNew(rule.name)

    // Apply the rule.
    val result = rule.searcher.searchEClass(eGraph)(eClass, childStopwatch)

    // Notify the observer.
    observer.onSearch(eGraph)(rule, childStopwatch.complete())

    // Return the set of changed e-classes.
    result
  }

  override def search[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass],
                                                          createStopwatch: () => HierarchicalStopwatch): Seq[Match[EClass]] = {
    if (!batchSearch) {
      return super.search(eGraph, createStopwatch)
    }

    val childStopwatch = createStopwatch().startChildOrNew(rule.name)

    // Apply the rule.
    val result = rule.searcher.search(eGraph, childStopwatch)

    // Notify the observer.
    observer.onSearch(eGraph)(rule, childStopwatch.complete())

    // Return the set of changed e-classes.
    result
  }
}
