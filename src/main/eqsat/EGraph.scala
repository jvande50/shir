package eqsat

import eqsat.explain.ExplanationService
import core.{BuiltinExpr, Expr, ParamDef, Type}

import scala.collection.mutable

/**
 * An e-graph: a data structure that stores an equivalence relation over expressions.
 *
 * @tparam ENode The type of e-nodes in the graph.
 * @tparam EClass The type of e-classes in the graph.
 */
trait EGraph[ENode <: ENodeT, EClass <: EClassT] {
  /**
   * Gets the set of classes in this e-graph.
   *
   * @return The set of classes in this e-graph.
   */
  def classes: Seq[EClass]

  /**
   * Adds an expression tree to this graph. The tree's descendants are recursively converted to e-nodes. They may be
   * unified with existing e-nodes/e-classes in the graph. If e-classes are included in the tree, then they are
   * canonicalized.
   * @param expr An expression to add to the graph.
   * @param reason The reason why nodes are added.
   * @return The e-class of the expression in the tree.
   */
  def add(expr: Expr, reason: NodeReason): EClass

  /**
   * Merges two e-classes. This indicates that `left` and `right` produce the exact same value.
   * @param left The first e-class to merge.
   * @param right The second e-class to merge.
   * @param reason The reason why classes are being merged.
   * @return `true` if merging the e-classes changed the graph; otherwise, `false`.
   */
  def merge(left: EClass, right: EClass, reason: NodeReason): Boolean

  /**
   * Adds an expression to this e-graph and merges it with an e-class.
   * @param expr The expression to add.
   * @param eClass The e-class to merge with `expr`.
   * @param reason The reason why nodes are added.
   */
  def addAndMerge(expr: Expr, eClass: EClass, reason: NodeReason): Boolean = {
    merge(eClass, add(expr), reason)
  }

  /**
   * Adds an expression to this e-graph and merges it with an e-class.
   * @param expr The expression to add.
   * @param eClass The e-class to merge with `expr`.
   * @param rule The rule that prompted `expr` to be added.
   * @param matchInfo The rule match that prompted `expr` to be added.
   */
  def addAndMerge(expr: Expr, eClass: EClass, rule: Rewrite, matchInfo: Match[EClass]): Boolean = {
    addAndMerge(expr, eClass, AddedByRewrite(rule, eClass, matchInfo))
  }

  /**
   * Rebuilds the e-graph's invariants, which may be temporarily broken during rule application.
   */
  def rebuild(): Unit

  /**
   * Adds an expression tree to this graph. The tree's descendants are recursively converted to e-nodes. They may be
   * unified with existing e-nodes/e-classes in the graph. If e-classes are included in the tree, then they are left
   * untouched.
   * @param expr An expression to add to the graph.
   * @return The e-class of the expression in the tree.
   */
  def add(expr: Expr): EClass = add(expr, DirectlyAdded)

  /**
   * Canonicalizes an e-class.
   * @param eClass The e-class to canonicalize.
   * @return The canonical version of `eClass`.
   */
  def find(eClass: EClass): EClass = add(eClass, NoReason)

  /**
   * The e-node explanation service, if one is configured for this e-graph.
   * @return An explanation service.
   */
  def explainer: Option[ExplanationService[ENode]]

  /**
   * Computes an analysis' results for the entire graph.
   * @param analysis The analysis to compute.
   * @tparam A The type of the analysis' results.
   */
  def recomputeAnalysis[A](analysis: Analysis[A]): Unit

  /**
   * The default saturation engine for this e-graph.
   * @return A saturation engine.
   */
  def defaultSaturationEngine: SaturationEngine[ENode, EClass] = SaturationEngine(this)

  /**
   * Applies rewrite rules until a fixpoint is reached.
   * @param rewrites The set of rewrite rules to apply.
   * @return The number of iterations performed by the saturation engine.
   */
  final def saturate(rewrites: Seq[Rewrite]): Int =
    defaultSaturationEngine.saturate(rewrites)

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param rewrites The set of rewrite rules to apply.
   * @param shouldTerminate An optional termination criterion that takes the number of saturation steps thus far and
   *                        returns a Boolean that indicates if saturation should terminate.
   * @return The number of iterations performed by the saturation engine.
   */
  final def saturate(rewrites: Seq[Rewrite], shouldTerminate: Int => Boolean): Int =
    defaultSaturationEngine.saturate(rewrites, shouldTerminate)

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param rewrites The set of rewrite rules that apply in a given iteration.
   * @param shouldTerminate An optional termination criterion that takes the number of saturation steps thus far and
   *                        returns a Boolean that indicates if saturation should terminate.
   * @return The number of iterations performed by the saturation engine.
   */
  final def saturate(rewrites: Int => Seq[Rewrite], shouldTerminate: Int => Boolean): Int =
    defaultSaturationEngine.saturate(rewrites, shouldTerminate)

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param strategy A saturation strategy.
   * @param shouldTerminate An optional termination criterion that takes the number of saturation steps thus far and
   *                        returns a Boolean that indicates if saturation should terminate.
   * @return The number of iterations performed by the saturation engine.
   */
  final def saturate(strategy: SaturationStrategy, shouldTerminate: Int => Boolean): Int =
    defaultSaturationEngine.saturate(strategy, shouldTerminate)
}

object EGraph {
  /**
   * Creates an instance of the default e-graph implementation.
   * @param analyses The set of analyses to equip the e-graph with.
   * @return An empty e-graph.
   */
  def apply(analyses: Analyses = Analyses(Seq()),
            observer: EGraphObserver = EGraphNullObserver): DeferredMergingEGraph[HashConsENode, HashConsEClass] = {
    new DeferredMergingEGraph(
      new Willsey2021EGraph(
        analyses = analyses,
        observer = observer))
  }
}

/**
 * An expression that identifies an e-class in a PEG.
 */
trait EClassT extends BuiltinExpr {
  /**
   * Gets the set of e-nodes in this e-class.
   * @return A set of e-nodes.
   */
  def nodes: Seq[ENodeT]

  /**
   * Gets the analysis result for a particular analysis.
   * @param analysis The analysis to query for a result.
   * @tparam A The type of the analysis' result.
   * @return The analysis' result for this e-class.
   */
  def analysisResult[A](analysis: Analysis[A]): A

  /**
   * Checks if an analysis' result has been computed for this e-class.
   * @param analysis The analysis to query for a result.
   * @tparam A The type of the analysis' result.
   * @return `true` if `analysis` has computed its result for this e-class; otherwise, `false`.
   */
  def hasAnalysisResult[A](analysis: Analysis[A]): Boolean

  /**
   * Gets all e-classes that are reachable from this e-class.
   * @return A sequence of e-classes.
   */
  def reachable: Seq[EClassT] = {
    val found = mutable.HashSet[EClassT]()
    val worklist = mutable.Queue[EClassT](this)
    while (worklist.nonEmpty) {
      val elem = worklist.dequeue()
      if (found.add(elem)) {
        for (node <- elem.nodes) {
          worklist ++= node.args.filterNot(found.contains)
        }
      }
    }
    found.toSeq
  }

  /**
   * Turns this e-class into a minimal expression with an additional requirement that only some e-nodes are eligible
   * for inclusion in the resulting expression.
   * @param costFunction A cost function for expressions.
   * @param mayInclude A predicate that determines if an e-node may be used to construct the resulting expression.
   * @return An expression that minimizes `costFunction` and satisfies `mayInclude`, if one exists; otherwise, `None`.
   */
  def toMinimalExpr[A](costFunction: CostFunction[A], mayInclude: ENodeT => Boolean)
                      (implicit num: Numeric[A]): Option[Expr] =
    DirectExtractor(costFunction, mayInclude, reachable).apply(this)


  /**
   * Turns this e-class into a minimal expression.
   * @param costFunction A cost function for expressions.
   * @return An expression that minimizes `costFunction`.
   */
  def toMinimalExpr[A](costFunction: CostFunction[A])(implicit num: Numeric[A]): Expr =
    toMinimalExpr(costFunction, _ => true).get

  /**
   * Turns this e-class into the smallest possible expression.
   * @return The most succinct expression corresponding to this e-class.
   */
  def toSmallestExpr: Expr = Extractor.DefaultSmallest(this).get

  /**
   * Checks if this e-class contains an expression consisting only of nodes that satisfy a predicate.
   * @param predicate A predicate that encodes a condition every node in an expression must satisfy.
   * @return `true` if there is an expression of which every node satisfies `predicate`; otherwise, `false`.
   */
  def hasExprSatisfying(predicate: ENodeT => Boolean): Boolean = toMinimalExpr(CostFunction.Size, predicate).nonEmpty

  /**
   * Computes an analysis for all e-classes that are reachable from this e-class.
   * @param analysis The analysis to compute.
   * @tparam A The analysis' result type.
   */
  def computeAnalysisForReachable[A](analysis: Analysis[A]): Map[EClassT, A] = {
    val results = mutable.Map[EClassT, A]()
    val reachable = this.reachable

    // Compute each class' parents.
    val parents = mutable.HashMap[EClassT, Set[EClassT]]()
    for (eClass <- reachable) {
      for (node <- eClass.nodes) {
        for (arg <- node.args) {
          parents(arg) = parents.getOrElse(arg, Set()) + eClass
        }
      }
    }

    // Compute analyses until we reach a fixpoint.
    var worklist = mutable.ArrayBuffer[EClassT]()
    worklist ++= reachable
    while (worklist.nonEmpty) {
      val newWorklist = mutable.ArrayBuffer[EClassT]()
      for (eClass <- reachable) {
        val eligibleNodes = eClass.nodes.filter(_.args.forall(results.contains))
        val nodeResults = eligibleNodes.map(analysis.make)
        if (nodeResults.nonEmpty) {
          val classResult = nodeResults.reduce(analysis.join)
          if (!results.contains(eClass) || classResult != results(eClass)) {
            results.put(eClass, classResult)
            newWorklist ++= parents.getOrElse(eClass, Set())
          }
        }
      }
      worklist = newWorklist
    }

    results.toMap
  }
}

/**
 * A node in a PEG. Equivalent e-nodes are grouped into e-classes.
 */
trait ENodeT {
  /**
   * The e-node's internal value, represented as an expression.
   */
  val expr: Expr

  /**
   * The e-node's type.
   */
  val t: Type = expr.t

  /**
   * The e-node's arguments, as e-classes.
   * @return A sequence of e-classes.
   */
  def args: Seq[EClassT]

  /**
   * Gets all parameter-valued arguments or expression children.
   * @return A sequence of parameter definitions.
   */
  def params: Seq[ParamDef] = ExprHelpers.params(expr)

  /**
   * Tells if this e-node is a leaf node, i.e., a node with no arguments.
   * @return `true` if this e-node has an empty argument list; otherwise, `false`.
   */
  final def isLeaf: Boolean = args.isEmpty

  /**
   * Tells if this e-node has the same operator as another e-node. Two e-nodes are equal if and only if their
   * operators and their argument lists are the same.
   * @param other An e-node to compare this e-node to.
   * @return `true` if this e-node has the same operator as `other`; otherwise, `false`.
   */
  final def matches(other: ENodeT): Boolean = {
    args.length == other.args.length &&
      args.zip(other.args).forall(t => t._1.t == t._2.t) &&
      ExprHelpers.isSameOp(expr, other.expr)
  }

  /**
   * Instantiates this e-node with a sequence of expression, each of which corresponds to an e-class argument to the
   * node.
   * @param args A sequence of expression arguments.
   * @return An instantiated version of the e-node.
   */
  final def instantiate(args: Seq[Expr]): Expr = ExprHelpers.buildFromArgs(expr, args)

  /**
   * Turns this e-node into the smallest possible expression.
   *
   * @return The most succinct expression corresponding to this e-node.
   */
  final def toSmallestExpr: Expr = instantiate(args.map(_.toSmallestExpr))
}
