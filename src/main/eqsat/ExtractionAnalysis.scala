package eqsat

import core.Expr

import scala.math.Numeric.BigIntIsIntegral

/**
 * An analysis that efficiently computes and stores the optimal extraction for every e-class in a PEG.
 * @param identifier The analysis' unique identifier.
 * @param cost A function that computes the cost of a node when instantiated with a sequence of arguments that each
 *             have a precomputed cost.
 * @param mayInclude A function that determines if an expression is eligible for extraction.
 */
case class ExtractionAnalysis[A](override val identifier: String,
                                 cost: CostFunction[A],
                                 mayInclude: Expr => Boolean)
                                (implicit num: Numeric[A])
  extends Analysis[Option[(Expr, A, Int)]] {

  /**
   * Gets an extractor that relies on this analysis.
   * @return An extractor.
   */
  def extractor: SelectingExtractor = new SelectingExtractor {
    override def select(eClass: EClassT): PartialFunction[EClassT, ENodeT] = Function.unlift(cheapest)
    override def apply(eClass: EClassT): Option[Expr] = eClass.analysisResult(ExtractionAnalysis.this).map(_._1)
  }

  /**
   * Computes the total cost of an expression tree.
   * @param expr The expression whose total cost is to be computed.
   */
  def totalCost(expr: Expr): A = cost(expr, ExprHelpers.args(expr).map(totalCost))

  /**
   * Creates a variant of this extraction analysis that excludes certain expressions.
   * @param identifier The name of the extraction analysis variant.
   * @param shouldExclude Determines which expressions may not be selected.
   * @return A variant of this analysis.
   */
  def exclude(identifier: String, shouldExclude: Expr => Boolean): ExtractionAnalysis[A] = {
    ExtractionAnalysis(identifier, cost, mayInclude = expr => mayInclude(expr) && !shouldExclude(expr))(num)
  }

  /**
   * Constructs a new analysis result for a newly created singleton e-class.
   *
   * @param node  The node in the singleton e-class.
   * @return An analysis result for the singleton e-class containing `node`.
   */
  override def make(node: ENodeT): Option[(Expr, A, Int)] = {
    if (!mayInclude(node.expr)) {
      None
    } else {
      val argResults = node.args.map(_.analysisResult(this))
      if (argResults.exists(_.isEmpty)) {
        None
      } else {
        val expr = ExprHelpers.buildFromArgs(node.expr, argResults.map(_.get._1))
        Some(expr, cost(node.expr, argResults.map(_.get._2)), argResults.map(_.get._3).sum + 1)
      }
    }
  }

  /**
   * When two e-classes are merged, join their analysis results into a new analysis result for the merged e-class.
   *
   * @param left  The analysis result of the first e-class being merged.
   * @param right The analysis result of the second e-class being merged.
   * @return A new analysis result for the merged e-class.
   */
  override def join(left: Option[(Expr, A, Int)], right: Option[(Expr, A, Int)]): Option[(Expr, A, Int)] = {
    (left, right) match {
      case (None, None) => None
      case (None, Some(p)) => Some(p)
      case (Some(p), None) => Some(p)
      case (Some((e1, c1, s1)), Some((_, c2, _))) if num.lt(c1, c2) => Some(e1, c1, s1)
      case (Some((_, c1, _)), Some((e2, c2, s2))) if num.gt(c1, c2) => Some(e2, c2, s2)
      case (Some((e1, c1, s1)), Some((_, _, s2))) if s1 <= s2 => Some(e1, c1, s1)
      case (Some(_), Some(p)) => Some(p)
    }
  }

  /**
   * Optionally modify an e-class based on its analysis result. Calling `modify` on the same e-class more than once
   * must produce the same result as calling it only once.
   *
   * @param graph          The graph that defines `eClass`.
   * @param eClass         The e-class to potentially modify.
   * @param analysisResult This analysis' result for `eClass`.
   */
  override def modify[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                         (eClass: EClass,
                                                          analysisResult: Option[(Expr, A, Int)]): Unit = ()

  override def areEquivalent(left: Option[(Expr, A, Int)], right: Option[(Expr, A, Int)]): Boolean = {
    (left, right) match {
      case (None, None) => true
      case (None, Some(_)) => false
      case (Some(_), None) => false
      case (Some((_, c1, s1)), Some((_, c2, s2))) => num.equiv(c1, c2) && s1 == s2
    }
  }

  /**
   * Gets the lowest-cost node in an e-class, assuming this analysis has already been computed on the e-class' nodes'
   * arguments.
   * @param eClass An e-class to examine.
   * @return The lowest-cost node in `eClass`, if an eligible node exists; otherwise, `false`.
   */
  def cheapest(eClass: EClassT): Option[ENodeT] = {
    val candidates = eClass.nodes
      .filter(n => mayInclude(n.expr))
      .map(node => {
        val argResults = node.args.map(_.analysisResult(this))
        if (argResults.exists(_.isEmpty)) {
          None
        } else {
          Some(node, cost(node.expr, argResults.map(_.get._2)))
        }
      })
      .collect({
        case Some((node, c)) => (node, c)
      })

    if (candidates.isEmpty) {
      None
    } else {
      Some(candidates.minBy(t => t._2)._1)
    }
  }
}

object ExtractionAnalysis {
  /**
   * An extraction analysis that always produces the smallest expression.
   */
  val smallestExpressionExtractor: ExtractionAnalysis[BigInt] = ExtractionAnalysis(
    "smallest-expression-extractor", CostFunction.Size, _ => true)
}
