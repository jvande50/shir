package algo.torch

import algo._
import core._
import lift.arithmetic.StartFromRange
import lift.arithmetic.simplifier.SimplifyIfThenElse

/**
 * The macro Exprs in this file extend from here.
 */
private[torch] sealed trait AlgoTorchMacro extends AlgoMacroExpr {
}

/**
 * Most shapes go from outside-in (as in tensors) as opposed to the algo-expr
 * order, which is (almost always) inside-out
 */

object Input {
  def apply(ty: IntTypeT, name: String, dims: Seq[ArithTypeT]): Expr = dims match {
    case Seq() =>
      Item(Join(algo.Input(name, ty, 1, 1)))
    case Seq(d) =>
      Join(algo.Input(name, ty, d, 1))
    case Seq(d, h) =>
      algo.Input(name, ty, h, d)
    case d +: h +: tail =>
      val inner = tail.foldLeft(h.ae)(_ * _.ae)
      Map({
        val _0 = ParamDef(SeqType(ty, inner))
        AlgoLambda(_0, SplitAll(ParamUse(_0), tail))
      }, algo.Input(name, ty, inner, d))
  }
}

object Flatten {
  def apply(x: Expr, from: Int, to: Int): Expr = {
    // XXX: because PyTorch allows negatives, but we don't
    assert(from >= 0 && from <= to, "Invalid flattening indices (negatives are not allowed)")

    if (from == 0)
      Seq.range(from, to).foldLeft[Expr](x)((acc, _) => Join(acc))
    else
      Map(from, {
        val _0 = ParamDef()
        AlgoLambda(_0,
          Seq.range(from, to).foldLeft[Expr](ParamUse(_0))((acc, _) => Join(acc))
        )
      }, x)
  }
}

object Broadcast {
  /**
   * The semantics is (meant to be) the same as torch.ops.prims.broadcast_in_dim.
   *
   * It roughly works like this:
   * x has a shape. broadcastDims has the same length as x's rank. it "moves"
   * the dimensions to relevant offset defined in newShape and broadcasting if
   * the original shape had length 1. every dimension of newShape not
   * referenced by broadcastDims is magically created and repeated.
   *
   * Example:
   *   TBroadcast(x : i32[6, 1, 2], [64, 6, 4, 2], [1, 2, 3])
   * i32[1, 2] is repeated 4 times giving i32[6, 4, 2],
   * then the whole i32[6, 4, 2] is repeated 64 times giving i32[64, 6, 4, 2]
   *
   * @param x input tensor
   * @param newShape shape of the output tensor
   * @param broadcastDims specifies the relation between the dimensions, must be strictly increasing
   * @return tensor of shape newShape and element type of x.
   */
  def apply(x: Expr, newShape: Seq[Int], broadcastDims: Seq[Int]): Expr = {
    def traverse(x: Expr, newShape: Seq[Int], oldIdx: Int, broadcastDims: Seq[Int]): Expr = {
      val (outerRepeat, broadcasted) = broadcastDims match {
        case Seq() => (newShape, x)
        case newIdx +: nextBroadcastDims =>
          assert(oldIdx < newIdx, "broadcast dims must be strictly increasing")

          val t = x.t.asInstanceOf[SeqTypeT]
          val (ours, nextNewShape) = newShape.splitAt(newIdx - oldIdx)
          val inner = Map({
            val _0 = ParamDef(t.et)
            AlgoLambda(_0, traverse(ParamUse(_0), nextNewShape, newIdx, nextBroadcastDims))
          }, x)

          val oldLen = t.len.ae.evalInt
          val newLen = ours.last
          val broadcasted = if (oldLen == newLen) inner else {
            assert(oldLen == 1, "broadcasted dimension must have size 1")
            Join(Repeat(inner, newLen))
          }
          (ours.init, broadcasted)
      }
      outerRepeat.foldRight(broadcasted)((w, x) => Repeat(x, w))
    }

    val xtc = TypeChecker.checkIfUnknown(x, x.t)
    traverse(xtc, newShape, -1, broadcastDims)
  }
}

object Clamp {
  def apply(input: Expr, min: Option[Expr], max: Option[Expr]): Expr = {
    val lo = min match {
      case Some(min) => Max2(input, min)
      case None => input
    }
    max match {
      case Some(max) => Min2(lo, max)
      case None => lo
    }
  }

  def asFunction(min: Option[Expr], max: Option[Expr]): Expr = {
    val x = ParamDef()
    AlgoLambda(x, apply(ParamUse(x), min, max))
  }
}

/**
 * MaybeTruncInt + AddInt. Lower width addition will not overflow.
 */
case class CappedAddIntExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoTorchMacro {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): CappedAddIntExpr =
    CappedAddIntExpr(newInnerIR, newTypes)

  override def expand(): Expr = {
    val CappedAddInt(input, limit, _) = this
    val ow = limit.width.ae.evalInt
    input.t match {
      case TupleType(Seq(t1: IntTypeT, t2: IntTypeT)) =>
        val t1w = t1.width.ae.evalInt
        val t2w = t2.width.ae.evalInt

        val addw = 1 + t1w.max(t2w)
        TypeChecker.check(
          if (addw <= ow)
            Add(Tuple(ResizeInteger(Select2(input, 0), addw), Select2(input, 1)))
          else
            Conversion(TruncInteger(Add(input), ow), limit)
        )
    }
  }
}

object CappedAddInt extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, limit: IntTypeT): CappedAddIntExpr = CappedAddIntExpr({
    val inputtv = TupleTypeVar(AbstractScalarTypeVar(), AbstractScalarTypeVar())
    val outtv = ConditionalTypeVar(
      inputtv,
      { case TupleType(Seq(t1: IntTypeT, t2: IntTypeT)) =>
        if (t1.kind != t2.kind || t1.kind != limit.kind)
          throw MalformedExprException(this.getClass().getName() + " does not allow mixing signed and unsigned ints")

        val addw = 1 + SimplifyIfThenElse(t1.width.ae.gt(t2.width.ae), t1.width.ae, t2.width.ae)
        val outw = SimplifyIfThenElse(addw.gt(limit.width.ae), limit.width.ae, addw)
        if (limit.kind == SignedIntTypeKind) SignedIntType(outw) else IntType(outw)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(inputtv, AlgoFunType(inputtv, outtv))
    ), input.t), input)
  }, Seq(limit))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): CappedAddIntExpr =
    apply(inputs.head, types.head.asInstanceOf[IntTypeT])

  def unapply(expr: CappedAddIntExpr): Some[(Expr, IntTypeT, Type)] =
    Some((
      expr.args.head,
      expr.types.head.asInstanceOf[IntTypeT],
      expr.t
    ))
}

/**
 * MaybeTruncInt + SubInt. Lower width subtraction will not overflow.
 *
 * XXX: mismatched input bit width is extended according to the input type.
 */
case class CappedSubIntExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoTorchMacro {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): CappedSubIntExpr =
    CappedSubIntExpr(newInnerIR, newTypes)

  override def expand(): Expr = {
    val CappedSubInt(input, limit, _) = this
    val ow = limit.width.ae.evalInt
    input.t match {
      case TupleType(Seq(t1: IntTypeT, t2: IntTypeT)) =>
        val t1w = t1.width.ae.evalInt
        val t2w = t2.width.ae.evalInt

        // algo.Sub generally zero extends, so we need to make sure we are
        // using the correct extension
        val subw = 1 + t1w.max(t2w)
        val sub = Sub(Tuple(
          ResizeInteger(Select2(input, 0), subw),
          ResizeInteger(Select2(input, 1), subw)
        ))
        TypeChecker.check(
          if (subw <= ow)
            Conversion(sub, if (limit.kind == SignedIntTypeKind) SignedIntType(subw) else IntType(subw))
          else
            Conversion(TruncInteger(sub, ow), limit)
        )
    }
  }
}

object CappedSubInt extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, limit: IntTypeT): CappedSubIntExpr = CappedSubIntExpr({
    val inputtv = TupleTypeVar(AbstractScalarTypeVar(), AbstractScalarTypeVar())
    val outtv = ConditionalTypeVar(
      inputtv,
      { case TupleType(Seq(t1: IntTypeT, t2: IntTypeT)) =>
        if (t1.kind != t2.kind || t1.kind != limit.kind)
          throw MalformedExprException(this.getClass().getName() + " does not allow mixing signed and unsigned ints")

        val subw = 1 + SimplifyIfThenElse(t1.width.ae.gt(t2.width.ae), t1.width.ae, t2.width.ae)
        val outw = SimplifyIfThenElse(subw.gt(limit.width.ae), limit.width.ae, subw)
        if (limit.kind == SignedIntTypeKind) SignedIntType(outw) else IntType(outw)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(inputtv, AlgoFunType(inputtv, outtv))
    ), input.t), input)
  }, Seq(limit))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): CappedSubIntExpr =
    apply(inputs.head, types.head.asInstanceOf[IntTypeT])

  def unapply(expr: CappedSubIntExpr): Some[(Expr, IntTypeT, Type)] =
    Some((
      expr.args.head,
      expr.types.head.asInstanceOf[IntTypeT],
      expr.t
    ))
}

/**
 * Ensures the result is no wider than the specified type.
 * The signedness has to match.
 */
case class MaybeTruncIntExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoTorchMacro {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MaybeTruncIntExpr =
    MaybeTruncIntExpr(newInnerIR, newTypes)

  override def expand(): Expr = {
    val MaybeTruncInt(input, limit, _) = this
    val iw = input.t.asInstanceOf[IntTypeT].width.ae.evalInt
    val ow = limit.width.ae.evalInt
    if (iw <= ow) input
    else Conversion(TruncInteger(input, ow), limit)
  }
}
object MaybeTruncInt extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, limit: IntTypeT): MaybeTruncIntExpr = MaybeTruncIntExpr({
    val inputtv = AbstractScalarTypeVar()
    val outtv = ConditionalTypeVar(
      inputtv,
      { case t: IntTypeT =>
        if (t.kind != limit.kind)
          throw MalformedExprException(this.getClass().getName() + " does not allow mixing signed and unsigned ints")

        val outw = SimplifyIfThenElse(t.width.ae.gt(limit.width.ae), limit.width.ae, t.width.ae)
        if (t.kind == SignedIntTypeKind) SignedIntType(outw) else IntType(outw)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(inputtv, AlgoFunType(inputtv, outtv))
    ), input.t), input)
  }, Seq(limit))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MaybeTruncIntExpr =
    apply(inputs.head, types.head.asInstanceOf[IntTypeT])

  def unapply(expr: MaybeTruncIntExpr): Some[(Expr, IntTypeT, Type)] =
    Some((
      expr.args.head,
      expr.types.head.asInstanceOf[IntTypeT],
      expr.t
    ))

  def signed(width: ArithTypeT): Expr = asFunction(types = Seq(SignedIntType(width)))
  def unsigned(width: ArithTypeT): Expr = asFunction(types = Seq(IntType(width)))
}

/**
 * Performs quantized addition, which is like fixed point requantization except
 * there are two values being multiplied and summed before rounding division
 * happens.
 *
 * The result is always exactly SignedIntType(8)
 */
case class RequantFixedAddInt8Expr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoTorchMacro {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RequantFixedAddInt8Expr =
    RequantFixedAddInt8Expr(newInnerIR, newTypes)

  override def expand(): Expr = {
    val RequantFixedAddInt8(x, y, sx, sy, fracbits, zp_, _) = this

    val zp = zp_.ae.evalInt
    val zpbits = (if (zp < 0) ~zp else zp).toBinaryString.length + 1

    val m1 = TypeChecker.check(Mul(Tuple(x, Signed(sx))))
    val m2 = TypeChecker.check(Mul(Tuple(y, Signed(sy))))
    val addw = 1 + m1.t.asInstanceOf[IntTypeT].width.ae.evalInt.max(m2.t.asInstanceOf[IntTypeT].width.ae.evalInt)
    val a1 = Add2(ResizeInteger(m1, addw), m2)
    val r = if (fracbits.ae.evalInt == 0) a1 else ClipBankersRound(a1, fracbits, 0)
    val a = if (zp == 0) r else Add2(r, ConstantInteger(zp, Some(SignedIntType(zpbits))))

    val u = TypeChecker.checkIfUnknown(a, a.t)
    val ubits = u.t.asInstanceOf[SignedIntTypeT].width.ae.evalInt
    if (ubits == 8) u
    else if (ubits > 8) ClipBankersRound(u, 0, ubits - 8)
    else ResizeInteger(u, 8)
  }
}
object RequantFixedAddInt8 extends BuiltinExprFun {
  override def args: Int = 4

  def apply(x: Expr, y: Expr, sx: Expr, sy: Expr, fracbits: ArithTypeT, zeroPoint: ArithTypeT): RequantFixedAddInt8Expr = RequantFixedAddInt8Expr({
    val xtv = SignedIntTypeVar()
    val ytv = SignedIntTypeVar()
    val sxtv = IntTypeVar()
    val sytv = IntTypeVar()
    val restype = SignedIntType(8)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(Seq(xtv, ytv, sxtv, sytv), AlgoFunType(Seq(xtv, ytv, sxtv, sytv), restype))
    ), Seq(x.t, y.t, sx.t, sy.t)), Seq(x, y, sx, sy))
  }, Seq(fracbits, zeroPoint))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RequantFixedAddInt8Expr =
    apply(
      inputs.head, inputs(1), inputs(2), inputs(3),
      types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT]
    )

  def unapply(expr: RequantFixedAddInt8Expr): Some[(Expr, Expr, Expr, Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((
      expr.args.head, expr.args(1), expr.args(2), expr.args(3),
      expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT],
      expr.t
    ))

  def asAddFunction(sx: Expr, sy: Expr, fracbits: ArithTypeT, zeroPoint: ArithTypeT): Expr = {
    val x = ParamDef()
    AlgoLambda(x, apply(Select2(ParamUse(x), 0), Select2(ParamUse(x), 1), sx, sy, fracbits, zeroPoint))
  }
}

/**
 * Applies requantization as a fixed point operation, that is multiply by an
 * unsigned scale, perform (signed) rounding division, then offset by a zero
 * point.
 *
 * The result is always exactly SignedIntType(8).
 */
case class RequantFixedInt8Expr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoTorchMacro {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RequantFixedInt8Expr =
    RequantFixedInt8Expr(newInnerIR, newTypes)

  override def expand(): Expr = {
    val RequantFixedInt8(input, uscale, fracbits, zp_, _) = this

    val zp = zp_.ae.evalInt
    val zpbits = (if (zp < 0) ~zp else zp).toBinaryString.length + 1

    val m = Mul(Tuple(input, Signed(uscale)))
    val r = if (fracbits.ae.evalInt == 0) m else ClipBankersRound(m, fracbits, 0)
    val a = if (zp == 0) r else Add2(r, ConstantInteger(zp, Some(SignedIntType(zpbits))))

    val u = TypeChecker.checkIfUnknown(a, a.t)
    val ubits = u.t.asInstanceOf[SignedIntTypeT].width.ae.evalInt
    if (ubits == 8) u
    else if (ubits > 8) ClipBankersRound(u, 0, ubits - 8)
    else ResizeInteger(u, 8)
  }
}
object RequantFixedInt8 extends BuiltinExprFun {
  override def args: Int = 2

  def apply(input: Expr, uscale: Expr, fracbits: ArithTypeT, zeroPoint: ArithTypeT): RequantFixedInt8Expr = RequantFixedInt8Expr({
    val inputtv = SignedIntTypeVar()
    val uscaletv = IntTypeVar()
    val restype = SignedIntType(8)  // as promised
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(Seq(inputtv, uscaletv), AlgoFunType(Seq(inputtv, uscaletv), restype))
    ), Seq(input.t, uscale.t)), Seq(input, uscale))
  }, Seq(fracbits, zeroPoint))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RequantFixedInt8Expr =
    apply(
      inputs.head, inputs(1),
      types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT]
    )

  def unapply(expr: RequantFixedInt8Expr): Some[(Expr, Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((
      expr.args.head, expr.args(1),
      expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT],
      expr.t
    ))

  def asPerChannelFunction(fracbits: ArithTypeT, zeroPoint: ArithTypeT): Expr = {
    val x = ParamDef()
    AlgoLambda(x, apply(Select2(ParamUse(x), 0), Select2(ParamUse(x), 1), fracbits, zeroPoint))
  }
}

/**
 * Applies requantization as a (approximated) float point operation, that is,
 * the scale and rounding division information is implicitly encoded within the
 * float32 scale. the scale is subject to the same restrictions as QScale.
 *
 * Like its fixed point counterpart, this also results in a SignedIntType(8).
 */
case class RequantFloatInt8Expr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoTorchMacro {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RequantFloatInt8Expr =
    RequantFloatInt8Expr(newInnerIR, newTypes)

  override def expand(): Expr = {
    val RequantFloatInt8(input, floatbits, zp_, _) = this

    val zp = zp_.ae.evalInt
    val zpbits = (if (zp < 0) ~zp else zp).toBinaryString.length + 1

    val r = QScale(Tuple(input, floatbits))
    val a = if (zp == 0) r else Add2(r, ConstantInteger(zp, Some(SignedIntType(zpbits))))

    val u = TypeChecker.checkIfUnknown(a, a.t)
    val ubits = u.t.asInstanceOf[SignedIntTypeT].width.ae.evalInt
    if (ubits == 8) u
    else if (ubits > 8) ClipBankersRound(u, 0, ubits - 8)
    else ResizeInteger(u, 8)
  }
}

object RequantFloatInt8 extends BuiltinExprFun {
  override def args: Int = 2

  def apply(input: Expr, floatbits32: Expr, zeroPoint: ArithTypeT): RequantFloatInt8Expr = RequantFloatInt8Expr({
    val inputtv = SignedIntTypeVar()
    val fscale = IntType(32)
    val restype = SignedIntType(8)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(inputtv, AlgoFunType(Seq(inputtv, fscale), restype))
    ), input.t), Seq(input, floatbits32))
  }, Seq(zeroPoint))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RequantFloatInt8Expr =
    apply(
      inputs.head, inputs(1),
      types.head.asInstanceOf[ArithTypeT]
    )

  def unapply(expr: RequantFloatInt8Expr): Some[(Expr, Expr, ArithTypeT, Type)] =
    Some((
      expr.args.head, expr.args(1),
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.t
    ))

  def asPerChannelFunction(zeroPoint: ArithTypeT): Expr = {
    val x = ParamDef()
    AlgoLambda(x, apply(Select2(ParamUse(x), 0), Select2(ParamUse(x), 1), zeroPoint))
  }
}

/**
 * Zips a vector with a tensor on its channel (2nd) dimension and maps a
 * function over it. Main use case is per channel requantization.
 */
case class MapZippedChannelExpr private[algo] (innerIR: Expr) extends AlgoTorchMacro {
  override val types: Seq[Type] = Seq()

  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapZippedChannelExpr =
    MapZippedChannelExpr(newInnerIR)

  override def expand(): Expr = {
    val MapZippedChannel(f, input, vector, _) = this
    val shape = input.t.asInstanceOf[SeqTypeT].dimensions

    TypeChecker.check(shape.length match {
      case 2 =>
        Map({
          val _0 = ParamDef()
          AlgoLambda(_0, Map(f, Zip2(Tuple2(ParamUse(_0), vector))))
        }, input)
      case _ =>
        val inner = shape.dropRight(2).map(_.ae).reduce(_ * _)
        Map({
          val _0 = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
          AlgoLambda(_0, Map({
            val _0 = ParamDef()
            AlgoLambda(_0, shape
              .dropRight(3)
              .foldLeft[AlgoExpr](Map(f, Zip2(ParamUse(_0))))(Split(_, _)))
          }, Zip2(Tuple2(
            Map({
              val _1 = ParamDef(_0.t.asInstanceOf[SeqTypeT].et)
              AlgoLambda(_1, JoinAll(ParamUse(_1)))
            }, ParamUse(_0)),
            Map(Repeat.asFunction(Seq(None), Seq(inner)), vector)
          ))))
        }, input)
    })
  }
}

object MapZippedChannel extends BuiltinExprFun {
  override def args: Int = 3

  def apply(f: Expr, input: Expr, vector: Expr): MapZippedChannelExpr = MapZippedChannelExpr({
    val rank = TypeChecker.checkIfUnknown(input, input.t).t.asInstanceOf[SeqTypeT].dimensions.length
    assert(rank >= 2, "Input must have at least two dimensions")

    val lhstv = AbstractScalarTypeVar()
    val rhstv = AbstractScalarTypeVar()
    val restv = AlgoDataTypeVar()
    val dimN = ArithTypeVar()
    val dimC = ArithTypeVar()
    val dimInner = Seq.fill(rank - 2)(ArithTypeVar())

    val ftv = FunTypeVar(TupleTypeVar(lhstv, rhstv), restv)
    val vectv = SeqTypeVar(rhstv, dimC)
    val inputtv = SeqTypeVar(SeqTypeVar(
      dimInner.foldLeft[AlgoDataTypeT](lhstv)(SeqTypeVar(_, _)), dimC), dimN)
    val restype = SeqType(SeqType(
      dimInner.foldLeft[AlgoDataTypeT](restv)(SeqType(_, _)), dimC), dimN)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(Seq(ftv, inputtv, vectv), AlgoFunType(Seq(ftv, inputtv, vectv), restype))
    ), Seq(f.t, input.t, vector.t)), Seq(f, input, vector))
  })

  override def apply(inputs: Seq[Expr], types: Seq[Type]): MapZippedChannelExpr =
    apply(inputs.head, inputs(1), inputs(2))

  def unapply(expr: MapZippedChannelExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((
      expr.args.head, expr.args(1), expr.args(2),
      expr.t
    ))
}

/**
 * Zips across every dimension and maps a function over it.
 */
object MapZipAll {
  def apply(f: Expr, x: Expr, y: Expr): Expr = {
    // f: (A, B) -> Z
    // x: A[i0, i1, ...]
    // y: B[i0, i1, ...]
    val xtc = TypeChecker.checkIfUnknown(x, x.t)
    val ytc = TypeChecker.checkIfUnknown(y, y.t)

    (xtc.t, ytc.t) match {
      case (xt: SeqTypeT, yt: SeqTypeT) =>
        val dims = xt.dimensions.reverse
        assert(dims.length == yt.dimensions.length, "Cannot zip tensors of different ranks")

        SplitAll(Map(f, Zip2(Tuple2(
          Flatten(xtc, 0, dims.length - 1), Flatten(ytc, 0, dims.length - 1)
        ))), dims.tail)
      case _ => FunctionCall(f, Tuple2(xtc, ytc))
    }
  }
}

/**
 * Performs reduction on a tensor on selected dimensions. If keepDims is true,
 * then the reduced dimensions are re-broadcasted with 1's.
 */
object Reduce {
  def apply(reducer: Expr, input: Expr, dims: Seq[Int], keepDims: Boolean): Expr = {
    assert(dims.nonEmpty, "Reduction dimensions cannot be empty")

    val xtc = TypeChecker.checkIfUnknown(input, input.t)
    val shape = xtc.t.asInstanceOf[SeqTypeT].dimensions.reverse

    val (outerdims, innerdims) = Seq.range(0, shape.length)
      .partition(!dims.contains(_))

    // push the dimensions being reduced inwards,
    // so then we just Map(Reduce(Flatten(X))) later
    val (transposed, innershape) =
    if (outerdims.zipWithIndex.forall({ case (a, b) => a == b }))
      (xtc, shape.drop(outerdims.length))
    else {
      val transposeIndices = (innerdims.reverse ++ outerdims.reverse)
        .map(i => ArithType(shape.length - i - 1))
      (TransposeND(xtc, transposeIndices), dims.map(shape(_)))
    }

    val reduced = Map(outerdims.length, {
      val _0 = ParamDef()
      AlgoLambda(_0, FunctionCall(reducer,
        innershape.tail.foldLeft[Expr](ParamUse(_0))((e, _) => Join(e))))
    }, transposed)

    if (!keepDims)
      reduced
    else
      // every dimension that has been reduced is now a 1
      SplitAll(JoinAll(reduced), shape.zipWithIndex.map({
        case (w, i) => if (dims.contains(i)) ArithType(1) else w
      }).tail)
  }
}

/**
 * Performs pooling reduction on a tensor.
 */
object Pool {
  def apply(reducer: Expr, input: Expr, hasChannel: Boolean, kernelSize: Seq[ArithTypeT], stride: Seq[Int], padding: Seq[Int]): Expr = {
    // pad the input (if necessary)
    val inputtc = {
      val ps = if (hasChannel) 0 +: padding else padding
      val e = BidiPad(input, 0 +: ps, 0)
      TypeChecker.checkIfUnknown(e, e.t)
    }

    val tyInput = inputtc.t.asInstanceOf[SeqTypeT]
    val dims = tyInput.dimensions.length - (if (hasChannel) 2 else 1)

    // TransposeND(x: T[r0, k0, r1, k1, ...]) ~~> T[r0, r1, ..., k0, k1, ...]
    val transposeIndicesSlides = (Seq.range(0, 2 * dims, 2) ++ Seq.range(1, 2 * dims, 2))
      .map(i => ArithType(i))

    Map(if (hasChannel) 2 else 1, {
      val _0 = ParamDef(
        // tyInst is T[N, C, i0, ...] or T[N, i0, ...]
        if (hasChannel) tyInput.et.asInstanceOf[SeqTypeT].et
        else tyInput.et
      )
      AlgoLambda(_0, Map(dims, {
        val _0 = ParamDef()
        AlgoLambda(_0, FunctionCall(reducer,
          kernelSize.tail.foldLeft[Expr](ParamUse(_0))((e, _) => Join(e))))
      }, TransposeND(
        SlideRegion(ParamUse(_0), kernelSize, stride),
        transposeIndicesSlides
      )))
    }, inputtc)
  }
}

object BidiPad {
  def apply(x: Expr, pad: Seq[Int], value: ArithTypeT): Expr = {
    if (pad.isEmpty)
      x
    else {
      val e = Map({
        val _0 = ParamDef()
        AlgoLambda(_0, apply(ParamUse(_0), pad.tail, value))
      }, x)
      if (pad.head == 0) e else Pad(e, pad.head, pad.head, value)
    }
  }
}

object SlideRegion {
  def apply(input: Expr, windowSize: Seq[ArithTypeT], stride: Seq[Int]): Expr = {
    def traverse(input: Expr, windowSize: Seq[ArithTypeT], stride: Seq[Int]): Expr = input.t match {
      case t: SeqTypeT if windowSize.nonEmpty =>
        val i = t.len.ae
        val k = (windowSize.head.ae - 1) + 1
        val remainder = (i - k) % stride.head

        val s = SlideGeneral(Drop(input, 0, remainder), k, stride.head)
        val d = s
        Map(2, {
          val _0 = ParamDef(t.et)
          AlgoLambda(_0, traverse(ParamUse(_0), windowSize.tail, stride.tail))
        }, d)
      case _ => input
    }

    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    traverse(inputtc, windowSize, stride)
  }
}

/**
 * Computes the average of a signed integer vector.
 * The result signed integer has the same width as the input elements.
 * (the fractional part is rounded to nearest even)
 *
 * XXX: This is currently only for signed integers
 * (has to do with how ClipBankersRound performs clipping)
 */
case class ReduceAvgIntExpr private[algo] (innerIR: Expr) extends AlgoTorchMacro {
  override val types: Seq[Type] = Seq()

  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReduceAvgIntExpr =
    ReduceAvgIntExpr(newInnerIR)

  override def expand(): Expr = {
    val ReduceAvgInt(input, ty: SignedIntTypeT) = this

    val len = input.t.asInstanceOf[SeqTypeT].len.ae.evalInt
    val (scl, shamt) = approximateReciprocal(len)
    assert(scl > 0) // length is positive, so must hold

    val sum = TypeChecker.check(Fold(Add2.asFunction(), input))
    val sumbits = sum.t.asInstanceOf[SignedIntTypeT].width.ae.evalInt
    val sclbits = scl.toBinaryString.length + 1 // +1 for sign bit

    // yet another assertion just to make sure the codegen isn't problematic.
    // as in the previous assert, this must hold since if it didn't, that would
    // mean that regardless of what values you pick, the average always results
    // in 0 (which we know is not true).
    assert(sumbits + sclbits > shamt)

    val resbits = ty.width.ae.evalInt
    TypeChecker.check(ClipBankersRound(
      Mul(Tuple(sum, ConstantInteger(scl, Some(SignedIntType(sclbits))))),
      shamt,
      sumbits + sclbits - shamt - resbits
    ))
  }

  def approximateReciprocal(d: Int): (Int, Int) = {
    assert(d != 0, "Attempt to divide by zero")
    val fbits = java.lang.Float.floatToIntBits(1.0f / d)
    val exp = ((fbits >> 23) & 0xff) - 127
    val mantissa = fbits & ((1 << 23) - 1)

    val frac = d.signum * ((if (exp == -127) 0 else 1 << 23) | mantissa)
    val sra = 23 - exp // 23 from mantissa
    if (frac == 0)
      // shouldn't be possible, but just in case
      return (0, 0)

    // try to shorten the (signed) integer multiplier by
    // moving the trailing zero bits to the shift amount.
    // (0b01000 * k) >> 1 ~~> (0b0100 * k) >> 0
    val adjustableZeros = Integer.numberOfTrailingZeros(frac).min(sra)
    (frac >> adjustableZeros, sra - adjustableZeros)
  }
}

object ReduceAvgInt extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): ReduceAvgIntExpr = ReduceAvgIntExpr({
    val etv = SignedIntTypeVar()
    val inputtv = SeqTypeVar(etv)
    val outtv = etv

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(inputtv, AlgoFunType(inputtv, outtv))
    ), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReduceAvgIntExpr =
    apply(inputs.head)

  def unapply(expr: ReduceAvgIntExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

/**
 * Computes the maximum of a vector.
 * It's only here as a shorthand for Fold(Max, ...)
 */
object ReduceMax extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): Expr =
    Fold(Max2.asFunction(), input)

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): Expr =
    apply(inputs.head)
}

object SIConv8 {
  def apply(input: Expr, zp: Int, weight: Expr, stride: Seq[Int], padding: Seq[Int]): Expr = {
    // NCHW --> NHWC --> convolution --> NHWC --> NCHW  (NCHW is the normal order)
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val weighttc = TypeChecker.checkIfUnknown(weight, weight.t)

    val idims = inputtc.t.asInstanceOf[SeqTypeT].dimensions.length
    val window = weighttc.t.asInstanceOf[SeqTypeT].dimensions.reverse.drop(2)

    // input (NCHW) and weights (OCHW) have the same number of dimension and the same format.
    // we transpose both by pushing C downwards.
    val toHWC = ArithType(idims - 2) +: Seq.range(0, idims - 2).map(ArithType(_))
    val toCHW = Seq.range(1, idims - 1).map(ArithType(_)) :+ ArithType(0)

    val nhwc = Map(TransposeND.asFunction(types = toHWC), inputtc)
    val ohwc = Map(TransposeND.asFunction(types = toHWC), weighttc)

    val (adjustedInput, bits) = if (zp == 0) (nhwc, 8) else {
      val bits = ((if (zp < 0) ~zp else zp).toBinaryString.length + 1).max(8) + 1
      (Map(idims, Add2.asFunction(Seq(None, Some(ConstantInteger(-zp, Some(SignedIntType(bits)))))), nhwc), bits)
    }
    val paddedInput = BidiPad(adjustedInput, 0 +: padding, 0)

    // (I)HJWC --> (I)JHWC
    val transposeSlide = ArithType(0) +: (Seq.range(1, 2 * (idims - 2), 2) ++ Seq.range(2, 2 * (idims - 2), 2))
      .map(ArithType(_))

    val slidedTy = TypeChecker.checkIfUnknown(paddedInput, paddedInput.t).t.asInstanceOf[SeqTypeT].et
    val foldTy = TypeChecker.checkIfUnknown(ohwc, ohwc.t).t.asInstanceOf[SeqTypeT].et

    Map({
      val _0 = ParamDef(slidedTy)
      AlgoLambda(_0,
        TransposeND(Map(idims - 2, {
          val _0 = ParamDef(
            foldTy.asInstanceOf[SeqTypeT].dimensions.foldLeft[AlgoDataTypeT](SignedIntType(bits))(SeqType(_, _))
          )
          AlgoLambda(_0, {
            Map({
              val _1 = ParamDef(foldTy)
              AlgoLambda(_1,
                Fold(Add2.asFunction(), Map(Mul.asFunction(), Zip2(Tuple2(
                  JoinAll(ParamUse(_0)), JoinAll(ParamUse(_1))
                ))))
              )
            }, ohwc)
          })
        }, Map({
          val _0 = ParamDef()
          AlgoLambda(_0, TransposeND(ParamUse(_0), transposeSlide))
        }, (SlideRegion(Buffer(ParamUse(_0)), window, stride)))), toCHW)
      )
    }, paddedInput)
  }
}

/**
 * Performs X \cross W.T + b where X: int[I, K], W: int[J, K] and b: int[J].
 *
 * The computation does not overflow.
 */
case class AddMMIntExpr private[algo] (innerIR: Expr) extends AlgoTorchMacro {
  override val types: Seq[Type] = Seq()

  override def build(newInnerIR: Expr, newTypes: Seq[Type]): AddMMIntExpr =
    AddMMIntExpr(newInnerIR)

  override def expand(): Expr = {
    val AddMMInt(acc, lhs, rhs, ty) = this
    val _0 = ParamDef(lhs.t.asInstanceOf[SeqTypeT].et)
    val _1 = ParamDef(rhs.t.asInstanceOf[SeqTypeT].et)
    val fold = TypeChecker.check(
      Fold(Add2.asFunction(),
        Map(Mul.asFunction(), Zip2(Tuple2(ParamUse(_0), ParamUse(_1)))))
    )

    val resty = ty.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et
    val innerf = ResizeInteger(fold, resty.asInstanceOf[IntTypeT].width)

    TypeChecker.check(
      Map(AlgoLambda(_0,
        Map(Add.asFunction(),
          Zip2(Tuple2(acc, Map(AlgoLambda(_1, innerf), rhs))))), lhs)
    )
  }
}
object AddMMInt extends BuiltinExprFun {
  override def args: Int = 3

  def apply(acc: Expr, lhs: Expr, rhs: Expr): AddMMIntExpr = AddMMIntExpr({
    val dimI = ArithTypeVar()
    val dimJ = ArithTypeVar(StartFromRange(1))  // range is for Log
    val dimK = ArithTypeVar()
    val accElt = AbstractScalarTypeVar()
    val lhsElt = AbstractScalarTypeVar()
    val rhsElt = AbstractScalarTypeVar()

    val acctv = SeqTypeVar(accElt, dimJ)
    val lhstv = SeqTypeVar(SeqTypeVar(lhsElt, dimK), dimI)
    val rhstv = SeqTypeVar(SeqTypeVar(rhsElt, dimK), dimJ)
    val outtv = ConditionalTypeVar(
      TupleType(accElt, lhstv, rhstv),
      { case TupleType(Seq(t1: IntTypeT, SeqType(SeqType(t2: IntTypeT, dimK), dimI), SeqType(SeqType(t3: IntTypeT, _), dimJ))) =>
        if (t1.kind != t2.kind || t1.kind != t3.kind)
          throw MalformedExprException(this.getClass().getName() + " does not support mixing signed and unsigned ints")

        val mulw = t2.width.ae + t3.width.ae
        val redw = mulw + lift.arithmetic.ceil(lift.arithmetic.Log(2, dimK.ae))
        val addw = 1 + SimplifyIfThenElse(t1.width.ae.gt(redw), t1.width.ae, redw)
        val intt = if (t1.kind == SignedIntTypeKind) SignedIntType(addw) else IntType(addw)
        SeqType(SeqType(intt, dimJ), dimI)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(Seq(acctv, lhstv, rhstv), AlgoFunType(Seq(acctv, lhstv, rhstv), outtv))
    ), Seq(acc.t, lhs.t, rhs.t)), Seq(acc, lhs, rhs))
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddMMIntExpr =
    apply(inputs.head, inputs(1), inputs(2))

  def unapply(expr: AddMMIntExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((
      expr.args.head, expr.args(1), expr.args(2),
      expr.t
    ))
}
