package algo

import core._
import lift.arithmetic
import lift.arithmetic.simplifier.SimplifyIfThenElse
import lift.arithmetic.{ArithExpr, StartFromRange, ceil}

import scala.annotation.tailrec

final case class AlgoLambda private (param: ParamDef, body: Expr, t: FunTypeT) extends LambdaT {
  override def build(newChildren: Seq[ShirIR]): AlgoLambda = AlgoLambda(newChildren.head.asInstanceOf[ParamDef], newChildren(1).asInstanceOf[Expr], newChildren(2).asInstanceOf[FunTypeT])
}

object AlgoLambda {
  def apply(param: ParamDef, body: Expr): AlgoLambda = AlgoLambda(param, body, AlgoFunType(param.t, body.t))

  @tailrec
  def apply(params: Seq[ParamDef], body: Expr): AlgoLambda = {
    if (params.isEmpty) {
      throw MalformedExprException("Lambda expression must have at least one parameter")
    } else if (params.size == 1) {
      AlgoLambda(params.head, body)
    } else {
      AlgoLambda(params.tail, AlgoLambda(params.head, body))
    }
  }

  def unapply(expr: Expr): Option[(ParamDef, Expr, Type)] = expr match {
    case l: LambdaT => Some(l.param, l.body, expr.t)
    case _ => None
  }
}

/**
  * root trait of all expression in the algo package / in the AlgoIR
  */
sealed trait AlgoExpr extends BuiltinExpr with AlgoIR

/**
 * a root trait for all "macros" of algo level expressions
 *
 * this is mainly for out-tree-ish things like the algo.torch namespace
 * which is on the same level as algo but is torch specific.
 */
trait AlgoMacroExpr extends BuiltinExpr with AlgoIR {

  /**
   * Expands the macro. This happens only when type checking succeeds.
   * Currently, ArchCompiler will call this (at latest)
   *
   * @return the decomposed operations
   */
  def expand(): Expr
}

/*
 ***********************************************************************************************************************
 value/input generators
 ***********************************************************************************************************************
 */

case class InputExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): InputExpr = InputExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Input(identifier, elementType, dimensions, _) =>
            val formattedDimensions = dimensions.map(formatter.formatNonAtomic).mkString(", ")
            Some(
              formatter.makeAtomic(
                s"input ${formatter.formatAtomic(identifier)} ${formatter.formatAtomic(elementType)} ($formattedDimensions)"))
        }
      case _ => None
    }
  }
}
object Input {
  def apply(identifier: TextTypeT, elementType: AlgoDataTypeT, dimensions: Seq[ArithTypeT]): InputExpr =
    InputExpr(Value(SeqType(dimensions, elementType)), Seq(identifier, elementType) ++ dimensions)
  def unapply(expr: InputExpr): Some[(TextTypeT, AlgoDataTypeT, Seq[ArithTypeT], Type)] =
    Some((expr.types.head.asInstanceOf[TextTypeT], expr.types(1).asInstanceOf[AlgoDataTypeT], expr.types.tail.tail.map(_.asInstanceOf[ArithTypeT]), expr.t))

  def apply(identifier: String, elementType: AlgoDataTypeT, numElements: ArithTypeT): InputExpr =
    apply(TextType(identifier), elementType, Seq(numElements))
  // TODO remove one of the following
  def apply(identifier: String, numInnerElements: ArithTypeT, numOuterElements: ArithTypeT, elementType: AlgoDataTypeT): InputExpr =
    apply(TextType(identifier), elementType, Seq(numInnerElements, numOuterElements))
  def apply(identifier: String, elementType: AlgoDataTypeT, numInnerElements: ArithTypeT, numOuterElements: ArithTypeT): InputExpr =
    apply(TextType(identifier), elementType, Seq(numInnerElements, numOuterElements))
}

case class StoreResultExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): StoreResultExpr = StoreResultExpr(newInnerIR)
}
object StoreResult {
  def apply(input: Expr): StoreResultExpr = StoreResultExpr({
    val dtv = AlgoDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, AlgoFunType(dtv, dtv))), input.t), input)
  })
  def unapply(expr: StoreResultExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class BufferExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): BufferExpr = BufferExpr(newInnerIR)
}
object Buffer {
  def apply(input: Expr): BufferExpr = BufferExpr({
    val dtv = AlgoDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, AlgoFunType(dtv, dtv))), input.t), input)
  })
  def unapply(expr: BufferExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class BufferHostExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): BufferHostExpr = BufferHostExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case BufferHost(input, identifier, _) =>
            Some(formatter.makeAtomic(s"buffer-host ${formatter.formatAtomic(identifier)} ${formatter.formatAtomic(input)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}
object BufferHost {
  def apply(input: Expr, identifier: TextTypeT): BufferHostExpr = BufferHostExpr({
    val dtv = AlgoDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, AlgoFunType(dtv, dtv))), input.t), input)
  }, Seq(identifier))
  def unapply(expr: BufferHostExpr): Some[(Expr, TextTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[TextTypeT], expr.t))
}

case class ConstantIntegerExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConstantIntegerExpr =
    ConstantInteger(newTypes.head.asInstanceOf[ArithTypeT], Some(newTypes(1)))
}
object ConstantInteger {
  def apply(value: ArithTypeT, valueType: Option[Type] = None): ConstantIntegerExpr = {
    val requiredBits = if (value.ae.evalInt < 0)
      (~value.ae.evalInt).toBinaryString.length + 1
    else
      value.ae.evalInt.toBinaryString.length
    valueType match {
      case Some(tv: TypeVarT) => ConstantIntegerExpr(Value(tv), Seq(value, tv))
      case Some(t: IntTypeT) if t.width.ae.evalInt >= requiredBits => ConstantIntegerExpr(Value(t), Seq(value, t))
      case Some(_: IntTypeT) => throw MalformedTypeException("constant value requires higher precision type")
      case Some(t) => throw MalformedTypeException("constant value requires int data type; found type: " + t)
      case None =>
        val intType = IntType(requiredBits)
        ConstantIntegerExpr(Value(intType), Seq(value, intType))
    }
  }
  def unapply(expr: ConstantIntegerExpr): Some[(ArithTypeT, Type)] =
    Some(expr.types.head.asInstanceOf[ArithTypeT], expr.t)
}

case class ConstantSeqExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConstantSeqExpr =
    ConstantSeq(newTypes.init.asInstanceOf[Seq[ArithTypeT]], Some(newTypes.last))
}
object ConstantSeq {

  /**
   * Constructs a sequence (SeqType) of integers.
   *
   * @param values    The elements of the stream. last element first
   * @param valueType The type of the individual elements of the stream.
   */
  def apply(values: Seq[ArithTypeT], valueType: Option[Type] = None): ConstantSeqExpr = {
    valueType match {
      case Some(t: IntTypeT) => ConstantSeqExpr(Value(SeqType(t, values.length)), values ++ Seq(t))
      case Some(t) => throw MalformedTypeException("ConstantSeq does not support " + t)
      case None =>
        val intSeq = values.map(_.ae.evalInt)
        val minValue = intSeq.min
        val valueBits = {
          val maxValue = intSeq.max
          if (minValue >= 0) maxValue.toBinaryString.length
          else (~minValue).toBinaryString.length.max(maxValue.toBinaryString.length) + 1
        }
        val dataType = if (minValue < 0) SignedIntType(valueBits) else IntType(valueBits)
        ConstantSeqExpr(Value(SeqType(dataType, values.length)), values ++ Seq(dataType))
    }
  }
  def unapply(expr: ConstantSeqExpr): Some[(Seq[ArithTypeT], Type)] =
    Some(expr.types.asInstanceOf[Seq[ArithTypeT]], expr.t)
}

case class CounterIntegerExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): CounterIntegerExpr = CounterIntegerExpr(newInnerIR, newTypes)
}
object CounterInteger {
  def apply(start: ArithTypeT, increment: ArithTypeT, numElements: ArithTypeT): CounterIntegerExpr =
    apply(start, increment, 0, Seq(numElements), Seq(0))
  def apply(start: ArithTypeT, increment: ArithTypeT, dimensions: Seq[ArithTypeT], valueType: Option[Type]): CounterIntegerExpr =
    apply(start, increment, 0, dimensions, Seq.fill(dimensions.size)(0), valueType)
  def apply(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT]): CounterIntegerExpr =
    apply(start, increment, loop, dimensions, repetitions, None)

  /**
    * @param start initial value of the counter
    * @param increment step size to increment counter
    * @param loop counter is just counting up once (if value is 0), counter is looping infinitely (otherwise)
    * @param dimensions sequence of arithmetic values with the innermost dimension first and the outermost dimension last
    * @param repetitions if value at position N is 1, the Nth dimension will repeat the values instead of incrementing them
    *                    this is similar to a combination of the Counter and Repeat primitives
    * @param valueType data type for the counter's output (e.g. 8-bit int)
    */
  def apply(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT], valueType: Option[Type]): CounterIntegerExpr = {
    val limit = dimensions.zip(repetitions).filter(_._2.ae.evalInt == 0).map(_._1).fold(ArithType(1))(_.ae * _.ae)
    val requiredBits = (start.ae + increment.ae * (limit.ae - 1)).evalInt.toBinaryString.length
    valueType match {
      case Some(t: IntTypeT) if t.width.ae.evalInt >= requiredBits => _apply(start, increment, loop, dimensions, repetitions, t)
      case Some(_: IntTypeT) => throw MalformedTypeException("counter requires higher precision type")
      case Some(_) => throw MalformedTypeException("counter requires int data type")
      case None => _apply(start, increment, loop, dimensions, repetitions, IntType(requiredBits))
    }
  }
  private def _apply(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT], valueType: AlgoDataTypeT): CounterIntegerExpr = {
    assert(dimensions.length == repetitions.length)
    CounterIntegerExpr(Value(SeqType(dimensions, valueType)),
      Seq(start, increment, loop) ++ dimensions ++ repetitions)
  }

  def unapply(expr: CounterIntegerExpr): Some[(ArithTypeT, ArithTypeT, ArithTypeT, Seq[ArithTypeT], Seq[ArithTypeT], SeqTypeT)] = {
    val seqs = expr.types.tail.tail.tail.map(_.asInstanceOf[ArithTypeT])
    val (dimensions, repetitions) = seqs.splitAt(seqs.length / 2)
    Some(
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.types(1).asInstanceOf[ArithTypeT],
      expr.types(2).asInstanceOf[ArithTypeT],
      dimensions,
      repetitions,
      expr.t.asInstanceOf[SeqTypeT]
    )
  }
}

case class TruncIntegerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TruncIntegerExpr = TruncIntegerExpr(newInnerIR, newTypes)
}
object TruncInteger extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, targetBitWidth: ArithTypeT): TruncIntegerExpr = TruncIntegerExpr({
    val dtv = AlgoDataTypeVar()
    val outt = IntType(targetBitWidth)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, AlgoFunType(dtv, outt))), input.t), input)
  }, Seq(targetBitWidth))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TruncIntegerExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: TruncIntegerExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class ResizeIntegerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ResizeIntegerExpr =
    ResizeIntegerExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case ResizeInteger(expr, width, _) =>
            Some(formatter.makeAtomic(s"resize ${formatter.format(width)} ${formatter.formatAtomic(expr)}"))
        }
      case _ => None
    }
  }
}
object ResizeInteger extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, width: ArithTypeT): ResizeIntegerExpr = ResizeIntegerExpr({
    val dtv = AbstractScalarTypeVar()
    val outtv = ConditionalTypeVar(
      dtv,
      {
        case _: SignedIntTypeT => SignedIntType(width)
        case _: IntTypeT => IntType(width)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(dtv, AlgoFunType(dtv, outtv))
    ), input.t), input)
  }, Seq(width))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ResizeIntegerExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: ResizeIntegerExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class ItemExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ItemExpr =
    ItemExpr(newInnerIR)
}
object Item extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): ItemExpr = ItemExpr({
    val dtv = AbstractScalarTypeVar()
    val inputtv = SeqTypeVar(dtv, 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(inputtv, AlgoFunType(inputtv, dtv))
    ), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ItemExpr =
    apply(inputs.head)
  def unapply(expr: ItemExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class RepeatExpr private[algo](innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RepeatExpr = RepeatExpr(newInnerIR, newTypes)
}
object Repeat extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, len: ArithTypeT): RepeatExpr = RepeatExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val outseqt = SeqType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv, lentv), AlgoFunType(dtv, outseqt))), Seq(input.t, len)), input)
  }, Seq(len))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): RepeatExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: RepeatExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object RepeatAll extends BuiltinExprFun {
  override def args: Int = 1
  @tailrec
  def apply(input: Expr, dims: Seq[ArithTypeT]): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    if(dims.nonEmpty) {
      RepeatAll(Repeat(inputtc, dims.last), dims.init)
    } else {
      inputtc.asInstanceOf[BuiltinExpr]
    }
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, types.asInstanceOf[Seq[ArithTypeT]])
}

case class SinkExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SinkExpr = SinkExpr(newInnerIR)
}
object Sink {
  def apply(input: Expr): SinkExpr = SinkExpr({
    val dtv = AlgoDataTypeVar()
    val fixedOutType = IntType(1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, AlgoFunType(dtv, fixedOutType))), input.t), input)
  })
  def unapply(expr: SinkExpr): Some[(Expr, Type)] =
    Some(expr.args.head, expr.t)
}

/*
 ***********************************************************************************************************************
 simple operations
 ***********************************************************************************************************************
 */

case class IdExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): IdExpr = IdExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Id(input, _) =>
            Some(formatter.makeAtomic(s"id ${formatter.formatAtomic(input)}"))
        }
    }
  }
}
object Id extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): IdExpr = IdExpr({
    val dtv = AlgoDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, AlgoFunType(dtv, dtv))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): IdExpr = apply(inputs.head)
  def unapply(expr: IdExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class SignedExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SignedExpr = SignedExpr(newInnerIR)
}
object Signed extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): SignedExpr = SignedExpr({
    val width = ArithTypeVar()
    val itv = IntTypeVar(width)
    val outt = SignedIntType(width + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, AlgoFunType(itv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SignedExpr = apply(inputs.head)
  def unapply(expr: SignedExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class UnSignedExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): UnSignedExpr = UnSignedExpr(newInnerIR)
}
object Unsigned extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): UnSignedExpr = UnSignedExpr({
    val width = ArithTypeVar()
    val itv = SignedIntTypeVar(width)
    val outt = IntType(width - 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, AlgoFunType(itv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): UnSignedExpr = apply(inputs.head)
  def unapply(expr: UnSignedExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class ClipBankersRoundExpr private[algo](innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ClipBankersRoundExpr = ClipBankersRoundExpr(newInnerIR, newTypes)
}
object ClipBankersRound extends BuiltinExprFun {
  override def args: Int = 1

  /**
    * Apply clipping to most significant bits and rounding to least significant bits.
    * The rounding technique is round-to-nearest even. (To follow the rounding scenario in Python.)
    *
    * @param input        The input expression.
    * @param lowElements  The number of least significant bits for rounding.
    * @param highElements The number of most significant bits for rounding.
    */
  def apply(input: Expr, lowElements: ArithTypeT, highElements: ArithTypeT): ClipBankersRoundExpr = ClipBankersRoundExpr({
    val itv = IntTypeVar()
    val outt = ConditionalTypeVar(
      itv,
      {
        case SignedIntType(width) => SignedIntType(width.ae - lowElements.ae - highElements.ae)
        case IntType(width) => IntType(width.ae - lowElements.ae - highElements.ae)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, AlgoFunType(itv, outt))), input.t), input)
  }, Seq(lowElements, highElements))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ClipBankersRoundExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: ClipBankersRoundExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class ClipBankersRoundShiftExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ClipBankersRoundShiftExpr = ClipBankersRoundShiftExpr(newInnerIR)
}
object ClipBankersRoundShift extends BuiltinExprFun {
  override def args: Int = 1

  /**
   * In short, this is ClipBankersRound wrapped inside a shift.
   *
   * IF the shift amount is positive, this is a left-shift + signed saturation
   * on the input width.
   *
   * IF the shift amount is negative, this is an arithmetic right-shift (of the
   * negated shift amount) + bankers round with the dropped bits interpreted as
   * the fractional part.
   *
   * @param input A tuple of the value and shift amount
   */
  def apply(input: Expr): ClipBankersRoundShiftExpr = ClipBankersRoundShiftExpr({
    val i = SignedIntTypeVar()
    val s = SignedIntTypeVar()
    val ttv = TupleTypeVar(i, s)
    val out = SignedIntType(i.width.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, out))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ClipBankersRoundShiftExpr = apply(inputs.head)
  def unapply(expr: ClipBankersRoundShiftExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class QScaleExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): QScaleExpr = QScaleExpr(newInnerIR)
}
object QScale extends BuiltinExprFun {
  override def args: Int = 1

  /**
   * Performs rounded integer-float multiplication. Useful for requantization.
   *
   * XXX: Currently, all fractional bits are kept, which might give you
   * slightly *unexpected* results when compared to results obtained when using
   * a real IEEE conforming float point implementation.
   *
   * XXX: Currently, the float *must* be a positive normal value.
   * that means a even a float value of zero would give incorrect results.
   *
   * @param input A tuple of the signed int value and float32 bits
   */
  def apply(input: Expr): QScaleExpr = QScaleExpr({
    val width = ArithTypeVar()
    val i = SignedIntTypeVar(width)
    val s = IntType(32)
    val ttv = TupleTypeVar(i, s)
    val out = SignedIntType(width.ae + 25)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, out))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): QScaleExpr = apply(inputs.head)
  def unapply(expr: QScaleExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class AddExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): AddExpr = AddExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Add(input, _) => Some(formatter.makeAtomic(s"add ${formatter.formatAtomic(input)}"))
        }
      case _ => None
    }
  }
}
object Add extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): AddExpr = AddExpr({
    val ttv = TupleTypeVar(AbstractScalarTypeVar(), AbstractScalarTypeVar())
    val outt =
      ConditionalTypeVar(
        ttv,
        {
          case TupleType(Seq(t0@(_: IntTypeT | _: SignedIntTypeT), t1@(_: IntTypeT | _: SignedIntTypeT))) if t0.kind != t0.kind => // Exclude info of size
            throw MalformedExprException(this.getClass().getName() + " does not support a tuple of " + t0.toString + " and " + t1.toString + ".")
          case TupleType(Seq(it1: SignedIntTypeT, it2: SignedIntTypeT)) =>
            SignedIntType(SimplifyIfThenElse(
              it1.width.ae.gt(it2.width.ae),
              it1.width.ae,
              SimplifyIfThenElse(
                it1.width.ae.eq(it2.width.ae),
                it1.width.ae + 1,
                it2.width.ae
              )
            ))
          case TupleType(Seq(it1: IntTypeT, it2: IntTypeT)) =>
            IntType(SimplifyIfThenElse(
              it1.width.ae.gt(it2.width.ae),
              it1.width.ae,
              SimplifyIfThenElse(
                it1.width.ae.eq(it2.width.ae),
                it1.width.ae + 1,
                it2.width.ae
              )
            ))
          case TupleType(Seq(ft1: FloatTypeT, ft2: FloatTypeT)) if ft1 == ft2 => ft1
        }
      )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddExpr = apply(inputs.head)
  def unapply(expr: AddExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object Inc extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): AddExpr = Add(Tuple2(input, ConstantInteger(1)))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddExpr = apply(inputs.head)
}

object Add2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): AddExpr = Add(Tuple2(input1, input2))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddExpr = apply(inputs.head, inputs(1))
}

case class SubExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SubExpr = SubExpr(newInnerIR)
}
object Sub extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): SubExpr = SubExpr({
    val itv1 = SignedIntTypeVar()
    val itv2 = SignedIntTypeVar()
    val ttv = TupleTypeVar(itv1, itv2)
    val outit = SignedIntType(
      SimplifyIfThenElse(
        itv1.width.ae.gt(itv2.width.ae),
        itv1.width.ae,
        itv2.width.ae
      )
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outit))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SubExpr = apply(inputs.head)
  def unapply(expr: SubExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class MulExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MulExpr = MulExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Mul(input, _) => Some(formatter.makeAtomic(s"mul ${formatter.formatAtomic(input)}"))
        }
      case _ => None
    }
  }
}
object Mul extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): MulExpr = MulExpr({
    val ttv = TupleTypeVar(AbstractScalarTypeVar(), AbstractScalarTypeVar())
    val outt = ConditionalTypeVar(
      ttv,
      {
        case TupleType(Seq(t0@(_: IntTypeT | _: SignedIntTypeT), t1@(_: IntTypeT | _: SignedIntTypeT))) if t0.kind != t0.kind => // Exclude info of size
          throw MalformedExprException(this.getClass().getName() + " does not support a tuple of " + t0.toString + " and " + t1.toString + ".")
        case TupleType(Seq(it1: SignedIntTypeT, it2: SignedIntTypeT)) => SignedIntType(it1.width.ae + it2.width.ae)
        case TupleType(Seq(it1: IntTypeT, it2: IntTypeT)) => IntType(it1.width.ae + it2.width.ae)
        case TupleType(Seq(ft1: FloatTypeT, ft2: FloatTypeT)) if ft1 == ft2 => ft1
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MulExpr = apply(inputs.head)
  def unapply(expr: MulExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class MinExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MinExpr = MinExpr(newInnerIR)
}
object Min extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): MinExpr = MinExpr({
    val ttv = TupleTypeVar(AbstractScalarTypeVar(), AbstractScalarTypeVar())
    val outt = ConditionalTypeVar(
      ttv,
      { case TupleType(Seq(t1: IntTypeT, t2: IntTypeT)) =>
        if (t1.kind != t2.kind)
          throw MalformedExprException(this.getClass().getName() + " does not support mixing signed and unsigned ints")

        val outw = SimplifyIfThenElse(t1.width.ae.gt(t2.width.ae), t1.width.ae, t2.width.ae)
        if (t1.kind == SignedIntTypeKind) SignedIntType(outw) else IntType(outw)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MinExpr = apply(inputs.head)
  def unapply(expr: MinExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object Min2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): MinExpr = Min(Tuple2(input1, input2))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MinExpr = apply(inputs.head, inputs(1))
}

case class MaxExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MaxExpr = MaxExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Max(input, _) => Some(formatter.makeAtomic(s"max ${formatter.formatAtomic(input)}"))
        }
      case _ => None
    }
  }
}
object Max extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): MaxExpr = MaxExpr({
    val ttv = TupleTypeVar(AbstractScalarTypeVar(), AbstractScalarTypeVar())
    val outt = ConditionalTypeVar(
      ttv,
      { case TupleType(Seq(t1: IntTypeT, t2: IntTypeT)) =>
        if (t1.kind != t2.kind)
          throw MalformedExprException(this.getClass().getName() + " does not support mixing signed and unsigned ints")

        val outw = SimplifyIfThenElse(t1.width.ae.gt(t2.width.ae), t1.width.ae, t2.width.ae)
        if (t1.kind == SignedIntTypeKind) SignedIntType(outw) else IntType(outw)
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MaxExpr = apply(inputs.head)
  def unapply(expr: MaxExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

object Max2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): MaxExpr = Max(Tuple2(input1, input2))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MaxExpr = apply(inputs.head, inputs(1))
}

object ReLU extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): MaxExpr = Max2(input, ConstantInteger(0, Some(input.t)))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MaxExpr = apply(inputs.head)
}

case class SelectExpr private[algo](innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SelectExpr = SelectExpr(newInnerIR, newTypes)
}

object Select extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, n: ArithTypeT, selection: ArithTypeT): SelectExpr = SelectExpr({
    val elts = n.ae.evalInt
    val sel = selection.ae.evalInt
    assert(sel < elts && sel >= 0, s"Invalid select element ${sel} from ${n}-tuple")

    val tvs = Seq.range(0, elts).map(_ => AlgoDataTypeVar())
    val inttv = TupleTypeVar(tvs: _*)
    val outt = tvs(sel)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, AlgoFunType(inttv, outt))), input.t), input)
  }, Seq(n, selection))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SelectExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])

  def unapply(expr: SelectExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

object Select2 extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, selection: ArithTypeT): SelectExpr = Select(input, 2, selection)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SelectExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SelectExpr): Option[(Expr, ArithTypeT, Type)] = expr match {
    case Select(e, n, s, t) if n.ae.evalInt == 2 => Some((e, s, t))
    case _ => None
  }
}

object Select3 extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, selection: ArithTypeT): SelectExpr = Select(input, 3, selection)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SelectExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SelectExpr): Option[(Expr, ArithTypeT, Type)] = expr match {
    case Select(e, n, s, t) if n.ae.evalInt == 3 => Some((e, s, t))
    case _ => None
  }
}

/*
 ***********************************************************************************************************************
 complex operations (map, reduce, slide, ...)
 ***********************************************************************************************************************
 */

case class MapExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapExpr = MapExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Map(function, input, _) =>
            Some(formatter.makeAtomic(s"map ${formatter.formatAtomic(function)} ${formatter.formatAtomic(input)}"))
        }
    }
  }
}
object Map extends BuiltinExprFun {
  override def args: Int = 2
  def apply(function: Expr, input: Expr): MapExpr = MapExpr({
    val indtv = AlgoDataTypeVar()
    val outdtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(indtv, outdtv, lentv),
      FunType(SeqType(indtv, lentv), FunType(FunType(indtv, outdtv), SeqType(outdtv, lentv))))),
      Seq(UnknownType, UnknownType, UnknownType)), Seq(function, input))
  })
  def apply(nestingLevels: Int, function: Expr, input: Expr): Expr = {
    if (nestingLevels <= 0) {
      FunctionCall(function, input)
    } else if (nestingLevels == 1) {
      apply(function, input)
    } else {
      val inputtc = TypeChecker.checkIfUnknown(input, input.t)
      val et = inputtc.t match {
        case st: SeqTypeT => st.et
        case _ => throw MalformedExprException(s"Cannot create $nestingLevels times nested Maps for input of type ${inputtc.t}")
      }
      Map(
        {
          val p = ParamDef(et)
          AlgoLambda(p, apply(nestingLevels - 1, function, ParamUse(p)))
        },
        input
      )
    }
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MapExpr = apply(inputs.head, inputs(1))
  def unapply(expr: MapExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

/**
 * MapAsync gets rids of tuples so that input streams can arrive at different time.
 */

case class MapAsyncExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MapAsyncExpr = MapAsyncExpr(newInnerIR)
}

object MapAsync {
  def apply(function: Expr, inputs: Expr*): MapAsyncExpr = MapAsyncExpr({
    val indtvs = inputs.map(_ => AlgoDataTypeVar())
    val outdtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val ft = FunType(indtvs, outdtv)
    val insts = indtvs.map(dtv => SeqType(dtv, lentv))
    val outst = SeqType(outdtv, lentv)
    /**
     * Note that the apply methods of FunType and TypeFunType revert the order of indtvs/indtvs.
     * In contrast, FunctionCall and TypeFunctionCall do not!
     */
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(indtvs.reverse :+ lentv :+ outdtv,
      FunType(ft +: insts.reverse, outst))), indtvs.map(_ => UnknownType) :+ UnknownType :+ UnknownType), function +: inputs)
  })

  def apply(nestingLevels: Int, function: Expr, inputs: Expr*): Expr = {
    if (nestingLevels <= 0) {
      FunctionCall(function, inputs)
    } else if (nestingLevels == 1) {
      apply(function, inputs: _*)
    } else {
      val inputstc = inputs.map(i => TypeChecker.check(i))
      val params = inputstc.map(i => i.t match {
        case st: SeqTypeT => ParamDef(st.et)
        case _ => throw MalformedExprException(s"Cannot create ${nestingLevels} times nested Maps for input of type ${i.t}")
      })
      val paramUses = params.map(p => ParamUse(p))
      val funBody = apply(function, paramUses: _*)
      /**
       * Note that the apply method of AlgoLambda reverts the order of params!
       */
      val fun = AlgoLambda(params.reverse, funBody)
      MapAsync(fun, inputs: _*)
    }
  }

  def unapply(expr: MapAsyncExpr): Some[(Expr, Seq[Expr], Type)] =
    Some((expr.args.head, expr.args.tail, expr.t))
}

case class FoldUpperBoundedSeqExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): FoldUpperBoundedSeqExpr = FoldUpperBoundedSeqExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case FoldUpperBoundedSeq(function, input, _) =>
            Some(formatter.makeAtomic(s"foldUnboundedSeq ${formatter.formatAtomic(function)} ${formatter.formatAtomic(input)}"))
        }
    }
  }
}
object FoldUpperBoundedSeq extends BuiltinExprFun {
  override def args: Int = 2
  def apply(function: Expr, input: Expr): FoldUpperBoundedSeqExpr = FoldUpperBoundedSeqExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar(StartFromRange(1))
    val acctv = AlgoDataTypeVar()

    val outt = ConditionalTypeVar(
      UpperBoundedSeqType(dtv, lentv),
      {
        case UpperBoundedSeqType(t: IntTypeT, len) =>
          var outBitWidth: ArithTypeT = t.width.ae + ceil(arithmetic.Log(2, len.ae))
          function.visit{
            case Max(_, _) | Min(_, _) => outBitWidth = t.width.ae
            case _ =>
          }
          if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth)

        case UpperBoundedSeqType(SeqType(t: IntTypeT, innerLen), outerLen) =>
          var outBitWidth: ArithTypeT = t.width.ae + ceil(arithmetic.Log(2, outerLen.ae))
          function.visit {
            case Max(_, _) | Min(_, _) => outBitWidth = t.width.ae
            case _ =>
          }
          SeqType(
            if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth),
            innerLen
          )

        /**
         * Note that we cannot use t:AlgoDataTypeT here because the default type is AlgoDataTypeVar. The default type
         * will make the type checker skip the previous two cases. It means if the type checker does not solve dtv type
         * first, it will always use default type aka AlgoDataTypeT.
         */
        case UpperBoundedSeqType(t: AbstractCollectionTypeT, _) => t
        case UpperBoundedSeqType(t: AbstractScalarTypeT, _) => t
      }
    )

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(dtv, acctv, lentv),
        FunType(Seq(FunType(dtv, FunType(outt, acctv)), UpperBoundedSeqType(dtv, lentv)), outt)
      )),
      Seq(UnknownType, UnknownType, UnknownType)),
      Seq(function, input)
    )
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FoldUpperBoundedSeqExpr = apply(inputs.head, inputs(1))
  def unapply(expr: FoldUpperBoundedSeqExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class FoldExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): FoldExpr = FoldExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Fold(function, input, _) =>
            Some(formatter.makeAtomic(s"fold ${formatter.formatAtomic(function)} ${formatter.formatAtomic(input)}"))
        }
    }
  }
}
object Fold extends BuiltinExprFun {
  override def args: Int = 2
  def apply(function: Expr, input: Expr): FoldExpr = FoldExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar(StartFromRange(1))
    val acctv = AlgoDataTypeVar()

    val outt = ConditionalTypeVar(
      SeqType(dtv, lentv),
      {
        case SeqType(t: IntTypeT, len) =>
          var outBitWidth: ArithTypeT = t.width.ae + ceil(arithmetic.Log(2, len.ae))
          function.visit{
            case Max(_, _) | Min(_, _) => outBitWidth = t.width.ae
            case _ =>
          }
          if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth)

        case SeqType(SeqType(t: IntTypeT, innerLen), outerLen) =>
          var outBitWidth: ArithTypeT = t.width.ae + ceil(arithmetic.Log(2, outerLen.ae))
          function.visit {
            case Max(_, _) | Min(_, _) => outBitWidth = t.width.ae
            case _ =>
          }
          SeqType(
            if (t.kind == SignedIntTypeKind) SignedIntType(outBitWidth) else IntType(outBitWidth),
            innerLen
          )

        /**
         * Note that we cannot use t:AlgoDataTypeT directly because the default type is AlgoDataType. The default type
         * will make the type checker skip the previous two cases. It means if the type checker does not solve dtv type
         * first, it will always use default type aka AlgoDataTypeT.
         */
        case SeqType(t: AlgoDataTypeT, _) if t.kind != AlgoDataTypeKind => t
      }
    )

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(dtv, acctv, lentv),
        FunType(Seq(FunType(dtv, FunType(outt, acctv)), SeqType(dtv, lentv)), outt)
      )),
      Seq(UnknownType, UnknownType, UnknownType)),
      Seq(function, input)
    )
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FoldExpr = apply(inputs.head, inputs(1))
  def unapply(expr: FoldExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class ReduceExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReduceExpr = ReduceExpr(newInnerIR)
}
object Reduce extends BuiltinExprFun {
  override def args: Int = 3
  def apply(function: Expr, initialValue: Expr, input: Expr): ReduceExpr = ReduceExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar(StartFromRange(1))
    val initialValueTv = AlgoDataTypeVar()
    val acctv = AlgoDataTypeVar()

    val outt = ConditionalTypeVar(
      TupleType(initialValueTv, SeqType(dtv, lentv)),
      {
        case TupleType(Seq(t: IntTypeT, SeqType(_, len))) if t.kind == IntTypeKind => // do not allow subtypes of IntTypeT here!
          var outBitWidth: ArithTypeT = t.width.ae + ceil(arithmetic.Log(2, len.ae))
          function.visit {
            case Max(_, _) | Min(_, _) => outBitWidth = t.width.ae
            case _ =>
          }
          IntType(outBitWidth)

        case TupleType(Seq(initt: AlgoDataTypeT, _)) if initt.kind != AlgoDataTypeKind => initt
      }
    )

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(
        Seq(dtv, initialValueTv, acctv, lentv),
        FunType(Seq(FunType(dtv, FunType(outt, acctv)), initialValueTv, SeqType(dtv, lentv)), outt)
      )),
      Seq(UnknownType, UnknownType, UnknownType, UnknownType)),
      Seq(function, initialValue, input)
    )
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReduceExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: ReduceExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

object DotProduct extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): BuiltinExpr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
    Fold(
      Add2.asFunction(),
      Map(
        Mul.asFunction(),
        Zip2(Tuple2(input1, input2))
      )
    )
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, inputs(1))
}

object VAdd extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): BuiltinExpr =
    Map(
      Add.asFunction(),
      Zip2(Tuple2(input1, input2))
    )
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, inputs(1))
}

object MVMul extends BuiltinExprFun {
  override def args: Int = 2
  def apply(matrix: Expr, vector: Expr): BuiltinExpr =
    Map(
      {
        val param = ParamDef(matrix.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          DotProduct(vector, ParamUse(param))
        )
      },
      matrix
    )
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, inputs(1))
}

object MMMul extends BuiltinExprFun {
  override def args: Int = 2
  def apply(matrix1: Expr, matrix2: Expr): BuiltinExpr =
    Map(
      {
        val param = ParamDef(matrix1.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          MVMul(matrix2, ParamUse(param))
        )
      },
      matrix1
    )
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, inputs(1))
}

case class SlideExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SlideExpr = SlideExpr(newInnerIR, newTypes)
}
object Slide extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, windowWidth: ArithTypeT): SlideExpr = SlideExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = SeqTypeVar(dtv, lentv)
    val outseqt = SeqType(SeqType(dtv, windowWidth), lentv - windowWidth.ae + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  }, Seq(windowWidth))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SlideExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SlideExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

// TODO merge into Slide. This one by default generates a stm of stm at Arch level.
case class SlideGeneralExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SlideGeneralExpr = SlideGeneralExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case SlideGeneral(input, windowWidth, stepSize, _) =>
            Some(formatter.makeAtomic(s"slide-general ${formatter.formatAtomic(windowWidth)} ${formatter.formatAtomic(stepSize)} ${formatter.formatAtomic(input)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}
object SlideGeneral extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, windowWidth: ArithTypeT, stepSize: ArithTypeT): SlideGeneralExpr = SlideGeneralExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = SeqTypeVar(dtv, lentv)
    val outseqt = SeqType(SeqType(dtv, windowWidth), (lentv  - windowWidth.ae) / stepSize.ae + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  }, Seq(windowWidth, stepSize))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SlideGeneralExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: SlideGeneralExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

object JoinAll extends BuiltinExprFun {
  override def args: Int = 1
  @tailrec
  def apply(input: Expr): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    inputtc.t match {
      case st: SeqTypeT if
        (st.et match {
          case _: SeqTypeT => true
          case _ => false
        }) =>
        JoinAll(Join(inputtc))
      case _ =>
        inputtc.asInstanceOf[BuiltinExpr]
    }
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head)
}

object SplitAll extends BuiltinExprFun {
  override def args: Int = 1
  @tailrec
  def apply(input: Expr, dims: Seq[ArithTypeT]): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    if(dims.nonEmpty) {
      SplitAll(Split(inputtc, dims.last), dims.init)
    } else {
      inputtc.asInstanceOf[BuiltinExpr]
    }
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head, types.asInstanceOf[Seq[ArithTypeT]])
}

case class IfelseExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): IfelseExpr = IfelseExpr(newInnerIR)
}
object Ifelse extends BuiltinExprFun {
  override def args: Int = 3

  def apply(function1: Expr, function2: Expr, input: Expr): IfelseExpr = IfelseExpr({
    val seltv = IntTypeVar()
    val intv = AlgoDataTypeVar()
    val intupletv = TupleTypeVar(intv, seltv)
    val outtv = AlgoDataTypeVar()
    val f1tv = FunTypeVar(intv, outtv)
    val f2tv = FunTypeVar(intv, outtv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(f1tv, f2tv, intupletv), AlgoFunType(Seq(f1tv, f2tv, intupletv), outtv))), Seq(function1.t, function2.t, input.t)), Seq(function1, function2, input))
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): IfelseExpr = apply(inputs.head, inputs(1), inputs(2))

  def unapply(expr: IfelseExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

/*
 ***********************************************************************************************************************
 data type conversions / rearrangement
 ***********************************************************************************************************************
 */
case class SeqToUpperBoundedSeqExpr private[algo](innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SeqToUpperBoundedSeqExpr = SeqToUpperBoundedSeqExpr(newInnerIR, newTypes)
}
object SeqToUpperBoundedSeq extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, maxLength: ArithTypeT): SeqToUpperBoundedSeqExpr = SeqToUpperBoundedSeqExpr({
    val instv = SeqTypeVar(AlgoDataTypeVar(), ArithTypeVar())
    val outst = ConditionalTypeVar(
      instv,
      {
        case SeqType(dtv, len) if len.ae.evalInt <= maxLength.ae.evalInt => UpperBoundedSeqType(dtv, maxLength)
        case SeqType(_, len) => throw MalformedExprException(this.getClass().getName() + " cannot convert an Seq of length " +
          len.ae.evalInt + " into an UpperBoundedSeq of max length " + maxLength.ae.evalInt + ".")
      }
    )
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(instv, AlgoFunType(instv, outst))), input.t), input)
  }, Seq(maxLength))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SeqToUpperBoundedSeqExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SeqToUpperBoundedSeqExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class TupleExpr private[algo](innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TupleExpr = TupleExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Tuple(args, _) =>
            val formattedArgs = args.map(formatter.formatAtomic).mkString(" ")
            Some(formatter.makeAtomic(s"tuple ${formattedArgs}"))
        }
      case _ => None
    }
  }
}

object Tuple {
  def apply(inputs: Expr*): TupleExpr = TupleExpr({
    val dtvs = inputs.map(_ => AlgoDataTypeVar())
    val outtt = TupleType(dtvs: _*)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtvs, AlgoFunType(dtvs, outtt))), inputs.map(_.t)), inputs)
  })

  def unapply(expr: TupleExpr): Some[(Seq[Expr], Type)] =
    Some((expr.args, expr.t))
}

object Tuple2 extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr): TupleExpr = Tuple(input1, input2)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TupleExpr = apply(inputs.head, inputs(1))
  def unapply(expr: TupleExpr): Option[(Expr, Expr, Type)] = expr match {
    case Tuple((Seq(x, y), t)) => Some((x, y, t))
    case _ => None
  }
}

case class FlipTupleExpr private[algo](innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): FlipTupleExpr = FlipTupleExpr(newInnerIR, newTypes)
}

object FlipTuple extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, n: ArithTypeT): FlipTupleExpr = FlipTupleExpr({
    val len = n.ae.evalInt
    val dtvs = Seq.range(0, len).map(_ => AlgoDataTypeVar())
    val intv = TupleTypeVar(dtvs: _*)
    val outtt = TupleType(dtvs.reverse: _*)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, AlgoFunType(intv, outtt))), input.t), input)
  }, Seq(n))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FlipTupleExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: FlipTupleExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object FlipTuple2 extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): FlipTupleExpr = FlipTuple(input, 2)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): FlipTupleExpr = apply(inputs.head)
  def unapply(expr: FlipTupleExpr): Option[(Expr, Type)] = expr match {
    case FlipTuple((e, n, t)) if n.ae.evalInt == 2 => Some((e, t))
    case _ => None
  }
}

case class ZipExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ZipExpr = ZipExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Zip(tuple, _, _) =>
            Some(formatter.makeAtomic(s"zip ${formatter.formatAtomic(tuple)}"))
        }
    }
  }
}

object Zip extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, n: ArithTypeT): ZipExpr = ZipExpr({
    val elts = n.ae.evalInt
    val dtvs = Seq.range(0, elts).map(_ => AlgoDataTypeVar())
    val lentv = ArithTypeVar()
    val instvs = dtvs.map(t => SeqTypeVar(t, lentv))
    val inttv = TupleTypeVar(instvs: _*)
    val outst = SeqType(TupleType(dtvs: _*), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, AlgoFunType(inttv, outst))), input.t), input)
  }, Seq(n))

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipExpr =
    apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: ZipExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

object Zip2 extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): ZipExpr = Zip(input, 2)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipExpr = apply(inputs.head)
  def unapply(expr: ZipExpr): Option[(Expr, Type)] = expr match {
    case Zip(e, n, t) if n.ae.evalInt == 2 => Some((e, t))
    case _ => None
  }
}

object Tuple3 extends BuiltinExprFun {
  override def args: Int = 3
  def apply(input1: Expr, input2: Expr, input3: Expr): TupleExpr = Tuple(input1, input2, input3)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TupleExpr = apply(inputs.head, inputs(1), inputs(2))
  def unapply(expr: TupleExpr): Option[(Expr, Expr, Expr, Type)] = expr match {
    case Tuple((Seq(x, y, z), t)) => Some((x, y, z, t))
    case _ => None
  }
}

object Zip3 extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): ZipExpr = Zip(input, 3)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipExpr = apply(inputs.head)
  def unapply(expr: ZipExpr): Option[(Expr, Type)] = expr match {
    case Zip(e, n, t) if n.ae.evalInt == 3 => Some((e, t))
    case _ => None
  }
}

case class SplitExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SplitExpr = SplitExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Split(input, n, _) =>
            Some(formatter.makeAtomic(s"split ${formatter.formatAtomic(n)} ${formatter.formatAtomic(input)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}
object Split extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, chunkSize: ArithTypeT): SplitExpr = apply(input, chunkSize, innerSize = true)
  def apply(input: Expr, size: ArithTypeT, innerSize: Boolean): SplitExpr = {
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = SeqTypeVar(dtv, lentv)

    val (chunkSize: ArithExpr, numberOfChunks: ArithExpr) =
      if (innerSize)
        (size.ae, lentv.ae / size.ae)
      else
        (lentv.ae / size.ae, size.ae)

    SplitExpr({
      val outseqt = SeqType(SeqType(dtv, chunkSize), numberOfChunks)
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
    }, Seq(chunkSize))
  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SplitExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: SplitExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class JoinExpr private[algo] (innerIR: Expr) extends AlgoExpr {
  override def types: Seq[Type] = Seq()
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): JoinExpr = JoinExpr(newInnerIR)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Join(input, _) => Some(formatter.makeAtomic(s"join ${formatter.formatAtomic(input)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}
object Join extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): JoinExpr = JoinExpr({
    val dtv = AlgoDataTypeVar()
    val innerlentv = ArithTypeVar()
    val outerlentv = ArithTypeVar()
    val inseqtv = SeqTypeVar(SeqTypeVar(dtv, innerlentv), outerlentv)
    val outseqt = SeqType(dtv, innerlentv.ae * outerlentv.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  })
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): JoinExpr = apply(inputs.head)
  def unapply(expr: JoinExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class DropExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): DropExpr = DropExpr(newInnerIR, newTypes)
}
object Drop extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, firstElements: ArithTypeT, lastElements: ArithTypeT): DropExpr = DropExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = SeqTypeVar(dtv, lentv)
    val outseqt = SeqType(dtv, lentv.ae - firstElements.ae - lastElements.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  }, Seq(firstElements, lastElements))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): DropExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT])
  def unapply(expr: DropExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.t))
}

case class PadExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PadExpr = PadExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Pad(input, firstElements, lastElements, value, _) =>
            Some(formatter.makeAtomic(s"pad ${formatter.formatAtomic(input)} ${formatter.formatAtomic(firstElements)} ${formatter.formatAtomic(lastElements)} ${formatter.formatAtomic(value)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}
object Pad extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, firstElements: ArithTypeT, lastElements: ArithTypeT, value: ArithTypeT): PadExpr = PadExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = SeqTypeVar(dtv, lentv)
    val outseqt = SeqType(dtv, lentv.ae + firstElements.ae + lastElements.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  }, Seq(firstElements, lastElements, value))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): PadExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT], types(1).asInstanceOf[ArithTypeT], types(2).asInstanceOf[ArithTypeT])
  def unapply(expr: PadExpr): Some[(Expr, ArithTypeT, ArithTypeT, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ArithTypeT], expr.types(2).asInstanceOf[ArithTypeT], expr.t))
}

case class ConcatExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConcatExpr = ConcatExpr(newInnerIR, newTypes)
}
object Concat extends BuiltinExprFun {
  override def args: Int = 2
  def apply(input1: Expr, input2: Expr, dimension: ArithTypeT = ArithType(0)): ConcatExpr = ConcatExpr({
    val (instv1, instv2, outst) = getSeqTypes(dimension.ae.evalInt)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(instv1, instv2), AlgoFunType(Seq(instv1, instv2), outst))), Seq(input1.t, input2.t)), Seq(input1, input2))
  }, Seq(dimension))

  private def getSeqTypes(dimension: Int): (SeqTypeVar, SeqTypeVar, SeqType) = dimension match {
    case 0 =>
      val dtv = AlgoDataTypeVar()
      val lentv1 = ArithTypeVar()
      val lentv2 = ArithTypeVar()
      (
        SeqTypeVar(dtv, lentv1),
        SeqTypeVar(dtv, lentv2),
        SeqType(dtv, lentv1 + lentv2)
      )
    case _ =>
      val lentv = ArithTypeVar()
      val streamTypes = getSeqTypes(dimension - 1)
      (
        SeqTypeVar(streamTypes._1, lentv),
        SeqTypeVar(streamTypes._2, lentv),
        SeqType(streamTypes._3, lentv)
      )
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ConcatExpr = apply(inputs.head, inputs(1), types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: ConcatExpr): Some[(Expr, Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class PermuteExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PermuteExpr = PermuteExpr(newInnerIR, newTypes)
}
object Permute extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, permuteFun: ArithLambdaType): PermuteExpr = PermuteExpr({
    val dtv = AlgoDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = SeqTypeVar(dtv, lentv)
    val outseqt = SeqType(dtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  }, Seq(permuteFun))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): PermuteExpr = apply(inputs.head, types.head.asInstanceOf[ArithLambdaType])
  def unapply(expr: PermuteExpr): Some[(Expr, ArithLambdaType, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithLambdaType], expr.t))
}

// Dummy primitive
case class TransposeNDExpr private[algo] (innerIR: Expr, types: Seq[Type]) extends AlgoExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TransposeNDExpr = TransposeNDExpr(newInnerIR, newTypes)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case TransposeND(input, dimOrder, _) =>
            val dims = dimOrder.map(formatter.formatAtomic).mkString(", ")
            Some(formatter.makeAtomic(s"transpose-nd ($dims) ${formatter.formatAtomic(input)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}
object TransposeND extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr, dimOrder: Seq[ArithTypeT]): TransposeNDExpr = TransposeNDExpr({
    def genReorderedSeqType(dimOrder: Seq[ArithTypeT], lentvSeq: Seq[ArithTypeT], dtv: AlgoDataTypeVar): SeqType = {
      assert(dimOrder.nonEmpty, "dimOrder must have at least one element")
      if (dimOrder.length <= 1) {
        SeqType(dtv, lentvSeq(dimOrder.last.ae.evalInt))
      } else {
        SeqType(genReorderedSeqType(dimOrder.init, lentvSeq, dtv), lentvSeq(dimOrder.last.ae.evalInt))
      }
    }

    val dtv = AlgoDataTypeVar()
    val lentvSeq = Seq.fill(dimOrder.length)(ArithTypeVar())
    val inseqtv = SeqTypeVar(lentvSeq, dtv)
    val outseqt = genReorderedSeqType(dimOrder, lentvSeq, dtv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  }, dimOrder)
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TransposeNDExpr = apply(inputs.head, types.asInstanceOf[Seq[ArithTypeT]])
  def unapply(expr: TransposeNDExpr): Some[(Expr, Seq[ArithTypeT], Type)] =
    Some((expr.args.head, expr.types.asInstanceOf[Seq[ArithTypeT]], expr.t))
}

object Transpose extends BuiltinExprFun {
  override def args: Int = 1
  def apply(input: Expr): BuiltinExpr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val height = inputtc.t.asInstanceOf[SeqType].len.ae.evalInt
    val width = inputtc.t.asInstanceOf[SeqType].et.asInstanceOf[SeqType].len.ae.evalInt
    val v = ArithTypeVar()
    Split(Permute(Join(inputtc), ArithLambdaType(v, v/width + (v%width) * height)), height)

  }
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): BuiltinExpr = apply(inputs.head)
}
