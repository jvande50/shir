package algo

import core._

import scala.annotation.tailrec

case object AlgoFunTypeKind extends Kind

trait AlgoFunTypeT extends FunTypeT {
  override def kind: Kind = AlgoFunTypeKind

  override def superType: Type = superFunType
}

final case class AlgoFunType(inType: Type, outType: Type) extends AlgoFunTypeT {
  override def build(newChildren: Seq[ShirIR]): FunTypeT = AlgoFunType(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        val rhs = outType match {
          case _: AlgoFunType => formatter.formatNonAtomic(outType)
          case _ => formatter.formatAtomic(outType)
        }
        Some(formatter.makeAtomic(s"${formatter.formatAtomic(inType)} -> $rhs"))
      case _ => None
    }
  }
}

object AlgoFunType {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type): AlgoFunType = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      AlgoFunType(inTypes.head, outType)
    } else {
      AlgoFunType(inTypes.tail, AlgoFunType(inTypes.head, outType))
    }
  }
}

final case class AlgoFunTypeVar(inType: Type = AnyTypeVar(), outType: Type = AnyTypeVar(), tvFixedId: Option[Long] = None) extends AlgoFunTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): AlgoFunTypeVar = AlgoFunTypeVar(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type], tvFixedId)

  override def upperBound: Type = AlgoFunType(inType, outType)
}

object AlgoFunTypeVar {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type): AlgoFunTypeVar = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      AlgoFunTypeVar(inTypes.head, outType)
    } else {
      AlgoFunTypeVar(inTypes.tail, AlgoFunTypeVar(inTypes.head, outType))
    }
  }
}
