package core

import core.commonSubtree.FunHelper
import core.util.Log
import lift.arithmetic.{NotSolvableException, SolveForVariable}

import scala.annotation.tailrec
import scala.collection.mutable

final case class TypeCheckerException(private val message: String) extends Exception(message)

object TypeChecker {

  private final def permissive: Boolean = false // for debugging only

  private case class DebugInfo(msg: String)

  private case class Assigment(t: Type, subType: Type, debugInfo: DebugInfo)

  private trait BoundT {
    val t: Type
    val debugInfo: DebugInfo
  }
  private case class UpperBound(t: Type, debugInfo: DebugInfo) extends BoundT
  private case class LowerBound(t: Type, debugInfo: DebugInfo) extends BoundT

  def check(ir: Expr): Expr = {
    val tBefore = System.nanoTime()
    Log.info(this, "starting type checking")

    // insert type vars for UnknownType, then solve all type vars (including any newly introduced vars)
    val irVars = replaceUnknownWithTypeVars(ir)
    val typeMap = solveTypeVarsImpl(irVars)

    val result = irVars.visitAndRebuild({
      case node: TypeVarT => typeMap.getOrElse(node, node)
      case node => node
    }).asInstanceOf[Expr]

    val tAfter = System.nanoTime()
    Log.info(this, "finished type checking after " + (tAfter - tBefore) + "ns")

    result
  }

  private def assignmentsToBounds(assignmentPairs: Seq[Assigment]): Map[TypeVarT, (Seq[LowerBound], Seq[UpperBound])] = {
    val bounds = assignmentPairs.flatMap(createBounds) // assigments to bounds
      .groupBy(_._1).mapValues(_.map(_._2).toList) // group by type var (left side of assigment)
      .map(p => (p._1, p._2.partition { // group lower and upper bounds
        case LowerBound(_, _) => true
        case UpperBound(_, _) => false
      }.asInstanceOf[(Seq[LowerBound], Seq[UpperBound])]))

    bounds.foreach(p => {
      val (lbs, ubs) = p._2
      checkBounds(lbs, ubs)
    })

    val alignedBounds = bounds.map(p => {
      val (tv, (lbs, ubs)) = p
      (tv, alignBounds(lbs, ubs))
    })

    alignedBounds
  }

  /**
   * Type-checks `expr` if and only if `t` is `UnknownType`.
   * @param expr An expression to possibly type check.
   * @param t A type that will be compared against `UnknownType`.
   * @return A type-checked version of `expr`, if `t` equals `UnknownType`; otherwise, `expr`.
   */
  def checkIfUnknown(expr: Expr, t: Type): Expr = {
    if (t.hasUnknownType)
      check(expr)
    else
      expr
  }

  /**
   * Replaces all instances of `UnknownType` in `ir` with fresh type variables.
   * @param ir An expression to process.
   * @return A version of `ir` that contains fresh type variables instead of `UnknownType`.
   */
  def replaceUnknownWithTypeVars(ir: Expr): Expr = {
    // insert type vars for UnknownType
    ir.visitAndRebuild({
      case UnknownType => AnyTypeVar()
      case t => t
    }).asInstanceOf[Expr]
  }

  /**
   * Solves a system of type constraints. If the system of constraints has a solution, said solution is returned
   * in the form of a mapping of type variables to types.
   * @param constraints A system of type constraints as a sequence of `(supertype, subtype)` pairs such that `subtype`
   *                    must be a subtype of `supertype`.
   * @return `None` if the system of constraints has no solution; otherwise, a mapping of type variables to types.
   */
  def solveConstraints(constraints: Seq[(Type, Type)]): Option[mutable.Map[TypeVarT, Type]] = {
    try {
      val alignedBounds: Map[TypeVarT, (Seq[LowerBound], Seq[UpperBound])] =
        assignmentsToBounds(constraints.map(p => Assigment(p._1, p._2, DebugInfo("constraint"))))

      // new approach
      //    val typeMap = solve(alignedBounds)

      // old approach
      Some(solveAlignedBounds(alignedBounds))
    }
    catch {
      case TypeCheckerException(_) => None
    }
  }

  /**
   * Solves type variables in expression `ir`, producing a mapping of type variables to the types to which they were
   * resolved.
   * @param ir An expression whose type variables are to be resolved.
   * @return A mapping of type variables in `ir` to types.
   */
  def solveTypeVars(ir: Expr): mutable.Map[TypeVarT, Type] = solveTypeVarsImpl(replaceUnknownWithTypeVars(ir))

  /**
   * boundedTypeVars contains the TypeVars used (or bounded) in TypeLambdas.
   * typeLambdaTemplates stores the original Assignments and outpyt type.
   */
  private var boundedTypeVars: Seq[TypeVarT] = Seq()
  private var typeLambdaTemplates: Map[TypeVarT, (Seq[Assigment], Type)] = Map()

  private def solveTypeVarsImpl(ir: Expr) = {
    boundedTypeVars = Seq()
    typeLambdaTemplates = Map()
    val assignmentPairs = collectAssignments(ir, DebugInfo("root expression"))
    val alignedBounds: Map[TypeVarT, (Seq[LowerBound], Seq[UpperBound])] = assignmentsToBounds(assignmentPairs)

    // new approach
    //    val typeMap = solve(alignedBounds)

    // old approach
    solveAlignedBounds(alignedBounds)
  }

  private def solveAlignedBounds(alignedBounds: Map[TypeVarT, (Seq[LowerBound], Seq[UpperBound])]) = {
    val assignments: Assignments = new Assignments()
    alignedBounds.foreach(p => (p._2._1 ++ p._2._2).foreach {
      case LowerBound(t, _) => assignments.addLowerBound(p._1, t)
      case UpperBound(t, _) => assignments.addUpperBound(p._1, t)
    })
    assignments.solve()
  }

  def checkAssert(expr: Expr): Expr = {
    val exprChecked = check(expr)
    assert(!exprChecked.hasUnknownType, "Expression contains free type variables. Cannot lower IR.")
    exprChecked
  }

  def createException(msg: String, debugInfo: DebugInfo): TypeCheckerException = TypeCheckerException("in " + debugInfo + ": " + msg)

  /**
   * Replace All TypeVars in Type with new ones.
   * Note that we have to store the replacements to maintain the type equations.
   */
  private var newTvMap: Map[Long, TypeVarT] = Map()
  private def replaceTypeVars(t: Type): Type = t.visitAndRebuild{
    case tv: TypeVarT if !newTvMap.contains(tv.tvid) =>
      val newTv = tv.buildTV(tv.children)
      newTvMap = newTvMap + (tv.tvid -> newTv.asInstanceOf[TypeVarT])
      newTv
    case tv: TypeVarT => newTvMap.get(tv.tvid).get
    case t => t
  }.asInstanceOf[Type]

  /**
   * traverses tree and collect assignments (e.g. of types to type variables)
   * also throws exceptions if there are invalid assignments
   *
   */
  private def collectAssignments(ir: Expr, debugInfo: DebugInfo): Seq[Assigment] = {
    ir match {
      case ParamDef(_, _) => Seq()
      case ParamUse(p) => collectAssignments(p, debugInfo)
      case ExprVar(tt, _) if tt.hasUnknownType =>
        // It seems no other case is using this primitive (ExprVar), we might want to hack this.
        tt.visit{
          case ttv: TypeVarT if !ttv.isInstanceOf[AnyTypeVar] =>
            // Only adds partially known TypeVars (aka not AnyTypeVar). This should allow type vars introduced by eqsat
            // to be bounded. (Then they cannot be replaced by the other types.)
            boundedTypeVars = boundedTypeVars :+ ttv
          case _ =>
        }
        Seq()
      case Value(_) | ExprVar(_, _) => Seq()
      case Conversion(input, _) => collectAssignments(input, debugInfo)
      case Marker(input, _) => collectAssignments(input, debugInfo)
      case l: LambdaT =>
        var paramTypes: Seq[Assigment] = Seq()
        l.body.visit({
          case ParamUse(pd) if pd.id == l.param.id =>
            paramTypes = paramTypes ++ Seq(Assigment(l.param.t, pd.t, debugInfo))
          case _ =>
        })

        paramTypes ++
          collectAssignments(l.param, debugInfo) ++ // not necessary
          // connect param definition type and param use types:
          collectAssignments(l.body, debugInfo) :+
          Assigment(l.t.inType, l.param.t, debugInfo) :+ // not necessary
          Assigment(l.body.t, l.t.outType, debugInfo)
      case _: FunctionT => Seq()
      case FunctionCall(f: Expr, arg: Expr, t: Type) =>
        (f.t match {
          case ft: FunTypeT =>
            // see https://docs.scala-lang.org/tour/variances.html for the order of arguments
            // inType => outType (defined function signature) must be a subtype of arg.t => t (given function signature)
            // ==> arg.t must be a subtype of inType && outType must be a subtype of t
            Seq(
              Assigment(ft.inType, arg.t, debugInfo),
              Assigment(t, ft.outType, debugInfo)
            )
          case tv: TypeVarT =>
            // version long
            // val ftv = FunTypeVar(AnyTypeVar(), AnyTypeVar())
            // add(ftv, tv)
            // add(ftv.inType, arg.t)
            // add(t, ftv.outType)
            // version short
            Seq(
              Assigment(FunTypeVar(arg.t, t), tv, debugInfo) // well that's a !#&%$ line of code
            )
          case _ =>
            if (!permissive) throw createException("expected FunType, found " + f.t, debugInfo)
            Seq()
        }) ++ ({
          val argAssignments = collectAssignments(arg, debugInfo)
          // Use first TypeVar in TypeLambda to help searching corresponding assignments.
          arg match {
            case TypeLambda(_: Expr, t: TypeFunTypeT) =>
              typeLambdaTemplates = typeLambdaTemplates + (t.tv -> (argAssignments, FunHelper.getTypeOutput(t)))
            case _ =>
          }
          argAssignments
        }) ++
          collectAssignments(f, debugInfo)
      case TypeLambda(body: Expr, t: TypeFunTypeT) =>
        // collect TypeVars bounded by TypeLambda so that they can be partially solved.
        boundedTypeVars = boundedTypeVars :+ t.tv
        collectAssignments(body, debugInfo) :+
          Assigment(body.t, t.outType, debugInfo)
      case _: TypeFunctionT => Seq()
      case tfc @ TypeFunctionCall(_: Expr, _: Type, t: Type) if FunHelper.checkSharedTypeFunc(tfc) =>
        // If we encounter let-TypeFun, we do not solve the types of original TypeFun directly.
        // Instead, we check if param is solved. If yes, a new copy of TypeFun is instantiated with new TypeVars.
        // After that, the input types are assigned to these new TypeVars.
        val args = FunHelper.getTypeFunCallInputs(tfc)
        val tf = FunHelper.getTypeFunCallParamUse(tfc)
        tf.t match {
          case tft@TypeFunType(firstTv: TypeVarT, _: Type) =>
            val origFunType = FunHelper.getTypeOutput(tft)

            // Replace all TypeVars in outType with new ones. We have to collect all replacements.
            newTvMap = Map()
            val newFunType = replaceTypeVars(origFunType)

            // Search assignments. If found, re-instantiate them.
            val newInTypes = FunHelper.getInputTypesFromFunType(newFunType)
            val replacedAssignments = typeLambdaTemplates.get(firstTv) match {
              case Some((origAssignments, origFunType)) =>
                // Create new Type and SubType. Replace all TypeVars.
                val replacedMiddleAssignments = origAssignments.map(a => {
                  val newAT = replaceTypeVars(a.t)
                  val newASubType = replaceTypeVars(a.subType)
                  Assigment(newAT, newASubType, a.debugInfo)
                })
                // Create new Output FunType.
                val replacedFunType = replaceTypeVars(origFunType)
                val replacedFunAssignment = Assigment(newFunType, replacedFunType, debugInfo)
                replacedMiddleAssignments :+ replacedFunAssignment
              case _ => Seq()
            }

            // collect old and new TypeVar mapping
            replacedAssignments ++ newInTypes.zip(args).map(e => Assigment(e._1, e._2, debugInfo)) :+ Assigment(t, newFunType, debugInfo)
          case _ => Seq()
        }
      case TypeFunctionCall(tf: Expr, arg: Type, t: Type) =>
        (tf.t match {
          case TypeFunType(tv: TypeVarT, outType: Type) =>
            Seq(
              Assigment(tv, arg, debugInfo),
              Assigment(t, outType, debugInfo)
            )
          case tv: TypeVarT =>
            val argTv = AnyTypeVar()
            Seq(
              Assigment(argTv, arg, debugInfo),
              Assigment(TypeFunType(argTv, t), tv, debugInfo)
            )
          case _ =>
            if (!permissive) throw createException("expected TypeFunType, found " + tf.t, debugInfo)
            Seq()
        }) ++
          collectAssignments(tf, debugInfo)
      case bi: BuiltinExpr =>
        Assigment(bi.t, bi.innerIR.t, debugInfo) +:
          collectAssignments(bi.innerIR, DebugInfo(bi.toShortString))
    }
  }

  /**
   * extracts assignments (type equations) and checks if both sides are unifiable
   */
  private def createBounds(a: Assigment): Seq[(TypeVarT, BoundT)] = a match {
    // both sides are equal => nothing to do
    case Assigment(t, subType, _) if t == subType => Seq()
    // if there is a typevar => add and check children
    case Assigment(t: TypeVarT, subType, debugInfo) if subType.isSubTypeOf(t).getOrElse(true) =>
      createBoundsTuple(t, LowerBound(subType, debugInfo)) +:
        createBoundsForChildren(a)
    case Assigment(t, subType: TypeVarT, debugInfo) if subType.isSubTypeOf(t).getOrElse(true) =>
      createBoundsTuple(subType, UpperBound(t, debugInfo)) +:
        createBoundsForChildren(a)
    // both sides are not TypeVars => check children
    case Assigment(t, subType, _) if subType.isSubTypeOf(t).getOrElse(true) =>
      createBoundsForChildren(a)
    // none of the above => error
    case Assigment(t, subType, debugInfo) =>
      if (!permissive) throw createException(t + " not unifiable with subtype " + subType, debugInfo)
      Seq()
  }

  @tailrec
  private def createBoundsForChildren(a: Assigment): Seq[(TypeVarT, BoundT)] = a match {
    case Assigment(t, subType, debugInfo) if t.kind != subType.kind =>
      val commonKind = t.getCommonSuperKind(subType)
      createBoundsForChildren(Assigment(t.getAsKind(commonKind), subType.getAsKind(commonKind), debugInfo))
    case Assigment(f: FunTypeT, fsub: FunTypeT, debugInfo) =>
      createBounds(Assigment(fsub.inType, f.inType, debugInfo)) ++
        createBounds(Assigment(f.outType, fsub.outType, debugInfo)) ++
        {
          val fRemainingChildren = f.children.filterNot(t => Seq(f.inType, f.outType).contains(t))
          val fsubRemainingChildren = fsub.children.filterNot(t => Seq(fsub.inType, fsub.outType).contains(t))
          fRemainingChildren.zip(fsubRemainingChildren).flatMap(p => createBounds(Assigment(p._1, p._2, debugInfo)))
        }
    case Assigment(t, subType, debugInfo) => // is same kind and no funtype
      t.children.zip(subType.children).flatMap(p => createBounds(Assigment(p._1, p._2, debugInfo)))
  }

  private def createBoundsTuple(tv: TypeVarT, bound: BoundT): (TypeVarT, BoundT) = bound match {
    case LowerBound(t, debugInfo) if !permissive && t.isSubTypeOf(tv) == Option(false) =>
      throw createException("lower bound " + t + " violates the lower bound of the typevar " + tv, debugInfo)
    case UpperBound(t, debugInfo) if !permissive && tv.isSubTypeOf(t) == Option(false) =>
      throw createException("upper bound " + t + " violates the upper bound of the typevar " + tv, debugInfo)
    case _ =>
      (tv, bound)
  }

  private def checkBounds(lbs: Seq[BoundT], ubs: Seq[BoundT]): Unit = {
    lbs.foreach(lbound => {
      val LowerBound(lb, debugInfo1) = lbound
      ubs.foreach(ubound => {
        val UpperBound(ub, _) = ubound
        if (lb.isSubTypeOf(ub) == Option(false)) {
          if (!permissive) throw createException("lower bound (" + lb + ") of type does not fit the upper bound (" + ub + ")", debugInfo1)
        }
      })
    })
  }

  //  private def mergeLowBounds(bounds: Seq[BoundT]): Seq[BoundT] = bounds match {
  //    case Seq(LowerBound(t1, _), LowerBound(t2, _), rest) =>
  //      val newBounds = LowerBound(findCommonLowerBound(t1, t2), _) +: rest
  //      mergeLowBounds(newBounds)
  //    case _ => bounds
  //  }


  private def alignBounds(lbs: Seq[LowerBound], ubs: Seq[UpperBound]): (Seq[LowerBound], Seq[UpperBound]) = {
    val lbTypes = lbs.map(_.t)
    val ubTypes = ubs.map(_.t)

    val alignedLbs = lbTypes.map(lb => {
      lbTypes.foldLeft(lb)(findCommonLowerBound)
    }).zip(lbs).distinct.map(p => LowerBound(p._1, p._2.debugInfo))
    val alignedUbs = ubTypes.map(ub => {
      ubTypes.foldLeft(ub)(findCommonLowestUpperBound)
    }).zip(ubs).distinct.map(p => UpperBound(p._1, p._2.debugInfo))

    (alignedLbs, alignedUbs)
  }

  private def findCommonLowerBound(t1: Type, t2: Type): Type = (t1, t2) match {
    case (t1, t2) if t1 == t2 => t1
    case (t1, t2) if t1.isSubTypeOf(t2) == Option(true) => t2
    case (t1, t2) if t2.isSubTypeOf(t1) == Option(true) => t1
    //    case (at: ArithTypeT, _) if at.ae.isEvaluable => at
    //    case (_, at: ArithTypeT) if at.ae.isEvaluable => at
    case (ft1: FunTypeT, ft2: FunTypeT) => // special case for funtype
      val ft = if (ft1.children.length <= ft2.children.length) ft1 else ft2
      // treat input and output type of FunTypes differently! The input is contravariant!
      ft.build(findCommonLowestUpperBound(ft1.inType, ft2.inType) +: ft1.children.zip(ft2.children).tail.map(p => findCommonLowerBound(p._1, p._2)))
    case (t1, t2) if t1.kind == t2.kind => // same kinds, create new type of this kind and recursively check children
      t1.build(t1.children.zip(t2.children).map(c => findCommonLowerBound(c._1, c._2)))
    case (t1, _) => // neither subtypes, not same kind => cannot merge
      t1
  }

  private def findCommonLowestUpperBound(t1: Type, t2: Type): Type = (t1, t2) match {
    case (t1, t2) if t1 == t2 => t1
    case (t1, t2) if t1.isSubTypeOf(t2) == Option(true) => t1
    case (t1, t2) if t2.isSubTypeOf(t1) == Option(true) => t2
    //    case (at: ArithTypeT, _) if at.ae.isEvaluable => at
    //    case (_, at: ArithTypeT) if at.ae.isEvaluable => at
    case (ft1: FunTypeT, ft2: FunTypeT) => // special case for funtype
      val ft = if (ft1.children.length <= ft2.children.length) ft1 else ft2
      // treat input and output type of FunTypes differently! The input is contravariant!
      ft.build(findCommonLowerBound(ft1.inType, ft2.inType) +: ft1.children.zip(ft2.children).tail.map(p => findCommonLowestUpperBound(p._1, p._2)))
    case (t1, t2) if t1.kind == t2.kind => // same kinds, create new type of this kind and recursively check children
      t1.build(t1.children.zip(t2.children).map(c => findCommonLowestUpperBound(c._1, c._2)))
    case (t1, _) => // neither subtypes, not same kind => cannot merge
      t1
  }

  /**
   * creates new entries for the type map, which result from integrating the new given solved type var into the map
   * thus, also existing entries are considered here!
   */
  private def addSolvedType(typeMap: Map[TypeVarT, Type], visitedTVs: Seq[TypeVarT], tv: TypeVarT, t: Type): Map[TypeVarT, Type] = typeMap.get(tv) match {
    case None =>
      Map(tv -> t) // no entry available, add whatever t is
    case Some(tvNew: TypeVarT) =>
      var newEntries: Map[TypeVarT, Type] = Map()
      if (!t.isInstanceOf[TypeVarT]) {
        newEntries += (tv -> t) // replace type var in entry with a specific type
        //solvedTypeMap.put(tvNew, t) // not needed will be dealt with recursively (better)
      }
      if (!visitedTVs.contains(tvNew)) { // only go further if there is no cycle dependency (we have not visited this tv's entry yet)
        newEntries ++= addSolvedType(typeMap ++ newEntries, visitedTVs ++ Seq(tvNew), tvNew, t)
      } else { // there is a circle (A=B, B=C, C=A) => break it and insert this new type (C=X)
        newEntries += (tv -> t)
      }
      newEntries ++= solveTypeChildren(typeMap ++ newEntries, tv, t)
      newEntries
    case Some(tt: Type) if !tt.hasUnknownType => // this tv is solved already! solve right hand sides of both equations
      solveTypes(typeMap, t, tt)
    case Some(tt: Type) => // there is an existing entry with still some type vars
      var newEntries = Map(tv -> t) // replace entry
      newEntries ++= solveTypes(typeMap ++ newEntries, t, tt) // but also solve type vars in old entry
      newEntries
  }

  /**
   * entry point to mark the two given types as equal
   */
  private def solveTypes(typeMap: Map[TypeVarT, Type], t1: Type, t2: Type): Map[TypeVarT, Type] = (t1, t2) match {
    case (t1, t2) if t1 == t2 => Map()
    case (tv1: TypeVarT, tv2: TypeVarT) =>
      var newEntries = addSolvedType(typeMap, Seq(tv1), tv1, tv2)
      newEntries ++= addSolvedType(typeMap ++ newEntries, Seq(tv2), tv2, tv1)
      newEntries ++= solveTypeChildren(typeMap ++ newEntries, t1, t2)
      newEntries
    case (tv: TypeVarT, t: Type) =>
      var newEntries = addSolvedType(typeMap, Seq(tv), tv, t)
      newEntries ++= solveTypeChildren(typeMap ++ newEntries, t1, t2)
      newEntries
    case (t: Type, tv: TypeVarT) =>
      var newEntries = addSolvedType(typeMap, Seq(tv), tv, t)
      newEntries ++= solveTypeChildren(typeMap ++ newEntries, t1, t2)
      newEntries
    case (at1: ArithTypeT, at2: ArithTypeT) =>
      if (at1.ae.isEvaluable && at2.ae.isEvaluable && at1.ae != at2.ae) { // are not type vars => they must have the same value!
        if (!permissive) throw TypeCheckerException("arith types " + at1 + " and " + at2 + " cannot be equal")
        Map()
      } else if (at1.children.length == at2.children.length && {
        at1.visitAndRebuild({
          case a if at1.children.contains(a) => at2.children(at1.children.indexOf(a))
          case a => a
        }) == at2}) { // at1 and at2 have the same structure, but different type vars
        at1.children.zip(at2.children).flatMap(p => solveTypes(typeMap, p._1, p._2)).toMap
      } else if (at1.children.length == 1 && !at2.children.contains(at1.children.head)) {
        try {
          addSolvedType(typeMap, Seq(at1.children.head), at1.children.head, ArithType(SolveForVariable(at1.ae, at2.ae)))
        } catch {
          case _: NotSolvableException =>
            Log.warn(this, "Solver lacks implementation to solve arithmetic equation: " + at1 + " = " + at2)
            Map()
        }
      } else if (at2.children.length == 1 && !at1.children.contains(at2.children.head)) {
        try {
          addSolvedType(typeMap, Seq(at2.children.head), at2.children.head, ArithType(SolveForVariable(at2.ae, at1.ae)))
        } catch {
          case _: NotSolvableException =>
            Log.warn(this, "Solver lacks implementation to solve arithmetic equation: " + at1 + " = " + at2)
            Map()
        }
      } else {
        Log.warn(this, "Could not solve arithmetic equation: " + at1 + " = " + at2)
        Map()
      }
    case _ if t1.kind == t2.kind && t1.kind != TextTypeKind =>
      solveTypeChildren(typeMap, t1, t2)
    case _ =>
      if (!permissive) throw TypeCheckerException("Types cannot be equal: " + t1 + " " + t2)
      Map()
  }

  private def solveTypeChildren(typeMap: Map[TypeVarT, Type], t1: Type, t2: Type): Map[TypeVarT, Type] = {
    var m = typeMap
    // independently of the cases above (which means: always!) match children if both types are of the same kind
    val commonKind = t1.getCommonSuperKind(t2)
    // do not compare children of ArithTypes! Does not make any sense!
    if (commonKind != ArithTypeKind) {
      // use explicit iteration here, zip is unsafe!
      val i1 = t1.getAsKind(commonKind).children.toIterator
      val i2 = t2.getAsKind(commonKind).children.toIterator
      while (i1.hasNext && i2.hasNext)
        m = solveTypes(m, i1.next(), i2.next())

      // e.g. solve (A, B) (A, B, C). It would succeed the previous check
      // but fail this one because their arity is different.
      if (i1.hasNext != i2.hasNext)
        throw TypeCheckerException("Types cannot be equal: " + t1 + " " + t2)
    }
    m
  }

  /**
   * solves all the assignments
   *
   */
  def solve(bounds: Map[TypeVarT, (Seq[LowerBound], Seq[UpperBound])]): Map[TypeVarT, Type] = {
    var typeMap: Map[TypeVarT, Type] = Map()

    for ((tv, (lbs, ubs)) <- bounds) {
      val newEntries: Map[TypeVarT, Type] = (lbs, ubs) match {
        case (Seq(), Seq()) =>
          throw new RuntimeException("never happens")
        case (Seq(), Seq(UpperBound(t, _))) => // if there is only an upper bound available, we use this, because we cannot be more specific
          solveTypes(typeMap, tv, t)
        case (Seq(LowerBound(t, _)), Seq()) =>
          solveTypes(typeMap, tv, t) // most specific type (the lower bound)
        case (Seq(LowerBound(lb, _)), Seq(UpperBound(ub, _))) => // typeMap most specific type and assign upper bound to be a subtype (or equal) to lower bound
          var newEntries = solveTypes(typeMap, tv, lb)
          newEntries ++= solveTypes(typeMap ++ newEntries, lb, ub)
          newEntries
        case (lbs, Seq()) =>
          val lbTypes = lbs.map(_.t)
          var newEntries = solveTypes(typeMap, tv, lbTypes.head)
          lbTypes.init.zip(lbTypes.tail).foreach(p => newEntries ++= solveTypes(typeMap ++ newEntries, p._1, p._2))
          newEntries
        case (Seq(), ubs) =>
          val ubTypes = ubs.map(_.t)
          var newEntries = solveTypes(typeMap, tv, ubTypes.head)
          ubTypes.init.zip(ubTypes.tail).foreach(p => newEntries ++= solveTypes(typeMap ++ newEntries, p._1, p._2))
          newEntries
        case (lbs, ubs) =>
          val lbTypes = lbs.map(_.t)
          val ubTypes = ubs.map(_.t)
          solveTypes(typeMap, tv, lbTypes.head) ++
            lbTypes.init.zip(lbTypes.tail).flatMap(p => solveTypes(typeMap, p._1, p._2)) ++
            solveTypes(typeMap, tv, ubTypes.head) ++
            ubTypes.init.zip(ubTypes.tail).flatMap(p => solveTypes(typeMap, p._1, p._2))
      }
      typeMap ++= newEntries
    }

    // replace right hand side typevars with entries from `typeMap` until (eventually) all right hand side typevars are removed

    //    var modified = false
    //    do {
    //      modified = false
    //      typeMap.foreach(e => {
    //        val (lhs, rhs) = e
    //        if (rhs.hasUnknownType) {
    //          val newRhs = rhs.visitAndRebuild(t => getType(typeMap, t.asInstanceOf[Type])).asInstanceOf[Type]
    //          // do not replace if the new right hand side is not a subtype of the left hand side
    //          if (newRhs != rhs && newRhs.isSubTypeOf(lhs).getOrElse(true)) {
    //            val newEntries = addSolvedType(typeMap, Seq(lhs), lhs, newRhs)
    ////            if (newEntries.exists{ bulshit
    ////              case (k, v) => typeMap.get(k).exists(_ != v)
    ////            }) {
    //              typeMap ++= newEntries
    //              modified = true
    ////            }
    //          }
    //        }
    //      })
    //    } while (modified) // as long as the type typeMap state is changed, we keep replacing

    var typeMapState = ""
    do {
      typeMapState = typeMap.toString()
      typeMap.foreach(e => {
        val LHS = e._1
        // TODO if this solve method is used, make parameters of `getType` immutable
        val newRHS = e._2.visitAndRebuild(t => getType(mutable.Map[TypeVarT, Type]() ++= typeMap, t.asInstanceOf[Type])).asInstanceOf[Type]
        // do not replace if the new right hand side is not a subtype of the left hand side
        if (newRHS.isSubTypeOf(LHS).getOrElse(true))
          typeMap ++= addSolvedType(typeMap, Seq(LHS), LHS, newRHS)
      })
    } while (typeMap.toString() != typeMapState) // as long as the type typeMap state is changed, we keep replacing

    if (typeMap.values.exists(_.hasUnknownType))
      Log.warn(this, "unsolved type vars left")

    typeMap
  }

  @tailrec
  private def getType(solvedTypeMap: mutable.Map[TypeVarT, Type], t: Type, visitedTVs: Seq[TypeVarT] = Seq()): Type = t match {
    case tv: TypeVarT =>
      solvedTypeMap.get(tv) match {
        case Some(tv_in: TypeVarT) =>
          val newVisitedTVs = visitedTVs :+ tv
          if (!visitedTVs.contains(tv_in)) {
            getType(solvedTypeMap, tv_in, newVisitedTVs)
          } else { // type var loop detected, return most specific one among them
            // remove anytypevars because they just serve as links but do not hold any useful information and will break this search for the most specific type
            newVisitedTVs.filterNot(_.kind == AnyTypeKind).foldRight(newVisitedTVs.head)((tv1, tv2) => if (tv1.isSubTypeOf(tv2).getOrElse(true)) tv1 else tv2)
          }
        case Some(typ: Type) if {
          // prevent substitution if the new type contains the original type var (as it occurs in some arith type vars)
          var hasRecursion = false
          typ.visit(childType => {
            if (childType == tv)
              hasRecursion = true
          })
          !hasRecursion
        } =>
          typ
        case _ => tv
      }
    case _ => t
  }

  private class Assignments {

    // lower bounds are further down in the type tree (closer to the leaves), upper bounds are further up in the type tree (closer to the root)
    private case class Bounds(lower: mutable.Set[Type], upper: mutable.Set[Type]) {
      override def toString: String = "lowerBound" + lower.toString() + "; upperBound" + upper.toString()
    }

    /**
     * maps typevars to their sets of lower and upper bounds
     */
    private val typeMap: mutable.Map[TypeVarT, Bounds] = mutable.Map.empty

    override def toString: String = "" + typeMap.map(entry => "\n\t" + entry._1 + " -> " + entry._2)

    /**
     * maps typevars to a single specific type, that matches all the constraints from the lower and upper bounds
     */
    private val solvedTypeMap: mutable.Map[TypeVarT, Type] = mutable.Map.empty

    def addLowerBound(tv: TypeVarT, lb: Type): Unit = {
      if (typeMap.isDefinedAt(tv)) {
        typeMap(tv).lower += lb
      } else {
        typeMap += ((tv, Bounds(mutable.Set(lb), mutable.Set.empty)))
      }
    }

    def addUpperBound(tv: TypeVarT, ub: Type): Unit = {
      if (typeMap.isDefinedAt(tv)) {
        typeMap(tv).upper += ub
      } else {
        typeMap += ((tv, Bounds(mutable.Set.empty, mutable.Set(ub))))
      }
    }

    private def mergeAssignments(): Unit = {
      typeMap.foreach(elem => {
        val lbs = elem._2.lower
        val uniqueLBPairs = for {
          (x, idxX) <- lbs.zipWithIndex
          (y, idxY) <- lbs.zipWithIndex
          if idxX < idxY
        } yield (x, y)

        uniqueLBPairs.foreach(pair => {
          if (!(pair._1.isInstanceOf[TypeVarT] && pair._2.isInstanceOf[TypeVarT])) { // can only merge if the two elements are both not typevars
            lbs.remove(pair._1)
            lbs.remove(pair._2)
            lbs.add(findCommonLowerBound(pair._1, pair._2))
          }
        })

        val ubs = elem._2.upper
        val uniqueUBPairs = for {
          (x, idxX) <- ubs.zipWithIndex
          (y, idxY) <- ubs.zipWithIndex
          if idxX < idxY
        } yield (x, y)

        uniqueUBPairs.foreach(pair => {
          if (!(pair._1.isInstanceOf[TypeVarT] && pair._2.isInstanceOf[TypeVarT])) { // can only merge if the two elements are both not typevars
            ubs.remove(pair._1)
            ubs.remove(pair._2)
            ubs.add(findCommonLowestUpperBound(pair._1, pair._2))
          }
        })
      })
    }

    /**
     * maps FunType (not FunTypeVar) to to another FunType. To solve the problem of FunType and HWFunType.
     */
    private val solvedFunTypeMap: mutable.Map[FunTypeT, FunTypeT] = mutable.Map.empty

    /**
     * We assume that all FunTypes have first two children for inType and outType.
     * One FunType's children will substitute the other FunType's.
     */
    private def isSubFunType(ft: FunTypeT, fSubType: FunTypeT): Boolean = {
      // check if they are the same.
      if(ft == fSubType)
        return true

      // Substitute input type and output type.
      val newFSubType = fSubType.build(Seq(ft.children.head, ft.children(1)) ++ fSubType.children.drop(2))
      newFSubType.isSubTypeOf(ft) == Some(true)
    }

    /**
     * has side effects!
     *
     * @param t1
     * @param t2
     * @return
     */
    private def findCommonLowerBound(t1: Type, t2: Type): Type = {
      (t1, t2) match {
        case (_, _) if t1.isSubTypeOf(t2) == Option(true) => t2
        case (_, _) if t2.isSubTypeOf(t1) == Option(true) => t1
        case (tv1: TypeVarT, _) =>
          val newTV = tv1.newTV()
          addLowerBound(newTV, t1)
          addLowerBound(newTV, t2)
          newTV
        case (_, tv2: TypeVarT) =>
          val newTV = tv2.newTV()
          addLowerBound(newTV, t1)
          addLowerBound(newTV, t2)
          newTV
        case (at1: ArithTypeT, at2: ArithTypeT) =>
          if (at1 == at2)
            at1
          else if (at1.ae.isEvaluable && at2.ae.isEvaluable && at1.ae.evalDouble != at2.ae.evalDouble)
            throw TypeCheckerException("cannot find common lower bounds for arith types " + at1 + " and " + at2)
          else if (at1.ae.isEvaluable)
            at1
          else if (at2.ae.isEvaluable)
            at2
          else if ({
            val at1vars = at1.ae.varList.map(_.asInstanceOf[TypeVarT])
            val at2vars = at2.ae.varList.map(_.asInstanceOf[TypeVarT])
            at1.visitAndRebuild({
              case a if at1vars.contains(a) => at2vars(at1vars.indexOf(a))
              case a => a
            }) == at2}) { // at1 and at2 have the same structure, but different type vars
            val at1vars = at1.ae.varList.map(_.asInstanceOf[TypeVarT])
            val at2vars = at2.ae.varList.map(_.asInstanceOf[TypeVarT])
            val newTVs = at1.ae.varList.map(_.asInstanceOf[TypeVarT].newTV()) // create new type vars
            newTVs.zip(at1vars).foreach(p => addLowerBound(p._1, p._2))
            newTVs.zip(at2vars).foreach(p => addLowerBound(p._1, p._2))
            val newAT = at1.visitAndRebuild({
              case tv if at1vars.contains(tv) => newTVs(at1vars.indexOf(tv))
              case t => t
            }).asInstanceOf[Type]
            newAT // return arithmetic expression with new type vars
          }
          else
            throw TypeCheckerException("cannot find common lower bounds for arith types " + at1 + " and " + at2)
        case (ft1: FunTypeT, ft2: FunTypeT) => // special case for funtype
          ft1.build(Seq(findCommonLowestUpperBound(ft1.inType, ft2.inType), findCommonLowerBound(ft1.outType, ft2.outType)))
        // TODO check remaining children, if ft1 or ft2 is a special FunType with more than 2 children
        case (_, _) if t1.kind == t2.kind => // same kinds, create new type of this kind and recursively check children
          t1.build(t1.children.zip(t2.children).map(c => findCommonLowerBound(c._1, c._2)))
        case (_, _) => // neither subtypes, not same kind => find common super type
          findCommonLowerBound(t1.superType, t2.superType)
      }
    }

    /**
     * has side effects!
     *
     * @param t1
     * @param t2
     * @return
     */
    private def findCommonLowestUpperBound(t1: Type, t2: Type): Type = {
      (t1, t2) match {
        case (_, _) if t1.isSubTypeOf(t2) == Option(true) => t1
        case (_, _) if t2.isSubTypeOf(t1) == Option(true) => t2
        case (tv1: TypeVarT, _) =>
          val newTV = tv1.newTV()
          addUpperBound(newTV, t1)
          addUpperBound(newTV, t2)
          newTV
        case (_, tv2: TypeVarT) =>
          val newTV = tv2.newTV()
          addUpperBound(newTV, t1)
          addUpperBound(newTV, t2)
          newTV
        case (at1: ArithTypeT, at2: ArithTypeT) =>
          if (at1 == at2)
            at1
          else if (at1.ae.isEvaluable && at2.ae.isEvaluable && at1.ae.evalDouble != at2.ae.evalDouble)
            throw TypeCheckerException("cannot find common upper bounds for arith types " + at1 + " and " + at2)
          else if (at1.ae.isEvaluable)
            at1
          else if (at2.ae.isEvaluable)
            at2
          else if ({
            val at1vars = at1.ae.varList.map(_.asInstanceOf[Type])
            val at2vars = at2.ae.varList.map(_.asInstanceOf[Type])
            at1.visitAndRebuild({
              case a if at1vars.contains(a) => at2vars(at1vars.indexOf(a))
              case a => a
            }) == at2}) { // at1 and at2 have the same structure, but different type vars
            val at1vars = at1.ae.varList.map(_.asInstanceOf[TypeVarT])
            val at2vars = at2.ae.varList.map(_.asInstanceOf[TypeVarT])
            val newTVs = at1.ae.varList.map(_.asInstanceOf[TypeVarT].newTV()) // create new type vars
            newTVs.zip(at1vars).foreach(p => addUpperBound(p._1, p._2))
            newTVs.zip(at2vars).foreach(p => addUpperBound(p._1, p._2))
            val newAT = at1.visitAndRebuild({
              case tv if at1vars.contains(tv) => newTVs(at1vars.indexOf(tv))
              case t => t
            }).asInstanceOf[Type]
            newAT // return arithmetic expression with new type vars
          }
          else
            throw TypeCheckerException("cannot find common upper bounds for arith types " + at1 + " and " + at2)
        case (ft1: FunTypeT, ft2: FunTypeT) => // special case for funtype
          ft1.build(Seq(findCommonLowerBound(ft1.inType, ft2.inType), findCommonLowestUpperBound(ft1.outType, ft2.outType)))
        // TODO check remaining children, if ft1 or ft2 is a special FunType with more than 2 children
        case (_, _) if t1.kind == t2.kind => // same kinds, create new type of this kind and recursively check children
          t1.build(t1.children.zip(t2.children).map(c => findCommonLowestUpperBound(c._1, c._2)))
        case (_, _) => // neither subtypes, not same kind => types do not have anything in common
          throw TypeCheckerException("cannot merge upperbounds " + t1 + " and " + t2)
      }
    }

    /**
     * this function should be called, when two types are declared to be equal (during the solving process)
     *
     * @param t1
     * @param t2
     */
    private def solveTypes(t1: Type, t2: Type): Unit = {
      if (t1 != t2) {
        (t1, t2) match {
          case (tv1: TypeVarT, tv2: TypeVarT) if boundedTypeVars.contains(tv1) && boundedTypeVars.contains(tv2) =>
            throw TypeCheckerException("Solver does not support equalization of bounded TypeVars" + t1 + " = " + t2 + ".")
          case (tv1: TypeVarT, tv2: TypeVarT) if boundedTypeVars.contains(tv2) =>
            addSolvedType(Seq(tv1), tv1, tv2)
          case (tv1: TypeVarT, tv2: TypeVarT) if boundedTypeVars.contains(tv1) =>
            addSolvedType(Seq(tv2), tv2, tv1)
          case (tv1: TypeVarT, tv2: TypeVarT) =>
            addSolvedType(Seq(tv1), tv1, tv2)
            addSolvedType(Seq(tv2), tv2, tv1)
          case (tv: TypeVarT, t: Type) => addSolvedType(Seq(tv), tv, t)
          case (t: Type, tv: TypeVarT) => addSolvedType(Seq(tv), tv, t)
          case (ft: FunTypeT, fSubType: FunTypeT) if isSubFunType(ft, fSubType) => addSolvedFunType(ft, fSubType)
          case (fSubType: FunTypeT, ft: FunTypeT) if isSubFunType(ft, fSubType) => addSolvedFunType(ft, fSubType)
          case (at1: ArithTypeT, at2: ArithTypeT) if at1.ae.isEvaluable && at2.ae.isEvaluable && at1.ae != at2.ae => // are not type vars => they must have the same value!
            if (!permissive) throw TypeCheckerException("arith types " + at1 + " and " + at2 + " cannot be equal")
          // TODO this case's condition may trigger a violation of an `assert` in the arith library ... update library to throw a (catchable) expression instead; and catch exception here
          //          case (at1: ArithTypeT, at2: ArithTypeT) if (at1.children.length == at2.children.length && {
          //            at1.visitAndRebuild({
          //              case a if at1.children.contains(a) => at2.children(at1.children.indexOf(a))
          //              case a => a
          //            }) == at2}) => // at1 and at2 have the same structure, but different type vars
          //            at1.children.zip(at2.children).foreach(p => solveTypes(p._1, p._2))
          case (at1: ArithTypeT, at2: ArithTypeT) if at1.children.length == 1 => // not type vars themselves, but might contain type vars as children
            try {
              addSolvedType(Seq(at1.children.head), at1.children.head, ArithType(SolveForVariable(at1.ae, at2.ae)))
            } catch {
              case _: NotSolvableException =>
                Log.warn(this, "Solver lacks implementation to solve arithmetic equation: " + at1 + " = " + at2)
                Map()
            }
          case (at1: ArithTypeT, at2: ArithTypeT) if at2.children.length == 1 => // not type vars themselves, but might contain type vars as children
            try {
              addSolvedType(Seq(at2.children.head), at2.children.head, ArithType(SolveForVariable(at2.ae, at1.ae)))
            } catch {
              case _: NotSolvableException =>
                Log.warn(this, "Solver lacks implementation to solve arithmetic equation: " + at1 + " = " + at2)
                Map()
            }
          case _ if t1.kind == t2.kind && t1.kind != TextTypeKind =>
            // nothing, children will be matched later
          case _ =>
            if (!permissive) throw TypeCheckerException("Types cannot be equal: " + t1 + " " + t2)
        }
        solveTypeChildren(t1, t2)
      }
    }

    private def solveTypeChildren(t1: Type, t2: Type): Unit = {
      // independently of the cases above (which means: always!) match children if both types are of the same kind
      val commonKind = t1.getCommonSuperKind(t2)
      // do not compare children of ArithTypes! Does not make any sense!
      if (commonKind != ArithTypeKind) {
        // use explicit iteration here, zip is unsafe!
        val i1 = t1.getAsKind(commonKind).children.toIterator
        val i2 = t2.getAsKind(commonKind).children.toIterator
        while (i1.hasNext && i2.hasNext)
          solveTypes(i1.next(), i2.next())

        // e.g. solve (A, B) (A, B, C). It would succeed the previous check
        // but fail this one because their arity is different.
        if (i1.hasNext != i2.hasNext)
          throw TypeCheckerException("Types cannot be equal: " + t1 + " " + t2)
      }
    }

    /**
     * Note boundedTypeVars are not solved because TypeFunctionCall can take different input types. A boundedTypeVar can
     * be used to solved another TypeVar. With this, TypeLambda's output type will be an equation of the boundedTypeVar.
     * These make TypeFunctionCall(ParamUse(p), tt) solvable with input type tt and type equation p.t.
     */
    private def addSolvedType(visitedTVs: Seq[TypeVarT], tv: TypeVarT, t: Type): Unit = {
      solvedTypeMap.get(tv) match {
        case None => solvedTypeMap.put(tv, t) // no entry available, add whatever t is
        case Some(tvNew: TypeVarT) if boundedTypeVars.contains(tvNew) => // boundedTypeVars should not be replaced.
        case Some(tvNew: TypeVarT) =>
          if (!t.isInstanceOf[TypeVarT] || boundedTypeVars.contains(t)) { // || t.isSubTypeOf(tvNew).getOrElse(false)) {
            solvedTypeMap.put(tv, t) // replace type var in entry with a specific type
            //solvedTypeMap.put(tvNew, t) // not needed will be dealt with recursively (better)
          }
          if (!visitedTVs.contains(tvNew)) { // only go further if there is no cycle dependency (we have not visited this tv's entry yet)
            addSolvedType(visitedTVs ++ Seq(tvNew), tvNew, t)
          } else { // there is a circle (A=B, B=C, C=A) => break it and insert this new type (C=X)
            solvedTypeMap.put(tv, t)
          }
          solveTypeChildren(tv, t)
        case Some(tt: Type) if !tt.hasUnknownType => // this tv is solved already! solve right hand sides of both equations
          solveTypes(t, tt)
        case Some(tt: Type) => // there is an existing entry with still some type vars
          solvedTypeMap.put(tv, t) // replace entry
          solveTypes(t, tt) // but also solve type vars in old entry
      }
    }

    /**
     * Add solved FunType pair (ft -> ftt) into solvedFunTypeMap. Note that we still need to solve their children.
     * We assume that only 2 children (inType and outType) need to be solved!
     */
    private def addSolvedFunType(ft: FunTypeT, ftt: FunTypeT): Unit = {
      solvedFunTypeMap.get(ft) match {
        case None => solvedFunTypeMap.put(ft, ftt)
        case Some(ftNew: FunTypeT) if isSubFunType(ftNew, ftt) => // Need replacement
          solvedFunTypeMap.put(ft, ftt)
        case Some(_: FunTypeT) =>
      }
      solveTypes(ft.inType, ftt.inType)
      solveTypes(ft.outType, ftt.outType)
    }

    /**
     * Exhaustively replace all the occurrences of ft in solvedTypeMap if it appears in solvedFunTypeMap!
     * We have to replace both key and value in solvedTypeMap.
     */
    private def updateSolvedTypeMap(): Unit = {
      val keys = solvedTypeMap.keySet
      keys.foreach(key => {
        val value = solvedTypeMap(key)

        var keyTypeFound: Boolean = false
        key.visit{
          case ft: FunTypeT if solvedFunTypeMap.contains(ft) =>
            solvedFunTypeMap.get(ft) match {
              case Some(_) =>
                keyTypeFound = true
              case _ => ft
            }
          case e => e
        }
        var valueTypeFound: Boolean = false
        value.visit{
          case ft: FunTypeT if solvedFunTypeMap.contains(ft) =>
            solvedFunTypeMap.get(ft) match {
              case Some(_) =>
                valueTypeFound = true
              case _ => ft
            }
          case e => e
        }

        val newKey = if(keyTypeFound) {
          key.visitAndRebuild {
            case ft: FunTypeT if solvedFunTypeMap.contains(ft) =>
              solvedFunTypeMap.get(ft) match {
                case Some(ftt) =>
                  ftt.build(Seq(ft.children.head, ft.children(1)) ++ ftt.children.drop(2))
                case _ => ft
              }
            case e => e
          }.asInstanceOf[TypeVarT]
        } else {
          key
        }

        val newValue = if(valueTypeFound) {
          value.visitAndRebuild {
            case ft: FunTypeT if solvedFunTypeMap.contains(ft) =>
              solvedFunTypeMap.get(ft) match {
                case Some(ftt) =>
                  ftt.build(Seq(ft.children.head, ft.children(1)) ++ ftt.children.drop(2))
                case _ => ft
              }
            case e => e
          }.asInstanceOf[Type]
        } else {
          value
        }

        if(keyTypeFound)
          solvedTypeMap.remove(key)
        if(keyTypeFound || valueTypeFound)
          solvedTypeMap.put(newKey, newValue)
      })
    }

    /**
     * solves all the assignments
     *
     */
    def solve(): mutable.Map[TypeVarT, Type] = {
      solvedTypeMap.clear()

      mergeAssignments()

      for ((tv, bounds) <- typeMap) {
        (bounds.lower.size, bounds.upper.size) match {
          case (0, 0) =>
            throw new RuntimeException("never happens")
          case (0, 1) =>
            solveTypes(tv, bounds.upper.head) // if there is only an upper bound available, we use this, because we cannot be more specific
          case (1, 0) =>
            solveTypes(tv, bounds.lower.head) // most specific type (the lower bound)
          case (1, 1) => // typeMap most specific type and assign upper bound to be a subtype (or equal) to lower bound
            solveTypes(tv, bounds.lower.head)
            solveTypes(bounds.lower.head, bounds.upper.head)
          case (_, 0) =>
            solveTypes(tv, bounds.lower.head)
            bounds.lower.init.zip(bounds.lower.tail).foreach(p => solveTypes(p._1, p._2))
          case (0, _) =>
            solveTypes(tv, bounds.upper.head)
            bounds.upper.init.zip(bounds.upper.tail).foreach(p => solveTypes(p._1, p._2))
          case (_, _) =>
            ???
        }
      }

      // Replace FunTypes
      updateSolvedTypeMap()

      // replace right hand side typevars with entries from `solvedTypeMap` until (eventually) all right hand side typevars are removed
      var typeMapState = ""
      do {
        typeMapState = solvedTypeMap.toString()
        solvedTypeMap.foreach(e => {
          val LHS = e._1
          val newRHS =
            e._2.visitAndRebuild({
              // replace type vars (with its specific upperbounds) if all its children are known now
              case tv: TypeVarGenT if tv.isInstanceOf[ComposedValueTypeT] && tv.children.nonEmpty && tv.children.forall(!_.hasUnknownType) =>
                tv.upperBound.visitAndRebuild(t => getType(solvedTypeMap, t.asInstanceOf[Type]))
              case t: Type =>
                getType(solvedTypeMap, t)
            }).asInstanceOf[Type]
          // do not replace if the new right hand side is not a subtype of the left hand side
          if ((newRHS.isSubTypeOf(LHS).getOrElse(true) || boundedTypeVars.contains(newRHS)) && !boundedTypeVars.contains(LHS))
            addSolvedType(Seq(LHS), LHS, newRHS)
        })
      } while (solvedTypeMap.toString() != typeMapState) // as long as the type typeMap state is changed, we keep replacing

      if (solvedTypeMap.values.exists(_.hasUnknownType))
        Log.warn(this, "unsolved type vars left")

      solvedTypeMap
    }
  }

}