package core

import scala.annotation.tailrec

final case class MalformedTypeException(private val message: String, private val cause: Throwable = None.orNull) extends Exception(message, cause)

sealed trait CoreType extends ValueTypeT with CoreIR

case object FunTypeKind extends Kind

trait FunTypeT extends CoreType {
  val inType: Type
  val outType: Type
  protected def superFunType: FunTypeT = FunType(inType, outType)

  override def kind: Kind = FunTypeKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq(inType, outType)
}

final case class FunType private[core] (inType: Type, outType: Type) extends FunTypeT {
  override def build(newChildren: Seq[ShirIR]): FunTypeT = FunType(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        val rhs = outType match {
          case _: FunType => formatter.formatNonAtomic(outType)
          case _ => formatter.formatAtomic(outType)
        }
        Some(formatter.makeAtomic(s"${formatter.formatAtomic(inType)} -> $rhs"))
      case _ => None
    }
  }
}

object FunType {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type): FunType = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      FunType(inTypes.head, outType)
    } else {
      FunType(inTypes.tail, FunType(inTypes.head, outType))
    }
  }
}

final case class FunTypeVar private[core] (inType: Type = UnknownType, outType: Type = UnknownType, tvFixedId: Option[Long] = None) extends FunTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): FunTypeVar = FunTypeVar(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type], tvFixedId)

  override def upperBound: Type = FunType(inType, outType)
}

object FunTypeVar {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type): FunTypeVar = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      FunTypeVar(inTypes.head, outType)
    } else {
      FunTypeVar(inTypes.tail, FunTypeVar(inTypes.head, outType))
    }
  }
}


case object TypeFunTypeKind extends Kind

sealed trait TypeFunTypeT extends CoreType {
  val tv: TypeVarT
  val outType: Type

  override def kind: Kind = TypeFunTypeKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq(tv, outType)

  private def collectTvs(): Seq[TypeVarT] = outType match {
    case tft: TypeFunTypeT => tv +: tft.collectTvs()
    case _ => Seq(tv)
  }

  def copy(): TypeFunTypeT = {
    val newIds = collectTvs().flatMap(_.flatten).distinct.filter(_.isInstanceOf[TypeVarT]).map(_.asInstanceOf[TypeVarT].tvid -> Counter(AnyTypeVar.getClass).next()).toMap
    visitAndRebuild({
      case tv: TypeVarT if newIds.isDefinedAt(tv.tvid) => tv.buildTV(tv.children, Some(newIds(tv.tvid)))
      case t => t
    }).asInstanceOf[TypeFunTypeT]
  }
}

final case class TypeFunType(tv: TypeVarT, outType: Type) extends TypeFunTypeT {
  override def build(newChildren: Seq[ShirIR]): Type = newChildren.head match {
    // only replace tv, if the new child is a type var too
    case tv: TypeVarT => TypeFunType(tv, newChildren(1).asInstanceOf[Type])
    case _ => TypeFunType(tv, newChildren(1).asInstanceOf[Type])
  }
}

object TypeFunType {
  @tailrec
  def apply(tvs: Seq[TypeVarT], outType: Type): TypeFunType = {
    if (tvs.isEmpty) {
      throw MalformedTypeException("Type Function Type must have at least one type variable")
    } else if (tvs.size == 1) {
      TypeFunType(tvs.head, outType)
    } else {
      TypeFunType(tvs.tail, TypeFunType(tvs.head, outType))
    }
  }
}

// never used ...
//final case class TypeFunTypeVar (tv: TypeVarT = AnyTypeVar(), outType: Type = AnyTypeVar(), tvFixedId: Option[Long] = None) extends TypeFunTypeT with TypeVarT {
//  override def buildTV(newChildren: Seq[LiftIR], tvFixedId: Option[Long]): TypeFunTypeVar = TypeFunTypeVar(tv, newChildren.head.asInstanceOf[Type], tvFixedId)
//}
