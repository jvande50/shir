package core.util

import core.Expr

import scala.sys.process.Process

final case class IRDotGraphAnimation(exprs: Seq[Expr]) extends ToFile {

  val HTML_FILE_EXTENSION: String = "html"
  val HTML_VIEWER: String = "xdg-open"

  override def toStringIterator: Iterator[String] = {
    (
      HTML_HEADER ++
      exprs.flatMap(
        Seq("[") ++
        IRDotGraph(_).toStringIterator.map("'" + _ + "',") ++
        Seq("],")
      ) ++
      HTML_FOOTER
    ).iterator
  }

  def show(): Unit = Process(HTML_VIEWER + " " + toFile("tmp." + HTML_FILE_EXTENSION)).!

  val HTML_HEADER: Seq[String] = Seq(
    "<!DOCTYPE html>",
    "  <meta charset='utf-8'>",
    "    <body>",
    "      <script src='https://d3js.org/d3.v5.min.js'></script>",
    "      <script src='https://unpkg.com/@hpcc-js/wasm@0.3.11/dist/index.min.js'></script>",
    "      <script src='https://unpkg.com/d3-graphviz@3.0.5/build/d3-graphviz.js'></script>",
    "      <div id='graph' style='text-align: center;'></div>",
    "      <script>",
    "        var dotIndex = 0;",
    "        var graphviz = d3.select('#graph').graphviz()",
    "        .transition(function () {",
    "        return d3.transition('main')",
    "          .ease(d3.easeLinear)",
    "          .delay(2000)",
    "          .duration(1500);",
    "        })",
    "        .logEvents(true)",
    "        .on('initEnd', render);",
    "        function render() {",
    "          var dotLines = dots[dotIndex];",
    "          var dot = dotLines.join('');",
    "          graphviz",
    "            .renderDot(dot)",
    "            .on('end', function () {",
    "              dotIndex = (dotIndex + 1) % dots.length;",
    "              render();",
    "            });",
    "        }",
    "var dots = ["
  )

  val HTML_FOOTER: Seq[String] = Seq(
    "];",
    "</script>"
  )

}
