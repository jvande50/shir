package core.util

import java.io.{File, PrintWriter}

trait ToFile {

  def toStringIterator: Iterator[String]

  final def toConsole: Unit =
    toStringIterator.foreach(line =>
      Log.info(this, line)
    )

  final def toFile(fileName: String, folderName: String = "."): String = {
    val folder = new File(folderName);
    if (!folder.exists() || !folder.isDirectory) {
      folder.mkdir()
    }

    val path = folderName + "/" + fileName
    val pw = new PrintWriter(new File(path))
    toStringIterator.foreach(line =>
      pw.write(line + "\n")
    )
    pw.close()

    Log.info(this, "file " + path + " written.")
    path
  }
}

trait ToFiles {

  def toStringIterators: Map[String, Iterator[String]]

  final def toConsole: Seq[String] = toStringIterators.map(elem => {
    Log.info(this, "========== " + elem._1 + " ==========")
    elem._2.foreach(line =>
      Log.info(this, line)
    )
    elem._1
  }).toSeq

  final def toFiles(folderName: String): Seq[String] = {
    val folder = new File(folderName);
    if (!folder.exists() || !folder.isDirectory) {
      folder.mkdir()
    }

    toStringIterators.map(elem => {
      val path = folderName + "/" + elem._1
      val pw = new PrintWriter(new File(path))
      elem._2.foreach(line =>
        pw.write(line + "\n")
      )
      pw.close()

      Log.info(this, "file " + path + " written.")
      path
    }).toSeq
  }

}
