package core.util

object Log {
  case class LogLevel(level: Int)
  val LevelAll: LogLevel = LogLevel(10000)
  val LevelInfo: LogLevel = LogLevel(3)
  val LevelWarning: LogLevel = LogLevel(1)
  val LevelError: LogLevel = LogLevel(0)

  private var currentLogLevel: LogLevel = LevelAll
  private def checkLevel(l: LogLevel): Boolean = currentLogLevel.level >= l.level
  def setLogLevel(l: LogLevel): Unit = currentLogLevel = l

  private var ignoreComponents: Seq[String] = Seq(core.TypeChecker.getClass.getName, classOf[backend.hdl.vhdl.VhdlProject].getName)
  private def checkComponent(c: Object): Boolean = !ignoreComponents.contains(c.getClass.getName)
  def ignoreOutputFrom(c: Object): Unit = ignoreComponents :+= c.getClass.getName

  def info(component: Object, msg: String): Unit = if (checkLevel(LevelInfo) && checkComponent(component)) println(component.getClass.getName.split("\\$").last + ": " + msg)
  def warn(component: Object, msg: String): Unit = if (checkLevel(LevelWarning) && checkComponent(component)) println(component.getClass.getName.split("\\$").last + ": " + msg)
  def error(component: Object, msg: String): Unit = if (checkLevel(LevelError) && checkComponent(component)) println(component.getClass.getName.split("\\$").last + ": " + msg)
}
