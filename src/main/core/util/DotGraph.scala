package core.util

import core.Counter

import scala.annotation.tailrec
import scala.sys.process._

sealed trait DotGraphComponent {
  def escapeStr(s: String): String = s.replace("\"", "\\\"")
}

final case class DotGraph(graph: DotSubGraph) extends DotGraphComponent with ToFile {

  val DOT_FILE_EXTENSION: String = "dot"
  val DOT_VIEWER: String = "xdot"

  override def toStringIterator: Iterator[String] =
    (
      Seq("digraph G {") ++
      graph.toStrings.map("\t" + _) ++
      Seq("}")
    ).iterator

  def show(): Unit = Process(DOT_VIEWER + " " + toFile("tmp." + DOT_FILE_EXTENSION)).!
}

final case class DotSubGraph(nodes: Seq[DotNode] = Seq(), connections: Seq[DotConnection] = Seq()) extends DotGraphComponent {
  def toStrings: Seq[String] = nodes.map(_.toString) ++ connections.map(_.toString)

  @tailrec
  def add(c: DotGraphComponent): DotSubGraph = c match {
    case DotSubGraph(newNodes, newConnections) =>
      DotSubGraph(nodes ++ newNodes, connections ++ newConnections)
    case node: DotNode =>
      DotSubGraph(nodes :+ node, connections)
    case con: DotConnection =>
      DotSubGraph(nodes, connections :+ con)
    case DotGraph(sub) => add(sub)
  }

  def addAll(c: Seq[DotGraphComponent]): DotSubGraph = c.foldLeft(this)((g1, g2) => g1.add(g2))

  def connectAsRootNode(root: DotNode, style: String = "", enumerateConnections: Boolean = true): DotSubGraph = {
    val oldRoots: Seq[DotNode] = nodes.filter(n => !connections.exists(_.destination == n.id))

    val newConnections: Seq[DotConnection] = if (enumerateConnections && oldRoots.length > 1)
      oldRoots.zipWithIndex.map(n => DotConnection(root, n._1, (n._2 + 1) + "", style))
    else {
      oldRoots.map(n => DotConnection(root, n, "", style))
    }
    this.addAll(root +: newConnections)
  }
}

object DotSubGraph {
  def apply(c: Seq[DotGraphComponent]): DotSubGraph = DotSubGraph().addAll(c)
}

object DotColor {
  final def HIGHLIGHT: String = BLUE
  final val BLUE: String = "blue"
}

sealed trait DotAttributes {
  def attributes: Map[String, String]

  def attributesSeq: Seq[String] = attributes.filter(p => p._2.nonEmpty).map(p => {
    if (p._1 == "label" || p._1 == "tooltip")
      p._1 + "=\"" + p._2 + "\""
    else
      p._1 + "=" + p._2
  }).toSeq

  def attributesString: String = {
    val str = attributesSeq.mkString(", ")
    if (str.isEmpty)
      ""
    else
      "[" + str + "]"
  }
}

final case class DotNode(label: String, shape: String = "", style: String = "", color: String = "", tooltip: String = "", subGraph: Option[DotSubGraph] = None, id: String = "node_" + Counter(DotNode.getClass).next()) extends DotGraphComponent with DotAttributes {
  override def attributes: Map[String, String] = Map("label" -> escapeStr(label), "shape" -> shape, "style" -> style, "color" -> color, "tooltip" -> escapeStr(tooltip))
  override def toString: String = subGraph match {
    case None =>
      id + attributesString + ";"
    case Some(g) =>
      "subgraph cluster_" + id + " {\n" +
      (attributesSeq ++ g.toStrings).map("\t" + _ + "\n").mkString + "\n\t}\n"
  }
}
object DotNode {
  final val RECTANGLE_SHAPE = "rectangle"
  final val SEPTAGON_SHAPE = "septagon"
  final val TRIPLE_OCTAGON_SHAPE = "tripleoctagon"
  final val FILLED_STYLE = "filled"
}

final case class DotConnection(source: String, destination: String, label: String, style: String) extends DotGraphComponent with DotAttributes {
  override def attributes: Map[String, String] = Map("label" -> label, "style" -> style)
  override def toString: String = source + " -> " + destination + attributesString
}
object DotConnection {
  final val DOTTED_STYLE = "dotted"
  def apply(source: DotNode, destination: DotNode, label: String = "", style: String = ""): DotConnection = DotConnection(source.id, destination.id, label, style)
}