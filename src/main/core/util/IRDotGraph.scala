package core.util

import core.{Type, _}

import scala.collection.mutable

object IRDotGraph {

  val HIDE_KNOWN_TYPES = true
  val SHRINK_IR = true
  val ALWAYS_SHOW_PARAMS_SHORT_FORM = true
  val ALWAYS_SHOW_UNKNOWN_TYPES = true
  val SHOW_LAMBDA = true
  val SHOW_ARITH_CHILDREN = true

  private val nodeCounterMap: mutable.Map[String, Int] = mutable.Map()

  def apply(ir: ShirIR): DotGraph = {
    nodeCounterMap.clear()
    ir match {
      case e: Expr => visit(e) match {
        case Some(g) => DotGraph(g)
        case None => DotGraph(DotSubGraph())
      }
      case t: Type => DotGraph(visit(t))
    }
  }

  private def visit(ir: Expr): Option[DotSubGraph] = ir match {
    case Value(_) if SHRINK_IR => None
    case Marker(input, label) =>
      val node = DotNode(label = label.s, shape = DotNode.TRIPLE_OCTAGON_SHAPE, color = DotColor.HIGHLIGHT, id = generateNodeId(ir.toShortString))
      visit(input) match {
        case Some(inputGraph) =>
          Some(inputGraph.connectAsRootNode(node))
        case None =>
          Some(DotSubGraph(Seq(node)))
      }
    case pd @ ParamDef(_, _) if ALWAYS_SHOW_PARAMS_SHORT_FORM =>
      Some(DotSubGraph(Seq(DotNode(label = "P" + pd.id, tooltip = ir.t.toString, id = generateNodeId(ir.toShortString)))))
    case ParamDef(_, _) if SHRINK_IR && !ALWAYS_SHOW_PARAMS_SHORT_FORM => None
    case ParamUse(p) if ALWAYS_SHOW_PARAMS_SHORT_FORM => visit(p)
    case ParamUse(_) if SHRINK_IR && !ALWAYS_SHOW_PARAMS_SHORT_FORM => None
    case l: LambdaT if SHRINK_IR && !SHOW_LAMBDA => visit(l.body)
    case BuiltinFunction(_) if SHRINK_IR => None
    case Let(pd, body, arg, _) if SHRINK_IR =>
      val node = DotNode(label = "Let\nP" + pd.id, id = generateNodeId(ir.toShortString))
      val childGraph = DotSubGraph(visit(arg).toSeq ++ visit(body).toSeq)
      Some(childGraph.connectAsRootNode(node))
    case FunctionCall(f: Expr, arg: Expr, _) if SHRINK_IR =>
      val children = visit(arg).toSeq ++ visit(f).toSeq
      if (children.isEmpty)
        None
      else
        Some(DotSubGraph(children))
    case TypeFunctionCall(_, _, _) if SHRINK_IR => None
    case BuiltinTypeFunction(_) if SHRINK_IR => None
    case _ =>
      val shape = ir match {
        case _: BuiltinExpr => DotNode.RECTANGLE_SHAPE
        case _ => ""
      }
      val node = DotNode(
        label = ir.toShortString,
        shape = shape,
        tooltip = if(SHOW_ARITH_CHILDREN && ir.children.filter(_ match {
          case _: ArithLambdaType => true
          case _: ArithType => true
          case _ => false
        }).length > 0) {
          ir.t.toString + "\nArithChildren: " + ir.children.filter(_ match {
            case _: ArithLambdaType => true
            case _: ArithType => true
            case _ => false
          }).mkString(", ")
        } else {
          ir.t.toString
        },
        id = generateNodeId(ir.toShortString)
      )
      val children: Seq[DotSubGraph] = ir.children.flatMap {
        case t: Type => Seq(visit(t))
        case e: Expr => visit(e).toSeq
      }
      val childGraph = DotSubGraph(children)
      val graph = childGraph.connectAsRootNode(node)
      val irType = visit(ir.t)
      if (irType.nodes.nonEmpty)
        Some(graph.addAll(Seq(irType, DotConnection(node, irType.nodes.head, "type", DotConnection.DOTTED_STYLE))))
      else
        Some(graph)
  }

  private def visit(t: Type): DotSubGraph = {
    val isUnknownType = t match {
      case _: TypeVarT => true
      case UnknownType => true
      case _ => false
    }

    if (!HIDE_KNOWN_TYPES || (ALWAYS_SHOW_UNKNOWN_TYPES && isUnknownType)) {
      val style = if (isUnknownType) {
        DotNode.FILLED_STYLE
      } else {
        ""
      }

      val node = DotNode(label = t.toShortString, shape = DotNode.SEPTAGON_SHAPE, style = style)
      t match {
        case ft: FunTypeT =>
          // special case for fun type to label the edges to the 'in' and 'out' types
          val finGraph = visit(ft.inType)
          val foutGraph = visit(ft.outType)
          DotSubGraph(Seq(
            node,
            finGraph,
            foutGraph,
            DotConnection(node, finGraph.nodes.head, "in", DotConnection.DOTTED_STYLE),
            DotConnection(node, foutGraph.nodes.head, "out", DotConnection.DOTTED_STYLE)
          ))
        case _ =>
          DotSubGraph(
            node +:
              t.children.flatMap(c => {
                val cGraph = visit(c)
                if (cGraph.nodes.isEmpty) {
                  Seq()
                } else {
                  Seq(
                    cGraph,
                    DotConnection(node, cGraph.nodes.head, "", DotConnection.DOTTED_STYLE)
                  )
                }
              })
          )
      }
    } else {
      DotSubGraph()
    }
  }

  private def generateNodeId(name: String): String = {
    val simpleName = name.replaceAll("\\W", "");
    nodeCounterMap.get(simpleName) match {
      case Some(id) =>
        nodeCounterMap.update(simpleName, id + 1)
        simpleName + "_" + id
      case None =>
        nodeCounterMap.put(simpleName, 1)
        simpleName + "_0"
    }
  }
}