package core.rewrite

import core.util.Log
import core.{Expr, ShirIR, TypeChecker}

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

case class RewriteStep(mode: RewriteMode, rules: Seq[Rule]) {
  def apply(expr: Expr): Expr = mode.rewrite(rules, expr)
}

sealed trait RewriteMode {

  def rewrite(rules: Seq[Rule], expr: Expr): Expr

  @tailrec
  final def rewriteSets(ruleSets: Seq[Seq[Rule]], expr: Expr): Expr = ruleSets.length match {
    case 1 => rewrite(ruleSets.head, expr)
    case _ => rewriteSets(ruleSets.tail, rewrite(ruleSets.head, expr))
  }

  protected final def collectPossibleRewrites(expr: Expr, rules: Seq[Rule]): Seq[(Expr, Rule)] = {
    rules.filter(_.isDefinedAt(expr)).map((expr, _)) ++
      expr.children.flatMap {
        case e: Expr => collectPossibleRewrites(e, rules)
        case _ => Seq()
      }
  }

  protected final def getPossibleRewrite(expr: Expr, rules: Seq[Rule]): Option[(Expr, Rule)] = {
    rules.foreach(rule =>
      if (rule.isDefinedAt(expr))
        return Some(expr, rule)
    )
    expr.children.foreach {
      case e: Expr =>
        val rw = getPossibleRewrite(e, rules)
        if (rw.isDefined)
          return rw
      case _ =>
    }
    None
  }

  protected final def applyRule(expr: Expr, toBeRewritten: Expr, rule: Rule): Expr = {
    if (expr == toBeRewritten) {
      Log.info(this, "apply rewrite rule " + rule.desc)
      // IRDotGraph(expr).show()//toFile("rewrite_expr_before.dot")
      val rewrittenExpr = TypeChecker.check(rule.rewrite.apply(TypeChecker.check(toBeRewritten)))
      // IRDotGraph(rewrittenExpr).show()//toFile("rewrite_expr_after.dot")
      rewrittenExpr
    } else {
      expr.build(expr.children.map {
        case e: Expr => applyRule(e, toBeRewritten, rule)
        case t: ShirIR => t
      })
    }
  }

}

final case class RewriteAll() extends RewriteMode {
  /*@tailrec
  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = {
    val rewriteOptions = getPossibleRewrite(expr, rules)
    rewriteOptions match {
      case None => expr
      case Some((toBeReplaced, rewriteRule)) =>
        val rewrittenExpr = applyRule(expr, toBeReplaced, rewriteRule)
        // IRDotGraph(rewrittenExpr).show()
        rewrite(rules, rewrittenExpr)
    }
  }*/

  // Store the paths that have been visited. This help us to prevent the compiler from starting rewrite from the root.
  val visitedExprs = new mutable.WeakHashMap[Expr, Expr]();
  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = rewriteEfficientTraversal(rules, expr)

  /**
    * Use foldLeft built-in to apply rewrite rules to the current root only.
    * Keep rewrite the expression until nothing can be changed.
    *
    * @param rules The set of rewrite rules.
    * @param expr  The input expression.
    * @return The rewritten expression.
    */
  @tailrec
  private def applyFixePointOnNode(rules: Seq[Rule], expr: Expr): Expr = {
    val possibleNewExprNew: Expr = rules.foldLeft(expr)((e, r) => {
      val possibleNewExpr = r.rewrite.applyOrElse[Expr, Expr](e, e => e)
      if (possibleNewExpr != e) {
        Log.info(this, "apply rewrite rule " + r.desc)
        TypeChecker.check(possibleNewExpr)
      } else
        possibleNewExpr
    })

    if (possibleNewExprNew != expr)
      applyFixePointOnNode(rules, possibleNewExprNew)
    else
      expr
  }

  /**
    * A more efficient way to apply rewriting. First apply rules to the root (Call applyFixePointOnNode). If nothing can
    * be changed, move to the children. If the entire subtree cannot be rewritten, move to the parent. "visitedExprs" is
    * used for tracking if the paths are visited or not. If the subtree has been visited, push it to visitedExprs.
    *
    * @param rules The set of rewrite rules.
    * @param expr  The input expression.
    * @return The rewritten expression.
    */
  private def rewriteEfficientTraversal(rules: Seq[Rule], expr: Expr): Expr = {
    val possibleNewExpr = applyFixePointOnNode(rules, expr)

    var childrenHaveChanged = false
    val possibleNewChildren: Seq[ShirIR] = possibleNewExpr.children.map({
      case child: Expr =>
        if (visitedExprs.contains(child))
          child
        else {
          val possibleNewChild = rewriteEfficientTraversal(rules, child)
          if (possibleNewChild != child)
            childrenHaveChanged = true
          possibleNewChild
        }
      case e => e
    })

    val result = if (childrenHaveChanged) {
      // If children are changed, we have to rebuild the subtree.
      val rebuiltExpr = possibleNewExpr.build(possibleNewChildren)
      rewriteEfficientTraversal(rules, rebuiltExpr)
    }
    else // If children didn't change, do not rebuild the subtree.
      possibleNewExpr

    visitedExprs.put(result, result)
    result
  }
}

final case class RewriteRandom(iterations: Int) extends RewriteMode {
  private final val random = new Random(1)

  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = _rewrite(rules, expr)

  @tailrec
  private def _rewrite(rules: Seq[Rule], expr: Expr, iterations: Int = this.iterations): Expr = {
    val rewriteOptions = collectPossibleRewrites(expr, rules)
    if (iterations == 0 || rewriteOptions.isEmpty) {
      expr
    } else {
      val optionId = random.nextInt(rewriteOptions.length)
      Log.info(this, "random rewriting (option " + (optionId + 1) + " of " + rewriteOptions.length + ") ...")
      val (toBeReplaced, rewriteRule) = rewriteOptions(optionId)
      val rewrittenExpr = applyRule(expr, toBeReplaced, rewriteRule)
      _rewrite(rules, rewrittenExpr, iterations - 1)
    }
  }
}

final case class RewriteTargeted(targetIds: Int*) extends RewriteMode {

  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = _rewrite(rules, expr)

  private def _rewrite(rules: Seq[Rule], expr: Expr, targetIds: Seq[Int] = this.targetIds): Expr = targetIds.length match {
    case 1 =>
      val rewriteOptions = collectPossibleRewrites(expr, rules)
      if (rewriteOptions.size <= targetIds.head)
        throw new Exception("Cannot apply targeted rewrite. No expressions found to apply rule!")
      val (toBeReplaced, rewriteRule) = rewriteOptions(targetIds.head)
      applyRule(expr, toBeReplaced, rewriteRule)
    case _ => _rewrite(rules, _rewrite(rules, expr, Seq(targetIds.head)), targetIds.tail)
  }

}

final case class RewriteOptimised(iterations: Int = 1, performanceModel: Expr => Int) extends RewriteMode {

  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = _rewrite(rules, expr)

  @tailrec
  private def _rewrite(rules: Seq[Rule], expr: Expr, iterations: Int = this.iterations): Expr = {
    val rewriteOptions = collectPossibleRewrites(expr, rules)
    if (iterations == 0 || rewriteOptions.isEmpty) {
      expr
    } else {
      Log.info(this, "optimised rewriting ...")
      val currentResult = (expr, performanceModel(expr))
      val newResults: Seq[(Expr, Int)] = rewriteOptions.map(e => applyRule(expr, e._1, e._2)).map(e => (e, performanceModel(e)))
      // find best result with optimal value
      val best = (newResults :+ currentResult).maxBy(_._2)
      // if our currentResult is still the best, stop rewriting and return current expression
      if (best == currentResult) {
        expr
      } else {
        _rewrite(rules, best._1, iterations - 1)
      }
    }
  }
}
