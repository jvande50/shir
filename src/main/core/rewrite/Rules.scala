package core.rewrite

object Rules {
  var disabled: Seq[String] = Seq()
}

trait RulesT {
  def get(config: Option[Int] = None): Seq[Rule] = all(config).filterNot(r => Rules.disabled.contains(r.desc))
  protected def all(config: Option[Int]): Seq[Rule]
}
