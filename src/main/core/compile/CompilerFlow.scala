package core.compile

import core.rewrite.RewriteStep
import core.util.Log
import core.{Expr, TypeChecker}

case class CompilerPhase(phase: Int)

object CompilerPhase {
  def first(): CompilerPhase = CompilerPhase(1)
}

trait CompilerPass {
  def phaseBefore: CompilerPhase
  final def phaseAfter: CompilerPhase = CompilerPhase(phaseBefore.phase + 1)
  def run(expr: Expr): Expr
  final def getName: String = getClass.getName.split('$').head
}

trait CompilerFlow {

  def passes: Seq[CompilerPass]
  def rewrites: Seq[(CompilerPhase, RewriteStep)]

  final def firstPhase: CompilerPhase = passes.map(_.phaseBefore).minBy(_.phase)
  final def lastPhase: CompilerPhase = passes.map(_.phaseAfter).maxBy(_.phase)

  final def compile(expr: Expr, fromPhase: CompilerPhase = firstPhase, toPhase: CompilerPhase = lastPhase): Expr = {
    assert(!expr.hasUnknownType)
    val selectedPasses = passes.filter(_.phaseBefore.phase >= fromPhase.phase).filter(_.phaseAfter.phase <= toPhase.phase)
    val firstRewrittenExpr = rewriteAtPhase(fromPhase, expr)
    TypeChecker.check(
      selectedPasses.sortBy(_.phaseBefore.phase).foldLeft(firstRewrittenExpr)((e, pass) => {
        Log.info(this, "Lowering with " + pass.getName + " to phase " + pass.phaseAfter)
        val loweredExpr = pass.run(e)
        val rewrittenExpr = rewriteAtPhase(pass.phaseAfter, loweredExpr)
        rewrittenExpr
      })
    )
  }

  final def rewriteAtPhase(phase: CompilerPhase, expr: Expr): Expr =
    rewrites.filter(_._1 == phase).map(_._2).foldLeft(expr)((expr, pass) => pass.apply(expr))
}
