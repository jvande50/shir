package core.commonSubtree

import core.{Expr, FunctionCall, Lambda, LambdaT, Let, Marker, ParamDef, ParamUse, Type, TypeChecker}

object FunBuilder {

  /**
   * Create a Let constructor for the given shared function and replace all detected shareable expressions.
   */
  def insertSharedFunc(expr: Expr, funBody: Expr, funParams: Seq[ParamDef]): Expr = {
    val func = TypeChecker.check(createLambdas(funBody, funParams))
    val funcIdParam = ParamDef(func.t)
    val newExpr = replaceExprs(expr, funcIdParam, funBody, funParams)
    TypeChecker.check(Let(
      funcIdParam,
      newExpr,
      func
    ))
  }

  def replaceExprs(expr: Expr, funParam: ParamDef, patternExpr: Expr, paramList: Seq[ParamDef]): Expr = {
    val inputList = getFullyMatchInputs(expr, patternExpr, paramList)
    if(inputList.length == paramList.length) {
      val newExpr = insertFunCalls(funParam, inputList)
      newExpr.build(newExpr.children.map({
        case e: Expr => replaceExprs(e, funParam, patternExpr, paramList)
        case t: Type => t
      }))
    } else if(inputList.length > 0 && inputList.length != paramList.length) {
      ???
    } else {
      expr.build(expr.children.map({
        case e: Expr => replaceExprs(e, funParam, patternExpr, paramList)
        case t: Type => t
      }))
    }
  }


  /** Collect input arguments for function calls. */
  var inputCollect: Seq[Expr] = Seq()

  def getFullyMatchInputs(expr1: Expr, expr2: Expr, paramList: Seq[ParamDef]): Seq[Expr] = {
    inputCollect = Seq()
    val checkRes = checkFullyMatchCollectInputs(expr1, expr2, paramList)
    if (checkRes) inputCollect else Seq()
  }

  /** Check if all primitives from two expressions are the same. */
  def checkFullyMatchCollectInputs(expr1: Expr, expr2: Expr, paramList: Seq[ParamDef]): Boolean = {
    (expr1, expr2) match {
      case (in, ParamUse(p)) if paramList.contains(p) =>
        inputCollect = inputCollect :+ in
        true
      case (e1, e2) if e1.getClass.equals(e2.getClass) && expr1.children.filter(_.isInstanceOf[Expr]).length == 0 => true
      case (e1, e2) if e1.getClass.equals(e2.getClass) =>
        val childrenZip = expr1.children.filter(_.isInstanceOf[Expr]) zip expr2.children.filter(_.isInstanceOf[Expr])
        val trueTable = childrenZip.map(x => checkFullyMatchCollectInputs(x._1.asInstanceOf[Expr], x._2.asInstanceOf[Expr], paramList))
        trueTable.foldLeft(true)(_ && _)
      case _ => false
    }
  }


  /** Convert a param and an expr into a lambda function. */
  def createLambda(expr: Expr, param: ParamDef): LambdaT = {
    Lambda(
      param,
      expr.visitAndRebuild {
        case Marker(ParamUse(param1: ParamDef), ts) if ts.s.contains("subTreeTmp") && param1.id == param.id =>
          ParamUse(param1)
        case e => e
      }.asInstanceOf[Expr]
    )
  }

  /** Convert a paramList and an expr into a nested lambda function. (high-order function.)*/
  def createLambdas(expr: Expr, params: Seq[ParamDef]): LambdaT = {
    var newExpr = expr
    params.foreach(p => {
      newExpr = createLambda(newExpr, p).asInstanceOf[Expr]
    })
    newExpr.asInstanceOf[LambdaT]
  }

  /** Instantiate primitives for function calls with input arguments. */
  def insertFunCall(funExpr: Expr, input: Expr): Expr = {
    FunctionCall(funExpr, input)
  }

  def insertFunCalls(funParam: ParamDef, inputs: Seq[Expr]): Expr = {
    var newExpr: Expr = ParamUse(funParam)
    inputs.foreach(i => {
      newExpr = insertFunCall(newExpr, i).asInstanceOf[Expr]
    })
    newExpr
  }
}
