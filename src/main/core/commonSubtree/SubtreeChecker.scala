package core.commonSubtree

import algo.{AlgoFunType, AlgoLambda, ConstantInteger, CounterInteger, CounterIntegerExpr}
import backend.hdl.arch.ArchLambda
import backend.hdl.util.False
import core.{BuiltinFunction, BuiltinTypeFunction, Expr, FunTypeT, FunctionCall, Lambda, LambdaT, Marker, ParamDef, ParamUse, ShirIR, TextType, TypeFunctionCall, Value}

object SubtreeChecker {

  /** Convert given expression to a string (exclude Param, Counter) */
  def exprToString(expr: Expr): String = {
    val idList = expr.children.filter(_.isInstanceOf[Expr]).map(child => exprToString(child.asInstanceOf[Expr]))
    val serialString = if (expr.children.filter(_.isInstanceOf[Expr]).length > 0)
      expr match {
        case ParamUse(_) => ""
        case CounterInteger(_) => ""
        case ConstantInteger(_) => ""
        case Value(_) => ""
        case FunctionCall(_, _, _) => idList.toString()
        case TypeFunctionCall(_, _, _) => idList.toString()
        case BuiltinFunction(_) => idList.toString()
        case BuiltinTypeFunction(_) => idList.toString()
        case _ =>
          expr.getClass.getSimpleName + idList
      }
    else
      ""
    serialString
  }

  /** Check if all primitives from two expressions are the same. */
  def checkFullyMatch(expr1: Expr, expr2: Expr): Boolean = {
    (expr1, expr2) match {
      case (e1, e2) if e1.getClass.equals(e2.getClass) && expr1.children.filter(_.isInstanceOf[Expr]).length == 0 => true
      case (e1, e2) if e1.getClass.equals(e2.getClass) =>
        val childrenZip = expr1.children.filter(_.isInstanceOf[Expr]) zip expr2.children.filter(_.isInstanceOf[Expr])
        val trueTable = childrenZip.map(x => checkFullyMatch(x._1.asInstanceOf[Expr], x._2.asInstanceOf[Expr]))
        trueTable.foldLeft(true)(_ && _)
      case _ => false
    }
  }

  /**
   * Retrieve the largest common subtree between 'expr1' and 'expr2' starting from the root.
   * Unmatched expressions will be replaced by function parameters.
   * Any Lambda function are required to pass a full check. Otherwise, the expression cannot be shared.
   *
   * @params expr1, expr2: input expressions
   * @params allowSameRef allows detector to match trees with the same reference.
   * @return a new built subtree with new placeholders.
   */
  def getMaxSubtree(expr1: Expr, expr2: Expr, allowSameRef: Boolean = false): Option[Expr] = {
    (expr1, expr2) match {
      case (e1, e2) if (e1 eq e2) && !allowSameRef => None
      case (e1: LambdaT, e2: LambdaT) => if (checkFullyMatch(e1, e2)) Some(e1) else None
      case (_: CounterIntegerExpr, _) => None//Some(Marker(ParamUse(ParamDef(expr1.t)), TextType("subTreeTmp")))
      case (_, _: CounterIntegerExpr) => None//Some(Marker(ParamUse(ParamDef(expr1.t)), TextType("subTreeTmp")))
      case (e1, e2) if e1.getClass.equals(e2.getClass) =>
        if (e1.children.filter(_.isInstanceOf[Expr]).length == 0) {
          Some(e1)
        } else {
          val childrenZip = e1.children zip e2.children
          // Iterate over all expr children
          val newBuildSeq = childrenZip.map(
            child => (child._1, child._2) match {
              case (ch1: LambdaT, ch2: Expr) => getMaxSubtree(ch1, ch2, allowSameRef)
              case (ch1: Expr, ch2: LambdaT) => getMaxSubtree(ch1, ch2, allowSameRef)
              case (ch1: Expr, ch2: Expr) if ch1.t.isInstanceOf[FunTypeT] || ch2.t.isInstanceOf[FunTypeT] => getMaxSubtree(ch1, ch2, allowSameRef)
              case (ch1: Expr, ch2: Expr) if ch1.isInstanceOf[FunctionCall] || ch2.isInstanceOf[FunctionCall] => getMaxSubtree(ch1, ch2, allowSameRef)
              case (ch1: ParamDef, _: ParamDef) => Some(ch1)
              case (ch1: Expr, ch2: Expr) =>
                val newChild = getMaxSubtree(ch1, ch2, allowSameRef)
                if(newChild == None)
                //Some(Marker(ParamUse(ParamDef(ch1.t)), TextType("subTreeTmp")))
                  Some(ParamUse(ParamDef(ch1.t)))
                else
                  newChild
              case (ch1, _) => Some(ch1)
            })
          // Propagate None for invalid case (Lambda)
          if(newBuildSeq.exists(_ == None))
            None
          else {
            Some(e1.build(newBuildSeq.map(child => child match{case Some(e) => e case _ => ???})))
          }
        }
      case _ => None//Some(Marker(ParamUse(ParamDef(expr1.t)), TextType("subTreeTmp")))
    }
  }

  /**
   * Get the all subtrees from max subtree.
   * When encountering primitives outside functions, create a new subtree.
   * If primitives in functions cannot be separated, there is no extra subtree.
   *
   * @params expr: input expression
   * @return a sequence of subtrees.
   */
  def decomposeMaxSubtree(expr: Expr): Seq[Expr] = {
    expr match {
      case e: LambdaT => Seq(e)
      case e: Expr if e.children.filter(_.isInstanceOf[Expr]).length == 0 => Seq(e)
      case e: CounterIntegerExpr => Seq(Marker(ParamUse(ParamDef(e.t)), TextType("subTreeTmp")))
      case e: Expr =>
        // generate all combinations of arguments
        val argSet = e.children.map(child => child match {
          case c @ Marker(ParamUse(_), ts) if ts.s == "subTreeTmp" => Seq(c) // avoid creating subtree in Subtree Marker
          case c: ParamDef => Seq(c)
          case c: LambdaT => Seq(c) // Do not split Lambda function
          case c: FunctionCall => decomposeMaxSubtree(c)
          case c: Expr if c.t.isInstanceOf[FunTypeT] => Seq(c)
          case c: Expr => decomposeMaxSubtree(c) ++ Seq(Marker(ParamUse(ParamDef(c.t)), TextType("subTreeTmp")))
          case c => Seq(c)
        })
        var totalLen = 1
        var argList: Seq[Seq[ShirIR]] = Seq()
        for (arg <- argSet) totalLen = totalLen * arg.length
        for (arg <- argSet) argList = argList :+ Seq.fill(totalLen / arg.length)(arg).flatten
        val newArgList = argList.transpose
        newArgList.map(args => e.build(args))
      case _ => ???
    }
  }

  /**
   * Scan target expr with pattern (for sharing).
   * This method is designed to identify all the occurrences of the given pattern inside target expression.
   * The process starts from the leaves of target and keeps comparing with pattern expression.
   *
   * @params target, pattern: input expressions
   * @return a sequence of subtrees.
   */
  def scanSubtreePattern(target: Expr, pattern: Expr, allowSameRef: Boolean = false): Seq[Expr] = {
    if ((target eq pattern) && !allowSameRef) // if target and pattern are the same instance, the subtree is no sharing
      return Seq()
    // Do DFS traverse
    var trees: Seq[Expr] = Seq()
    trees = target match {
      case e: Expr if e.children.filter(_.isInstanceOf[Expr]).length == 0 => Seq()
      case _: CounterIntegerExpr => Seq()
      case e: Expr => e.children.filter(_.isInstanceOf[Expr]).map(child => scanSubtreePattern(child.asInstanceOf[Expr], pattern, allowSameRef)).flatten
    }
    val currentTree = getMaxSubtree(target, pattern, allowSameRef)
    val getCurrentTree = currentTree match {
      case Some(e) => Seq(e)
      case _ => Seq()
    }
    trees = trees ++ getCurrentTree
    trees
  }

  /**
   * Depth-first traverse expr and match the expression with top.
   * TODO: remove duplicates
   *
   * @params top, expr: input expressions
   * @return a sequence of subtrees.
   */
  def TraverseScanTree(top: Expr, expr: Expr): Seq[Expr] = {
    expr match {
      case _: CounterIntegerExpr => Seq()
      case _: BuiltinFunction => Seq()
      case _: ParamDef => Seq()
      case _: ParamUse => Seq()
      case e: FunctionCall =>
        e.children.filter(_.isInstanceOf[Expr]).map(child => TraverseScanTree(top, child.asInstanceOf[Expr])).flatten
      case e: LambdaT =>
        e.children.filter(_.isInstanceOf[Expr]).map(child => TraverseScanTree(top, child.asInstanceOf[Expr])).flatten
      case e: Expr =>
        val resList = e.children.filter(_.isInstanceOf[Expr]).map(child => TraverseScanTree(top, child.asInstanceOf[Expr])).flatten
        resList ++ scanSubtreePattern(top, expr, false)
      case _ => Seq()
    }
  }

  /**
   * Scan Tree itself.
   * Also avoid duplicates.
   */
  def selfScan(expr: Expr): Seq[Expr] = {
    val potentialList = TraverseScanTree(expr, expr)
    var resultList: Seq[Expr] = Seq()
    var stringNameList: Seq[String] = Seq()
    potentialList.foreach(e => {
      val name = exprToString(e)
      if(!stringNameList.contains(name)) {
        resultList = resultList :+ e
        stringNameList = stringNameList :+ name
      }
    })
    resultList
  }

  /**
   * Check invalid FunCall.
   * Check the pattern with Lambda(_, Marker(_: ParamDef, t), _), which is inserted by subtree detector.
   */
  def isInvalidSubtree(expr: Expr): Boolean = {
    var invalidFound = false
    expr.visit{
      case Lambda(_, Marker(_: ParamDef, t), _) if t.s.contains("subTreeTmp") =>
        invalidFound = true
      case _ =>
    }
    invalidFound
  }

  /** Collect All new Params. */
  def getSubtreeParams(expr: Expr): Seq[ParamDef] = {
    var boundParam: Seq[ParamDef] = Seq()
    var unboundParam: Seq[ParamDef] = Seq()
    expr.visit {
      //case AlgoLambda(p: ParamDef, _, _) => boundParam = boundParam :+ p
      case Lambda(p: ParamDef, _, _) => boundParam = boundParam :+ p
      case _ =>
    }
    expr.visit {
      case ParamUse(p) if !boundParam.contains(p) => unboundParam = unboundParam :+ p
      case _ =>
    }
    unboundParam
  }
}
