package core.commonSubtree

import core.{Expr, FunTypeT, FunctionCall, LambdaT, ParamDef, ParamUse, Type, TypeFunType, TypeFunctionCall, TypeVarT}

object FunHelper {
  /**
   * Get Lambda Function's body.
   */
  def getLambdaBody(e: Expr): Expr = e match {
    case l: LambdaT => getLambdaBody(l.body)
    case e => e
  }

  /**
   * Get Lambda Params.
   */
  def getLambdaParams(e: Expr): Seq[ParamDef] = e match {
    case l: LambdaT => Seq(l.param) ++ getLambdaParams(l.body)
    case _ => Seq()
  }

  def getLambdaParamIds(e: Expr): Seq[Long] = e match {
    case l: LambdaT => Seq(l.param.id) ++ getLambdaParamIds(l.body)
    case _ => Seq()
  }

  /**
   * Get input param types.
   */
  def getInputTypesFromFunType(lt: Type): Seq[Type] = lt match {
    case t: FunTypeT => Seq(t.inType) ++ getInputTypesFromFunType(t.outType)
    case _ => Seq()
  }

  def getOutputTypeFromFunType(lt: Type): Type = lt match {
    case t: FunTypeT => getOutputTypeFromFunType(t.outType)
    case t => t
  }

  /** Find out full function call. */
  def matchFunCall(expr: Expr, p: ParamDef, pCount: Int = 0): Boolean = expr match {
    case FunctionCall(fc@FunctionCall(_, _, _), _, _) => matchFunCall(fc, p, pCount + 1)
    case FunctionCall(ParamUse(pd), _, _) if pd.id == p.id && (pCount + 1) == getInputTypesFromFunType(p.t).length => true
    case _ => false
  }

  /**
   * Fetch pattern of high-order function calls.
   */
  def fetchFunCall(e: Expr, p: ParamDef): Seq[Expr] = e match {
    case FunctionCall(f, i, _) => Seq(i) ++ fetchFunCall(f, p)
    case _ => Seq()
  }

  /**
   * Check if a multiple input TypeFunction is shared.
   */
  def checkSharedTypeFunc(e: Expr): Boolean = e match {
    case TypeFunctionCall(f, _, _) => checkSharedTypeFunc(f)
    case ParamUse(_) => true
    case _ => false
  }

  def getTypeFunCallInputs(e: Expr): Seq[Type] = e match {
    case TypeFunctionCall(f, i, _) => getTypeFunCallInputs(f) :+ i
    case _ => Seq()
  }

  def getTypeFunCallParamUse(e: Expr): Expr = e match {
    case TypeFunctionCall(f, _, _) => getTypeFunCallParamUse(f)
    case ParamUse(pd) => ParamUse(pd)
    case _ => ???
  }

  def getTypeFunCallParam(e: Expr): ParamDef = e match {
    case TypeFunctionCall(f, _, _) => getTypeFunCallParam(f)
    case ParamUse(pd) => pd
    case _ => ???
  }


  def getTypeInputs(t: Type): Seq[TypeVarT] = t match {
    case TypeFunType(tv: TypeVarT, outType: Type) => tv +: getTypeInputs(outType)
    case _ => Seq()
  }

  def getTypeOutput(t: Type): Type = t match {
    case TypeFunType(tv: TypeVarT, outType: Type) => getTypeOutput(outType)
    case tt => tt
  }
}
