package core.commonSubtree

import core.Expr
import core.commonSubtree.SubtreeChecker.scanSubtreePattern

object SubtreeSelector {

  /** Create pairs from the list of shared function candidates. */
  def getExprPairs(exprs: Seq[Expr]): Seq[(Expr, Expr)] = {
    exprs
      .tails
      .filter(_.nonEmpty)
      .flatMap(xs => xs.tail.map((xs.head, _)))
      .toList
  }

  /**
   * Check if each expr candidate pair has the same occurrences and if one is a subtree of the other one.
   * If yes, return the larger tree. Otherwise, return None.
   *
   * @params top: Entire tree.
   * @params exprPairs: Paris of subtree candidates.
   * @return a list of subtrees.
   */
  def pairwiseOverlappingCheck(top: Expr, exprPairs: Seq[(Expr, Expr)]): Seq[Option[Expr]] = {
    exprPairs.map(pair => {
      // First check overlapping
      var result: Option[Expr] = None
      if(scanSubtreePattern(pair._1, pair._2, true).length > 0)
        result = Some(pair._1)
      else if (scanSubtreePattern(pair._2, pair._1, true).length > 0)
        result = Some(pair._2)

      // Next check occurrence
      if(result != None)
        if(scanSubtreePattern(top, pair._1, true).length != scanSubtreePattern(top, pair._1, true).length)
          result = None

      result
    })
  }

  /** Group subtrees pairs and their comparison results into Map. */
  def pairwiseToMap(exprPairs: Seq[(Expr, Expr)], pairRes: Seq[Option[Expr]] ): Map[Expr, Expr] = {
    exprPairs.zip(pairRes).filter(_._2.nonEmpty).map(e => (e._1, e._2.get)).map(e =>
      if(e._1._1 == e._2) e._1._2 -> e._2 else e._1._1 -> e._2).toMap
  }

  /**
   * Check if each expr pair has the same occurrences and if one is a subtree of the other one.
   * If yes, remove the smaller one.
   *
   * @params top: Entire tree.
   * @params subtrees: A List of subtrees.
   * @return a list of subtrees.
   */
  def removeOverlapping(top: Expr, subtrees: Seq[Expr]): Seq[Expr] = {
    val pairs = getExprPairs(subtrees)
    val pairRes = pairwiseOverlappingCheck(top, pairs)
    val pairMapping = pairwiseToMap(pairs, pairRes)
    val result = subtrees.filterNot(e => pairMapping.contains(e))
    result
  }

  /**
   * Sort subtrees based on overlapping.
   *
   * @params top: Entire tree.
   * @params subtrees: A List of subtrees.
   * @return soted list of subtrees.
   */
  def rankByOverlapping(top: Expr, subtrees: Seq[Expr]): Seq[Expr] = {
    val pairs = getExprPairs(subtrees)
    val pairRes = pairwiseOverlappingCheck(top, pairs).map(_.get)
    val ranks = subtrees.map(e => pairRes.count(_ == e))
    subtrees.zip(ranks).sortBy(_._2).map(_._1).reverse
  }
}
