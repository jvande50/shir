package core

/**
  * Describes a tree node's kind. A node kind contains all the information in a tree node except for its children.
  * Kinds can be compared for equality and two equal expressions will always have equal kinds.
  */
trait TreeNodeKind
private case class DefaultTreeNodeKind(c: Class[_], numberOfChildren: Int) extends TreeNodeKind

trait TreeNode[T <: TreeNode[T]] {
  def children: Seq[T]

  /**
    * Gets this tree node's kind.
    * @return A tree node kind.
    */
  def nodeKind: TreeNodeKind = {
    DefaultTreeNodeKind(this.getClass, children.length)
  }

  final def foreach(f: T => Unit): Unit = {
    children.foreach(f(_))
  }

  def zip(t: T): Seq[(T, T)] = (this.asInstanceOf[T], t) +: children.zip(t.children).flatMap(p => p._1.zip(p._2))

  final def flatten: Seq[T] = this.asInstanceOf[T] +: children.flatMap(_.flatten)

//  final def visit(prePost: T => T => Unit): Unit = {
//    val post = prePost(this.asInstanceOf[T])
//    foreach(_.visit(prePost))
//    post(this.asInstanceOf[T])
//  }

  private final def visit(pre: T => Unit, post: T => Unit): Unit = {
    pre(this.asInstanceOf[T])
    foreach(_.visit(pre, post))
    post(this.asInstanceOf[T])
  }

  final def visit(pre: T => Unit): Unit = visit(pre, t => Unit)
}

trait BuildableTreeNode[T <: BuildableTreeNode[T]] extends TreeNode[T] {
  def build(newChildren: Seq[T]): T

  def map(f: T => T): T = {
    build(children.map(f(_)))
  }

//  final def visitAndRebuild(prePost: T => (T, T => T)): T = {
//    val (newType, post) = prePost(this.asInstanceOf[T])
//    post(newType.map(_.visitAndRebuild(prePost)))
//  }

  final def visitAndRebuild(pre: T => T, post: T => T): T = post(pre(this.asInstanceOf[T]).map(_.visitAndRebuild(pre, post)))

  final def visitAndRebuild(post: T => T): T = visitAndRebuild(t => t, post)
}
