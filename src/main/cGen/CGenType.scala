package cGen
import cGen.NamedTupleStructTypeKind.{TUPLE_NAME_PREFIX, TUPLE_PREFIX}
import core._

import java.util.concurrent.atomic.AtomicInteger
import scala.::


// Make initialBody public, for the unapply in BuildExpr
trait CGenLambdaT extends LambdaT {
  val body: Expr
}

/**
 * This type is the root of all types in the cGen package / in the CGenIR
 */

case object CGenDataTypeKind extends Kind

sealed trait CGenDataTypeT extends BuiltinDataTypeT with CGenIR {
  override def kind: Kind = CGenDataTypeKind

  override def superType: Type = BuiltinDataType()
}

final case class CGenDataType() extends CGenDataTypeT {
  override def build(newChildren: Seq[ShirIR]): CGenDataTypeT = this
}

final case class CGenDataTypeVar(tvFixedId: Option[Long] = None) extends CGenDataTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = CGenDataTypeVar(tvFixedId)

  override def upperBound: CGenDataTypeT = CGenDataType()
}

/**
 * abstract super type for all collection like types
 */

case object AbstractCollectionTypeKind extends Kind

sealed trait AbstractCollectionTypeT extends CGenDataTypeT {
  override def kind: Kind = AbstractCollectionTypeKind

  override def superType: Type = CGenDataType()

  override def children: Seq[Type] = Seq()
}

final case class AbstractCollectionType() extends AbstractCollectionTypeT {
  override def build(newChildren: Seq[ShirIR]): AbstractCollectionTypeT = AbstractCollectionType()
}

final case class AbstractCollectionTypeVar(tvFixedId: Option[Long] = None) extends AbstractCollectionTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AbstractCollectionTypeVar(tvFixedId)

  override def upperBound: CGenDataTypeT = AbstractCollectionType()
}

case object NamedTupleStructTypeKind extends Kind {
  final val TUPLE_PREFIX: String = "t"
  final val TUPLE_NAME_PREFIX: String = "tuple_"
}

sealed trait NamedTupleStructTypeT extends AbstractCollectionTypeT {
  protected val name: String
  val namedTypes: Seq[(String, CGenDataTypeT)]

  assert(namedTypes.nonEmpty)

  override def kind: Kind = NamedTupleStructTypeKind

  override def superType: Type = AbstractCollectionType()

  override def children: Seq[Type] = namedTypes.map(_._2)

  def getName() = {
    if (name == "") TUPLE_NAME_PREFIX + namedTypes.map(_._2).map(_.toShortString).mkString("_")
    else name
  }
}

// nameType represent the name of the struct
// should not use the name, should use the getName method instead
final case class NamedTupleStructType(name: String, namedTypes: Seq[(String, CGenDataTypeT)]) extends NamedTupleStructTypeT {
  override def build(newChildren: Seq[ShirIR]): NamedTupleStructTypeT = {
      NamedTupleStructType(name, namedTypes.map(_._1).zip(newChildren.map(_.asInstanceOf[CGenDataTypeT])))
  }
}

final case class NamedTupleStructTypeVar(name: String, namedTypes: Seq[(String, CGenDataTypeT)], tvFixedId: Option[Long] = None) extends NamedTupleStructTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT =
    NamedTupleStructTypeVar(name, namedTypes.map(_._1).zip(newChildren.map(_.asInstanceOf[CGenDataTypeT])), tvFixedId)

  override def upperBound: CGenDataTypeT = NamedTupleStructType(name, namedTypes)
}

object TupleStructType {
  def apply(types: CGenDataTypeT*): NamedTupleStructType = NamedTupleStructType(name = "", types.zipWithIndex.map(p => (TUPLE_PREFIX + p._2, p._1)))
  def apply(name: String, types: CGenDataTypeT*): NamedTupleStructType = NamedTupleStructType(name, types.zipWithIndex.map(p => (TUPLE_PREFIX + p._2, p._1)))
}

object TupleStructTypeVar {
  def apply(types: CGenDataTypeT*): NamedTupleStructTypeVar = NamedTupleStructTypeVar(name = "", types.zipWithIndex.map(p => (TUPLE_PREFIX + p._2, p._1)))
}

/**
 * This type represents arrays.
 */

case object ArrayTypeKind extends Kind

sealed trait ArrayTypeT extends AbstractCollectionTypeT {
  val et: CGenDataTypeT
  val len: ArithTypeT

  override def kind: Kind = ArrayTypeKind

  override def superType: Type = AbstractCollectionType()

  override def children: Seq[Type] = Seq(et, len)
}

final case class ArrayType(et: CGenDataTypeT, len: ArithTypeT) extends ArrayTypeT {
  override def build(newChildren: Seq[ShirIR]): ArrayTypeT = ArrayType(newChildren.head.asInstanceOf[CGenDataTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}

final case class ArrayTypeVar(et: CGenDataTypeT = CGenDataTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends ArrayTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = ArrayTypeVar(newChildren.head.asInstanceOf[CGenDataTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = ArrayType(et, len)
}

case object VoidTypeKind extends Kind

sealed trait VoidTypeT extends CGenDataTypeT with CGenIR {
  override def kind: Kind = VoidTypeKind

  override def superType: Type = CGenDataType()
}

final case class VoidType() extends VoidTypeT {
  override def build(newChildren: Seq[ShirIR]): VoidTypeT = this
}

final case class VoidTypeVar(tvFixedId: Option[Long] = None) extends VoidTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = VoidTypeVar(tvFixedId)

  override def upperBound: VoidTypeT = VoidType()
}

case object AddressTypeKind extends Kind

sealed trait AddressTypeT extends CGenDataTypeT with CGenIR {
  val ptrType: Type

  override def kind: Kind = AddressTypeKind

  override def superType: Type = CGenDataType()

  override def children: Seq[Type] = Seq(ptrType)
}

final case class AddressType(ptrType: Type) extends AddressTypeT {
  override def build(newChildren: Seq[ShirIR]): AddressTypeT = AddressType(newChildren.head.asInstanceOf[Type])
}

final case class AddressTypeVar(ptrType: Type = CGenDataTypeVar(), tvFixedId: Option[Long] = None) extends AddressTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AddressTypeVar(newChildren.head.asInstanceOf[Type], tvFixedId)

  override def upperBound: Type = AddressType(ptrType)
}

object Allocation extends Enumeration {
  type Allocation = Value
  val ALLOCATED, UNALLOCATED = Value
}

case object CScalarTypeKind extends Kind

sealed trait CScalarTypeT extends CGenDataTypeT {
  override def kind: Kind = CScalarTypeKind

  override def superType: Type = CGenDataType()

  override def children: Seq[Type] = Seq()
}

final case class CScalarType() extends CScalarTypeT {
  override def build(newChildren: Seq[ShirIR]): CScalarTypeT = CScalarType()
}

final case class CScalarTypeVar(tvFixedId: Option[Long] = None) extends CScalarTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = CScalarTypeVar(tvFixedId)

  override def upperBound: CGenDataTypeT = CScalarType()
}

case object IntTypeKind extends Kind

sealed trait IntTypeT extends CScalarTypeT {
  val width: ArithTypeT
  val unsigned: ArithTypeT // 0: signed int, 1: unsigned int

  override def kind: Kind = IntTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq(width, unsigned)
}

final case class IntType(width: ArithTypeT, unsigned: ArithTypeT = 0) extends IntTypeT {
  override def build(newChildren: Seq[ShirIR]): IntTypeT = IntType(newChildren.head.asInstanceOf[ArithTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}

final case class IntTypeVar(width: ArithTypeT = ArithTypeVar(), unsigned: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends IntTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = IntTypeVar(newChildren.head.asInstanceOf[ArithTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = IntType(width, unsigned)
}

case object DoubleTypeKind extends Kind

sealed trait DoubleTypeT extends CScalarTypeT {
  override def kind: Kind = DoubleTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq()
}

final case class DoubleType() extends DoubleTypeT {
  override def build(newChildren: Seq[ShirIR]): DoubleTypeT = DoubleType()

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case _: TextFormatter => Some("double")
      case _ => None
    }
  }
}

final case class DoubleTypeVar(tvFixedId: Option[Long] = None) extends DoubleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = DoubleTypeVar(tvFixedId)

  override def upperBound: Type = DoubleType()
}

case object BoolTypeKind extends Kind

sealed trait BoolTypeT extends CScalarTypeT {
  override def kind: Kind = BoolTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq()
}

final case class BoolType() extends BoolTypeT {
  override def build(newChildren: Seq[ShirIR]): BoolTypeT = BoolType()
}

final case class BoolTypeVar(tvFixedId: Option[Long] = None) extends BoolTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = BoolTypeVar(tvFixedId)

  override def upperBound: Type = BoolType()
}

case object CharTypeKind extends Kind

sealed trait CharTypeT extends CScalarTypeT {
  override def kind: Kind = CharTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq()
}

final case class CharType() extends CharTypeT {
  override def build(newChildren: Seq[ShirIR]): CharTypeT = CharType()
}

final case class CharTypeVar(tvFixedId: Option[Long] = None) extends CharTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = CharTypeVar(tvFixedId)

  override def upperBound: Type = CharType()
}
