package cGen

import algo.SeqTypeT
import cGen.pass.AlgoPass.BinaryPass
import cGen.pass.CommonPass.BetaReductionPass
import cGen.pass.DataflowPass._
import cGen.pass.RemoveViewPass._
import cGen.pass.{MemoryPass, Pass, RebuildPass}
import core._
import core.rewrite.Rule
import core.util.IRDotGraph

import scala.language.{existentials, postfixOps}

object DPSPass {
  var graphId = 0

  def getGraphId: Int = {
    val id = graphId
    graphId += 1
    id
  }

  def apply(expr: Expr): Expr = {
    var tree = expr
    tree = TypeChecker.check(tree)
    tree = TypeChecker.check(tree)
    tree = BinaryPass()(tree)
    tree = MemoryPass()(tree)
    if (Debug.PROFILE >= 3) IRDotGraph(tree).toFile(s"tmp/$getGraphId-to-build")
    tree = IntroIdxPass()(tree)
    if (Debug.PROFILE >= 3) IRDotGraph(tree).toFile(s"tmp/$getGraphId-alloc0")
    tree = Pass.orchestrate(tree,
      MemoryPass(), // required by forward rule which introduce new ids
      BetaReductionPass(),
      AggressiveBuildFuse(), // not required
      RemoveSourceView(),
    )
    tree = Pass.orchestrate(tree,
      AllocLetLiftPass(),
      RemoveDestView(), // need to be place after aggressive build fuse, because it lifts alloc out of build
      DestBuildFusePass(), // AllocBackwardLift introduces DestBuild
      BetaReductionPass(),
    )
    TypeChecker.check(tree)
    tree = AllocLambdaLift(tree)
    if (Debug.PROFILE >= 3) IRDotGraph(tree).toFile(s"tmp/$getGraphId-fused")
    if (Debug.PROFILE >= 3) IRDotGraph(tree).toFile(s"tmp/$getGraphId-to-block")
    tree = ANF(tree)
    if (Debug.PROFILE >= 3) IRDotGraph(tree).toFile(s"tmp/$getGraphId-ANF")
    tree = UnusedDefAnalysis()(tree)
    tree = RemoveValueAtRule()(tree) // required
    tree = Hoist()(tree) // not required
    tree = ChangeAllocKindRule()(tree)
    tree = InsertFree(tree)(Seq())
    tree = TypeChecker.check(tree)
    if (Debug.PROFILE >= 3) IRDotGraph(tree).toFile(s"tmp/$getGraphId-done")
    tree = TypeChecker.check(tree)
    tree = TypeChecker.check(tree)
    tree
  }

  // isEager must match the AllocRule0
  def isEager(expr: Expr): Boolean = expr match {
    case _: BinaryOpExpr => true
    case _: SingleOpExpr => true
    case _: IdExpr => true
    case _ => false
  }

  def isScalar(t: Type): Boolean = t match {
    case IntType(_, _) => true
    case DoubleType() => true
    case BoolType() => true
    case _ => false
  }
}


