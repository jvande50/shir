package cGen

import core.Type

abstract class Platform {
  val DOUBLE_SIZE: Int
  val POINTER_SIZE: Int

  def getSize(t: Type): Int
}

trait Bits64 extends {
  val DOUBLE_SIZE = 8;
  val POINTER_SIZE = 8;
  // .... TBD

  def getSize(t: Type): Int = t match {
    case DoubleType() => DOUBLE_SIZE
    case AddressType(_) => POINTER_SIZE
    case IntType(width, _) => width.ae.evalInt / 8
  }
}

object Linux64 extends Platform with Bits64
