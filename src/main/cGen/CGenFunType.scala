package cGen

import core.{AnyType, AnyTypeVar, ArithTypeT, ArithTypeVar, FunTypeT, FunctionT, Kind, ShirIR, MalformedTypeException, MetaTypeT, Type, TypeVarGenT, TypeVarT}
import lift.arithmetic.{ArithExpr, Cst}

case object CGenFunTypeKind extends Kind

trait CGenFunTypeT extends FunTypeT {
  override def kind: Kind = CGenFunTypeKind

  override def superType: Type = superFunType
}

final case class CGenFunType(inType: Type, outType: Type) extends CGenFunTypeT {
  override def build(newChildren: Seq[ShirIR]): FunTypeT = CGenFunType(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type])
}

object CGenFunType {
  def apply(inTypes: Seq[Type], outType: Type): CGenFunType = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      CGenFunType(inTypes.head, outType)
    } else {
      CGenFunType(inTypes.tail, CGenFunType(inTypes.head, outType))
    }
  }
}

final case class CGenFunTypeVar(inType: Type = AnyTypeVar(), outType: Type = AnyTypeVar(), tvFixedId: Option[Long] = None) extends CGenFunTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): CGenFunTypeVar = CGenFunTypeVar(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type], tvFixedId)

  override def upperBound: Type = CGenFunType(inType, outType)
}

object CGenFunTypeVar {
  def apply(inTypes: Seq[Type], outType: Type): CGenFunTypeVar = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      CGenFunTypeVar(inTypes.head, outType)
    } else {
      CGenFunTypeVar(inTypes.tail, CGenFunTypeVar(inTypes.head, outType))
    }
  }
}