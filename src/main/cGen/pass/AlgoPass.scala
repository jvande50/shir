package cGen.pass

import cGen.{AddExpr, AlgoOp, BinaryOp, Map, MulExpr, Range, Select, SubExpr}

object AlgoPass {
  def BinaryPass(): RebuildPass = RebuildPass("BinaryPass", {
    case add: AddExpr => BinaryOp(Select(add.args.head, 0), Select(add.args.head, 1), AlgoOp.Add)
    case sub: SubExpr => BinaryOp(Select(sub.args.head, 0), Select(sub.args.head, 1), AlgoOp.Sub)
    case mul: MulExpr => BinaryOp(Select(mul.args.head, 0), Select(mul.args.head, 1), AlgoOp.Mul)
  })
}
