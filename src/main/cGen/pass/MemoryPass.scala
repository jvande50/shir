package cGen.pass

import cGen.AllocKind.Heap
import cGen._
import core.{ParamDef, ParamUse}

object MemoryPass {
  def apply(): RebuildPass = RebuildPass("IntroMemoryPass", {
    case bin@BinaryOp(in1, in2, op, isBool, t) if !bin.isWritten =>
      val d = ParamDef(AddressType(t))
      CGenLet(d, Alloc(t, Heap)) apply Assign(BinaryOp(in1, in2, op, isBool, isWritten = true), ParamUse(d)) >> ValueAt(ParamUse(d))
    case sin@SingleOp(in, op, t) if !sin.isWritten =>
      val d = ParamDef(AddressType(t))
      CGenLet(d, Alloc(t, Heap)) apply Assign(SingleOp(in, op, isWritten = true), ParamUse(d)) >> ValueAt(ParamUse(d))
    case id@Id(in, t) if !id.isWritten => 
      val d = ParamDef(AddressType(t))
      CGenLet(d, Alloc(t, Heap)) apply Assign(Id(in, isWritten = true), ParamUse(d)) >> ValueAt(ParamUse(d))
    case efc@ExternFunctionCall(name, ins, intvs, t) =>
      val d = ParamDef(AddressType(t))
      // still treat as DExternFunctionCall because DPS is needed
      CGenLet(d, Alloc(t, Heap)) apply DExternFunctionCall(name, ins.+:(ParamUse(d)), intvs.+:(AddressTypeVar(t))) >> ValueAt(ParamUse(d))
  }, isTypeCheck = true)
}
