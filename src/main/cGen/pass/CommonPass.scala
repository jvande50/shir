package cGen.pass

import cGen.CFC
import core.util.IRDotGraph
import core.{Expr, LambdaT, ParamDef, ParamUse}

import scala.collection.mutable

object CommonPass {
  case class BetaReductionPass() extends Pass {
    override val isMuted: Boolean = false
    override val isFixPoint: Boolean = false
    val renames: mutable.Map[Long, Expr] = mutable.Map[Long, Expr]() // pairs for origin (Id of ParamDef) and target

    // mark
    def ReduceRule(): RebuildPass = RebuildPass("ReduceRule", {
      case c@CFC(f: LambdaT, arg, _) if !c.isLet =>
        RenamePdPass(f.param, arg, "BetaReductionRename")(f.body)
    })

    // rewrite
    def RenameRule(): RebuildPass = RebuildPass("RenameRule", {
      case ParamUse(pd) if renames.contains(pd.id) => renames(pd.id)
    })

    override val name: String = this.getClass.getSimpleName

    override def run(e: Expr): Expr = pipeline(e, ReduceRule(), RenameRule())
  }


  def RenameRebuilder(renemas: mutable.Map[Expr, Expr], name: String = "RenameRebuilder"): RebuildPass = RebuildPass(name, {
    case e if renemas.contains(e) => renemas(e)
  })

  def RenamePdsRebuilder(renemas: mutable.Map[Long, Expr], name: String = "RenamePdRebuilder"): RebuildPass = RebuildPass(name, {
    case ParamUse(pd) if renemas.contains(pd.id) => renemas(pd.id)
  })

  def RenamePdPass(pd: ParamDef, target: Expr, name: String = "RenamePdRebuilder"): RebuildPass = RebuildPass(name, {
    case ParamUse(cpd) if cpd.id == pd.id => target
  }, isFixPoint = false)

  def CountVisitor(): VisitPass = {
    var counter = 0
    VisitPass("CountVisitor", {
      case _ => counter = counter + 1
    }, Some(() => println(counter)))
  }

  def DummyVisitPass(): VisitPass = VisitPass("DummyVisitPass", {
    case _ => Unit
  })

  def DummyRebuildPass(): RebuildPass = RebuildPass("DummyRebuildPass", {
    case other => other
  })

}
