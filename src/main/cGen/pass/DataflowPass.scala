package cGen.pass

import cGen.AllocKind.Heap
import cGen.DPSPass.isScalar
import cGen.{Alloc, AllocExpr, AllocKind, ArrayAccess, Assign, BinaryOp, CGenLet, CLambda, ConstantExpr, DBuild, DExternFunctionCall, DId, DIf, DIfold, Debug, DestArrayAccess, DestArrayAccessExpr, DestBuild, DestBuildExpr, DestSelect, DestSelectExpr, ExternFunctionCall, Free, FreeExpr, Id, IdxOffsetOp, Select, SingleOp, Tuple, TypeConv, ValueAt, ValueAtExpr, VoidType, VoidTypeT}
import core.util.IRDotGraph
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import sun.reflect.generics.reflectiveObjects.NotImplementedException

import scala.annotation.tailrec
import scala.collection.mutable

object DataflowPass {

  def RemoveValueAtRule(): RebuildPass = RebuildPass("RemoveValueAtRule",
    rewrite = {
      case CGenLet(_, _: ValueAtExpr, a, _) => a
    },
    isFixPoint = false)

  // before using this rule, all alloc should be placed in the heap
  def ChangeAllocKindRule(): RebuildPass = RebuildPass("ChangeAllocKindRule", {
    case Alloc(t, Heap, _) if isScalar(t) => Alloc(t, AllocKind.Stack)
  })

  object ANF {
    def apply(expr: Expr): Expr = {
      val ANFExpr = _ANF(expr)
      ANFLiftingRule()(ANFExpr)
    }

    def isFunctionCall(expr: Expr): Boolean = expr match {
      case _: ParamDef => false
      case _: ParamUse => false
      case _: ConstantExpr => false
      case _ => true
    }

    def toLet(place: Seq[Expr] => Expr, pairs: (ParamDef, Expr)*): Expr = {
      val exprs = pairs map { pair =>
        if (isFunctionCall(pair._2)) ParamUse(pair._1)
        else pair._2
      }
      pairs.foldLeft(place(exprs))((acc, pair) =>
        if (isFunctionCall(pair._2)) CGenLet(pair._1, pair._2) apply acc
        else acc
      )
    }

    def ANFLiftingRule(): RebuildPass = RebuildPass("ANFLiftingRule", {
      case CGenLet(p0, b0, CGenLet(p1, b1, a1, _), _) => CGenLet(p1, CGenLet(p0, b0, b1), a1)
    })

    def _ANF(expr: Expr): Expr = expr match {
      case CGenLet(p, b, a, _) =>
        //        val newa = _ANF(a)
        //        val newp = ParamDef(newa.t)
        //        CGenLet(newp, rewritePd(_ANF(b), p, ParamUse(newp)), newa)
        CGenLet(p, _ANF(b), _ANF(a)) // TODO: make sure the type of _ANF(a) will not change
      case BinaryOp(in1, in2, op, isBool, _) =>
        val p1 = ParamDef(in1.t)
        val p2 = ParamDef(in2.t)
        toLet(exprs => BinaryOp(exprs.head, exprs(1), op, isBool), (p1, _ANF(in1)), (p2, _ANF(in2)))
      case SingleOp(in, op, t) =>
        val p = ParamDef(in.t)
        toLet(exprs => SingleOp(exprs.head, op), (p, _ANF(in)))
      case Id(in, t) =>
        val p = ParamDef(in.t)
        toLet(exprs => Id(exprs.head), (p, _ANF(in)))
      case Assign(in, dest, t) =>
        val p = ParamDef(in.t)
        val destp = ParamDef(dest.t)
        toLet(exprs => DId(exprs.head, exprs(1)), (p, _ANF(in)), (destp, _ANF(dest)))
      case b@DBuild(idxf, n, ltt, _) => DBuild(_ANF(idxf), n, ltt)
      case DIfold(idxf, init, d, n, _) =>
        val p = ParamDef(init.t)
        toLet(exprs => DIfold(_ANF(idxf), exprs.head, d, n), (p, _ANF(init)))
      case CLambda(p, body, _) =>
        CLambda(p, _ANF(body))
      case DIf(computeCond, cond, b0, b1, _) =>
        //        toLet(exprs => DIf(_ANF(computeCond), cond, exprs.head, exprs(1)), Seq(b0, b1).map(in => (ParamDef(in.t), _ANF(in))): _*)
        DIf(_ANF(computeCond), _ANF(cond), _ANF(b0), _ANF(b1))
      case DExternFunctionCall(name, ins, intvs, _) =>
        val hi = ins.map(in => (ParamDef(in.t), _ANF(in)))
        toLet(exprs => DExternFunctionCall(name, exprs, intvs), hi: _*)
      case ArrayAccess(in, idx, _) =>
        val pin = ParamDef(in.t)
        val pidx = ParamDef(idx.t)
        toLet(exprs => ArrayAccess(exprs.head, exprs(1)), (pin, _ANF(in)), (pidx, _ANF(idx)))
      case DestArrayAccess(in, idx, _) =>
        val p = ParamDef(in.t)
        val idxp = ParamDef(idx.t)
        toLet(exprs => DestArrayAccess(exprs.head, exprs(1)), (p, _ANF(in)), (idxp, _ANF(idx)))
      case TypeConv(in, t) =>
        val p = ParamDef(in.t)
        toLet(exprs => TypeConv(exprs.head, t), (p, _ANF(in)))
      case Tuple(in1, in2, _) =>
        val p1 = ParamDef(in1.t)
        val p2 = ParamDef(in2.t)
        toLet(exprs => Tuple(exprs.head, exprs(1)), (p1, _ANF(in1)), (p2, _ANF(in1)))
      case Select(in, selection, _) =>
        val p = ParamDef(in.t)
        toLet(exprs => Select(exprs.head, selection), (p, _ANF(in)))
      case DestSelect(in, selection, _) =>
        val p = ParamDef(in.t)
        toLet(exprs => DestSelect(exprs.head, selection), (p, _ANF(in)))
      case db: DestBuildExpr => db
      case pd: ParamDef => pd
      case pu: ParamUse => pu
      case cs: ConstantExpr => cs
      case alloc: AllocExpr => alloc
      case other => other
//        throw new Exception(s"ANF not defined: ${other.toShortString}")
    }
  }

  object Hoist {
    def apply() = HoistPass()

    def HoistPass(): RebuildPass = RebuildPass("HoistPass", {
      case DBuild(CLambda(idx, e, _), n, ltt, _) =>
        val hoisted = mutable.ArrayBuffer[(ParamDef, Expr)]()
        val boundVars = mutable.Set[Long](idx.id)
        findBoundVars(e)(boundVars)
        val rest = findFree(e)(boundVars, hoisted)
        hoisted.foldRight(DBuild(CLambda(idx, rest), n, ltt): Expr)((pairs, r) => CGenLet(pairs._1, r, pairs._2))
      case DIfold(CLambda(idx, CLambda(acc, e, _), _), init, d, n, _) =>
        val hoisted = mutable.ArrayBuffer[(ParamDef, Expr)]()
        val boundVars = mutable.Set[Long](idx.id, acc.id)
        findBoundVars(e)(boundVars)
        val rest = findFree(e)(boundVars, hoisted)
        hoisted.foldRight(DIfold(CLambda(idx, CLambda(acc, rest)), init, d, n): Expr)((pairs, r) => CGenLet(pairs._1, r, pairs._2))
    }, isFixPoint = false)

    def findFree(e: Expr)(implicit boundVars: mutable.Set[Long], hoisted: mutable.ArrayBuffer[(ParamDef, Expr)]): Expr = e match {
      case CGenLet(p, b, a, _) if isFree(a) => hoisted.append((p, a)); findFree(b)
      case CGenLet(p, b, a, _) => CGenLet(p, findFree(b), a)
      case other => other // TODO: Handle the last one
    }

    def findBoundVars(e: Expr)(implicit boundVars: mutable.Set[Long]): Unit = {
      @tailrec
      def helper(e: Expr)(implicit boundVars: mutable.Set[Long]): Unit = e match {
        case CGenLet(p, b, a, _) if !isFree(a)(boundVars) => {
          boundVars += p.id ++= defineAt(a)
        };
          helper(b)
        case CGenLet(p, b, a, _) =>
          helper(b)
        case other => boundVars ++= defineAt(other)
      }

      var len = 0
      do {
        len = boundVars.size
        helper(e)
      } while (len != boundVars.size)
    }

    def isFree(e: Expr)(implicit boundVars: mutable.Set[Long]): Boolean = {
      var free = true
      VisitPass("HoistVisit", {
        case pd: ParamDef if boundVars.contains(pd.id) => free = false
      }, isMuted = true)(e)
      free
    }

    def defineAt(e: Expr): Set[Long] = e match {
      case CLambda(_, b, _) => defineAt(b)
      case CGenLet(p, b, a, _) => defineAt(b) ++ defineAt(a) + p.id
      case BinaryOp(_, _, _, _, _) => Set()
      case SingleOp(_, _, _) => Set()
      case Id(_, _) => Set()
      case Assign(_, ParamUse(pd), _) => Set(pd.id)
      case DIfold(idxf, _, _, _, _) => defineAt(idxf)
      case DBuild(idxf, _, _, _) => defineAt(idxf)
      case DIf(computeCond, cond, b0, b1, _) => defineAt(computeCond) ++ defineAt(cond) ++ defineAt(b0) ++ defineAt(b1)
      case DExternFunctionCall(name, ins, intvs, _) => Set(ins.head.asInstanceOf[ParamUse].p.id)
      case ArrayAccess(_, _, _) => Set()
      case DestArrayAccess(_, _, _) => Set()
      case IdxOffsetOp(_, _, _, _) => Set()
      case Alloc(_, _, _) => Set()
      case ValueAt(_, _) => Set()
      case TypeConv(_, _) => Set()
      case DestSelect(_, _, _) => Set()
      case Select(_, _, _) => Set()
      case _ => throw new Exception(s"defineAt can't handle $e")
    }

  }



  // isLetBody is used to check if we are in the tail of a let expression chain
  // should be used in ANF
  def InsertFree(expr: Expr, isLetBody: Boolean = false)(implicit toFree: Seq[FreeExpr] = Seq()): Expr = expr match {
    case CLambda(p, b, _) => CLambda(p, InsertFree(b))
    case CGenLet(p, b, a: AllocExpr, _) => {
      if (Debug.PROFILE >= 2) println(s"[Free] found $a, ${a.ak} ${a.t}")
      CGenLet(p, InsertFree(b)(toFree :+ Free(ParamUse(p), a.ak)), a)
    }
    case CGenLet(p, b, a, _) => CGenLet(p, InsertFree(b, true), InsertFree(a)(Seq()))
    case DBuild(idxf, n, ltt, _) => {
      val init: Expr = DBuild(InsertFree(idxf)(Seq()), n, ltt)
      if (isLetBody) toFree.foldRight(init)(CGenLet(ParamDef(VoidType()), _, _))
      else init
    }
    case DIfold(idxf, init, dest, ltt, _) =>
      val _init: Expr = DIfold(InsertFree(idxf)(Seq()), InsertFree(init)(Seq()), InsertFree(dest)(Seq()), ltt)
      if (isLetBody) toFree.foldRight(_init)(CGenLet(ParamDef(VoidType()), _, _))
      else _init
    case DIf(computeCond, cond, branch0, branch1, _) =>
      val init: Expr = DIf(InsertFree(computeCond)(Seq()), InsertFree(cond)(Seq()), InsertFree(branch0)(Seq()), InsertFree(branch1)(Seq()))
      if (isLetBody) toFree.foldRight(init)(CGenLet(ParamDef(VoidType()), _, _))
      else init
    case other =>
      if (Debug.PROFILE >= 2) println(s"[Free] $toFree ${toFree.map(_.ak)}")
      if (isLetBody) toFree.foldRight(other)(CGenLet(ParamDef(VoidType()), _, _))
      else other
  }

  // find out all unused def
  // should be only used in ANF
  case class UnusedDefAnalysis() extends Pass {
    var m: mutable.Map[Expr, Boolean] = mutable.Map()

    override val isMuted: Boolean = false

    def visit(defs: mutable.Set[Long], uses: mutable.Set[Long]): VisitPass = VisitPass("UnusedDefVisit", {
      case CGenLet(p, _, _, _) => defs.add(p.id)
      case ParamUse(p) => uses.add(p.id)
    })

    def rebuild(unuseds: mutable.Set[Long]): RebuildPass = RebuildPass("UnusedDefRebuild", {
      // when p is a VoidType, it means that the arg will have side-effect (an alloc or a write)
      case CGenLet(p, b, _, _) if unuseds.contains(p.id) && !p.t.isInstanceOf[VoidTypeT] => b
    })

    override def run(e: Expr): Expr = {
      var tree = e
      val defs: mutable.Set[Long] = mutable.Set()
      val uses: mutable.Set[Long] = mutable.Set()
      tree = TypeChecker.check(tree)
      tree = visit(defs, uses)(tree)
      rebuild(defs -- uses)(tree)
    }
  }
}
