package cGen.pass

import cGen.Debug
import cGen.Util.{formatTime, tic, toc}
import core.{Expr, FunTypeT, FunctionCall, ShirIR, ParamDef, ParamUse, TypeChecker, UnknownType}

import scala.annotation.tailrec
import scala.collection.mutable


trait Pass {
  val name: String = this.getClass.getSimpleName
  val isFixPoint: Boolean = true
  val isMuted: Boolean

  protected var curModified = false // indicates the modified status of the on going apply
  var modified = false // indicates the final status of the finished apply
  var rebuildTime: Long = 0
  var rebuildNum: Int = 0
  var rewriteTime: Long = 0
  var rewriteNum: Int = 0

  def apply(_ne: Expr, _isFixPoint: Boolean = this.isFixPoint): Expr = {
    if (Debug.PROFILE >= 2 && !isMuted) println(s"Apply Rule $name")
    val r = _apply(_ne, _isFixPoint)
    if (Debug.PROFILE >= 1 && !isMuted) println(summary())
    r
  }

  def _apply(_ne: Expr, isFixPoint: Boolean): Expr = {
    modified = false
    var ne = _ne
    var iter = 0
    curModified = true
    while (curModified) {
      curModified = false
      val before = tic()
      ne = run(ne)
      if (Debug.PROFILE >= 2 && !isMuted) println(s"$name (iter $iter) finish takes ${formatTime(toc(before))}")
      if (!isFixPoint) {
        modified = false // when isFixPoint == false, always set modified to be false
        return ne
      }
      if (curModified) iter += 1
    }
    if (iter != 0) modified = true // set the final status of this apply
    ne
  }

  def run(e: Expr): Expr

  def summary(): String = s"[$name] rebuild: ${formatTime(rebuildTime)}, $rebuildNum times, rewrite: ${formatTime(rewriteTime)}, $rewriteNum times"

  // a helper function to pipeline multiple passes
  def pipeline(e: Expr, passes: Pass*): Expr = passes.foldLeft(e)((e, pass) => {
    val newE = pass(e)
    this.rewriteTime += pass.rewriteTime
    this.rewriteNum += pass.rewriteNum
    this.rebuildTime += pass.rebuildTime
    this.rebuildNum += pass.rebuildNum
    newE
  })

  def resetStat(): Unit = {
    rebuildTime = 0
    rebuildNum = 0
    rewriteTime = 0
    rewriteNum = 0
  }

}

object Pass {
  @tailrec
  def orchestrate(_ne: Expr, rules: Pass*)(implicit callbacks: Seq[Expr => Unit] = Seq()): Expr = {
    var ne = _ne
    // apply all rule until no rewrite happens
    if (rules.exists(nrule => {
      ne = nrule(ne)
      callbacks.foreach(f => f(ne))
      nrule.modified
    })) {
      rules.foreach(_.resetStat())
      orchestrate(ne, rules: _*)
    }
    else ne
  }
}

case class VisitPass(override val name: String, visit: PartialFunction[Expr, Unit],
                     callback: Option[() => Unit] = None, override val isFixPoint: Boolean = false,
                     override val isMuted: Boolean = false) extends Pass {
  def isDefinedAt(e: Expr): Boolean = visit.isDefinedAt(e)

  def visiting(node: ShirIR): Unit = node match {
    case e: Expr if isDefinedAt(e) =>
      rebuildNum += 1
      visit(e)
    case _ =>
  }

  def run(e: Expr): Expr = {
    e.visit(visiting) // visit is top-down
    callback match {
      case Some(f) => f()
      case None =>
    }
    e
  }
}

case class RebuildPass(override val name: String,
                       rewrite: PartialFunction[Expr, Expr],
                       stopRule: Option[PartialFunction[Expr, Expr]] = None,
                       override val isFixPoint: Boolean = true,
                       isTypeCheck: Boolean = false,
                       override val isMuted: Boolean = false
                      ) extends Pass {
  def isDefinedAt(e: Expr): Boolean = rewrite.isDefinedAt(e)

  def rewriting(node: ShirIR): (ShirIR, Boolean) = node match {
    case e: Expr if isDefinedAt(e) =>
      curModified = true
      val before = tic()
      val r = rewrite(e)
      rewriteTime += toc(before)
      rewriteNum += 1
      if (Debug.PROFILE >= 2) println(s"$name(${formatTime(toc(before))}): $e => $r")
      (r, true)
    case other => (other, false)
  }

  // a better version of visitAndRebuild
  // only build when rewrite happens
  def visitAndRebuild(e: ShirIR): (ShirIR, Boolean) = {
    stopRule match {
      case Some(stop) => e match {
        case e: Expr if stop.isDefinedAt(e) =>
          if (Debug.PROFILE >= 2) println(s"$name stops at $e")
          return (e, false) // stop delving in
        case _ =>
      }
      case None =>
    }
    val children: Seq[(ShirIR, Boolean)] = e.children.map(visitAndRebuild)
    if (children.exists(_._2)) { // indicates that there is a rewrite on one of the children
      val before = tic()
      val newE = e match {
        case fc: FunctionCall => // special case on FunctionCall, because the FunctionCall's build cannot infer the type correctly
          val newChildren = children.map(_._1)
          val t = newChildren.head.asInstanceOf[Expr].t match {
            case ft: FunTypeT => ft.outType
            case UnknownType => UnknownType
            case _ => throw new Exception("unhandled type on FunctionCall")
          }
          e.build(newChildren = newChildren.dropRight(1) ++ Seq(t))
        case _ => e.build(newChildren = children.map(_._1))
      }
      rebuildTime += toc(before)
      rebuildNum += 1
      (rewriting(newE)._1, true) // the tree branch all the way up to the root needed to be rewritten
    }
    else rewriting(e) // no write on its children or no child
  }

  def run(e: Expr): Expr = {
    var tree = e
    if (isTypeCheck) {
      val before = tic()
      println("[TypeCheck] begins")
      try {
        tree = TypeChecker.check(tree)
      } catch {
        case other: Throwable => throw other
      }
      println(s"[TypeCheck] takes ${formatTime(toc(before))}")
    }
    visitAndRebuild(tree)._1.asInstanceOf[Expr]
  }
}
