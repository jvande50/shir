package cGen.pass

import algo.SeqTypeT
import cGen.AllocKind.Heap
import cGen.DPSPass.isEager
import cGen.pass.CommonPass.{RenamePdPass, RenamePdsRebuilder}
import cGen._
import core._
import core.util.IRDotGraph
import lift.arithmetic.Cst

import scala.collection.mutable

object RemoveViewPass {
  def IntroIdxPass(): RebuildPass = RebuildPass("IntroIdxRule", {
    case map: MapExpr =>
      val f = map.args.head
      val in = TypeChecker.check(map.args.tail.head)
      val idx = ParamDef(IntType(32))
      val idxf = CLambda(idx, CFC(f, ArrayAccess(in, ParamUse(idx))))
      var isEagerFlag = 0
      f.visit {
        case e: Expr if isEager(e) => isEagerFlag = 1
        case _ =>
      }
      isEagerFlag = map.ltt.ae.evalInt match {
        case 99 => isEagerFlag // based on whether there are eager expr
        case 0 => 0 // not eager
        case 1 => 1 // is eager
        case _ => throw new Exception("Should not reach here")
      }
      in match {
        case b: BuildExpr => Build(idxf, b.n.asInstanceOf[ArithTypeT], isEagerFlag)
        case _ => Build(idxf, map.t.asInstanceOf[ArrayTypeT].len, isEagerFlag)
      }
    case reduce: ReduceExpr =>
      val idx = ParamDef(IntType(32))
      val f = reduce.args.head // U -> T -> U
      val init = reduce.args(1)
      val u = ParamDef(init.t)
      val in = TypeChecker.check(reduce.args(2))
      val idxf = CLambda(idx, CLambda(u, CFC(CFC(f, ParamUse(u)), ArrayAccess(in, ParamUse(idx)))))
      in match {
        case b: BuildExpr => Ifold(idxf, init, b.n.asInstanceOf[ArithTypeT])
        case _ => Ifold(idxf, init, in.t.asInstanceOf[ArrayTypeT].len)
      }
    case Range(start, step, n, _) =>
      Build({
        val idx = ParamDef(IntType(32))
        CLambda(idx, BinaryOp(start, BinaryOp(ParamUse(idx), step, AlgoOp.Mul), AlgoOp.Add))
      }, n)
    case AccMap(f, init, in, _) => {
      val acc = ParamDef(init.t)
      val idx = ParamDef(IntType(32))
      val idxf = CLambda(acc, CLambda(idx, CFC(CFC(f, ParamUse(acc)), ArrayAccess(in, ParamUse(idx)))))
      in match {
        case b: BuildExpr => IdxAccMap(idxf, init, b.n.asInstanceOf[ArithTypeT])
        case _ => IdxAccMap(idxf, init, in.t.asInstanceOf[ArrayTypeT].len)
      }
    }
  })

  def RemoveSourceView(): RebuildPass = RebuildPass("RemoveSourceView", {
    case ArrayAccess(CGenLet(p, b, a, _), idx, _) => CGenLet(p, ArrayAccess(b, idx), a)
    case Select(CGenLet(p, b, a, _), which, _) => CGenLet(p, Select(b, which), a)
    case ArrayAccess(b@Build(idxf, _, _, _), idx, _) if !b.isEager => CFC(idxf, idx) // fuse when build is lazy
    case ArrayAccess(Split(in, cs, _), idx, _) =>
      val n = in.t.asInstanceOf[ArrayTypeT].len
      val idx0 = ParamDef(IntType(32))
      val idx1 = ParamDef(IntType(32))
      val insideBuild = Build(CLambda(idx1, ArrayAccess(in, IdxBiOp(IdxOffsetOp(ParamUse(idx0), cs, AlgoOp.Mul), ParamUse(idx1), AlgoOp.Add))), cs)
      ArrayAccess(Build(CLambda(idx0, insideBuild), n.ae / cs.ae), idx)
    case ArrayAccess(join: JoinExpr, idx, _) => ArrayAccess({
      val in = join.args.head
      val cs = join.types.head.asInstanceOf[ArithTypeT]
      val n = in.t.asInstanceOf[ArrayTypeT].len.asInstanceOf[ArithTypeT]
      val idx = ParamDef(IntType(32))
      Build(CLambda(idx, ArrayAccess(ArrayAccess(in, IdxOffsetOp(ParamUse(idx), cs, AlgoOp.Div)), IdxOffsetOp(ParamUse(idx), cs, AlgoOp.Mod))), n.ae * cs.ae)
    }, idx)
    case ArrayAccess(zip: ZipExpr, idx, _) =>
      ArrayAccess({
        val in = zip.args.head
        val idxPd = ParamDef(IntType(32))
        val idxf = CLambda(idxPd, Tuple(ArrayAccess(Select(in, 0), ParamUse(idxPd)), ArrayAccess(Select(in, 1), ParamUse(idxPd))))
        Build(idxf, in.t.asInstanceOf[NamedTupleStructTypeT].namedTypes.head._2.asInstanceOf[ArrayTypeT].len)
      }, idx)
    case ArrayAccess(zip: Zip3Expr, idx, _) =>
      ArrayAccess({
        val in = zip.args.head
        val idx = ParamDef(IntType(32))
        val idxf = CLambda(idx, Tuple3(ArrayAccess(Select(in, 0, 3), ParamUse(idx)), ArrayAccess(Select(in, 1, 3), ParamUse(idx)), ArrayAccess(Select(in, 2, 3), ParamUse(idx))))
        Build(idxf, in.t.asInstanceOf[NamedTupleStructTypeT].namedTypes.head._2.asInstanceOf[ArrayTypeT].len)
      }, idx)
    case ArrayAccess(Slice(arr, offset, _), idx, _) => ArrayAccess(arr, BinaryOp(idx, offset, AlgoOp.Add))
    case ArrayAccess(Concat(_in1, _in2, ArithType(Cst(1)), _), idx, _) => {
      val in1 = TypeChecker.check(_in1)
      val in2 = TypeChecker.check(_in2)
      If(BinaryOp(idx, Constant(in1.t.asInstanceOf[ArrayTypeT].len.ae.evalInt, IntType(32)), AlgoOp.Lt, isBool = true),
        Id(ArrayAccess(in1, idx)),
        Id(ArrayAccess(in2, BinaryOp(idx, Constant(in1.t.asInstanceOf[ArrayTypeT].len.ae.evalInt, IntType(32)), AlgoOp.Sub))))
    }
    case ArrayAccess(Push(in, _), _, _) => in
    case ArrayAccess(Permute(f, in, _), idx, _) => ArrayAccess(in, CFC(f, idx))
    case Select(Tuple(in1, in2, _), n, _) => n.ae match {
      case Cst(0) => in1
      case Cst(1) => in2
      case _ => throw new Exception("Should not reach")
    }
    case Select(Tuple3(in1, in2, in3, _), n, _) => n.ae match {
      case Cst(0) => in1
      case Cst(1) => in2
      case Cst(2) => in3
      case _ => throw new Exception("Should not reach")
    }
  })

  def AggressiveBuildFuse(): RebuildPass = RebuildPass("AggressiveBuildFuse", {
    case ArrayAccess(b@Build(idxf, _, _, _), idx, _) => CFC(idxf, idx)
  })

  def ConcatLiftPass(): RebuildPass = RebuildPass("ConcatLiftPass", {
    case b@Build(CLambda(idx, CFC(f, Concat(in1, in2, _, _), _), _), n, ltt, _) if b.isEager =>
      Concat(Build(CLambda(idx, CFC(f, in1)), in1.t.asInstanceOf[SeqTypeT].len), Build(CLambda(idx, CFC(f, in2)), in2.t.asInstanceOf[SeqTypeT].len, ltt))
  })

  def RemoveDestView(): RebuildPass = RebuildPass("RemoveDestView", {
    case Build(CLambda(idx, CGenLet(allocPd, inlet@CGenLet(_, _, _, _), Alloc(_, ak, _), _), _), n, ltt, t) =>
      val dt = t
      val d = ParamDef(AddressType(dt))
      val newDest = DestArrayAccess(ParamUse(d), ParamUse(idx))
      val db = DBuild(CLambda(idx, makeLetReturnsVoid(inlet)), n, ltt = 1)
      RenamePdPass(allocPd, newDest, "AllocRenameRule1")(
        CGenLet(d, Alloc(dt, ak)) apply db >> ValueAt(ParamUse(d))
      )
    case Ifold(CLambda(idx, CLambda(acc, CGenLet(d, inlet@CGenLet(_, _, _, _), Alloc(_, ak, _), _), _), _), init, n, t) =>
      val dt = t
      val di = DIfold(CLambda(idx, CLambda(acc, makeLetReturnsVoid(inlet))), init, ParamUse(d), n)
      CGenLet(d, Alloc(dt, ak)) apply di >> ValueAt(ParamUse(d))
    case If(CGenLet(dcond, computeCond, allocCond@Alloc(_, _, _), _), CGenLet(d0, in0, alloc0@Alloc(t0, _, _), _), CGenLet(d1, in1, _@Alloc(t1, _, _), _), _) =>
      assert(t0 == t1)
      CGenLet(d0, alloc0) apply DIf(CGenLet(dcond, computeCond, allocCond), ValueAt(ParamUse(dcond)), in0, RenamePdPass(d1, ParamUse(d0))(in1)) >> ValueAt(ParamUse(d0))
    case split@Split(CGenLet(allocPd, body, Alloc(_, _, _), _), _, _) =>
      val dt = split.t
      val newDest = ParamDef(AddressType(dt))
      RenamePdPass(allocPd, DestBuild(split, ParamUse(newDest)), "AllocBackwardLiftRename")(
        CGenLet(newDest, body, Alloc(dt, Heap)))
    case concat@Concat(CGenLet(allocPd1, in1, Alloc(_, _, _), _), CGenLet(allocPd2, in2, Alloc(_, _, _), _), ltt, _) =>
      val renames: mutable.Map[Long, Expr] = mutable.Map()
      val dt = concat.t
      val newDest = ParamDef(AddressType(dt))
      val db1 = DestBuild(concat, ParamUse(newDest), which = 0)
      val db2 = DestBuild(concat, ParamUse(newDest), which = 1)
      renames.put(allocPd1.id, db1)
      renames.put(allocPd2.id, db2)
      RenamePdsRebuilder(renames, "AllocBackwardLiftRename")(
        CGenLet(newDest, Alloc(dt, Heap)) apply (
          CGenLet(ParamDef(in1.t), in1) apply (
            CGenLet(ParamDef(in2.t), in2) apply
              ValueAt(ParamUse(newDest))))
      )
    case Push(CGenLet(allocPd, in, _: AllocExpr, _), dt) =>
      val renames: mutable.Map[Long, Expr] = mutable.Map()
      val newDest = ParamDef(AddressType(dt)) // [A]1*n
      renames.put(allocPd.id, DestArrayAccess(ParamUse(newDest), Constant(0, IntType(32))))
      RenamePdsRebuilder(renames, "AllocBackwardLiftRename")(
        CGenLet(newDest, Alloc(dt, Heap)) apply (
          CGenLet(ParamDef(in.t), in) apply
            ValueAt(ParamUse(newDest)))
      )
    case Tuple(CGenLet(allocPd1, in1, _: AllocExpr, _), CGenLet(allocPd2, in2, _: AllocExpr, _), dt) => {
      val renames: mutable.Map[Long, Expr] = mutable.Map()
      val newDest = ParamDef(AddressType(dt))
      val db1 = DestSelect(ParamUse(newDest), 0);
      val db2 = DestSelect(ParamUse(newDest), 1);
      renames.put(allocPd1.id, db1);
      renames.put(allocPd2.id, db2);
      RenamePdsRebuilder(renames, "AllocBackwardLiftRename")(
        CGenLet(newDest, Alloc(dt, Heap)) apply (
          CGenLet(ParamDef(in1.t), in1) apply (
            CGenLet(ParamDef(in2.t), in2) apply
              ValueAt(ParamUse(newDest))
            )
          )
      )
    }
    case IdxAccMap(CLambda(acc, CLambda(idx, CGenLet(allocPd, body, _: AllocExpr, _), _), _), init, n, dt) =>
      val renames: mutable.Map[Long, Expr] = mutable.Map()
      val d = ParamDef(AddressType(dt))
      val newDest = DestArrayAccess(ParamUse(d), ParamUse(idx))
      renames.put(allocPd.id, newDest)
      renames.put(acc.id, ValueAt(ParamUse(d)))
      RenamePdsRebuilder(renames, "AllocBackwardLiftRename")(
        CGenLet(d, Alloc(dt, Heap)) apply ({
          val idx1 = ParamDef(IntType(32))
          DBuild(CLambda(idx1, DId(ArrayAccess(init, ParamUse(idx1)), DestArrayAccess(ParamUse(d), ParamUse(idx1)))), n, ltt = 1)
        } >> DBuild(CLambda(idx, makeLetReturnsVoid(body)), n, ltt = 1)) >> ValueAt(ParamUse(d))
      )
    case Permute(f, CGenLet(allocPd, in, alloc: AllocExpr, _), dt) => {
      val permutePass = RebuildPass("PermutePass", {
        case DestArrayAccess(d: ParamUse, idx, _) if d.p.id == allocPd.id => DestArrayAccess(ParamUse(allocPd), CFC(f, idx))
      }, isFixPoint = false)
      CGenLet(allocPd, alloc) apply permutePass(in)
    }
  })

  def DestBuildFusePass(): RebuildPass = RebuildPass("DestBuildFusePass", {
    case DestArrayAccess(DestBuild(idxf, _, _), idx, _) => CFC(idxf, idx)
  })

  def AllocLetLiftPass(): RebuildPass = RebuildPass("AllocLetLiftPass", {
    case CGenLet(p0, CGenLet(p1, in, alloc: AllocExpr, _), arg, _) if !arg.isInstanceOf[AllocExpr] => CGenLet(p1, CGenLet(p0, in, arg), alloc)
    case CGenLet(p0, CLambda(p1, body, _), arg, _) => CLambda(p1, CGenLet(p0, body, arg)) // when bottom-up rebuilding, CGenLet(p, f, arg)
    //    case ArrayAccess(CGenLet(p, b, alloc: AllocExpr, _), idx, _) => CGenLet(p, ArrayAccess(b, idx), alloc)
  })

  // it searchs from the root until reaches the first alloc
  def AllocLambdaLift(e: Expr): Expr = e match {
    case CLambda(p, b, _) => CLambda(p, AllocLambdaLift(b))
    case CGenLet(p, b, Alloc(_, _, _), _) => CLambda(p, b)
    case _ => {
      IRDotGraph(e).show()
      throw new Exception(s"$e Should not reach here in AllocLambdaLift")
    }
  }

  def makeLetReturnsVoid(e: Expr): Expr = {
    e match {
      case CGenLet(p, _: ValueAtExpr, a, _) => a
      case CGenLet(p, b, a, _) => CGenLet(p, makeLetReturnsVoid(b), a)
    }
    //    CGenLet(ParamDef(e.t), ParamUse(ParamDef(VoidType())),e)
  }
}
