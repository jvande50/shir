package cGen

import algo._
import cGen.AlgoOp.AlgoOp
import cGen.Lib.GetPosedRelatives
import cGen.TupleStructName.COMPLEX
import core._
import lift.arithmetic.Cst

object Lib {
  def addTup(implicit t: () => Type): CLambda = {
    val tup = ParamDef(TupleStructType(t().asInstanceOf[CGenDataTypeT], t().asInstanceOf[CGenDataTypeT]))
    CLambda(tup, Add(ParamUse(tup)))
  }

  def subTup(implicit t: () => Type): CLambda = {
    val tup = ParamDef(TupleStructType(t().asInstanceOf[CGenDataTypeT], t().asInstanceOf[CGenDataTypeT]))
    CLambda(tup, BinaryOp(Select(ParamUse(tup), 0), Select(ParamUse(tup), 1), AlgoOp.Sub))
  }

  def add2(implicit t: () => Type): CLambda = {
    val p1 = ParamDef(t())
    val p2 = ParamDef(t())
    CLambda(Seq(p1, p2), Add(Tuple(ParamUse(p1), ParamUse(p2))))
  }

  def mulTup(implicit t: () => Type): CLambda = {
    val tup = ParamDef(TupleStructType(t().asInstanceOf[CGenDataTypeT], t().asInstanceOf[CGenDataTypeT]))
    CLambda(tup, BinaryOp(Select(ParamUse(tup), 0), Select(ParamUse(tup), 1), AlgoOp.Mul))
  }

  def mul2: CLambda = {
    val p1 = ParamDef(IntType(32))
    val p2 = ParamDef(IntType(32))
    CLambda(Seq(p1, p2), Mul(Tuple(ParamUse(p1), ParamUse(p2))))
  }

  def zip(len: Int): CLambda = {
    val p1 = ParamDef(ArrayType(IntType(32), len))
    val p2 = ParamDef(ArrayType(IntType(32), len))
    CLambda(Seq(p1, p2), Zip(Tuple(ParamUse(p1), ParamUse(p2))))
  }

  def vectorAdd(len: Int)(implicit t: () => Type): CLambda = {
    val a = ParamDef(ArrayType(IntType(32), len))
    val b = ParamDef(ArrayType(IntType(32), len))
    val map = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))))
    CLambda(Seq(a, b), map)
  }

  def vectorSub(len: Int)(implicit t: () => Type): CLambda = vectorAdd(len) // TODO: support sub

  def vectorMul(len: Int)(implicit t: () => Type): CLambda = {
    val a = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val b = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val map = Map(Lib.mulTup, Zip(Tuple(ParamUse(a), ParamUse(b))))
    CLambda(Seq(a, b), map)
  }

  def zero(implicit t: () => Type): Expr = num(0)

  def one(implicit t: () => Type): Expr = num(1)

  def two(implicit t: () => Type): Expr = num(2)

  def three(implicit t: () => Type): Expr = num(3)

  def four(implicit t: () => Type): Expr = num(4)

  def six(implicit t: () => Type): Expr = num(6)

  def seven(implicit t: () => Type): Expr = num(7)

  def nine(implicit t: () => Type): Expr = num(9)

  def num(value: Double)(implicit t: () => Type) = Constant(value, t())

  def add(s1: Expr, s2: Expr): Expr = Add(Tuple(s1, s2))

  def sub(s1: Expr, s2: Expr): Expr = Sub(Tuple(s1, s2))

  def mul(s1: Expr, s2: Expr): Expr = Mul(Tuple(s1, s2))

  def div(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Div)

  def mod(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Mod)

  def pow(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Pow)

  def rightShift(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.RightShift)

  def leftShift(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.LeftShift)

  def cos(s: Expr): Expr = SingleOp(s, AlgoOp.Cos)

  def sin(s: Expr): Expr = SingleOp(s, AlgoOp.Cos)

  def minus(s: Expr): Expr = SingleOp(s, AlgoOp.Minus)

  def exp(s: Expr): Expr = SingleOp(s, AlgoOp.Exp)

  def log(s: Expr): Expr = SingleOp(s, AlgoOp.Log)

  def sqrt(s: Expr): Expr = SingleOp(s, AlgoOp.Sqrt)

  def ge(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Ge, isBool = true)

  def gt(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Gt, isBool = true)

  def lt(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Lt, isBool = true)

  def eq(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Eq, isBool = true)

  def at(e0: Expr, e1: Expr): Expr = ArrayAccess(e0, e1)

  def andand(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Andand, isBool = true)

  def VectorSum(v: Expr)(implicit t: () => Type): Expr = Reduce(add2, zero, v)

  def VectorAdd(v1: Expr, v2: Expr)(implicit t: () => Type): Expr = Map(addTup, Zip(Tuple(v1, v2)))

  def VectorMulElemwise(v1: Expr, v2: Expr)(implicit t: () => Type): Expr = Map(mulTup, Zip(Tuple(v1, v2)))

  def MatrixMulElemwise(m1: Expr, m2: Expr)(implicit t: () => Type): Expr = {
    val v1 = ParamDef(tt(m1).asInstanceOf[ArrayTypeT].et)
    val v2 = ParamDef(tt(m2).asInstanceOf[ArrayTypeT].et)
    val m = MatrixMap2(CLambda(v1, CLambda(v2, VectorMulElemwise(ParamUse(v1), ParamUse(v2)))), m1, m2)
    TypeChecker.check(m)
    m
  }

  def VectorSub(v1: Expr, v2: Expr)(implicit t: () => Type): Expr = Map(subTup, Zip(Tuple(v1, v2)))

  def VectorSubScaler(v: Expr, scaler: Expr)(implicit t: () => Type): Expr = {
    val elm = ParamDef(t())
    Map(CLambda(elm, sub(ParamUse(elm), scaler)), v)
  }

  def VectorSingleOp(v: Expr, op: AlgoOp)(implicit t: () => Type): Expr = {
    val elm = ParamDef(t())
    Map(CLambda(elm, SingleOp(ParamUse(elm), op)), v)
  }

  def VectorMax(v: Expr)(implicit t: () => Type): Expr = {
    val acc = ParamDef(t())
    val cur = ParamDef(t())
    val f = CLambda(acc, CLambda(cur, If(gt(ParamUse(acc), ParamUse(cur)), Id(ParamUse(acc)), Id(ParamUse(cur)))))
    Reduce(f, Constant(-1000, t()), v)
  }

  def SqNorm(v: Expr)(implicit t: () => Type): Expr = VectorSum(
    Build({
      val idx = ParamDef(IntType(32))
      CLambda(idx, BinaryOp(ArrayAccess(v, ParamUse(idx)), Constant(2, DoubleType()), AlgoOp.Pow))
    }, length(v))
    //    Map(mulTup, Zip(Tuple(v, v)))
  )

  def DotProd(v1: Expr, v2: Expr)(implicit t: () => Type): Expr = VectorSum(Map(mulTup, Zip(Tuple(v1, v2))))

  def MulByScalar(scalar: Expr, v: Expr)(implicit t: () => Type) = Map({
    val p = ParamDef(t())
    CLambda(p, mul(ParamUse(p), scalar))
  }, v)

  def complexMul(c0: Expr, c1: Expr): Expr = {
    Tuple(COMPLEX,
      sub(mul(Select(c0, 0), Select(c1, 0)), mul(Select(c0, 1), Select(c1, 1))),
      add(mul(Select(c0, 0), Select(c1, 1)), mul(Select(c0, 1), Select(c1, 0)))
    )
  }

  def complexAdd(c0: Expr, c1: Expr): Expr = {
    Tuple(COMPLEX, add(Select(c0, 0), Select(c1, 0)), add(Select(c0, 1), Select(c1, 1)))
  }

  def complexSub(c0: Expr, c1: Expr): Expr = {
    Tuple(COMPLEX, sub(Select(c0, 0), Select(c1, 0)), sub(Select(c0, 1), Select(c1, 1)))
  }

  // radParams is a view
  def RadialDistort(radParams: Expr, _proj: Expr) = {
    implicit val t: () => Type = () => DoubleType()
    val prog = new Program()
    val proj = prog.add(_proj)
    val rsq = prog.add(SqNorm(proj))
    val l0 = mul(ArrayAccess(radParams, zero(() => IntType(32))), rsq)
    val l1 = mul(mul(ArrayAccess(radParams, one(() => IntType(32))), rsq), rsq)
    val l = add(add(one, l0), l1)
    prog.build(MulByScalar(l, proj))
  }

  def Cross(v1: Expr, v2: Expr): Expr = {
    val c00 = Mul(Tuple(ArrayAccess(v1, Constant(1, IntType(32))), ArrayAccess(v2, Constant(2, IntType(32)))))
    val c01 = Mul(Tuple(ArrayAccess(v1, Constant(2, IntType(32))), ArrayAccess(v2, Constant(1, IntType(32)))))
    val c10 = Mul(Tuple(ArrayAccess(v1, Constant(2, IntType(32))), ArrayAccess(v2, Constant(0, IntType(32)))))
    val c11 = Mul(Tuple(ArrayAccess(v1, Constant(0, IntType(32))), ArrayAccess(v2, Constant(2, IntType(32)))))
    val c20 = Mul(Tuple(ArrayAccess(v1, Constant(0, IntType(32))), ArrayAccess(v2, Constant(1, IntType(32)))))
    val c21 = Mul(Tuple(ArrayAccess(v1, Constant(1, IntType(32))), ArrayAccess(v2, Constant(0, IntType(32)))))
    val c0 = Sub(Tuple(c00, c01))
    val c1 = Sub(Tuple(c10, c11))
    val c2 = Sub(Tuple(c20, c21))
    Concat(Concat(ToArray(c0), ToArray(c1)), ToArray(c2))
  }

  // rot is a forward view
  def RodriguesRotatePoint(rot: Expr, _x: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val x = prog.add(_x)
    val sqtheta = prog.add(SqNorm(rot))
    prog.build(
      If(BinaryOp(sqtheta, zero(() => DoubleType()), AlgoOp.Eq, isBool = true),
        {
          val prog = new Program()
          val theta = prog.add(SingleOp(sqtheta, AlgoOp.Sqrt))
          val costheta = prog.add(SingleOp(theta, AlgoOp.Cos))
          val sintheta = prog.add(SingleOp(theta, AlgoOp.Sin))
          val thetaInv = prog.add(BinaryOp(one, theta, AlgoOp.Div))
          val w = prog.add(MulByScalar(thetaInv, rot))
          val wCrossX = Cross(w, x)
          val tmp = mul(DotProd(w, x), sub(one, costheta))
          prog.build(VectorAdd(
            VectorAdd(MulByScalar(costheta, x), MulByScalar(sintheta, wCrossX)),
            MulByScalar(tmp, w)
          ))
        },
        {
          VectorAdd(x, Cross(rot, x))
        }
      )
    )
  }

  def Project(cam: Expr, X: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val Xcam = prog.add(RodriguesRotatePoint(
      Slice(cam, zero(() => IntType(32)), ArithType(3)),
      VectorSub(X, Slice(cam, three(() => IntType(32)), ArithType(3)))))
    val distorted = RadialDistort(
      Slice(cam, nine(() => IntType(32)), ArithType(2)),
      MulByScalar(BinaryOp(one, ArrayAccess(Xcam, two(() => IntType(32))), AlgoOp.Div), Slice(Xcam, zero(() => IntType(32)), ArithType(2)))
    )
    val proj = VectorAdd(Slice(cam, seven(() => IntType(32)), ArithType(2)), MulByScalar(ArrayAccess(cam, six(() => IntType(32))), distorted))
    prog.build(proj)
  }

  def ProjectAndSqNorm(cam: Expr, X: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val Xcam = prog.add(RodriguesRotatePoint(
      Slice(cam, zero(() => IntType(32)), ArithType(3)),
      VectorSub(X, Slice(cam, three(() => IntType(32)), ArithType(3)))))
    val distorted = RadialDistort(
      Slice(cam, nine(() => IntType(32)), ArithType(2)),
      MulByScalar(BinaryOp(one, ArrayAccess(Xcam, two(() => IntType(32))), AlgoOp.Div), Slice(Xcam, zero(() => IntType(32)), ArithType(2)))
    )
    val proj = VectorAdd(Slice(cam, seven(() => IntType(32)), ArithType(2)), MulByScalar(ArrayAccess(cam, six(() => IntType(32))), distorted))
    prog.build(SqNorm(proj))
  }

  def BundleAdjustment(a: Expr, b: Expr, cam: Expr): Expr = {
    val prog = new Program()
    prog.build({
      val j = prog.add(RadialDistort(a, b))
      Project(cam, j)
    })
  }

  class Program {
    var pairs = Seq[(ParamDef, Expr)]()

    def add(expr: Expr): ParamUse = {
      val p = ParamDef(TypeChecker.check(expr).t)
      pairs = pairs :+ (p, expr)
      ParamUse(p)
    }

    def :+(expr: Expr): ParamUse = add(expr)

    def build(expr: Expr): Expr = pairs.foldRight(expr)((pe, in) => CGenLet(pe._1, in, pe._2))
  }

  def vectorAdd3(len: Int): CLambda = {
    implicit val t: () => IntType = () => IntType(32)
    val a = ParamDef(ArrayType(IntType(32), len))
    val b = ParamDef(ArrayType(IntType(32), len))
    val c = ParamDef(ArrayType(IntType(32), len))
    val map1 = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))))
    val map2 = Map(TypeChecker.checkAssert(Lib.addTup), Zip(Tuple(ParamUse(c), map1)))
    CLambda(Seq(a, b, c), map2)
  }

  def vectorAdd3Opt(len: Int): CLambda = {
    val a = ParamDef(ArrayType(IntType(32), len))
    val b = ParamDef(ArrayType(IntType(32), len))
    val c = ParamDef(ArrayType(IntType(32), len))
    val tup = ParamDef(TupleStructType(IntType(32), IntType(32), IntType(32)))
    val add3 = CLambda(tup, Add(Tuple(Select(ParamUse(tup), 2, 3), (Add(Tuple(Select(ParamUse(tup), 0, 3), Select(ParamUse(tup), 1, 3)))))))
    val map = Map(add3, Zip3(Tuple3(ParamUse(a), ParamUse(b), ParamUse(c))))
    CLambda(Seq(a, b, c), map)
  }

  def LogSumExp(x: Expr)(implicit t: () => Type = () => DoubleType()): Expr = {
    val prog = new Program()
    val mx = prog.add(VectorMax(x))
    val xMinusMx = VectorSubScaler(x, mx)
    val emx = VectorSingleOp(xMinusMx, AlgoOp.Exp)
    prog.build(add(SingleOp(VectorSum(emx), AlgoOp.Log), mx))
  }

  def tri(n: Expr): Expr = {
    implicit val t = () => IntType(32)
    div(mul(n, add(n, one)), two)
  }

  def Qtimesv(q: Expr, _l: Expr, _v: Expr): Expr = {
    implicit val t = () => DoubleType()
    val l = TypeChecker.check(_l)
    val v = TypeChecker.check(_v)
    val i = ParamDef(IntType(32))
    val buildIdxf = CLambda(i, {
      val idx = ParamDef(IntType(32))
      val acc = ParamDef(t())
      val p0 = new Program()
      val tis = p0 :+ tri(sub(ParamUse(i), one(() => IntType(32))))
      val ifoldIdxf = CLambda(idx, CLambda(acc, {
        val p1 = new Program()
        val j = p1 :+ sub(ParamUse(idx), tis)
        val isRange = andand(ge(j, zero(() => IntType(32))), lt(j, ParamUse(i)))
        p1.build(If(isRange, add(ParamUse(acc), mul(ArrayAccess(l, ParamUse(idx)), ArrayAccess(v, j))), Id(ParamUse(acc))))
      }))
      val sum = Ifold(ifoldIdxf, zero, length(l))
      p0.build(add(sum, mul(exp(ArrayAccess(q, ParamUse(i))), ArrayAccess(v, ParamUse(i)))))
    })
    Build(buildIdxf, length(v), ltt = 1)
  }

  def length(v: Expr): ArithTypeT = TypeChecker.check(v).t.asInstanceOf[ArrayTypeT].len

  def MatrixMul(m1: Expr, m2: Expr)(implicit t: () => Type): Expr = {
    val len = cols(TypeChecker.check(m1))
    val m2T = TypeChecker.check(MatrixTranspose(m2))
    val _m1 = TypeChecker.check(m1)
    val _m2 = TypeChecker.check(m2)
    val row1 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val row2 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))

    TypeChecker.check(Map(CLambda(row2,
      DotProd(ParamUse(row1), ParamUse(row2))
    ), m2T))

    Map(CLambda(row1,
      Map(CLambda(row2,
        DotProd(ParamUse(row1), ParamUse(row2))
      ), m2T)), m1)
  }

  def MatrixAdd(m1: Expr, m2: Expr): Expr = {
    implicit val t = () => DoubleType()
    val p1 = ParamDef(tt(m1).asInstanceOf[ArrayTypeT].et)
    val p2 = ParamDef(tt(m2).asInstanceOf[ArrayTypeT].et)
    MatrixMap2(CLambda(p1, CLambda(p2, VectorAdd(ParamUse(p1), ParamUse(p2)))), m1, m2)
  }

  def MatrixMap2(f: Expr, m1: Expr, m2: Expr): Expr = {
    val in = ParamDef(TupleStructType(tt(m1).asInstanceOf[ArrayTypeT].et.asInstanceOf[CGenDataTypeT], tt(m2).asInstanceOf[ArrayTypeT].et.asInstanceOf[CGenDataTypeT]))
    Map(CLambda(in, CFC(CFC(f, Select(ParamUse(in), 0)), Select(ParamUse(in), 1))), Zip(Tuple(m1, m2)))
  }

  def tt(e: Expr): Type = {
    TypeChecker.check(e).t
  }

  def GMM(x: Expr, alphas: Expr, means: Expr, qs: Expr, ls: Expr, wishartGamma: Expr, wishartM: Expr): Expr = {
    implicit val t = () => DoubleType()
    val n = 100;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val i = ParamDef(IntType(32))
    val k = ParamDef(IntType(32))
    val part1 = VectorSum(
      Build(CLambda(i,
        LogSumExp(Build(CLambda(k,
          sub(add(ArrayAccess(alphas, ParamUse(k)), VectorSum(ArrayAccess(qs, ParamUse(k)))),
            div(SqNorm(Extern.qtimesv(ArrayAccess(qs, ParamUse(k)), ArrayAccess(ls, ParamUse(k)), VectorSub(ArrayAccess(x, ParamUse(i)), ArrayAccess(means, ParamUse(k))))), two))
        ), K, ltt = 1))
      ), n, ltt = 1))
    val part2 =
      div(VectorSum(Map({
        val x = ParamDef(TupleStructType(ArrayType(t(), d), ArrayType(t(), td)))
        val y = ParamDef(t())
        CLambda(x, add(SqNorm(Map(CLambda(y, exp(ParamUse(y))), Select(ParamUse(x), 0))), SqNorm(Select(ParamUse(x), 1))))
      }, Zip(Tuple(qs, ls)))), two)
    add(sub(part1, mul(Constant(n, t()), LogSumExp(alphas))), part2)
  }

  def rows(v: Expr): ArithTypeT = length(v)

  def cols(v: Expr): ArithTypeT = TypeChecker.check(v).t.asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].len

  def AngleAxisToRotationMatrix(angleAxis: Expr): Expr = {
    implicit val t = () => DoubleType()
    val prog = new Program()
    val n = prog.add(sqrt(SqNorm(angleAxis)))
    prog.build(
      If(lt(n, div(Constant(1, DoubleType()), Constant(1000, DoubleType()))),
        {
          val v0 = Concat(Concat(Push(Id(one)), Push(Id(zero))), Push(Id(zero)))
          val v1 = Concat(Concat(Push(Id(zero)), Push(Id(one))), Push(Id(zero)))
          val v2 = Concat(Concat(Push(Id(zero)), Push(Id(zero))), Push(Id(one)))
          Concat(Concat(Push(v0), Push(v1)), Push(v2))
        },
        {
          val prog = new Program()
          val x = prog.add(div(ArrayAccess(angleAxis, zero(() => IntType(32))), n))
          val y = prog.add(div(ArrayAccess(angleAxis, one(() => IntType(32))), n))
          val z = prog.add(div(ArrayAccess(angleAxis, two(() => IntType(32))), n))
          val s = prog.add(sin(n))
          val c = prog.add(cos(n))

          val v0 = Concat(Concat(Push(add(mul(x, x), mul(sub(one, mul(x, x)), c))), Push(sub(mul(mul(x, y), sub(one, c)), mul(z, s)))), Push(add(mul(mul(x, z), sub(one, c)), mul(y, s))))
          val v1 = Concat(Concat(Push(add(mul(mul(x, y), sub(one, c)), mul(z, s))), Push(add(mul(y, y), mul(sub(one, mul(y, y)), c)))), Push(sub(mul(mul(y, z), sub(one, c)), mul(x, s))))
          val v2 = Concat(Concat(Push(sub(mul(mul(x, z), sub(one, c)), mul(y, s))), Push(add(mul(mul(z, y), sub(one, c)), mul(x, s)))), Push(add(mul(z, z), mul(sub(one, mul(z, z)), c))))
          prog.build(Concat(Concat(Push(v0), Push(v1)), Push(v2)))
        }
      )
    )
  }

  def EulerAnglesToRotationMatrix(xzy: Expr): Expr = {
    implicit val t = () => DoubleType()
    val tx = ArrayAccess(xzy, zero(() => IntType(32)))
    val ty = ArrayAccess(xzy, two(() => IntType(32)))
    val tz = ArrayAccess(xzy, one(() => IntType(32)))
    val rx = Concat(Concat(
      Push(Concat(Concat(Push(Id(one)), Push(Id(zero))), Push(Id(zero)))),
      Push(Concat(Concat(Push(Id(zero)), Push(cos(tx))), Push(minus(sin(tx)))))),
      Push(Concat(Concat(Push(Id(zero)), Push(sin(ty))), Push(minus(cos(ty))))))
    val ry = Concat(Concat(
      Push(Concat(Concat(Push(cos(ty)), Push(Id(zero))), Push(sin(ty)))),
      Push(Concat(Concat(Push(Id(zero)), Push(Id(one))), Push(Id(zero))))),
      Push(Concat(Concat(Push(minus(sin(ty))), Push(Id(zero))), Push(cos(ty)))))
    val rz = Concat(Concat(
      Push(Concat(Concat(Push(cos(tz)), Push(minus(sin(tz)))), Push(Id(zero)))),
      Push(Concat(Concat(Push(sin(tz)), Push(cos(tz))), Push(Id(zero))))),
      Push(Concat(Concat(Push(Id(zero)), Push(Id(zero))), Push(Id(one)))))
    MatrixMul(rx, MatrixMul(ry, rz))
  }

  def MakeRelative(possParams: Expr, baseRelative: Expr): Expr = {
    implicit val t = () => DoubleType()
    val R = EulerAnglesToRotationMatrix(possParams)
    val r1 = Concat(Concat(Push(Push(Id(zero))), Push(Push(Id(zero)))), Push(Push(Id(zero))))
    val r2 = TypeChecker.check(Push(Concat(Concat(Concat(Push(Id(zero)), Push(Id(zero))), Push(Id(zero))), Push(Id(one)))))
    val T = TypeChecker.check(Concat(MatrixConcatCol(R, r1), r2))
    MatrixMul(baseRelative, T)
  }

  def MatrixTranspose(_m: Expr, ltt: ArithTypeT = 0): Expr = {
    val m = TypeChecker.check(_m)
    val r = rows(m)
    val c = cols(m)
    Build({
      val i = ParamDef(IntType(32))
      CLambda(i, Build({
        val j = ParamDef(IntType(32))
        ltt.ae match {
          case Cst(1) => CLambda(j, Id(ArrayAccess(ArrayAccess(m, ParamUse(j)), ParamUse(i))))
          case Cst(0) => CLambda(j, ArrayAccess(ArrayAccess(m, ParamUse(j)), ParamUse(i)))
          case _ => throw new Exception("should not reach")
        }
      }, r, ltt
      ))
    }, c, ltt)
  }

  def MatrixConcatCol(_m1: Expr, _m2: Expr): Expr = {
    val m1 = TypeChecker.check(_m1)
    val m2 = TypeChecker.check(_m2)
    val _m1t = MatrixTranspose(m1, ltt = 1)
    val _m2t = MatrixTranspose(m2, ltt = 1)
    val m1t = TypeChecker.check((_m1t))
    val m2t = TypeChecker.check((_m2t))
    MatrixTranspose(Concat(m1t, m2t), ltt = 1)
  }

  def ApplyGlobalTransform(poseParams: Expr, positions: Expr): Expr = {
    implicit val t = () => DoubleType()
    val R = AngleAxisToRotationMatrix(ArrayAccess(poseParams, zero(() => IntType(32))))
    val scale = ArrayAccess(poseParams, one(() => IntType(32)))
    //    val scale = Slide(ArrayAccess(poseParams, one(() => IntType(32))), zero(() => IntType(32)), 3)
    val k = TypeChecker.check(R)
    val d = TypeChecker.check(scale)
    print(k, d)
    val R1 = Map({
      val row = ParamDef(TypeChecker.check(scale).t)
      CLambda(row, VectorMulElemwise(ParamUse(row), scale))
    }, R)
    TypeChecker.check(TypeChecker.check(R1))
    val T = MatrixConcatCol(R1, MatrixTranspose(Push(Id(ArrayAccess(poseParams, two(() => IntType(32)))))))
    //    val T = MatrixConcatCol(R1, MatrixTranspose(Push(Id(Slide(ArrayAccess(poseParams, two(() => IntType(32))), zero(() => IntType(32)), 3)))))
    val ones = Build({
      val idx = ParamDef(IntType(32))
      CLambda(idx, Id(one))
    }, cols(positions), ltt = 1)
    val positionsHomog = Concat(positions, Push(ones), ltt = 1)
    MatrixMul(T, positionsHomog)
  }

  // n -> mat -> mat -> mat
  def GetPosedRelatives(nBones: ArithTypeT, poseParams: Expr, baseRelative: Expr): Expr = {
    val offset = ArithType(2) // should be 3
    Build({
      val i = ParamDef(IntType(32))
      CLambda(i, MakeRelative(ArrayAccess(poseParams, IdxOffsetOp(ParamUse(i), offset, AlgoOp.Add)), ArrayAccess(baseRelative, ParamUse(i))))
    }, nBones, ltt = 1) // should be just nBone
  }

  def GetPosedRelativesFunction(nBones: ArithTypeT): Expr = {
    val poseParams = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 6))
    val baseRelatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    CLambda(poseParams, CLambda(baseRelatives, GetPosedRelatives(nBones, ParamUse(poseParams), ParamUse(baseRelatives))))
  }

  def Matrix3DUpdate(_m: Expr, s: Expr, e: Expr, _nm: Expr): Expr = {
    val i = ParamDef(IntType(32))
    val m = TypeChecker.check(_m)
    val nm = TypeChecker.check(_nm)
    Build(CLambda(i, {
      val isInRange = BinaryOp(
        BinaryOp(ParamUse(i), s, AlgoOp.Gt, isBool = true),
        BinaryOp(ParamUse(i), e, AlgoOp.Lt, isBool = true), AlgoOp.Andand, isBool = true)
      If(isInRange, Id(ArrayAccess(nm, ParamUse(i))), Id(ArrayAccess(m, ParamUse(i))))
    }), length(m))
  }

  // relatives_to_absolutes (relatives: Matrix[]) (parents: Vector)
  def RelativeToAbsolute(relatives: Expr, parents: Expr): Expr = {
    implicit val t = () => DoubleType()
    val init = relatives
    Ifold({
      val idx = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
      CLambda(idx, CLambda(acc,
        If(BinaryOp(ArrayAccess(parents, ParamUse(idx)), minus(Constant(1, DoubleType())), AlgoOp.Eq, isBool = true),
          {
            val newMatrix = Push(ArrayAccess(relatives, ParamUse(idx)))
            Matrix3DUpdate(ParamUse(acc), ParamUse(idx), add(ParamUse(idx), Constant(1, IntType(32))), newMatrix)
          },
          {
            val newMatrix = Push(MatrixMul(ArrayAccess(ParamUse(acc), TypeConv(ArrayAccess(parents, ParamUse(idx)), IntType(32))), ArrayAccess(relatives, ParamUse(idx))))
            Matrix3DUpdate(ParamUse(acc), ParamUse(idx), add(ParamUse(idx), Constant(1, IntType(32))), newMatrix)
          }
        )))
    }, init, length(relatives))
  }

  // let get_skinned_vertex_positions (is_mirrored: Index) (n_bones: Cardinality) (pose_params: Matrix) (base_relatives: Matrix[]) (parents: Vector)
  // (inverse_base_absolutes: Matrix[]) (base_positions: Matrix) (weights: Matrix)
  def GetSkinnedVertexPositions(isMirrored: Expr, nBones: ArithTypeT, poseParams: Expr, baseRelatives: Expr, parents: Expr,
                                inverseBaseAbsolutes: Expr, basePosition: Expr, basePositionHomg: Expr, weights: Expr): Expr = {
    implicit val t = () => DoubleType()
    val relatives = Extern.getPosedRelatives(nBones, poseParams, baseRelatives, gen = false)
    val absolutes = RelativeToAbsolute(relatives, parents)

    val transforms = Map({
      val m = ParamDef(TupleStructType(ArrayType(ArrayType(DoubleType(), 4), 4), ArrayType(ArrayType(DoubleType(), 4), 4)))
      CLambda(m, MatrixMul(Select(ParamUse(m), 0), Select(ParamUse(m), 1)))
    }, Zip(Tuple(absolutes, inverseBaseAbsolutes)))

    val nVerts = length(ArrayAccess(basePosition, zero(() => IntType(32))))
    val initPositions = Build({
      val i = ParamDef(IntType(32))
      CLambda(i,
        Build({
          val j = ParamDef(IntType(32))
          CLambda(j, Id(zero))
        }, 3, ltt = 1)
      )
    }, 3, ltt = 1)
    val positions = Ifold({
      val iTransform = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(ArrayType(t(), 3), 3))
      CLambda(iTransform, CLambda(acc, {
        val currPositions = MatrixMul(Slice(ArrayAccess(transforms, ParamUse(iTransform)), zero(() => IntType(32)), 3), basePositionHomg)
        val w = Build({
          val i = ParamDef(IntType(32))
          CLambda(i, ArrayAccess(weights, ParamUse(iTransform)))
        }, length(basePosition), ltt = 1)
        MatrixAdd(ParamUse(acc), MatrixMulElemwise(currPositions, w))
      }))
    }, initPositions, length(transforms).ae - 1)
    ApplyGlobalTransform(poseParams, positions)
    //    positions
  }

  def newValue(rows: Int, cols: Int) = {
    implicit val t: () => IntType = () => IntType(32)
    val row = ParamDef(IntType(32))
    val col = ParamDef(IntType(32))
    val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
    CLambda(image, CLambda(row, CLambda(col, {
      div(
        Seq(
          ArrayAccess(ArrayAccess(ParamUse(image), sub(ParamUse(row), one)), sub(ParamUse(col), one)),
          ArrayAccess(ArrayAccess(ParamUse(image), sub(ParamUse(row), one)), ParamUse(col)),
          ArrayAccess(ArrayAccess(ParamUse(image), sub(ParamUse(row), one)), add(ParamUse(col), one)),
          ArrayAccess(ArrayAccess(ParamUse(image), ParamUse(row)), sub(ParamUse(col), one)),
          ArrayAccess(ArrayAccess(ParamUse(image), ParamUse(row)), ParamUse(col)),
          ArrayAccess(ArrayAccess(ParamUse(image), ParamUse(row)), add(ParamUse(col), one)),
          ArrayAccess(ArrayAccess(ParamUse(image), add(ParamUse(row), one)), sub(ParamUse(col), one)),
          ArrayAccess(ArrayAccess(ParamUse(image), add(ParamUse(row), one)), ParamUse(col)),
          ArrayAccess(ArrayAccess(ParamUse(image), add(ParamUse(row), one)), add(ParamUse(col), one)),
        ) reduce ((a: Expr, b: Expr) => add(a, b)),
        nine(() => DoubleType())
      )
    }
    )))
  }

  def blur_forward(rows: Int, cols: Int) = {
    implicit val t: () => IntType = () => IntType(32)

    val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
    val row = ParamDef(IntType(32))
    val col = ParamDef(IntType(32))
    CLambda(image,
      Build(CLambda(row, Build(CLambda(col,
        If({
          Seq(gt(row, zero), lt(row, num(rows - 1)), gt(col, zero), lt(col, num(cols - 1))) reduce ((a, b) => andand(a, b))
        }, CFC(newValue(rows, cols), Seq(ParamUse(col), ParamUse(row), ParamUse(image))),
          Id(ArrayAccess(ArrayAccess(ParamUse(image), ParamUse(row)), ParamUse(col)))
        )
      ), cols, ltt = 1)), rows, ltt = 1)
    )
  }

  def blur_backward(rows: Int, cols: Int) = {
    implicit val t: () => IntType = () => IntType(32)

    val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
    val row = ParamDef(IntType(32))
    val col = ParamDef(IntType(32))
    CLambda(image,
      Concat(Concat(Push(Build(CLambda(col, Id(ArrayAccess(ArrayAccess(ParamUse(image), zero), ParamUse(col)))), cols, ltt = 1)),
        Build(CLambda(row,
          Concat(Concat(Push(Id(ArrayAccess(ArrayAccess(ParamUse(image), add(ParamUse(row), one)), zero))),
            Build(CLambda(col,
              CFC(newValue(rows, cols), Seq(add(ParamUse(col), one), add(ParamUse(row), one), ParamUse(image))))
              , cols - 2, ltt = 1)), Push(Id(ArrayAccess(ArrayAccess(ParamUse(image), ParamUse(row)), num(cols - 1)))))),
          rows - 2, ltt = 1)), Push(Build(CLambda(col, Id(ArrayAccess(ArrayAccess(ParamUse(image), num(rows - 1)), ParamUse(col)))), cols, ltt = 1)))
    )
  }

  def fft(N: Int): CLambda = {
    val LEN: Int = ((N / 2) * (math.log(N) / math.log(2))).toInt
    val primal = ParamDef(ArrayType(TupleStructType(COMPLEX, DoubleType(), DoubleType()), N))
//    val p = ParamDef(ArrayType(IntType(32), LEN))
//    val offset = ParamDef(ArrayType(IntType(32), LEN))
//    val offset1 = ParamDef(ArrayType(IntType(32), LEN))
    val FFT = CLambda(primal, {
      val prog = new Program()

      val p = {
        implicit val t = () => IntType(32)
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, add(one, div(ParamUse(idx), Constant(N / 2, IntType(32)))))
        }, Range(zero, one, LEN))
      }

      val offset = {
        implicit val t = () => IntType(32)
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, {
            val prog = new Program()
            val j = prog.add(div(ParamUse(idx), Constant(N / 2, IntType(32))))
            val k = prog.add(mod(ParamUse(idx), Constant(N / 2, IntType(32))))
            val part0 = mul(div(k, pow(two, j)), pow(two, add(j, one)))
            val part1 = mod(k, pow(two, j))
            prog.build(add(part0, part1))
          })
        }, Range(zero, one, LEN))
      }

      val P = prog.add(p)
      val Offset0 = prog.add(offset)

      val offset1 = TypeChecker.check({
        implicit val t = () => IntType(32)
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, {
            add(ArrayAccess(Offset0, ParamUse(idx)), div(leftShift(one, ArrayAccess(P, ParamUse(idx))), two))
          })
        }, Range(zero, one, LEN))
      })

      val Offset1 = prog.add(offset1)

      val unityRoots = Map({
        val pi = ParamDef(IntType(32))
        CLambda(pi, {
          val unityStep = BinaryOp(one(() => IntType(32)), ParamUse(pi), AlgoOp.LeftShift)
          val prog = new Program()
          val theta = prog.add(BinaryOp(num(2 * math.Pi)(() => DoubleType()), TypeConv(unityStep, DoubleType()), AlgoOp.Div))
          prog.build(Tuple(COMPLEX, SingleOp(theta, AlgoOp.Cos), SingleOp(theta, AlgoOp.Sin)))
        })
      }, P)

      val init = {
        implicit val t = () => IntType(32)
        Map(CLambda(ParamDef(IntType(32)), Tuple(COMPLEX, Id(zero(() => DoubleType())), Id(zero(() => DoubleType())))),
          Range(zero, one, LEN))
      }
      val omegas = AccMap({
        val acc = ParamDef(ArrayType(TupleStructType(COMPLEX, DoubleType(), DoubleType()), LEN))
        val idx = ParamDef(IntType(32))
        CLambda(acc, CLambda(idx, {
          implicit val t = () => IntType(32)
          val x = div(ParamUse(idx), num(N / 2))
          val cond = eq(mod(ParamUse(idx), pow(two, x)), zero)
          If(cond, Tuple(COMPLEX, Id(one(() => DoubleType())), Id(zero(() => DoubleType()))), {
            val j = sub(ParamUse(idx), one)
            complexMul(ArrayAccess(ParamUse(acc), j), ArrayAccess(unityRoots, j))
          })
        }))
      }, init, {
        implicit val t = () => IntType(32)
        Range(zero, one, LEN)
      })

      val initDual = Permute({
        implicit val t = () => IntType(32)
        val idx = ParamDef(IntType(32))
        CLambda(idx, Extern.reverseBits(TypeConv(ParamUse(idx), IntType(32, unsigned = 1)), Constant(math.log(N) / math.log(2), DoubleType())))
      },
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, {
            Tuple(COMPLEX, Id(Select(ArrayAccess(ParamUse(primal), ParamUse(idx)), 0)), Id(Select(ArrayAccess(ParamUse(primal), ParamUse(idx)), 1)))
          })
        }, Range(zero(() => IntType(32)), one(() => IntType(32)), N))
      )

      val InitDual = prog.add(initDual)
      val Omegas = prog.add(omegas)

      val j = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(TupleStructType(COMPLEX, DoubleType(), DoubleType()), N))
      val reduceBody = {
        val prog = new Program()

        val us = {
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), ParamUse(idx)))
            prog.build(Id(ArrayAccess(ParamUse(acc), ArrayAccess(Offset0, i))))
          }), Range(zero, one, N / 2))
        }

        val ms = {
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), ParamUse(idx)))
            prog.build(complexMul(
              ArrayAccess(Omegas, i),
              ArrayAccess(ParamUse(acc), ArrayAccess(Offset1, i))))
          }), Range(zero, one, N / 2))
        }

        val Us = prog.add(us)
        val Ms = prog.add(ms)

        val dual0 = {
          val tup = ParamDef(TupleStructType(
            TupleStructType(COMPLEX, DoubleType(), DoubleType()),
            TupleStructType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexAdd(Select(ParamUse(tup), 0), Select(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)))
        }
        //
        val dual1 = {
          val tup = ParamDef(TupleStructType(
            TupleStructType(COMPLEX, DoubleType(), DoubleType()),
            TupleStructType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexSub(Select(ParamUse(tup), 0), Select(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)))
        }

        val dualConcat = Concat(dual0, dual1)

        val dualPermute = TypeChecker.check({
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Permute(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), mod(ParamUse(idx), num(N / 2))))
            prog.build(If(lt(ParamUse(idx), num(N / 2)),
              Id(ArrayAccess(Offset0, i)),
              Id(ArrayAccess(Offset1, i)) // equivalent to concat(slide)
            ))
          }), dualConcat)
        })
        prog.build(dualPermute)
      }
      implicit val t = () => IntType(32)
      val r = Reduce(CLambda(acc, CLambda(j, reduceBody)), InitDual, Range(zero, one, math.floor(math.log(N) / math.log(2)).toInt))
      prog.build(r)
    })
    FFT
  }
}

object TupleStructName {
  val COMPLEX = "complex"
}

object Extern {
  def qtimesv(q: Expr, l: Expr, v: Expr): ExternFunctionCallExpr = {
    val d = 3;
    val td = ((d) * ((d) + (1))) / (2)
    ExternFunctionCall("qtimesv",
      Seq(q, l, v).reverse,
      Seq(ArrayTypeVar(DoubleType(), d), ArrayTypeVar(DoubleType(), td), ArrayTypeVar(DoubleType(), d)).reverse,
      ArrayType(DoubleType(), d))
  }

  def getPosedRelatives(nBones: ArithTypeT, poseParams: Expr, baseRelative: Expr, gen: Boolean): ExternFunctionCallExpr = {
    val poseParamsDef = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 6))
    val baseRelativesDef = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val body = GetPosedRelatives(nBones, poseParams, baseRelative)
    val tree = CLambda(poseParamsDef, CLambda(baseRelativesDef, body))
    if (gen) {
      CCodeGen(DPSPass(tree)).genCode("get_posed_relatives")
    }
    ExternFunctionCall("get_posed_relatives",
      Seq(poseParams, baseRelative).reverse,
      Seq(ArrayTypeVar(ArrayType(DoubleType(), 4), 6), ArrayTypeVar(ArrayType(ArrayType(DoubleType(), 4), 4), 4)).reverse,
      TypeChecker.check(body).t
    )
  }

  def reverseBits(x: Expr, P: Expr): ExternFunctionCallExpr = {
    ExternFunctionCall("reverse_bits",
      Seq(x, P).reverse,
      Seq(IntTypeVar(32, unsigned = 1), IntTypeVar(32)).reverse,
      IntType(32)
    )
  }
}

