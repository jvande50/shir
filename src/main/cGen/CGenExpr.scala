package cGen

import algo._
import cGen.AlgoOp.AlgoOp
import cGen.AllocKind.{AllocKind, toAk, toAt}
import core.util.IRDotGraph
import core.{FunctionCall, Value, _}
import lift.arithmetic.{ArithExpr, Cst, StartFromRange}

import scala.annotation.tailrec
import scala.language.postfixOps

object AllocKind extends Enumeration {
  type AllocKind = Value
  val Heap, Stack, Static = Value

  def toAt(ak: AllocKind): ArithType = ak match {
    case AllocKind.Heap => ArithType(0)
    case AllocKind.Stack => ArithType(1)
    case AllocKind.Static => ArithType(2)
  }

  def toAk(at: ArithTypeT): AllocKind = at match {
    case ArithType(Cst(0)) => AllocKind.Heap
    case ArithType(Cst(1)) => AllocKind.Stack
    case ArithType(Cst(2)) => AllocKind.Static
    case _ => throw new Exception("Not Implemented")
  }
}

final case class CLambda(param: ParamDef, body: Expr, t: FunTypeT) extends LambdaT {
  override def build(newChildren: Seq[ShirIR]): CLambda = {
    val param = newChildren.head.asInstanceOf[ParamDef]
    val body = newChildren(1).asInstanceOf[Expr]
    CLambda(param, body, AlgoFunType(param.t, body.t))
  }
}

trait NonDPSWriteExpr {
  val isWritten: Boolean
}

object CLambda {
  def apply(param: ParamDef, body: Expr): CLambda = CLambda(param, body, AlgoFunType(param.t, body.t))

  @tailrec
  def apply(params: Seq[ParamDef], body: Expr): CLambda = {
    if (params.isEmpty) {
      throw MalformedExprException("Lambda expression must have at least one parameter")
    } else if (params.size == 1) {
      CLambda(params.head, body)
    } else {
      CLambda(params.tail, CLambda(params.head, body))
    }
  }
}

object CGenLet {
  def apply(param: ParamDef, arg: Expr): Expr => CGenExpr = {
    body: Expr => CFC(CLambda(param, body), arg, isLet = true)
  }

  def apply(param: ParamDef, body: Expr, arg: Expr): CGenExpr = {
    CFC(CLambda(param, body), arg, isLet = true)
  }

  def apply(params: Seq[ParamDef], body: Expr, args: Seq[Expr]): CGenExpr = {
    assert(params.nonEmpty)
    assert(params.length == args.length)
    // ugly nesting: FunctionCall(Lambda(params, body), args, applyBetaReduction = false)
    // clean nesting:
    params.length match {
      case 1 => apply(params.head, body, args.head)
      case _ => apply(params.head, apply(params.tail, body, args.tail), args.head)
    }
  }

  def unapply(expr: CFCExpr): Option[(ParamDef, Expr, Expr, Type)] = expr match {
    case CFC(f: LambdaT, arg, t) if expr.isLet => Some(f.param, f.body, arg, t)
    case _ => None
  }
}

abstract class CGenExpr extends BuiltinExpr with CGenIR {
  def build(newInnerIR: Expr, newTypes: Seq[Type]): CGenExpr = cgenBuild(newInnerIR, newTypes)

  def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): CGenExpr

  def isEager: Boolean = false // false by default

  // aka semicolon operator
  def >>(other: CGenExpr): CGenExpr = {
    CGenLet(ParamDef(VoidType()), other, this)
  }
}

abstract class EagerCGenExpr extends CGenExpr {
  override def isEager: Boolean = true

  val isDPS: Boolean = false
}

case class ArrayAccessExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ArrayAccessExpr = ArrayAccessExpr(newInnerIR)
}

object ArrayAccess {
  private[cGen] def apply(input: Expr, idx: Expr): ArrayAccessExpr = ArrayAccessExpr({
    val idxtv = IntTypeVar(32)
    val outtv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val intv = ArrayTypeVar(outtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(intv, idxtv), AlgoFunType(Seq(intv, idxtv), outtv)))
      , Seq(input.t, idx.t))
      , Seq(input, idx))
  })

  def unapply(expr: ArrayAccessExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class AddressOfExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AddressOfExpr = AddressOfExpr(newInnerIR)
}

object AddressOf {
  private[cGen] def apply(input: Expr): AddressOfExpr = AddressOfExpr({
    val intv = CGenDataTypeVar()
    val outtv = AddressTypeVar(intv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, CGenFunType(intv, outtv))), input.t), input)
  })

  def unapply(expr: AddressOfExpr): Some[(Expr, Type)] = Some((expr.args.head, expr.t))
}

case class DestTupleExpr(innerIR: Expr) extends CGenExpr {
  override def types = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DestTupleExpr = DestTupleExpr(newInnerIR)
}

// n |-> (idx -> Addr[B]) -> Addr[[B]n]
// Addr[T] -> Addr[U] -> Addr[(T, U)]
object DestTuple {
  def apply(addrt: Expr, addru: Expr): DestTupleExpr = DestTupleExpr({
    val ttv = CGenDataTypeVar()
    val utv = CGenDataTypeVar()
    val addrttv = AddressTypeVar(ttv)
    val addrutv = AddressTypeVar(utv)
    val outt = AddressType(TupleStructType(addrttv, addrutv))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ttv, utv),
      AlgoFunType(Seq(ttv, utv), outt))), Seq(addrt.t, addru.t)), Seq(addrt, addru))
  })

  def unapply(expr: DestTupleExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class DestSelectExpr(innerIR: Expr, selection: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(selection)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DestSelectExpr = DestSelectExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])
}

object DestSelect {
  private[cGen] def apply(input: Expr, selection: ArithTypeT): DestSelectExpr = DestSelectExpr({
    val ttv = CGenDataTypeVar()
    val utv = CGenDataTypeVar()
    val inttv = AddressTypeVar(TupleStructTypeVar(ttv, utv))
    val outt = AddressType(inttv.ptrType.children(selection.ae.evalInt))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(inttv), AlgoFunType(Seq(inttv), outt)))
      , Seq(input.t))
      , Seq(input))
  }, selection)

  def unapply(expr: DestSelectExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.selection, expr.t))
}

case class DestBuildExpr(innerIR: Expr, n: Type) extends CGenExpr {
  override def types = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DestBuildExpr = DestBuildExpr(newInnerIR, newTypes.head)
}

// n, |-> (idx -> Addr[B]) -> Addr[[B]n]
object DestBuild {
  def apply(idxf: Expr, n: ArithTypeT): DestBuildExpr = DestBuildExpr({
    val btv = CGenDataTypeVar() //B
    val addrtv = AddressTypeVar(btv)
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, addrtv)
    val outtv = AddressType(ArrayType(btv, n))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      AlgoFunType(Seq(idxftv), outtv))), Seq(idxf.t)), Seq(idxf))
  }, n)

  def apply(split: SplitExpr, dest: Expr): DestBuildExpr = {
    val in = TypeChecker.check(split.args.head)
    val cs = split.types.head.asInstanceOf[ArithTypeT]
    val n = in.t.asInstanceOf[ArrayTypeT].len.asInstanceOf[ArithTypeT]
    val idx = ParamDef(IntType(32))
    DestBuild(CLambda(idx, DestArrayAccess(DestArrayAccess(dest, IdxOffsetOp(ParamUse(idx), cs, AlgoOp.Div)), IdxOffsetOp(ParamUse(idx), cs, AlgoOp.Mod))), n)
  }

  def apply(concat: ConcatExpr, dest: Expr, which: Int): DestBuildExpr = {
    val in1 = TypeChecker.check(concat.args.head)
    try {
      TypeChecker.check(concat.args.tail.head)
    } catch {
      case _: Throwable => IRDotGraph(concat.args.tail.head).show()
    }
    val in2 = TypeChecker.check(concat.args.tail.head)
    val in1len = in1.t.asInstanceOf[ArrayType].len
    val in2len = in2.t.asInstanceOf[ArrayType].len
    val idx = ParamDef(IntType(32))
    if (which == 0) DestBuild(CLambda(idx, DestArrayAccess(dest, ParamUse(idx))), in1len)
    else DestBuild(CLambda(idx, DestArrayAccess(dest, IdxOffsetOp(ParamUse(idx), in1len, AlgoOp.Add))), in2len)
  }

  def unapply(expr: DestBuildExpr): Some[(Expr, ArithTypeT, Type)] = Some((expr.args.head, expr.n.asInstanceOf[ArithTypeT], expr.t))
}

case class BuildExpr(innerIR: Expr, n: Type, ltt: Type, override val isDPS: Boolean = false) extends EagerCGenExpr {
  override def types = Seq(n, ltt)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): BuildExpr = {
    //    val idxf = FunctionCall.getArgument(newInnerIR, 0).get
    //    // when the type is changed in any branch, the return type of if need to be changed
    //    if (!idxf.t.isInstanceOf[AnyTypeVar] && !newInnerIR.t.isInstanceOf[AnyTypeVar]) {
    //      DBuild(idxf, newTypes.head.asInstanceOf[ArithTypeT], newTypes.tail.head.asInstanceOf[ArithTypeT])
    //    } else {
    //      BuildExpr(newInnerIR, newTypes.head, newTypes.tail.head, isDPS).updateId(this.id).asInstanceOf[BuildExpr]
    //    }
    BuildExpr(newInnerIR, newTypes.head, newTypes.tail.head, isDPS).asInstanceOf[BuildExpr]
  }

  override def isEager = {
    ltt.asInstanceOf[ArithTypeT].ae.evalInt == 1
  }
}

object DBuild {
  def apply(idxf: Expr, n: ArithTypeT, ltt: ArithTypeT): BuildExpr = BuildExpr({
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, VoidTypeVar())
    val outtv = VoidType()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      AlgoFunType(Seq(idxftv), outtv))), Seq(idxf.t)), Seq(idxf))
  }, n, ltt, true)

  def unapply(expr: BuildExpr): Option[(Expr, ArithTypeT, ArithTypeT, Type)] = {
    if (expr.isDPS) Some((expr.args.head, expr.n.asInstanceOf[ArithTypeT], expr.ltt.asInstanceOf[ArithTypeT], expr.t))
    else None
  }
}

// n |-> (idx -> B) -> [B]n
object Build {
  def apply(idxf: Expr, n: ArithTypeT, ltt: ArithTypeT = 0): BuildExpr = BuildExpr({
    val btv = CGenDataTypeVar() //B
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, btv)
    val outtv = ArrayType(btv, n)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      AlgoFunType(Seq(idxftv), outtv))), Seq(idxf.t)), Seq(idxf))
  }, n, ltt)

  def apply(idxf: Expr, n: ArithTypeT, ltt: ArithTypeT, isDPS: Boolean): BuildExpr = BuildExpr({
    val btv = VoidTypeVar()
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, btv)
    val outtv = VoidType()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      AlgoFunType(Seq(idxftv), outtv))), Seq(idxf.t)), Seq(idxf))
  }, n, ltt)

  def unapply(expr: BuildExpr): Option[(Expr, ArithTypeT, ArithTypeT, Type)] = {
    if (expr.isDPS)
      None
    else
      Some((expr.args.head, expr.n.asInstanceOf[ArithTypeT], expr.ltt.asInstanceOf[ArithTypeT], expr.t))
  }
}

case class SelectExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SelectExpr = SelectExpr(newInnerIR, newTypes)
}

object Select {
  def apply(input: Expr, selection: ArithTypeT, len: Int = 3): SelectExpr = SelectExpr({
    val inttv = TupleStructTypeVar(Seq.fill(len)(CGenDataTypeVar()): _*)
    val outt = inttv.children(selection.ae.evalInt)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, AlgoFunType(inttv, outt))), input.t), input)
  }, Seq(selection))

  def unapply(expr: SelectExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class IdExpr(innerIR: Expr, override val isWritten: Boolean) extends CGenExpr with NonDPSWriteExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdExpr = IdExpr(newInnerIR, isWritten)
}

case class DIdExpr(innerIR: Expr, override val isWritten: Boolean) extends CGenExpr with NonDPSWriteExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DIdExpr = DIdExpr(newInnerIR, isWritten)
}

object DId {
  def apply(input: Expr, dest: Expr): DIdExpr = DIdExpr({
    val inttv = CGenDataTypeVar()
    val dtv = AddressTypeVar(inttv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(inttv, dtv), AlgoFunType(Seq(inttv, dtv), VoidType()))), Seq(input.t, dest.t)), Seq(input, dest))
  }, true)

  def unapply(expr: DIdExpr): Option[(Expr, Expr, Type)] = {
    Some((expr.args.head, expr.args(1), expr.t))
  }
}

object Assign {
  def apply(input: Expr, dest: Expr): DIdExpr = DId(input, dest)

  def unapply(expr: DIdExpr): Option[(Expr, Expr, Type)] = {
    Some((expr.args.head, expr.args(1), expr.t))
  }
}

object Id {
  def apply(input: Expr, isWritten: Boolean = false): IdExpr = IdExpr({
    val dtv = CGenDataTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(dtv, AlgoFunType(dtv, dtv))), input.t), input)
  }, isWritten)

  def unapply(expr: IdExpr): Option[(Expr, Type)] = {
    Some((expr.args.head, expr.t))
  }
}

case class SingleOpExpr(innerIR: Expr, op: AlgoOp, override val isWritten: Boolean = false) extends CGenExpr with NonDPSWriteExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SingleOpExpr = SingleOpExpr(newInnerIR, op, isWritten).asInstanceOf[SingleOpExpr]
}

object SingleOp {
  def apply(in: Expr, op: AlgoOp, isWritten: Boolean = false): SingleOpExpr = {
    SingleOpExpr({
      val itv = CScalarTypeVar()
      val outit = itv
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, AlgoFunType(itv, outit))), in.t), in)
    }, op, isWritten = isWritten)
  }

  def unapply(expr: SingleOpExpr): Option[(Expr, AlgoOp, Type)] = {
    Some((expr.args.head, expr.op, expr.t))
  }
}

private case class BinaryOpNodeKind(op: AlgoOp, isBool: Boolean, isWritten: Boolean) extends TreeNodeKind

case class BinaryOpExpr(innerIR: Expr, op: AlgoOp, val isBool: Boolean, override val isWritten: Boolean = false) extends CGenExpr with NonDPSWriteExpr {
  override def nodeKind: TreeNodeKind = BinaryOpNodeKind(op, isBool, isWritten)

  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): BinaryOpExpr = BinaryOpExpr(newInnerIR, op, isBool, isWritten).asInstanceOf[BinaryOpExpr]

  override def tryFormat(formatter: Formatter): Option[String] = {
    def opToText(algoOp: AlgoOp): String = algoOp match {
      case AlgoOp.Add => "+"
      case AlgoOp.Sub => "-"
      case AlgoOp.Mul => "*"
      case AlgoOp.Div => "/"
      case AlgoOp.Mod => "%"
      case AlgoOp.Lt => "<"
    }

    formatter match {
      case formatter: TextFormatter =>
        Some(
          formatter.makeAtomic(
            s"${formatter.formatAtomic(args.head)} ${opToText(op)} ${formatter.formatAtomic(args.tail.head)}"))

      case _ => None
    }
  }
}

object BinaryOp {
  def apply(in1: Expr, in2: Expr, op: AlgoOp, isBool: Boolean = false, isWritten: Boolean = false): BinaryOpExpr = {
    BinaryOpExpr({
      val itv1 = CScalarTypeVar()
      val itv2 = itv1
      val outit = if (isBool) BoolType() else itv1
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(itv1, itv2), AlgoFunType(Seq(itv1, itv2), outit))), Seq(in1.t, in2.t)), Seq(in1, in2))
    }, op, isBool = isBool, isWritten = isWritten)
  }

  def unapply(expr: BinaryOpExpr): Option[(Expr, Expr, AlgoOp, Boolean, Type)] = {
    Some((expr.args.head, expr.args.tail.head, expr.op, expr.isBool, expr.t))
  }
}

case class CFCExpr(innerIR: Expr, isLet: Boolean, isExtern: Boolean) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): CFCExpr = CFCExpr(newInnerIR, isLet, isExtern)

  override def toString: String = {
    val paramString = innerIR.asInstanceOf[FunctionCall].f match {
      case lam: LambdaT => lam.param.toShortString + ", "
      case _ => "_, "
    }
    this.getClass.getSimpleName + "(" + paramString + "_, " + args.mkString(", ").stripSuffix(", ") + ")"
  }
}

object CFC {
  def apply(f: Expr, arg: Expr): CFCExpr = CFC(f, arg, false)

  def apply(f: Expr, arg: Expr, isLet: Boolean): CFCExpr = {
    CFCExpr(FunctionCall(f, arg), isLet, false)
  }

  def apply(f: Expr, args: Seq[Expr]): CFCExpr = CFC(f, args, false)

  def apply(f: Expr, args: Seq[Expr], isLet: Boolean): CFCExpr = {
    if (args.isEmpty) {
      throw MalformedExprException("Function Call expression must have at least one argument")
    } else if (args.size == 1) {
      CFC(f, args.head, isLet)
    } else {
      CFC(CFC(f, args.tail, isLet), args.head, isLet)
    }
  }

  def unapply(expr: CFCExpr): Some[(Expr, Expr, Type)] = Some(
    (expr.innerIR.asInstanceOf[FunctionCall].f,
      expr.innerIR.asInstanceOf[FunctionCall].arg,
      expr.t,
    ))
}

case class DExternFunctionCallExpr(innerIR: Expr, name: String, ins: Seq[Expr], intvs: Seq[TypeVarT]) extends EagerCGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DExternFunctionCallExpr = DExternFunctionCallExpr(newInnerIR, name, ins, intvs)
}

object DExternFunctionCall { // the input intvs/outt/ins/ should be in-order
  def apply(name: String, ins: Seq[Expr], intvs: Seq[TypeVarT]): DExternFunctionCallExpr = {
    val outt = VoidType()
    DExternFunctionCallExpr({
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intvs, AlgoFunType(intvs, outt))), ins.map(_.t)), ins)
    }, name, ins, intvs)
  }

  def unapply(expr: DExternFunctionCallExpr): Some[(String, Seq[Expr], Seq[TypeVarT], Type)] =
    Some(expr.name, expr.ins.indices.map(expr.args(_)), expr.intvs, VoidType())
}

private case class ExternFunctionCallNodeKind(name: String, argLength: Int) extends TreeNodeKind

case class ExternFunctionCallExpr(innerIR: Expr, name: String, ins: Seq[Expr], intvs: Seq[TypeVarT], outt: Type) extends CGenExpr {
  override def nodeKind: TreeNodeKind = ExternFunctionCallNodeKind(name, ins.length)

  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ExternFunctionCallExpr = ExternFunctionCallExpr(newInnerIR, name, ins, intvs, outt)
}

object ExternFunctionCall { // the input intvs/outt/ins/ should be reversed
  def apply(name: String, ins: Seq[Expr], intvs: Seq[TypeVarT], outt: Type, isWritten: Boolean = false): ExternFunctionCallExpr = {
    ExternFunctionCallExpr({
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intvs, AlgoFunType(intvs, outt))), ins.map(_.t)), ins)
    }, name, ins, intvs, outt)
  }

  def unapply(expr: ExternFunctionCallExpr): Some[(String, Seq[Expr], Seq[TypeVarT], Type)] = {
    Some(expr.name, expr.ins.indices.map(expr.args(_)), expr.intvs, expr.outt)
  }
}

object GetArgument {
  @tailrec
  def apply(functionCall: Expr, argPos: Int): Option[Expr] = functionCall match {
    case fc: FunctionCall =>
      if (argPos == 0) {
        Some(fc.arg)
      } else {
        apply(fc.f, argPos - 1)
      }
    case _ => None
  }
}

case class DIfExpr(innerIR: Expr) extends EagerCGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DIfExpr = {
    val computeCond = GetArgument(newInnerIR, 0).get
    val cond = GetArgument(newInnerIR, 1).get
    val b0 = GetArgument(newInnerIR, 2).get
    val b1 = GetArgument(newInnerIR, 3).get
    // when the type is changed in any branch, the return type of if need to be changed
    if (!b0.t.isInstanceOf[AnyTypeVar] && !newInnerIR.t.isInstanceOf[AnyTypeVar]) {
      DIf(computeCond, cond, b0, b1)
    } else {
      DIfExpr(newInnerIR)
    }
  }
}

/* DIf will be compiled to in DPS:
condStmt; // computes the value of cond and stored to it
if(cond) branch0
else branch1
 */
object DIf {
  def apply(computeCond: Expr, cond: Expr, branch0: Expr, branch1: Expr): DIfExpr = DIfExpr({
    val computeCondtv = CGenDataTypeVar()
    val condtv = BoolTypeVar()
    val btv = CGenDataTypeVar() // U

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(computeCondtv, condtv, btv, btv),
      AlgoFunType(Seq(computeCondtv, condtv, btv, btv), VoidType()))), Seq(computeCond.t, cond.t, branch0.t, branch1.t)), Seq(computeCond, cond, branch0, branch1))
  })

  def unapply(expr: DIfExpr): Some[(Expr, Expr, Expr, Expr, Type)] =
    Some(expr.args.head, expr.args.tail.head, expr.args(2), expr.args(3), expr.t)
}

case class IfExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IfExpr = {
    val cond = GetArgument(newInnerIR, 0).get
    val b0 = GetArgument(newInnerIR, 1).get
    val b1 = GetArgument(newInnerIR, 2).get
    // when the type is changed in any branch, the return type of if need to be changed
    if (!b0.t.isInstanceOf[AnyTypeVar] && !newInnerIR.t.isInstanceOf[AnyTypeVar] && b0.t != newInnerIR.t)
      If(cond, b0, b1)
    else IfExpr(newInnerIR)
  }
}

// Bool -> U -> U -> U
object If {
  def apply(cond: Expr, branch0: Expr, branch1: Expr): IfExpr = IfExpr({
    val condtv = BoolTypeVar()
    val btv = CGenDataTypeVar() // U

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(condtv, btv, btv),
      AlgoFunType(Seq(condtv, btv, btv), btv))), Seq(cond.t, branch0.t, branch1.t)), Seq(cond, branch0, branch1))
  })

  def unapply(expr: IfExpr): Some[(Expr, Expr, Expr, Type)] =
    Some(expr.args.head, expr.args.tail.head, expr.args(2), expr.t)
}

case class IfoldExpr(innerIR: Expr, n: ArithTypeT, override val isDPS: Boolean) extends EagerCGenExpr {
  override def types: Seq[Type] = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IfoldExpr = IfoldExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT], isDPS)
}

object DIfold {
  def apply(idxf: Expr, init: Expr, dest: Expr, n: ArithTypeT): IfoldExpr = IfoldExpr({
    // (U -> T -> U) -> U -> [T]n -> U
    // n |-> (idx -> Void) -> U -> Addr[U] -> Void
    val utv = CGenDataTypeVar() // U
    val dtv = AddressTypeVar(utv)
    val idxtv = IntTypeVar(32)
    val ftv = AlgoFunTypeVar(Seq(utv, idxtv), VoidTypeVar())

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, utv, dtv),
      AlgoFunType(Seq(ftv, utv, dtv), VoidType()))), Seq(idxf.t, init.t, dest.t)), Seq(idxf, init, dest))
  }, n, true)

  def unapply(expr: IfoldExpr): Option[(Expr, Expr, Expr, ArithTypeT, Type)] = {
    if (expr.isDPS) Some((expr.args.head, expr.args.tail.head, expr.args(2), expr.n, expr.t))
    else None
  }
}

object Ifold {
  def apply(idxf: Expr, init: Expr, n: ArithTypeT): IfoldExpr = IfoldExpr({
    // (U -> T -> U) -> U -> [T]n -> U
    // n |-> (idx -> U -> U) -> U -> U
    val utv = CGenDataTypeVar() // U
    val idxtv = IntTypeVar(32)
    val ftv = AlgoFunTypeVar(Seq(utv, idxtv), utv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, utv),
      AlgoFunType(Seq(ftv, utv), utv))), Seq(idxf.t, init.t)), Seq(idxf, init))
  }, n, false)

  def unapply(expr: IfoldExpr): Option[(Expr, Expr, ArithTypeT, Type)] = {
    if (expr.isDPS) None
    else Some((expr.args.head, expr.args.tail.head, expr.n, expr.t))
  }
}

// Split:   T |-> n(nat) |-> m(nat) -> [T]n-> [[T]m]n/m
// RVSplit: T |-> n(nat) |-> m(nat) -> Addr[[[T]m]n/m] -> Addr[[T]n]
case class ValueAtExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ValueAtExpr = ValueAtExpr(newInnerIR)
}

object ValueAt {
  private[cGen] def apply(input: Expr): ValueAtExpr = ValueAtExpr({
    //    if (input.isInstanceOf[ParamDef])
    //      throw new Exception("Should not be a ParamDef")
    val outtv = CGenDataTypeVar()
    val intv = AddressTypeVar(outtv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, AlgoFunType(intv, outtv))), input.t), input)
  })

  def unapply(expr: ValueAtExpr): Some[(Expr, Type)] = Some((expr.args.head, expr.t))
}

case class DestArrayAccessExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DestArrayAccessExpr = DestArrayAccessExpr(newInnerIR)
}

object DestArrayAccess {
  private[cGen] def apply(input: Expr, idx: Expr): DestArrayAccessExpr = DestArrayAccessExpr({
    val idxtv = IntTypeVar(32)
    val datatv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val intv = AddressTypeVar(ArrayTypeVar(datatv, lentv))
    val outtv = AddressType(datatv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(intv, idxtv), AlgoFunType(Seq(intv, idxtv), outtv)))
      , Seq(input.t, idx.t))
      , Seq(input, idx))
  })

  def unapply(expr: DestArrayAccessExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class IdxOffsetOpExpr(innerIR: Expr, n: ArithTypeT, op: AlgoOp) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxOffsetOpExpr = IdxOffsetOpExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT], op)

  override def types: Seq[Type] = Seq(n)
}

// n |-> idx -> idx
object IdxOffsetOp {
  def apply(idx: Expr, n: ArithTypeT, op: AlgoOp): IdxOffsetOpExpr = IdxOffsetOpExpr({
    val idxtv = IntTypeVar(32)
    val outt = IntType(32)
    FunctionCall(
      TypeFunctionCall(
        BuiltinTypeFunction(TypeFunType(idxtv, AlgoFunType(idxtv, outt))),
        idx.t),
      idx)
  }, n, op)

  def unapply(expr: IdxOffsetOpExpr): Some[(Expr, ArithTypeT, AlgoOp, Type)] = Some(
    expr.args.head,
    expr.types.head.asInstanceOf[ArithTypeT],
    expr.op,
    expr.t
  )
}

case class IdxBiOpExpr(innerIR: Expr, op: AlgoOp) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxBiOpExpr = IdxBiOpExpr(newInnerIR, op)

  override def types: Seq[Type] = Seq()
}

// n |-> idx -> idx
object IdxBiOp {
  def apply(idx0: Expr, idx1: Expr, op: AlgoOp): IdxBiOpExpr = IdxBiOpExpr({
    val idx0tv = IntTypeVar(32)
    val idx1tv = IntTypeVar(32)
    val outt = IntType(32)
    FunctionCall(
      TypeFunctionCall(
        BuiltinTypeFunction(TypeFunType(Seq(idx0tv, idx1tv), AlgoFunType(Seq(idx0tv, idx1tv), outt))),
        Seq(idx0.t, idx1.t)),
      Seq(idx0, idx1))
  }, op)

  def unapply(expr: IdxBiOpExpr): Some[(Expr, Expr, AlgoOp, Type)] = Some(
    expr.args.head,
    expr.args.tail.head,
    expr.op,
    expr.t
  )
}

case class PushExpr(innerIR: Expr) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): PushExpr = PushExpr(newInnerIR)

  override def types: Seq[Type] = Seq()
}

object Push {
  // [A] -> [A]1
  def apply(in: Expr): PushExpr = PushExpr({
    val atv = CGenDataTypeVar()
    val intv = atv
    val outtv = ArrayType(atv, 1)
    FunctionCall(
      TypeFunctionCall(
        BuiltinTypeFunction(TypeFunType(intv, AlgoFunType(intv, outtv))),
        in.t),
      in)
  })

  def unapply(expr: PushExpr): Some[(Expr, Type)] = Some(expr.args.head, expr.t)
}

case class ConcatExpr(innerIR: Expr, ltt: ArithTypeT) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ConcatExpr = ConcatExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def types: Seq[Type] = Seq(ltt)
}

object Concat {
  // [A]n -> [A]m -> [A]n+m
  // ltt should be either 0 or 1, 0 means that it should be backward and 1 means that it should be forward
  def apply(_in1: Expr, _in2: Expr, ltt: ArithTypeT = 0): ConcatExpr = ConcatExpr({
    val in1 = TypeChecker.check(_in1) // to prevent type checker error
    val in2 = TypeChecker.check(_in2)
    val atv = CGenDataTypeVar()
    val ntv = ArithTypeVar()
    val mtv = ArithTypeVar()
    val in1tv = ArrayTypeVar(atv, ntv)
    val in2tv = ArrayTypeVar(atv, mtv)
    val outtv = ArrayType(atv, ntv + mtv)
    FunctionCall(
      TypeFunctionCall(
        BuiltinTypeFunction(TypeFunType(Seq(in1tv, in2tv), AlgoFunType(Seq(in1tv, in2tv), outtv))),
        Seq(in1.t, in2.t)),
      Seq(in1, in2))
  }, ltt)

  def unapply(expr: ConcatExpr): Some[(Expr, Expr, ArithTypeT, Type)] = Some(expr.args.head, expr.args.tail.head, expr.ltt, expr.t)
}

case class ToArrayExpr(innerIR: Expr) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ToArrayExpr = ToArrayExpr(newInnerIR)

  override def types: Seq[Type] = Seq()
}

// A -> [A]1
object ToArray {
  def apply(in: Expr): BuildExpr = {
    Build(CLambda(ParamDef(IntType(32)), Id(in)), ArithType(1), ArithType(1))
  } // use id to raise an alloc

  def unapply(expr: ToArrayExpr): Some[(Expr, Type)] = Some(expr.args.head, expr.t)
}

case class SlideExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SlideExpr = SlideExpr(newInnerIR)
}

// m |-> [A]n -> s -> [A]m
object Slice {
  def apply(arr: Expr, start: Expr, m: ArithTypeT): SlideExpr = SlideExpr({
    val starttv = IntTypeVar(32)
    val atv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val arrtv = ArrayTypeVar(atv, lentv)
    val outtv = ArrayType(atv, m)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(arrtv, starttv), AlgoFunType(Seq(arrtv, starttv), outtv)))
      , Seq(arr.t, start.t))
      , Seq(arr, start))
  })

  def unapply(expr: SlideExpr): Some[(Expr, Expr, Type)] = Some(expr.args.head, expr.args(1), expr.t)
}

case class MapExpr(innerIR: Expr, ltt: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): MapExpr = MapExpr(newInnerIR, ltt)
}

object Map extends BuiltinExprFun {
  override def args: Int = 2

  // hack: if ltt == 99, then the lazyness of this map is judged by build
  def apply(function: Expr, input: Expr, ltt: ArithTypeT = 99): MapExpr = MapExpr({
    val indtv = CGenDataTypeVar()
    val outdtv = CGenDataTypeVar()
    val ftv = FunTypeVar(indtv, outdtv)
    val lentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(indtv, lentv)
    val outseqt = ArrayType(outdtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, inseqtv), AlgoFunType(Seq(ftv, inseqtv), outseqt))), Seq(function.t, input.t)), Seq(function, input))
  }, ltt)

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MapExpr = apply(inputs.head, inputs(1))

  def unapply(expr: MapExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class AddExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AddExpr = AddExpr(newInnerIR)
}

object Add extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): AddExpr = AddExpr({
    val itv1 = CScalarTypeVar()
    val itv2 = itv1
    val ttv = TupleStructTypeVar(itv1, itv2)
    val outit = itv1
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outit))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddExpr = apply(inputs.head)

  def unapply(expr: AddExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class SubExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SubExpr = SubExpr(newInnerIR)
}

object Sub extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): SubExpr = SubExpr({
    val i1tv = CScalarTypeVar()
    val i2tv = i1tv
    val ttv = TupleStructTypeVar(i1tv, i2tv)
    val outit = i1tv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outit))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SubExpr = apply(inputs.head)

  def unapply(expr: SubExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class MulExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): MulExpr = MulExpr(newInnerIR)
}

object Mul extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): MulExpr = MulExpr({
    val i1tv = CScalarTypeVar()
    val i2tv = i1tv
    val ttv = TupleStructTypeVar(i1tv, i2tv)
    val outit = i1tv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, AlgoFunType(ttv, outit))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MulExpr = apply(inputs.head)

  def unapply(expr: MulExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

private case class ConstantNodeKind(value: Double) extends TreeNodeKind

case class ConstantExpr(innerIR: Expr, types: Seq[Type], value: Double) extends CGenExpr {
  override def nodeKind: TreeNodeKind = ConstantNodeKind(value)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ConstantExpr =
    Constant(value, newTypes(0))

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case _: TextFormatter => Some(value.toString)
      case _ => None
    }
  }
}

object Constant {
  def apply(value: Double, valueType: Type): ConstantExpr = ConstantExpr(Value(valueType), Seq(valueType), value)

  def unapply(expr: ConstantExpr): Some[(Double, Type)] = Some(expr.value, expr.t)
}

case class ReduceExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ReduceExpr = ReduceExpr(newInnerIR)
}

object Reduce extends BuiltinExprFun {
  override def args: Int = 3

  // f: acc -> cur -> acc
  def apply(function: Expr, initialValue: Expr, input: Expr): ReduceExpr = ReduceExpr({
    val initialValuetc = TypeChecker.checkIfUnknown(initialValue, initialValue.t)
    initialValuetc.t match {
      case _: IntTypeT =>
        val dtv = IntTypeVar()
        val initialValueTv = IntTypeVar()
        val lentv = ArithTypeVar(StartFromRange(1))
        val inseqtv = ArrayTypeVar(dtv, lentv)
        val outt = IntType(dtv.width.ae)
        // TODO "... + Log(2 ..." if function is ADD; "... * Log(2 ..." if function is MUL
        val ftv = FunTypeVar(Seq(outt, dtv), IntTypeVar())
        FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
          TypeFunType(
            Seq(ftv, initialValueTv, inseqtv),
            AlgoFunType(
              Seq(ftv, initialValueTv, inseqtv),
              outt
            )
          )),
          Seq(function.t, initialValue.t, input.t)),
          Seq(function, initialValue, input)
        )
      case _ =>
        val dtv = CGenDataTypeVar()
        val acctv = CGenDataTypeVar()
        val ftv = FunTypeVar(acctv, FunTypeVar(dtv, acctv))
        val inseqtv = ArrayTypeVar(dtv)
        FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
          TypeFunType(
            Seq(ftv, acctv, inseqtv),
            AlgoFunType(
              Seq(ftv, acctv, inseqtv),
              acctv
            )
          )),
          Seq(function.t, initialValue.t, input.t)),
          Seq(function, initialValue, input)
        )
    }
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ReduceExpr = apply(inputs.head, inputs(1), inputs(2))

  def unapply(expr: ReduceExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class ZipExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ZipExpr = ZipExpr(newInnerIR)
}

object Zip extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): ZipExpr = ZipExpr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv1 = ArrayTypeVar(dtv1, lentv)
    val inseqtv2 = ArrayTypeVar(dtv2, lentv)
    val inttv = TupleStructTypeVar(inseqtv1, inseqtv2)
    val outseqt = ArrayType(TupleStructTypeVar(dtv1, dtv2), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, AlgoFunType(inttv, outseqt))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipExpr = apply(inputs.head)

  def unapply(expr: ZipExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class Zip3Expr(innerIR: Expr) extends CGenExpr {
  override def types = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): Zip3Expr = Zip3Expr(newInnerIR)
}

object Zip3 {
  def apply(input: Expr): Zip3Expr = Zip3Expr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val dtv3 = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv1 = ArrayTypeVar(dtv1, lentv)
    val inseqtv2 = ArrayTypeVar(dtv2, lentv)
    val inseqtv3 = ArrayTypeVar(dtv3, lentv)
    val inttv = TupleStructTypeVar(inseqtv1, inseqtv2, inseqtv3)
    val outseqt = ArrayType(TupleStructType(dtv1, dtv2, dtv3), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, AlgoFunType(inttv, outseqt))), input.t), input)
  })

  def unapply(expr: Zip3Expr): Some[(Expr, Type)] = Some((expr.args.head, expr.t))

}


case class TupleExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): TupleExpr = TupleExpr(newInnerIR)
}

// V |-> T |-> U |-> T_V -> U_V -> (T_V, U_V)_V
object Tuple {
  def apply(input1: Expr, input2: Expr): TupleExpr = TupleExpr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val outtt = TupleStructType(dtv1, dtv2)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv1, dtv2), AlgoFunType(Seq(dtv1, dtv2), outtt))), Seq(input1.t, input2.t)), Seq(input1, input2))
  })

  def apply(name: String, input1: Expr, input2: Expr): TupleExpr = TupleExpr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val outtt = TupleStructType(name, dtv1, dtv2)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv1, dtv2), AlgoFunType(Seq(dtv1, dtv2), outtt))), Seq(input1.t, input2.t)), Seq(input1, input2))
  })

  def apply(inputs: Seq[Expr], types: Seq[Type]): TupleExpr = apply(inputs.head, inputs(1))

  def unapply(expr: TupleExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class Tuple3Expr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): Tuple3Expr = Tuple3Expr(newInnerIR)
}

object Tuple3 {
  def apply(input1: Expr, input2: Expr, input3: Expr): Tuple3Expr = Tuple3Expr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val dtv3 = CGenDataTypeVar()
    val outtt = TupleStructType(dtv1, dtv2, dtv3)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv1, dtv2, dtv3), AlgoFunType(Seq(dtv1, dtv2, dtv3), outtt))), Seq(input1.t, input2.t, input3.t)), Seq(input1, input2, input3))
  })

  def unapply(expr: Tuple3Expr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class SplitExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SplitExpr = SplitExpr(newInnerIR, newTypes)
}

object Split extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, chunkSize: ArithTypeT): SplitExpr = apply(input, chunkSize, innerSize = true)

  def apply(input: Expr, size: ArithTypeT, innerSize: Boolean): SplitExpr = {
    val dtv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(dtv, lentv)

    val (chunkSize: ArithExpr, numberOfChunks: ArithExpr) =
      if (innerSize)
        (size.ae, lentv.ae / size.ae)
      else
        (lentv.ae / size.ae, size.ae)

    val split = SplitExpr({
      val outseqt = ArrayType(ArrayType(dtv, chunkSize), numberOfChunks)
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
    }, Seq(chunkSize))
    split
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SplitExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: SplitExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class JoinExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): JoinExpr = JoinExpr(newInnerIR)
}

object Join extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): JoinExpr = JoinExpr({
    val dtv = CGenDataTypeVar()
    val innerlentv = ArithTypeVar()
    val outerlentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(ArrayTypeVar(dtv, innerlentv), outerlentv)
    val outseqt = ArrayType(dtv, innerlentv.ae * outerlentv.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, AlgoFunType(inseqtv, outseqt))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): JoinExpr = apply(inputs.head)

  def unapply(expr: JoinExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class AllocExpr(innerIR: Expr, at: ArithTypeT) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AllocExpr = AllocExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def types: Seq[Type] = Seq(at)

  def ak = toAk(at)
}

// A |-> Dest[A]
object Alloc {
  def apply(t: Type, ak: AllocKind): AllocExpr = AllocExpr({
    AllocExpr(Value(AddressType(t)), toAt(ak))
  }, toAt(ak))

  def unapply(expr: AllocExpr): Some[(Type, AllocKind, Type)] =
    Some((expr.t.asInstanceOf[AddressType].ptrType, expr.ak, expr.t))
}

case class TypeConvExpr(innerIR: Expr) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): TypeConvExpr = TypeConvExpr(newInnerIR)

  override def types: Seq[Type] = Seq()
}

// A |-> B -> A
// TODO: the lift rule for typeconv
object TypeConv {
  def apply(in: Expr, toT: Type): TypeConvExpr = TypeConvExpr({
    val atv = CGenDataTypeVar()
    val intv = toT
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(atv, AlgoFunType(atv, intv))), in.t), in)
  })

  def unapply(expr: TypeConvExpr): Some[(Expr, Type)] = {
    Some((expr.args.head, expr.t))
  }
}

case class FreeExpr(innerIR: Expr, at: ArithTypeT) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): FreeExpr = FreeExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def types: Seq[Type] = Seq(at)

  def ak: AllocKind = toAk(at)
}

// A |-> Dest[A]
object Free {
  def apply(in: Expr, ak: AllocKind): FreeExpr = FreeExpr({
    val intv = AddressTypeVar()
    val out = VoidType()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, AlgoFunType(intv, out))), in.t), in)
  }, toAt(ak))

  def unapply(expr: FreeExpr): Some[(Expr, AllocKind, Type)] =
    Some((expr.args.head, expr.ak, expr.t))
}

case class RangeExpr(innerIR: Expr, n: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): RangeExpr = RangeExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])
}

object Range extends BuiltinExprFun {
  override def args: Int = 3

  def apply(start: Expr, step: Expr, n: ArithTypeT): RangeExpr = RangeExpr({
    val starttv = IntTypeVar(32)
    val steptv = IntTypeVar(32)
    val outt = IntType(32)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(starttv, steptv), AlgoFunType(Seq(starttv, steptv), outt))), Seq(start.t, step.t)), Seq(start, step))
  }, n)

  def unapply(expr: RangeExpr): Some[(Expr, Expr, ArithTypeT, Type)] = {
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithTypeT], expr.t))
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): Expr = Range(inputs.head, inputs.tail.head, types.head.asInstanceOf[ArithTypeT])
}

case class AccMapExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AccMapExpr = AccMapExpr(newInnerIR)
}

object AccMap {

  // ([U]n -> T -> U) -> [U]n -> [T]n -> [U]n
  def apply(f: Expr, init: Expr, input: Expr): AccMapExpr = AccMapExpr({
    val utv = CGenDataTypeVar()
    val ttv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val uarrtv = ArrayTypeVar(utv, lentv)
    val ftv = FunTypeVar(uarrtv, FunTypeVar(ttv, utv))
    val inttv = ArrayTypeVar(ttv, lentv)
    val outt = ArrayType(utv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, uarrtv, inttv), AlgoFunType(Seq(ftv, uarrtv, inttv), outt))), Seq(f.t, init.t, input.t)), Seq(f, init, input))
  })

  def unapply(expr: AccMapExpr): Some[(Expr, Expr, Expr, Type)] = {
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
  }
}

case class IdxAccMapExpr(innerIR: Expr, n: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxAccMapExpr = IdxAccMapExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])
}

object IdxAccMap {
  // n |-> ([U]n -> idx -> U) -> [U]n -> [U]n
  def apply(f: Expr, init: Expr, n: ArithTypeT): IdxAccMapExpr = IdxAccMapExpr({
    val utv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val uarrtv = ArrayTypeVar(utv, lentv)
    val idxtv = IntType(32)
    val ftv = FunTypeVar(uarrtv, FunTypeVar(idxtv, utv))
    val outt = ArrayType(utv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, uarrtv), AlgoFunType(Seq(ftv, uarrtv), outt))), Seq(f.t, init.t)), Seq(f, init))
  }, n)

  def unapply(expr: IdxAccMapExpr): Some[(Expr, Expr, ArithTypeT, Type)] = {
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithTypeT], expr.t))
  }
}

case class PermuteExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): PermuteExpr = PermuteExpr(newInnerIR)
}

object Permute {
  // s |-> (Idx -> Idx) -> [T]n -> [T]n
  def apply(f: Expr, in: Expr, s: ArithTypeT = 0): PermuteExpr = PermuteExpr({
    val idxt = IntType(32)
    val ttv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inarrtv = ArrayTypeVar(ttv, lentv)
    val ftv = FunTypeVar(idxt, idxt)
    val outt = ArrayType(ttv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, inarrtv), AlgoFunType(Seq(ftv, inarrtv), outt))), Seq(f.t, in.t)), Seq(f, in))
  })

  def unapply(expr: PermuteExpr): Some[(Expr, Expr, Type)] = Some((expr.args.head, expr.args(1), expr.t))
}
