package cGen

object AlgoOp extends Enumeration {
  type AlgoOp = Value
  val Add, Mul, Div, Sub, Mod, Pow, Minus, Cos, Sin, Sqrt, Exp, Log, Gt, Ge, Lt, Le, Andand, Eq, RightShift, LeftShift = Value
}
