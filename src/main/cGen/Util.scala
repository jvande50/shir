package cGen

import core._
import core.rewrite.{RewriteMode, Rule}
import core.util.IRDotGraph

import java.io.PrintWriter
import scala.annotation.tailrec
import scala.collection.mutable

object Util {
  @tailrec
  def param(lam: LambdaT, i: Int): ParamDef = i match {
    case 0 => lam.param // param starts from 0
    case _ => param(lam.body.asInstanceOf[LambdaT], i - 1)
  }

  def params(lam: LambdaT): Seq[ParamDef] = lam match {
    case CLambda(p, b: LambdaT, _) => Seq(p) ++ params(b)
    case CLambda(p, _, _) => Seq(p)
    case Lambda(p, b: LambdaT, _) => Seq(p) ++ params(b)
    case Lambda(p, _, _) => Seq(p)
    case _ =>
      throw new Exception("Should not reach")
  }

  @tailrec
  def innermostBody(lam: LambdaT): Expr = lam.body match {
    case _: LambdaT => innermostBody(lam.body.asInstanceOf[LambdaT])
    case other => other
  }

  trait ToFile {
    def write(path: String, content: String): Unit = {
      new PrintWriter(path) {
        write(content)
        close()
      }
    }
  }

  object FileWriter extends ToFile {
    def apply(path: String, content: String): Unit = write(path, content)
  }

  def tic(): Long = System.nanoTime()

  def toc(t: Long): Long = System.nanoTime() - t

  def formatTime(t: Long) = {
    val s: Double = t.toDouble / 1000000000
    f"${s}%2.4fs"
  }
}