package cGen

import cGen.AlgoOp.AlgoOp
import cGen.Util.FileWriter
import core._
import lift.arithmetic.Cst

import java.time.temporal.TemporalQueries.offset
import java.util.concurrent.atomic.AtomicInteger
import scala.collection.mutable
import scala.language.implicitConversions

object CConversion {
  implicit def CExpr2CStmt(e: CExpr): CStmt = CExprStmt(e)

  def toStmt(node: CAST): CStmt = node match {
    case e: CExpr => CExprStmt(e)
    case s: CStmt => s
  }
}

object GetId {
  private val _id = new AtomicInteger(0)
  def apply(): Int = _id.getAndIncrement()
}

// A function defines a block
// blocks can be nested
// each block defines a Env
class Env(parent: Option[Env]) {
  var cvds: Seq[CVarDecl] = Seq() // use to generate variable definition in block
  var cvdIds: mutable.Set[Long] = mutable.Set()
  var cvdFuncIds: mutable.Set[Long] = mutable.Set()
  val pdvdMap: mutable.Map[Long, CVarDecl] = parent match {
    case Some(e) => e.pdvdMap
    case None => mutable.Map() // map from paramDef to varDecl
  }
  val pdCExprMap: mutable.Map[Long, CExpr] = parent match { // map from paramDef/Use id to a AST CExpr
    case Some(e) => e.pdCExprMap // only one pdCExprMap exists, if there is a parent
    case None => mutable.Map()
  }
  val structDecls: mutable.Set[CStructDecl] = parent match {
    case Some(e) => e.structDecls// only one pdCExprMap exists, if there is a parent
    case None => mutable.Set()
  }

  def isPresent(cvd: CVarDecl): Boolean =
    if (cvdIds.contains(cvd.pd.id) || cvdFuncIds.contains(cvd.pd.id)) true
    else {
      parent match {
        case Some(e) => e.isPresent(cvd)
        case None => false
      }
    }

  def add(cvd: CVarDecl): Unit = if (!isPresent(cvd)) {
    cvds = cvds :+ cvd
    cvdIds.add(cvd.pd.id)
  }

  def addFuncId(cvd: CVarDecl): Unit = if (!isPresent(cvd)) {
    cvdFuncIds.add(cvd.pd.id)
  }

  def toSeq: Seq[CVarDecl] = cvds.toSeq

  def addCStructDecls(csd: CStructDecl): Unit = structDecls.add(csd)
}

object CCodeGen {
  def apply(expr: Expr, moreInclude: String = "", platform: Platform = Linux64): CCodeGen = new CCodeGen(moreInclude, platform).apply(expr)
}

class CCodeGen(val moreInclude: String, val platform: Platform) {
  var funDecls: Seq[CFunDecl] = Seq()
  private val _id = new AtomicInteger(0)

  def id: Int = _id.getAndIncrement()

  def funcName: String = "func_" + id

  def apply(expr: Expr): CCodeGen = {
    val env = new Env(None)
    AST(expr)(env)
    val cprogram = CProgram(funDecls, env.structDecls)
    cgen(cprogram)
    this
  }

  // every block should has its onw env
  def AST(expr: Expr)(implicit env: Env = new Env(None)): CAST = expr match {
    case _: LambdaT =>
      val newEnv: Env = new Env(Some(env))
      val fd = CFunDecl(getParams(expr, newEnv), getBody(expr, newEnv))
      funDecls = funDecls :+ fd
      fd
    case DBuild(f, n, ltt, _) => n.ae match {
      case Cst(0) => CDummy()
      case Cst(1) => // when n == 1, no need to generate a forloop
        val idxDecl = AST(f.asInstanceOf[LambdaT].param).asInstanceOf[CVarDecl]
        CTwoStmts(
          CAssign(CVarExpr(idxDecl), CNumLit(0)),
          AST(f.asInstanceOf[LambdaT].body).toStmt
        )
      case _ =>
        val newEnv = new Env(Some(env))
        val idxDecl = AST(f.asInstanceOf[LambdaT].param)(newEnv).asInstanceOf[CVarDecl]
        newEnv.addFuncId(idxDecl)
        val blockStmts = Seq(AST(f.asInstanceOf[LambdaT].body)(newEnv).toStmt)
        CForloop(CNumLit(0), CNumLit(n.ae.evalInt),
          idxDecl,
          CBlock(newEnv.toSeq, blockStmts))
    }
    case DIfold(f, init, d, n, _) =>
      val newEnv = new Env(Some(env))
      val idxDecl = AST(Util.param(f.asInstanceOf[LambdaT], 0))(newEnv).asInstanceOf[CVarDecl] // add vardecl to block
      newEnv.addFuncId(idxDecl)
      val accDecl = AST(Util.param(f.asInstanceOf[LambdaT], 1))(env).asInstanceOf[CVarDecl]
      env.add(accDecl)
      val blockStmts = Seq(AST(Util.innermostBody(f.asInstanceOf[LambdaT]))(newEnv).toStmt)
      CTwoStmts(
        CAssign(CVarExpr(accDecl), AST(init).toExpr),
        CTwoStmts(
          CForloop(CNumLit(0), CNumLit(n.ae.evalInt), idxDecl, CBlock(newEnv.toSeq, blockStmts :+ CAssign(CVarExpr(accDecl), AST(ValueAt(d)).toExpr).toStmt)),
          CAssign(AST(ValueAt(d)).toExpr, CVarExpr(accDecl))
        )
      )
    case BinaryOp(in1, in2, op, _, _) => CBinOp(op, AST(in1), AST(in2))
    case SingleOp(in, op, _) => CSingleOp(op, AST(in))
    case Id(in, _) => AST(in)
    case Assign(in, d, _) => in match {
      case ParamUse(pd) if env.pdCExprMap.contains(pd.id) => CAssign(AST(ValueAt(d)).toExpr, env.pdCExprMap(pd.id).toExpr)
      case ParamUse(pd) => CAssign(AST(ValueAt(d)).toExpr, AST(in).toExpr)
      case _ => CAssign(AST(ValueAt(d)).toExpr, AST(in).toExpr)
    }
    case DestArrayAccess(in, idx, _) => CDestArrayAccess(AST(in).asInstanceOf[CExpr], AST(idx).asInstanceOf[CExpr])
    case DestSelect(in, selection, _) => selection.ae match {
      // assuming that selection is up to 2
      // when selection limit is more than 2, need a better algorithm to calculate the offset given the memory alignment on structure
      case Cst(0) => AST(in)
      case Cst(1) =>
        val tst = in.t.asInstanceOf[AddressType].ptrType.asInstanceOf[NamedTupleStructType]
        env.addCStructDecls(CStructDecl(tst))
        val offset = tst.namedTypes.map(_._2).map(platform.getSize).foldLeft(0)((a, b) => if (a > b) a else b)
        CBinOp(AlgoOp.Add, CTypeConv(AST(in).toExpr, AddressType(CharType())), AST(Constant(offset, IntType(32))))
      case _ => ???
    }
    case IdxOffsetOp(idx, offset, op, _) => CBinOp(op, AST(idx), AST(offset))
    case ArrayAccess(in, idx, _) =>
      val cin = AST(in).toExpr
      val cidx = AST(idx).toExpr
      CArrayAccess(cin, cidx)
    case pd: ParamDef =>
      val cvd = CVarDecl(pd)
      env.pdvdMap.put(pd.id, cvd)
//      env.add(cvd)
      cvd
    case ParamUse(pd) => env.pdvdMap.get(pd.id) match {
      case Some(cvd) => {
        if (!env.isPresent(cvd)) env.add(cvd)
        CVarExpr(cvd)
      }
      case None =>
        // using a paramDef that is not defined yet (in the funcDecl)
        val cvd = AST(pd).asInstanceOf[CVarDecl]
        env.add(cvd)
        CVarExpr(cvd)
    }
    case CGenLet(ParamDef(VoidType(), _), body, arg, _) => CTwoStmts(AST(arg).toStmt, AST(body).toStmt)
    case CGenLet(p, body, Alloc(t, ak, _), _) => ak match {
        case AllocKind.Heap => t match {
          case ArrayType(nT@ArrayType(mT@ArrayType(_, _), m), n) =>
            CTwoStmts(
            CTwoStmts(
              CAssign(AST(ParamUse(p)).toExpr, CDynamicAlloc(t)),
              {
                val newEnv = new Env(Some(env))
                val i = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
                CForloop(CNumLit(0), CNumLit(n.ae.evalInt), i,
                  CBlock(newEnv.toSeq, Seq(CAssign(CDestArrayAccess(AST(ParamUse(p)).toExpr, CVarExpr(i)), CDynamicAlloc(nT)), {
                    val newEnv = new Env(Some(env))
                    val j = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
                    CForloop(CNumLit(0), CNumLit(m.ae.evalInt), j,
                      CBlock(newEnv.toSeq, Seq(CAssign(CDestArrayAccess(CDestArrayAccess(AST(ParamUse(p)).toExpr, CVarExpr(i)), CVarExpr(j)), CDynamicAlloc(mT)))))
                  })))
              }),
              AST(body).toStmt)
          case ArrayType(inT@ArrayType(_, _), len) =>
            CTwoStmts(
              CAssign(AST(ParamUse(p)).toExpr, CDynamicAlloc(t)),
              CTwoStmts({
                  val newEnv = new Env(Some(env))
                  val idxDecl = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
                  CForloop(CNumLit(0), CNumLit(len.ae.evalInt), idxDecl,
                    CBlock(newEnv.toSeq, Seq(CAssign(CDestArrayAccess(AST(ParamUse(p)).toExpr, CVarExpr(idxDecl)), CDynamicAlloc(inT)))))
                },
                AST(body).toStmt
            ))
          case ArrayType(_, _) => CTwoStmts(
            CAssign(AST(ParamUse(p)).toExpr, CDynamicAlloc(t)),
            AST(body).toStmt
          )
          case NamedTupleStructType(_, _) => CTwoStmts(
              CAssign(AST(ParamUse(p)).toExpr, CDynamicAlloc(t)),
              AST(body).toStmt
            )
          case other =>
            throw new NotImplementedError(other.toShortString)
        }
        case AllocKind.Stack =>
          val s = ParamDef(t)
          CTwoStmts(CAssign(AST(ParamUse(p)).toExpr, CAddressOf(AST(ParamUse(s)).toExpr)), AST(body).toStmt)
    }
    case CGenLet(p, body, we: NonDPSWriteExpr, _) =>
      env.pdCExprMap.put(p.id, AST(we).toExpr) // not generate any code for this
      AST(body)
    case CGenLet(p, body, tuple: TupleExpr, _) =>
      env.pdCExprMap.put(p.id, AST(tuple).toExpr) // not generate any code for this
      AST(body)
    case CGenLet(p, body, arg, _) =>
      val ASTArg = AST(arg)
      ASTArg match {
        case c: CTwoStmts =>
          val last = c.toExpr
          val ASTArg1 = c.popLast
          CTwoStmts(ASTArg1.toStmt,
            CTwoStmts(CAssign(AST(ParamUse(p)).toExpr, last.toExpr),
              AST(body).toStmt))
        case _ =>
          // arg is an expr
          CTwoStmts(CAssign(AST(ParamUse(p)).toExpr, ASTArg.toExpr), AST(body).toStmt)
      }
    case ValueAt(in, _) => CValueAt(AST(in).toExpr)
    case Constant(value, t) => CNumLit(value, t)
    case DIf(computeCond, cond, b0, b1, _) => CIf(AST(computeCond).toStmt, AST(cond).toExpr, AST(b0).toStmt, AST(b1).toStmt)
    case DExternFunctionCall(name, ins, _, _) => CFunctionCall(name, ins.map(AST(_).toExpr))
    case TypeConv(in, t) =>
      CTypeConv(AST(in).toExpr, t)
    case Free(in, ak, _) => ak match {
      case AllocKind.Static => CDummy()
      case AllocKind.Stack => CDummy()
      case AllocKind.Heap => CFree(AST(in).toExpr)
      case _ => throw new Exception("Not defined")
    }
    case Select(in, selection, _) => selection.ae match {
      case Cst(0) => CSelect0(AST(in).toExpr)
      case Cst(1) => CSelect1(AST(in).toExpr)
      case _ => throw new Exception("Not defined")
    }
    case Tuple(in1, in2, t: NamedTupleStructType) =>
      env.addCStructDecls(CStructDecl(t))
      CStructExpr(CStructDecl(t))
    case other => throw new Exception(s"Should not reach: $other")
  }

  def AST(t: Type): CAST = t match {
    case att: ArithTypeT => CNumLit(att.ae.evalInt)
  }

  val sb = new StringBuilder()

  // the string it returns should not be used outside of this function
  def cgen(node: CAST): String = node match {
    case CProgram(fds, sds) =>
      sds.foreach(cgen(_))
      fds.foreach(cgen(_))
      ""
    case CFunDecl(params, block, t) => {
      sb.append(s"${cgen(t)} ${funcName} (")
      sb.append(params.map(vd => s"${cgen(vd.t)} ${cgen(vd)}").mkString(", "))
      sb.append(")\n")
      cgen(block)
    }
    case CStructDecl(tst) =>
      sb.append(s"typedef struct DEF_${tst.getName().toUpperCase()} {\n")
      tst.namedTypes.foreach(p => sb.append(s"${cgen(p._2)} ${p._1};\n"))
      sb.append(s"} ${tst.getName()};\n\n")
      ""
    case CBlock(vds, stmts) => {
      // TODO: gen the code for vd in cblock
      sb.append("{\n")
      vds.foreach { vd => sb.append(s"${cgen(vd.t)} ${cgen(vd)};\n") }
      stmts.foreach {
        case CExprStmt(e) =>
          sb.append(s"${cgen(e)};\n")
        case other => cgen(other)
      }
      sb.append("}\n")
      ""
    }
    case CForloop(begin, end, idx, block) => {
      sb.append(s"for (int ${cgen(CVarExpr(idx))} = ${cgen(begin)}; ${cgen(idx)} < ${cgen(end)}; ${cgen(idx)}++)\n")
      cgen(block)
      ""
    }
    case CVarDecl(pd) => s"var${pd.id}" // specially one, treated as VarExpr
    case CVarExpr(vd) => cgen(vd)
    case CNumLit(i, t) => t match {
      case IntType(_, _) => i.toInt.toString
      case DoubleType() => i.toString
    }
    case CAssign(lhs, rhs) =>
      sb.append(s"${cgen(lhs)} = ${cgen(rhs)};\n") // .toString
      ""
    case CValueAt(n) => n.t match {
      case AddressType(pt) if pt.isInstanceOf[ArrayType] => s"${cgen(n)}"
      case _ => s"*(${cgen(n)})"
    }
    case CAddressOf(n) => s"&(${cgen(n)})"
    case CDestArrayAccess(dest, idx) => node.asInstanceOf[CExpr].t match {
      case AddressType(pt) if pt.isInstanceOf[ArrayType] => s"${cgen(dest)}[${cgen(idx)}]"
      case _ => s"&(${cgen(dest)}[${cgen(idx)}])"
    }
    case CBinOp(op, in1, in2) => op match {
      case AlgoOp.Div => s"(${cgen(in1)} / ${cgen(in2)})"
      case AlgoOp.Mod => s"(${cgen(in1)} % ${cgen(in2)})"
      case AlgoOp.Add => s"(${cgen(in1)} + ${cgen(in2)})"
      case AlgoOp.Sub => s"(${cgen(in1)} - ${cgen(in2)})"
      case AlgoOp.Mul => s"(${cgen(in1)} * ${cgen(in2)})"
      case AlgoOp.Pow => s"pow(${cgen(in1)}, ${cgen(in2)})"
      case AlgoOp.Gt => s"(${cgen(in1)} > ${cgen(in2)})"
      case AlgoOp.Lt => s"(${cgen(in1)} < ${cgen(in2)})"
      case AlgoOp.Ge => s"(${cgen(in1)} >= ${cgen(in2)})"
      case AlgoOp.Eq => s"(${cgen(in1)} == ${cgen(in2)})"
      case AlgoOp.Andand => s"(${cgen(in1)} && ${cgen(in2)})"
      case AlgoOp.RightShift => s"(${cgen(in1)} >> ${cgen(in2)})"
      case AlgoOp.LeftShift => s"(${cgen(in1)} << ${cgen(in2)})"
    }
    case CSingleOp(op, in) => op match {
      case AlgoOp.Minus => s"-(${cgen(in)})"
      case AlgoOp.Sin => s"sin(${cgen(in)})"
      case AlgoOp.Cos => s"cos(${cgen(in)})"
      case AlgoOp.Sqrt => s"sqrt(${cgen(in)})"
      case AlgoOp.Exp => s"exp(${cgen(in)})"
      case AlgoOp.Log => s"log(${cgen(in)})"
    }
    case CTypeConv(in, t) => s"(${cgen(t)}) (${cgen(in)})"
    case CArrayAccess(array, idx) => s"${cgen(array)}[${cgen(idx)}]"
    case CTwoStmts(first, second) => {
      if (first.isInstanceOf[CExpr]) sb.append(s"${cgen(first)};\n")
      else cgen(first)
      if (second.isInstanceOf[CExpr]) sb.append(s"${cgen(second)};\n")
      else cgen(second)
      ""
    }
    case CDynamicAlloc(tt) => s"malloc(${sizeof(tt)})"
    case CDummy() => ""
    case CExprStmt(e) => {
      sb.append(s"${cgen(e)};\n")
      ""
    }
    case CIf(computeCond, cond, branch0, branch1) =>
      cgen(computeCond)
      sb.append(s"if (${cgen(cond)}) {\n")
      cgen(branch0)
      sb.append(s"}\nelse {\n")
      cgen(branch1)
      sb.append(s"}\n")
      ""
    case CFunctionCall(name, ins) =>
      s"$name(${ins.map(cgen).reverse.mkString(",")})"
    case CFree(in) =>
      sb.append(s"free(${cgen(in)});\n")
      ""
    case s@CSelect0(in) => s"${cgen(in)}.${s.name}"
    case s@CSelect1(in) => s"${cgen(in)}.${s.name}"
    case CStructExpr(csd) => csd.tst.getName()
  }

  def cgen(t: Type): String = t match {
    case v: VoidType => "void"
    case ArrayType(dt, _) => s"${cgen(dt)}*"
    case IntType(ArithType(Cst(32)), ArithType(Cst(0))) => "int"
    case IntType(ArithType(Cst(32)), ArithType(Cst(1))) => "unsigned int"
    case DoubleType() => "double"
    case CharType() => "char"
    case AddressType(pt) => pt match {
      // in C the pointer type to an array(dt) should the type of *dt, thus no * in the front of it
      case at: ArrayType => s"${cgen(at)}"
      case _ => s"${cgen(pt)}*"
    }
    case BoolType() => "int"
    case tst@NamedTupleStructType(_, _) => tst.getName()
    case _ =>
      throw new Exception(s"Unknown Type ${t}")
  }

  def sizeof(t: Type): String = t match {
    case ArrayType(dt, len) => s"sizeof(${cgen(dt)}) * ${len.ae.evalInt}"
    case IntType(ArithType(Cst(32)), ArithType(Cst(0))) => "sizeof(int)"
    case IntType(ArithType(Cst(32)), ArithType(Cst(1))) => "sizeof(unsigned int)"
    case DoubleType() => "sizeof(double)"
    case BoolType() => "sizeof(int)"
    case NamedTupleStructType(name, _) => s"sizeof($name)"
    case _ => throw new Exception(s"Unknown Type ${t}")
  }

  def getParams(expr: Expr, env: Env): Seq[CVarDecl] = expr match {
    case lam: LambdaT => {
      val cvd = AST(lam.param)(env).asInstanceOf[CVarDecl]
      env.addFuncId(cvd) // need a special handle, add the funcId set, not a general id set
      Seq(cvd) ++ getParams(lam.body, env)
    }
    case _ => Seq()
  }

  def getBody(expr: Expr, env: Env): CBlock = expr match {
    case CLambda(p, body, _) => getBody(body, env)
    case other =>
      val stmts = Seq(AST(other)(env).toStmt)
      CBlock(env.toSeq, stmts) // Block(other)
  }

  override def toString: String = sb.toString

  def includes: String = s"#include <stdlib.h>\n#include<stdio.h>\n#include <math.h>\n${moreInclude}"

  def ccode(funcName: String = "func_0"): String = (includes + this.toString()).replace("func_0", funcName)

  def templete(funcName: String = "func_0"): String = {
    s"""#include "vector.h"
       |#include "test.h"
       |#include <stdio.h>
       |#include <stdlib.h>
       |#include <time.h>
       |#include <math.h>
       |#include "$funcName.h"
       |
       |int main() {
       |
       |}
       |""".stripMargin
  }

  def genTest(funcName: String): Unit = {
    genCode(funcName)
    val t = templete(funcName)
    if (!new java.io.File(s"src/test/cGen/C/$funcName.c").exists) {
      FileWriter(s"src/test/cGen/C/$funcName.c", t)
    }
  }

  def genCode(funcName: String): Unit = {
    val c = ccode(funcName)
    FileWriter(s"src/test/cGen/C/$funcName.h", c)
  }
}

object CAST {
  private val _id = new AtomicInteger(0)

  def id: Int = _id.getAndIncrement()
}

class CAST {
  val id = CAST.id

  def toStmt: CStmt = this match {
    case e: CExpr => CExprStmt(e)
    case s: CStmt => s
    case _ =>
      throw new Exception(s"$this cannot toStmt")
  }

  def toExpr: CExpr = {
    this match {
      case expr: CExpr => expr
      case _ =>
        throw new Exception(s"$this cannot toExpr")
    }
  }

  def isExpr: Boolean = this.isInstanceOf[CExpr]

  def children: Seq[CAST] = Seq()
}

// TODO: provide the correct type for CFunDecl
case class CFunDecl(params: Seq[CVarDecl], block: CBlock, t: Type = VoidType()) extends CAST {
  override def children: Seq[CAST] = params ++ Seq(block)

  //  val ints: Seq[Type] = params.map(_.t)
}

case class CVarDecl(pd: ParamDef) extends CAST {
  override def children: Seq[CAST] = Seq()

  val t: Type = pd.t
}

trait CStmt extends CAST {
  def ::(that: CStmt): CTwoStmts = CTwoStmts(this, that)
}

trait CExpr extends CAST {
  def t: Type
}

case class CProgram(funDecls: Seq[CFunDecl], structDecls: mutable.Set[CStructDecl]) extends CAST {
  override def children: Seq[CAST] = funDecls
}

case class CExprStmt(e: CExpr) extends CStmt {
  override def children: Seq[CAST] = Seq(e)

  override def toExpr: CExpr = e
}

case class CBlock(varDecls: Seq[CVarDecl], stmts: Seq[CStmt]) extends CStmt {
  override def children: Seq[CAST] = varDecls ++ stmts
}

case class CIf(computeCond: CStmt, cond: CExpr, branch0: CStmt, branch1: CStmt) extends CStmt {
  override def children: Seq[CAST] = Seq(computeCond, cond, branch0, branch1)
}

case class CForloop(begin: CNumLit, end: CNumLit, idx: CVarDecl, block: CBlock) extends CStmt {
  override def children: Seq[CAST] = Seq(begin, end, idx, block)
}

case class CFunctionCall(name: String, ins: Seq[CExpr]) extends CExpr {
  override def children: Seq[CAST] = ins

  override def t: Type = ???
}


case class CNumLit(num: Double, t: Type = IntType(32)) extends CExpr {
  override def children: Seq[CAST] = Seq()
}

case class CBinOp(op: AlgoOp, in1: CAST, in2: CAST) extends CExpr {
  override def children: Seq[CAST] = Seq(in1, in2)

  override def t: Type = IntType(32)
}

case class CSingleOp(op: AlgoOp, in: CAST) extends CExpr {
  override def children: Seq[CAST] = Seq(in)

  override def t: Type = IntType(32)
}

case class CAssign(lhs: CExpr, rhs: CExpr) extends CStmt {
  override def children: Seq[CAST] = Seq(lhs, rhs)
}

case class CVarExpr(vd: CVarDecl) extends CExpr {
  override def children: Seq[CAST] = Seq(vd)

  override def t: Type = vd.t
}

case class CValueAt(n: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(n)

  override def t: Type = n.t.asInstanceOf[AddressType].ptrType
}

case class CAddressOf(n: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(n)

  override def t: Type = AddressType(n.t)
}

case class CArrayAccess(array: CExpr, idx: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(array, idx)

  override def t: Type = array.t.asInstanceOf[ArrayType].et
}

case class CDestArrayAccess(dest: CExpr, idx: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(dest, idx)

  override def t: Type = {
    val et = dest.t.asInstanceOf[AddressType].ptrType.asInstanceOf[ArrayType].et
    AddressType(et)
  }
}

case class CDummy() extends CExpr {
  override def children: Seq[CAST] = ???

  override def t: Type = ???
}

case class CTwoStmts(first: CStmt, second: CStmt) extends CStmt {
  override def children: Seq[CAST] = Seq(first, second)

  override def toExpr: CExpr = second.toExpr

  def popLast: CStmt = second match {
    case c@CTwoStmts(_, _) => CTwoStmts(first, c.popLast)
    case other => first
  }
}

case class CDynamicAlloc(tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = AddressType(tt)
}

case class CStaticAlloc(tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = AddressType(tt)
}

case class CSizeOf(tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = IntType(32);
}

case class CTypeConv(in: CExpr, tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = tt;
}

case class CFree(in: CExpr) extends CStmt {
  override def children: Seq[CAST] = Seq()
}

case class CSelect0(in: CExpr) extends CExpr {
  val name = "t0"

  override def children: Seq[CAST] = Seq()

  override def t: Type = in.t.asInstanceOf[NamedTupleStructType].namedTypes.head._2
}

case class CSelect1(in: CExpr) extends CExpr {
  val name = "t1"

  override def children: Seq[CAST] = Seq()

  override def t: Type = in.t.asInstanceOf[NamedTupleStructType].namedTypes(1)._2
}

case class CStructDecl(tst: NamedTupleStructType) extends CStmt {
  override def children: Seq[CAST] = Seq()

  override def equals(obj: Any): Boolean = obj match {
    case CStructDecl(_) => obj.hashCode() == this.hashCode()
    case _ => false
  }

  override def hashCode(): Int = tst.getName().hashCode()
}

case class CStructExpr(csd: CStructDecl) extends CExpr {
  override def t: Type = csd.tst
}