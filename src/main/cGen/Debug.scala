package cGen

object Debug {
  // 0 means no profile info, 1 means standard info, 2 means verbose info, 3 print AST
  var PROFILE: Int = try {
    scala.util.Properties.envOrElse("PROFILE", "2").toInt
  } catch {
    case _: Throwable => 0
  }
}


