package cGen

import cGen.Lib._
import cGen.Util.FileWriter
import core._
import core.util.IRDotGraph
import org.junit.Test

import scala.language.postfixOps

class DPSPassTest {

  @Test
  def buildFuse1Test(): Unit = {
    var tree: Expr = Lib.vectorAdd3Opt(100)
    tree = DPSPass(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def buildFuse2Test(): Unit = {
    var tree: Expr = Lib.vectorAdd3(100)
    tree = DPSPass(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def buildFuse3Test(): Unit = {
    val len = 100
    implicit val t: () => IntType = () => IntType(32)
    val a = ParamDef(ArrayType(IntType(32), len))
    val b = ParamDef(ArrayType(IntType(32), len))
    val c = ParamDef(ArrayType(IntType(32), len))
    val map1 = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))))
    val map2 = Map(Lib.addTup, Zip(Tuple(ParamUse(c), map1)))
    var tree: Expr = CLambda(Seq(a, b, c), map2)
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
    ccode.genTest("add3")
  }

  @Test
  def simpleCrossTest(): Unit = {
    val a = ParamDef(IntType(32))
    val b = ParamDef(IntType(32))
    val c = ParamDef(IntType(32))
    val f = CLambda(Seq(a, b, c), Concat(Concat(ToArray(ParamUse(a)), ToArray(ParamUse(b))), ToArray(ParamUse(c))))
    //    val f = CGenLambda(Seq(a, b), Concat(ToArray(ParamUse(a)), ToArray(ParamUse(b))))
    var tree: Expr = f
    tree = DPSPass(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def crossTest(): Unit = {
    val a = ParamDef(ArrayType(IntType(32), 3))
    val b = ParamDef(ArrayType(IntType(32), 3))
    val f = CLambda(a, CLambda(b, Cross(ParamUse(a), ParamUse(b))))
    val tree = DPSPass(f)
    val ccode = CCodeGen(tree).ccode("cross")
    println(ccode)
    FileWriter("src/test/cGen/C/cross.h", ccode)
  }

  @Test
  def toArrayTest(): Unit = {
    val a = ParamDef(ArrayType(IntType(32), 3))
    val aInt = Constant(1, IntType(32))
    val f = CLambda(Seq(a), Concat(ToArray(aInt), ToArray(aInt)))

    var tree: Expr = f
    tree = TypeChecker.check(tree)
    tree = DPSPass(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def vectorSumTest(): Unit = {
    implicit val t: () => Type = () => DoubleType()
    val v = ParamDef(ArrayType(DoubleType(), 12))
    var tree: Expr = CLambda(v, VectorSum(ParamUse(v)))
    tree = DPSPass(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def sqNormTest(): Unit = {
    implicit val t: () => CGenDataTypeT = () => DoubleType()
    val v = ParamDef(ArrayType(DoubleType(), 2))
    var tree: Expr = CLambda(v, SqNorm(ParamUse(v)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("sqnorm")
    println(ccode)
  }


  @Test
  def slideTest(): Unit = {
    val x = ParamDef(ArrayType(DoubleType(), 12))
    val arr = Slice(ParamUse(x), Constant(3, IntType(32)), ArithType(3))
    var tree: Expr = CLambda(x, Id(ArrayAccess(arr, Constant(1, IntType(32)))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode()
    println(ccode)
  }

  @Test
  def radialDistortTest(): Unit = {
    val radParams = ParamDef(ArrayType(DoubleType(), 3))
    val proj = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(radParams, CLambda(proj, RadialDistort(ParamUse(radParams), ParamUse(proj))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode("radial_distort")
    FileWriter("src/test/cGen/C/radial-distort.h", ccode)
    println(ccode)
  }

  @Test
  def rodriguesRotatePointTest(): Unit = {
    val rot = ParamDef(ArrayType(DoubleType(), 3))
    val x = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(rot, CLambda(x, RodriguesRotatePoint(ParamUse(rot), ParamUse(x))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode("rodrigues_rotate_point")
    println(ccode)
    FileWriter("src/test/cGen/C/rodrigues-rotate-point.h", ccode)
  }

  @Test
  def projectTest(): Unit = {
    val cam = ParamDef(ArrayType(DoubleType(), 11))
    val x = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(cam, CLambda(x, Project(ParamUse(cam), ParamUse(x))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode("project")
    FileWriter("src/test/cGen/C/project.h", ccode)
  }

  @Test
  def projectAndSqNormTest(): Unit = {
    implicit val t: () => DoubleType = () => DoubleType()
    val cam = ParamDef(ArrayType(DoubleType(), 11))
    val x = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(cam, CLambda(x, ProjectAndSqNorm(ParamUse(cam), ParamUse(x))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode("project_and_sqnorm")
    FileWriter("src/test/cGen/C/project-and-sqnorm.h", ccode)
  }

  @Test
  def baTest(): Unit = {
    val a = ParamDef(ArrayType(DoubleType(), 3))
    val b = ParamDef(ArrayType(DoubleType(), 3))
    val cam = ParamDef(ArrayType(DoubleType(), 11))
    var tree: Expr = CLambda(a, CLambda(b, CLambda(cam, BundleAdjustment(ParamUse(a), ParamUse(b), ParamUse(cam)))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode("ba")
    FileWriter("src/test/cGen/C/ba.h", ccode)
    println(ccode)
  }

  @Test
  def ifTest(): Unit = {
    val a = ParamDef(DoubleType())
    val b = ParamDef(DoubleType())
    var tree: Expr = CLambda(a, CLambda(b, If(gt(ParamUse(a), ParamUse(b)), Id(ParamUse(a)), Id(ParamUse(b)))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode("_if")
    FileWriter("src/test/cGen/C/if.h", ccode)
    println(ccode)
  }

  @Test
  def vectorMaxTest(): Unit = {
    val v = ParamDef(ArrayType(DoubleType(), 5))
    var tree: Expr = CLambda(v, VectorMax(ParamUse(v))(() => DoubleType()))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode("vector_max")
    FileWriter("src/test/cGen/C/vector-max.h", ccode)
    println(ccode)
  }

  @Test
  def logsumexpTest(): Unit = {
    val v = ParamDef(ArrayType(DoubleType(), 5))
    var tree: Expr = CLambda(v, LogSumExp(ParamUse(v))(() => DoubleType()))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("logsumexp")
    println(ccode)
  }


  @Test
  def matrixMulTest(): Unit = {
    implicit val t = () => DoubleType()
    val m1 = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    val m2 = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(m1, CLambda(m2, MatrixMul(ParamUse(m1), ParamUse(m2))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("matrix_mul")
    println(ccode)
  }


  @Test
  def qtimesvTest(): Unit = {
    val d = 3;
    val td = ((d) * ((d) + (1))) / (2)
    val q = ParamDef(ArrayType(DoubleType(), d))
    val l = ParamDef(ArrayType(DoubleType(), td))
    val v = ParamDef(ArrayType(DoubleType(), d))
    var tree: Expr = CLambda(q, CLambda(l, CLambda(v, Qtimesv(ParamUse(q), ParamUse(l), ParamUse(v)))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("qtimesv")
    println(ccode)
  }

  @Test
  def GMMTest(): Unit = {
    implicit val t = () => DoubleType()
    val n = 100;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val x = ParamDef(ArrayType(ArrayType(t(), d), n))
    val alphas = ParamDef(ArrayType(t(), K))
    val means = ParamDef(ArrayType(ArrayType(t(), d), K))
    val qs = ParamDef(ArrayType(ArrayType(t(), d), K))
    val ls = ParamDef(ArrayType(ArrayType(t(), td), K))
    val wishartGamma = ParamDef(t())
    val wishartM = ParamDef(t())
    var tree: Expr = CLambda(x, CLambda(alphas, CLambda(means, CLambda(qs, CLambda(ls, CLambda(wishartGamma, CLambda(wishartM,
      GMM(ParamUse(x), ParamUse(alphas), ParamUse(means), ParamUse(qs), ParamUse(ls), ParamUse(wishartGamma), ParamUse(wishartM)))))))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, moreInclude = "#include \"qtimesv.h\"\n")
    ccode.genTest("gmm")
    println(ccode)
  }

  @Test
  def crazyConcatPushTest(): Unit = {
    implicit val t = () => DoubleType()
    val r0 = Concat(Concat(ToArray(one), ToArray(zero)), ToArray(zero))
    val r1 = Concat(Concat(ToArray(one), ToArray(zero)), ToArray(zero))
    val r2 = Concat(Concat(ToArray(one), ToArray(zero)), ToArray(zero))
    val a = ParamDef(t())
    var tree: Expr = CLambda(a, Concat(Concat(Push(r0), Push(r1)), Push(r2)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def angleAxisToRotationMatrixTest(): Unit = {
    val angleAxis = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(angleAxis, AngleAxisToRotationMatrix(ParamUse(angleAxis)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("angle_axis_to_rotation_matrix")
    println(ccode)
  }

  @Test
  def eulerAnglesToRotationMatrixTest(): Unit = {
    val eulerAngles = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(eulerAngles, EulerAnglesToRotationMatrix(ParamUse(eulerAngles)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("angle_axis_rotation_matrix")
    println(ccode)
  }

  @Test
  def makeRelativeTest(): Unit = {
    val poseParam = ParamDef(ArrayType(DoubleType(), 3))
    val baseRelative = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 4))
    var tree: Expr = CLambda(poseParam, CLambda(baseRelative, MakeRelative(ParamUse(poseParam), ParamUse(baseRelative))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("make_relative")
    println(ccode)
  }

  @Test
  def applyGlobalTransformTest(): Unit = {
    val poseParams = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    val positions = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(poseParams, CLambda(positions, ApplyGlobalTransform(ParamUse(poseParams), ParamUse(positions))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("apply_global_transform")
    println(ccode)
  }

  @Test
  def getPosedRelativesTest(): Unit = {
    val tree = DPSPass(GetPosedRelativesFunction(4))
    val ccode = CCodeGen(tree)
    ccode.genTest("get_posed_relatives")
    println(ccode)
  }

  @Test
  def matrix3DUpdate(): Unit = {
    val m = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    val nm = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(m, CLambda(nm, Matrix3DUpdate(ParamUse(m), one(() => IntType(32)) , two(() => IntType(32)), ParamUse(nm))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def relativeToAbsoluteTest(): Unit = {
    val relatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val parents = ParamDef(ArrayType(DoubleType(), 4))
    var tree: Expr = CLambda(relatives, CLambda(parents, RelativeToAbsolute(ParamUse(relatives), ParamUse(parents))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def getSkinnedVertexPositionsTest(): Unit = {
    val isMirror = ParamDef(ArrayType(DoubleType(), 3))
    val nBones = 4
    val poseParams = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 6))
    val baseRelatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val inverseBaseRelatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val basePositions = ParamDef(ArrayType(ArrayType(DoubleType(), nBones), 3))
    val basePositionsHomg = ParamDef(ArrayType(ArrayType(DoubleType(), 3), nBones))
    val parents = ParamDef(ArrayType(DoubleType(), 4))
    val weights = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(isMirror, CLambda(poseParams, CLambda(baseRelatives, CLambda(parents, CLambda(inverseBaseRelatives, CLambda(basePositions, CLambda(basePositionsHomg, CLambda(weights,
      GetSkinnedVertexPositions(ParamUse(isMirror), nBones,ParamUse(poseParams), ParamUse(baseRelatives), ParamUse(parents), ParamUse(inverseBaseRelatives), ParamUse(basePositions), ParamUse(basePositionsHomg), ParamUse(weights))))))))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, moreInclude = "#include \"get_posed_relatives.h\"\n")
    ccode.genTest("get_skinned_vertex_position")
    println(ccode)
  }

  @Test
  def arrayAccessLiftTest(): Unit = {
    val a = ParamDef(ArrayType(DoubleType(), 4))
    var tree : Expr = CLambda(a, Split(Map({
      val x = ParamDef(DoubleType())
      CLambda(x, Id(ParamUse(x)))
    }, ParamUse(a)), 2))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def blurForwardTest(): Unit = {
    val rows = 1000
    val cols = 1000
    val tree = DPSPass(blur_forward(rows, cols))
    val ccode = CCodeGen(tree)
    ccode.genTest("blur_forward")
    println(ccode)
  }

  @Test
  def blurBackwardTest(): Unit = {
    val rows = 1000
    val cols = 1000
    val tree = DPSPass(blur_backward(rows, cols))
    val ccode = CCodeGen(tree)
    ccode.genTest("blur_backward")
    println(ccode)
  }

  @Test
  def blurTest(): Unit = {
    val rows = 1000
    val cols = 1000
    var tree = DPSPass(blur_backward(rows, cols))
    var ccode = CCodeGen(tree)
    ccode.genTest("blur_backward")

    tree = DPSPass(blur_forward(rows, cols))
    ccode = CCodeGen(tree)
    ccode.genTest("blur_forward")
  }

  @Test
  def fftTest(): Unit = {
    val N = 1024;
    val tree = DPSPass(fft(N))
    var ccode = CCodeGen(tree, "#include \"reverse_bits.h\"\n")
    ccode.genTest("fft")
  }

  @Test
  def TestDriver(): Unit = {
    projectAndSqNormTest()
    GMMTest()
    angleAxisToRotationMatrixTest()
//    blurTest()
  }
}
