package core

import cGen.{AlgoOp, BinaryOp, Constant, DoubleType, DoubleTypeVar, ExternFunctionCall, IntType}
import org.junit.Test

class SpecialNodeKindTest {
  @Test
  def testConstantNodeKind(): Unit = {
    val expr1 = Constant(1.0, IntType(32))
    val expr2 = Constant(1.0, DoubleType())
    val expr3 = Constant(0.0, IntType(32))

    assert(expr1.nodeKind == expr1.nodeKind)
    assert(expr2.nodeKind == expr2.nodeKind)
    assert(expr3.nodeKind == expr3.nodeKind)

    assert(expr1.nodeKind == expr2.nodeKind)
    assert(expr1.nodeKind != expr3.nodeKind)
  }

  @Test
  def testBinaryOpNodeKind(): Unit = {
    val c1 = Constant(1.0, DoubleType())
    val c2 = Constant(0.0, DoubleType())
    val expr1 = BinaryOp(c1, c1, AlgoOp.Add)
    val expr2 = BinaryOp(c2, c2, AlgoOp.Add)
    val expr3 = BinaryOp(c1, c1, AlgoOp.Andand)
    val expr4 = BinaryOp(c1, c1, AlgoOp.Andand, isBool = true)

    assert(expr1.nodeKind == expr1.nodeKind)
    assert(expr2.nodeKind == expr2.nodeKind)
    assert(expr3.nodeKind == expr3.nodeKind)
    assert(expr4.nodeKind == expr4.nodeKind)

    assert(expr1.nodeKind == expr2.nodeKind)
    assert(expr1.nodeKind != expr3.nodeKind)
    assert(expr1.nodeKind != expr4.nodeKind)
    assert(expr3.nodeKind != expr4.nodeKind)
  }

  @Test
  def testExternFunctionCallNodeKind(): Unit = {
    val c1 = Constant(1.0, DoubleType())
    val c2 = Constant(1.0, DoubleType())
    val expr1 = ExternFunctionCall("f1", Seq(c1, c1), Seq(DoubleTypeVar(), DoubleTypeVar()), DoubleType())
    val expr2 = ExternFunctionCall("f1", Seq(c2, c2), Seq(DoubleTypeVar(), DoubleTypeVar()), DoubleType())
    val expr3 = ExternFunctionCall("f2", Seq(c1, c1), Seq(DoubleTypeVar(), DoubleTypeVar()), DoubleType())
    val expr4 = ExternFunctionCall("f1", Seq(c1), Seq(DoubleTypeVar()), DoubleType())

    assert(expr1.nodeKind == expr1.nodeKind)
    assert(expr2.nodeKind == expr2.nodeKind)
    assert(expr3.nodeKind == expr3.nodeKind)
    assert(expr4.nodeKind == expr4.nodeKind)

    assert(expr1.nodeKind == expr2.nodeKind)
    assert(expr1.nodeKind != expr3.nodeKind)
    assert(expr1.nodeKind != expr4.nodeKind)
    assert(expr3.nodeKind != expr4.nodeKind)
  }
}
