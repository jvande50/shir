package datareshaping

import algo.util.Matrix
import algo.{Input, MMMul}
import backend.hdl.HDLProject
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.tiling.{ABTilingRules, FoldTilingRules}
import backend.hdl.arch.rewrite.{FixTimingRules, InputBufferingRules, IntermediateBufferingRules, ParallelizeDotProductRules}
import backend.hdl.arch.tiling.{ParallelizationCompiler, TranspositionCompiler}
import core._
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted, Rules}
import org.junit.Test

class Expt4 {

 @Test
  def run(): Unit = {
    Counter.resetAll()
    val outerTile = 512
    val innerTile = 2048
    val imageSize = 4096
    val inputElementBitWidth = 8

    // all rewrite rules enabled for best performance!
    Rules.disabled = Seq.empty

    // the high-level algorithmic specification
    val alg =
      MMMul(
        Input("matrix1", imageSize, imageSize, algo.IntType(inputElementBitWidth)),
        Input("matrix2", imageSize, imageSize, algo.IntType(inputElementBitWidth))
      )

    HDLProject("expt4", alg, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(0, outerTile)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.exchangeABMapNoCondition(1)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(0, outerTile)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.exchangeABMapNoCondition(1)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(FoldTilingRules.tilingInsideFold(innerTile)))),

      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.all(Some(innerTile)))),
      (ParallelizationCompiler.phaseAfter, RewriteStep(RewriteAll(), IntermediateBufferingRules.get())),

      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), InputBufferingRules.readDoubleBuffering)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
    )).writeAllFiles({
      val matA = Matrix.fromRandom(imageSize, imageSize, 9, Some("A"))
      val matB = Matrix.fromRandom(imageSize, imageSize, 9, Some("B"))
      // reference CPU implementation to precompute expected result for comparison
      val matC = matA.tiledMul(matB, outerTile, cache = false)
      Predef.Map("matrix1" -> matA.data, "matrix2" -> matB.data, MemoryImage.RESULT_VAR_NAME -> matC.data)
    })
  }
}
