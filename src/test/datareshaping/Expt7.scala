package datareshaping

import algo.util.Matrix
import backend.hdl.HDLProject
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.conv.ParallelizeConvolutionRules
import backend.hdl.arch.rewrite.tiling.{ABTilingRules, TilingPreprocessingRules}
import backend.hdl.arch.rewrite.{FixTimingRules, InputBufferingRules, IntermediateBufferingRules, ParallelizeDotProductRules}
import backend.hdl.arch.tiling.{ParallelizationCompiler, TranspositionCompiler}
import backend.hdl.conv.ConvTemplate
import core._
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted, Rules}
import org.junit.Test

class Expt7 {

 @Test
  def run(): Unit = {
    Counter.resetAll()
    val inputWidth = 1024
    val inputHeight = 1024
    val inputChannel = 3
    val weightSize = 3
    val outputChannel =  64
    val tiledHeightNoPad = 128
    val tiledWidthNoPad = 128

    val elementBits = 8
    val inputPadding = 1

    val limitMulPerWindow = 27
    val limitMul = limitMulPerWindow * 32

    // manually disable rewrite rule optimizations to show their effect
    Rules.disabled = Seq(
      "moveUpVectorToStream_mapFission", "moveUpVectorToStream_mapFusion", "moveUpVectorToStream_convertMapStreamToMapVector", "moveUpVectorToStream_skipExprs",
      "moveUpVectorToStream_skipTuple", "moveUpVectorToStream_skipSplitStream", "moveUpVectorToStream_mergeWithStreamToVector", "moveUpVectorToStream_skipJoinStream",
      "moveUpVectorToStream_mapFusionMapParam", "removeConversion1", "removeConversion2",
      "moveUpJoinStream_mapFission", "moveUpJoinStream_skipExpr", "moveUpJoinStream_mapFusion", "moveUpJoinStream_skipSplit", "moveUpJoinStream_reorderJoins",
      "moveDownMapRepeatParamUse", "moveRepeatIntoLet", "moveMapRepeatIntoLet", "moveDownRepeat2", "moveDownRepeat3", "moveDownRepeat4", "reorderRepeats",
      "removeRepeatTupleType", "moveDownRepeatIntoReadAddress",
    )

    val conv = ConvTemplate.convHalfCachePaddingChannelMajor(inputWidth, inputHeight, inputChannel,
      weightSize, outputChannel, inputPadding, elementBits, elementBits)

    HDLProject("expt7", conv, CompilerPhase.first(), Seq(
      // Tiling
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(TilingPreprocessingRules.moveDownInnerMapBody))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(1, tiledHeightNoPad)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(1, tiledWidthNoPad)))),

      // Parallelize
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow("image", 1)))),
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("weight", 1)))),
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.all(Some(limitMulPerWindow)))),
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeConvolutionRules.all(Some(limitMul)))),

      // Memory and timing fix
      (ParallelizationCompiler.phaseAfter, RewriteStep(RewriteAll(), IntermediateBufferingRules.get())),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), InputBufferingRules.readDoubleBuffering)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
    )).writeAllFiles({
      val imageOrig = Matrix.fromRandom(inputHeight, inputWidth * inputChannel, 9, Some("A"))
      val weightOrig = Matrix.fromRandom(outputChannel, weightSize * weightSize * inputChannel, 9, Some("B"))
      val imagePad = imageOrig.pad(1, 1 ,inputChannel, inputChannel)
      // reference CPU implementation to precompute expected result for comparison
      val output = imagePad.conv(weightOrig, inputChannel).mod(256)
      // Padding for cacheline alignment
      val cacheLineSize = 64
      val imagePadCache = imagePad.pad(0, 0 , cacheLineSize / 2 - inputChannel % (cacheLineSize / 2), cacheLineSize / 2 - inputChannel % (cacheLineSize / 2))
      val weightPadCache = weightOrig.pad(0, 0, 0, cacheLineSize - (weightSize * weightSize * inputChannel) % cacheLineSize)
      Predef.Map("image" -> imagePadCache.data, "weight" -> weightPadCache.data, MemoryImage.RESULT_VAR_NAME -> output.data)
    })
  }
}
