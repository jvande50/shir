package eqsat

import cGen._
import core._
import org.junit.Test

class ExplanationTest {
  @Test
  def explainDirectAddition(): Unit = {
    val graph = new Willsey2021EGraph(enableExplanations = true, checkConsistency = true)
    val expr = BinaryOp(Constant(0.0, DoubleType()), Constant(1.0, DoubleType()), AlgoOp.Add)
    graph.add(expr)
    val explanation = graph.explainer.get.explainExistence(expr)
    assert(explanation.steps.length == 2)
    assert(explanation.steps.head.toString == "0.0 + 1.0")
    assert(explanation.steps(1).toString == "0.0 + 1.0")
  }

  @Test
  def explainSingleRuleApplication(): Unit = {
    val graph = new Willsey2021EGraph(enableExplanations = true, checkConsistency = true)
    val expr = BinaryOp(Constant(0.0, DoubleType()), Constant(1.0, DoubleType()), AlgoOp.Add)
    val c = graph.add(expr)
    graph.rebuild()
    graph.saturate(Seq(
      {
        val x = ExprVar(DoubleType())
        Rewrite(
          "add-zero",
          BinaryOp(Constant(0.0, DoubleType()), x, AlgoOp.Add),
          x)
      }
    ))
    val explanation = graph.explainer.get.explainEquivalence(expr, c.toSmallestExpr)
    assert(explanation.steps.length == 2)
    assert(explanation.steps.head.toString == "0.0 + 1.0")
    assert(explanation.steps(1).toString == "[Rewrite=> add-zero] 1.0")
  }

  @Test
  def eggExample1(): Unit = {
    val graph = new DeferredMergingEGraph(new Willsey2021EGraph(enableExplanations = true))

    def mul(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Mul))
    }

    def div(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Div))
    }

    val one = Constant(1.0, DoubleType())

    // Input expr: (/ (* (/ 2 3) (/ 3 2)) 1)
    val expr = div(
      mul(
        div(
          Constant(2.0, DoubleType()),
          Constant(3.0, DoubleType())),
        div(
          Constant(3.0, DoubleType()),
          Constant(2.0, DoubleType()))),
      one)

    assert(expr == TypeChecker.check(expr))

    val c = graph.add(expr)
    graph.rebuild()

    // Rules:
    // let rules: &[Rewrite<SymbolLang, ()>] = &[
    //    rw!("div-one"; "?x" => "(/ ?x 1)"),
    //    rw!("unsafe-invert-division"; "(/ ?a ?b)" => "(/ 1 (/ ?b ?a))"),
    //    rw!("simplify-frac"; "(/ ?a (/ ?b ?c))" => "(/ (* ?a ?c) (* (/ ?b ?c) ?c))"),
    //    rw!("cancel-denominator"; "(* (/ ?a ?b) ?b)" => "?a"),
    //    rw!("times-zero"; "(* ?a 0)" => "0"),
    //];
    graph.saturate(Seq(
      {
        val x = ExprVar(DoubleType())
        Rewrite(
          "div-one",
          x,
          div(x, one))
      },
      {
        val a = ExprVar(DoubleType())
        val b = ExprVar(DoubleType())
        Rewrite(
          "unsafe-invert-division",
          div(a, b),
          div(one, div(b, a)))
      },
      {
        val a = ExprVar(DoubleType())
        val b = ExprVar(DoubleType())
        Rewrite(
          "simplify-frac",
          div(a, div(b, c)),
          div(mul(a, c), mul(div(b, c), c)))
      },
      {
        val a = ExprVar(DoubleType())
        val b = ExprVar(DoubleType())
        Rewrite(
          "cancel-denominator",
          mul(div(a, b), b),
          a)
      },
      {
        val a = ExprVar(DoubleType())
        Rewrite(
          "times-zero",
          BinaryOp(a, Constant(0.0, DoubleType()), AlgoOp.Mul),
          Constant(0.0, DoubleType()))
      }
    ))

    // Check the tree-form explanation.
    val explanation = graph.explainer.get.explainEquivalence(expr, c.toSmallestExpr)
    assert(explanation.steps.length == 4)
    assert(explanation.steps.head.toString == "((2.0 / 3.0) * (3.0 / 2.0)) / 1.0")
    assert(explanation.steps(1).toString == "[Rewrite<= div-one] (2.0 / 3.0) * (3.0 / 2.0)")
    assert(explanation.steps(2).toString == "([Explanation] { (2.0 / 3.0) ([Rewrite=> unsafe-invert-division] 1.0 / (3.0 / 2.0)) }) * (3.0 / 2.0)")
    assert(explanation.steps(3).toString == "[Rewrite=> cancel-denominator] 1.0")

    // Flatten the explanation.
    val flatExplanation = explanation.flatten
    assert(flatExplanation.steps.map(_.toString) == Seq(
      "((2.0 / 3.0) * (3.0 / 2.0)) / 1.0",
      "[Rewrite=> div-one] (2.0 / 3.0) * (3.0 / 2.0)",
      "([Rewrite=> unsafe-invert-division] 1.0 / (3.0 / 2.0)) * (3.0 / 2.0)",
      "[Rewrite=> cancel-denominator] 1.0"))
  }

  @Test
  def eggExample2(): Unit = {
    val a = ParamUse(ParamDef(DoubleType()))
    val zero = Constant(0.0, DoubleType())
    val one = Constant(1.0, DoubleType())
    val two = Constant(2.0, DoubleType())

    def add(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Add))
    }

    def sub(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Sub))
    }

    def mul(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Mul))
    }

    // Expression: (+ 1 (- a (* (- 2 1) a)))
    val expr = add(one, sub(a, mul(sub(two, one), a)))

    // Rules:
    //   constant-fold-1: 2 - 1 => 1
    //   constant-fold-2: 1 + 0 => 1
    //   comm-mul: x * y => y * x
    //   mul-one: x => x * 1
    //   cancel-sub: x - x => 0
    val rules = Seq(
      Rewrite(
        "constant-fold-1",
        sub(two, one),
        one),
      Rewrite(
        "constant-fold-2",
        add(one, zero),
        one),
      {
        val x = ExprVar(DoubleType())
        val y = ExprVar(DoubleType())
        Rewrite(
          "comm-mul",
          mul(x, y),
          mul(y, x))
      },
      {
        val x = ExprVar(DoubleType())
        Rewrite(
          "mul-one",
          x,
          mul(x, one))
      },
      {
        val x = ExprVar(DoubleType())
        Rewrite(
          "cancel-sub",
          sub(x, x),
          zero)
      })

    val graph = new DeferredMergingEGraph(new Willsey2021EGraph(enableExplanations = true, checkConsistency = true))
    val c = graph.add(expr)
    graph.saturate(rules)

    assert(c.toSmallestExpr == one)

    // We expect the explanation below for 1.0 + (a - ((2.0 - 1.0) * a)) = 1.
    //
    // TreeExplanation(ArrayBuffer(
    //   1.0 + (a - ((2.0 - 1.0) * a)),
    //   1.0 + ([Explanation] {
    //     (a - ((2.0 - 1.0) * a))
    //     (([Explanation] {
    //       a
    //       ([Rewrite=> mul-one] a * 1.0)
    //       (a * ([Explanation] {
    //         1.0
    //         ([Rewrite<= constant-fold-1] 2.0 - 1.0)
    //       }))
    //       ([Rewrite<= comm-mul] (2.0 - 1.0) * a)
    //     }) - ((2.0 - 1.0) * a))
    //     ([Rewrite=> cancel-sub] 0.0)
    //   }),
    //   [Rewrite=> constant-fold-2] 1.0))
    val explanation = graph.explainer.get.explainEquivalence(expr, c.toSmallestExpr)
    assert(explanation.steps.length == 3)
    // TODO: check remainder of tree
    assert(explanation.steps.last.toString == "[Rewrite=> constant-fold-2] 1.0")
  }

  @Test
  def explainBinaryOpExistence(): Unit = {
    def add(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Add))
    }

    def mul(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Mul))
    }

    def sub(a: Expr, b: Expr): Expr = {
      TypeChecker.check(BinaryOp(a, b, AlgoOp.Sub))
    }

    val graph = new Willsey2021EGraph(enableExplanations = true, checkConsistency = true)
    val zero = Constant(0.0, DoubleType())
    val two = Constant(2.0, DoubleType())
    graph.add(sub(two, zero))
    graph.rebuild()
    graph.saturate(Seq(
      {
        val x = ExprVar(DoubleType())
        Rewrite(
          "add-zero",
          x,
          add(zero, x))
      },
      {
        val x = ExprVar(DoubleType())
        Rewrite(
          "add-to-mul",
          add(x, x),
          mul(two, x))
      }
    ))

    val flatExplanation = graph.explainer.get.explainExistence(mul(two, zero)).flatten
    assert(flatExplanation.steps.map(_.toString) == Seq(
      "2.0 - 0.0",
      "2.0 - ([Rewrite=> add-zero] 0.0 + 0.0)",
      "[Rewrite<= add-to-mul] 2.0 * 0.0")) // FIXME: Are we sure this last Rewrite<= is correct?

    val flatExplanation2 = graph.explainer.get.explainExistence(sub(two, mul(two, zero))).flatten
    assert(flatExplanation2.steps.map(_.toString) == Seq(
      "2.0 - 0.0",
      "2.0 - ([Rewrite=> add-zero] 0.0 + 0.0)",
      "2.0 - ([Rewrite<= add-to-mul] 2.0 * 0.0)")) // FIXME: Are we sure this last Rewrite<= is correct?

    // val flatExplanation3 = graph.explainer.get.explainExistence(mul(two, mul(two, zero))).flatten
    // println(flatExplanation3)
  }
}
