package eqsat

import algo.eqsat.OutliningRewriteRules
import algo._
import backend.hdl.HDLProject
import backend.hdl.arch.costModel.CostModel.estimateDSPBlocks
import cGen.{AlgoOp, BinaryOp, Constant, DoubleType}
import core._
import org.junit.Test

class OutliningExtractorTest {
  val solverExtractor: SolverExtractor = {
    SolverExtractor({
      case algo.Map(_, _, _) | algo.Zip(_, _, _) | algo.Fold(_, _, _) => 10
      case _ => 1
    }, !_.expr.isInstanceOf[DeBruijnShiftExpr])
  }

  @Test
  def canOutlineConstantMul(): Unit = {
    val t = DoubleType()
    val constMul = BinaryOp(Constant(1.0, t), Constant(42.0, t), AlgoOp.Mul)
    val expr = BinaryOp(constMul, constMul, AlgoOp.Add)

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(expr)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor)
    val outlined = extractor(root).get
    assert(ExprToText(outlined) == "(λdouble. ((p_0: double) + (p_0: double))) (1.0 * 42.0)")
  }

  @Test
  def canControlOutlining(): Unit = {
    val t = DoubleType()
    val constMul = BinaryOp(Constant(1.0, t), Constant(42.0, t), AlgoOp.Mul)
    val expr = BinaryOp(constMul, constMul, AlgoOp.Add)

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(expr)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor, _ => false)
    val outlined = extractor(root).get
    assert(ExprToText(outlined) == "(1.0 * 42.0) + (1.0 * 42.0)")
  }

  @Test
  def canOutlineOneArgMul(): Unit = {
    val t = DoubleType()
    val argMul = BinaryOp(DeBruijnParamUse(4, t), Constant(42.0, t), AlgoOp.Mul)
    val expr = BinaryOp(argMul, argMul, AlgoOp.Add)

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(expr)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor)
    val outlined = extractor(root).get
    assert(ExprToText(outlined) ==
      "(λdouble -> double. (((p_0: (double -> double)) (p_5: double)) + ((p_0: (double -> double)) (p_5: double)))) (λdouble. ((p_0: double) * 42.0))")
  }

  @Test
  def canOutlineTwoArgMul(): Unit = {
    val t = DoubleType()
    val argMul = BinaryOp(DeBruijnParamUse(4, t), DeBruijnParamUse(0, t), AlgoOp.Mul)
    val expr = BinaryOp(argMul, argMul, AlgoOp.Add)

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(expr)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor)
    val outlined = extractor(root).get
    assert(ExprToText(outlined) ==
      "(λdouble -> double -> double. ((((p_0: (double -> double -> double)) (p_1: double)) (p_5: double)) + (((p_0: (double -> double -> double)) (p_1: double)) (p_5: double)))) (λdouble. (λdouble. ((p_0: double) * (p_1: double))))")
  }

  @Test
  def canOutlineFunction(): Unit = {
    val t = DoubleType()
    val f = DeBruijnLambda(t, BinaryOp(DeBruijnParamUse(0, t), Constant(42, t), AlgoOp.Add))
    val expr = BinaryOp(
      FunctionCall(f, DeBruijnParamUse(1, t)),
      FunctionCall(f, DeBruijnParamUse(2, t)),
      AlgoOp.Add)

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(expr)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor)
    val outlined = extractor(root).get
    assert(ExprToText(outlined) ==
      "(λdouble -> double. (((p_0: (double -> double)) (p_2: double)) + ((p_0: (double -> double)) (p_3: double)))) (λdouble. ((p_0: double) + 42.0))")
  }

  @Test
  def canOutlineCommonArgUse(): Unit = {
    val t = DoubleType()
    val expr = BinaryOp(
      DeBruijnParamUse(0, t),
      DeBruijnParamUse(0, t),
      AlgoOp.Add)

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(expr)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor)
    assert(ExprToText(extractor(root).get) ==
      "(p_0: double) + (p_0: double)")

    val naiveExtractor = OutliningExtractor(
      ExtractionAnalysis.smallestExpressionExtractor.extractor,
      isWorthOutlining = _ => true)
    assert(ExprToText(naiveExtractor(root).get) ==
      "(λdouble -> double. (((p_0: (double -> double)) (p_1: double)) + ((p_0: (double -> double)) (p_1: double)))) (λdouble. (p_0: double))")
  }

  @Test
  def canOutlineRecursively(): Unit = {
    val t = DoubleType()
    val firstCommonExpr = Constant(42, t)
    val secondCommonExpr = BinaryOp(
      DeBruijnParamUse(0, t),
      firstCommonExpr,
      AlgoOp.Add)

    val expr = BinaryOp(
      firstCommonExpr,
      BinaryOp(
        secondCommonExpr,
        secondCommonExpr,
        AlgoOp.Mul),
      AlgoOp.Add)

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(expr)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor)
    assert(ExprToText(extractor(root).get) ==
      "(λdouble. ((λdouble -> double. ((p_1: double) + (((p_0: (double -> double)) (p_2: double)) * ((p_0: (double -> double)) (p_2: double))))) (λdouble. ((p_0: double) + (p_1: double))))) 42.0")
  }

  @Test
  def canFindSubtreeInTwoMMuls(): Unit = {
    val tree1 = DeBruijnTransform.forward(TypeChecker.check(MMMul(CounterInteger(1, 1, Seq(6, 6), None), CounterInteger(11, 1, Seq(6, 6), None))))
    val tree2 = DeBruijnTransform.forward(TypeChecker.check(MMMul(TypeChecker.check(Id(CounterInteger(1, 1, Seq(6, 6), None))), CounterInteger(11, 1, Seq(6, 6), None))))

    val combined = TypeChecker.check(Tuple(tree1, tree2))

    val eGraph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    val root = eGraph.add(combined)
    eGraph.rebuild()

    val extractor = OutliningExtractor(ExtractionAnalysis.smallestExpressionExtractor.extractor)
    assert(ExprToText(extractor(root).get) ==
      """(λ[i6 x 6] -> [i15 x 6].
        |  ((λ[[i6 x 6] x 6].
        |    (tuple
        |      (map (p_1: ([i6 x 6] -> [i15 x 6])) (p_0: [[i6 x 6] x 6]))
        |      (map (p_1: ([i6 x 6] -> [i15 x 6])) (id (p_0: [[i6 x 6] x 6])))))
        |    CounterIntegerExpr[Value[[[i6 x 6] x 6]];1;1;0;6;6;0;0]))
        |(λ[i6 x 6].
        |  (map (λ[i6 x 6].
        |    (fold (λi12. (λi15. (add (tuple (p_0: i15) (p_1: i12)))))
        |      (map (λ{i6, i6}. (mul (p_0: {i6, i6})))
        |        (zip (tuple (p_1: [i6 x 6]) (p_0: [i6 x 6]))))))
        |    CounterIntegerExpr[Value[[[i6 x 6] x 6]];11;1;0;6;6;0;0]))
        |""".stripMargin.split('\n').map(_.trim()).mkString(" "))
  }

  @Test
  def outlineAlternative3LayerMLP(): Unit = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val inputSize = 128
    val weight1Size = 128
    val weight2Size = 128
    val weight3Size = 128
    val input1 = algo.Input("input1", algo.IntType(8), inputSize)
    val weight1 = algo.Input("weight1", inputSize, weight1Size, algo.IntType(8))
    val weight2 = algo.Input("weight2", weight1Size, weight2Size, algo.IntType(8))
    val weight3 = algo.Input("weight3", weight2Size, weight3Size, algo.IntType(8))

    val tmpRes1 = TypeChecker.check(algo.MVMul(weight1, input1))
    val input2 = TypeChecker.check(algo.Map(
      {
        val param = ParamDef(tmpRes1.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes1))
    val tmpRes2 = TypeChecker.check(algo.MVMul(weight2, input2))
    val input3 = TypeChecker.check(algo.Map(
      {
        val param = ParamDef(tmpRes2.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes2))
    val tmpRes3 = TypeChecker.check(algo.MVMul(weight3, input3))
    val output = TypeChecker.check(algo.Map(
      {
        val param = ParamDef(tmpRes3.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes3))

    val transformed = TypeChecker.check(DeBruijnTransform.forward(output))

    println(ExprToText(transformed))
    println(s"DSP blocks: ${estimateDSPBlocks(transformed)}")
    println(s"Expr size: ${ExtractionAnalysis.smallestExpressionExtractor.cost(transformed)}")

    val extractorAnalysis = ExtractionAnalysis.smallestExpressionExtractor
    val extractor = extractorAnalysis.extractor
    val eGraph = EGraph(analyses = new AnalysesBuilder().add(extractorAnalysis).add(DeBruijnIndexAnalysis).toAnalyses)
    val root = eGraph.add(transformed)
    eGraph.rebuild()
    eGraph.saturate(OutliningRewriteRules(extractor).all)

    println(ExprToText(solverExtractor(root).get))

    val outliner = OutliningExtractor(solverExtractor)
    val outlined = outliner(root).get
    println(ExprToText(outlined))
    println(s"DSP blocks: ${estimateDSPBlocks(outlined)}")
    println(s"Expr size: ${ExtractionAnalysis.smallestExpressionExtractor.cost(outlined)}")
  }
}
