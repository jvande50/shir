package eqsat.nn

import backend.hdl.HDLProject
import backend.hdl.nn.conv.{SimpleVggConv3LayersTest, Vgg2LayerWithTilingTest}
import core.Counter
import org.junit.Test

class VggFirstTwoLayersTiled {
  @Test
  def outlineVgg(): Unit = {
    Counter.resetAll()
    val network = new Vgg2LayerWithTilingTest().vggFirstTwoLayersTiled
    //    println(network)
    val result = Helpers.outline(network, maxSteps = 14)
    HDLProject("vgg-first-two-layers-tiled", result).writeHDLFiles()
  }
}
