package eqsat.nn

import backend.hdl.HDLProject
import backend.hdl.nn.conv.SimpleConvOnlyTest
import core.Counter
import org.junit.Test

class TwoLayerConv {
  @Test
  def outlineTwoLayerConv(): Unit = {
    Counter.resetAll()
    val network = new SimpleConvOnlyTest().twoLayerConv
//    println(network)
    val result = Helpers.outline(network)
    HDLProject("two-layer-conv", result).writeHDLFiles()
  }
}
