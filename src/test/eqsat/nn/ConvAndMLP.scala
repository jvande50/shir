package eqsat.nn

import backend.hdl.HDLProject
import backend.hdl.nn.conv.SimpleConvTest
import core.Counter
import org.junit.Test

class ConvAndMLP {
  @Test
  def outlineConvAndMLP(): Unit = {
    Counter.resetAll()
    val network = new SimpleConvTest().layerConvMLP
    //    println(network)
    val result = Helpers.outline(network, maxSteps = 14)
    HDLProject("conv-and-mlp", result).writeHDLFiles()
  }
}
