package eqsat.nn

import algo.AlgoLambda
import algo.eqsat.OutliningRewriteRules
import backend.hdl.arch.costModel.CostModel.estimateDSPBlocks
import core.{Expr, LambdaT, ParamDef, TypeChecker}
import eqsat.{AnalysesBuilder, DeBruijnIndexAnalysis, DeBruijnShiftExpr, DeBruijnTransform, EGraph, ExprToText, ExtractionAnalysis, HierarchicalStopwatch, OutliningExtractor, SolverExtractor, TimingObserver}

object Helpers {
  val solverExtractor: SolverExtractor = {
    SolverExtractor({
      case algo.Map(_, _, _) | algo.Zip(_, _, _) | algo.Fold(_, _, _) => 10
      case _ => 1
    }, !_.expr.isInstanceOf[DeBruijnShiftExpr])
  }

  /**
   * Applies the equality saturation-based outliner to an algo expression.
   * @param expr An algo expression to transform.
   * @param debug When set to `true`, this method will print information that might be helpful for debugging purposes;
   *              otherwise, the method does not print any additional information.
   * @param maxSteps The maximum number of saturation steps.
   * @return An algo expression that has been sent through the equality saturation engine and outliner.
   */
  def outline(expr: Expr, debug: Boolean = true, maxSteps: Int = 100): Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val transformed = TypeChecker.check(DeBruijnTransform.forward(expr))

    if (debug) {
      println(ExprToText(transformed))
      println(s"DSP blocks: ${estimateDSPBlocks(transformed)}")
      println(s"Expr size: ${ExtractionAnalysis.smallestExpressionExtractor.cost(transformed)}")
    }

    val extractorAnalysis = ExtractionAnalysis.smallestExpressionExtractor
    val extractor = extractorAnalysis.extractor
    val observer = new TimingObserver()
    val eGraph = EGraph(
      analyses = new AnalysesBuilder().add(extractorAnalysis).add(DeBruijnIndexAnalysis).toAnalyses,
      observer = observer)
    val root = eGraph.add(transformed)
    eGraph.rebuild()
    eGraph.saturate(OutliningRewriteRules(extractor).all, _ >= maxSteps)

    if (debug) {
      println(s"Saturation took ${observer.totalTime.toSeconds}s")
      println(ExprToText(Helpers.solverExtractor(root).get))
    }

    val outliner = OutliningExtractor(Helpers.solverExtractor)
    val stopwatch = HierarchicalStopwatch.start("extraction")
    val outlined = outliner(root).get
    val extractionTime = stopwatch.complete()
    if (debug) {
      println(ExprToText(outlined))
      println(s"Extraction took ${extractionTime.duration.toSeconds}s")
      println(s"DSP blocks: ${estimateDSPBlocks(outlined)}")
      println(s"Expr size: ${ExtractionAnalysis.smallestExpressionExtractor.cost(outlined)}")
    }

    val result = TypeChecker.check(DeBruijnTransform.backward(outlined))

    if (debug) {
      println(ExprToText(result))
      println(result)
    }

    result
  }
}
