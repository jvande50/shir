package eqsat.nn

import backend.hdl.HDLProject
import backend.hdl.nn.conv.SimpleVggConv3LayersTest
import core.Counter
import org.junit.Test

class VggFirstThreeLayers {
  @Test
  def outlineVgg(): Unit = {
    Counter.resetAll()
    val network = new SimpleVggConv3LayersTest().vggFirstThreeLayers
    //    println(network)
    val result = Helpers.outline(network, maxSteps = 14)
    HDLProject("vgg-first-three-layers", result).writeHDLFiles()
  }
}
