package eqsat

import core._
import eqsat.explain.Annotation
import org.junit.Test

class DeBruijnFormattingTest {
  @Test
  def formatDeBruijnParamUse(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val p = DeBruijnParamUse(42, UnknownType)
    assert(formatter.format(p) == s"p_42: ${formatter.format(UnknownType)}")
  }

  @Test
  def formatDeBruijnLambda(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val p = DeBruijnParamUse(0, UnknownType)
    val lambda = DeBruijnLambda(UnknownType, p)
    assert(formatter.format(lambda) == s"λ${formatter.format(UnknownType)}. (p_0: ${formatter.format(UnknownType)})")
  }

  @Test
  def formatAnnotation(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val p1 = DeBruijnParamUse(0, UnknownType)
    val p2 = DeBruijnParamUse(0, UnknownType)
    val annotation1 = Annotation("first annotation", Seq(p1))
    val annotation2 = Annotation("second annotation", Seq(p1, p2))

    assert(formatter.format(annotation1) == s"[first annotation] ${formatter.formatNonAtomic(p1)}")
    assert(
      formatter.format(annotation2) ==
        s"[second annotation] { ${formatter.formatAtomic(p1)} ${formatter.formatAtomic(p2)} }")
  }
}
