package eqsat

import algo.eqsat.OutliningRewriteRules
import algo.{AlgoDataTypeVar, AlgoFunType, SeqType}
import core.{ArithTypeVar, ExprVar, FunTypeVar, FunctionCall, TypeChecker}
import org.junit.Test

class MapReduceRewriteTests {
  @Test
  def checkGenericizeMapWorks(): Unit = {
    val inputSize = 128
    val t = algo.IntType(8)
    val input = algo.Input("input", t, inputSize)

    val f = DeBruijnParamUse(0, AlgoFunType(t, t))

    val tree = TypeChecker.check(algo.Map(f, input))
    val transformed = TypeChecker.check(DeBruijnTransform.forward(tree))

    val extractorAnalysis = ExtractionAnalysis.smallestExpressionExtractor
    val extractor = extractorAnalysis.extractor
    val eGraph = EGraph(analyses = new AnalysesBuilder().add(extractorAnalysis).add(DeBruijnIndexAnalysis).toAnalyses)
    val root = eGraph.add(transformed)
    eGraph.rebuild()

    eGraph.saturate({
      val rules = OutliningRewriteRules(extractor)
      Seq(rules.expandShift, rules.hoistMapArg)
    })

    val expected = TypeChecker.check(
      FunctionCall(
        DeBruijnLambda(input.t, algo.Map(DeBruijnParamUse(1, AlgoFunType(t, t)), DeBruijnParamUse(0, input.t))),
        input))

    assert(Pattern(expected).searchEClass(eGraph)(root, HierarchicalStopwatch.disabled).length == 1)
  }
}
