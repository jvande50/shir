package algo

import core._
import core.util.IRDotGraph
import lift.arithmetic.simplifier.SimplifyIfThenElse
import org.junit.Test

class TypeCheckerTest {

  @Test()
  def testSolveChildType(): Unit = {
    val a = Tuple(ConstantInteger(1), ConstantInteger(2), ConstantInteger(3))
    val b = Add(a)  // we expect this to fail because Add acts on Tuple2's
    try {
      TypeChecker.check(b)
    } catch {
      case _: core.TypeCheckerException => () // good
      case _ => assert(false, "An type checker exception should have happened since it's ill-typed")
    }
  }

  @Test
  def testTupleTypingValid(): Unit = {
    // well-typed because the the NamedTupleType still has the same fields
    // (name matches type)
    val term = FunctionCall(
      {
        val p1 = ParamDef(NamedTupleType(collection.immutable.SortedMap(
          "t1" -> IntType(1),
          "t0" -> SeqType(IntType(1), 1),
        )))
        AlgoLambda(p1, ParamUse(p1))
      },
      Tuple2(ConstantSeq(Seq(1)), ConstantInteger(1))
    )

    val tree = TypeChecker.check(term)
    assert(!tree.hasUnknownType)
    assert(tree.t == TupleType(SeqType(IntType(1), 1), IntType(1)))
  }

  @Test
  def testTupleTypingInvalid(): Unit = {
    // ill-typed because the fields' name do not match
    val term = FunctionCall(
      {
        val p1 = ParamDef(NamedTupleType(collection.immutable.SortedMap(
          "p0" -> SeqType(IntType(1), 1),
          "p1" -> IntType(1),
        )))
        AlgoLambda(p1, ParamUse(p1))
      },
      Tuple2(ConstantSeq(Seq(1)), ConstantInteger(1))
    )

    try {
      TypeChecker.check(term)
    } catch {
      case _: core.TypeCheckerException => () // good
      case _ => assert(false, "An type checker exception should have happened since it's ill-typed")
    }
  }

  @Test
  def testLambdaPickFirst(): Unit = {
    val inputType1 = IntType(123)
    val inputType2 = IntType(2)
    val tree =
      {
        val p1 = ParamDef()
        val p2 = ParamDef()
        AlgoLambda(Seq(p1, p2), ParamUse(p1))
      }.call(Value(inputType1), Value(inputType2))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == inputType1)
  }

  @Test
  def testMapFixedTypes(): Unit = {
    val tree = Map(
      {
        val p = ParamDef(IntType(8))
        AlgoLambda(p, ParamUse(p))
      },
      Value(SeqType(IntType(8), 4))
    )
    val rebuiltTree = TypeChecker.check(tree)
    assert(rebuiltTree.t == SeqType(IntType(8), 4))
  }

  @Test
  def testMapId(): Unit = {
    val inputType = SeqType(IntType(16), ArithType(100))
    val tree =
      Map(
        {
          val dtv = AlgoDataTypeVar()
          BuiltinFunction(
            AlgoFunType(dtv, dtv)
          )
        },
        Value(inputType)
      )
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == inputType)
  }

  @Test
  def testMapId2(): Unit = {
    val inputType = SeqType(IntType(16), ArithType(100))
    val tree =
      Map(
        {
          val p = ParamDef()
          AlgoLambda(
            p,
            Id(ParamUse(p))
          )
        },
        Value(inputType)
      )
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == inputType)
  }

  @Test
  def testMapId3(): Unit = {
    val inputType = SeqType(IntType(13), ArithType(100))
    val tree =
      Map(
        Id.asFunction(),
        Value(inputType)
      )
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == inputType)
  }

  @Test
  def testZip(): Unit = {
    val tree =
      Zip2(Tuple2(
        Value(SeqType(IntType(10), 4)),
        Value(SeqType(IntType(12), 4))
      ))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == SeqType(TupleType(IntType(10), IntType(12)), 4))
  }

  @Test
  def testConstant(): Unit = {
    val tree =
      Repeat(
        ConstantInteger(4, Some(IntType(32))),
        ArithType(10)
      )
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == SeqType(IntType(32), 10))
  }

  @Test
  def testReduce(): Unit = {
    val tree =
      Reduce(
        Add2.asFunction(),
        ConstantInteger(7),
        Value(SeqType(IntType(16), ArithType(4)))
      )
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == IntType(18))
  }

  @Test
  def testInput(): Unit = {
    val tree = Input("var", IntType(123), 4)
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == SeqType(IntType(123), 4))
  }

  @Test
  def testDotProduct(): Unit = {
    val tree = DotProduct(Input("varA", IntType(16), 10), Input("varB", IntType(16), 10))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == IntType(36))
  }

  @Test
  def testMV(): Unit = {
    val tree = MVMul(Input("varA", IntType(16), 10, 3), Input("varB", IntType(16), 10))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == SeqType(IntType(36), 3))
  }

  @Test
  def testMM(): Unit = {
    val tree = MMMul(Input("varA", IntType(16), 10, 3), Input("varB", IntType(16), 10, 3))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == SeqType(SeqType(IntType(36), 3), 3))
  }

  @Test
  def testAddFun(): Unit = {
    val fun = Add.asFunction()
    val tree = fun.call(Tuple2(ConstantInteger(2), ConstantInteger(2)))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == IntType(3))
  }

  @Test
  def testReuseAddFun(): Unit = {
    val fun = Add.asTypeFunction()
    val tree = fun.call(Tuple2(Marker(fun.call(Tuple2(ConstantInteger(2), ConstantInteger(2))), TextType("marker")), ConstantInteger(4)))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == IntType(4))
  }

  @Test
  def test3xReuseInc(): Unit = {
    val fun = Inc.asTypeFunction()
    val tree = fun.call(fun.call(fun.call(ConstantInteger(1))))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == IntType(2))
  }

  @Test
  def testReuseTuple(): Unit = {
    val fun = Tuple2.asTypeFunction()
    val tree = fun.call(ConstantInteger(1), fun.call(ConstantInteger(8), ConstantInteger(128)))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == TupleType(IntType(1), TupleType(IntType(4), IntType(8))))
  }

  @Test
  def testTrunc(): Unit = {
    val tree = Map(TruncInteger.asFunction(Seq(None), Seq(ArithType(8))), Value(SeqType(IntType(20), 9)))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == SeqType(IntType(8), 9))
  }

  @Test
  def testConditionalType(): Unit = {
    val conditionalSubstitution: PartialFunction[Type, Type] = {
      case TupleType(Seq(it1: IntTypeT, it2: IntTypeT)) => IntType(it1.width.ae + 1)
      case TupleType(Seq(_: FloatSingleTypeT, _: FloatSingleTypeT)) => FloatSingleType()
    }
    val inputTv = TupleTypeVar(AbstractScalarTypeVar(), AbstractScalarTypeVar())
    val expr =
      FunctionCall(
        BuiltinFunction(FunType(inputTv, ConditionalTypeVar(inputTv, conditionalSubstitution))),
        Value(TupleType(FloatSingleType(), FloatSingleType()))
      )
    val rebuiltExpr = TypeChecker.check(expr)
    assert(rebuiltExpr.t == FloatSingleType())
  }

}
