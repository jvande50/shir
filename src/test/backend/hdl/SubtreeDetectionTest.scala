package backend.hdl

import algo.{Add, AlgoLambda, Buffer, ConstantInteger, CounterInteger, Id, Join, MMMul, Tuple}
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.rewrite.ParallelizeDotProductRules
import backend.hdl.arch.sharedFunc.SharedFuncBuilder.{createMultipleSharedFunc, createSharedFunc}
import core.commonSubtree.FunBuilder.{getFullyMatchInputs, insertSharedFunc}
import core.commonSubtree.SubtreeChecker.{TraverseScanTree, decomposeMaxSubtree, exprToString, getMaxSubtree, getSubtreeParams, isInvalidSubtree, scanSubtreePattern, selfScan}
import core.commonSubtree.SubtreeSelector.{getExprPairs, pairwiseOverlappingCheck, removeOverlapping}
import core.compile.CompilerPhase
import core.rewrite.{RewriteStep, RewriteTargeted}
import core.util.IRDotGraph
import core.{Counter, ParamDef, ParamUse, TypeChecker}
import org.junit.Test

class SubtreeDetectionTest {
  @Test
  def testGetSubtree1(): Unit = {
    // Find out a max common subtree from two separate expressions.
    Counter.resetAll()
    val tree1 = TypeChecker.check(MMMul(CounterInteger(1, 1, Seq(3, 3), None), CounterInteger(11, 1, Seq(3, 3), None)))
    val tree2 = TypeChecker.check(MMMul(TypeChecker.check(Id(CounterInteger(1, 1, Seq(6, 6), None))), CounterInteger(11, 1, Seq(6, 6), None)))
    val subtree = getMaxSubtree(tree1, tree2)
    // params = getSubtreeParams(subtree)
    //val newFun = insertLambdas(subtree, params)
    //IRDotGraph(subtree).show()
    print(subtree)
  }

  @Test
  def testGetSubtreeMapDifferent(): Unit = {
    // Find out a max common subtree from two separate expressions. Note that size info is ignored.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None)))
    val tree2 = TypeChecker.check(algo.Map({
      val param = ParamDef()
      AlgoLambda(
        param, Id(Id(ParamUse(param)))
      )
    }, CounterInteger(1, 1, Seq(3, 3), None)))
    val subtree = getMaxSubtree(tree1, tree2)
    val newtree = subtree match {
      case Some(e) => e
      case _ => ???
    }
    //val x = isInvalidSubtree(newtree)
    IRDotGraph(newtree).show()
    print(subtree)
  }

  @Test
  def testGetSubtreeMapSame(): Unit = {
    // Check if the subtree detector works with Maps.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None)))
    val tree2 = TypeChecker.check(algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None)))
    val subtree = getMaxSubtree(tree1, tree2)
    val newtree = subtree match {
      case Some(e) => e
      case _ => ???
    }
    //val x = isInvalidSubtree(newtree)
    IRDotGraph(newtree).show()
    print(subtree)
  }

  @Test
  def testGetSubtreeCheckSelf(): Unit = {
    // Note that there is no opportunity of sharing, so no common subtree is found.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None)))
    val subtree = getMaxSubtree(tree1, tree1)
    print(subtree)
  }

  @Test
  def testGetSubtreeNewScan(): Unit = {
    // Find out common subtrees by decomposing the max sub tree.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None)))
    val tree2 = TypeChecker.check(algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None)))
    val subtree = getMaxSubtree(tree1, tree2)
    val newtree = subtree match {
      case Some(e) => e
      case _ => ???
    }
    val trees = decomposeMaxSubtree(newtree)
    print(trees)
  }

  @Test
  def testGetSubtreeNewScan2(): Unit = {
    // Find out common subtrees by decomposing the max sub tree.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map(Id.asFunction(), algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None))))
    val tree2 = TypeChecker.check(algo.Map(Id.asFunction(), algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None))))
    val subtree = getMaxSubtree(tree1, tree2)
    val newtree = subtree match {
      case Some(e) => e
      case _ => ???
    }
    val trees = decomposeMaxSubtree(newtree)
    print(subtree)
  }

  @Test
  def scanSubtrees(): Unit = {
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map(Id.asFunction(), algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None))))
    val tree2 = TypeChecker.check(algo.Map(Id.asFunction(), algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None))))
    val subtrees = scanSubtreePattern(tree1, tree2)
    //val trees = decomposeMaxSubtree(newtree)
    print(subtrees)
  }

  @Test
  def testSelfScan(): Unit = {
    // Scan all subtree inside one expression.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map(Id.asFunction(), algo.Map(Id.asFunction(), CounterInteger(1, 1, Seq(3, 3), None))))
    val subtrees = selfScan(tree1)
    val overlap = scanSubtreePattern(subtrees.last, subtrees.head, true)
    val pairs = getExprPairs(subtrees)
    print(pairs)
  }

  @Test
  def testAddOneTwiceGetTopCandidate(): Unit = {
    // Scan all subtree inside one expression and get max sub tree candidate.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, CounterInteger(1, 1, Seq(10), None))))
    val subtrees = selfScan(tree1)
    val candidates = removeOverlapping(tree1, subtrees)
    print(candidates)
  }

  @Test
  def testAddOneTwiceBuildNewFunc(): Unit = {
    // Test shared function insertion.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, CounterInteger(1, 1, Seq(10), None))))
    val subtrees = selfScan(tree1)
    val candidates = removeOverlapping(tree1, subtrees)
    val params = getSubtreeParams(candidates.head)
    val test = insertSharedFunc(tree1, candidates.head, params)
    IRDotGraph(test).show()
  }

  @Test
  def testAddOneTwiceWithBufferOneSharedFunc(): Unit = {
    // Test shared function insertion. Manually fix call conflict to see if the circuit works.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, Buffer(algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, CounterInteger(1, 1, Seq(10), None)))))
    val sharedFuncExpr = createSharedFunc(tree1)

    HDLProject("mm", sharedFuncExpr, CompilerPhase.first(), Seq()).compileHDL
  }

  @Test
  def testAddOneTwiceWithBufferMultipleSharedFuncs(): Unit = {
    // Test shared function insertion. Note that multiple functions are inserted.
    Counter.resetAll()
    val tree1 = TypeChecker.check(algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, Buffer(algo.Map({
      val param = ParamDef()
      AlgoLambda(param, Add(Tuple(ParamUse(param), ConstantInteger(1))))
    }, CounterInteger(1, 1, Seq(10), None)))))
    val sharedFuncExpr = createMultipleSharedFunc(tree1, 2)

    HDLProject("mm", sharedFuncExpr, CompilerPhase.first(), Seq()).compileHDL
  }
}

