package backend.hdl.arch.sharedMM

import algo.util.Matrix
import backend.hdl.arch.mem.MemoryImage

object MMMulDataGen {
  def mul3Matrices(rows: Int, cols: Int, tmpBufName: String = "tmp"): Map[String, Seq[Seq[Int]]] = {
    val matA = Matrix.fromRandom(rows, cols, 9, Some("A"))
    val matB = Matrix.fromRandom(rows, cols, 9, Some("B"))
    val matT1 = matA.mul(matB).mod(256)
    val matC = Matrix.fromRandom(rows, cols, 9, Some("C"))
    val out = matT1.mul(matC).mod(256)
    Predef.Map(
      "matrix1" -> matA.data,
      "matrix2" -> matB.data,
      tmpBufName -> matT1.data,
      "matrix3" -> matC.data,
      MemoryImage.RESULT_VAR_NAME -> out.data
    )
  }
}
