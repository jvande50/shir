package backend.hdl.arch.sharedMM

import backend.hdl.arch.{Select2, Tuple}
import backend.hdl.arch.mem.StoreResult
import backend.hdl.arch.sharedFunc.FunCallConflictSolver.solveConflicts
import backend.hdl.arch.sharedMM.MMMulTemplate.createMMMulInput
import core.util.IRDotGraph
import core.{FunctionCall, Let, ParamDef, ParamUse, TypeChecker}
import org.junit.Test

class SharedMMTupleConflictTest {
  @Test
  def testMMMul(): Unit = {
    val matSize = 128

    val input1 = TypeChecker.check(createMMMulInput(matSize, 8, "matrix1", true))
    val input2 = TypeChecker.check(createMMMulInput(matSize, 8, "matrix2", false))
    val input3 = TypeChecker.check(createMMMulInput(matSize, 8, "matrix3", true))
    val input4 = TypeChecker.check(createMMMulInput(matSize, 8, "matrix4", false))

    val mf = MMMulTemplate.createMMMulFun(matSize, 8)
    val mfParam = ParamDef(mf.t)

    val mmmul = Let(
      mfParam,
      {
        val tmp1 = TypeChecker.check(FunctionCall(FunctionCall(ParamUse(mfParam), input1), input2))
        val tmp2 = TypeChecker.check(FunctionCall(FunctionCall(ParamUse(mfParam), input3), input4))
        Select2(Tuple(tmp1, tmp2), 1)
      },
      mf
    )
    val expr = TypeChecker.check(StoreResult(mmmul))
    val newExpr = TypeChecker.check(solveConflicts(expr))
  }
}
