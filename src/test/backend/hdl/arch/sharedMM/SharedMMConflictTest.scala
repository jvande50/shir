package backend.hdl.arch.sharedMM

import backend.hdl.HDLProject
import backend.hdl.arch.{ArchCompiler, MapCompiler, MapOrderedStream, OrderedStreamToVector, Repeat}
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, StoreResult}
import backend.hdl.arch.rewrite.{CleanupRules, FixTimingRules, InputBufferingRules, ParallelizeDotProductRules}
import backend.hdl.arch.sharedFunc.FunCallConflictSolver.solveConflicts
import backend.hdl.arch.sharedMM.MMMulDataGen.mul3Matrices
import backend.hdl.arch.sharedMM.MMMulTemplate.createMMMulInput
import core.{Counter, FunctionCall, Let, ParamDef, ParamUse, TypeChecker}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import org.junit.Test

class SharedMMConflictTest {
  @Test
  def testMMMul(): Unit = {
    Counter.resetAll()
    val matSize = 128
    val scale = matSize/64

    val input1 = TypeChecker.check(createMMMulInput(matSize, 8, "matrix1", true))
    val input2 = createMMMulInput(matSize, 8, "matrix2", false)
    val input3 = createMMMulInput(matSize, 8, "matrix3", false)

    val mf = MMMulTemplate.createMMMulFun(matSize, 8)
    val mfParam = ParamDef(mf.t)

    val mmmul = Let(
      mfParam,
      {
        val tmpRes = TypeChecker.check(FunctionCall(FunctionCall(ParamUse(mfParam), input1), input2))
        val tmp1 = TypeChecker.check(MapOrderedStream(Repeat.asFunction(Seq(None), Seq(matSize)), MapOrderedStream(OrderedStreamToVector.asFunction(), tmpRes)))
        FunctionCall(FunctionCall(ParamUse(mfParam), tmp1), input3)
      },
      mf
    )
    val expr = TypeChecker.check(StoreResult(mmmul))
    val newExpr = TypeChecker.check(solveConflicts(expr))

    HDLProject(this.getClass.getSimpleName, newExpr, CompilerPhase.first(),
      Seq(
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("matrix3", scale)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow("Inserted_1", scale)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("matrix2", scale)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow("matrix1", scale)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.all(Some(matSize)))),
        (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(2, 1, 0), Seq(InputBufferingRules.doubleBufferRead))),
        (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
        (MapCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      )
    ).simulateWithHostRam(mul3Matrices(matSize, matSize, "Inserted_1")).print()
  }
}
