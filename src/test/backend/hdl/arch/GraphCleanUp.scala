package backend.hdl.arch

import backend.hdl.IntType
import backend.hdl.graph.GraphGenerator
import backend.hdl.graph.util.DataFlowDotGraph
import core.{Counter, TypeChecker}
import org.junit.Test

class GraphCleanUp {
  @Test
  def cleanupTest(): Unit = {
    Counter.resetAll()
    val graph1 = GraphGenerator.generate(TypeChecker.check(Id(Id(ConstantValue(5, Some(IntType(10)))))))
    DataFlowDotGraph(graph1).show()
    val graph2 = GraphGenerator.generate(TypeChecker.check(Tuple2(Id(Id(ConstantValue(5, Some(IntType(10))))), ConstantValue(0, Some(IntType(1))))))
    DataFlowDotGraph(graph2).show()
    val graph3 = GraphGenerator.generate(TypeChecker.check(Id(Id(Tuple2(Id(Id(ConstantValue(5, Some(IntType(10))))), Id(ConstantValue(0, Some(IntType(1)))))))))
    DataFlowDotGraph(graph3).show()
    val graph4 = GraphGenerator.generate(TypeChecker.check(Id(Id(Repeat(Id(ConstantValue(5, Some(IntType(10)))), 4)))))
    DataFlowDotGraph(graph4).show()
  }
}