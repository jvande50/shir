package backend.hdl.arch

import core._
import org.junit.Test

class MapLoweringTest {

  @Test
  def test(): Unit = {
    Counter.resetAll()
    val tree =
      MapOrderedStream(
        {
          val p1 = ParamDef()
          ArchLambda(p1, MapOrderedStream(
            {
              val p2 = ParamDef()
              ArchLambda(p2, Tuple2(ParamUse(p1), ParamUse(p2)))
            },
            CounterInteger(0, 1, Seq(10))
          ))
        },
        CounterInteger(0, 1, Seq(10))
      )
    val newTree = MapCompiler.run(TypeChecker.check(tree))

    println(newTree)
  }
}
