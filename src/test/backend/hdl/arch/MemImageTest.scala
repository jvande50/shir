package backend.hdl.arch

import backend.hdl.arch.mem.MemoryImage
import backend.hdl
import backend.hdl.{IntType, OrderedStreamType, SignedIntType}
import org.junit.Test

class MemImageTest {

  @Test
  def memTest(): Unit = {
    println(MemoryImage.generateMemDataEntry(Seq(31, 31, 31), Seq(3), IntType(8), 8).map(_.mkString))

    println(MemoryImage.generateMemDataEntry(Seq(31, 31, 31), Seq(3), IntType(5), 8).map(_.mkString))

    println(MemoryImage.generateMemDataEntry(Seq(31, 31, 31), Seq(3), IntType(5), 16).map(_.mkString))

    println(MemoryImage.generateMemDataEntry(Seq(127, 0, -128), Seq(3), SignedIntType(8), 8).map(_.mkString))
  }

}
