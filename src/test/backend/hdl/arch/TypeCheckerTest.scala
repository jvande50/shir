package backend.hdl.arch

import backend.hdl.arch.mem.BufferStreamInBlockRam
import backend.hdl._
import core._
import core.util.IRDotGraph
import org.junit.Test

class TypeCheckerTest {

  @Test
  def testZip(): Unit = {
    val tree =
      Zip2OrderedStream(Tuple2(
        CounterInteger(0, 1, Seq(111)),
        CounterInteger(0, 1, Seq(111))
      ))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == OrderedStreamType(TupleType(IntType(7), IntType(7)), 111))
  }

  @Test
  def testMatrixZip(): Unit = {
    val tree =
      Zip2OrderedStream(Tuple2(
        Repeat(Repeat(Value(IntType(10)), 111), 222),
        Repeat(Value(IntType(8)), 222)
      ))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == OrderedStreamType(TupleType(OrderedStreamType(IntType(10), 111), IntType(8)), 222))
  }

  @Test
  def testSortingStream(): Unit = {
    val tree =
      UnorderedStreamToOrderedStream(
        Value(UnorderedStreamType(IntType(32), 10))
      )
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == OrderedStreamType(IntType(32), 10))
  }

  @Test
  def testBufferStream(): Unit = {
    val streamType =
      OrderedStreamType(
        UnorderedStreamType(
          OrderedStreamType(IntType(32), 2),
          4
        ),
        10
      )
    val tree = BufferStreamInBlockRam(Value(streamType))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
  }

  @Test
  def testTrunc(): Unit = {
    val input = Value(OrderedStreamType(IntType(20), 9))
    val tree = MapOrderedStream({
      val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
      ArchLambda(
        param,
        TruncInteger(ParamUse(param), 8)
      )
    }, input)
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == OrderedStreamType(IntType(8), 9))
  }

  @Test
  def testIsSubType: Unit = {
    val tv = AnyTypeVar()
    assert(tv.isSubTypeOf(tv) == Some(true))
  }

  @Test
  def testFunTypeAndHWFunType(): Unit = {
    val ft = FunType(LogicType(), LogicType())
    val hwft = HWFunType(LogicType(), LogicType())
    assert(hwft.isSubTypeOf(ft) == Some(true))
    assert(ft.isSubTypeOf(hwft) == Some(false))
    assert(hwft.isSubTypeOf(hwft) == Some(true))
    assert(ft.isSubTypeOf(ft) == Some(true))

    val ftv = FunTypeVar(LogicType(), LogicType())
    val ftv1 = FunTypeVar(LogicType(), LogicType())
    val ftv2 = FunTypeVar(LogicType(), LogicType())
    val hwftv = HWFunTypeVar(LogicType(), LogicType())
    val hwftv1 = HWFunTypeVar(LogicType(), LogicType())
    val hwftv2 = HWFunTypeVar(LogicType(), LogicType())
    assert(hwftv.isSubTypeOf(ftv) == None)
    assert(ftv.isSubTypeOf(hwft) == None)
    assert(ftv1.isSubTypeOf(ftv2) == None)
    assert(ftv2.isSubTypeOf(ftv1) == None)
    assert(hwftv1.isSubTypeOf(hwftv2) == None)
    assert(hwftv2.isSubTypeOf(hwftv1) == None)
  }

  @Test
  def testDoSome(): Unit = {
    val input = ConstantValue(0, Some(LogicType()))
    val param = ParamDef(LogicTypeVar())
    val tree = DoSome(ArchLambda(param, ParamUse(param)), input)
    //val tree = DoSome(Lambda(param, ParamUse(param), FunTypeVar()), input)

    // There must be a scala.MatchError because we do not have graph level implementation for DoSome!
    try{
      HDLProject("testDoSome", tree).compileHDL
    } catch {
      case e: scala.MatchError => print(e)
    }
  }

}
