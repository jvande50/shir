package backend.hdl.arch

import backend.hdl.{HDLProject, IntType, OrderedStreamTypeT}
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{BufferStream2DInBlockRam, BufferStreamInBlockRam, BufferStreamInHostRam, Input, MemFunctionsCompiler, StoreResult}
import backend.hdl.arch.rewrite.{CleanupRules, FixTimingRules, InputBufferingRules, ParallelizeDotProductRules}
import backend.hdl.arch.costModel.CostModel.{estimateDSPBlocks, estimateHostReadAccess, estimateHostReadCacheLines, estimateInputWiring}
import backend.hdl.arch.sharedMM.MMMulDataGen.mul3Matrices
import backend.hdl.arch.sharedMM.MMMulTemplate
import backend.hdl.arch.sharedMM.MMMulTemplate.createMMMulInput
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import core.util.IRDotGraph
import core.{Counter, FunctionCall, Let, ParamDef, ParamUse, TextType, TypeChecker}
import org.junit.Test

class CostModelTest {

  @Test
  def testReadAccess(): Unit = {
    val input = Input("test", 128, 128, IntType(8))
    val repeatInput = TypeChecker.check(Repeat(input, 8))
    val mapRepeat_Input = TypeChecker.check(MapOrderedStream(Repeat.asFunction(Seq(None), Seq(8)), input))
    val repeatBufferInput = TypeChecker.check(Repeat(BufferStream2DInBlockRam(input), 8))
    val mapRepeat_BufferInput = TypeChecker.check(MapOrderedStream(Repeat.asFunction(Seq(None), Seq(8)), BufferStream2DInBlockRam(input)))
    val repeatMapBuffer_Input = TypeChecker.check(Repeat(MapOrderedStream({
      val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
      ArchLambda(
        param,
        BufferStreamInBlockRam(ParamUse(param))
      )
    }, input), 8))

    val repeatMapBuffer_BufferInput = TypeChecker.check(Repeat(MapOrderedStream({
      val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
      ArchLambda(
        param,
        BufferStreamInBlockRam(ParamUse(param))
      )
    }, BufferStream2DInBlockRam(input)), 8))

    print("Vanilla Read Access estimation: " + estimateHostReadCacheLines(input) + "\n")
    print("Repeat Read Access estimation: " + estimateHostReadCacheLines(repeatInput) + "\n")
    print("Map-Repeat Read Access estimation: " + estimateHostReadCacheLines(mapRepeat_Input) + "\n")
    print("Repeat on Block RAM estimation: " + estimateHostReadCacheLines(repeatBufferInput) + "\n")
    print("Map Repeat on Block RAM estimation: " + estimateHostReadCacheLines(mapRepeat_BufferInput) + "\n")
    print("Repeat with Block RAM inside Map estimation: " + estimateHostReadCacheLines(repeatMapBuffer_Input) + "\n")
    print("Repeat with Block RAM inside Map with buffered input estimation: " + estimateHostReadCacheLines(repeatMapBuffer_BufferInput) + "\n")
  }

  @Test
  def testMMMul(): Unit = {
    Counter.resetAll()
    Counter.resetAll()
    val matSize = 128
    val scale = matSize / 64

    val input1 = createMMMulInput(matSize, 8, "matrix1", true)
    val input2 = createMMMulInput(matSize, 8, "matrix2", false)
    val input3 = createMMMulInput(matSize, 8, "matrix3", false)

    val mf = MMMulTemplate.createMMMulFun(matSize, 8)
    val mfParam = ParamDef(mf.t)

    val mmmul = Let(
      mfParam,
      {
        val tmpRes = TypeChecker.check(FunctionCall(FunctionCall(ParamUse(mfParam), input1), input2))
        val bufferTmpRes = TypeChecker.check(BufferStreamInHostRam(tmpRes, TextType("tmp")))
        val tmp1 = TypeChecker.check(MapOrderedStream(Repeat.asFunction(Seq(None), Seq(matSize)), MapOrderedStream(OrderedStreamToVector.asFunction(), bufferTmpRes)))
        FunctionCall(FunctionCall(ParamUse(mfParam), tmp1), input3)
      },
      mf
    )
    val tree = StoreResult(mmmul)

    print("DSP estimation: " + estimateDSPBlocks(tree) + "\n")
    print("Wiring estimation: " + estimateInputWiring(tree) + "\n")
    print("Read Access estimation: " + estimateHostReadAccess(tree) + "\n")
  }

}
