package backend.hdl.arch

import backend.hdl._
import core.{ArithTypeT, TypeChecker}
import org.junit.Test

class ConversionTest {

  @Test
  def testPrecisionDecrease(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(10), Some(IntType(512))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(IntType(256), 20))
    TypeChecker.check(conversion)
  }

  @Test
  def testPrecisionIncrease(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(10), Some(IntType(256))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(IntType(512), 10))
    TypeChecker.check(conversion)
  }

  @Test
  def testShortStream(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(1), Some(IntType(32))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 512), 1))
    TypeChecker.check(conversion)
  }

  @Test
  def testLongStream(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(1), Some(IntType(1024))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 512), 2))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream1(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(3), Some(IntType(200))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 512), 2))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream2(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(2), Some(IntType(512))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 200), 3))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream3(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 0, Seq(171), Some(IntType(3))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 512), 2))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream4(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(2), Some(IntType(512))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 3), 171))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream5(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(1), Some(IntType(800))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 512), 2))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream6(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(2), Some(IntType(512))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 800), 1))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream7(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(6), Some(IntType(200))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 512), 3))
    TypeChecker.check(conversion)
  }

  @Test
  def testNonPowerOfTwoPrecisionStream8(): Unit = {
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(3), Some(IntType(512))))
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 200), 6))
    TypeChecker.check(conversion)
  }

  @Test
  def testTuple(): Unit = {
    val input = TypeChecker.check(
      Tuple2(
        ConstantValue(0, Some(IntType(128))),
        ConstantValue(0, Some(IntType(128)))
      )
    )
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 32), 8))
    TypeChecker.check(conversion)
  }

  @Test
  def testTupleStream(): Unit = {
    val input = TypeChecker.check(
      Zip2OrderedStream(Tuple2(
        CounterInteger(0, 1, Seq(3), Some(IntType(12))),
        CounterInteger(0, 1, Seq(3), Some(IntType(9)))
      ))
    )
    val conversion = ArchConversion(input, input.t, OrderedStreamType(VectorType(LogicType(), 256), 3))
    TypeChecker.check(conversion)
  }

  @Test
  def testCalcLength(): Unit = {
    val elementType = IntType(512)
    var len: ArithTypeT = 0
    len = ArchConversion.calcLength(OrderedStreamType(IntType(64), 7), elementType)
    assert(len.ae.evalInt == 1)
    len = ArchConversion.calcLength(OrderedStreamType(IntType(64), 8), elementType)
    assert(len.ae.evalInt == 1)
    len = ArchConversion.calcLength(OrderedStreamType(IntType(64), 9), elementType)
    assert(len.ae.evalInt == 2)
    len = ArchConversion.calcLength(OrderedStreamType(IntType(64), 15), elementType)
    assert(len.ae.evalInt == 2)
    len = ArchConversion.calcLength(OrderedStreamType(IntType(64), 16), elementType)
    assert(len.ae.evalInt == 2)
    len = ArchConversion.calcLength(OrderedStreamType(IntType(64), 17), elementType)
    assert(len.ae.evalInt == 3)

    len = ArchConversion.calcLength(OrderedStreamType(IntType(1), 1), elementType)
    assert(len.ae.evalInt == 1)

    len = ArchConversion.calcLength(OrderedStreamType(IntType(200), 3), elementType)
    assert(len.ae.evalInt == 2)
    len = ArchConversion.calcLength(OrderedStreamType(IntType(200), 9), elementType)
    assert(len.ae.evalInt == 5)

    len = ArchConversion.calcLength(OrderedStreamType(IntType(1000), 1), elementType)
    assert(len.ae.evalInt == 2)
    len = ArchConversion.calcLength(OrderedStreamType(IntType(1000), 3), elementType)
    assert(len.ae.evalInt == 6)
  }

  @Test
  def testCalcLengthNested(): Unit = {
    val elementType = IntType(512)
    var len: ArithTypeT = 0
    len = ArchConversion.calcLength(OrderedStreamType(OrderedStreamType(IntType(1), 8), 1), elementType)
    assert(len.ae.evalInt == 1)

    len = ArchConversion.calcLength(OrderedStreamType(OrderedStreamType(OrderedStreamType(OrderedStreamType(IntType(64), 8), 1), 1), 1), elementType)
    assert(len.ae.evalInt == 1)
    len = ArchConversion.calcLength(OrderedStreamType(OrderedStreamType(OrderedStreamType(OrderedStreamType(IntType(64), 8), 2), 2), 2), elementType)
    assert(len.ae.evalInt == 8)

    len = ArchConversion.calcLength(OrderedStreamType(OrderedStreamType(OrderedStreamType(OrderedStreamType(IntType(200), 1), 2), 2), 2), elementType)
    assert(len.ae.evalInt == 4)
  }
}
