package backend.hdl

import algo.util.Matrix
import algo.{AlgoLambda, Concat, ConstantInteger, CounterInteger, DotProduct, FlipTuple2, Id, Input, Join, Repeat, Select2, SeqType, SeqTypeT, Slide, Split, TransposeND, Tuple2, Zip2}
import backend.hdl.arch.mem.MemoryImage
import backend.hdl.arch.rewrite.conv.ParallelizeConvolutionRules
import backend.hdl.arch.rewrite.tiling.ABTilingRules
import backend.hdl.arch.rewrite.transpose.{MoveUpTransposeRules, TransposeConversionRules}
import backend.hdl.arch.rewrite.{CleanupRules, MoveDownConcatRules, MoveUpJoinStreamRules, ParallelizeDotProductRules}
import backend.hdl.arch.{ArchCompiler, PerformanceEstimate, PerformanceModel}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteOptimised, RewriteRandom, RewriteStep, RewriteTargeted, Rules}
import core.{ArithType, Counter, ParamDef, ParamUse, TypeChecker}
import org.junit.Test

class RewriteTest {

  @Test
  def testDP_rewrite_parallel(): Unit = {
    Counter.resetAll()
    val alg =
      DotProduct(
        ParamDef(SeqType(algo.IntType(16), ArithType(8))),
        ParamDef(SeqType(algo.IntType(16), ArithType(8)))
      )
    HDLProject("mm", alg, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), ParallelizeDotProductRules.get(Some(2))))
    )).compileHDL
  }

  @Test
  def testConv1D_rewrite_parallel(): Unit = {
    Counter.resetAll()

    Rules.disabled = Seq("moveDownSlideVector_skipStreamToVector")

    val input = CounterInteger(0, 1, 10)
    val weight = CounterInteger(1, 1, 3)
    val slidedInput = Slide(input, 3)
    val slidedInputtc = TypeChecker.checkIfUnknown(slidedInput, slidedInput.t)
    val alg = algo.Map(
      {
        val param = ParamDef(slidedInputtc.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          TypeChecker.check(DotProduct(ParamUse(param), weight))
        )
      }, slidedInputtc
    )
    HDLProject("conv1d", alg, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeConvolutionRules.get(Some(12)))),
    )).compileHDL
  }

  @Test
  def testTilingInner(): Unit = {
    Counter.resetAll()
    val inputInner = Input("inputInner", algo.IntType(8), 6)
    val inputOuter = Input("inputOuter", algo.IntType(8), 6)
    val alg = algo.Map(
      {
        val param1 = ParamDef()
        AlgoLambda(
          param1,
          algo.Map(
            {
              val param2 = ParamDef()
              AlgoLambda(
                param2,
                Select2(Id(Tuple2(ParamUse(param1), ParamUse(param2))), 1)
              )
            }, inputInner
          )
        )
      }, inputOuter
    )
    HDLProject("tilingInner", alg, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(0, 3)))),
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(TransposeConversionRules.cleanTranspose))),
    )).simulateWithHostRam(
      Predef.Map(
        "inputInner" -> Seq(0, 1, 2, 3, 4, 5),
        "inputOuter" -> Seq(10, 11, 12, 13, 14, 15),
        MemoryImage.RESULT_VAR_NAME -> Seq.fill(6)(Seq(0, 1, 2, 3, 4, 5)),
      )
    ).print()
  }

  @Test
  def testTilingOuter(): Unit = {
    Counter.resetAll()
    val inputInner = Input("inputInner", algo.IntType(8), 6)
    val inputOuter = Input("inputOuter", algo.IntType(8), 6)
    val alg = algo.Map(
      {
        val param1 = ParamDef()
        AlgoLambda(
          param1,
          algo.Map(
            {
              val param2 = ParamDef()
              AlgoLambda(
                param2,
                Select2(Id(Tuple2(ParamUse(param1), ParamUse(param2))), 1)
              )
            }, inputInner
          )
        )
      }, inputOuter
    )
    HDLProject("tilingOuter", alg, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.exchangeABMapNoCondition(0)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(0, 3)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.exchangeABMapNoCondition(0)))),
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), TransposeConversionRules.get() ++ Seq(TransposeConversionRules.convertTranspose2DIntoPermuteVec))),
    )).simulateWithHostRam(
      Predef.Map(
        "inputInner" -> Seq(0, 1, 2, 3, 4, 5),
        "inputOuter" -> Seq(10, 11, 12, 13, 14, 15),
        MemoryImage.RESULT_VAR_NAME -> Seq.fill(6)(Seq(0, 1, 2, 3, 4, 5)),
      )
    ).print()
  }

  def testMergeMapMapRepeat(): Unit = {
    Counter.resetAll()
    val alg = algo.Map(
      {
        val param = ParamDef()
        AlgoLambda(
          param,
          Repeat(Repeat(ParamUse(param), 3), 2)
        )
      }, CounterInteger(0, 1, Seq(5), None)
    )
    HDLProject("mapMapRepeat", alg).compileHDL
  }

  @Test
  def testRemoveTransposeWithRealignment(): Unit = {
    Counter.resetAll()
    val input = algo.Map(Join.asFunction(), TransposeND(Split(Input("input", 4, 16, algo.IntType(10)), 4), Seq(0, 2, 1)))

    HDLProject("realignTranspose", input, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(CleanupRules.moveUpMapOrderedToUnorderedStream, CleanupRules.mapFissionMapStmFun))),
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), MoveUpJoinStreamRules.get())),
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(MoveUpTransposeRules.skipMapNDFun)))
    )).compileHDL
  }

  @Test
  def testMapBodySplitSplit(): Unit = {
    Counter.resetAll()
    val input = TypeChecker.check(algo.Map(Zip2.asFunction(), Zip2(Tuple2(CounterInteger(0, 1, Seq(8, 8, 8), None), CounterInteger(0, 1, Seq(8, 8), None)))))
    val alg = algo.Map(
      2,
      {
        val param = ParamDef(input.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          Tuple2(Select2(ParamUse(param), 0), Repeat(Select2(ParamUse(param), 1), 8))
        )
      }, input
    )
    HDLProject("testZipTuple", alg).compileHDL
  }

  @Test
  def testMapBodySplitSplit2(): Unit = {
    Counter.resetAll()
    val alg = algo.Map(
      {
        val param = ParamDef()
        AlgoLambda(
          param,
          Id(algo.Map(Split.asFunction(Seq(None), Seq(2)), Split(ParamUse(param), 2)))
        )
      }, CounterInteger(0, 1, Seq(8, 8), None)
    )
    HDLProject("mapSplitSplit2", alg).compileHDL
  }

  @Test
  def testMapBodySplitSplit3(): Unit = {
    Counter.resetAll()
    val alg = algo.Map(
      {
        val param = ParamDef()
        AlgoLambda(
          param,
          Id(algo.Map(2, Split.asFunction(Seq(None), Seq(2)), algo.Map(Split.asFunction(Seq(None), Seq(2)), Split(ParamUse(param), 2))))
        )
      }, Id(CounterInteger(0, 1, Seq(8, 8), None))
    )
    HDLProject("mapSplitSplit3", alg).compileHDL
  }

  @Test
  def testZipTuple2(): Unit = {
    Counter.resetAll()
    val alg = algo.Map(
      {
        val param1 = ParamDef()
        AlgoLambda(
          param1,
          algo.Map(
            {
              val param2 = ParamDef()
              AlgoLambda(
                param2,
                Tuple2(Select2(ParamUse(param2), 0), Repeat(Select2(ParamUse(param2), 1), 8))
              )
            }, Zip2(ParamUse(param1))
          )
        )
      }, Zip2(Tuple2(CounterInteger(0, 1, Seq(8, 8, 8), None), CounterInteger(0, 1, Seq(8, 8), None)))
    )
    HDLProject("testZipTuple2", alg).compileHDL
  }

  @Test
  def testZipTuple3(): Unit = {
    Counter.resetAll()
    val alg = algo.Map(
      {
        val param1 = ParamDef()
        AlgoLambda(
          param1,
          algo.Map(
            {
              val param2 = ParamDef()
              AlgoLambda(
                param2,
                Tuple2(Select2(ParamUse(param2), 0), algo.Map(Repeat.asFunction(Seq(None), Seq(8)), Select2(ParamUse(param2), 1)))
              )
            }, Zip2(ParamUse(param1))
          )
        )
      }, Zip2(Tuple2(CounterInteger(0, 1, Seq(8, 8, 8), None), CounterInteger(0, 1, Seq(8, 8, 8), None)))
    )
    HDLProject("testZipTuple3", alg).compileHDL
  }

  @Test
  def testZipTupleWithFlipTuple(): Unit = {
    Counter.resetAll()
    val alg = algo.Map(2,
      {
        val param = ParamDef()
        AlgoLambda(
          param,
          Tuple2(Select2(FlipTuple2(ParamUse(param)), 1), Repeat(Select2(ParamUse(param), 1), 8))
        )
      }, algo.Map(Zip2.asFunction(), Zip2(Tuple2(CounterInteger(0, 1, Seq(8, 8, 8), None), CounterInteger(0, 1, Seq(8, 8), None))))
    )
    HDLProject("testZipTupleWithFlipTuple", alg).compileHDL
  }

  @Test
  def testInputPad(): Unit = {
    Counter.resetAll()
    val testData = Matrix.fromRandom(1, 62, 9, Some("A"))
    val tree = Split(Concat(Repeat(ConstantInteger(0, Some(algo.SignedIntType(9))), 2), Join(Input("input", 62, 1, algo.SignedIntType(9)))), 64)

    HDLProject("inputpad", tree, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), MoveDownConcatRules.get())),
    )).simulateWithHostRam(
      Predef.Map(
        "input" -> testData.data,
        MemoryImage.RESULT_VAR_NAME -> Seq(Seq(0, 0) ++ testData.data.head)
      )
    ).assertCorrect.print()
  }

  @Test
  def testInputPad2(): Unit = {
    Counter.resetAll()
    val testData = Matrix.fromRandom(1, 62, 9, Some("A"))
    val tree = Split(Concat(Repeat(ConstantInteger(0, Some(algo.SignedIntType(512))), 2), Join(Input("input", 62, 1, algo.SignedIntType(512)))), 64)

    HDLProject("inputpad", tree, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), MoveDownConcatRules.get())),
    )).simulateWithHostRam(
      Predef.Map(
        "input" -> testData.data,
        MemoryImage.RESULT_VAR_NAME -> Seq(Seq(0, 0) ++ testData.data.head)
      )
    ).assertCorrect.print()
  }
}

