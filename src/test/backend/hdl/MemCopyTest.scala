package backend.hdl

import algo.util.Matrix
import algo.{Id, Input}
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.{CleanupRules, InputBufferingRules, MoveDownSplitStreamRules, MoveUpJoinStreamRules, MoveUpVectorToStreamRules, RemoveStreamVectorConversionRules}
import core._
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import org.junit.Test

class MemCopyTest {

  def memcopy(cachelines: Int, run: Boolean = true): Unit = {
    val data = Matrix.fromCounter(1, cachelines, 1, Some("memcopy")).data
    Counter.resetAll()
    val alg = Id(Input("data", cachelines, 1, algo.IntType(512)))
    val proj = HDLProject("memcopy", alg, CompilerPhase.first(), Seq(
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(InputBufferingRules.doubleBufferRead)))
    ))
    if (run)
      proj.simulateWithHostRam(
      Predef.Map(
        "data" -> data,
        MemoryImage.RESULT_VAR_NAME -> data
      )
    ).assertCorrect.print()
    else
      proj.writeHDLFiles()
  }

  @Test
  def testMemCopy1(): Unit = memcopy(1024) // fast sim: 1105 cycles single buffer, 1046 cycles double buffering

  @Test
  def testMemCopy2(): Unit = memcopy(16384) // fast sim: 17665 cycles single buffer, 16646 cycles double buffering

  @Test
  def testMemCopy3(): Unit = memcopy(131072)

  @Test
  def testMemCopy4(): Unit = memcopy(1048576)

  // 512MB = 8 * 1024 * 1024 * 64 bytes
  @Test
  def testMemCopy5(): Unit = memcopy(8388608, run = false) // too large to run in simulation

}
