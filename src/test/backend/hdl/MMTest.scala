package backend.hdl

import algo.util.Matrix
import algo.{CounterInteger, Input, MMMul}
import backend.hdl.arch.{ArchCompiler, MapCompiler}
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.{CleanupRules, FixTimingRules, InputBufferingRules, ParallelizeDotProductRules}
import core._
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import org.junit.Test

class MMTest {

  def matrices(rows: Int, cols: Int): Map[String, Seq[Seq[Int]]] = {
    val matA = Matrix.fromRandom(rows, cols, 9, Some("A"))
    val matB = Matrix.fromRandom(rows, cols, 9, Some("B"))
    val matC = matA.mul(matB)
    Predef.Map(
      "matrix1" -> matA.data,
      "matrix2" -> matB.data,
      MemoryImage.RESULT_VAR_NAME -> matC.data
    )
  }

  @Test
  def testNoHostRam(): Unit = {
    Counter.resetAll()
    val tree = MMMul(CounterInteger(1, 1, Seq(3, 3), None), CounterInteger(11, 1, Seq(3, 3), None))
    HDLProject("mm", tree).compileHDL
  }

  @Test
  def testMMRowSmallerCacheline(): Unit = {
    Counter.resetAll()
    val tree = MMMul(Input("matrix1", 2, 4, algo.IntType(32)), Input("matrix2", 2, 4, algo.IntType(32)))
    HDLProject("mm", tree).simulateWithHostRam(matrices(4, 2)).assertCorrect.print()
  }

  @Test
  def testMMRowBiggerCacheline(): Unit = {
    Counter.resetAll()
    val tree = MMMul(Input("matrix1", 20, 1, algo.IntType(32)), Input("matrix2", 20, 1, algo.IntType(32)))
    HDLProject("mm", tree).simulateWithHostRam(matrices(1, 20)).assertCorrect.print()
  }

  @Test
  def testMMElementAlmostCacheline(): Unit = {
    Counter.resetAll()
    val tree = MMMul(Input("matrix1", 2, 2, algo.IntType(500)), Input("matrix2", 2, 2, algo.IntType(500)))
    HDLProject("mm", tree).simulateWithHostRam(matrices(2, 2)).assertCorrect.print()
  }

  @Test
  def testMMElementBiggerCacheline(): Unit = {
    Counter.resetAll()
    val tree = MMMul(Input("matrix1", 2, 2, algo.IntType(800)), Input("matrix2", 2, 2, algo.IntType(800)))
    HDLProject("mm", tree).simulateWithHostRam(matrices(2, 2)).assertCorrect.print()
  }

  @Test
  def testMMMemInput2(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 3, 4, algo.IntType(9)), Input("matrix2", 3, 4, algo.IntType(9)))).simulateWithHostRam(matrices(4, 3)).assertCorrect.print()
  }

  @Test
  def testMMMemInput3(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 3, 4, algo.IntType(32)), Input("matrix2", 3, 4, algo.IntType(32)))).simulateWithHostRam(matrices(4, 3)).assertCorrect.print()
  }

  @Test
  def testMMMemInput4(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 3, 4, algo.IntType(50)), Input("matrix2", 3, 4, algo.IntType(128)))).simulateWithHostRam(matrices(4, 3)).assertCorrect.print()
  }

  @Test
  def testMMMemInput5(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 3, 4, algo.IntType(9)), Input("matrix2", 3, 4, algo.IntType(200)))).simulateWithHostRam(matrices(4, 3)).assertCorrect.print()
  }

  @Test
  def testMMMemInput6(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 3, 4, algo.IntType(100)), Input("matrix2", 3, 4, algo.IntType(100)))).simulateWithHostRam(matrices(4, 3)).assertCorrect.print()
  }

  @Test
  def testMMMemInput7(): Unit = {
    // TODO
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 3, 4, algo.IntType(1100)), Input("matrix2", 3, 4, algo.IntType(11)))).simulateWithHostRam(matrices(4, 3)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_1x20(): Unit = {
    Counter.resetAll()
    val tree = MMMul(Input("matrix1", 1, 20, algo.IntType(32)), Input("matrix2", 1, 20, algo.IntType(32)))
    HDLProject("mm", tree).simulateWithHostRam(matrices(20, 1)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_1x1024(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 1024, 1, algo.IntType(8)), Input("matrix2", 1024, 1, algo.IntType(8)))).simulateWithHostRam(matrices(1, 1024)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_1024x1(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 1, 1024, algo.IntType(8)), Input("matrix2", 1, 1024, algo.IntType(8)))).simulateWithHostRam(matrices(1024, 1)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_256x1(): Unit = { // 150 sec (458761 cycles)
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 1, 256, algo.IntType(8)), Input("matrix2", 1, 256, algo.IntType(8)))).simulateWithHostRam(matrices(256, 1)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_8x8(): Unit = { // 6 sec (1796 cycles)
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 8, 8, algo.IntType(256)), Input("matrix2", 8, 8, algo.IntType(256))), CompilerPhase.first(),
      Seq(
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(2))))
      )
    ).simulateWithHostRam(matrices(8, 8)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_16x16(): Unit = { // 8 sec (5695 cycles)
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 16, 16, algo.IntType(8)), Input("matrix2", 16, 16, algo.IntType(8)))).simulateWithHostRam(matrices(16, 16)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_16x16_Float(): Unit = { // 8 sec (5695 cycles)
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 16, 16, algo.FloatSingleType()), Input("matrix2", 16, 16, algo.FloatSingleType()))).simulateWithHostRam(matrices(16, 16)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_32x32(): Unit = { // 20 sec (38924 cycles) (9229 cycles with 16x parallel dot product)
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 32, 32, algo.IntType(32)), Input("matrix2", 32, 32, algo.IntType(32))), CompilerPhase.first(),
      Seq(
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("matrix1", 1)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("matrix2", 1)))),
//        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.all(16))),
//        (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.all)),
//        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.parallelReadRequests(1))))
      )
    ).simulateWithHostRam(matrices(32, 32)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_64x64(): Unit = { // 77 sec (282759 cycles)
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 64, 64, algo.IntType(8)), Input("matrix2", 64, 64, algo.IntType(8)))).simulateWithHostRam(matrices(64, 64)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_128x128(): Unit = { // 37min (6325762 cycles)
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 128, 128, algo.IntType(8)), Input("matrix2", 128, 128, algo.IntType(8)))).simulateWithHostRam(matrices(128, 128)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_256x256(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 256, 256, algo.IntType(8)), Input("matrix2", 256, 256, algo.IntType(8)))).simulateWithHostRam(matrices(256, 256)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_512x512(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 512, 512, algo.IntType(8)), Input("matrix2", 512, 512, algo.IntType(8)))).simulateWithHostRam(matrices(512, 512)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_1024x1024_512Muls(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 1024, 1024, algo.IntType(8)), Input("matrix2", 1024, 1024, algo.IntType(8))), CompilerPhase.first(),
      Seq(
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow("matrix1", 8)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("matrix2", 8)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(64*8)))),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(1, 0), Seq(InputBufferingRules.doubleBufferRead))),
        (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
        // (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.parallelReadRequests(64)))) // not needed only 16 cachelines requested in parallel
        (MapCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      )
    ).simulateWithHostRam(matrices(1024, 1024)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_1024x1024_1024Muls(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 1024, 1024, algo.IntType(8)), Input("matrix2", 1024, 1024, algo.IntType(8))), CompilerPhase.first(),
      Seq(
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow("matrix1", 16)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("matrix2", 16)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(64*16)))),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(1, 0), Seq(InputBufferingRules.doubleBufferRead))),
        (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
        // (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.parallelReadRequests(64)))) // not needed only 16 cachelines requested in parallel
        (MapCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      )
    ).simulateWithHostRam(matrices(1024, 1024)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_512x3072(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 3072, 512, algo.IntType(8)), Input("matrix2", 3072, 512, algo.IntType(8))), CompilerPhase.first(),
      Seq(
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow("matrix1", 48)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("matrix2", 48)))),
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(64*48)))),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(1, 0), Seq(InputBufferingRules.doubleBufferRead))),
        (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
        //        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.parallelReadRequests(64)))) // not needed only 16 cachelines requested in parallel
        (MapCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      )
    ).simulateWithHostRam(matrices(512, 3072)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_2048x2048(): Unit = {
    Counter.resetAll()
    HDLProject("mm", MMMul(Input("matrix1", 2048, 2048, algo.IntType(8)), Input("matrix2", 2048, 2048, algo.IntType(8)))).simulateWithHostRam(matrices(2048, 2048)).assertCorrect.print()
  }

  @Test
  def testMMMemInput_8Kx8K_rewrite(): Unit = {
    Counter.resetAll()
    val alg = MMMul(Input("matrix1", 8192, 8192, algo.IntType(8)), Input("matrix2", 8192, 8192, algo.IntType(8)))
    HDLProject("mm", alg, CompilerPhase.first(),
      Seq(
        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(64)))),
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
        (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
//        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Rules.cleanup)),
//        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(1, 0), Seq(Rules.doubleBufferRead)))
      )
    ).writeHDLFiles()
  }

}
