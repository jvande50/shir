package backend.hdl

import algo.{Input, Join, Repeat}
import algo.util.Matrix
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.{CleanupRules, InputBufferingRules}
import core.{Counter, TypeChecker}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import org.junit.Test

class WriteSpeedTest {
  def repeatInner(A: Seq[Seq[Int]], repetitions: Int): Seq[Seq[Int]] = {
    assert(A.length == 1, "This test only considers outer dimension size as 1.")
    val result: Array[Array[Int]] = Array.ofDim[Int](A(0).length, repetitions)
    for (i <- A(0).indices) {
      for (j <- 0 until repetitions) {
        result(i)(j) = A(0)(i)
      }
    }
    result.map(_.toSeq).toSeq
  }

  @Test
  def fastWriteTest(): Unit = {
    val cachelines = 128
    val repeatTimes = 16
    val data = Matrix.fromCounter(1, cachelines, 1, Some("input")).data
    val res = repeatInner(data, repeatTimes)
    Counter.resetAll()
    val alg = TypeChecker.check(algo.Map(Repeat.asFunction(Seq(None), Seq(repeatTimes)), Join(Input("data", cachelines, 1, algo.IntType(512)))))
    val proj = HDLProject("repeatV2", alg, CompilerPhase.first(), Seq(
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(InputBufferingRules.doubleBufferRead)))
    ))

    proj.simulateWithHostRam(
      Predef.Map(
        "data" -> data,
        MemoryImage.RESULT_VAR_NAME -> res
      )
    ).print()
  }
}
