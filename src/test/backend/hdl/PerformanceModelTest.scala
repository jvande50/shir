package backend.hdl

import algo.{DotProduct, SeqType}
import backend.hdl.arch.PerformanceModel
import backend.hdl.arch.mem.MemFunctionsCompiler
import backend.hdl.arch.rewrite.ParallelizeDotProductRules
import core._
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep}
import org.junit.Test

class PerformanceModelTest {

  @Test
  def testDPstream(): Unit = {
    Counter.resetAll()
    val tree = DotProduct(ParamDef(SeqType(algo.IntType(16), ArithType(4))), ParamDef(SeqType(algo.IntType(16), ArithType(4))))

    val finalExpr = HDLProject("dotproduct", tree).compiledExpr

    val result = PerformanceModel.analyse(finalExpr)
    assert(result.area.dsps == 1)
    println(result)
  }

  @Test
  def testDPvector(): Unit = {
    Counter.resetAll()
    val tree = DotProduct(ParamDef(SeqType(algo.IntType(16), ArithType(4))), ParamDef(SeqType(algo.IntType(16), ArithType(4))))

    val finalExpr = HDLProject("dotproduct", tree, CompilerPhase.first(),
      Seq(
        (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(4)))),
      )
    ).compiledExpr

    val result = PerformanceModel.analyse(finalExpr)
    assert(result.area.dsps == 4)
    println(result)
  }
}
