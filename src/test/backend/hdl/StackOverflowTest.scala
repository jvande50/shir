package backend.hdl

import algo.{Buffer, Input}
import core.{Counter, Expr}
import org.junit.Test

class StackOverflowTest {
  /**
    * Reproduce StackOverflow error for too many primitives.
    * This can be solved by adding -Xss flag to sbt or Edit Configurations in IDE.
    */
  def repeatBuffer(in: Expr, count: Int): Expr = {
    if(count == 0)
      in
    else
      repeatBuffer(Buffer(in), count - 1)
  }

  @Test
  def testGenerateBuffers(): Unit = {
    Counter.resetAll()
    val tree = repeatBuffer(Input("matrix1", 2, 100, algo.IntType(512)), 5)

    HDLProject("genBuffers", tree).compileHDL
  }
}
