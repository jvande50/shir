package backend.hdl.nn.mlp

import algo.{Add2, AlgoLambda, SeqTypeT}
import backend.hdl.HDLProject
import core.{Counter, Marker, ParamDef, ParamUse, TextType, TypeChecker}
import core.compile.CompilerPhase
import org.junit.Test

class simpleRegressorTest {
  @Test
  def test3LayerMLP(): Unit = {
    // All layers are the same in this test.
    Counter.resetAll()
    val inputSize = 128
    val weight1Size = 128
    val weight2Size = 128
    val weight3Size = 128
    val input1 = algo.Input("input1", inputSize, 1,  algo.IntType(8))
    val weight1 = algo.Input("weight1", inputSize, weight1Size, algo.IntType(8))
    val weight2 = algo.Input("weight2", weight1Size, weight2Size, algo.IntType(8))
    val weight3 = algo.Input("weight3", weight2Size, weight3Size, algo.IntType(8))

    val tmpRes1 = TypeChecker.check(algo.MMMul(input1, weight1))
    val input2 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes1.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes1))
    val tmpRes2 = TypeChecker.check(algo.MMMul(input2, weight2))
    val input3 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes2.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes2))
    val tmpRes3 = TypeChecker.check(algo.MMMul(input3, weight3))
    val output = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes3.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes3))

    val tree = Marker(output, TextType("shared"))

    HDLProject(this.getClass.getSimpleName, tree, CompilerPhase.first()).compileHDL
  }

  @Test
  def test3LayerMLPRegressor(): Unit = {
    // The last layer is smaller than the first two.
    Counter.resetAll()
    val inputSize = 128
    val weight1Size = 128
    val weight2Size = 128
    val weight3Size = 1
    val input1 = algo.Input("input1", inputSize, 1, algo.IntType(8))
    val weight1 = algo.Input("weight1", inputSize, weight1Size, algo.IntType(8))
    val weight2 = algo.Input("weight2", weight1Size, weight2Size, algo.IntType(8))
    val weight3 = algo.Input("weight3", weight2Size, weight3Size, algo.IntType(8))

    val tmpRes1 = TypeChecker.check(algo.MMMul(input1, weight1))
    val input2 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes1.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes1))
    val tmpRes2 = TypeChecker.check(algo.MMMul(input2, weight2))
    val input3 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes2.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes2))
    val tmpRes3 = TypeChecker.check(algo.MMMul(input3, weight3))
    val output = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes3.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes3))

    val tree = Marker(output, TextType("shared"))

    HDLProject(this.getClass.getSimpleName, tree, CompilerPhase.first()).compileHDL
  }

  @Test
  def test3LayerMLPLargeLastLayer(): Unit = {
    // The last layer is smaller than the first two.
    Counter.resetAll()
    val inputSize = 128
    val weight1Size = 128
    val weight2Size = 128
    val weight3Size = 256
    val input1 = algo.Input("input1", inputSize, 1, algo.IntType(8))
    val weight1 = algo.Input("weight1", inputSize, weight1Size, algo.IntType(8))
    val weight2 = algo.Input("weight2", weight1Size, weight2Size, algo.IntType(8))
    val weight3 = algo.Input("weight3", weight2Size, weight3Size, algo.IntType(8))

    val tmpRes1 = TypeChecker.check(algo.MMMul(input1, weight1))
    val input2 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes1.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes1))
    val tmpRes2 = TypeChecker.check(algo.MMMul(input2, weight2))
    val input3 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes2.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes2))
    val tmpRes3 = TypeChecker.check(algo.MMMul(input3, weight3))
    val output = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes3.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes3))

    val tree = Marker(output, TextType("shared"))

    HDLProject(this.getClass.getSimpleName, tree, CompilerPhase.first()).compileHDL
  }

  @Test
  def test2LayerMLPSmallSecondLayer(): Unit = {
    // The last layer is smaller than the first two.
    Counter.resetAll()
    val inputSize = 128
    val weight1Size = 128
    val weight2Size = 64
    val input1 = algo.Input("input1", inputSize, 1, algo.IntType(8))
    val weight1 = algo.Input("weight1", inputSize, weight1Size, algo.IntType(8))
    val weight2 = algo.Input("weight2", weight1Size, weight2Size, algo.IntType(8))

    val tmpRes1 = TypeChecker.check(algo.MMMul(input1, weight1))
    val input2 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes1.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes1))
    val tmpRes2 = TypeChecker.check(algo.MMMul(input2, weight2))
    val output = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(tmpRes2.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, tmpRes2))

    val tree = Marker(output, TextType("shared"))

    HDLProject(this.getClass.getSimpleName, tree, CompilerPhase.first()).compileHDL
  }
}
