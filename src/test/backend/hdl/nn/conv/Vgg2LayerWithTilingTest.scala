package backend.hdl.nn.conv

import backend.hdl.HDLProject
import backend.hdl.nn.conv.SimpleLayerTemplate.{createConvLayer, createConvLayerTiledInput, createRoundingLayer, createSaveLayer}
import core.{Counter, Expr}
import core.compile.CompilerPhase
import core.rewrite.Rules
import org.junit.Test

class Vgg2LayerWithTilingTest {
  def vggFirstTwoLayersTiled: Expr = {
    val conv1 = createConvLayerTiledInput(3, 3, 64, 32, 32, 64, 1, true, 1, 16, None)
    val conv2 = createConvLayer(64, 64, 3, 1, false, 8, 2, conv1)
    val round2 = createRoundingLayer(8, 8, conv2)
    val res = createSaveLayer(round2)
    res
  }

  @Test
  def testVgg(): Unit = {
    // All layers are the same in this test.
    Counter.resetAll()
    Rules.disabled = Seq("fixZipSelectDim")
    HDLProject(this.getClass.getSimpleName, vggFirstTwoLayersTiled, CompilerPhase.first()).compileHDL
  }
}
