package backend.hdl.nn.conv

import algo.util.Matrix
import backend.hdl.arch.mem.MemoryImage

object ConvDataGen {
  def convLayerGen(
    imageRows: Int,
    imageCols: Int,
    weightSize: Int,
    inChannel: Int,
    outChannel: Int,
    pool: Boolean,
    bitOffset: Int = 8,
    bitWidth: Int = 8,
    id: Int = 0,
    bias: Boolean = true,
    previousLayer: Option[Predef.Map[String, Seq[Seq[Int]]]] = None
  ): Map[String, Seq[Seq[Int]]] = {
    var inputName: String = "image"
    val imageOrig = previousLayer match {
      case Some(layerData) if layerData.contains(MemoryImage.RESULT_VAR_NAME) =>
        inputName = "Inserted_"
        Matrix(layerData.get(MemoryImage.RESULT_VAR_NAME).get, "i" + id).deReshape(imageCols, false)
      case _ => Matrix.fromRandom(imageRows, imageCols * inChannel, 9, Some("i" + id))
    }
    val weightOrig = Matrix.fromRandom(outChannel, weightSize * weightSize * inChannel, 9, Some("w" + id))
    val imagePad = imageOrig.pad(1, 1, inChannel, inChannel, false)
    val output = imagePad.conv(weightOrig, inChannel, false, false)
    if (bias) {
      val biasOrig = Matrix.fromRandom(1, outChannel, 9, Some("b" + id))
      val outputBias = output.addBias(biasOrig, false)
      val outputRound = outputBias.round(bitOffset).clip(bitWidth).max(0)
      val outputPool = outputRound.pool(2, 2, outChannel)
      val realOut = if (pool) outputPool.reshape(imageCols / 2, false).data else outputRound.reshape(imageCols, false).data
      Predef.Map(
        inputName + id -> imageOrig.reshape(imageCols, false).data,
        "weight" + id -> weightOrig.data,
        "bias" + id -> biasOrig.data,
        MemoryImage.RESULT_VAR_NAME -> realOut
      )
    } else {
      val outputRound = output.round(bitOffset).clip(bitWidth).max(0)
      val outputPool = outputRound.pool(2, 2, outChannel, false)
      val realOut = if (pool) outputPool.reshape(imageCols / 2, false).data else outputRound.reshape(imageCols, false).data
      Predef.Map(
        inputName + id-> imageOrig.reshape(imageCols, false).data,
        "weight" + id-> weightOrig.data,
        MemoryImage.RESULT_VAR_NAME -> realOut
      )
    }
  }
}
