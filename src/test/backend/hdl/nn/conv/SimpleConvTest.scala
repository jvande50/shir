package backend.hdl.nn.conv

import algo.{AlgoLambda, SeqTypeT}
import backend.hdl.HDLProject
import core.{Counter, Expr, LambdaT, ParamDef, ParamUse, TextType, TypeChecker}
import core.compile.CompilerPhase
import org.junit.Test

class SimpleConvTest {
  def layerConvMLP: Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    // All layers are the same in this test.
    // We are using a very small conv to fit with MLP size.
    Counter.resetAll()
    val kernel1_width = 3
    val kernel1_height = 3
    val kernel1_num = 64
    val input_width = 3
    val input_height = 3
    val input_channel = 64
    val pad_size = 1

    val kernel2_num = 64

    // We are using (input) channel major data allocation!
    val input1 = algo.Input("input1", input_channel, input_width * input_height,  algo.IntType(8))
    val weight1 = algo.Input("weight1", input_channel, kernel1_width * kernel1_height * kernel1_num, algo.IntType(8))

    val input1Padded = TypeChecker.check(algo.Map(algo.Pad.asFunction(Seq(None), Seq(pad_size, pad_size, 0)),
      algo.Pad(algo.Split(input1, input_width), pad_size, pad_size, 0)))

    val input1Slided = TypeChecker.check(algo.TransposeND(
      algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernel1_width, 1)),
        algo.SlideGeneral(input1Padded, kernel1_height, 1)), Seq(0, 1, 3, 2, 4)))

    val input1Reshaped = TypeChecker.check(algo.Map(2, algo.Join.asFunction(), algo.Map(2, algo.Join.asFunction(),
      input1Slided)))

    val weight1Reshaped = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Split(weight1,
      kernel1_width * kernel1_height)))

    val conv = TypeChecker.check(
      algo.Map(
        2,
        {
          val paramI = ParamDef()
          AlgoLambda(
            paramI,
            algo.Map(
              {
                val paramW = ParamDef()
                AlgoLambda(
                  paramW,
                  algo.ResizeInteger(algo.DotProduct(ParamUse(paramI), ParamUse(paramW)), 8)
                )
              }, weight1Reshaped
            )
          )
        }, input1Reshaped
      )
    )

    val input2 = TypeChecker.check(algo.BufferHost(algo.Join(conv), TextType("input2")))
    val weight2 = algo.Input("weight2", kernel1_num * input_width * input_height, kernel2_num, algo.IntType(8))

    val input2Reshaped = TypeChecker.check(algo.Join(input2))
    val fc = TypeChecker.check(algo.MVMul(weight2, input2Reshaped))
    val output = TypeChecker.check(algo.Map(
      {
        val param = ParamDef(fc.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, fc))

    TypeChecker.check(algo.Split(output, kernel2_num))
  }

  @Test
  def testLayerConvMLP(): Unit = {
    // All layers are the same in this test.
    // We are using a very small conv to fit with MLP size.
    Counter.resetAll()
    HDLProject(this.getClass.getSimpleName, layerConvMLP, CompilerPhase.first()).compileHDL
  }

  @Test
  def testLayerConvMLPRewritten(): Unit = {
    // All layers are the same in this test.
    // We are using a very small conv to fit with MLP size.
    Counter.resetAll()
    val kernel1_width = 3
    val kernel1_height = 3
    val kernel1_num = 64
    val input_width = 3
    val input_height = 3
    val input_channel = 64
    val pad_size = 1

    val kernel2_num = 64

    // We are using (input) channel major data allocation!
    val input1 = algo.Input("input1", input_channel, input_width * input_height,  algo.IntType(8))
    val weight1 = algo.Input("weight1", input_channel, kernel1_width * kernel1_height * kernel1_num, algo.IntType(8))

    val input1Padded = TypeChecker.check(algo.Map(algo.Pad.asFunction(Seq(None), Seq(pad_size, pad_size, 0)),
      algo.Pad(algo.Split(input1, input_width), pad_size, pad_size, 0)))

    val input1Slided = TypeChecker.check(algo.TransposeND(
      algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernel1_width, 1)),
        algo.SlideGeneral(input1Padded, kernel1_height, 1)), Seq(0, 1, 3, 2, 4)))

    val input1Reshaped = TypeChecker.check(algo.Map(2, algo.Join.asFunction(), algo.Map(2, algo.Join.asFunction(),
      input1Slided)))

    val weight1Reshaped = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Split(weight1,
      kernel1_width * kernel1_height)))

    val conv = TypeChecker.check(
      algo.Map(
        2,
        {
          val paramI = ParamDef()
          AlgoLambda(
            paramI,
            algo.Map(
              {
                val paramW = ParamDef()
                AlgoLambda(
                  paramW,
                  algo.DotProduct(ParamUse(paramI), ParamUse(paramW))
                )
              }, weight1Reshaped
            )
          )
        }, input1Reshaped
      )
    )

    val clippedConv = TypeChecker.check(algo.Map(3, algo.ResizeInteger.asFunction(Seq(None), Seq(8)), conv))

    val input2 = TypeChecker.check(algo.BufferHost(algo.Join(clippedConv), TextType("input2")))
    val weight2 = algo.Input("weight2", kernel1_num * input_width * input_height, kernel2_num, algo.IntType(8))

    val input2Reshaped = TypeChecker.check(algo.Join(input2))
    val fc = TypeChecker.check(algo.MVMul(weight2, input2Reshaped))
    val output = TypeChecker.check(algo.Map(
      {
        val param = ParamDef(fc.t.asInstanceOf[SeqTypeT].leafType)
        AlgoLambda(param, algo.ResizeInteger(ParamUse(param), 8))
      }, fc))
    val outputReshaped = TypeChecker.check(algo.Split(output, kernel2_num))

    HDLProject(this.getClass.getSimpleName, outputReshaped, CompilerPhase.first()).compileHDL
  }
}
