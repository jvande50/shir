package backend.hdl.nn.conv

import algo.{AlgoLambda, SeqTypeT}
import backend.hdl.HDLProject
import core.{Counter, Expr, LambdaT, Marker, ParamDef, ParamUse, TextType, TypeChecker}
import core.compile.CompilerPhase
import org.junit.Test

class SimpleConvOnlyTest {
  def oneLayerConv: Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val kernel_width = 3
    val kernel_height = 3
    val kernel_num = 64
    val input_width = 32
    val input_height = 32
    val input_channel = 64
    val pad_size = 1

    // We are using (input) channel major data allocation!
    val input1 = algo.Input("input1", input_channel, input_width * input_height,  algo.IntType(8))
    val weight1 = algo.Input("weight1", input_channel, kernel_width * kernel_height * kernel_num, algo.IntType(8))

    val input1Padded = TypeChecker.check(algo.Map(algo.Pad.asFunction(Seq(None), Seq(pad_size, pad_size, 0)),
      algo.Pad(algo.Split(input1, input_width), pad_size, pad_size, 0)))

    val input1Slided = TypeChecker.check(algo.TransposeND(
      algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernel_width, 1)),
        algo.SlideGeneral(input1Padded, kernel_height, 1)), Seq(0, 1, 3, 2, 4)))

    val input1Reshaped = TypeChecker.check(algo.Map(2, algo.Join.asFunction(), algo.Map(2, algo.Join.asFunction(),
      input1Slided)))

    val weight1Reshaped = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Split(weight1,
      kernel_width * kernel_height)))

    val conv = TypeChecker.check(
      algo.Map(
        2,
        {
          val paramI = ParamDef()
          AlgoLambda(
            paramI,
            algo.Map(
              {
                val paramW = ParamDef()
                AlgoLambda(
                  paramW,
                  algo.ResizeInteger(algo.DotProduct(ParamUse(paramI), ParamUse(paramW)), 8)
                )
              }, weight1Reshaped
            )
          )
        }, input1Reshaped
      )
    )

    TypeChecker.check(algo.Join(conv))
  }

  def twoLayerConv: Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val kernel1_width = 3
    val kernel1_height = 3
    val kernel1_num = 64
    val kernel2_width = 3
    val kernel2_height = 3
    val kernel2_num = 64
    val input_width = 32
    val input_height = 32
    val input_channel = 64
    val pad_size = 1

    // We are using (input) channel major data allocation!
    val input1 = algo.Input("input1", input_channel, input_width * input_height,  algo.IntType(8))
    val weight1 = algo.Input("weight1", input_channel, kernel1_width * kernel1_height * kernel1_num, algo.IntType(8))

    val input1Padded = TypeChecker.check(algo.Map(algo.Pad.asFunction(Seq(None), Seq(pad_size, pad_size, 0)),
      algo.Pad(algo.Split(input1, input_width), pad_size, pad_size, 0)))

    val input1Slided = TypeChecker.check(algo.TransposeND(
      algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernel1_width, 1)),
        algo.SlideGeneral(input1Padded, kernel1_height, 1)), Seq(0, 1, 3, 2, 4)))

    val input1Reshaped = TypeChecker.check(algo.Map(2, algo.Join.asFunction(), algo.Map(2, algo.Join.asFunction(),
      input1Slided)))

    val weight1Reshaped = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Split(weight1,
      kernel1_width * kernel1_height)))

    val conv1 = TypeChecker.check(
      algo.Map(
        2,
        {
          val paramI = ParamDef()
          AlgoLambda(
            paramI,
            algo.Map(
              {
                val paramW = ParamDef()
                AlgoLambda(
                  paramW,
                  algo.ResizeInteger(algo.DotProduct(ParamUse(paramI), ParamUse(paramW)), 8)
                )
              }, weight1Reshaped
            )
          )
        }, input1Reshaped
      )
    )

    // Repeat again for layer 2

    val input2 = TypeChecker.check(algo.BufferHost(algo.Join(conv1), TextType("input2")))
    val weight2 = algo.Input("weight2", kernel1_num, kernel2_width * kernel2_height * kernel2_num, algo.IntType(8))

    val input2Padded = TypeChecker.check(algo.Map(algo.Pad.asFunction(Seq(None), Seq(pad_size, pad_size, 0)),
      algo.Pad(algo.Split(input2, input_width), pad_size, pad_size, 0)))

    val input2Slided = TypeChecker.check(algo.TransposeND(
      algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernel2_width, 1)),
        algo.SlideGeneral(input2Padded, kernel2_height, 1)), Seq(0, 1, 3, 2, 4)))

    val input2Reshaped = TypeChecker.check(algo.Map(2, algo.Join.asFunction(), algo.Map(2, algo.Join.asFunction(),
      input2Slided)))

    val weight2Reshaped = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Split(weight2,
      kernel2_width * kernel2_height)))

    val conv2 = TypeChecker.check(
      algo.Map(
        2,
        {
          val paramI = ParamDef()
          AlgoLambda(
            paramI,
            algo.Map(
              {
                val paramW = ParamDef()
                AlgoLambda(
                  paramW,
                  algo.ResizeInteger(algo.DotProduct(ParamUse(paramI), ParamUse(paramW)), 8)
                )
              }, weight2Reshaped
            )
          )
        }, input2Reshaped
      )
    )

    TypeChecker.check(algo.Join(conv2))
  }

  @Test
  def test1LayerConv(): Unit = {
    // All layers are the same in this test.
    Counter.resetAll()
    HDLProject(this.getClass.getSimpleName, oneLayerConv, CompilerPhase.first()).compileHDL
  }

  @Test
  def test2LayerConv(): Unit = {
    Counter.resetAll()
    HDLProject(this.getClass.getSimpleName, twoLayerConv, CompilerPhase.first()).compileHDL
  }

}
