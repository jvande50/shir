package backend.hdl.nn.conv

import algo.AlgoLambda
import backend.hdl.HDLProject
import backend.hdl.nn.conv.SimpleLayerTemplate.{createConvLayer, createInputLayer, createPoolingLayer, createRoundingLayer, createSaveLayer}
import core.{Counter, Expr, ParamDef, ParamUse, TypeChecker}
import core.compile.CompilerPhase
import core.util.IRDotGraph
import org.junit.Test

class SimpleVggConv3LayersTest {
  def vggFirstThreeLayers: Expr = {
    val input = createInputLayer(3, 32, 32, 8, 0)
    val conv0 = createConvLayer(3, 64, 3, 1, false, 8, 0, input)
    val round0 = createRoundingLayer(8, 8, conv0)
    val conv1 = createConvLayer(64, 64, 3, 1, false, 8, 1, round0)
    val round1 = createRoundingLayer(8, 8, conv1)
    val conv1Pool = createPoolingLayer(2, round1)
    val conv2 = createConvLayer(64, 128, 3, 1, false, 8, 2, conv1Pool)
    val round2 = createRoundingLayer(8, 8, conv2)
    val res = createSaveLayer(round2)
    res
  }

  @Test
  def testVgg(): Unit = {
    // All layers are the same in this test.
    Counter.resetAll()
    HDLProject(this.getClass.getSimpleName, vggFirstThreeLayers, CompilerPhase.first()).compileHDL
  }
}
