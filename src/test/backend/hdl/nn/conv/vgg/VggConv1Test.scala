package backend.hdl.nn.conv.vgg

import backend.hdl.HDLProject
import backend.hdl.nn.conv.ConvDataGen.convLayerGen
import backend.hdl.nn.conv.SimpleLayerTemplate.{createConvLayer, createInputLayer, createPoolingLayer, createReLULayer, createRoundingLayer, createSaveLayer}
import backend.hdl.nn.conv.guidedRules.TileConvRules
import core.{Counter, TypeChecker}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph
import org.junit.Test

class VggConv1Test {
  @Test
  def testVgg(): Unit = {
    // All layers are the same in this test.
    Counter.resetAll()
    val input = createInputLayer(64, 32, 32, 8, 0)
    val conv = createConvLayer(64, 64, 3, 1, true, 8, 0, input)
    val round = createRoundingLayer(8, 8, conv)
    val relu = createReLULayer(round)
    val pool = createPoolingLayer(2, relu)
    val expr = createSaveLayer(pool)

    val newExpr = TypeChecker.check(RewriteStep(RewriteAll(), Seq(TileConvRules.tileHW(8))).apply(expr))
    IRDotGraph(newExpr).show()

    HDLProject(this.getClass.getSimpleName, newExpr, CompilerPhase.first()).simulateWithHostRam(convLayerGen(32, 32, 3, 64, 64, true))
  }
}
