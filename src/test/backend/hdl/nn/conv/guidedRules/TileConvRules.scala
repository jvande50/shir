package backend.hdl.nn.conv.guidedRules

import algo.{AlgoLambda, SeqTypeT}
import core.{ParamUse, TypeChecker}
import core.rewrite.Rule

object TileConvRules {
  // These are specific rules to tile conv layers/
  def tileHW(tiledSize: Int): Rule = Rule("tileHW", {
    case algo.Map(AlgoLambda(p1, algo.Map(AlgoLambda(p2, conv @ algo.Map(AlgoLambda(_, algo.Fold(AlgoLambda(_, AlgoLambda(_,
    algo.Add(algo.Tuple2(_, _, _), _), _), _), algo.Map(AlgoLambda(_, algo.Mul(_, _), _), algo.Zip2(algo.Tuple2(_, _, _), _),
    _), _), _), weight, _), _), ParamUse(p3), _), _), input, t: SeqTypeT) if p1.id == p3.id && t.len.ae.evalInt > tiledSize &&
      t.len.ae.evalInt % tiledSize == 0 && t.et.asInstanceOf[SeqTypeT].len.ae.eval > tiledSize &&
      t.et.asInstanceOf[SeqTypeT].len.ae.eval % tiledSize == 0=>
      val tiledInput = TypeChecker.check(algo.TransposeND(TypeChecker.check(algo.Split(algo.Map(
        algo.Split.asFunction(Seq(None), Seq(tiledSize)), input), tiledSize)), Seq(0, 1, 3, 2, 4)))
      val tiledConv = TypeChecker.check(algo.Map(4, AlgoLambda(p2,conv), tiledInput))
      algo.Map(algo.Join.asFunction(), algo.Join(TypeChecker.check(algo.TransposeND(tiledConv, Seq(0, 1, 3, 2, 4)))))
  })

}
