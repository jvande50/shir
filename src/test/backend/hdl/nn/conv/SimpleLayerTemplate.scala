package backend.hdl.nn.conv

import algo.{AlgoLambda, ConstantInteger, SeqTypeT}
import core.{Expr, LambdaT, ParamDef, ParamUse, TypeChecker}

object SimpleLayerTemplate {
  implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

  /**
   * Input tensor holder for convolutional networks. Note that we are using channel last memory allocation.
   * The dimension order is Seq(C, W, H). So we will get H x W C-dimensional vectors.
   */
  def createInputLayer(
    inputChannel: Int,
    inputWidth: Int,
    inputHeight: Int,
    bitWidth: Int,
    id: Int,
  ): Expr = {
    TypeChecker.check(algo.Split(algo.Input("input" + id, inputChannel, inputWidth * inputHeight,  algo.SignedIntType(bitWidth)), inputWidth))
  }

  /**
   * Assumptions: Bias has twice of bit width compared to weight.
   */
  def createConvLayer(
    inputChannel: Int,
    kernelNum: Int,
    kernelSize: Int,
    padSize: Int,
    bias: Boolean,
    weightBitWidth : Int,
    id: Int,
    input: Expr
  ): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
    assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "input must be 3D!")
    assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.head.ae.evalInt == inputChannel, "Layer and input channels do not match!")

    val weight = algo.Input("weight" + id, inputChannel, kernelSize * kernelSize * kernelNum, algo.SignedIntType(weightBitWidth))

    val inputPadded = TypeChecker.check(algo.Map(algo.Pad.asFunction(Seq(None), Seq(padSize, padSize, 0)),
      algo.Pad(inputtc, padSize, padSize, 0)))

    val inputSlided = TypeChecker.check(algo.TransposeND(
      algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernelSize, 1)),
        algo.SlideGeneral(inputPadded, kernelSize, 1)), Seq(0, 1, 3, 2, 4)))

    val inputReshaped = TypeChecker.check(algo.Map(2, algo.Join.asFunction(), algo.Map(2, algo.Join.asFunction(),
      inputSlided)))

    val weightReshaped = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Split(weight,
      kernelSize * kernelSize)))

    val conv = TypeChecker.check(
      algo.Map(
        2,
        {
          val paramI = ParamDef()
          AlgoLambda(
            paramI,
            algo.Map(
              {
                val paramW = ParamDef()
                AlgoLambda(
                  paramW,
                  algo.DotProduct(ParamUse(paramI), ParamUse(paramW))
                )
              }, weightReshaped
            )
          )
        }, inputReshaped
      )
    )

    val addBias = if(bias) {
      val bias = TypeChecker.check(algo.Join(algo.Input("bias" + id, kernelNum, 1, algo.SignedIntType(2 * weightBitWidth))))
      TypeChecker.check(
        algo.Map(
          2,
          {
            val param1 = ParamDef()
            AlgoLambda(
              param1,
              algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(param1), bias)))
            )
          }, conv
        )
      )
    } else {
      conv
    }

    addBias
  }

  /**
   * Assumptions: Kernel size and step size are the same.
   */
  def createPoolingLayer(
    kernelSize: Int,
    input: Expr
  ): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
    assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "input must be 3D!")

    val splitWins = TypeChecker.check(algo.Split(algo.Map(algo.Split.asFunction(Seq(None), Seq(kernelSize)), inputtc), kernelSize))
    val transposedConv = TypeChecker.check(algo.TransposeND(splitWins, Seq(1, 3, 0, 2, 4)))
    val joinedTpConv = TypeChecker.check(algo.Map(3, algo.Join.asFunction(), transposedConv))
    val maxRes = TypeChecker.check(algo.Map(3, {
      val param = ParamDef(joinedTpConv.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
      AlgoLambda(
        param,
        algo.Fold(algo.Max2.asFunction(), ParamUse(param))
      )
    }, joinedTpConv))
    maxRes
  }

  def createReLULayer(
    input: Expr
  ): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")

    val dimLevels = inputtc.t.asInstanceOf[SeqTypeT].dimensions.length
    TypeChecker.check(algo.Map(dimLevels, {
      val param = ParamDef()
      AlgoLambda(param, algo.Max2(ParamUse(param), ConstantInteger(0, Some(algo.SignedIntType(8)))))
    }, inputtc))
  }

  def createRoundingLayer(
    bitWidth : Int,
    bitRound : Int,
    input: Expr
  ): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")

    val dimLevels = inputtc.t.asInstanceOf[SeqTypeT].dimensions.length
    val newElementWidth = inputtc.t.asInstanceOf[SeqTypeT].leafType.asInstanceOf[algo.SignedIntTypeT].width.ae.evalInt
    val round = TypeChecker.check(algo.Map(dimLevels, algo.ClipBankersRound.asFunction(Seq(None),
      Seq(bitRound, newElementWidth - bitWidth - bitRound)), inputtc))
    round
  }

  /**
   * Convert 3D tensor into 2D one for cacheline.
   */
  def createSaveLayer(
    input: Expr
  ): Expr = {
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
    assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "input must be 3D!")
    TypeChecker.check(algo.Join(inputtc))
  }


  // TODO: remove this. There is a more general way.

  def createConvLayerTiledInput(
    kernel_width: Int,
    kernel_height: Int,
    kernel_num: Int,
    input_width: Int,
    input_height: Int,
    input_channel: Int,
    pad_size: Int,
    pooling: Boolean,
    id: Int,
    input_tile_size: Int,
    preInput: Option[Expr]
  ): Expr = {
    val input = preInput match {
      case Some(e) => e
      case _ => algo.Input("input" + id, input_channel, input_width * input_height,  algo.SignedIntType(8))
    }
    val weight = algo.Input("weight" + id, input_channel, kernel_width * kernel_height * kernel_num, algo.SignedIntType(8))

    val inputPadded = TypeChecker.check(algo.Map(algo.Pad.asFunction(Seq(None), Seq(pad_size, pad_size, 0)),
      algo.Pad(algo.Split(input, input_width), pad_size, pad_size, 0)))

    val tiledInput = TypeChecker.check(algo.TransposeND(TypeChecker.check(algo.SlideGeneral(algo.Map(
      algo.SlideGeneral.asFunction(Seq(None), Seq(input_tile_size + 2, input_tile_size)), inputPadded),
      input_tile_size + 2, input_tile_size)), Seq(0, 1, 3, 2, 4)))

    val weightReshaped = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Split(weight,
      kernel_width * kernel_height)))

    val tiledConv = TypeChecker.check(algo.Map(2,
      {
        val tiledParam = ParamDef(tiledInput.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          tiledParam,
          {
            val newInput = TypeChecker.check(TypeChecker.check(algo.TransposeND(
              algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernel_width, 1)),
                algo.SlideGeneral(ParamUse(tiledParam), kernel_height, 1)), Seq(0, 1, 3, 2, 4))))
            val inputReshaped = TypeChecker.check(algo.Map(2, algo.Join.asFunction(), algo.Map(2, algo.Join.asFunction(),
              newInput)))
            val conv = TypeChecker.check(
              algo.Map(
                2,
                {
                  val paramI = ParamDef()
                  AlgoLambda(
                    paramI,
                    algo.Map(
                      {
                        val paramW = ParamDef()
                        AlgoLambda(
                          paramW,
                          algo.ResizeInteger(algo.DotProduct(ParamUse(paramI), ParamUse(paramW)), 8)
                        )
                      }, weightReshaped
                    )
                  )
                }, inputReshaped
              )
            )

            if(pooling) {
              val splitConv = TypeChecker.check(algo.Split(algo.Map(algo.Split.asFunction(Seq(None), Seq(2)), conv), 2))
              val transposedConv = TypeChecker.check(algo.TransposeND(splitConv, Seq(1, 3, 0, 2, 4)))
              val joinedTpConv = TypeChecker.check(algo.Map(3, algo.Join.asFunction(), transposedConv))
              val maxConv = TypeChecker.check(algo.Map(3, {
                val param = ParamDef(joinedTpConv.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
                AlgoLambda(
                  param,
                  algo.Fold(algo.Max2.asFunction(), ParamUse(param))
                )
              }, joinedTpConv))
              maxConv
            } else {
              conv
            }
          }
        )
      }, tiledInput
    ))

    val reshapedOutput = TypeChecker.check(algo.Map(algo.Join.asFunction(), algo.Join(TypeChecker.check(algo.TransposeND(tiledConv, Seq(0, 1, 3, 2, 4))))))
    reshapedOutput

  }
}
