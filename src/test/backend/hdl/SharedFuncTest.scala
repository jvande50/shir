package backend.hdl

import algo.{AlgoLambda, Buffer, ConstantInteger, CounterInteger, SeqType, SeqTypeT}
import core.{Counter, FunctionCall, Let, ParamDef, ParamUse, TypeChecker}
import core.compile.CompilerPhase
import org.junit.Test

class SharedFuncTest {

  @Test
  def testPlusOneFunOneCall(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody = algo.Map({
      {
        val param = ParamDef(algo.IntType(8))
        AlgoLambda(param, algo.Add(algo.Tuple2(ParamUse(param), ConstantInteger(1))))
      }
    }, ParamUse(lParam))
    val plusOneFun = TypeChecker.check(AlgoLambda(lParam, lBody))
    val plusOneFunParam = ParamDef(plusOneFun.t)

    // Instantiate first input
    val input = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = Let(
      plusOneFunParam,
      FunctionCall(ParamUse(plusOneFunParam), input),
      plusOneFun
    )

    HDLProject("testPlusOneFunOneCall", expr, CompilerPhase.first(), Seq()).compileHDL
  }

  @Test
  def testPlusOneFunTwoCallsWithBuffer(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody = algo.Map({
      {
        val param = ParamDef(algo.IntType(8))
        AlgoLambda(param, algo.Add(algo.Tuple2(ParamUse(param), ConstantInteger(1))))
      }
    }, ParamUse(lParam))
    val plusOneFun = TypeChecker.check(AlgoLambda(lParam, lBody))
    val plusOneFunParam = ParamDef(plusOneFun.t)

    // Instantiate first input
    val input = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = Let(
      plusOneFunParam,
      FunctionCall(ParamUse(plusOneFunParam), Buffer(FunctionCall(ParamUse(plusOneFunParam), input))),
      plusOneFun
    )

    HDLProject("testPlusOneFunTwoCallsWithBuffer", expr, CompilerPhase.first(), Seq()).compileHDL
  }

  @Test
  def testMapFunCall(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam1 = ParamDef(algo.IntType(8))
    val lParam2 = ParamDef(algo.IntType(2))
    val lBody = algo.Add(algo.Tuple2(algo.Add(algo.Tuple2(ParamUse(lParam1), ParamUse(lParam2))), ConstantInteger(1)))
    val plusOneFun = TypeChecker.check(AlgoLambda(lParam1, AlgoLambda(lParam2, lBody)))
    val plusOneFunParam = ParamDef(plusOneFun.t)

    // Instantiate 2 inputs
    val input1 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input2 = ConstantInteger(1, Some(algo.IntType(2)))

    // Use Let and call the function
    val expr = Let(
      plusOneFunParam,
      algo.Map(
        {
          val param = ParamDef(algo.IntType(8))
          AlgoLambda(param, FunctionCall(FunctionCall(ParamUse(plusOneFunParam), ParamUse(param)), input2))
        },
        input1
      ),
      plusOneFun
    )

    HDLProject("testMapFunCall", expr, CompilerPhase.first(), Seq()).compileHDL
  }

  /**
   * Test a two-input function that contains a Concat primitive.
   *
   * Concat primitive forces the two inputs to be consumed at different time.
   * The order of function's parameters matches that of Concat's inputs.
   */
  @Test
  def testConcatFunc(): Unit = {
    Counter.resetAll()

    val param1 = ParamDef(SeqType(algo.IntType(8), 10))
    val param2 = ParamDef(SeqType(algo.IntType(8), 10))
    val func = TypeChecker.check(AlgoLambda(param1, AlgoLambda(param2, algo.Concat(ParamUse(param1), ParamUse(param2)))))
    val funcParam = ParamDef(func.t)

    val input1 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input4 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      algo.Tuple(
        algo.Buffer(FunctionCall(FunctionCall(ParamUse(funcParam), input1), input2)),
        algo.Buffer(FunctionCall(FunctionCall(ParamUse(funcParam), input3), input4))
      ),
      func
    ))

    HDLProject("testConcatFunc", expr, CompilerPhase.first(), Seq()).compileHDL //simulateStandalone()
  }

  /**
   * Test a two-input function that contains a Concat primitive.
   *
   * Concat primitive forces the two inputs to be consumed at different time.
   * The order of function's parameters is a reverse of that of Concat's inputs.
   */
  @Test
  def testConcatFuncReverseConcat(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(SeqType(algo.IntType(8), 10))
    val param2 = ParamDef(SeqType(algo.IntType(8), 10))
    val func = TypeChecker.check(AlgoLambda(param1, AlgoLambda(param2, algo.Concat(ParamUse(param2), ParamUse(param1)))))
    val funcParam = ParamDef(func.t)

    val input1 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input4 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      algo.Tuple(
        algo.Buffer(FunctionCall(FunctionCall(ParamUse(funcParam), input1), input2)),
        algo.Buffer(FunctionCall(FunctionCall(ParamUse(funcParam), input3), input4))
      ),
      func
    ))

    HDLProject("testConcatFuncReverseConcat", expr, CompilerPhase.first(), Seq()).compileHDL //simulateStandalone()
  }

  /** Test a three-input function that contains a Concat primitive. */
  @Test
  def testConcatFuncConcatThreeInputs(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(SeqType(algo.IntType(8), 10))
    val param2 = ParamDef(SeqType(algo.IntType(8), 10))
    val param3 = ParamDef(SeqType(algo.IntType(8), 10))
    val func = TypeChecker.check(AlgoLambda(param1, AlgoLambda(param2, AlgoLambda(param3,
      algo.Concat(ParamUse(param1), algo.Concat(ParamUse(param2), ParamUse(param3)))))))
    val funcParam = ParamDef(func.t)

    val input1 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input2 = CounterInteger(10, 1, Seq(10), Some(algo.IntType(8)))
    val input3 = CounterInteger(20, 1, Seq(10), Some(algo.IntType(8)))

    val input4 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input5 = CounterInteger(10, 1, Seq(10), Some(algo.IntType(8)))
    val input6 = CounterInteger(20, 1, Seq(10), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      algo.Tuple(
        algo.Buffer(FunctionCall(FunctionCall(FunctionCall(ParamUse(funcParam), input1), input2), input3)),
        algo.Buffer(FunctionCall(FunctionCall(FunctionCall(ParamUse(funcParam), input4), input5), input6))
      ),
      func
    ))

    HDLProject("testConcatFuncConcatThreeInputs", expr, CompilerPhase.first(), Seq()).compileHDL //simulateStandalone()
  }

  /** Test a five-input function that contains a Concat primitive. */
  @Test
  def testConcatFuncConcatFiveInputs(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(SeqType(algo.IntType(8), 10))
    val param2 = ParamDef(SeqType(algo.IntType(8), 10))
    val param3 = ParamDef(SeqType(algo.IntType(8), 10))
    val param4 = ParamDef(SeqType(algo.IntType(8), 10))
    val param5 = ParamDef(SeqType(algo.IntType(8), 10))
    val func = TypeChecker.check(AlgoLambda(param1, AlgoLambda(param2, AlgoLambda(param3, AlgoLambda(param4, AlgoLambda(param5,
      algo.Concat(ParamUse(param1), algo.Concat(ParamUse(param2), algo.Concat(ParamUse(param3), algo.Concat(ParamUse(param4), ParamUse(param5)))))))))))
    val funcParam = ParamDef(func.t)

    val input1 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input2 = CounterInteger(10, 1, Seq(10), Some(algo.IntType(8)))
    val input3 = CounterInteger(20, 1, Seq(10), Some(algo.IntType(8)))
    val input4 = CounterInteger(30, 1, Seq(10), Some(algo.IntType(8)))
    val input5 = CounterInteger(40, 1, Seq(10), Some(algo.IntType(8)))

    val input6 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input7 = CounterInteger(10, 1, Seq(10), Some(algo.IntType(8)))
    val input8 = CounterInteger(20, 1, Seq(10), Some(algo.IntType(8)))
    val input9 = CounterInteger(30, 1, Seq(10), Some(algo.IntType(8)))
    val input10 = CounterInteger(40, 1, Seq(10), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      algo.Tuple(
        algo.Buffer(FunctionCall(FunctionCall(FunctionCall(FunctionCall(FunctionCall(ParamUse(funcParam), input1), input2), input3), input4), input5)),
        algo.Buffer(FunctionCall(FunctionCall(FunctionCall(FunctionCall(FunctionCall(ParamUse(funcParam), input6), input7), input8), input9), input10))
      ),
      func
    ))

    HDLProject("testConcatFuncConcatFiveInputs", expr, CompilerPhase.first(), Seq()).compileHDL //simulateStandalone()
  }

  /** Test a two-input function that contains a scalar input and a stream input. */
  @Test
  def testAddConstFunc(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(SeqType(algo.IntType(8), 10))
    val param2 = ParamDef(algo.IntType(8))
    val func = TypeChecker.check(AlgoLambda(param1, AlgoLambda(param2, algo.Map({
      val param = ParamDef()
      AlgoLambda(param, algo.Add2(ParamUse(param), ParamUse(param2)))
    }, ParamUse(param1)))))
    val funcParam = ParamDef(func.t)

    val input1 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input2 = ConstantInteger(1, Some(algo.IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(10), Some(algo.IntType(8)))
    val input4 = ConstantInteger(1, Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      algo.Tuple(
        algo.Buffer(FunctionCall(FunctionCall(ParamUse(funcParam), input1), input2)),
        algo.Buffer(FunctionCall(FunctionCall(ParamUse(funcParam), input3), input4))
      ),
      func
    ))

    HDLProject("testAddConstFunc", expr, CompilerPhase.first(), Seq()).compileHDL //simulateStandalone()
  }

  /**
   * Test shared functions inside map. We expect the arbiter will switch between two calls multiple times.
   */
  @Test
  def testFunCallsInMap(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody = algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(lParam), CounterInteger(0, 1, Seq(10), Some(algo.IntType(4))))))
    val plusFun = TypeChecker.check(AlgoLambda(lParam, lBody))
    val plusFunParam = ParamDef(plusFun.t)

    // Instantiate 2 inputs
    val input = CounterInteger(0, 1, Seq(10, 3), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = Let(
      plusFunParam,
      algo.Map(
        {
          val param = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param,
            FunctionCall(ParamUse(plusFunParam),algo.Buffer(FunctionCall(ParamUse(plusFunParam), ParamUse(param)))),
          )
        }, input
      ),
      plusFun
    )

    HDLProject("testFunCallsInMap", expr, CompilerPhase.first(), Seq()).compileHDL //simulateStandalone()
  }

  /**
   * Test shared function with counters inside it to see if RepeatHidden is working properly.
   */
  @Test
  def testFunNested(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam1 = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody1 = algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(lParam1), CounterInteger(0, 1, Seq(10), Some(algo.IntType(4))))))
    val plusFun1 = TypeChecker.check(AlgoLambda(lParam1, lBody1))
    val plusFun1Param = ParamDef(plusFun1.t)

    val lParam2 = ParamDef(SeqType(algo.IntType(8), 10))
    val plusFun2 = TypeChecker.check(AlgoLambda(lParam2, FunctionCall(ParamUse(plusFun1Param), ParamUse(lParam2))))
    val plusFun2Param = ParamDef(plusFun2.t)

    // Instantiate 2 inputs
    val input = CounterInteger(0, 1, Seq(10, 3), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = Let(
      plusFun1Param,
      Let(
        plusFun2Param,
        algo.Map(
          {
            val param = ParamDef(SeqType(algo.IntType(8), 10))
            AlgoLambda(
              param,
              FunctionCall(ParamUse(plusFun2Param), ParamUse(param))
            )
          }, input
        ),
        plusFun2
      ),
      plusFun1
    )

    HDLProject("testFunNested", expr, CompilerPhase.first(), Seq()).simulateStandalone()
  }

  @Test
  def testFunMoreNested(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam1 = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody1 = algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(lParam1), CounterInteger(0, 1, Seq(10), Some(algo.IntType(4))))))
    val plusFun1 = TypeChecker.check(AlgoLambda(lParam1, lBody1))
    val plusFun1Param = ParamDef(plusFun1.t)

    val lParam2 = ParamDef(SeqType(algo.IntType(8), 10))
    val plusFun2 = TypeChecker.check(AlgoLambda(lParam2, FunctionCall(ParamUse(plusFun1Param), ParamUse(lParam2))))
    val plusFun2Param = ParamDef(plusFun2.t)

    val lParam3 = ParamDef(SeqType(algo.IntType(8), 10))
    val plusFun3 = TypeChecker.check(AlgoLambda(lParam3, FunctionCall(ParamUse(plusFun2Param), ParamUse(lParam3))))
    val plusFun3Param = ParamDef(plusFun3.t)

    // Instantiate 2 inputs
    val input = CounterInteger(0, 1, Seq(10, 3), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = Let(
      plusFun1Param,
      Let(
        plusFun2Param,
        Let(
          plusFun3Param,
          algo.Map(
            {
              val param = ParamDef(SeqType(algo.IntType(8), 10))
              AlgoLambda(
                param,
                FunctionCall(ParamUse(plusFun3Param),algo.Buffer(FunctionCall(ParamUse(plusFun3Param), ParamUse(param))))
              )
            }, input
          ),
          plusFun3
        ),
        plusFun2
      ),
      plusFun1
    )

    HDLProject("testFunMoreNested", expr, CompilerPhase.first(), Seq()).simulateStandalone()
  }

  @Test
  def testFunNestedMorePorts(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam11 = ParamDef(SeqType(algo.IntType(8), 10))
    val lParam12 = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody1 = algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(
      algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(lParam11), ParamUse(lParam12)))),
      CounterInteger(0, 1, Seq(10), Some(algo.IntType(4))))))
    val plusFun1 = TypeChecker.check(AlgoLambda(lParam11, AlgoLambda(lParam12, lBody1)))
    val plusFun1Param = ParamDef(plusFun1.t)

    val lParam21 = ParamDef(SeqType(algo.IntType(8), 10))
    val lParam22 = ParamDef(SeqType(algo.IntType(8), 10))
    val plusFun2 = TypeChecker.check(AlgoLambda(lParam21, AlgoLambda(lParam22, FunctionCall(FunctionCall(ParamUse(plusFun1Param), ParamUse(lParam21)), ParamUse(lParam22)))))
    val plusFun2Param = ParamDef(plusFun2.t)

    // Instantiate 2 inputs
    val input1 = CounterInteger(0, 1, Seq(10, 3), Some(algo.IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(10, 3), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = Let(
      plusFun1Param,
      Let(
        plusFun2Param,
        algo.MapAsync(
          {
            val param1 = ParamDef(SeqType(algo.IntType(8), 10))
            val param2 = ParamDef(SeqType(algo.IntType(8), 10))
            AlgoLambda(
              param1,
              AlgoLambda(
                param2,
                FunctionCall(FunctionCall(ParamUse(plusFun2Param), ParamUse(param1)), ParamUse(param2))
              )
            )
          }, input1, input2
        ),
        plusFun2
      ),
      plusFun1
    )

    HDLProject("testFunNestedMorePorts", expr, CompilerPhase.first(), Seq()).simulateStandalone()
  }

  @Test
  def testIt(): Unit = {
    Counter.resetAll()
    // Define a function
    val lParam1 = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody1 = algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(lParam1), CounterInteger(0, 1, Seq(10), Some(algo.IntType(4))))))
    val plusFun1 = TypeChecker.check(AlgoLambda(lParam1, lBody1))
    val plusFun1Param = ParamDef(plusFun1.t)

    val lParam2 = ParamDef(SeqType(algo.IntType(8), 10))
    val lBody2 = FunctionCall(ParamUse(plusFun1Param),
      algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(lParam2), CounterInteger(0, 1, Seq(10), Some(algo.IntType(4)))))))
    val plusFun2 = TypeChecker.check(AlgoLambda(lParam2, lBody2))
    val plusFun2Param = ParamDef(plusFun2.t)

    // Instantiate 2 inputs
    val input = CounterInteger(0, 1, Seq(10, 3), Some(algo.IntType(8)))

    // Use Let and call the function
    val expr = Let(
      plusFun1Param,
      Let(
        plusFun2Param,
        algo.Map(
          {
            val param = ParamDef(SeqType(algo.IntType(8), 10))
            AlgoLambda(
              param,
              FunctionCall(ParamUse(plusFun2Param), ParamUse(param))
            )
          }, input
        ),
        plusFun2
      ),
      plusFun1
    )

    HDLProject("testIt", expr, CompilerPhase.first(), Seq()).writeHDLFiles()//.simulateStandalone()
  }
}
