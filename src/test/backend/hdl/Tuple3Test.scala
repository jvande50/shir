package backend.hdl

import algo.{AlgoLambda, CounterInteger, SeqTypeT}
import backend.hdl.arch.ArchCompiler
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep}
import core.{ParamDef, ParamUse, TypeChecker}
import org.junit.Test

class Tuple3Test {
  @Test
  def testTuple3(): Unit = {
    val input1 = CounterInteger(0,1,5)
    val input2 = CounterInteger(0,1,5)
    val input3 = CounterInteger(0,1,5)
    val input1tc = TypeChecker.checkIfUnknown(input1, input1.t)
    val input2tc = TypeChecker.checkIfUnknown(input2, input2.t)
    val input3tc = TypeChecker.checkIfUnknown(input3, input3.t)
    val test = algo.Map(
      {
        val p1 = ParamDef(input1tc.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          p1,
          algo.Map(
            {
              val p2 = ParamDef(input2tc.t.asInstanceOf[SeqTypeT].et)
              AlgoLambda(
                p2,
                algo.Map(
                  {
                    val p3 = ParamDef(input3tc.t.asInstanceOf[SeqTypeT].et)
                    AlgoLambda(
                      p3,
                      algo.Tuple3(ParamUse(p1), ParamUse(p2), ParamUse(p3))
                    )
                  }, input3tc
                )
              )
            }, input2tc
          )
        )
      }, input1tc
    )
    HDLProject("test", test, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(
      )),
      ))).simulateStandalone()
  }

  @Test
  def testSelect3(): Unit = {
    val input1 = CounterInteger(0,1,5)
    val input2 = CounterInteger(0,1,5)
    val input3 = CounterInteger(0,1,5)
    val test = algo.Select3(algo.Tuple3(input1, input2, input3), 0)


    HDLProject("test", test, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(
      )),
      ))).simulateStandalone()
  }
}
