package backend.hdl.conv

import algo.{Add2, AlgoLambda, Drop, Fold, Input, Join, Mul, SeqTypeT, Slide, Split, Transpose, TruncInteger, Tuple2, Zip2}
import algo.util.Matrix
import core.{Expr, ParamDef, ParamUse, TypeChecker}

object ConvTemplate {
  def convHalfCachePaddingChannelMajor(inputWidth: Int, inputHeight:Int, inputChannel: Int,
                                       weightSize:Int, outputChannel: Int, inputPadding: Int,
                                       inputBitWidth: Int, outputBitWidth: Int): Expr ={
    val elementBits = inputBitWidth
    val cacheLineWidth = 512 / elementBits
    val halfCacheLine = cacheLineWidth / 2
    assert(inputWidth % cacheLineWidth == 0)
    val paddedWeightSize = (math.ceil(weightSize * weightSize * inputChannel * 1.0 / cacheLineWidth) * cacheLineWidth).toInt
    val paddedWeightRedundant = paddedWeightSize - inputChannel * weightSize * weightSize

    // Calculate padding and redundant elements from real memory storage
    val paddingWidthCacheLine = ((inputPadding * inputChannel - 1) / halfCacheLine + 1) * halfCacheLine
    val paddingWidthRedundant = halfCacheLine - ((inputPadding * inputChannel - 1) % halfCacheLine + 1)
    val inputWIch = (math.ceil(inputWidth * inputChannel * 1.0 / cacheLineWidth) * cacheLineWidth).toInt
    val inputWIchRedundant = inputWIch - inputWidth * inputChannel
    val paddedHeight = inputHeight + inputPadding * 2
    val paddedInputWIchCacheLine = inputWIch + paddingWidthCacheLine * 2

    val inputFromCache = Input("image", paddedInputWIchCacheLine, paddedHeight, algo.IntType(elementBits))
    val weightFromCache = Input("weight", paddedWeightSize, outputChannel, algo.IntType(elementBits))

    val input = algo.Map(Drop.asFunction(Seq(None), Seq(paddingWidthRedundant, inputWIchRedundant + paddingWidthRedundant)), inputFromCache)
    val weightGroup = TypeChecker.check(algo.Map(Drop.asFunction(Seq(None), Seq(0, paddedWeightRedundant)), weightFromCache))
    val slideInputFirstDim = TypeChecker.check(Slide(input, weightSize))

    val weightParam = ParamDef(weightGroup.t.asInstanceOf[SeqTypeT].et)
    val rowGroupParam = ParamDef(slideInputFirstDim.t.asInstanceOf[algo.SeqType].et)
    val transposeSlideInput = algo.Map(Join.asFunction(), Transpose(algo.Map(Split.asFunction(Seq(None), Seq(inputChannel)), ParamUse(rowGroupParam))))
    val slideInputSecondDim = TypeChecker.check(Slide(transposeSlideInput, weightSize))
    val windowParam = ParamDef(slideInputSecondDim.t.asInstanceOf[algo.SeqType].et)

    val conv2_5d = algo.Map(
      AlgoLambda(
        weightParam,
        algo.Map(
          AlgoLambda(
            rowGroupParam,
            algo.Map(
              AlgoLambda(
                windowParam,
                Fold(Add2.asFunction(),
                  algo.Map(
                    Mul.asFunction(),
                    Zip2(Tuple2(Join(ParamUse(windowParam)), ParamUse(weightParam)))
                  )
                )
              ), slideInputSecondDim
            )
          ), slideInputFirstDim
        )
      ), weightGroup
    )

    // Truncate bit width
    val truncOut = algo.Map(3, TruncInteger.asFunction(Seq(None), Seq(outputBitWidth)), conv2_5d)

    // transpose data
    algo.Map(Join.asFunction(), Split(Transpose(algo.Map(Join.asFunction(), truncOut)), inputWidth))
  }
}
