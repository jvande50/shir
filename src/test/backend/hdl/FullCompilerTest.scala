package backend.hdl

import algo._
import algo.util.Matrix
import backend.hdl.arch.mem.MemoryImage
import core._
import core.util.IRDotGraph
import org.junit.Test

class FullCompilerTest {

  @Test
  def testGenerateConstantStream(): Unit = {
    Counter.resetAll()
    val tree = Repeat(ConstantInteger(60000, Some(algo.IntType(16))), ArithType(10))

    HDLProject("constantstream", tree).compileHDL
  }

  @Test
  def testGenerateCounterStream(): Unit = {
    Counter.resetAll()
    val tree = Repeat(CounterInteger(2, 2, 10), ArithType(10))

    HDLProject("counterstream", tree).compileHDL
  }

  @Test
  def testReshapeStream(): Unit = {
    Counter.resetAll()
    val tree = Sink(Split(Join(Repeat(Repeat(CounterInteger(2, 2, 10), 3), 2)), 2))

    HDLProject("reshapestream", tree).compileHDL
  }

  @Test
  def testMapEmptyLambda(): Unit = {
    Counter.resetAll()
    val tree =
      Map(
        {
          val p = ParamDef()
          AlgoLambda(p, ParamUse(p))
        },
        CounterInteger(0, 1, 10)
      )

    HDLProject("mapemptylambda", tree).compileHDL
  }

  @Test
  def testDotProduct(): Unit = {
    Counter.resetAll()
    val tree = DotProduct(CounterInteger(1, 0, 8), CounterInteger(4, 0, 8))

    HDLProject("dotproduct", tree).compileHDL //.simulateStandalone
  }

  @Test
  def testMemInput(): Unit = {
    Counter.resetAll()
    val tree = Input("input", 4, 1, algo.IntType(512))
    HDLProject("meminput", tree).simulateWithHostRam(
      Predef.Map(
        "input" -> Seq(Seq(1, 2, 3, 4)),
        "result" -> Seq(Seq(1, 2, 3, 4)),
      )
    )
  }

  @Test
  def testMemInputPrecisions(): Unit = {
    Counter.resetAll()
    val tree1 = Input("input", 2, 4, algo.IntType(3)) // row smaller than cacheline
    HDLProject("meminput", tree1).compileHDL
    Counter.resetAll()
    val tree2 = Input("input", 3, 1, algo.IntType(200)) // row bigger than cacheline
    HDLProject("meminput", tree2).compileHDL
    Counter.resetAll()
    val tree3 = Input("input", 2, 2, algo.IntType(500)) // element almost as big as cacheline
    HDLProject("meminput", tree3).compileHDL
    Counter.resetAll()
    val tree4 = Input("input", 2, 2, algo.IntType(800)) // element bigger than cacheline
    HDLProject("meminput", tree4).compileHDL
    Counter.resetAll()
    val tree5 = Input("input", 2, 2, algo.IntType(1200)) // element bigger two cachelines
    HDLProject("meminput", tree5).compileHDL
  }

  @Test
  def testDotProductMemInput(): Unit = {
    Counter.resetAll()
    val tree = Repeat(Repeat(DotProduct(Input("varA", algo.IntType(16), 10), Input("varB", algo.IntType(16), 10)), 1), 1)
    HDLProject("dotproduct", tree).compileHDL
  }

  @Test
  def testSumRow1(): Unit = {
    Counter.resetAll()
    val matrix = Matrix.fromRandom(4, 4, 65536)
    val input = Input("matrix", 4, 4, algo.IntType(16))
    val tree = Split(Map(
      {
        val param = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          Fold(Add2.asFunction(), ParamUse(param))
        )
      },
      input
    ), 4)
    HDLProject("rowsum", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.sumRows().data)
    ).assertCorrect.print()
  }

  @Test
  def testSumRow2(): Unit = {
    Counter.resetAll()
    val matrix = Matrix.fromRandom(128, 128, 65536)
    val input = Input("matrix", 128, 128, algo.IntType(16))
    val tree = Split(Map(
      {
        val param = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          Fold(Add2.asFunction(), ParamUse(param))
        )
      },
      input
    ), 128)
    HDLProject("rowsum", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.sumRows().data)
    ).assertCorrect.print()
  }

  @Test
  def testSumRow3(): Unit = {
    Counter.resetAll()
    val matrix = Matrix.fromRandom(64, 64, 65536)
    val input = Input("matrix", 64, 64, algo.IntType(16))
    val tree = Split(Map(
      {
        val param = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          Fold(Add2.asFunction(), ParamUse(param))
        )
      },
      input
    ), 64)
    HDLProject("rowsum", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.sumRows().data)
    ).assertCorrect.print()
  }

  @Test
  def testIncMatrix1(): Unit = {
    val matrix = Matrix.fromRandom(64, 64, 64)
    Counter.resetAll()
    val tree = Map(
      Map.asFunction(Seq(Some(Inc.asFunction()), None)),
      Input("matrix", 64, 64, algo.IntType(8))
    )
    HDLProject("inc", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.inc().data)
    ).assertCorrect.print()
  }

  @Test
  def testIncMatrix2(): Unit = {
    val matrix = Matrix.fromRandom(128, 128,256)
    Counter.resetAll()
    val tree = Map(
      Map.asFunction(Seq(Some(Inc.asFunction()), None)),
      Input("matrix", 128, 128, algo.IntType(16))
    )
    HDLProject("inc", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.inc().data)
    ).assertCorrect.print()
  }

  @Test
  def testIncMatrix3(): Unit = {
    val matrix = Matrix.fromRandom(1, 128,256)
    Counter.resetAll()
    val tree = Map(
      Map.asFunction(Seq(Some(Inc.asFunction()), None)),
      Input("matrix", 128, 1, algo.IntType(16))
    )
    HDLProject("inc", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.inc().data)
    ).assertCorrect.print()
  }

  @Test
  def testIncMatrix4(): Unit = {
    val matrix = Matrix.fromRandom(2, 128,256)
    Counter.resetAll()
    val tree = Map(
      Map.asFunction(Seq(Some(Inc.asFunction()), None)),
      Input("matrix", 128, 2, algo.IntType(16))
    )
    HDLProject("inc", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.inc().data)
    ).assertCorrect.print()
  }

  @Test
  def testIncMatrix5(): Unit = {
    val matrix = Matrix.fromRandom(1, 16*128,256)
    Counter.resetAll()
    val tree = Map(
      Map.asFunction(Seq(Some(Inc.asFunction()), None)),
      Input("matrix", 16*128, 1, algo.IntType(16))
    )
    HDLProject("inc", tree).simulateWithHostRam(
      Predef.Map("matrix" -> matrix.data, MemoryImage.RESULT_VAR_NAME -> matrix.inc().data)
    ).assertCorrect.print()
  }

  @Test
  def testMV(): Unit = {
    Counter.resetAll()
    val tree = MVMul(CounterInteger(1, 1, Seq(2, 2), Some(algo.IntType(8))), CounterInteger(1, 1, Seq(2), Some(algo.IntType(8))))
    HDLProject("mv", tree).compileHDL //.simulateStandalone
  }

  @Test
  def testMVMemInput(): Unit = {
    Counter.resetAll()
    val tree = Split(MVMul(Input("matrix", 2, 2, algo.IntType(8)), Join(Input("vector", 2, 1, algo.IntType(16)))), 2)
    HDLProject("mv", tree).compileHDL
  }

  @Test
  def testMM(): Unit = {
    Counter.resetAll()
    val tree = MMMul(CounterInteger(1, 1, Seq(2, 2), None), CounterInteger(11, 1, Seq(2, 2), None))
    HDLProject("mm", tree).compileHDL //simulateStandalone
  }

  @Test
  def testJoinOrderedStream(): Unit = {
    Counter.resetAll()
    val tree = Join(Repeat(Repeat(CounterInteger(2, 2, 10), 10), 4))
    HDLProject("join", tree).compileHDL
  }

  @Test
  def testSlide(): Unit = {
    Counter.resetAll()
    val tree = Slide(CounterInteger(0, 1, 10), 3)

    HDLProject("slide", tree).compileHDL
  }

  @Test
  def testBuffer(): Unit = {
    Counter.resetAll()
    val tree = Split(Buffer(Join(Input("input", 4, 1, algo.IntType(3)))), 4)
    HDLProject("buffer", tree).simulateWithHostRam(
      Predef.Map(
        "input" -> Seq(Seq(1, 2, 3, 4)),
        "result" -> Seq(Seq(1, 2, 3, 4)),
      )
    ).assertCorrect.print()
  }

  @Test
  def testFold2D(): Unit = {
    val input = Split(CounterInteger(0, 1, 64), 8)
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val tree =
      Fold(
        {
          val p1 = ParamDef()
          val p2 = ParamDef()
          AlgoLambda(Seq(p1, p2),
            algo.Map(
              Add.asFunction(),
              Zip2(Tuple2(ParamUse(p1), ParamUse(p2)))
            )
          )
        },
        inputtc
      )

    HDLProject("fold2D", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testInputClash(): Unit = {
    val tree = Map(Zip2.asFunction(), Zip2(Tuple2(
        Input("matrix", 64, 64, algo.IntType(8)),
        Input("matrix", 64, 64, algo.IntType(8))
    )))

    HDLProject("inputs", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testIfelse(): Unit = {
    val input = CounterInteger(0, 1, 8)
    val cond = Join(Repeat(CounterInteger(0, 1, 2), 4))
    val inTuple = Zip2(Tuple2(input, cond))
    val f1 = {
      val p = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
      AlgoLambda(p, Add(Tuple2(ParamUse(p), ConstantInteger(1, Some(algo.IntType(3))))))
    }
    val f2 = {
      val p = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
      AlgoLambda(p, Add(Tuple2(ParamUse(p), ConstantInteger(2, Some(algo.IntType(3))))))
    }
    val tree = algo.Map(
      {
        val p = ParamDef()
        AlgoLambda(
          p,
          Ifelse(f1, f2, ParamUse(p))
        )
      }, inTuple
    )

    HDLProject("inputs", tree).compileHDL//simulateStandalone()
  }

  @Test
  def reduce(): Unit = {
    Counter.resetAll()
    val initValue = ConstantInteger(0, Some(algo.IntType(8)))
    val input = CounterInteger(0, 1, Seq(3), Some(algo.IntType(8)))

    val tree = Reduce(
      {
        val accParam = ParamDef()
        val addParam = ParamDef()
        AlgoLambda(
          Seq(accParam, addParam),
          Add(Tuple2(
            ParamUse(accParam),
            ParamUse(addParam)
          ))
        )
      },
      initValue,
      input
    )

    HDLProject("reduce", tree).compileHDL
  }

  @Test
  def testMax(): Unit = {
    Counter.resetAll()
    val tree = Max(Tuple2(ConstantInteger(1, Some(algo.IntType(8))), ConstantInteger(3, Some(algo.IntType(8)))))
    HDLProject("max", tree).compileHDL //simulateStandalone
  }

  @Test
  def testFoldMax(): Unit = {
    val input = CounterInteger(0, 1, 8)
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val tree =
      Fold(
        Max2.asFunction(),
        inputtc
      )

    HDLProject("foldMax", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testFoldMax2D(): Unit = {
    val input = Split(CounterInteger(0, 1, 64), 8)
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val tree =
      Fold(
        {
          val p1 = ParamDef()
          val p2 = ParamDef()
          AlgoLambda(Seq(p1, p2),
            algo.Map(
              Max.asFunction(),
              Zip2(Tuple2(ParamUse(p1), ParamUse(p2)))
            )
          )
        },
        inputtc
      )

    HDLProject("foldMax2D", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testFold2DSigned(): Unit = {
    val input = Split(algo.Map(Signed.asFunction(), CounterInteger(0, 1, 64)), 8)
    //val input = Split(CounterInteger(0, 1, 64), 8)
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val tree =
      Fold(
        {
          val p1 = ParamDef()
          val p2 = ParamDef()
          AlgoLambda(Seq(p1, p2),
            algo.Map(
              Add.asFunction(),
              Zip2(Tuple2(ParamUse(p1), ParamUse(p2)))
            )
          )
        },
        inputtc
      )

    HDLProject("fold2DSigned", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testSignedSum(): Unit = {
    Counter.resetAll()

    val tree = TypeChecker.check(Fold(Add2.asFunction(), TypeChecker.check(algo.Map(Signed.asFunction(), CounterInteger(0, 1, 8)))))

    HDLProject("signedSum", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testSigned(): Unit = {
    Counter.resetAll()
    val tree = Input("input", 4, 1, algo.SignedIntType(4))
    HDLProject("SignedInput", tree).simulateWithHostRam(
      Predef.Map(
        "input" -> Seq(Seq(-1, -2, 3, 4)),
        "result" -> Seq(Seq(-1, -2, 3, 4)),
      )
    ).assertCorrect.print()
  }

  @Test
  def testBufferHost(): Unit = {
    Counter.resetAll()
    val tree = BufferHost(Input("input", 2, 2, algo.IntType(512)), TextType("tmp"))
    HDLProject("bufferHost", tree).simulateWithHostRam(
      Predef.Map(
        "input" -> Seq(Seq(1, 2), Seq(3, 4)),
        "tmp" -> Seq(Seq(1, 2), Seq(3, 4)),
        "result" -> Seq(Seq(1, 2), Seq(3, 4)),
      )
    ).assertCorrect.print()
  }

  @Test
  def testReLU(): Unit = {
    Counter.resetAll()
    val tree = ReLU(ConstantInteger(10))
    HDLProject("max", tree).compileHDL //simulateStandalone()
  }

  @Test
  def testConstIntSeq1(): Unit = {
    Counter.resetAll()
    val tree = ConstantSeq(Seq(1, 2, 3, -4))

    HDLProject("ConstantSeq", tree).compileHDL //simulateStandalone()
  }

  @Test
  def testConstIntVector2(): Unit = {
    Counter.resetAll()
    val tree = ConstantSeq(Seq(1, 2, 3, 4), Some(algo.SignedIntType(512)))

    HDLProject("ConstantSeq", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testSeq1ToInt(): Unit = {
    Counter.resetAll()
    val tree = Item(CounterInteger(0, 1, Seq(1), Some(algo.IntType(3))))

    HDLProject("testSeq1ToInt", tree).compileHDL
  }

  @Test
  def testAddSigned(): Unit = {
    Counter.resetAll()
    val tree = TypeChecker.check(Add2(ConstantInteger(-1, Some(algo.SignedIntType(2))), Conversion(ConstantInteger(0, Some(algo.IntType(3))), algo.SignedIntType(3))))

    HDLProject("AddSigned", tree).compileHDL
  }
  @Test
  def testClipRound(): Unit = {
    Counter.resetAll()
    val tree = ClipBankersRound(ConstantInteger(-126, Some(algo.SignedIntType(8))), 3, 2)

    HDLProject("clip-round", tree).compileHDL
  }

  @Test
  def testSignedExtension(): Unit = {
    Counter.resetAll()
    val tree = ResizeInteger(ConstantInteger(10, Some(algo.SignedIntType(8))), 16)
    HDLProject("testSignedExtension", tree).compileHDL
  }

  @Test
  def testNegativeNumber(): Unit = {
    Counter.resetAll()
    val tree = ConstantInteger(-8, Some(algo.SignedIntType(4)))
    HDLProject("testNegativeNumber", tree).compileHDL //simulateStandalone()
  }

  @Test
  def testRepeatAll(): Unit = {
    Counter.resetAll()
    val tree = RepeatAll(ConstantInteger(0, Some(algo.SignedIntType(4))), Seq(1, 2, 3))
    HDLProject("testRepeatAll", tree).compileHDL //simulateStandalone()
  }

  @Test
  def testSlideGeneral(): Unit = {
    Counter.resetAll()
    val tree = SlideGeneral(CounterInteger(0, 1, 17), 3, 2)
    HDLProject("testSlideGeneral", tree).compileHDL
  }

  @Test
  def testSlideGeneralHighDimension(): Unit = {
    Counter.resetAll()
    val tree = SlideGeneral(CounterInteger(0, 1, Seq(3, 4, 5, 17), None), 3, 2)
    HDLProject("testSlideGeneralHighDimension", tree).compileHDL
  }

  @Test
  def testPad(): Unit = {
    Counter.resetAll()
    val tree = Pad(CounterInteger(0, 1, Seq(3, 4, 5, 17), None), 3, 2, 0)
    HDLProject("testPad", tree).compileHDL
  }

  @Test
  def testPadPartial(): Unit = {
    Counter.resetAll()
    val tree = Pad(CounterInteger(0, 1, Seq(3, 4, 5, 17), None), 3, 2, 0)
    HDLProject("testPadPartial", tree).compileHDL
  }

  @Test
  def testMultipleStreamParamUse(): Unit = {
    Counter.resetAll()
    val input = Input("input", algo.IntType(8), 3, 1)
    val inputParam = ParamDef(input.t)
    val fun = AlgoLambda(inputParam, Select3(Id(Tuple3(ParamUse(inputParam), ParamUse(inputParam), ParamUse(inputParam))), 2))
    val tree = FunctionCall(fun, input)
    HDLProject("testMultipleStreamParamUse", tree).simulateWithHostRam(
      Predef.Map(
        "input" -> Seq(Seq(3, 4, 5)),
        MemoryImage.RESULT_VAR_NAME -> Seq(Seq(3, 4, 5))
      )
    ).print()
  }

  @Test
  def testTransposeND1(): Unit = {
    Counter.resetAll()
    val tree = TransposeND(CounterInteger(0, 1, Seq(3, 3, 3, 3, 1), None), Seq(0, 2, 4, 1, 3))
    HDLProject("testTransposeND1", tree).compileHDL
  }

  @Test
  def testTransposeND2(): Unit = {
    Counter.resetAll()
    val tree = TransposeND(Id(CounterInteger(0, 1, Seq(3, 3, 3, 3, 1), None)), Seq(0, 2, 4, 1, 3))
    HDLProject("testTransposeND2", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testSeqToUpperBoundedSeq(): Unit = {
    Counter.resetAll()
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(8), Some(algo.IntType(8))))
    val tree = SeqToUpperBoundedSeq(input, 16)
    HDLProject("testSeqToUpperBoundedSeq", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testFoldDifferentSizes(): Unit = {
    Counter.resetAll()
    val param = ParamDef(UpperBoundedSeqType(algo.IntType(8), 16))
    val fun =
      AlgoLambda(param, FoldUpperBoundedSeq(
        {
          val p1 = ParamDef()
          val p2 = ParamDef(algo.IntType(8))
          AlgoLambda(Seq(p1, p2),
            Add(Tuple(ParamUse(p1), ParamUse(p2)))
          )
        },
        ParamUse(param)
      ))
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(8), Some(algo.IntType(8))))
    val tree = FunctionCall(fun, SeqToUpperBoundedSeq(input, 16))
    HDLProject("testFoldDifferentSizes", tree).compileHDL//simulateStandalone()
  }

  @Test
  def mapAsyncTest: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5, 5), Some(algo.IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(5, 5), Some(algo.IntType(8)))
    val param1 = ParamDef(input1.t.asInstanceOf[SeqTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[SeqTypeT].et)
    val fun = TypeChecker.check(AlgoLambda(param1, AlgoLambda(param2, Concat(ParamUse(param1), ParamUse(param2)))))
    val tree = MapAsync(1, fun, input1, input2)

    HDLProject("map2Input", tree).compileHDL//simulateStandalone()
  }
}
