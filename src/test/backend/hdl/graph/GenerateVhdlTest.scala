package backend.hdl.graph

import backend.hdl.vhdl.{VhdlGenerator, VhdlProject}
import backend.hdl.{LogicType, VectorType}
import org.junit.Test

class GenerateVhdlTest {

  @Test
  def testPartialConnections(): Unit = {
    val vType = VectorType(LogicType(), 2)
    val nodeA = Node("A", Seq(PortOutHandshake(vType)), None)
    val nodeB = Node("B", Seq(PortInHandshake(vType)), None)
    val nodeC = Node("C", Seq(PortOutHandshake(vType)), None)
    val nodeD = Node("D", Seq(PortInHandshake(vType)), None)

    val connections =
      nodeA.portsOutHandshake.head.connectTo(nodeB.portsInHandshake.head, Some(0), Some(0)) ++
      nodeA.portsOutHandshake.head.data.connectTo(nodeD.portsInHandshake.head.data, Some(1), Some(0)) ++
      nodeC.portsOutHandshake.head.data.connectTo(nodeB.portsInHandshake.head.data, Some(0), Some(1)) ++
      nodeC.portsOutHandshake.head.connectTo(nodeD.portsInHandshake.head, Some(1), Some(1))

    val top = TopNode(Graph(Seq(nodeA, nodeB, nodeC, nodeD), connections))

    VhdlProject(VhdlGenerator.generate(top)).toFiles("out/partialconnections")
  }
}
