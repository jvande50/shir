package core

import algo._
import backend.hdl.HWFunType
import org.junit.Test

class SubTypingTest {

  @Test
  def test(): Unit = {
    val dtv = BuiltinDataTypeVar()
    val arit1 = ArithType(3)
    val arit2 = ArithType(4)
    val ft1 = FunType(dtv, dtv)
    val ft2 = FunType(dtv, dtv)
    val ft3 = FunType(FloatHalfType(), dtv)
    val st1 = SeqType(FloatSingleType(), ArithType(3))
    val st2 = SeqType(AbstractScalarTypeVar(), ArithType(3))
    val st3 = SeqType(IntType(32), ArithType(3))
    val st4 = SeqType(AbstractScalarTypeVar(), ArithType(4))
    val anyType = new AnyType

    assert(dtv.getCommonSuperKind(ft1) == AnyTypeKind)
    assert(anyType.getCommonSuperKind(dtv) == AnyTypeKind)
    assert(ft1.getCommonSuperKind(ft2) == FunTypeKind)
    assert(st1.getCommonSuperKind(st2) == SeqTypeKind)

    assert(arit1.isSubTypeOf(arit2) == Option(false))

    assert(ft1.isSubTypeOf(ft2) == Option(true))
    assert(ft2.isSubTypeOf(ft1) == Option(true))
    assert(ft1.isSubTypeOf(dtv) == Option(false))

    assert(ft1.isSubTypeOf(ft3).isEmpty)
    assert(ft3.isSubTypeOf(ft1).isEmpty)

    assert(st1.isSubTypeOf(st2).isEmpty)
    assert(st1.isSubTypeOf(st3) == Option(false))

    assert(st1.isSubTypeOf(st4) == Option(false))
  }

  @Test
  def testFunSubTyping(): Unit = {
    val ft1 = FunType(IntType(3), IntType(3))
    val ft2 = HWFunType(IntType(3), IntType(3))
    val ft3 = AlgoFunType(IntType(3), IntType(3))

    assert(ft2.isSubTypeOf(ft1) == Option(true))
    assert(ft3.isSubTypeOf(ft1) == Option(true))
    assert(ft2.isSubTypeOf(ft3) == Option(false))
    assert(ft3.isSubTypeOf(ft2) == Option(false))

    val ftv = AlgoFunTypeVar(AnyTypeVar(), AnyTypeVar())
    val ft4 = FunType(AnyTypeVar(), AnyTypeVar())
    assert(ft4.isSubTypeOf(ftv) == Option(false))
    assert(ftv.isSubTypeOf(ft4).isEmpty)
  }

}
