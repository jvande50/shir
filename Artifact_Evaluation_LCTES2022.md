# Artifact Evaluation LCTES 2022

## Getting Started Guide

This artifact fully covers all the experiments in section 4 of the LCTES 2022 paper "Optimizing Data Reshaping Operations in Functional IRs for High-Level Synthesis".
Starting from high-level expressions as described in sections 4.1 and 4.2 for tiled matrix multiplication and 2D convolution, the compiler automatically generates an optimized hardware design based on VHDL files.
On the FPGA server, a bitstream for an FPGA is synthesised from these generated design files. Then the design is run on a real FPGA to get all the performance numbers and resource usages of Table 2 and Table 3.

## Step-by-Step Instructions

First of all, connect to the server (via SSH) to run the following commands of this guide.

### Compile SHIR experiments

```bash
$ git clone https://bitbucket.org/cdubach/shir.git --branch lctes22-artifact
$ cd shir
$ ./updateSubmodules.sh
$ for i in {1..9}; do sbt "testOnly datareshaping.Expt$i" ; done
```

This will compile all the experiments 1-9 and create the folders "out/expt1" - "out/expt9" with the corresponding VHDL files. The tests must be started separately, e.g. by using a for loop as shown above, to reset the compiler state between the runs.

### Synthesize using Quartus

- navigate to the generated VHDL files

```bash
$ cd out
```

- navigate to one of the experiments, e.g. `expt1`, and copy the shir wrapper files into it:

```bash
$ cd expt1
$ cp -r ../../host/shir_wrapper/* .
$ mkdir hw/rtl/generated
$ mv *.vhd *.dat hw/rtl/generated/
```

Run the freshly copied `real.sh` script to start the synthesis for the current experiment:

```bash
$ ./real.sh
```

This may take a while (up to a few hours per experiment)! However, you can synthesise the other experiments at the same time by running the synthesis in the background (e.g. using `tmux` or `screen`). Just navigate into another experiment's directory, copy the wrapper files, move the generated files and execute `real.sh` again, as described above.

According to Table 2 and Table 3, experiments 1, 5 and 6 are not synthesizable. Their synthesis processes will terminate with an error message and no bitstream for the FPGA will be generated.

### Run SHIR experiments on Intel Arria 10 FPGA

The other experiments 2-4 and 7-9 are synthesizable and can be run on the FPGA.
Once the synthesis has finished, run the following two scripts to install the hardware design on the FPGA and to compile and run the software part, which will trigger the FPGA:

```bash
$ ./real_start.sh
$ ./real_sw.sh
```

The output will show "Result is correct!" if the FPGAs result matches the result computed by the reference CPU implementation. Moreover, the execution time as a number of cycles is shown, as well as the resource usage, which is extracted from synthesis reports in the `build_synth/build/output_files` directory. There you can find the text file `afu_default.fit.summary`, which provides a useful summary.

#### Resource Usage (Table 2 and Table 3)

The printed values from the synthesis reports are used to fill the columns "Logic blocks", "On-chip RAM" and "DSP blocks" in both Table 2 and Table 3. For the on-chip RAM usage, only the number of RAM blocks it considered. Note, that each DSP block contains two multipliers. In the following calculations, we will use the number of multipliers, when referring to "DSPs".

#### Tiled Matrix Multiplication Performance Numbers (Table 2)

In matrix multiplication with 4096x4096 square matrices, the total number of operations is 4096^3.
The FPGA runs with a clock frequency of 200 MHz and the design for matrix multiplication uses 2048 DSPs.
With all the above constants and the number of cycles, the performance numbers from Table 2 are calculated as follows:

- GOPS = 4096^3 / (#cycles / #clockFreq) / 1000^3, with #clockFreq = 200,000,000 Hz
- OPC = 4096^3 / #cycles
- DSP efficiency = (4096^3 / #cycles) / #DSPs, with #DSPs = 2048

#### 2D Convolution Performance Numbers (Table 3)

In the 2D convolution experiments, the amount of data transferred to the FPGA (3159756 B) and from the FPGA (67108864 B) is 70268620 B = 0.065443 GB in total.
With a separate memcopy experiment, the maximum throughput of 6.5 GB/s was determined. This value is used to put the speed of convolution into perspective.
To calculate the result of the 2D convolution, the FPGA has to perform the following number of operations: imageHeight * imageWidth * outputChannels * windowHeight * windowWidth * inputChannels = 1024 * 1024 * 64 * 3 * 3 * 3 = 1811939328 OPs
The clock frequency is still 200 MHz, but the design uses 864 DSPs this time.
Now, the relative throughput and the DSP efficiency from Table 3 are calculated as follows:

- Throughput = (0.065443 GB / (#cycles / #clockFreq)) / #maxThroughput, with #clockFreq = 200,000,000 Hz and $maxThroughput = 6.5 GB/s
- DSP efficiency = (1024 * 1024 * 64 * 3 * 3 * 3 / #cycles) / #DSPs, with #DSPs = 864

