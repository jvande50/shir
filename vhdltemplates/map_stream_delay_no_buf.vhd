library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity map_stream_delay is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type;
        port_in_control_last: in last_1_type;
        port_in_control_ready: in ready_1_type
    );
end map_stream_delay;

architecture behavioral of map_stream_delay is
    constant control_last_all_one: std_logic_vector(port_in_control_last'range) := (others => '1');
begin

    port_out_data <= port_in_data;
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;

    delay_ready_signal: process(port_out_ready, port_in_control_last, port_in_control_ready)
    begin
        if port_in_control_last /= control_last_all_one then
            port_in_ready <= port_out_ready;
            port_in_ready(port_in_ready'high) <= '0';
        else
            port_in_ready <= port_in_control_ready(port_in_control_ready'high downto port_in_control_ready'high - port_in_ready'length + 1);
        end if;
    end process;

end behavioral;

