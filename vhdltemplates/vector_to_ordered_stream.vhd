library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity vector_to_ordered_stream is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end vector_to_ordered_stream;

architecture behavioral of vector_to_ordered_stream is
    signal cnt: natural range 0 to port_in_data'high := 0;
begin

    port_out_data <= port_in_data(cnt);
    port_out_last <= "1" when cnt = port_in_data'high else "0";
    port_out_valid <= port_in_valid;

    process(cnt, port_out_ready)
    begin
        port_in_ready <= (others => '0');
        if cnt = port_in_data'high and port_out_ready(port_out_ready'high-1) = '1' then
            port_in_ready <= (others => '1');
            port_in_ready(port_in_ready'high) <= port_out_ready(port_out_ready'high);
        end if;
    end process;

    send_counter: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                cnt <= 0;
            else
                if port_in_valid = '1' and port_out_ready(port_out_ready'high-1) = '1' then
                    if cnt < port_in_data'high then
                        cnt <= cnt + 1;
                    else
                        cnt <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
