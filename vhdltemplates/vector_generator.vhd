library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity vector_generator is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out data_word_vector_type;
        port_out_valid: out std_logic;
        port_out_ready: in std_logic
    );
end vector_generator;

architecture behavioral of vector_generator is

    component counter -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in std_logic -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    signal port_f_in_data: data_word_type; -- %TESTING_ONLY
    signal port_f_in_valid: std_logic; -- %TESTING_ONLY
    signal port_f_in_ready: std_logic; -- %TESTING_ONLY

begin

    f: counter port map( -- %TESTING_ONLY
        clk => clk, -- %TESTING_ONLY
        reset => reset, -- %TESTING_ONLY
        port_out_data => port_f_in_data, -- %TESTING_ONLY
        port_out_valid => port_f_in_valid, -- %TESTING_ONLY
        port_out_ready => port_f_in_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    port_out_data <= (others => port_f_in_data);
    port_out_valid <= port_f_in_valid;
    port_f_in_ready <= port_out_ready;

end behavioral;
