library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity banked_ram_buffer is
    generic(
        entries: natural := 8;
        data_width: natural := 32;
        num_banks: natural := 4
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in memory_input_vector_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_vector_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end banked_ram_buffer;

architecture behavioral of banked_ram_buffer is
    constant banked_data_width: natural := data_width / num_banks;
    constant word_per_bank: natural := port_in_data.data'length / num_banks;
    component ram_sp
        generic (
            data_width: natural := 32;
            addr_width: natural := 4;
            entries: natural := 8
        );
        port(
            clk: in std_logic;
            data_in: in std_logic_vector(banked_data_width - 1 downto 0);
            addr: in std_logic_vector(addr_width - 1 downto 0);
            we: in std_logic;
            data_out: out std_logic_vector(banked_data_width - 1 downto 0)
        );
    end component;

    constant single_word_width: natural := port_in_data.data(0)'length;
    type banked_data_word_vector_type is array (num_banks-1 downto 0) of std_logic_vector(banked_data_width - 1 downto 0);
    type addr_vector_type is array (num_banks-1 downto 0) of std_logic_vector(port_in_data.addr(0)'range);
    signal ram_in_we: std_logic_vector(num_banks-1 downto 0) := (others => '0');
    signal ram_in_addr: addr_vector_type := (others => (others => '0'));
    signal ram_out_valid: std_logic := '0';
    signal ram_out_data: banked_data_word_vector_type := (others => (others => '0'));
    signal ram_in_data: banked_data_word_vector_type := (others => (others => '0'));

    signal pipeline_count: natural range 0 to 1 := 0;

    type data_word_vector_type is array (port_in_data.data'range) of std_logic_vector(port_in_data.data(0)'range);
    type output_buffer_type is array (0 to 2) of data_word_vector_type;
    signal output_buffer_in: data_word_vector_type := (others => (others => '0'));
    signal output_buffer: output_buffer_type := (others => (others => (others => '0')));
    signal output_buffer_count: natural range 0 to (output_buffer_type'high + 1) := 0;

    subtype output_buffer_index_type is natural range output_buffer_type'range;
    signal next_free_output_buffer_entry: output_buffer_index_type := 0;
    signal first_used_output_buffer_entry: output_buffer_index_type := 0;

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');
    signal out_valid: std_logic := '0';

begin

    IN_DATA_CONVERT: process(port_in_data.data)
    begin
        for i in num_banks-1 downto 0 loop
            for j in word_per_bank-1 downto 0 loop
                ram_in_data(i)((j+1) * single_word_width - 1 downto j*single_word_width) <= port_in_data.data(i * word_per_bank + j);
            end loop;
        end loop;
    end process;

    OUT_DATA_CONVERT: process(ram_out_data)
    begin
        for i in num_banks-1 downto 0 loop
            for j in word_per_bank-1 downto 0 loop
                output_buffer_in(i * word_per_bank + j) <= ram_out_data(i)((j+1) * single_word_width - 1 downto j*single_word_width);
            end loop;
        end loop;
    end process;

    BANK_GEN: for I in num_banks-1 downto 0 generate
        inferred_ram: ram_sp
        generic map(
            data_width => banked_data_width,
            addr_width => port_in_data.addr(0)'length,
            entries => entries
        )
        port map(
            clk => clk,
            data_in => ram_in_data(I),
            addr => ram_in_addr(I),
            we => ram_in_we(I),
            data_out => ram_out_data(I)
        );
    end generate BANK_GEN;

    ADDR_CONVERT: process(port_in_data.addr)
    begin
        for i in num_banks-1 downto 0 loop
            ram_in_addr(i) <= port_in_data.addr(i);
        end loop;
    end process;

    BANK_WE_CTRL: process(port_in_data.we, port_in_valid)
    begin
        for i in num_banks-1 downto 0 loop
            ram_in_we(i) <= port_in_data.we(i) and port_in_valid;
        end loop;
    end process;


    in_ready <= "1" when output_buffer_count + pipeline_count <= output_buffer_type'high else "0";
    port_in_ready <= in_ready;

    output_conversion: process(output_buffer, first_used_output_buffer_entry)
    begin
        for i in port_out_data'range loop
            for j in port_out_data(0)'range loop
                port_out_data(i)(j) <= output_buffer(first_used_output_buffer_entry)(i)(j);
            end loop;
        end loop;
    end process;
    port_out_last <= (others => '0');
    out_valid <= '1' when output_buffer_count > 0 else '0';
    port_out_valid <= out_valid;

    pipeline_counter: process(clk)
        variable pipeline_count_v: natural range 0 to (1 + 1) := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                pipeline_count <= 0;
            else
                pipeline_count_v := pipeline_count;
                if port_in_valid = '1' and in_ready = "1" then
                    pipeline_count_v := pipeline_count_v + 1;
                end if;
                if ram_out_valid = '1' then
                    pipeline_count_v := pipeline_count_v - 1;
                end if;
                pipeline_count <= pipeline_count_v;
            end if;
        end if;
    end process;

    output_buffer_logic: process(clk)
        variable output_buffer_count_v: natural range 0 to (output_buffer_type'high + 2) := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                output_buffer <= (others => (others => (others => '0'))); -- enfore the use of registers instead of ram blocks
                output_buffer_count <= 0;
                next_free_output_buffer_entry <= 0;
                first_used_output_buffer_entry <= 0;
            else
                -- router hat problems with the reset signal for this output buffer
                if output_buffer_count = 0 then
                    output_buffer <= (others => (others => (others => '0'))); -- enfore the use of registers instead of ram blocks
                end if;

                output_buffer_count_v := output_buffer_count;
                if ram_out_valid = '1' then
                    output_buffer(next_free_output_buffer_entry) <= output_buffer_in;
                    output_buffer_count_v := output_buffer_count_v + 1;
                    if next_free_output_buffer_entry = output_buffer_index_type'high then
                        next_free_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        next_free_output_buffer_entry <= next_free_output_buffer_entry + 1;
                    end if;
                end if;
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    output_buffer_count_v := output_buffer_count_v - 1;
                    if first_used_output_buffer_entry = output_buffer_index_type'high then
                        first_used_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        first_used_output_buffer_entry <= first_used_output_buffer_entry + 1;
                    end if;
                end if;
                output_buffer_count <= output_buffer_count_v;
            end if;
        end if;
    end process;

    pipeline_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                ram_out_valid <= '0';
            else
                ram_out_valid <= '0';
                if port_in_valid = '1' and in_ready = "1" then
                    ram_out_valid <= '1';
                end if;
            end if;
        end if;
    end process;

end behavioral;
