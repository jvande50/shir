library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity read_sync_banked is
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- from controller
        port_mem_in_data: in data_word_vector_type;
        port_mem_in_last: in last_0_type;
        port_mem_in_valid: in std_logic;
        port_mem_in_ready: out ready_0_type;
        -- to controller
        port_mem_out_data: out mem_address_vector_type;
        port_mem_out_last: out last_0_type;
        port_mem_out_valid: out std_logic;
        port_mem_out_ready: in ready_0_type;
        -- address vector
        port_in_data: in data_word_vector_stream_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        -- base address (added to address)
        port_in_baseaddr_data: in base_address_type;
        port_in_baseaddr_last: in last_0_type;
        port_in_baseaddr_valid: in std_logic;
        port_in_baseaddr_ready: out ready_0_type;

        -- outgoing data vector
        port_out_data: out data_word_vector_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end read_sync_banked;

architecture behavioral of read_sync_banked is
begin
    ADDR_CONVERT: process(port_in_baseaddr_data, port_in_data)
    begin
        for i in port_mem_out_data'high downto 0 loop
            port_mem_out_data(i) <= std_logic_vector(to_unsigned(
                              to_integer(unsigned(port_in_baseaddr_data)) +
                              to_integer(unsigned(port_in_data(i))),
                              port_mem_out_data(0)'length));
        end loop;
    end process;
    --port_mem_out_data <= std_logic_vector(to_unsigned(
    --                          to_integer(unsigned(port_in_baseaddr_data)) +
    --                          to_integer(unsigned(port_in_data)),
    --                          port_mem_out_data'length));
    port_mem_out_last <= port_in_last;
    port_mem_out_valid <= port_in_valid and port_in_baseaddr_valid;
    port_in_ready <= port_mem_out_ready when port_in_baseaddr_valid = '1' else "0";
    port_in_baseaddr_ready <= port_mem_out_ready when port_in_valid = '1' else "0";

    port_out_data <= port_mem_in_data;
    port_out_last <= port_mem_in_last;
    port_out_valid <= port_mem_in_valid;
    port_mem_in_ready <= port_out_ready;

end behavioral;