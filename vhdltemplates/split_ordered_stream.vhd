library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity split_ordered_stream is
    generic(
        inner_stream_length: natural := 4;
        outer_stream_length: natural := 4
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_2_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_2_type
    );
end split_ordered_stream;

architecture behavioral of split_ordered_stream is
    signal inner_counter: natural range 0 to inner_stream_length-1 := 0;
    signal outer_counter: natural range 0 to outer_stream_length-1 := 0;

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

    -- when the inner "new" created stream has to be repeated, we need to repeat the entire split operation, until the same inner stream has been reached
    signal repeat_inner: std_logic;
    signal repeat_outer_counter: natural range 0 to outer_stream_length-1 := 0;
begin

    port_out_data <= port_in_data;
    port_out_valid <= port_in_valid and not repeat_inner;

    last_signal: process(port_in_last, outer_counter, inner_counter)
    begin
        port_out_last(port_out_last'high - 1 downto port_out_last'low) <= port_in_last;
        if inner_counter = inner_stream_length - 1 then
            port_out_last(port_out_last'high - 1) <= '1';
        else
            port_out_last(port_out_last'high - 1) <= '0';
        end if;
        if outer_counter = outer_stream_length - 1 then
            port_out_last(port_out_last'high) <= '1';
        else
            port_out_last(port_out_last'high) <= '0';
        end if;
    end process;

    port_in_ready <= in_ready;
    ready_signal: process(repeat_inner, port_out_ready, in_ready, port_in_last)
    begin
        if repeat_inner = '0' then
            in_ready <= port_out_ready(port_out_ready'high) & port_out_ready(port_out_ready'high - 2 downto port_out_ready'low);
        else
            -- always ready!
            in_ready(in_ready'low) <= '1';
            for i in in_ready'low + 1 to in_ready'high - 1 loop
                if port_in_last(i - 1) = '1' and in_ready(i - 1) = '1' then
                    in_ready(i) <= '1';
                else
                    in_ready(i) <= '0';
                end if;
            end loop;
            -- but repeat!
            in_ready(in_ready'high) <= '0';
        end if;
    end process;

    repeat_inner_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                repeat_inner <= '0';
                repeat_outer_counter <= 0;
            else
                if repeat_inner = '0' then
                    -- repeat inner stream, when inner element is last, all subelements of this are ready and the ready signal for the inner stream is not set (indicating the demand to repeat the stream)
                    if inner_counter = inner_stream_length - 1 and port_in_valid = '1' and port_out_ready(port_out_ready'high - 2) = '1' and port_out_ready(port_out_ready'high-1) = '0'then
                        repeat_outer_counter <= outer_counter;
                        repeat_inner <= '1';
                    end if;
                else
                    -- if we receive an element
                    if port_in_valid = '1' and in_ready(in_ready'high - 1) = '1' then
                        -- which is last in the inner stream
                        if inner_counter = inner_stream_length-1 then
                            -- and the next stream is our desired one (which had to be repeated)
                            if (repeat_outer_counter = 0 and outer_counter = outer_stream_length-1)
                                or (repeat_outer_counter = outer_counter + 1) then
                                -- finish repeat
                                repeat_inner <= '0';
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    counter_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                inner_counter <= 0;
                outer_counter <= 0;
            else
                if port_in_valid = '1' and in_ready(in_ready'high - 1) = '1' then
                    if inner_counter < inner_stream_length-1 then
                        inner_counter <= inner_counter + 1;
                    else
                        inner_counter <= 0;
                        if outer_counter < outer_stream_length-1 then
                            outer_counter <= outer_counter + 1;
                        else
                            outer_counter <= 0;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
