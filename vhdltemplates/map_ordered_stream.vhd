library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity map_ordered_stream is
    generic(
        stream_length: natural := 4
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_2_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_2_type;
        port_out_data: out data_word_type;
        port_out_last: out last_2_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_2_type
    );
end map_ordered_stream;

architecture behavioral of map_ordered_stream is
    component map_ordered_stream is -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_in_data: in data_word_type; -- %TESTING_ONLY
            port_in_last: in last_1_type; -- %TESTING_ONLY
            port_in_valid: in std_logic; -- %TESTING_ONLY
            port_in_ready: out ready_1_type; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_1_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_1_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    function min(left, right: integer) return integer is
    begin
        if left < right then
            return left;
        else
            return right;
        end if;
    end function;

    signal delay_ctrl_last: std_logic_vector(port_in_last'range) := (others => '0'); -- %TESTING_ONLY
    signal delay_ctrl_ready: std_logic_vector(port_in_ready'range) := (others => '0'); -- %TESTING_ONLY

    signal port_to_f_data: data_word_type; -- %TESTING_ONLY
    signal port_to_f_last: last_1_type; -- %TESTING_ONLY
    signal port_to_f_valid: std_logic; -- %TESTING_ONLY
    signal port_to_f_ready: ready_1_type; -- %TESTING_ONLY

    signal port_from_f_data: data_word_type; -- %TESTING_ONLY
    signal port_from_f_last: last_1_type; -- %TESTING_ONLY
    signal port_from_f_valid: std_logic; -- %TESTING_ONLY
    signal port_from_f_ready: ready_1_type; -- %TESTING_ONLY

    signal counter_to_f: natural range 0 to stream_length - 1 := 0;
    signal counter_from_f: natural range 0 to stream_length - 1 := 0;

    signal buffer_to_f_data: data_to_f_type;
    signal buffer_to_f_last: std_logic_vector(port_in_last'range) := (others => '0');
    signal buffer_to_f_valid: std_logic := '0';
    signal buffer_to_f_ready: std_logic_vector(port_in_ready'range) := (others => '0');

    signal buffer_to_f_very_last: std_logic := '0';

    signal buffer_from_f_data: data_from_f_type;
    signal buffer_from_f_last: std_logic_vector(port_out_last'range) := (others => '0');
    signal buffer_from_f_valid: std_logic := '0';
    signal buffer_from_f_ready: std_logic_vector(port_out_ready'range) := (others => '0');

    signal buffer_from_f_very_last: std_logic := '0';

    type buffer_state_type is (empty, full, confirming);
    signal buffer_to_f_state: buffer_state_type := empty;
    signal buffer_from_f_state: buffer_state_type := empty;

    signal flush: std_logic := '0';

    signal init_flush: std_logic := '0';
    signal disable_flush: std_logic := '0';

begin
    
    f: map_ordered_stream port map( -- %TESTING_ONLY
        clk => clk,  -- %TESTING_ONLY
        reset => reset,  -- %TESTING_ONLY
        port_in_data =>   port_to_f_data,  -- %TESTING_ONLY
        port_in_last =>   port_to_f_last,  -- %TESTING_ONLY
        port_in_valid =>  port_to_f_valid,  -- %TESTING_ONLY
        port_in_ready =>  port_to_f_ready,  -- %TESTING_ONLY
        port_out_data =>  port_from_f_data,  -- %TESTING_ONLY
        port_out_last =>  port_from_f_last,  -- %TESTING_ONLY
        port_out_valid => port_from_f_valid,  -- %TESTING_ONLY
        port_out_ready => port_from_f_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    delay_ctrl_last <= port_in_last;
    delay_ctrl_ready <= buffer_to_f_ready when flush = '0' else (others => '0');

    sl1: if stream_length = 1 and port_out_last'length <= 1 generate
        -- do not select this init flush condition, when the outgoing stream has more than 1 level of nesting
        init_flush <= '1' when disable_flush = '0' and buffer_to_f_state = full and buffer_to_f_very_last = '1' else '0';
    end generate sl1;

    sln: if stream_length > 1 or port_out_last'length > 1 generate
        init_flush <= '1' when disable_flush = '0' and buffer_to_f_state = full and buffer_to_f_very_last = '1' and buffer_from_f_very_last = '0' else '0';
    end generate sln;

    flush_logic: process(clk)
        constant ready_all_one_to_f: std_logic_vector(port_to_f_ready'range) := (others => '1');
    begin
        if rising_edge(clk) then
            if reset = '1' then
                flush <= '0';
                disable_flush <= '0';
            else
                if flush = '0' and buffer_to_f_state = confirming then
                    -- wait until the ingoing buffer (buffer_to_f) has confirmed the incoming data, which is the very last step of one pipeline 'lifecycle'; only after that it is possible to initiate flush pipeline again
                    disable_flush <= '0';
                end if;
                -- when the last one has been send out and the to_f buffer is confirming we have finished the entire flushing process
                -- old problem: if buffer_to_f_state = confirming and buffer_from_f_very_last = '1' and port_out_ready(port_out_ready'low) = '1' then
                if buffer_from_f_very_last = '1' and buffer_from_f_ready(buffer_from_f_ready'low) = '1' then
                    flush <= '0';
                    if flush = '1' then
                        -- disable the mechanism to initiate pipeline flush; we just flushed it..
                        disable_flush <= '1';
                    end if;
                elsif init_flush = '1' then -- buffer_to_f_state = full and buffer_to_f_very_last = '1' and buffer_from_f_very_last = '0' then 
                    flush <= '1';
                    -- is repeating! no need to flush
                    if (port_to_f_ready'length > 1 and port_to_f_ready /= ready_all_one_to_f) and port_to_f_ready(port_to_f_ready'low) = '1' then
                        flush <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    port_to_f_data <= buffer_to_f_data;
    port_to_f_last <= buffer_to_f_last(buffer_to_f_last'high - 1 downto buffer_to_f_last'low);
    port_to_f_valid <= buffer_to_f_valid;
    ready_signal: process(buffer_to_f_ready, flush)
    begin
        if flush = '0' then
            port_in_ready <= buffer_to_f_ready;
            if buffer_to_f_ready(buffer_to_f_ready'high - 1) = '0' then
                port_in_ready(port_in_ready'high) <= '0';
            end if;
        else
            port_in_ready <= (others => '0');
        end if;
    end process;

    buffer_to_f_logic: process(clk)
        constant last_all_one_to_f: std_logic_vector(buffer_to_f_last'range) := (others => '1');
    begin
        if rising_edge(clk) then
            if reset = '1' then
                buffer_to_f_state <= empty;
                buffer_to_f_last <= (others => '0');
                buffer_to_f_very_last <= '0';
                buffer_to_f_valid <= '0';
                buffer_to_f_ready <= (others => '0');
            else
                case buffer_to_f_state is
                    when empty =>
                        if port_in_valid = '1' then
                            buffer_to_f_state <= full;
                            buffer_to_f_data <= port_in_data;
                            buffer_to_f_last <= port_in_last;
                            if port_in_last = last_all_one_to_f then
                                buffer_to_f_very_last <= '1';
                            else
                                buffer_to_f_very_last <= '0';
                            end if;
                            buffer_to_f_valid <= '1';
                            buffer_to_f_ready <= (others => '0');
                        end if;
                    when full =>
                        if port_to_f_ready(port_to_f_ready'low) = '1' then
                            buffer_to_f_state <= confirming;
                            buffer_to_f_last <= (others => '0');
                            buffer_to_f_valid <= '0';
                            buffer_to_f_ready(port_to_f_ready'high downto port_to_f_ready'low) <= port_to_f_ready;
                        end if;
                        if flush = '1' then
                            if buffer_from_f_state = full then -- and buffer_from_f_valid = '1' then -- optional check if buffer_from_f_last is all last
                                buffer_to_f_ready(buffer_to_f_ready'high) <= port_out_ready(port_out_ready'high);
                            end if;
                        end if;
                    when confirming =>
                        if flush = '0' then
                            if port_in_valid = '1' then
                                buffer_to_f_state <= empty;
                                buffer_to_f_last <= (others => '0');
                                buffer_to_f_valid <= '0';
                                buffer_to_f_ready <= (others => '0');
                            end if;
                        end if;
                        if flush = '1' then
                            if buffer_from_f_state = full then -- optional check if buffer_from_f_last is all last
                                buffer_to_f_ready(buffer_to_f_ready'high) <= port_out_ready(port_out_ready'high);
                            end if;
                        end if;
                end case;
            end if;
        end if;
    end process;

    port_out_data <= buffer_from_f_data;
    port_out_last <= buffer_from_f_last;
    port_out_valid <= buffer_from_f_valid;
    port_from_f_ready <= buffer_from_f_ready(buffer_from_f_ready'high - 1 downto buffer_from_f_ready'low);

    buffer_from_f_state_logic: process(clk)
        constant last_all_one_from_f: std_logic_vector(port_from_f_last'range) := (others => '1');
    begin
        if rising_edge(clk) then
            if reset = '1' then
                buffer_from_f_state <= empty;
                buffer_from_f_last <= (others => '0');
                buffer_from_f_very_last <= '0';
                buffer_from_f_valid <= '0';
                buffer_from_f_ready <= (others => '0');
            else
                case buffer_from_f_state is
                    when empty =>
                        if port_from_f_valid = '1' then
                            buffer_from_f_state <= full;
                            buffer_from_f_data <= port_from_f_data;
                            -- TODO check if the counter is on time
                            if counter_from_f = stream_length - 1 then

                                if port_from_f_last = last_all_one_from_f then
                                    buffer_from_f_very_last <= '1';
                                else
                                    buffer_from_f_very_last <= '0';
                                end if;

                                buffer_from_f_last <= "1" & port_from_f_last;
                            else
                                -- TODO added this line... correct?
                                buffer_from_f_very_last <= '0';
                                buffer_from_f_last <= "0" & port_from_f_last;
                            end if;
                            buffer_from_f_valid <= '1';
                            buffer_from_f_ready <= (others => '0');
                        end if;
                    when full =>
                        if port_out_ready(port_out_ready'low) = '1' then
                            buffer_from_f_state <= confirming;
                            buffer_from_f_last <= (others => '0');
                            buffer_from_f_valid <= '0';
                            buffer_from_f_ready <= port_out_ready;
                        end if;
                    when confirming =>
                        if port_from_f_valid = '1' then
                            buffer_from_f_state <= empty;
                            buffer_from_f_last <= (others => '0');
                            buffer_from_f_valid <= '0';
                            buffer_from_f_ready <= (others => '0');
                        end if;
                end case;
            end if;
        end if;
    end process;

    counter_to_f_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_to_f <= 0;
            else
                if port_to_f_valid = '1' and port_to_f_ready(port_to_f_ready'high) = '1' then
                    if counter_to_f < stream_length - 1 then
                        counter_to_f <= counter_to_f + 1;
                    else
                        counter_to_f <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

    counter_from_f_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_from_f <= 0;
            else
                if port_from_f_valid = '1' and port_from_f_ready(port_from_f_ready'high) = '1' then
                    if counter_from_f < stream_length - 1 then
                        counter_from_f <= counter_from_f + 1;
                    else
                        counter_from_f <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
