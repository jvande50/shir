-- The "signed" version of fold_int_ordered_stream.vhd
-- this one sign extends the first element of the stream to fit the accumulator
-- instead of zero extending

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fold_integer_ordered_stream is
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- stream of data to fold
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        -- accumulator output
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end fold_integer_ordered_stream;

architecture behavioral of fold_integer_ordered_stream is

    component add is -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_in_1_data: in data_word_type; -- %TESTING_ONLY
            port_in_1_last: in last_0_type; -- %TESTING_ONLY
            port_in_1_valid: in std_logic; -- %TESTING_ONLY
            port_in_1_ready: out ready_0_type; -- %TESTING_ONLY
            port_in_2_data: in data_word_type; -- %TESTING_ONLY
            port_in_2_last: in last_0_type; -- %TESTING_ONLY
            port_in_2_valid: in std_logic; -- %TESTING_ONLY
            port_in_2_ready: out ready_0_type; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_0_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_0_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    signal first_element: std_logic := '1';

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

    -- accumulates the values in this stream
    signal buf: std_logic_vector(port_out_data'range) := (others => '0');
    signal buf_valid: std_logic := '1'; -- buffer contains a value that is ready to be processes for the next iteration
    signal buf_last: std_logic := '0'; -- if the buffer contains the reduction up to the last value of the stream => output is ready!
    signal f_last: std_logic := '0'; -- if the function is currently working of the last value of the stream

    signal port_f_out_acc_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_acc_last: last_0_type; -- %TESTING_ONLY
    signal port_f_out_acc_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_acc_ready: ready_0_type; -- %TESTING_ONLY
    signal port_f_out_data_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_data_last: last_0_type; -- %TESTING_ONLY
    signal port_f_out_data_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_data_ready: ready_0_type; -- %TESTING_ONLY
    signal port_f_in_data: data_word_type; -- %TESTING_ONLY
    signal port_f_in_last: last_0_type; -- %TESTING_ONLY
    signal port_f_in_valid: std_logic; -- %TESTING_ONLY
    signal port_f_in_ready: ready_0_type; -- %TESTING_ONLY

begin
    
    f: add port map( -- %TESTING_ONLY
        clk => clk, -- %TESTING_ONLY
        reset => reset, -- %TESTING_ONLY
        port_in_1_data => port_f_out_acc_data, -- %TESTING_ONLY
        port_in_1_last => port_f_out_acc_last, -- %TESTING_ONLY
        port_in_1_valid => port_f_out_acc_valid, -- %TESTING_ONLY
        port_in_1_ready => port_f_out_acc_ready, -- %TESTING_ONLY
        port_in_2_data => port_f_out_data_data, -- %TESTING_ONLY
        port_in_2_last => port_f_out_data_last, -- %TESTING_ONLY
        port_in_2_valid => port_f_out_data_valid, -- %TESTING_ONLY
        port_in_2_ready => port_f_out_data_ready, -- %TESTING_ONLY
        port_out_data => port_f_in_data, -- %TESTING_ONLY
        port_out_last => port_f_in_last, -- %TESTING_ONLY
        port_out_valid => port_f_in_valid, -- %TESTING_ONLY
        port_out_ready => port_f_in_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    port_out_data <= buf;
    port_out_valid <= '1' when buf_valid = '1' and buf_last = '1' else '0';

    port_f_out_acc_data <= buf;
    port_f_out_acc_last <= (others => '0');
    port_f_out_acc_valid <= '1' when buf_valid = '1' and buf_last = '0' else '0';

    port_f_out_data_data <= port_in_data;
    port_f_out_data_last <= port_in_last(port_f_out_data_last'high downto port_f_out_data_last'low);
    port_f_out_data_valid <= '1' when first_element = '0' and port_in_valid = '1' and buf_last = '0' else '0';

    in_ready(0) <= port_f_out_data_ready(0) when first_element = '0' else ((not buf_valid) or (buf_last and port_out_ready(port_out_ready'high)));
    in_ready(1) <= port_in_last(port_in_last'high) and in_ready(0);
    port_in_ready <= in_ready;

    -- there will never be the request to repeat the stream, because the result of the reduction is just an element, that can be "reread"
    port_f_in_ready <= "1" when buf_last = '0' else "0";

    state_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                first_element <= '1';
            else
                if port_in_valid = '1' and in_ready(in_ready'low) = '1' then
                    -- if length of the stream is 1, last is always 1 and first_element must be always '1', too.
                    if port_in_last(port_in_last'high) = '1' then
                        first_element <= '1';
                    elsif first_element = '1' then
                        first_element <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    buffer_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                buf <= (others => '0');
                buf_valid <= '0';
                buf_last <= '0';
                f_last <= '0';
            else
                if buf_valid = '1' and buf_last = '1' and port_out_ready(port_out_ready'high) = '1' then
                    buf_valid <= '0';
                    buf_last <= '0';
                end if;

                if first_element = '1' then
                    if port_in_valid = '1' and in_ready(in_ready'low) = '1' and (buf_valid = '0' or (buf_last = '1' and port_out_ready(port_out_ready'high) = '1')) then
                        buf <= (others => port_in_data(port_in_data'high)); -- fill with sign bit
                        buf(port_in_data'high downto port_in_data'low) <= port_in_data;
                        buf_valid <= '1';
                        if port_in_last(port_in_last'high) = '1' then
                            buf_last <= port_in_last(port_in_last'high);
                        end if;
                    end if;
                else
                    -- new data just entered function f, buffer contains 'old' results and is not valid anymore
                    if port_f_out_acc_valid = '1' and port_f_out_data_valid = '1' and port_f_out_acc_ready(port_f_out_acc_ready'high) = '1' and port_f_out_data_ready(port_f_out_data_ready'high) = '1' then
                        buf_valid <= '0';
                        f_last <= port_in_last(port_in_last'high);
                    end if;
                    -- if function f is done, store result in buffer and mark as valid
                    if port_f_in_valid = '1' and port_f_in_ready(port_f_in_ready'high) = '1' then
                        buf <= port_f_in_data(buf'high downto buf'low);
                        buf_valid <= '1';
                        -- seems like this function has 0 cycles of delay => directly forward last signal
                        if port_f_out_acc_valid = '1' and port_f_out_data_valid = '1' and port_f_out_data_ready(port_f_out_data_ready'high) = '1' then
                            buf_last <= port_in_last(port_in_last'high);
                        else
                            buf_last <= f_last;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
