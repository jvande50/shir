library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity cip_bankers_round is
    generic(
        low_elements: natural := 2;
        high_elements: natural := 1
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_vector_slice_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end cip_bankers_round;

architecture behavioral of cip_bankers_round is
    signal round_value: std_logic_vector(port_in_data'high + 1 - low_elements downto port_in_data'low) := (others => '0');
    signal clip_value: std_logic_vector(port_in_data'high - low_elements - high_elements downto port_in_data'low) := (others => '0');
begin
    -- Round to nearest even
    GEN_ROUND: if low_elements > 0 generate
        round: process(port_in_data)
            variable round_value_next: std_logic_vector(round_value'range) := (others => '0');
            constant all_zeros: std_logic_vector(low_elements - 2 downto 0) := (others => '0');
        begin
            round_value_next(round_value'high - 1 downto round_value'low) := port_in_data(port_in_data'high downto low_elements);
            round_value_next(round_value'high) := port_in_data(port_in_data'high);
            if port_in_data(port_in_data'high) = '1' then -- negative
                if port_in_data(low_elements) = '1' then -- odd
                    if (port_in_data(low_elements - 1) = '0') then -- round
                        round_value_next := round_value_next;
                    else
                        round_value_next := std_logic_vector(signed(round_value_next) + 1);
                    end if;
                else -- even
                    if (port_in_data(low_elements - 1) = '0') or (port_in_data(low_elements - 1) = '1' and port_in_data(low_elements - 2 downto port_in_data'low) = all_zeros) then -- round
                        round_value_next := round_value_next;
                    else
                        round_value_next := std_logic_vector(signed(round_value_next) + 1);
                    end if;
                end if;
            else -- positive
                if port_in_data(low_elements) = '1' then -- odd
                    if port_in_data(low_elements - 1) = '1'  then -- round
                        round_value_next := std_logic_vector(signed(round_value_next) + 1);
                    else
                        round_value_next := round_value_next;
                    end if;
                else -- even
                    if port_in_data(low_elements - 1) = '1' and not (port_in_data(low_elements - 2 downto port_in_data'low) = all_zeros) then -- round
                        round_value_next := std_logic_vector(signed(round_value_next) + 1);
                    else
                        round_value_next := round_value_next;
                    end if;
                end if;
            end if;
            round_value <= round_value_next;
        end process;
    end generate GEN_ROUND;

    GEN_NO_ROUND: if low_elements <= 0 generate
        round_value(round_value'high - 1 downto round_value'low) <= port_in_data;
        round_value(round_value'high) <= port_in_data(port_in_data'high);
    end generate GEN_NO_ROUND;   

    clip: process(round_value)
        constant max_val: std_logic_vector(clip_value'range) := (clip_value'high => '0', others => '1');
        constant min_val: std_logic_vector(clip_value'range) := (clip_value'high => '1', others => '0');
    begin
        if signed(round_value) > signed(max_val) then
            clip_value <= max_val;
        elsif signed(round_value) < signed(min_val) then
            clip_value <= min_val;
        else
            clip_value <= round_value(clip_value'range);
        end if;
    end process;


    port_out_data <= clip_value;
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;