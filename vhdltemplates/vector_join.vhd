library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity vector_join is
    generic(
        num_clients: natural := 4
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_clients_data: in data_word_vector_generic_type(num_clients - 1 downto 0);
        port_in_clients_last: in last_0_vector_generic_type(num_clients - 1 downto 0);
        port_in_clients_valid: in std_logic_vector(num_clients - 1 downto 0);
        port_in_clients_ready: out ready_0_vector_generic_type(num_clients - 1 downto 0);

        port_out_data: out data_word_vector_generic_type(num_clients - 1 downto 0);
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end vector_join;

architecture behavioral of vector_join is
begin

    port_out_data <= port_in_clients_data;
    port_out_last <= port_in_clients_last(0);
    -- TODO this is not very generic! check that all clients have been valid, then send valid signal
    port_out_valid <= port_in_clients_valid(0);
    port_in_clients_ready <= (others => port_out_ready);

end behavioral;
