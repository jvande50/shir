library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity sel is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end sel;

architecture behavioral of sel is
    signal selected_data: data_word_type;
    signal data_out: data_word_type;
    signal data_valid: std_logic := '0';
begin

    port_out_data <= data_out;
    port_out_last <= (others => '0'); -- single element out, no stream
    port_out_valid <= data_valid;
    selected_data <= port_in_data.t0; -- %TESTING_ONLY

    buffer_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                -- data_out <= (others => '0');
                data_valid <= '0';
            else
                if data_valid = '0' then
                    if port_in_valid = '1' then
                        data_out <= selected_data;
                        data_valid <= '1';
                    end if;
                else
                    if port_out_ready = "1" then
                        data_valid <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    ready_signal: process(data_valid, port_in_last, port_out_ready)
    begin
        if data_valid = '0' then
            port_in_ready <= (others => '0');
        else
            if port_in_last(0) = '0' then
                port_in_ready <= (others => '1');
                port_in_ready(port_in_ready'high downto port_in_ready'high - port_out_ready'length + 1) <= port_out_ready;
            else
                port_in_ready <= port_out_ready(0) & port_out_ready(0);
            end if;
        end if;
    end process;

end behavioral;
