library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity write_sync is
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- to controller
        port_mem_out_data: out cache_line_with_address_type;
        port_mem_out_last: out last_0_type;
        port_mem_out_valid: out std_logic;
        port_mem_out_ready: in ready_0_type;
        -- from controller
        port_mem_in_data: in std_logic_vector(-1 downto 0);
        port_mem_in_last: in last_0_type;
        port_mem_in_valid: in std_logic;
        port_mem_in_ready: out ready_0_type;
        -- tuple of data and address
        port_in_data: in write_input_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        -- base address (added to address)
        port_in_baseaddr_data: in base_address_type;
        port_in_baseaddr_last: in last_0_type;
        port_in_baseaddr_valid: in std_logic;
        port_in_baseaddr_ready: out ready_0_type;

        -- outgoing base address after write has finished
        port_out_data: out base_address_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end write_sync;

architecture behavioral of write_sync is
begin

    port_mem_out_data.data <= port_in_data.t0;
    port_mem_out_data.addr <= std_logic_vector(to_unsigned(
                              to_integer(unsigned(port_in_baseaddr_data)) +
                              to_integer(unsigned(port_in_data.t1)),
                              port_mem_out_data.addr'length));
    port_mem_out_last <= port_in_last;
    port_mem_out_valid <= port_in_valid and port_in_baseaddr_valid;
    port_in_ready <= port_mem_out_ready when port_in_baseaddr_valid = '1' else "0";
    port_in_baseaddr_ready <= port_mem_out_ready when port_in_valid = '1' else "0";

    port_out_data <= port_in_baseaddr_data;
    port_out_last <= port_in_baseaddr_last;
    port_out_valid <= port_mem_in_valid;
    port_mem_in_ready <= port_out_ready;

end behavioral;
