library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity ordered_stream_to_vector is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_stream_type;
        port_in_valid: in std_logic;
        port_in_ready: out std_logic;
        port_out_data: out data_word_vector_type;
        port_out_valid: out std_logic;
        port_out_ready: in std_logic
    );
end ordered_stream_to_vector;

architecture behavioral of ordered_stream_to_vector is
    signal cnt: natural range 0 to data_word_vector_type'length := 0;
    signal reg: data_word_vector_type := (others => (others => '0'));
    signal dout_valid: std_logic := '0';

    -- the first word in the vector is double buffered
    signal selbuf: std_logic := '0';
    signal buf: data_word_type := (others => '0');

begin

    port_out_data <= reg when selbuf = '0' else buf & reg(data_word_vector_type'high - 1 downto 0);

    port_out_valid <= dout_valid;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                port_in_ready <= '0';
                cnt <= 0;
                reg <= (others => (others => '0'));
                dout_valid <= '0';
                selbuf <= '0';
                buf <= (others => '0');
            else
                port_in_ready <= '0';

                -- outgoing vector has been transmitted
                if dout_valid = '1' and port_out_ready = '1' then
                    port_in_ready <= '1';
                    dout_valid <= '0';
                    -- only reset counter to 0, if doublebuffer was not filled
                    if cnt = data_word_vector_type'length then
                        cnt <= 0;
                    end if;
                    selbuf <= not selbuf; -- switch active buffer
                end if;

                -- buffer is full, but we can use the doublebuffer to store one more element, if incoming data is valid
                if cnt = data_word_vector_type'length and port_in_valid = '1' then
                    if selbuf = '1' then
                        reg(data_word_vector_type'high) <= port_in_data.data;
                    else
                        buf <= port_in_data.data;
                    end if;
                    cnt <= 1;
                -- buffer is not full and no outgoing data is waiting to be transmitted
                elsif dout_valid = '0' then
                    port_in_ready <= '1';
                    if port_in_valid = '1' then
                        -- shift begin
                        if selbuf = '0' then
                            for i in 0 to data_word_vector_type'high - 1 loop
                                reg(i) <= reg(i + 1);
                            end loop;
                            reg(data_word_vector_type'high) <= port_in_data.data;
                        else
                            for i in 0 to data_word_vector_type'high - 2 loop
                                reg(i) <= reg(i + 1);
                            end loop;
                            reg(data_word_vector_type'high - 1) <= buf;
                            buf <= port_in_data.data;
                        end if;
                        -- shift end

                        cnt <= cnt + 1;

                        -- the final element just arrived, so the outgoing data will be ready in the next cycle
                        if cnt = data_word_vector_type'length - 1 then
                            dout_valid <= '1';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
