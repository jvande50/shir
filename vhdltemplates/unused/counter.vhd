library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity counter is
    generic(
        start: natural := 0;
        increment: natural := 2;
        dimensions: natural_vector_type := (5, 3);
        repetitions: natural_vector_type := (2, 3);
        loop_all: std_logic := '0'
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out data_word_type;
        port_out_last: out last_2_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_2_type
    );
end counter;

architecture behavioral of counter is

    -- function to precompute constants (during compilation)
    function precomp_increments return natural_vector_type is
        variable increment_per_dimension: natural_vector_type := (others => increment);
    begin
        -- multiplication accumulate
        increment_per_dimension(0) := dimensions(0) * increment;
        for i in dimensions'low + 1 to dimensions'high loop
            increment_per_dimension(i) := increment_per_dimension(i - 1) * dimensions(i);
        end loop;
        return increment_per_dimension;
    end function;

    signal counter_value: natural := 0;
    signal counter_dimensions: natural_vector_type := (others => 0);
    constant increment_per_dimension: natural_vector_type := precomp_increments;

    signal counter_repetitions: natural_vector_type := (others => 0);

    signal out_last: std_logic_vector(port_out_last'range) := (others => '0');
    signal out_valid: std_logic := '1';

begin

    port_out_data <= std_logic_vector(to_unsigned(counter_value + start, port_out_data'length));
    port_out_last <= out_last;
    port_out_valid <= out_valid;

    last_signals: process(counter_dimensions)
    begin
        out_last <= (others => '0');
        for i in dimensions'low to dimensions'high loop
            if counter_dimensions(i) = dimensions(i) - 1 then
                out_last(i) <= '1';
            end if;
        end loop;
    end process;

    counter_dimensions_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_dimensions <= (others => 0);
            else
                if out_valid = '1' then
                    for i in dimensions'low to dimensions'high loop
                        if port_out_ready(i) = '1' then
                            if i = 0 or counter_repetitions(i - 1) = repetitions(i - 1) then
                                if counter_dimensions(i) < dimensions(i) - 1 then
                                    counter_dimensions(i) <= counter_dimensions(i) + 1;
                                else
                                    counter_dimensions(i) <= 0;
                                end if;
                            end if;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;

    counter_repetitions_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_repetitions <= (others => 1);
            else
                if out_valid = '1' then
                    for i in repetitions'low to repetitions'high loop
                        if out_last(i) = '1' and port_out_ready(i) = '1' then
                            if counter_repetitions(i) < repetitions(i) then
                                counter_repetitions(i) <= counter_repetitions(i) + 1;
                            else
                                counter_repetitions(i) <= 1;
                            end if;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;

    counter_value_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_value <= 0;
                out_valid <= '1';
            else
                if out_valid = '1' then
                    -- increase value
                    if port_out_ready(port_out_ready'low) = '1' then
                        if counter_value <= increment_per_dimension(dimensions'high) - increment - 1 then
                            counter_value <= counter_value + increment;
                        else
                            counter_value <= 0;
                            -- finished counting, do not repeat, if loop_all is not set
                            out_valid <= loop_all;
                        end if;
                    end if;

                    -- repeat inner
                    for i in dimensions'low to dimensions'high loop
                        if out_last(i) = '1' and port_out_ready(i) = '1' and (port_out_ready(i + 1) = '0' or counter_repetitions(i) < repetitions(i)) then
                            assert(counter_value + increment - increment_per_dimension(i) >= 0) report "error in incoming ready signals";
                            counter_value <= counter_value + increment - increment_per_dimension(i);
                            -- repeat, not finished!
                            out_valid <= '1';
                            exit;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;

end behavioral;
