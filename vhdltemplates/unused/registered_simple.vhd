library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity registered is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end registered;

architecture behavioral of registered is

    signal buf: data_word_type;
    signal buf_last: std_logic_vector(port_in_last'range) := (others => '0');
    signal buf_valid: std_logic := '0';

begin

    port_in_ready <= port_out_ready when buf_valid = '1' else (others => '0');
    port_out_data <= buf;
    port_out_last <= buf_last;
    port_out_valid <= port_in_valid when buf_valid = '1' else '0';

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                buf_last <= (others => '0');
                buf_valid <= '0';
            else
                if buf_valid = '0' then
                    if port_in_valid = '1' then
                        buf <= port_in_data;
                        buf_last <= port_in_last;
                        buf_valid <= '1';
                    end if;
                else
                    if port_in_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                        buf_valid <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
