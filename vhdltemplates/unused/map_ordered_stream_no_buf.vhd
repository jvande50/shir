library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity map_ordered_stream is
    generic(
        stream_length: natural := 4
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_2_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_2_type;
        port_out_data: out data_word_type;
        port_out_last: out last_2_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_2_type
    );
end map_ordered_stream;

architecture behavioral of map_ordered_stream is
    component map_ordered_stream is -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_in_data: in data_word_type; -- %TESTING_ONLY
            port_in_last: in last_1_type; -- %TESTING_ONLY
            port_in_valid: in std_logic; -- %TESTING_ONLY
            port_in_ready: out ready_1_type; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_1_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_1_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    function min(left, right: integer) return integer is
    begin
        if left < right then
            return left;
        else
            return right;
        end if;
    end function;

    signal port_f_in_data: data_word_type; -- %TESTING_ONLY
    signal port_f_in_last: last_1_type; -- %TESTING_ONLY
    signal port_f_in_valid: std_logic; -- %TESTING_ONLY
    signal port_f_in_ready: ready_1_type; -- %TESTING_ONLY
    signal port_f_out_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_last: last_1_type; -- %TESTING_ONLY
    signal port_f_out_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_ready: ready_1_type; -- %TESTING_ONLY

    signal out_counter: natural range 0 to stream_length - 1 := 0;
    signal in_counter: natural range 0 to stream_length - 1 := 0;

begin
    
    f: map_ordered_stream port map( -- %TESTING_ONLY
        clk => clk,  -- %TESTING_ONLY
        reset => reset,  -- %TESTING_ONLY
        port_in_data => port_f_out_data,  -- %TESTING_ONLY
        port_in_last => port_f_out_last,  -- %TESTING_ONLY
        port_in_valid => port_f_out_valid,  -- %TESTING_ONLY
        port_in_ready => port_f_out_ready,  -- %TESTING_ONLY
        port_out_data => port_f_in_data,  -- %TESTING_ONLY
        port_out_last => port_f_in_last,  -- %TESTING_ONLY
        port_out_valid => port_f_in_valid,  -- %TESTING_ONLY
        port_out_ready => port_f_in_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    port_out_data <= port_f_in_data;
    port_out_last <= "1" & port_f_in_last when out_counter = stream_length - 1 else "0" & port_f_in_last; -- do not use ingoing last signal here, since the function f may delay the data!
    port_out_valid <= port_f_in_valid;
    port_f_in_ready <= port_out_ready(port_out_ready'high - 1 downto port_out_ready'low);

    port_f_out_data <= port_in_data;
    port_f_out_last <= port_in_last(port_in_last'high - 1 downto port_in_last'low);

    flush_pipeline_logic: process(port_in_last, port_f_out_ready, port_in_valid, in_counter, out_counter, port_f_in_last, port_f_in_valid, port_out_ready)
        constant map_last_all_one: std_logic_vector(port_in_last'range) := (others => '1');
        constant f_last_all_one: std_logic_vector(port_f_in_last'range) := (others => '1');
        constant f_ready_all_one: std_logic_vector(port_f_out_ready'range) := (others => '1');
    begin
        if port_in_last /= map_last_all_one or (port_f_out_ready'length > 1 and port_f_out_ready = '0' & f_ready_all_one(f_ready_all_one'high - 1 downto f_ready_all_one'low)) then -- as long as we are not giving the last element into the function OR we are repeating the stream (in which case the last element is not really the last element) do basic forwarding:
            port_in_ready <= '0' & port_f_out_ready;
            port_f_out_valid <= port_in_valid;
        else
            -- flush pipeline

            port_in_ready <= (others => '0');
            port_f_out_valid <= '0';

            -- allow the very last element to enter the function
            if in_counter = stream_length - 1 then
                port_f_out_valid <= port_in_valid;
            end if;

            -- when the very last element leaves the function connect ready in and ready out signals of map
            if out_counter = stream_length - 1 and port_f_in_last = f_last_all_one and port_f_in_valid = '1' then
                -- handle both cases, where port_in_ready'length is < or >= port_out_ready'length
                -- this is just for the port_in_ready'length >= port_out_ready'length case. In the other case all the bits are overwritten again (which is no problem; this line will probably be removed during synthesis then)
                port_in_ready <= (others => port_out_ready(port_out_ready'low));
                -- now assign the number of uppermost bits (that both signals have). e.g. if port_in_ready'length = 2 and port_out_ready'length = 3: assign the uppermost 2 bits of port_out_ready to the uppermost 2 bits of port_in_ready
                port_in_ready(port_in_ready'high downto port_in_ready'high - min(port_in_ready'length, port_out_ready'length) + 1) <= 
                    port_out_ready(port_out_ready'high downto port_out_ready'high - min(port_in_ready'length, port_out_ready'length) + 1);
            end if;
        end if;
    end process;

    ingoing_elements_counter_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                in_counter <= 0;
            else
                if port_f_out_valid = '1' and port_f_out_ready(port_f_out_ready'high) = '1' then
                    if in_counter < stream_length - 1 then
                        in_counter <= in_counter + 1;
                    else
                        in_counter <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

    outgoing_elements_counter_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                out_counter <= 0;
            else
                if port_f_in_valid = '1' and port_f_in_ready(port_f_in_ready'high) = '1' then
                    if out_counter < stream_length - 1 then
                        out_counter <= out_counter + 1;
                    else
                        out_counter <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
