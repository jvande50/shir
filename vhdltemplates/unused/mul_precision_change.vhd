library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity mul is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end mul;

architecture behavioral of mul is
begin

    port_out_data <= std_logic_vector(resize((unsigned(port_in_data.t0) * unsigned(port_in_data.t1)), port_out_data'length));
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;
