library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity const_bit_vector is
    generic(
        value: std_logic_vector(1 downto 0) := "11"
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end const_bit_vector;

architecture behavioral of const_bit_vector is

begin

    port_out_data(value'high downto value'low) <= value;
    port_out_data(port_out_data'high downto port_out_data'low + value'length) <= (others => '0');
    port_out_last <= (others => '0');
    port_out_valid <= '1';

end behavioral;
