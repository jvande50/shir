library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity zip_stream is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_tuple_vector_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end zip_stream;

architecture behavioral of zip_stream is
begin

    connect: process(port_in_data)
    begin
        for i in 0 to port_out_data'length - 1 loop
            port_out_data(i).t0 <= port_in_data.t0(i);
            port_out_data(i).t1 <= port_in_data.t1(i);
        end loop;
    end process;
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;
