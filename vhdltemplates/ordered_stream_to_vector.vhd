library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity ordered_stream_to_vector is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_vector_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end ordered_stream_to_vector;

architecture behavioral of ordered_stream_to_vector is
    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');
    signal reg: data_word_vector_type; -- := (others => (others => '0')); -- not generic
    type state_type is (receiving, sending);
    signal state: state_type := receiving;
begin

    port_in_ready <= in_ready;
    ready_signal: process(state, in_ready, port_in_last)
    begin
        if state = sending then
            in_ready <= (others => '0');
        else
            -- always ready!
            in_ready(0) <= '1';
            for i in in_ready'low + 1 to in_ready'high loop
                if port_in_last(i - 1) = '1' and in_ready(i - 1) = '1' then
                    in_ready(i) <= '1';
                else
                    in_ready(i) <= '0';
                end if;
            end loop;
        end if;
    end process;

    port_out_data <= reg;
    port_out_valid <= '1' when state = sending else '0';

    shift_reg: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                -- reg <= (others => (others => '0')); -- not generic
            else
                if state = receiving then
                    if port_in_valid = '1' then
                        for i in reg'low to reg'high - 1 loop
                            reg(i) <= reg(i + 1);
                        end loop;
                        reg(reg'high) <= port_in_data;
                    end if;
                end if;
            end if;
        end if;
    end process;

    next_state_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= receiving;
            else
                case state is
                    when receiving =>
                        if port_in_valid = '1' and port_in_last = "1" then
                            state <= sending;
                        end if;
                    when sending =>
                        if port_out_ready = "1" then
                            state <= receiving;
                        end if;
                end case;
            end if;
        end if;
    end process;

end behavioral;
