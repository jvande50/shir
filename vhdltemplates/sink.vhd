library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity sink is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out std_logic_vector(-1 downto 0);
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end sink;

architecture behavioral of sink is

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

    signal finished: std_logic := '0';

begin

    port_in_ready <= in_ready;

    always_ready: process(finished, in_ready, port_in_last)
    begin
        in_ready(0) <= not finished;
        for i in in_ready'low + 1 to in_ready'high loop
            if port_in_last(i - 1) = '1' and in_ready(i - 1) = '1' then
                in_ready(i) <= '1';
            else
                in_ready(i) <= '0';
            end if;
        end loop;
    end process;

    finished_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                finished <= '0';
            else
                if finished = '0' then
                    if port_in_valid = '1' and in_ready(in_ready'high) = '1' then
                        finished <= '1';
                    end if;
                else
                    if port_out_ready(port_out_ready'low) = '1' then
                        finished <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    port_out_last <= (others => '0');
    port_out_valid <= finished;

end behavioral;
