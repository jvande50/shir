library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity registered is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end registered;

architecture behavioral of registered is

    -- this implementation uses double buffering to also 'cut' the paths for valid and ready control signals
    -- therefore registered can also be used to prevent too long combinational paths, when caused by control logic signals

    type output_buffer_type is array (0 to 1) of data_word_type;
    signal output_buffer: output_buffer_type := (others => (others => '0'));
    signal output_buffer_count: natural range 0 to (output_buffer_type'high + 1) := 0;

    subtype output_buffer_index_type is natural range output_buffer_type'range;
    signal next_free_output_buffer_entry: output_buffer_index_type := 0;
    signal first_used_output_buffer_entry: output_buffer_index_type := 0;

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');
    signal out_valid: std_logic := '0';

begin

    in_ready <= "1" when output_buffer_count <= output_buffer_type'high else "0";
    port_in_ready <= in_ready;

    port_out_data <= output_buffer(first_used_output_buffer_entry);
    port_out_last <= (others => '0');
    out_valid <= '1' when output_buffer_count > 0 else '0';
    port_out_valid <= out_valid;

    output_buffer_logic: process(clk)
        variable output_buffer_count_v: natural range 0 to (output_buffer_type'high + 2) := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                output_buffer <= (others => (others => '0')); -- enfore the use of registers instead of ram blocks
                output_buffer_count <= 0;
                next_free_output_buffer_entry <= 0;
                first_used_output_buffer_entry <= 0;
            else
                output_buffer_count_v := output_buffer_count;
                if port_in_valid = '1' and in_ready(in_ready'low) = '1' then
                    output_buffer(next_free_output_buffer_entry) <= port_in_data;
                    output_buffer_count_v := output_buffer_count_v + 1;
                    if next_free_output_buffer_entry = output_buffer_index_type'high then
                        next_free_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        next_free_output_buffer_entry <= next_free_output_buffer_entry + 1;
                    end if;
                end if;
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    output_buffer_count_v := output_buffer_count_v - 1;
                    if first_used_output_buffer_entry = output_buffer_index_type'high then
                        first_used_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        first_used_output_buffer_entry <= first_used_output_buffer_entry + 1;
                    end if;
                end if;
                output_buffer_count <= output_buffer_count_v;
            end if;
        end if;
    end process;

end behavioral;
