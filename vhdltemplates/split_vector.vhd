library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity split_vector is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_joined_vector_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_vector_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end split_vector;

architecture behavioral of split_vector is
begin

    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;
    
    process(port_in_data)
        constant chunksize: natural := port_out_data(0)'length;
    begin
        for i in 0 to port_out_data'length - 1 loop
            for j in 0 to chunksize - 1 loop
                port_out_data(i)(j) <= port_in_data(i * chunksize + j);
            end loop;
        end loop;
    end process;

end behavioral;
