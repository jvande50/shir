library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity id_hs is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end id_hs;

architecture behavioral of id_hs is
    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');
begin

    port_in_ready <= in_ready;
    ready_signal: process(in_ready, port_in_last, port_out_ready)
    begin
        if not port_out_ready = "1"then
            in_ready <= (others => '0');
        else
            -- always ready!
            in_ready(0) <= '1';
            for i in in_ready'low + 1 to in_ready'high loop
                if port_in_last(i - 1) = '1' and in_ready(i - 1) = '1' then
                    in_ready(i) <= '1';
                else
                    in_ready(i) <= '0';
                end if;
            end loop;
        end if;
    end process;

    port_out_data <= port_in_data;
    port_out_valid <= port_in_valid;

end behavioral;