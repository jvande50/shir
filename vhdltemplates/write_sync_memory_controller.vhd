library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity write_sync_memory_controller is
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- from ram
        port_mem_in_data: in data_word_type;
        port_mem_in_last: in last_0_type;
        port_mem_in_valid: in std_logic;
        port_mem_in_ready: out ready_0_type;
        -- to ram
        port_mem_out_data: out memory_input_type;
        port_mem_out_last: out last_0_type;
        port_mem_out_valid: out std_logic;
        port_mem_out_ready: in ready_0_type;

        -- from client
        port_in_data: in data_with_address_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        -- to client
        port_out_data: out std_logic_vector(-1 downto 0);
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end write_sync_memory_controller;

architecture behavioral of write_sync_memory_controller is
begin

    port_mem_out_data.data <= port_in_data.data;
    port_mem_out_data.addr <= port_in_data.addr;
    port_mem_out_data.we <= '1';
    port_mem_out_last <= port_in_last;
    port_mem_out_valid <= port_in_valid;
    port_in_ready <= port_mem_out_ready;

    port_out_data <= (others => '0');
    port_out_last <= port_mem_in_last;
    port_out_valid <= port_mem_in_valid;
    port_mem_in_ready <= port_out_ready;
 
end behavioral;
