library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity write_async is
    generic(
        data_length: natural := 8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- to controller
        port_mem_out_data: out request_cache_line_with_address_type;
        port_mem_out_valid: out std_logic;
        port_mem_out_ready: in std_logic;
        -- from controller
        port_mem_in_data: in request_cache_line_type;
        port_mem_in_valid: in std_logic;
        -- stream of tuple of data and address
        port_in_data: in write_input_tuple_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        -- base address (added to address)
        port_in_baseaddr_data: in base_address_type;
        port_in_baseaddr_last: in last_0_type;
        port_in_baseaddr_valid: in std_logic;
        port_in_baseaddr_ready: out ready_0_type;

        -- outgoing base address after all writes have finished
        port_out_data: out base_address_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end write_async;

architecture behavioral of write_async is
    type state_type is (writing, waiting, confirming);
    signal state: state_type := writing;

    signal base_addr_reg: std_logic_vector(port_in_baseaddr_data'range) := (others => '0');

    signal requested: natural range 0 to data_length := 0;
    signal delivered: natural range 0 to data_length := 0;
begin

    -- requests
    port_mem_out_data.data.data <= port_in_data.t0;
    port_mem_out_data.data.addr <= std_logic_vector(to_unsigned(
                                   to_integer(unsigned(port_in_baseaddr_data)) +
                                   to_integer(unsigned(port_in_data.t1)),
                                   port_mem_out_data.data.addr'length)) when port_in_baseaddr_valid = '1' and port_in_valid = '1' else (others => '0');
    port_mem_out_data.req_id <= (others => '0'); -- can also send port_in_data.data.idx, but the req id is never used
    port_mem_out_valid <= port_in_baseaddr_valid and port_in_valid when state = writing else '0';

    port_in_baseaddr_ready(0) <= port_in_last(port_in_last'high) when state = writing and port_in_valid = '1' and port_in_baseaddr_valid = '1' and port_mem_out_ready = '1' else '0';
    port_in_ready <= port_in_last(port_in_last'high) & '1' when state = writing and port_in_valid = '1' and port_in_baseaddr_valid = '1' and port_mem_out_ready = '1' else (others => '0');

    -- responses out
    port_out_data <= base_addr_reg;
    port_out_valid <= '1' when state = confirming else '0';

    base_addr_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                base_addr_reg <= (others => '0');
            else
                if state = writing then
                    if port_mem_out_ready = '1' then
                        if requested >= data_length - 1 and port_in_baseaddr_valid = '1' and port_in_valid = '1' then
                            base_addr_reg <= port_in_baseaddr_data;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    request: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= writing;
            else
                case state is
                    when writing =>
                        if port_mem_out_ready = '1' then
                            if requested >= data_length - 1 and port_in_baseaddr_valid = '1' and port_in_valid = '1' then
                                state <= waiting;
                            end if;
                        end if;
                    when waiting =>
                        if delivered >= requested - 1 and port_mem_in_valid = '1' then
                            state <= confirming;
                        end if;
                    when confirming =>
                        if port_out_ready = "1" then
                            state <= writing;
                        end if;
                end case;
            end if;
        end if;
    end process;

    entry_selector: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                requested <= 0;
                delivered <= 0;
            else
                if port_in_baseaddr_valid = '1' and port_in_valid = '1' and state = writing and port_mem_out_ready = '1' then
                    requested <= requested + 1;
                end if;
                if port_mem_in_valid = '1' and (state = writing or state = waiting) then
                    delivered <= delivered + 1;
                end if;
                if state = confirming and port_out_ready = "1" then
                    requested <= 0;
                    delivered <= 0;
                end if;
            end if;
        end if;
    end process;

end behavioral;
