library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity map_stream_delay is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type;
        port_in_control_last: in last_1_type;
        port_in_control_ready: in ready_1_type
    );
end map_stream_delay;

architecture behavioral of map_stream_delay is
    constant control_last_all_one: std_logic_vector(port_in_control_last'range) := (others => '1');
    
    -- signal buffer_data: data_type;
    -- signal buffer_last: std_logic_vector(port_in_last'range) := (others => '0');
    -- signal buffer_valid: std_logic := '0';
    signal buffer_ready: std_logic_vector(port_in_ready'range) := (others => '0');

    -- type buffer_state_type is (empty, full, confirming);
    -- signal buffer_state: buffer_state_type := empty;

    type state_type is (forwarding, ctrl_ready, main_ready, all_ready);
    signal state: state_type := forwarding;
begin

    port_out_data <= port_in_data;
    port_out_last <= port_in_last when state = forwarding or state = ctrl_ready else (others => '0');
    port_out_valid <= port_in_valid when state = forwarding or state = ctrl_ready else '0';
    ready_signal: process(state, port_out_ready, port_in_control_last, buffer_ready)
    begin
        if (state = forwarding or state = ctrl_ready) and port_in_control_last /= control_last_all_one then
            port_in_ready <= port_out_ready;
            port_in_ready(port_in_ready'high) <= '0';
        elsif state = all_ready then
            port_in_ready <= buffer_ready;
        else
            port_in_ready <= (others => '0');
        end if;
    end process;

    state_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= forwarding;
                buffer_ready <= (others => '0');
            else
                case state is
                    when forwarding =>
                        if port_in_valid = '1' and port_in_control_last = control_last_all_one then
                            if port_in_control_ready(port_in_control_ready'low) = '1' and port_out_ready(port_out_ready'low) = '1' then
                                state <= all_ready;
                                buffer_ready <= port_in_control_ready(port_in_control_ready'high downto port_in_control_ready'high - port_in_ready'length + 1);
                            elsif port_in_control_ready(port_in_control_ready'low) = '1' then
                                state <= ctrl_ready;
                                buffer_ready <= port_in_control_ready(port_in_control_ready'high downto port_in_control_ready'high - port_in_ready'length + 1);
                            elsif port_out_ready(port_out_ready'low) = '1' then
                                state <= main_ready;
                            end if;
                        end if;
                    when ctrl_ready =>
                        if port_in_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                            state <= all_ready;
                        end if;
                    when main_ready =>
                        if port_in_control_last = control_last_all_one and port_in_control_ready(port_in_control_ready'low) = '1' then
                            state <= all_ready;
                            buffer_ready <= port_in_control_ready(port_in_control_ready'high downto port_in_control_ready'high - port_in_ready'length + 1);
                        end if;
                    when all_ready =>
                        if port_in_valid = '1' then
                            state <= forwarding;
                        end if;
                end case;
            end if;
        end if;
    end process;

end behavioral;

