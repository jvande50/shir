library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity arbiter_async_function is
    generic(
        num_clients: natural := 2;
        req_list_size: natural := 64
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        -- master request
        port_out_data: out function_in_type;
        port_out_valid: out std_logic;
        port_out_ready: in std_logic;
        -- master response
        port_in_data: in function_out_type;
        port_in_valid: in std_logic;
        -- no ready signal, because the master may not have a buffer to store results => this arbiter must accect responses at any time and forward it directly to the clients

        -- client requests
        port_in_clients_data: in function_in_vector_type(num_clients-1 downto 0);
        port_in_clients_valid: in std_logic_vector(num_clients-1 downto 0);
        port_in_clients_ready: out std_logic_vector(num_clients-1 downto 0);
        -- client response
        port_out_clients_data: out function_out_vector_type(num_clients-1 downto 0);
        port_out_clients_valid: out std_logic_vector(num_clients-1 downto 0)
        -- no ready signal, clients must accept forwarded date from master at any time!
    );
end arbiter_async_function;

architecture behavioral of arbiter_async_function is

    subtype client_id_type is natural range 0 to num_clients-1;

    type req_entry is record
        orig_req_id: std_logic_vector(port_out_data.req_id'range);
        cli_id: client_id_type;
    end record req_entry;
    constant empty_req_entry: req_entry := (orig_req_id => (others => '0'), cli_id => 0);

    type req_list_type is array (0 to req_list_size-1) of req_entry;
    signal req_list: req_list_type := (others => empty_req_entry);

    subtype req_entry_list_index_type is natural range req_list_type'range;
    type req_entry_list_type is array (req_list_type'range) of req_entry_list_index_type;
    signal req_entry_list: req_entry_list_type := (others => 0);
    signal next_free_req_entry: req_entry_list_index_type := 0;
    signal first_used_req_entry: req_entry_list_index_type := 0;
    signal req_count: natural range 0 to req_list_size := 0;

    signal selected_client: client_id_type := 0;

    signal req_valid: std_logic := '0';
    signal req_ready: std_logic := '0';

    procedure incr(signal index: inout natural range req_list_type'range) is
    begin
        if index = req_list_type'high then
            index <= req_list_type'low;
        else
            index <= index + 1;
        end if;
    end procedure;

begin

    process(port_in_clients_data, selected_client, req_entry_list, next_free_req_entry)
    begin
        port_out_data <= port_in_clients_data(selected_client); -- in case of a write response this may just contain the req_id from the master. However, this we be overwritten in the next line, with the original req_id => it works for any data type
        port_out_data.req_id <= std_logic_vector(to_unsigned(req_entry_list(next_free_req_entry), port_out_data.req_id'length));
    end process;
    port_out_valid <= req_valid;

    req_valid <= port_in_clients_valid(selected_client) when req_count < req_list_size else '0';

    req_ready <= port_out_ready when req_count < req_list_size else '0';

    process(selected_client, port_out_ready, req_count)
    begin
        port_in_clients_ready <= (others => '0');
        if req_count < req_list_size then
            port_in_clients_ready(selected_client) <= port_out_ready;
        end if;
    end process;


    -- simple round robin WITH possible wait times! (if a client is not ready to make a request)
    select_client_logic: process(clk)
        variable next_client: client_id_type := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                selected_client <= 0;
            else
                if selected_client = client_id_type'high then
                    next_client := client_id_type'low;
                else
                    next_client := selected_client + 1;
                end if;
                -- remove condition here to go back to true round robin without possible starvation of clients
                --if port_in_clients_valid(selected_client) = '0' or port_in_clients_valid(next_client) = '1' then
                    selected_client <= next_client;
                --end if;
            end if;
        end if;
    end process;

    req_entry_list_logic: process(clk)
        variable req_count_v: natural range 0 to req_list_size := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                next_free_req_entry <= 0;
                first_used_req_entry <= 0;
                req_count <= 0;
                for i in req_list_type'range loop
                    req_entry_list(i) <= i;
                end loop;
            else
                req_count_v := req_count;
                if req_valid = '1' and req_ready = '1' then
                    incr(next_free_req_entry);
                    req_count_v := req_count_v + 1;
                end if;
                if port_in_valid = '1' then
                    req_entry_list(first_used_req_entry) <= to_integer(unsigned(port_in_data.req_id));
                    incr(first_used_req_entry);
                    req_count_v := req_count_v - 1;
                end if;
                req_count <= req_count_v;
            end if;
        end if;
    end process;

    requests: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
            else
                if req_valid = '1' and req_ready = '1' then
                    req_list(req_entry_list(next_free_req_entry)).orig_req_id <= port_in_clients_data(selected_client).req_id;
                    req_list(req_entry_list(next_free_req_entry)).cli_id <= selected_client;
                end if;
            end if;
        end if;
    end process;


    -- responses
    -- TODO maybe buffer (just 1 cycle latency!)
    process(port_in_data, port_in_valid, req_list)
    begin
        port_out_clients_data <= (others => (data => (others => '0'), req_id => (others => '0')));
        port_out_clients_valid <= (others => '0');
        if port_in_valid = '1' then
            port_out_clients_data(req_list(to_integer(unsigned(port_in_data.req_id))).cli_id) <= port_in_data;
            port_out_clients_data(req_list(to_integer(unsigned(port_in_data.req_id))).cli_id).req_id <= req_list(to_integer(unsigned(port_in_data.req_id))).orig_req_id;
            port_out_clients_valid(req_list(to_integer(unsigned(port_in_data.req_id))).cli_id) <= '1';
        end if;
    end process;

end behavioral;
