library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity drop_vector is
    generic(
        low_elements: natural := 2;
        high_elements: natural := 1
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_vector_slice_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end drop_vector;

architecture behavioral of drop_vector is
begin

    port_out_data <= data_word_vector_slice_type(port_in_data((data_word_vector_type'high - high_elements) downto (data_word_vector_type'low + low_elements)));
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;
