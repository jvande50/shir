library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity arbiter_sync_function is
    generic(
        num_clients: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        -- to master
        port_out_data: out function_in_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type;
        -- from master
        port_in_data: in function_out_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;

        -- from clients
        port_in_clients_data: in function_in_vector_type(num_clients-1 downto 0);
        port_in_clients_last: in function_last_vector_type(num_clients-1 downto 0);
        port_in_clients_valid: in std_logic_vector(num_clients-1 downto 0);
        port_in_clients_ready: out function_ready_vector_type(num_clients-1 downto 0);
        -- to clients
        port_out_clients_data: out function_out_vector_type(num_clients-1 downto 0);
        port_out_clients_last: out function_last_vector_type(num_clients-1 downto 0);
        port_out_clients_valid: out std_logic_vector(num_clients-1 downto 0);
        port_out_clients_ready: in function_ready_vector_type(num_clients-1 downto 0)
    );
end arbiter_sync_function;

architecture behavioral of arbiter_sync_function is

    subtype client_id_type is natural range 0 to num_clients-1;

    signal selected_client: client_id_type := 0;

    signal pipeline_count: natural := 0;

    signal start_arrival: std_logic := '0';

begin

    -- to master
    port_out_data <= port_in_clients_data(selected_client);
    port_out_last <= port_in_clients_last(selected_client);
    port_out_valid <= port_in_clients_valid(selected_client);
    master_ready_signal: process(selected_client, port_out_ready)
    begin
        port_in_clients_ready <= (others => (others => '0'));
        port_in_clients_ready(selected_client) <= port_out_ready;
    end process;

    -- from master
    port_out_clients_data <= (others => port_in_data);
    port_out_clients_last <= (others => port_in_last);
    clients_valid_signal: process(selected_client, port_in_valid)
    begin
        port_out_clients_valid <= (others => '0');
        port_out_clients_valid(selected_client) <= port_in_valid;
    end process;
    port_in_ready <= port_out_clients_ready(selected_client);

    -- simple round robin WITH possible wait times! (if a client is not ready to make a request)
    select_client_logic: process(clk)
        variable next_client: client_id_type := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                selected_client <= 0;
            else
                if selected_client = client_id_type'high then
                    next_client := client_id_type'low;
                else
                    next_client := selected_client + 1;
                end if;
                -- remove second condition here to go back to true round robin without possible starvation of clients
                if pipeline_count = 0 and port_in_clients_valid(selected_client) = '0' and start_arrival = '0' then
                    selected_client <= next_client;
                end if;
            end if;
        end if;
    end process;

    pipeline_counter: process(clk)
        variable pipeline_count_v: natural := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                pipeline_count <= 0;
            else
                pipeline_count_v := pipeline_count;
                if port_in_clients_valid(selected_client) = '1' and port_out_ready(port_out_ready'low) = '1' then
                    pipeline_count_v := pipeline_count_v + 1;
                end if;
                if port_in_valid = '1' and port_out_clients_ready(selected_client)(port_out_ready'low) = '1' then
                    pipeline_count_v := pipeline_count_v - 1;
                end if;
                pipeline_count <= pipeline_count_v;
            end if;
        end if;
    end process;

    start_logic: process(clk)
        constant last_all_one: std_logic_vector(port_in_clients_last(0)'range) := (others => '1');
    begin
        if rising_edge(clk) then
            if reset = '1' then
                start_arrival <= '0';
            else
                if port_in_clients_valid(selected_client) = '1' and port_out_ready(port_out_ready'low) = '1' then
                    if port_in_clients_last(selected_client) = last_all_one then
                        start_arrival <= '0';
                    else
                        start_arrival <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
