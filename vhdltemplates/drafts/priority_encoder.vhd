library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity priority_encoder is
    generic(
        width: natural := 64
    );
    port(
        port_in_data: in std_logic_vector(width-1 downto 0);
        port_out_index: out natural range 0 to width-1;
        port_out_valid: out std_logic
    );
end priority_encoder;

architecture behavioral of priority_encoder is

    constant DEPTH: natural := integer(ceil(log2(real(width)))); -- 6 for width = 64
    signal valid: std_logic_vector(2*width-1 downto 0);
    type index_type is array(2*width-1 downto 0) of natural range 0 to width-1;
    signal index: index_type;

begin

    port_out_index <= index(1);
    port_out_valid <= valid(1);

    process(port_in_data, index, valid)
    begin
        valid <= (others => '0');
        index <= (others => 0);
        valid(width*2-1 downto width) <= port_in_data;
        for i in 0 to width-1 loop
            index(width + i) <= i;
        end loop;
        for level in 1 to DEPTH loop
            for i in 2**(DEPTH - level + 1) - 1 downto 2**(DEPTH - level) loop
                if valid(i * 2 + 1) = '1' then
                    index(i) <= index(i * 2 + 1);
                else
                    index(i) <= index(i * 2);
                end if;
                valid(i) <= valid(i * 2 + 1) or valid(i * 2);
            end loop;
        end loop;
    end process;

end behavioral;
