library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity slide_ordered_stream is
    generic(
        window_width: natural := 4;
		step_size: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_window_vector_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end slide_ordered_stream;

architecture behavioral of slide_ordered_stream is
    type shift_reg_type is array(window_width - 2 downto 0) of data_word_type;

    signal elements: natural range 0 to window_width - 1 := 0;

    signal reg: shift_reg_type;

    signal out_valid: std_logic := '0';
    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

begin

    data_signal: process(port_in_data, reg)
    begin
        for i in reg'low to reg'high loop
            port_out_data(i) <= reg(i);
        end loop;
        port_out_data(port_out_data'high) <= port_in_data;
    end process;

    port_out_last <= port_in_last;

    out_valid <= '1' when port_in_valid = '1' and elements = window_width - 1 else '0';
    port_out_valid <= out_valid;

    port_in_ready <= in_ready;
    ready_signal: process(port_out_ready, elements)
    begin
        in_ready <= port_out_ready;
        -- always ready if shift register is not full!
        if elements < window_width - 1 then
            in_ready(in_ready'low) <= '1';
        end if;
    end process;

    shift_reg_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                --reg <= (others => (others => '0'));
            else
                if port_in_valid = '1' and in_ready(in_ready'low) = '1' then
                    -- shift
                    for i in reg'low to reg'high - 1 loop
                        reg(i) <= reg(i + 1);
                    end loop;
                    reg(reg'high) <= port_in_data;
                end if;
            end if;
        end if;
    end process;

    element_counter_logic: process(clk)
        variable elements_v: natural range 0 to window_width := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                elements <= 0;
            else
                elements_v := elements;
                if port_in_valid = '1' and in_ready(in_ready'low) = '1' then
                    elements_v := elements_v + 1;
                end if;
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    if port_in_last(port_in_last'high) = '1' then
                        elements_v := 0;
                    else
                        elements_v := elements_v - step_size;
                    end if;
                end if;
                elements <= elements_v;
            end if;
        end if;
    end process;

end behavioral;
