library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity read_local_memory_controller is
	port(
		clk: in std_logic;
		reset: in std_logic;
		-- this controller must stall until the entire memory is ready to be accessed
		port_in_mem_ready: in std_logic;
		
		-- request to mem controller
		port_out_req_data: out mixed_cache_line_type;
		port_out_req_valid: out std_logic;
		
		-- response from mem controller
		port_in_rsp_data: in request_cache_line_type;
		port_in_rsp_valid: in std_logic;

		-- client requests
		port_in_data: in request_mem_address_type;
		port_in_valid: in std_logic;
		port_in_ready: out std_logic;
		
		-- client response
		port_out_data: out request_cache_line_type;
		port_out_valid: out std_logic
	);
end read_local_memory_controller;

architecture behavioral of read_local_memory_controller is

begin

	port_out_req_data.addr <= port_in_data.data;
	port_out_req_data.req_id <= port_in_data.req_id;
	port_out_req_data.we <= '0';

	port_out_req_valid <= port_in_valid;

	port_in_ready <= port_in_mem_ready;

	port_out_data.data <= port_in_rsp_data.data;
	port_out_data.req_id <= port_in_rsp_data.req_id;

	port_out_valid <= port_in_rsp_valid;
 
end behavioral;
