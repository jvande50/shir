library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity collector_arbiter is
    generic(
        num_clients: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_clients_data: in data_word_vector_generic_type(num_clients - 1 downto 0);
        port_in_clients_last: in last_0_vector_generic_type(num_clients - 1 downto 0);
        port_in_clients_valid: in std_logic_vector(num_clients - 1 downto 0);
        port_in_clients_ready: out ready_0_vector_generic_type(num_clients - 1 downto 0);

        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end collector_arbiter;

architecture behavioral of collector_arbiter is

    subtype client_id_type is natural range 0 to num_clients - 1;
    signal current_client: client_id_type := 0;

begin

    port_out_data <= port_in_clients_data(current_client);
    port_out_valid <= port_in_clients_valid(current_client);
    process(current_client, port_out_ready)
    begin
        port_in_clients_ready <= (others => "0");
        port_in_clients_ready(current_client) <= port_out_ready;
    end process;
    
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                current_client <= 0;
            else
                -- go to next client if current client does not send valid data or 
                -- the transmission has finished
                if port_in_clients_valid(current_client) = '0' or
                    (port_in_clients_valid(current_client) = '1' and port_out_ready = "1") then
                    if current_client < num_clients-1 then
                        current_client <= current_client + 1;
                    else
                        current_client <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
