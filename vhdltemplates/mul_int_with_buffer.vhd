library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity mul_int is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_mul_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end mul_int;

architecture behavioral of mul_int is
    signal mul_in1_reg1: std_logic_vector(port_in_data.t0'range) := (others => '0');
    signal mul_in2_reg1: std_logic_vector(port_in_data.t1'range) := (others => '0');
    signal mul_in1_reg2: std_logic_vector(port_in_data.t0'range) := (others => '0');
    signal mul_in2_reg2: std_logic_vector(port_in_data.t1'range) := (others => '0');
    signal mul_in_valid1: std_logic := '0';
    signal mul_in_valid2: std_logic := '0';
    signal mul_out_reg1: std_logic_vector(port_out_data'range) := (others => '0');
    signal mul_out_reg2: std_logic_vector(port_out_data'range) := (others => '0');
    signal mul_out_valid1: std_logic := '0';
    signal mul_out_valid2: std_logic := '0';


    signal pipeline_count: natural range 0 to 4 := 0;

    type output_buffer_type is array (0 to 5) of std_logic_vector(port_out_data'range);
    signal output_buffer: output_buffer_type := (others => (others => '0'));
    signal output_buffer_count: natural range 0 to (output_buffer_type'high + 1) := 0;

    subtype output_buffer_index_type is natural range output_buffer_type'range;
    signal next_free_output_buffer_entry: output_buffer_index_type := 0;
    signal first_used_output_buffer_entry: output_buffer_index_type := 0;

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');
    signal out_valid: std_logic := '0';

begin

    in_ready <= "1" when output_buffer_count + pipeline_count <= output_buffer_type'high else "0";
    port_in_ready <= in_ready;

    port_out_data <= output_buffer(first_used_output_buffer_entry);
    port_out_last <= (others => '0');
    out_valid <= '1' when output_buffer_count > 0 else '0';
    port_out_valid <= out_valid;

    mul_logic: process(clk, reset)
    begin
        -- reset must be async here in order to exploit DSP output registers!
        if reset = '1' then
            mul_in1_reg1 <= (others => '0');
            mul_in2_reg1 <= (others => '0');
            mul_in1_reg2 <= (others => '0');
            mul_in2_reg2 <= (others => '0');
            mul_out_reg1 <= (others => '0');
            mul_out_reg2 <= (others => '0');
        else
            -- MUST NOT USE rising_edge(clk) here! DSP registers will not be used correctly otherwise!!!!
            if (clk'event and clk = '1') then
                mul_in1_reg1 <= port_in_data.t0;
                mul_in2_reg1 <= port_in_data.t1;
                mul_in1_reg2 <= mul_in1_reg1;
                mul_in2_reg2 <= mul_in2_reg1;
                mul_out_reg1 <= std_logic_vector(unsigned(mul_in1_reg2) * unsigned(mul_in2_reg2));
                mul_out_reg2 <= mul_out_reg1;
            end if;
        end if;
    end process;

    pipeline_counter: process(clk)
        variable pipeline_count_v: natural range 0 to (4 + 1) := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                pipeline_count <= 0;
            else
                pipeline_count_v := pipeline_count;
                if port_in_valid = '1' and in_ready = "1" then
                    pipeline_count_v := pipeline_count_v + 1;
                end if;
                if mul_out_valid2 = '1' then
                    pipeline_count_v := pipeline_count_v - 1;
                end if;
                pipeline_count <= pipeline_count_v;
            end if;
        end if;
    end process;

    output_buffer_logic: process(clk)
        variable output_buffer_count_v: natural range 0 to (output_buffer_type'high + 2) := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                output_buffer <= (others => (others => '0')); -- enfore the use of registers instead of ram blocks
                output_buffer_count <= 0;
                next_free_output_buffer_entry <= 0;
                first_used_output_buffer_entry <= 0;
            else
                output_buffer_count_v := output_buffer_count;
                if mul_out_valid2 = '1' then
                    output_buffer(next_free_output_buffer_entry) <= mul_out_reg2;
                    output_buffer_count_v := output_buffer_count_v + 1;
                    if next_free_output_buffer_entry = output_buffer_index_type'high then
                        next_free_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        next_free_output_buffer_entry <= next_free_output_buffer_entry + 1;
                    end if;
                end if;
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    output_buffer_count_v := output_buffer_count_v - 1;
                    if first_used_output_buffer_entry = output_buffer_index_type'high then
                        first_used_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        first_used_output_buffer_entry <= first_used_output_buffer_entry + 1;
                    end if;
                end if;
                output_buffer_count <= output_buffer_count_v;
            end if;
        end if;
    end process;

    pipeline_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                mul_in_valid1 <= '0';
                mul_in_valid2 <= '0';
                mul_out_valid1 <= '0';
                mul_out_valid2 <= '0';
            else
                mul_in_valid1 <= '0';
                if port_in_valid = '1' and in_ready = "1" then
                    mul_in_valid1 <= '1';
                end if;

                mul_in_valid2 <= mul_in_valid1;
                mul_out_valid1 <= mul_in_valid2;
                mul_out_valid2 <= mul_out_valid1;
            end if;
        end if;
    end process;

end behavioral;
