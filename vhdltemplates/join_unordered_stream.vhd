library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity join_unordered_stream is
    generic(
        inner_stream_length: natural := 8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_unordered_stream_of_unordered_stream_type;
        port_in_last: in last_2_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_2_type;
        port_out_data: out data_word_joined_unordered_stream_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end join_unordered_stream;

architecture behavioral of join_unordered_stream is
begin

    port_out_data.data <= port_in_data.data.data;
    port_out_data.idx <= std_logic_vector(to_unsigned(to_integer(unsigned(port_in_data.idx)) * inner_stream_length + to_integer(unsigned(port_in_data.data.idx)), port_out_data.idx'length)) when port_in_valid = '1' else (others => '0');
    port_out_valid <= port_in_valid;

    last_signal: process(port_in_last)
    begin
        port_out_last <= port_in_last(port_in_last'high - 1 downto port_in_last'low);
        if port_in_last(port_in_last'high) = '1' and port_in_last(port_in_last'high - 1) = '1' then
            port_out_last(port_out_last'high) <= '1';
        else
            port_out_last(port_out_last'high) <= '0';
        end if;
    end process;

    ready_signal: process(port_out_ready, port_in_last)
    begin
        port_in_ready <= port_out_ready(port_out_ready'high) & port_out_ready;
        if port_in_last(port_in_last'high - 1) = '1' and port_out_ready(port_out_ready'high - 1) = '1' then
            -- there will never be a demand to repeat the inner stream only!
            port_in_ready(port_in_ready'high - 1) <= '1';
        else
            port_in_ready(port_in_ready'high - 1) <= '0';
        end if;
    end process;

end behavioral;
