library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity read_async is
    generic(
        data_length: natural :=  8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- to controller
        port_mem_out_data: out request_mem_address_type;
        port_mem_out_valid: out std_logic;
        port_mem_out_ready: in std_logic;
        -- from controller
        port_mem_in_data: in request_cache_line_type;
        port_mem_in_valid: in std_logic;
        -- stream of addresses
        port_in_data: in data_word_stream_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        -- base address (added to address)
        port_in_baseaddr_data: in base_address_type;
        port_in_baseaddr_last: in last_0_type;
        port_in_baseaddr_valid: in std_logic;
        port_in_baseaddr_ready: out ready_0_type;

        -- outgoing unordered stream of tuple of data and address
        port_out_data: out cache_line_unordered_stream_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end read_async;

architecture behavioral of read_async is

    component ram_dp
        generic (
            data_width: natural := 32;
            addr_width: natural := 4;
            entries: natural := 8
        );
        port(
            clk: in std_logic;
            data_in: in std_logic_vector(data_width - 1 downto 0);
            write_addr: in std_logic_vector(addr_width - 1 downto 0);
            read_addr: in std_logic_vector(addr_width - 1 downto 0);
            we: in std_logic;
            data_out: out std_logic_vector(data_width - 1 downto 0)
        );
    end component;

    signal write_addr: std_logic_vector(port_out_data.idx'range) := (others => '0');
    signal read_addr: std_logic_vector(port_out_data.idx'range) := (others => '0');
    signal write_enable: std_logic := '0';

    signal req_id: natural range 0 to data_length - 1 := 0;

    type state_type is (waiting, requesting, receiving, confirming);
    signal state: state_type := waiting;

    signal sending_entry: natural range 0 to data_length := 0;
    signal receiving_entry: natural range 0 to data_length := 0;

    signal out_valid: std_logic := '0';

begin

    inferred_data_ram: ram_dp
    generic map(
        data_width => port_out_data.data'length,
        addr_width => write_addr'length,
        entries => data_length
    )
    port map(
        clk => clk,
        data_in => port_mem_in_data.data,
        write_addr => write_addr,
        read_addr => read_addr,
        we => write_enable,
        data_out => port_out_data.data
    );

    inferred_idx_ram: ram_dp
    generic map(
        data_width => port_out_data.idx'length,
        addr_width => write_addr'length,
        entries => data_length
    )
    port map(
        clk => clk,
        data_in => port_mem_in_data.req_id(port_out_data.idx'high downto port_out_data.idx'low),
        write_addr => write_addr,
        read_addr => read_addr,
        we => write_enable,
        data_out => port_out_data.idx
    );

    write_addr <= std_logic_vector(to_unsigned(receiving_entry, write_addr'length));
    read_addr <= std_logic_vector(to_unsigned(sending_entry, read_addr'length));
    write_enable <= port_mem_in_valid;

    -- requests
    port_mem_out_data.data <= std_logic_vector(to_unsigned(
                              to_integer(unsigned(port_in_baseaddr_data)) +
                              to_integer(unsigned(port_in_data)),
                              port_mem_out_data.data'length)) when state = requesting else (others => '0');
    port_mem_out_data.req_id <= std_logic_vector(to_unsigned(req_id, port_mem_out_data.req_id'length));
    port_mem_out_valid <= '1' when state = requesting and port_in_valid = '1' and port_in_baseaddr_valid = '1' else '0';
    port_in_ready <= port_in_last(port_in_last'high) & '1' when state = requesting and port_mem_out_ready = '1' else (others => '0');
    port_in_baseaddr_ready <= "1" when port_in_last(port_in_last'high) = '1' and state = requesting and port_mem_out_ready = '1' else (others => '0');

    -- responses out
    port_out_last <= "1" when sending_entry = data_length - 1 else "0";
    port_out_valid <= out_valid;

    request_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= waiting;
                req_id <= 0;
            else
                case state is
                    when waiting =>
                        req_id <= 0;
                        if port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                            state <= requesting;
                        end if;
                    when requesting =>
                        if port_mem_out_ready = '1' and port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                            if req_id < data_length - 1 then
                                req_id <= req_id + 1;
                            else
                                state <= receiving;
                            end if;
                        end if;
                    when receiving =>
                        if sending_entry = data_length then
                            state <= confirming;
                        end if;
                    when confirming =>
                        if port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                            state <= waiting;
                        end if;
                end case;
            end if;
        end if;
    end process;

    receive_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                receiving_entry <= 0;
            else
                if port_mem_in_valid = '1' then
                    receiving_entry <= receiving_entry + 1;
                end if;
                if state = confirming and port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                    receiving_entry <= 0;
                end if;
            end if;
        end if;
    end process;

    send_logic: process(clk)
        variable sending_entry_v: natural range 0 to data_length := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                sending_entry <= 0;
                out_valid <= '0';
            else
                sending_entry_v := sending_entry;
                out_valid <= '0';
                if out_valid = '1' and p2_in_ready(p2_in_ready'low) = '1' then
                    -- last element, but ready bit is not 1 --> repeat entire data from buffer
                    if sending_entry_v = data_length - 1 and p2_in_ready(p2_in_ready'high) = '0' then
                        sending_entry_v := 0; -- repeat optimisation
                    else
                        sending_entry_v := sending_entry + 1;
                    end if;
                end if;

                if sending_entry_v < data_length then
                    if data_buffer_valid(sending_entry_v) = '1' then
                        out_valid <= '1';
                    end if;
                end if;
                
                if state = confirming and p1_in_valid = '1' and p0_in_valid = '1' then
                    sending_entry_v := 0;
                end if;
                sending_entry <= sending_entry_v;
            end if;
        end if;
    end process;

end behavioral;
