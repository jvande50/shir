library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity distributor is
    generic(
        num_clients: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_stream_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;

        port_out_clients_data: out data_word_stream_vector_generic_type(num_clients - 1 downto 0);
        port_out_clients_last: out last_1_vector_generic_type(num_clients - 1 downto 0);
        port_out_clients_valid: out std_logic_vector(num_clients - 1 downto 0);
        port_out_clients_ready: in ready_1_vector_generic_type(num_clients - 1 downto 0)
    );
end distributor;

architecture behavioral of distributor is
    signal transmitted: std_logic_vector(num_clients-1 downto 0) := (others => '0');
begin

    port_out_clients_data <= (others => port_in_data);
    port_out_clients_last <= (others => port_in_last);

    valid_signal: process(port_in_valid, transmitted)
    begin
        for i in transmitted'range loop
            port_out_clients_valid(i) <= port_in_valid and not transmitted(i);
        end loop;
    end process;

     ready_signal: process(port_out_clients_ready, transmitted)
         variable ready: std_logic_vector(port_in_ready'range) := (others => '1');
         constant transmitted_all_one: std_logic_vector(transmitted'range) := (others => '1');
     begin
         if transmitted = transmitted_all_one then
             ready := (others => '0');
         else
            ready := (others => '1');
         end if;

         for i in 0 to num_clients - 1 loop
              if transmitted(i) = '0' then
                  ready := ready and port_out_clients_ready(i);
              end if;
         end loop;
         port_in_ready <= ready;
     end process;

    transmitted_state: process(clk)
        constant transmitted_all_one: std_logic_vector(transmitted'range) := (others => '1');
        variable transmitted_v: std_logic_vector(transmitted'range) := (others => '0');
    begin
        if rising_edge(clk) then
            if reset = '1' then
                transmitted <= (others => '0');
            else
                transmitted_v := transmitted;
                if port_in_valid = '1' then
                    for i in transmitted'range loop
                        if port_out_clients_ready(i)(0) = '1' then
                            -- mark current client, because it has received the data now
                            transmitted_v(i) := '1';
                        end if;
                    end loop;
                end if;

                -- if all clients received the data, transmittion is complete. Reset the state!
                if transmitted_v = transmitted_all_one then
                    transmitted_v := (others => '0');
                end if;

                transmitted <= transmitted_v;
            end if;
        end if;
    end process;

end behavioral;
