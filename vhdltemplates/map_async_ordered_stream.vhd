library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity map_buffered_ordered_stream is
    generic(
        stream_length: natural := 4
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        -- inputs
        port_out_data: out data_word_type;
        port_out_last: out last_2_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_2_type
    );
end map_buffered_ordered_stream;

architecture behavioral of map_buffered_ordered_stream is
    component add is -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_in_1_data: in data_word_type; -- %TESTING_ONLY
            port_in_1_last: in last_0_type; -- %TESTING_ONLY
            port_in_1_valid: in std_logic; -- %TESTING_ONLY
            port_in_1_ready: out ready_0_type; -- %TESTING_ONLY
            port_in_2_data: in data_word_type; -- %TESTING_ONLY
            port_in_2_last: in last_0_type; -- %TESTING_ONLY
            port_in_2_valid: in std_logic; -- %TESTING_ONLY
            port_in_2_ready: out ready_0_type; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_0_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_0_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    signal port_f_in_data: data_word_type; -- %TESTING_ONLY
    signal port_f_in_last: last_1_type; -- %TESTING_ONLY
    signal port_f_in_valid: std_logic; -- %TESTING_ONLY
    signal port_f_in_ready: ready_1_type; -- %TESTING_ONLY
    signal port_f_out_1_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_1_last: last_1_type; -- %TESTING_ONLY
    signal port_f_out_1_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_1_ready: ready_1_type; -- %TESTING_ONLY
    signal port_f_out_2_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_2_last: last_1_type; -- %TESTING_ONLY
    signal port_f_out_2_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_2_ready: ready_1_type; -- %TESTING_ONLY

    signal in_1_counter: natural range 0 to stream_length - 1 := 0; -- %TESTING_ONLY
    signal in_2_counter: natural range 0 to stream_length - 1 := 0; -- %TESTING_ONLY
    signal out_counter: natural range 0 to stream_length - 1 := 0;

begin

    f: map_function port map( -- %TESTING_ONLY
        clk => clk, -- %TESTING_ONLY
        reset => reset, -- %TESTING_ONLY
        port_in_1_data => port_f_out_1_data, -- %TESTING_ONLY
        port_in_1_last => port_f_out_1_last, -- %TESTING_ONLY
        port_in_1_valid => port_f_out_1_valid, -- %TESTING_ONLY
        port_in_1_ready => port_f_out_1_ready, -- %TESTING_ONLY
        port_in_2_data => port_f_out_2_data, -- %TESTING_ONLY
        port_in_2_last => port_f_out_2_last, -- %TESTING_ONLY
        port_in_2_valid => port_f_out_2_valid, -- %TESTING_ONLY
        port_in_2_ready => port_f_out_2_ready, -- %TESTING_ONLY
        port_out_data => port_f_in_data, -- %TESTING_ONLY
        port_out_last => port_f_in_last,  -- %TESTING_ONLY
        port_out_valid => port_f_in_valid,  -- %TESTING_ONLY
        port_out_ready => port_f_in_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    port_out_data <= port_f_in_data;
    port_out_last <= "1" & port_f_in_last when out_counter = stream_length - 1 else "0" & port_f_in_last;
    port_out_valid <= port_f_in_valid;
    port_f_in_ready <= port_out_ready(port_f_in_ready'high downto port_f_in_ready'low);

    outgoing_elements_counter_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                out_counter <= 0;
            else
                if port_f_in_valid = '1' and port_f_in_ready(port_f_in_ready'high) = '1' then
                    if out_counter < stream_length - 1 then
                        out_counter <= out_counter + 1;
                    else
                        out_counter <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- input logics

end behavioral;
