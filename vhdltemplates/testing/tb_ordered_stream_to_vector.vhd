library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_ordered_stream_to_vector is
end tb_ordered_stream_to_vector;

architecture behavioral of tb_ordered_stream_to_vector is
    component ordered_stream_to_vector
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_in_data: in data_word_stream_type;
            port_in_valid: in std_logic;
            port_in_ready: out std_logic;
            port_out_data: out data_word_vector_type;
            port_out_valid: out std_logic;
            port_out_ready: in std_logic
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    signal reset: std_logic;

    signal tb_data_in: data_word_stream_type;
    signal tb_data_in_valid: std_logic;
    signal tb_data_in_ready: std_logic;
    signal tb_data_out: data_word_vector_type;
    signal tb_data_out_valid: std_logic;
    signal tb_data_out_ready: std_logic;

begin

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset_process: process
    begin
        reset <= '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        reset <= '0';
        wait;
    end process;

    uut: ordered_stream_to_vector port map(
        clk => clk,
        reset => reset,
        port_in_data => tb_data_in,
        port_in_valid => tb_data_in_valid,
        port_in_ready => tb_data_in_ready,
        port_out_data => tb_data_out,
        port_out_valid => tb_data_out_valid,
        port_out_ready => tb_data_out_ready
    );

    ready_proc: process
    begin
        wait until rising_edge(clk);
        tb_data_out_ready <= '1';
        wait for 41*clk_period;
        wait until rising_edge(clk);
        tb_data_out_ready <= '0';
        wait;
    end process;

    stim_proc: process
    begin
        tb_data_in.last <= "0";
        tb_data_in.data <= "00000000";
        tb_data_in_valid <= '0';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_in.last <= "0";
        tb_data_in.data <= "00000001";
        tb_data_in_valid <= '1';
        wait until rising_edge(clk);
        tb_data_in_valid <= '0';
        wait until rising_edge(clk);
        tb_data_in.data <= "00000010";
        tb_data_in_valid <= '1';
        wait until rising_edge(clk);
        tb_data_in.data <= "00000011";
        tb_data_in_valid <= '1';
        wait until rising_edge(clk);
        tb_data_in.data <= "00000100";
        tb_data_in.last <= "1";
        tb_data_in_valid <= '1';
        wait until rising_edge(clk);
        tb_data_in_valid <= '0';
        wait until rising_edge(clk);
    end process;
end behavioral;
