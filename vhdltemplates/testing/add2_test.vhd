library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity add is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_1_data: in data_word_type;
        port_in_1_last: in last_0_type;
        port_in_1_valid: in std_logic;
        port_in_1_ready: out ready_0_type;
        port_in_2_data: in data_word_type;
        port_in_2_last: in last_0_type;
        port_in_2_valid: in std_logic;
        port_in_2_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end add;

architecture behavioral of add is
    signal a1: std_logic_vector(port_out_data'range) := (others => '0');
    signal a2: std_logic_vector(port_out_data'range) := (others => '0');
begin

    fill_zero: process(port_in_1_data, port_in_2_data)
    begin
        a1 <= (others => '0');
        a1(port_in_1_data'high downto port_in_1_data'low) <= port_in_1_data;
        a2 <= (others => '0');
        a2(port_in_2_data'high downto port_in_2_data'low) <= port_in_2_data;
    end process;

    port_out_data <= std_logic_vector(unsigned(a1) + unsigned(a2));
    port_out_last <= port_in_1_last;
    port_out_valid <= port_in_1_valid and port_in_2_valid;
    port_in_1_ready <= port_out_ready;
    port_in_2_ready <= port_out_ready;

end behavioral;
