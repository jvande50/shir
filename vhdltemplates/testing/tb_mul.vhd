library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.all;
use work.common.all;

entity tb_mul is
end tb_mul;

architecture behavioral of tb_mul is
    component mul_int
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_in_data: in data_word_tuple_type;
            port_in_last: in last_0_type;
            port_in_valid: in std_logic;
            port_in_ready: out ready_0_type;
            port_out_data: out data_word_mul_type;
            port_out_last: out last_0_type;
            port_out_valid: out std_logic;
            port_out_ready: in ready_0_type
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;
    signal reset: std_logic;

    signal tb_data1_in: data_word_type := (others => '0');
    signal tb_data2_in: data_word_type := (others => '0');
    signal tb_valid_in: std_logic := '0';
    signal tb_ready_in: ready_0_type := "0";

    signal tb_data_out: data_word_mul_type := (others => '0');
    signal tb_valid_out: std_logic := '0';
    signal tb_ready_out: ready_0_type := "0";

    signal counter_value: natural := 0;

    signal switch: std_logic := '1';
begin

    uut: mul_int port map(
        clk => clk,
        reset => reset,
        port_in_data.t0 => tb_data1_in,
        port_in_data.t1 => tb_data2_in,
        port_in_last => (others => '0'),
        port_in_valid => tb_valid_in,
        port_in_ready => tb_ready_in,
        port_out_data => tb_data_out,
        port_out_last => open,
        port_out_valid => tb_valid_out,
        port_out_ready => tb_ready_out
    );

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset <= '1', '0' after 4*clk_period;

    tb_data1_in <= std_logic_vector(to_unsigned(counter_value, tb_data1_in'length));
    tb_data2_in <= std_logic_vector(to_unsigned(counter_value, tb_data1_in'length));

    counter_dimensions_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_value <= 0;
            else
                if tb_valid_in = '1' and tb_ready_in = "1" then
                    counter_value <= counter_value + 1;
                end if;
            end if;
        end if;
    end process;

    random_ready_logic: process(clk)
        variable seed1, seed2 : positive := 3; 
        impure function rand_int(min_val, max_val: integer) return integer is
            variable r : real;
        begin
            uniform(seed1, seed2, r);
            return integer(round(r * real(max_val - min_val + 1) + real(min_val) - 0.5));
        end function;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                tb_valid_in <= '0';
                tb_ready_out <= "0";
            else
                if rand_int(0, 2) = 0 then
                    tb_ready_out(0) <= not tb_ready_out(0);
                end if;
                if rand_int(0, 2) = 1 then
                    tb_valid_in <= not tb_valid_in;
                end if;
            end if;
        end if;
    end process;


end behavioral;
