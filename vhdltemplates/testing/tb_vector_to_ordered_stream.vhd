library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_vector_to_ordered_stream is
end tb_vector_to_ordered_stream;

architecture behavioral of tb_vector_to_ordered_stream is
    component vector_to_ordered_stream
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_in_data: in data_word_vector_type;
            port_in_valid: in std_logic;
            port_in_ready: out ready_type;
            port_out_data: out data_word_stream_type;
            port_out_valid: out std_logic;
            port_out_ready: in ready_1_type
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;
    signal reset: std_logic;

    signal tb_data_in: data_word_vector_type := ("00000100", "00000011", "00000010", "00000001");
    signal tb_data_in_valid: std_logic := '0';
    signal tb_data_in_ready: ready_type := "0";
    signal tb_data_out: data_word_stream_type;
    signal tb_data_out_valid: std_logic := '0';
    signal tb_data_out_ready: ready_1_type := "00";
begin
    uut: vector_to_ordered_stream port map(
        clk => clk,
        reset => reset,
        port_in_data => tb_data_in,
        port_in_valid => tb_data_in_valid,
        port_in_ready => tb_data_in_ready,
        port_out_data => tb_data_out,
        port_out_valid => tb_data_out_valid,
        port_out_ready => tb_data_out_ready
    );

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset_process: process
    begin
        reset <= '1';
        wait for 3*clk_period;
        wait until rising_edge(clk);
        reset <= '0';
        wait;
    end process;

    stim_proc: process
    begin
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_in_valid <= '1';

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_in_valid <= '1';
        tb_data_in_valid <= '1';
        tb_data_in_valid <= '1';
        tb_data_out_ready <= "01";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "00";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "01";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "00";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "01";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "00";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "11";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "00";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "11";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "00";
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_data_out_ready <= "11";
        wait;
    end process;
end behavioral;
