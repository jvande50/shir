library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_reduce_ordered_stream is
end tb_reduce_ordered_stream;

architecture behavioral of tb_reduce_ordered_stream is
    component reduce_ordered_stream
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_in_data: in data_word_type;
            port_in_last: in last_1_type;
            port_in_valid: in std_logic;
            port_in_ready: out ready_1_type;
            port_out_data: out data_word_type;
            port_out_last: out last_0_type;
            port_out_valid: out std_logic;
            port_out_ready: in ready_0_type
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    signal reset: std_logic;
    constant wait_time: time := 2 * clk_period;

    signal tb_in_data: data_word_type := (others => '0');
    signal tb_in_last: last_1_type := (others => '0');
    signal tb_in_valid: std_logic := '0';
    signal tb_in_ready: ready_1_type := "00";
    signal tb_out_data: data_word_type := (others => '0');
    signal tb_out_last: last_0_type := (others => '0');
    signal tb_out_valid: std_logic := '0';
    signal tb_out_ready: ready_0_type := "0";

    signal count: natural := 0;

begin

    uut: reduce_ordered_stream port map(
        clk => clk,
        reset => reset,
        port_in_data => tb_in_data,
        port_in_last => tb_in_last,
        port_in_valid => tb_in_valid,
        port_in_ready => tb_in_ready,
        port_out_data => tb_out_data,
        port_out_last => tb_out_last,
        port_out_valid => tb_out_valid,
        port_out_ready => tb_out_ready
    );

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset <= '1', '0' after 4*clk_period;

    tb_in_data <= "00000001";
    tb_in_last <= "1" when count = 8 else "0";

    tb_out_ready <= "1";

    stim_proc: process
    begin
        tb_in_valid <= '1';
        wait until rising_edge(clk);
        --tb_in_valid <= '0';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
    end process;

    last_process: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                count <= 0;
            else
                if tb_in_valid = '1' and tb_in_ready(tb_in_ready'low) = '1' then
                    if count < 8 then
                        count <= count + 1;
                    else
                        count <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
