library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_counter is
end tb_counter;

architecture behavioral of tb_counter is
    component counter
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_out_data: out data_word_type;
            port_out_last: out last_3_type;
            port_out_valid: out std_logic;
            port_out_ready: in ready_3_type
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;
    signal reset: std_logic;

    signal tb_data: data_word_type := (others => '0');
    signal tb_last: last_3_type := "000";
    signal tb_valid: std_logic := '0';
    signal tb_ready: ready_3_type := "0000";

    signal switch: std_logic := '1';
begin

    uut: counter port map(
        clk => clk,
        reset => reset,
        port_out_data => tb_data,
        port_out_last => tb_last,
        port_out_valid => tb_valid,
        port_out_ready => tb_ready
    );

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset <= '1', '0' after 4*clk_period;

    tb_ready(0) <= '1';
    tb_ready(1) <= '1' when tb_last(0) = '1' and switch = '1' else '0';
    tb_ready(2) <= '1' when tb_last(0) = '1' and tb_last(1) = '1' and switch = '1' else '0';
    tb_ready(3) <= '1' when tb_last(0) = '1' and tb_last(1) = '1' and tb_last(2) = '1' and switch = '1' else '0';


end behavioral;
