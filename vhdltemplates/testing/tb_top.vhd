library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity tb_top is
end tb_top;

architecture behavioral of tb_top is
    component host_memory
        port(
            clk: in std_logic;
            -- read request
            read_req_address: in type_IntTypeArithType42;
            read_req_mdata: in type_VectorTypeLogicTypeArithType16;
            read_req_valid: in std_logic;
            read_req_almFull: out std_logic;
            -- read response
            read_rsp_data: out type_VectorTypeLogicTypeArithType512;
            read_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
            read_rsp_valid: out std_logic;
            -- write request
            write_req_address: in type_IntTypeArithType42;
            write_req_mdata: in type_VectorTypeLogicTypeArithType16;
            write_req_data: in type_VectorTypeLogicTypeArithType512;
            write_req_valid: in std_logic;
            write_req_almFull: out std_logic;
            -- write response
            write_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
            write_rsp_valid: out std_logic;
            -- final mem image correct
            mem_correct: out std_logic
        );
    end component;

    component top
        port(
            clk: in type_LogicType;
            reset: in type_LogicType;
            p_in_read_mem_ready: in type_LogicType;
            p_out_read_req_address: out type_IntTypeArithType42;
            p_out_read_req_mdata: out type_VectorTypeLogicTypeArithType16;
            p_out_read_req_valid: out type_LogicType;
            p_out_read_req_total: out type_IntTypeArithType64;
            p_out_read_req_pending: out type_IntTypeArithType64;
            p_in_read_req_almFull: in type_LogicType;
            p_in_read_rsp_data: in type_VectorTypeLogicTypeArithType512;
            p_in_read_rsp_mdata: in type_VectorTypeLogicTypeArithType16;
            p_in_read_rsp_valid: in type_LogicType;
            p_in_write_mem_ready: in type_LogicType;
            p_out_write_req_address: out type_IntTypeArithType42;
            p_out_write_req_mdata: out type_VectorTypeLogicTypeArithType16;
            p_out_write_req_data: out type_VectorTypeLogicTypeArithType512;
            p_out_write_req_valid: out type_LogicType;
            p_out_write_req_total: out type_IntTypeArithType64;
            p_out_write_req_pending: out type_IntTypeArithType64;
            p_in_write_req_almFull: in type_LogicType;
            p_in_write_rsp_mdata: in type_VectorTypeLogicTypeArithType16;
            p_in_write_rsp_valid: in type_LogicType;
            p2_out_data: out std_logic_vector(-1 downto 0);
            p2_out_last: out std_logic_vector(-1 downto 0);
            p2_out_valid: out type_LogicType;
            p2_in_ready: in type_ReadyVectorTypeArithType0
        );
    end component;
    
    signal tb_read_mem_ready: type_LogicType;
    signal tb_read_req_address: type_IntTypeArithType42;
    signal tb_read_req_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_read_req_valid: type_LogicType;
    signal tb_read_req_total: type_IntTypeArithType64;
    signal tb_read_req_pending: type_IntTypeArithType64;
    signal tb_read_req_almFull: type_LogicType;
    signal tb_read_rsp_data: type_VectorTypeLogicTypeArithType512;
    signal tb_read_rsp_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_read_rsp_valid: type_LogicType;

    signal tb_write_mem_ready: type_LogicType;
    signal tb_write_req_address: type_IntTypeArithType42;
    signal tb_write_req_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_write_req_data: type_VectorTypeLogicTypeArithType512;
    signal tb_write_req_valid: type_LogicType;
    signal tb_write_req_total: type_IntTypeArithType64;
    signal tb_write_req_pending: type_IntTypeArithType64;
    signal tb_write_req_almFull: type_LogicType;
    signal tb_write_rsp_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_write_rsp_valid: type_LogicType;

    signal tb_valid: type_LogicType;
    signal tb_ready: type_ReadyVectorTypeArithType0;

    signal tb_mem_correct: std_logic;

    signal clk: std_logic;
    signal reset: std_logic := '1';
    constant clk_period: time := 100 ns;

    signal clk_counter: natural := 0;

begin
    
    hm: host_memory port map(
        clk => clk,

        read_req_address => tb_read_req_address,
        read_req_mdata => tb_read_req_mdata,
        read_req_valid => tb_read_req_valid,
        read_req_almFull => tb_read_req_almFull,

        read_rsp_data => tb_read_rsp_data,
        read_rsp_mdata => tb_read_rsp_mdata,
        read_rsp_valid => tb_read_rsp_valid,

        write_req_address => tb_write_req_address,
        write_req_mdata => tb_write_req_mdata,
        write_req_data => tb_write_req_data,
        write_req_valid => tb_write_req_valid,
        write_req_almFull => tb_write_req_almFull,

        write_rsp_mdata => tb_write_rsp_mdata,
        write_rsp_valid => tb_write_rsp_valid,

        mem_correct => tb_mem_correct
    );

    uut: top port map(
        clk => clk,
        reset => reset,

        p_in_read_mem_ready => tb_read_mem_ready,
        p_out_read_req_address => tb_read_req_address,
        p_out_read_req_mdata => tb_read_req_mdata,
        p_out_read_req_valid => tb_read_req_valid,
        p_out_read_req_total => tb_read_req_total,
        p_out_read_req_pending => tb_read_req_pending,
        p_in_read_req_almFull => tb_read_req_almFull,
        
        p_in_read_rsp_data => tb_read_rsp_data,
        p_in_read_rsp_mdata => tb_read_rsp_mdata,
        p_in_read_rsp_valid => tb_read_rsp_valid,

        p_in_write_mem_ready => tb_write_mem_ready,
        p_out_write_req_address => tb_write_req_address,
        p_out_write_req_mdata => tb_write_req_mdata,
        p_out_write_req_data => tb_write_req_data,
        p_out_write_req_valid => tb_write_req_valid,
        p_out_write_req_total => tb_write_req_total,
        p_out_write_req_pending => tb_write_req_pending,
        p_in_write_req_almFull => tb_write_req_almFull,

        p_in_write_rsp_mdata => tb_write_rsp_mdata,
        p_in_write_rsp_valid => tb_write_rsp_valid,

        p2_out_data => open,
        p2_out_last => open,
        p2_out_valid => tb_valid,
        p2_in_ready => tb_ready
    );

    clock_counter: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                clk_counter <= 0;
            else
                clk_counter <= clk_counter + 1;
            end if;
        end if;
    end process;

    clock_generator: process
    begin
        clk <= '1';
        -- now < 10 ms
        while (clk_counter < 1000000000 and (reset = '1' or tb_valid = '0')) loop
            clk <= '1';
            wait for clk_period /2;
            clk <= '0';
            wait for clk_period /2;
        end loop;
        report "<<<cycles:" & natural'image(clk_counter) & ">>>";
        if tb_mem_correct = '1' then
            report "<<<correctResult:1>>>";
        else
            report "<<<correctResult:0>>>";
        end if;
        report "<<<readRequestsTotal:" & natural'image(to_integer(unsigned(tb_read_req_total))) & ">>>";
        report "<<<readRequestsPending:" & natural'image(to_integer(unsigned(tb_read_req_pending))) & ">>>";
        report "<<<writeRequestsTotal:" & natural'image(to_integer(unsigned(tb_write_req_total))) & ">>>";
        report "<<<writeRequestsPending:" & natural'image(to_integer(unsigned(tb_write_req_pending))) & ">>>";
        wait;
    end process;

    tb_ready(0) <= '1';

    reset <= '1', '0' after 4*clk_period;
    tb_read_mem_ready <= not reset;
    tb_write_mem_ready <= not reset;

end behavioral;
