library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_priority_encoder is
end tb_priority_encoder;

architecture behavioral of tb_priority_encoder is
    component priority_encoder
        generic(
            width: natural := 64
        );
        port(
            port_in_data: in std_logic_vector(width-1 downto 0);
            port_out_index: out natural range 0 to width-1;
            port_out_valid: out std_logic
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;

    signal tb_data_in: std_logic_vector(63 downto 0) := (others => '0');
    signal tb_index_out: natural;
    signal tb_valid_out: std_logic := '0';
begin
    uut: priority_encoder port map(
        port_in_data => tb_data_in,
        port_out_index => tb_index_out,
        port_out_valid => tb_valid_out
    );

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    stim: process
    begin
        wait for clk_period;
        tb_data_in <= (others => '0');
        wait for clk_period;
        tb_data_in <= (others => '0');
        tb_data_in(63) <= '1';
        wait for clk_period;
        tb_data_in <= (others => '0');
        tb_data_in(0) <= '1';
        wait for clk_period;
        tb_data_in <= (others => '0');
        tb_data_in(22) <= '1';
        wait for clk_period;
        tb_data_in(9) <= '1';
        wait for clk_period;
        tb_data_in <= (others => '0');
        tb_data_in(9) <= '1';
    end process;

end behavioral;
