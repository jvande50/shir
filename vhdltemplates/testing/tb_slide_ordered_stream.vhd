library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_slide_ordered_stream is
end tb_slide_ordered_stream;

architecture behavioral of tb_slide_ordered_stream is
    component slide_ordered_stream
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_in_data: in data_word_type;
            port_in_last: in last_1_type;
            port_in_valid: in std_logic;
            port_in_ready: out ready_1_type;
            port_out_data: out data_word_window_vector_type;
            port_out_last: out last_1_type;
            port_out_valid: out std_logic;
            port_out_ready: in ready_1_type
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    signal reset: std_logic;

    signal tb_in_data: data_word_type;
    signal tb_in_last: last_1_type;
    signal tb_in_valid: std_logic := '0';
    signal tb_in_ready: ready_1_type;
    signal tb_out_data: data_word_window_vector_type;
    signal tb_out_last: last_1_type;
    signal tb_out_valid: std_logic;
    signal tb_out_ready: ready_1_type;

    signal index: natural := 0;
begin

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset_process: process
    begin
        reset <= '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        reset <= '0';
        wait;
    end process;

    uut: slide_ordered_stream port map(
                   clk => clk,
                 reset => reset,
        port_in_data   => tb_in_data,
        port_in_last   => tb_in_last,
        port_in_valid  => tb_in_valid,
        port_in_ready  => tb_in_ready,
        port_out_data  => tb_out_data,
        port_out_last  => tb_out_last,
        port_out_valid => tb_out_valid,
        port_out_ready => tb_out_ready
    );
    
    tb_in_last <= "0" when index < 7 else "1";
    tb_in_data <= std_logic_vector(to_unsigned(index, tb_in_data'length));
    tb_in_valid <= '1';

    tb_out_ready(0) <= '1';
    tb_out_ready(1) <= '1' when tb_out_last = "1" else '0';

    stim_proc: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
            else
                if tb_in_ready(tb_in_ready'high - 1) = '1' then
                    if index < 7 then
                        index <= index + 1;
                    else
                        index <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
