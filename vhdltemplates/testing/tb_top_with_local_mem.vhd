library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity tb_top is
end tb_top;

architecture behavioral of tb_top is
    component host_memory
        port(
            clk: in std_logic;
            -- read request
            read_req_address: in type_IntTypeArithType42;
            read_req_mdata: in type_VectorTypeLogicTypeArithType16;
            read_req_valid: in std_logic;
            read_req_almFull: out std_logic;
            -- read response
            read_rsp_data: out type_VectorTypeLogicTypeArithType512;
            read_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
            read_rsp_valid: out std_logic;
            -- write request
            write_req_address: in type_IntTypeArithType42;
            write_req_mdata: in type_VectorTypeLogicTypeArithType16;
            write_req_data: in type_VectorTypeLogicTypeArithType512;
            write_req_valid: in std_logic;
            write_req_almFull: out std_logic;
            -- write response
            write_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
            write_rsp_valid: out std_logic;
            -- final mem image correct
            mem_correct: out std_logic
        );
    end component;

    component local_memory is
        port(
            clk: in std_logic;
            
            bank_waitrequest: out std_logic;
            bank_readdata: out std_logic_vector(512-1 downto 0);
            bank_readdatavalid: out std_logic;

            bank_burstcount: in std_logic_vector(7-1 downto 0);     -- Note that this is not used by model
            bank_writedata: in std_logic_vector(512-1 downto 0);
            bank_address: in std_logic_vector(10-1 downto 0);
            bank_write: in std_logic;
            bank_read: in std_logic;
            bank_byteenable: in std_logic_vector(64-1 downto 0)     -- Also not used by model
        );
    end component;

    component top
        port(
            clk: in type_LogicType;
            reset: in type_LogicType;
            p_in_read_mem_ready: in type_LogicType;
            p_out_read_req_address: out type_IntTypeArithType42;
            p_out_read_req_mdata: out type_VectorTypeLogicTypeArithType16;
            p_out_read_req_valid: out type_LogicType;
            p_out_read_req_total: out type_IntTypeArithType64;
            p_out_read_req_pending: out type_IntTypeArithType64;
            p_in_read_rsp_data: in type_VectorTypeLogicTypeArithType512;
            p_in_read_rsp_mdata: in type_VectorTypeLogicTypeArithType16;
            p_in_read_rsp_valid: in type_LogicType;
            p_in_write_mem_ready: in type_LogicType;
            p_out_write_req_address: out type_IntTypeArithType42;
            p_out_write_req_mdata: out type_VectorTypeLogicTypeArithType16;
            p_out_write_req_data: out type_VectorTypeLogicTypeArithType512;
            p_out_write_req_valid: out type_LogicType;
            p_out_write_req_total: out type_IntTypeArithType64;
            p_out_write_req_pending: out type_IntTypeArithType64;
            p_in_write_rsp_mdata: in type_VectorTypeLogicTypeArithType16;
            p_in_write_rsp_valid: in type_LogicType;
            p2_in_bank_waitrequest: in type_LogicType;
            p2_in_bank_readdata: in type_VectorTypeLogicTypeArithType512;
            p2_in_bank_readdatavalid: in type_LogicType;
            p2_out_bank_burstcount: out type_VectorTypeLogicTypeArithType7;
            p2_out_bank_writedata: out type_VectorTypeLogicTypeArithType512;
            p2_out_bank_address: out type_IntTypeArithType27;
            p2_out_bank_write: out type_LogicType;
            p2_out_bank_read: out type_LogicType;
            p2_out_bank_byteenable: out type_VectorTypeLogicTypeArithType64;
            --p2_out_write_req_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_write_rsp_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_read_req_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_read_rsp_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_obr_write_req_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_obr_read_req_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_obr_read_rsp_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_write_req_id_req_sum: out type_VectorTypeLogicTypeArithType64;
            --p2_out_write_req_id_rsp_sum: out type_VectorTypeLogicTypeArithType64;
            --p2_out_read_req_id_req_sum: out type_VectorTypeLogicTypeArithType64;
            --p2_out_read_req_id_rsp_sum: out type_VectorTypeLogicTypeArithType64;
            --p2_out_client_read_req_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_idx_buffer_count: out type_VectorTypeLogicTypeArithType64;
            --p2_out_OBR_controller_information: out type_VectorTypeLogicTypeArithType5;
            p3_out_data: out std_logic_vector(-1 downto 0);
            p3_out_last: out std_logic_vector(-1 downto 0);
            p3_out_valid: out type_LogicType;
            p3_in_ready: in type_ReadyVectorTypeArithType0
        );
    end component;
    
    signal tb_read_mem_ready: type_LogicType;
    signal tb_read_req_address: type_IntTypeArithType42;
    signal tb_read_req_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_read_req_valid: type_LogicType;
    signal tb_read_req_total: type_IntTypeArithType64;
    signal tb_read_req_pending: type_IntTypeArithType64;
    signal tb_read_rsp_data: type_VectorTypeLogicTypeArithType512;
    signal tb_read_rsp_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_read_rsp_valid: type_LogicType;

    signal tb_write_mem_ready: type_LogicType;
    signal tb_write_req_address: type_IntTypeArithType42;
    signal tb_write_req_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_write_req_data: type_VectorTypeLogicTypeArithType512;
    signal tb_write_req_valid: type_LogicType;
    signal tb_write_req_total: type_IntTypeArithType64;
    signal tb_write_req_pending: type_IntTypeArithType64;
    signal tb_write_rsp_mdata: type_VectorTypeLogicTypeArithType16;
    signal tb_write_rsp_valid: type_LogicType;

    signal tb_valid: type_LogicType;
    signal tb_ready: type_ReadyVectorTypeArithType0;

    signal tb_memory0_waitrequest: type_LogicType;
    signal tb_memory0_readdata: type_VectorTypeLogicTypeArithType512;
    signal tb_memory0_readdatavalid: type_LogicType;
    signal tb_memory0_burstcount: type_VectorTypeLogicTypeArithType7;
    signal tb_memory0_writedata: type_VectorTypeLogicTypeArithType512;
    signal tb_memory0_address: type_IntTypeArithType27;
    signal tb_memory0_write: type_LogicType;
    signal tb_memory0_read: type_LogicType;
    signal tb_memory0_byteenable: type_VectorTypeLogicTypeArithType64;

    --signal temp_write_req_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_write_rsp_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_read_req_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_read_rsp_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_obr_write_req_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_obr_read_req_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_obr_read_rsp_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_write_req_id_req_sum: type_VectorTypeLogicTypeArithType64;
    --signal temp_write_req_id_rsp_sum: type_VectorTypeLogicTypeArithType64;
    --signal temp_read_req_id_req_sum: type_VectorTypeLogicTypeArithType64;
    --signal temp_read_req_id_rsp_sum: type_VectorTypeLogicTypeArithType64;
    --signal temp_client_read_req_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_idx_buffer_count: type_VectorTypeLogicTypeArithType64;
    --signal temp_information: type_VectorTypeLogicTypeArithType64;

    signal tb_mem_correct: std_logic;

    signal clk: std_logic;
    signal reset: std_logic := '1';
    constant clk_period: time := 100 ns;

    signal clk_counter: natural := 0;

    signal is_read_going_on: std_logic;
    signal is_write_going_on: std_logic;

    signal num_writes: natural;
    signal num_reads: natural;

    signal write_clk_counter: natural;
    signal read_clk_counter: natural;
    constant NUM_REQS: natural := 4;

begin
    
    hm: host_memory port map(
        clk => clk,

        read_req_address => tb_read_req_address,
        read_req_mdata => tb_read_req_mdata,
        read_req_valid => tb_read_req_valid,
        read_req_almFull => open,

        read_rsp_data => tb_read_rsp_data,
        read_rsp_mdata => tb_read_rsp_mdata,
        read_rsp_valid => tb_read_rsp_valid,

        write_req_address => tb_write_req_address,
        write_req_mdata => tb_write_req_mdata,
        write_req_data => tb_write_req_data,
        write_req_valid => tb_write_req_valid,
        write_req_almFull => open,

        write_rsp_mdata => tb_write_rsp_mdata,
        write_rsp_valid => tb_write_rsp_valid,

        mem_correct => tb_mem_correct
    );

    lm: local_memory port map(
        clk => clk,

        bank_waitrequest => tb_memory0_waitrequest,
        bank_readdata => tb_memory0_readdata,
        bank_readdatavalid => tb_memory0_readdatavalid,

        bank_burstcount => tb_memory0_burstcount,
        bank_address => tb_memory0_address(10-1 downto 0),
        bank_writedata => tb_memory0_writedata,
        bank_write => tb_memory0_write,
        bank_read => tb_memory0_read,
        bank_byteenable => tb_memory0_byteenable
    );

    uut: top port map(
        clk => clk,
        reset => reset,

        p_in_read_mem_ready => tb_read_mem_ready,
        p_out_read_req_address => tb_read_req_address,
        p_out_read_req_mdata => tb_read_req_mdata,
        p_out_read_req_valid => tb_read_req_valid,
        p_out_read_req_total => tb_read_req_total,
        p_out_read_req_pending => tb_read_req_pending,

        p_in_read_rsp_data => tb_read_rsp_data,
        p_in_read_rsp_mdata => tb_read_rsp_mdata,
        p_in_read_rsp_valid => tb_read_rsp_valid,

        p_in_write_mem_ready => tb_write_mem_ready,
        p_out_write_req_address => tb_write_req_address,
        p_out_write_req_mdata => tb_write_req_mdata,
        p_out_write_req_data => tb_write_req_data,
        p_out_write_req_valid => tb_write_req_valid,
        p_out_write_req_total => tb_write_req_total,
        p_out_write_req_pending => tb_write_req_pending,

        p_in_write_rsp_mdata => tb_write_rsp_mdata,
        p_in_write_rsp_valid => tb_write_rsp_valid,

        p2_in_bank_waitrequest => tb_memory0_waitrequest,
        p2_in_bank_readdata => tb_memory0_readdata,
        p2_in_bank_readdatavalid => tb_memory0_readdatavalid,
        p2_out_bank_burstcount => tb_memory0_burstcount,
        p2_out_bank_writedata => tb_memory0_writedata,
        p2_out_bank_address => tb_memory0_address,
        p2_out_bank_write => tb_memory0_write,
        p2_out_bank_read => tb_memory0_read,
        p2_out_bank_byteenable => tb_memory0_byteenable,

        --p2_out_write_req_count => temp_write_req_count,
        --p2_out_write_rsp_count => temp_write_rsp_count,
        --p2_out_read_req_count => temp_read_req_count,
        --p2_out_read_rsp_count => temp_read_rsp_count,
        --p2_out_obr_write_req_count => temp_obr_write_req_count,
        --p2_out_obr_read_req_count => temp_obr_read_req_count,
        --p2_out_obr_read_rsp_count => temp_obr_read_rsp_count,
        --p2_out_write_req_id_req_sum => temp_write_req_id_req_sum,
        --p2_out_write_req_id_rsp_sum => temp_write_req_id_rsp_sum,
        --p2_out_read_req_id_req_sum => temp_read_req_id_req_sum,
        --p2_out_read_req_id_rsp_sum => temp_read_req_id_rsp_sum,
        --p2_out_client_read_req_count => temp_client_read_req_count,
        --p2_out_idx_buffer_count => temp_idx_buffer_count,
        --p2_out_OBR_controller_information => temp_information(4 downto 0),

        p3_out_data => open,
        p3_out_last => open,
        p3_out_valid => tb_valid,
        p3_in_ready => tb_ready
    );

    clock_counter: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                clk_counter <= 0;
            else
                clk_counter <= clk_counter + 1;
            end if;
        end if;
    end process;

    num_writes_proc: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                num_writes <= 0;
            else
                if tb_memory0_write = '1' and tb_memory0_waitrequest = '0' then
                    num_writes <= num_writes + 1;
                end if;
            end if;
        end if;
    end process;

    num_reads_proc: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                num_reads <= 0;
            else
                if tb_memory0_readdatavalid = '1' then
                    num_reads <= num_reads + 1;
                end if;
            end if;
        end if;
    end process;

    is_write_going_on_proc: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                is_write_going_on <= '0';
            else
                if is_write_going_on = '0' and tb_memory0_write = '1' and num_writes = 0 then
                    is_write_going_on <= '1';
                elsif is_write_going_on = '1' and num_writes = NUM_REQS then
                    is_write_going_on <= '0';
                end if;
            end if;
        end if;
    end process;

    is_read_going_on_proc: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                is_read_going_on <= '0';
            else
                if is_read_going_on = '0' and tb_memory0_readdatavalid = '1' and num_reads = 0 then
                    is_read_going_on <= '1';
                elsif is_read_going_on = '1' and num_reads = NUM_REQS then
                    is_read_going_on <= '0';
                end if;
            end if;
        end if;
    end process;

    clk_counter_writes: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                write_clk_counter <= 0;
            else
                if is_write_going_on = '1' then
                    write_clk_counter <= write_clk_counter + 1;
                end if;
            end if;
        end if;
    end process;

    clk_counter_reads: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                read_clk_counter <= 0;
            else
                if is_read_going_on = '1' then
                    read_clk_counter <= read_clk_counter + 1;
                end if;
            end if;
        end if;
    end process;

    clock_generator: process
    begin
        clk <= '1';
        -- now < 10 ms
        while (clk_counter < 1000000000 and (reset = '1' or tb_valid = '0')) loop
            clk <= '1';
            wait for clk_period /2;
            clk <= '0';
            wait for clk_period /2;
        end loop;
        report "<<<cycles:" & natural'image(clk_counter) & ">>>";
        if tb_mem_correct = '1' then
            report "<<<correctResult:1>>>";
        else
            report "<<<correctResult:0>>>";
        end if;
        report "<<<readRequestsTotal:" & natural'image(to_integer(unsigned(tb_read_req_total))) & ">>>";
        report "<<<readRequestsPending:" & natural'image(to_integer(unsigned(tb_read_req_pending))) & ">>>";
        report "<<<writeRequestsTotal:" & natural'image(to_integer(unsigned(tb_write_req_total))) & ">>>";
        report "<<<writeRequestsPending:" & natural'image(to_integer(unsigned(tb_write_req_pending))) & ">>>";
        report "<<<Number of cycles required for writing:" & natural'image(write_clk_counter) & ">>>";
        report "<<<Number of cycles required for reading:" & natural'image(read_clk_counter) & ">>>";
        --report "<<<Number of write requests recieved:" & natural'image(to_integer(unsigned(temp_write_req_count))) & ">>>";
        --report "<<<Number of write responses sent out:" & natural'image(to_integer(unsigned(temp_write_rsp_count))) & ">>>";
        --report "<<<Number of read requests recieved:" & natural'image(to_integer(unsigned(temp_read_req_count))) & ">>>";
        --report "<<<Number of read responses sent out:" & natural'image(to_integer(unsigned(temp_read_rsp_count))) & ">>>";
        --report "<<<Number of write requests sent to OBR:" & natural'image(to_integer(unsigned(temp_obr_write_req_count))) & ">>>";
        --report "<<<Number of read requests sent to OBR:" & natural'image(to_integer(unsigned(temp_obr_read_req_count))) & ">>>";
        --report "<<<Number of read responses recieved from OBR:" & natural'image(to_integer(unsigned(temp_obr_read_rsp_count))) & ">>>";
        --report "<<<Sum of all write Request IDs (reqs) to controller:" & natural'image(to_integer(unsigned(temp_write_req_id_req_sum))) & ">>>";
        --report "<<<Sum of all write Request IDs (rsps) from controller:" & natural'image(to_integer(unsigned(temp_write_req_id_rsp_sum))) & ">>>";
        --report "<<<Sum of all read Request IDs (reqs) to controller:" & natural'image(to_integer(unsigned(temp_read_req_id_req_sum))) & ">>>";
        --report "<<<Sum of all read Request IDs (rsps) from controller:" & natural'image(to_integer(unsigned(temp_read_req_id_rsp_sum))) & ">>>";
        --report "<<<Total number of read requests sent by client:" & natural'image(to_integer(unsigned(temp_client_read_req_count))) & ">>>";
        --report "<<<Number of elements inside Req ID Buffer:" & natural'image(to_integer(unsigned(temp_idx_buffer_count))) & ">>>";
        --report "<<<Status Register of Controller:" & natural'image(to_integer(unsigned(temp_information))) & ">>>";
        wait;
    end process;

    tb_ready(0) <= '1';

    reset <= '1', '0' after 4*clk_period;
    tb_read_mem_ready <= not reset;
    tb_write_mem_ready <= not reset;

end behavioral;
