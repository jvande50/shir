library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.common.all;

entity host_memory is
    generic(
        req_buffer_size: natural := 64
    );
    port(
        clk: in std_logic;
        -- read request
        read_req_address: in type_IntTypeArithType42;
        read_req_mdata: in type_VectorTypeLogicTypeArithType16;
        read_req_valid: in std_logic;
        read_req_almFull: out std_logic;
        -- read response
        read_rsp_data: out type_VectorTypeLogicTypeArithType512;
        read_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
        read_rsp_valid: out std_logic;
        -- write request
        write_req_address: in type_IntTypeArithType42;
        write_req_mdata: in type_VectorTypeLogicTypeArithType16;
        write_req_data: in type_VectorTypeLogicTypeArithType512;
        write_req_valid: in std_logic;
        write_req_almFull: out std_logic;
        -- write response
        write_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
        write_rsp_valid: out std_logic;
        -- final mem image correct
        mem_correct: out std_logic
    );
end host_memory;

architecture behavioral of host_memory is

    impure function count_lines (constant filename: in string) return natural is
        file f: text;
        variable lines: natural := 0;
        variable linebuffer: line;
    begin
        lines := 0;
        file_open(f, filename, read_mode);
        while not endfile(f) loop
            readline(f, linebuffer);
            lines := lines + 1;
        end loop;
        file_close(f);
        return lines;
    end count_lines;

    type mem_image_type is array (0 to count_lines("mem_final.dat") - 1) of std_logic_vector(511 downto 0);

    impure function load_mem_image (constant filename: in string) return mem_image_type is
        file mem_file: text;
        variable index: natural := 0;
        variable linebuffer: line;
        variable cacheline: std_logic_vector(511 downto 0);
        variable result: mem_image_type;
    begin
        index := 0;
        file_open(mem_file, filename, read_mode);
        while not endfile(mem_file) loop
            readline(mem_file, linebuffer);
            read(linebuffer, cacheline);
            result(index) := cacheline;
            index := index + 1;
        end loop;
        file_close(mem_file);
        return result;
    end load_mem_image;

    signal ram: mem_image_type := load_mem_image("mem_initial.dat");

    signal final_ram: mem_image_type := load_mem_image("mem_final.dat");

    type read_req_entry is record
        address: type_IntTypeArithType42;
        mdata: type_VectorTypeLogicTypeArithType16;
        valid: std_logic;
    end record read_req_entry;
    type read_req_buffer_type is array (0 to req_buffer_size-1) of read_req_entry;
    signal read_req_buffer: read_req_buffer_type := (others => (address => (others => '0'), mdata => (others => '0'), valid => '0'));

    type write_req_entry is record
        address: type_IntTypeArithType42;
        mdata: type_VectorTypeLogicTypeArithType16;
        data: type_VectorTypeLogicTypeArithType512;
        valid: std_logic;
    end record write_req_entry;
    type write_req_buffer_type is array (0 to req_buffer_size-1) of write_req_entry;
    signal write_req_buffer: write_req_buffer_type := (others => (address => (others => '0'), mdata => (others => '0'), data => (others => '0'), valid => '0'));

    subtype req_index_type is natural range 0 to req_buffer_size-1;
    signal read_rsp_idx: req_index_type := 0;
    signal read_req_count: natural range 0 to req_buffer_size := 0;
    signal write_rsp_idx: req_index_type := 0;
    signal write_req_count: natural range 0 to req_buffer_size := 0;

begin

    mem_correct <= '1' when ram = final_ram else '0';

    -- set flag, when there are only 8 entries left in the buffer
    read_req_almFull <= '1' when read_req_count + 8 > req_buffer_size-1 else '0';
    write_req_almFull <= '1' when write_req_count + 8 > req_buffer_size-1 else '0';


    -- read

    read_req_counter: process(read_req_buffer)
        variable count: natural range 0 to req_buffer_size := 0;
    begin
        count := 0;
        for i in 0 to req_buffer_size-1 loop
            if read_req_buffer(i).valid = '1' then
                count := count + 1;
            end if;
        end loop;
        read_req_count <= count;
    end process;

    read_requests: process(clk)
    begin
        if rising_edge(clk) then
            -- response at read_rsp_idx has been send
            read_req_buffer(read_rsp_idx).valid <= '0';

            -- request
            if read_req_valid = '1' and read_req_count < req_buffer_size then
                for i in 0 to req_buffer_size-1 loop
                    if read_req_buffer(i).valid = '0' then
                        read_req_buffer(i).address <= read_req_address;
                        read_req_buffer(i).mdata <= read_req_mdata;
                        read_req_buffer(i).valid <= '1';
                        exit;
                    end if;
                end loop;
            end if;
        end if;
    end process;

    read_rsp_data <= ram(to_integer(unsigned(read_req_buffer(read_rsp_idx).address)));
    read_rsp_mdata <= read_req_buffer(read_rsp_idx).mdata;
    read_rsp_valid <= read_req_buffer(read_rsp_idx).valid;

    read_responses: process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to req_buffer_size-1 loop
                -- do not select the same index that has just been responded to
                -- the entry at this position will be invalidated in this cycle
                if i /= read_rsp_idx and read_req_buffer(i).valid = '1' then
                    read_rsp_idx <= i;
                    exit;
                end if;
            end loop;
        end if;
    end process;


    -- write

    write_req_counter: process(write_req_buffer)
        variable count: natural range 0 to req_buffer_size := 0;
    begin
        count := 0;
        for i in 0 to req_buffer_size-1 loop
            if write_req_buffer(i).valid = '1' then
                count := count + 1;
            end if;
        end loop;
        write_req_count <= count;
    end process;

    write_requests: process(clk)
    begin
        if rising_edge(clk) then
            -- response at write_rsp_idx has been send
            write_req_buffer(write_rsp_idx).valid <= '0';

            -- request
            if write_req_valid = '1' and write_req_count < req_buffer_size then
                for i in 0 to req_buffer_size-1 loop
                    if write_req_buffer(i).valid = '0' then
                        write_req_buffer(i).address <= write_req_address;
                        write_req_buffer(i).mdata <= write_req_mdata;
                        write_req_buffer(i).data <= write_req_data;
                        write_req_buffer(i).valid <= '1';
                        exit;
                    end if;
                end loop;
            end if;
        end if;
    end process;

    write_rsp_mdata <= write_req_buffer(write_rsp_idx).mdata;
    write_rsp_valid <= write_req_buffer(write_rsp_idx).valid;

    write_responses: process(clk)
    begin
        if rising_edge(clk) then
            if write_req_buffer(write_rsp_idx).valid = '1' then
                -- this write is late by 1 cycle
                ram(to_integer(unsigned(write_req_buffer(write_rsp_idx).address))) <= write_req_buffer(write_rsp_idx).data;

                if final_ram(to_integer(unsigned(write_req_buffer(write_rsp_idx).address))) = write_req_buffer(write_rsp_idx).data then
                    report "cacheline " & natural'image(to_integer(unsigned(write_req_buffer(write_rsp_idx).address))) & " correctly written!";
                else
                    report "cacheline " & natural'image(to_integer(unsigned(write_req_buffer(write_rsp_idx).address))) & " written! But incorrect value!";
                end if;
            end if;
            for i in 0 to req_buffer_size-1 loop
                if write_req_buffer(i).valid = '1' then
                    write_rsp_idx <= i;
                    exit;
                end if;
            end loop;
        end if;
    end process;

end behavioral;
