library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_split_vector is
end tb_split_vector;

architecture behavioral of tb_split_vector is
    component split_vector
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_in_data: in data_word_joined_vector_type;
            port_in_valid: in std_logic;
            port_in_ready: out std_logic;
            port_out_data: out data_word_vector_type;
            port_out_valid: out std_logic;
            port_out_ready: in std_logic
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;
    signal reset: std_logic;

    signal tb_data_in: data_word_joined_vector_type := "00000100000000110000001000000001";
    signal tb_data_in_valid: std_logic := '0';
    signal tb_data_in_ready: std_logic := '0';
    signal tb_data_out: data_word_vector_type;
    signal tb_data_out_valid: std_logic := '0';
    signal tb_data_out_ready: std_logic := '0';
begin
    uut: split_vector port map(
        clk => clk,
        reset => reset,
        port_in_data => tb_data_in,
        port_in_valid => tb_data_in_valid,
        port_in_ready => tb_data_in_ready,
        port_out_data => tb_data_out,
        port_out_valid => tb_data_out_valid,
        port_out_ready => tb_data_out_ready
    );

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

end behavioral;
