library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_host_memory is
end tb_host_memory;

architecture behavioral of tb_host_memory is
    component host_memory
        port(
            clk: in std_logic;
            -- read request
            read_req_address: in mem_address_type;
            read_req_mdata: in req_id_type;
            read_req_valid: in std_logic;
            read_req_almFull: out std_logic;
            -- read response
            read_rsp_data: out cache_line_type;
            read_rsp_mdata: out req_id_type;
            read_rsp_valid: out std_logic;
            -- write request
            write_req_address: in mem_address_type;
            write_req_mdata: in req_id_type;
            write_req_data: in cache_line_type;
            write_req_valid: in std_logic;
            write_req_almFull: out std_logic;
            -- write response
            write_rsp_mdata: out req_id_type;
            write_rsp_valid: out std_logic
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;

    signal tb_read_req_address: mem_address_type := o"00000000000013";
    signal tb_read_req_mdata: req_id_type := x"00F2";
    signal tb_read_req_valid: std_logic := '0';
    signal tb_read_req_almFull: std_logic;
    signal tb_read_rsp_data: cache_line_type;
    signal tb_read_rsp_mdata: req_id_type;
    signal tb_read_rsp_valid: std_logic;

    signal tb_write_req_address: mem_address_type := o"00000000000013";
    signal tb_write_req_mdata: req_id_type := x"00F2";
    signal tb_write_req_data: cache_line_type := (others => '1');
    signal tb_write_req_valid: std_logic := '0';
    signal tb_write_req_almFull: std_logic;
    signal tb_write_rsp_mdata: req_id_type;
    signal tb_write_rsp_valid: std_logic;
begin
    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    uut: host_memory port map(
        clk => clk,
        read_req_address => tb_read_req_address,
        read_req_mdata => tb_read_req_mdata,
        read_req_valid => tb_read_req_valid,
        read_req_almFull => tb_read_req_almFull,
        read_rsp_data => tb_read_rsp_data,
        read_rsp_mdata => tb_read_rsp_mdata,
        read_rsp_valid => tb_read_rsp_valid,
        write_req_address => tb_write_req_address,
        write_req_mdata => tb_write_req_mdata,
        write_req_data => tb_write_req_data,
        write_req_valid => tb_write_req_valid,
        write_req_almFull => tb_write_req_almFull,
        write_rsp_mdata => tb_write_rsp_mdata,
        write_rsp_valid => tb_write_rsp_valid
    );
    
    stim_read_proc: process
    begin
        wait for 2*clk_period;
        if tb_read_req_almFull = '0' then
            tb_read_req_address <= std_logic_vector(unsigned(tb_read_req_address) + 1);
            tb_read_req_mdata <= std_logic_vector(unsigned(tb_read_req_mdata) + 1);
            tb_read_req_valid <= '1';
        end if;
        wait for clk_period;
        tb_read_req_valid <= '0';
    end process;

    stim_write_proc: process
    begin
        wait for 2*clk_period;
        if tb_write_req_almFull = '0' then
            tb_write_req_address <= std_logic_vector(unsigned(tb_write_req_address) + 1);
            tb_write_req_mdata <= std_logic_vector(unsigned(tb_write_req_mdata) + 1);
            tb_write_req_valid <= '1';
        end if;
        wait for clk_period;
        tb_write_req_valid <= '0';
    end process;

end behavioral;
