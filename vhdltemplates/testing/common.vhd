library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
package common is
    subtype ready_0_type is std_logic_vector(0 downto 0);
    subtype ready_1_type is std_logic_vector(1 downto 0);
    subtype ready_2_type is std_logic_vector(2 downto 0);
    subtype ready_3_type is std_logic_vector(3 downto 0);
    type ready_0_vector_generic_type is array (integer range <>) of ready_0_type;
    type ready_1_vector_generic_type is array (integer range <>) of ready_1_type;

    subtype last_0_type is std_logic_vector(-1 downto 0);
    subtype last_1_type is std_logic_vector(0 downto 0);
    subtype last_2_type is std_logic_vector(1 downto 0);
    subtype last_3_type is std_logic_vector(2 downto 0);
    type last_0_vector_generic_type is array (integer range <>) of last_0_type;
    type last_1_vector_generic_type is array (integer range <>) of last_1_type;

    -- basic data type
    subtype data_word_type is std_logic_vector(7 downto 0);
    subtype data_word_mul_type is std_logic_vector(15 downto 0);
    subtype data_word_mul2add_type is std_logic_vector(16 downto 0);
    type data_word_tuple_type is record
        t0 : data_word_type;
        t1 : data_word_type;
    end record data_word_tuple_type;
    type data_word_tuple_tuple_type is record
        t0 : data_word_tuple_type;
        t1 : data_word_tuple_type;
    end record data_word_tuple_tuple_type;

    -- streams
    subtype index_type is std_logic_vector(7 downto 0);

    alias data_word_stream_type is data_word_type;
    type data_word_unordered_stream_type is record
        data : data_word_type;
        idx : index_type;
    end record data_word_unordered_stream_type;

    alias data_word_stream_of_stream_type is data_word_stream_type;
    type data_word_unordered_stream_of_unordered_stream_type is record
        data : data_word_unordered_stream_type;
        idx : index_type;
    end record data_word_unordered_stream_of_unordered_stream_type;

    -- for join
    type data_word_joined_unordered_stream_type is record
        data : data_word_type;
        idx : std_logic_vector(15 downto 0);
    end record data_word_joined_unordered_stream_type;

    -- vectors
    type data_word_vector_type is array (3 downto 0) of data_word_type;
    type data_word_vector_slice_low_type is array (1 downto 0) of data_word_type;
    type data_word_vector_slice_high_type is array (1 downto 0) of data_word_type;
    alias data_word_vector_slice_type is data_word_vector_slice_low_type;
    type data_word_vector_tuple_type is record
        t0 : data_word_vector_slice_low_type;
        t1 : data_word_vector_slice_high_type;
    end record data_word_vector_tuple_type;
    type data_word_tuple_vector_type is array (1 downto 0) of data_word_tuple_type;
    type natural_vector_type is array (2 downto 0) of natural;

    subtype data_word_joined_vector_type is std_logic_vector(31 downto 0);

    -- vector of basic data type (for arbiter)
    type data_word_vector_generic_type is array (integer range <>) of data_word_type;
    type data_word_stream_vector_generic_type is array (integer range <>) of data_word_stream_type;

    -- map
    alias data_to_f_type is data_word_type;
    alias data_from_f_type is data_word_type;
    
    -- memory
    subtype mem_address_type is std_logic_vector(41 downto 0);
    type memory_input_type is record
        data : data_word_type;
        addr : mem_address_type;
        we : std_logic;
    end record memory_input_type;
    type data_with_address_type is record
        data : data_word_type;
        addr : mem_address_type;
    end record data_with_address_type;
    -- banked buffer
    type mem_address_vector_type is array (3 downto 0) of mem_address_type;
    type memory_input_vector_type is record
        data : data_word_vector_type;
        addr : mem_address_vector_type;
        we : std_logic_vector(3 downto 0);
    end record memory_input_vector_type;
    alias data_word_vector_stream_type is data_word_vector_type;
    type write_input_vector_tuple3_type is record
        t0 : data_word_vector_type;
        t1 : mem_address_vector_type;
        t2 : std_logic_vector(3 downto 0);
    end record write_input_vector_tuple3_type;
    -- dma request id
    subtype req_id_type is std_logic_vector(15 downto 0);
    type request_req_id_type is record
        req_id : req_id_type;
    end record request_req_id_type;
    -- dma memory address
    type request_mem_address_type is record
        data : mem_address_type;
        req_id : req_id_type;
    end record request_mem_address_type;
    type request_mem_address_vector_type is array(integer range <>) of request_mem_address_type;
    -- dma cache line (input to memory controller)
    subtype cache_line_type is std_logic_vector(511 downto 0);
    subtype local_mem_addr_type is std_logic_vector(26 downto 0);
    type request_cache_line_type is record
        data : cache_line_type;
        req_id : req_id_type;
    end record request_cache_line_type;
    type mixed_cache_line_type is record
        data : cache_line_type;
        addr : local_mem_addr_type;
        req_id : req_id_type;
        we : std_logic;
    end record mixed_cache_line_type;
    type request_cache_line_vector_type is array(integer range <>) of request_cache_line_type;
    type cache_line_with_address_type is record
        data : cache_line_type;
        addr : mem_address_type;
    end record cache_line_with_address_type;
    type request_cache_line_with_address_type is record
        data : cache_line_with_address_type;
        req_id : req_id_type;
    end record request_cache_line_with_address_type;
    -- read and write
    subtype base_address_type is std_logic_vector(3 downto 0);

    type cache_line_unordered_stream_type is record
        data : cache_line_type;
        idx : index_type;
    end record cache_line_unordered_stream_type;

    type write_input_tuple_type is record
        t0 : cache_line_type;
        t1 : mem_address_type;
    end record write_input_tuple_type;

    -- for arbiter async function
    alias function_in_type is request_mem_address_type;
    alias function_out_type is request_cache_line_type;
    alias function_in_vector_type is request_mem_address_vector_type;
    alias function_out_vector_type is request_cache_line_vector_type;
    -- for arbiter sync function
    type function_last_vector_type is array (integer range <>) of last_0_type;
    type function_ready_vector_type is array (integer range <>) of ready_0_type;

    -- scheduler output
    type scheduler_output_type is record
        cli_id : std_logic_vector(4 downto 0);
        valid : std_logic;
    end record scheduler_output_type;

    -- window vector for slide
    type data_word_window_vector_type is array(3 downto 0) of data_word_type;

    -- update vector inpu
    type data_word_update_entry_type is record
        t0 : index_type;
        t1 : data_word_type;
    end record data_word_update_entry_type;
    type data_word_update_vector_tuple_type is record
        t0 : data_word_vector_type;
        t1 : data_word_update_entry_type;
    end record data_word_update_vector_tuple_type;

    -- generated host ram types
    alias type_IntTypecoreArithType42 is mem_address_type;
    alias type_VectorTypeLogicTypecoreArithType16 is req_id_type;
    alias type_VectorTypeLogicTypecoreArithType512 is cache_line_type;

end package;
